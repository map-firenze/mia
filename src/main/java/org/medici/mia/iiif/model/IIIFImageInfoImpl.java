package org.medici.mia.iiif.model;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class IIIFImageInfoImpl implements IIIFImageInfo {

  private IIIFImageBitDepth bitDepth;
  private int height;
  private IIIFImageFormat format;
  private int width;

  @Override
  public IIIFImageBitDepth getBitDepth() {
    return bitDepth;
  }

  @Override
  public IIIFImageFormat getFormat() {
    return format;
  }

  @Override
  public int getHeight() {
    return height;
  }

  @Override
  public int getWidth() {
    return width;
  }

  @Override
  public void setBitDepth(IIIFImageBitDepth bitDepth) {
    this.bitDepth = bitDepth;
  }

  @Override
  public void setFormat(IIIFImageFormat format) {
    this.format = format;
  }

  @Override
  public void setHeight(int height) {
    this.height = height;
  }

  @Override
  public void setWidth(int width) {
    this.width = width;
  }

}
