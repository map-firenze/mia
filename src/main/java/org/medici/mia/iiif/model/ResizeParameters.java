package org.medici.mia.iiif.model;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public interface ResizeParameters {

  int getHeight();

  void setHeight(int targetHeight);

  int getMaxHeight();

  void setMaxHeight(int maxHeight);

  int getMaxWidth();

  void setMaxWidth(int maxWidth);

  int getScaleFactor();

  void setScaleFactor(int scaleFactor);

  int getWidth();

  void setWidth(int targetWidth);

}
