package org.medici.mia.iiif.model;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public interface IIIFImageInfo {

  IIIFImageBitDepth getBitDepth();

  void setBitDepth(IIIFImageBitDepth bitDepth);

  IIIFImageFormat getFormat();

  void setFormat(IIIFImageFormat format);

  int getHeight();

  void setHeight(int height);

  int getWidth();

  void setWidth(int width);

}
