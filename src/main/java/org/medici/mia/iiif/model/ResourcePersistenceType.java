package org.medici.mia.iiif.model;

public enum ResourcePersistenceType {
  REFERENCED, MANAGED, RESOLVED, CUSTOM
}
