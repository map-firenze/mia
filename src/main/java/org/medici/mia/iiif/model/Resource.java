package org.medici.mia.iiif.model;


import java.net.URI;
import java.util.UUID;

import javax.activation.MimeType;


public interface Resource {

  String getFilename();

  String getFilenameExtension();

  void setFilenameExtension(String filenameExtension);

  long getLastModified();

  void setLastModified(long lastModified);

  MimeType getMimeType();

  void setMimeType(MimeType mimeType);

  boolean isReadonly();

  void setReadonly(boolean readonly);

  long getSize();

  void setSize(long size);

  URI getUri();

  void setUri(URI uri);

  UUID getUuid();

  void setUuid(UUID uuid);
}
