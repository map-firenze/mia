package org.medici.mia.iiif.model;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
/**
 * The bit depth parameter defines the image's colors: color, grayscale or black and white.
 */
public enum IIIFImageBitDepth {

  COLOR,
  GRAYSCALE,
  BITONAL // black and white
}
