package org.medici.mia.iiif.model;

import java.awt.Dimension;
/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public interface IIIFImageRepository {

  public IIIFImageInfo getImageInfo(String identifier) throws Exception;

  public IIIFImage getImage(String identifier, RegionParameters regionParameters)
          throws Exception;

  public boolean supportsInputFormat(IIIFImageFormat inFormat);

  public boolean supportsOutputFormat(IIIFImageFormat outFormat);

  public boolean supportsCropOperation(RegionParameters region);

  public boolean supportsScaleOperation(Dimension imageDims, ResizeParameters scaleParams);

  public boolean supportsBitDepth(IIIFImageBitDepth bitDepth);
}
