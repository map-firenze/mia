package org.medici.mia.iiif.model;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public interface RegionParameters {

  float getHeight();

  void setHeight(float height);

  float getHorizontalOffset();

  void setHorizontalOffset(float horizontalOffset);

  float getVerticalOffset();

  void setVerticalOffset(float verticalOffset);

  float getWidth();

  void setWidth(float width);

  boolean isAbsolute();

  void setAbsolute(boolean absolute);

}
