package org.medici.mia.iiif.model;


import java.io.IOException;
/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public interface IIIFImage {
  IIIFImageFormat getFormat();

  int getHeight();

  int getWidth();

  byte[] toByteArray() throws UnsupportedOperationException, IOException;

  IIIFImage crop(RegionParameters params) throws Exception;

  IIIFImage scale(ResizeParameters params) throws Exception;

  IIIFImage rotate(int arcDegree) throws Exception;

  IIIFImage flipHorizontally();

  IIIFImage toDepth(IIIFImageBitDepth depth) throws UnsupportedOperationException;

  IIIFImage convert(IIIFImageFormat format) throws UnsupportedOperationException;

  void performTransformation() throws Exception;
}
