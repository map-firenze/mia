package org.medici.mia.service.people;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.medici.mia.common.json.document.DocPeopleFieldJson;
import org.medici.mia.common.json.document.DocumentFactoryJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.document.DocumentPeopleBaseJson;
import org.medici.mia.common.json.document.DocumentPeopleFieldsJson;
import org.medici.mia.common.json.document.DocumentPeopleJson;
import org.medici.mia.dao.documentfield.DocumentFieldDAO;
import org.medici.mia.dao.miadoc.DocumentRefToPeopleDao;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.domain.DocumentFieldEntity;
import org.medici.mia.domain.DocumentRefToPeopleEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.exception.ApplicationThrowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Service
class BiographicalDocumentServiceImpl implements BiographicalDocumentService {

	@Autowired
	private GenericDocumentDao documentDAO;

	@Autowired
	private DocumentFieldDAO documentFieldDao;

	@Autowired
	private DocumentRefToPeopleDao documentRefToPeopleDao;

	@Override
	public Integer getCountAllDocumentsPeople(Integer personId)
			throws ApplicationThrowable {

		try {

			DocumentPeopleJson docPeopleJson = getDocumentsPeople(personId);
			if (docPeopleJson == null) {
				return 0;
			}

			return docPeopleJson.getDocPeople().size();

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Override
	public DocumentPeopleJson getDocumentsPeople(Integer personId)
			throws ApplicationThrowable {

		DocumentPeopleJson docPeopleJson = new DocumentPeopleJson();
		docPeopleJson.setPersonId(personId);
		List<DocumentPeopleFieldsJson> docPeopleList = new ArrayList<DocumentPeopleFieldsJson>();
		try {

			List<MiaDocumentEntity> documentEnts = getDocumentDAO()
					.findDocuments();

			if (documentEnts == null || documentEnts.isEmpty()) {
				return docPeopleJson;
			}

			List<DocumentFieldEntity> documentFields = getDocumentFieldDao()
					.getDocumentFields();

			for (MiaDocumentEntity docEnt : documentEnts) {

				if (docEnt.getFlgLogicalDelete() == null
						|| !docEnt.getFlgLogicalDelete()) {

					DocumentJson docJson = DocumentFactoryJson
							.getDocument(docEnt.getCategory());

					List<DocPeopleFieldJson> fields = docJson
							.getDocPeopleFields(personId, docEnt,
									documentFields);

					if (fields != null && !fields.isEmpty()) {

						DocumentPeopleFieldsJson docPeople = new DocumentPeopleFieldsJson();
						docPeople.toJson(docEnt, fields);
						docPeopleList.add(docPeople);
					}

				}

			}

			List<DocumentRefToPeopleEntity> docRefs = getDocumentRefToPeopleDao()
					.findDocRefToPeopleByPersonId(personId);

			if (docRefs != null && !docRefs.isEmpty()) {
				for (DocumentRefToPeopleEntity docRef : docRefs) {
					MiaDocumentEntity docEnt = getDocumentDAO()
							.findDocumentById(docRef.getDocumentId());
					if (docEnt != null) {
						DocumentPeopleFieldsJson docRefPerson = new DocumentPeopleFieldsJson();
						List<DocPeopleFieldJson> docRefFields = new ArrayList<DocPeopleFieldJson>();
						docRefFields.add(new DocPeopleFieldJson("docRefFields",
								""));
						docRefPerson.toJson(docEnt, docRefFields);
						docPeopleList.add(docRefPerson);
					}
				}
			}

			docPeopleJson.setDocPeople(docPeopleList);

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return docPeopleJson;
	}

	@Override
	public Map<String, Integer> getDocumentsCategoryCountPeople(Integer personId)
			throws ApplicationThrowable {

		DocumentPeopleJson docPeopleJson = getDocumentsPeople(personId);
		if (docPeopleJson == null || docPeopleJson.getDocPeople() == null
				|| docPeopleJson.getDocPeople().isEmpty()) {
			return null;
		}

		Map<String, Integer> resMap = new HashMap<String, Integer>();

		try {

			for (DocumentPeopleFieldsJson json : docPeopleJson.getDocPeople()) {

				Integer count = 0;
				if (!resMap.containsKey(json.getCategory())) {
					resMap.put(json.getCategory(), 0);
				} else {
					count = resMap.get(json.getCategory());
				}

				resMap.put(json.getCategory(), count + 1);

			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resMap;
	}

	@Override
	public Map<String, Map<String, Integer>> getDocumentsCategoryAndFieldCountPeople(
			Integer personId) throws ApplicationThrowable {

		DocumentPeopleJson docPeopleJson = getDocumentsPeople(personId);
		if (docPeopleJson == null || docPeopleJson.getDocPeople() == null
				|| docPeopleJson.getDocPeople().isEmpty()) {
			return null;
		}

		Map<String, Map<String, Integer>> resMap = new HashMap<String, Map<String, Integer>>();

		try {

			for (DocumentPeopleFieldsJson json : docPeopleJson.getDocPeople()) {

				if (!resMap.containsKey(json.getCategory())) {
					resMap.put(json.getCategory(),
							new HashMap<String, Integer>());
				}

				Map<String, Integer> fieldMap = resMap.get(json.getCategory());

				if (json.getFields() != null && !json.getFields().isEmpty()) {

					for (DocPeopleFieldJson fieldJson : json.getFields()) {
						Integer count = 0;
						if (fieldMap.containsKey(fieldJson.getFieldName())) {
							count = fieldMap.get(fieldJson.getFieldName());
						}
						fieldMap.put(fieldJson.getFieldName(), count + 1);
					}

					resMap.put(json.getCategory(), fieldMap);
				}
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resMap;
	}

	public GenericDocumentDao getDocumentDAO() {
		return documentDAO;
	}

	public void setDocumentDAO(GenericDocumentDao documentDAO) {
		this.documentDAO = documentDAO;
	}

	public DocumentFieldDAO getDocumentFieldDao() {
		return documentFieldDao;
	}

	public void setDocumentFieldDao(DocumentFieldDAO documentFieldDao) {
		this.documentFieldDao = documentFieldDao;
	}

	public DocumentRefToPeopleDao getDocumentRefToPeopleDao() {
		return documentRefToPeopleDao;
	}

	public void setDocumentRefToPeopleDao(
			DocumentRefToPeopleDao documentRefToPeopleDao) {
		this.documentRefToPeopleDao = documentRefToPeopleDao;
	}

}