package org.medici.mia.service.people;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.ModifyPersonJson;
import org.medici.mia.common.json.PersonJson;
import org.medici.mia.common.json.biographical.AddHeadquarterJson;
import org.medici.mia.common.json.biographical.BiographicalPeopleJson;
import org.medici.mia.common.json.biographical.FindPersonChildrenJson;
import org.medici.mia.common.json.biographical.FindPersonParentsJson;
import org.medici.mia.common.json.biographical.FindPersonSpousesJson;
import org.medici.mia.common.json.biographical.FindPersonTitlesOccsJson;
import org.medici.mia.common.json.biographical.HeadquarterJson;
import org.medici.mia.common.json.biographical.ModifyPersonChildJson;
import org.medici.mia.common.json.biographical.ModifyPersonParentsJson;
import org.medici.mia.common.json.biographical.ModifyPersonSpouseJson;
import org.medici.mia.common.json.biographical.ModifyPersonTitlesOccsJson;
import org.medici.mia.common.json.biographical.OrganizationJson;
import org.medici.mia.common.json.biographical.PersonPortraitJson;
import org.medici.mia.common.json.biographical.TitleOccJson;
import org.medici.mia.exception.ApplicationThrowable;
import org.springframework.web.multipart.MultipartFile;

public interface BiographicalPeopleService {

	public GenericResponseJson modifyBiographicalPeople(
			BiographicalPeopleJson peopleToModify) throws ApplicationThrowable;

	public GenericResponseJson modifyPersonName(ModifyPersonJson personToModify)
			throws ApplicationThrowable;

	public GenericResponseJson addPersonName(ModifyPersonJson personToModify)
			throws ApplicationThrowable;

	public GenericResponseJson deletePersonName(ModifyPersonJson personToModify)
			throws ApplicationThrowable;

	public BiographicalPeopleJson findBiographicalPeople(Integer peopleId)
			throws ApplicationThrowable;

	public List<Integer> findDocsRefToPeople(Integer peopleId)
			throws ApplicationThrowable;

	public PersonJson findPersonNames(Integer peopleId)
			throws ApplicationThrowable;

	public List<TitleOccJson> findTitlesAndOccs(String titleOcc)
			throws ApplicationThrowable;

	public FindPersonTitlesOccsJson findPersonTitlesAndOccupations(
			Integer peopleId) throws ApplicationThrowable;

	public GenericResponseJson modifyPersonTitleAndOccupation(
			ModifyPersonTitlesOccsJson personTitlesOccsJson)
			throws ApplicationThrowable;

	public GenericResponseJson addPersonTitleAndOccupation(
			ModifyPersonTitlesOccsJson personTitlesOccsJson)
			throws ApplicationThrowable;

	public GenericResponseJson deletePersonTitleAndOccupation(
			ModifyPersonTitlesOccsJson personTitlesOccsJson)
			throws ApplicationThrowable;

	public FindPersonParentsJson findBiographicalPersonParents(Integer peopleId)
			throws ApplicationThrowable;

	public GenericResponseJson modifyBiographicalPersonParents(
			ModifyPersonParentsJson modifyPersonParentsJson)
			throws ApplicationThrowable;

	public GenericResponseJson addBiographicalPersonParents(
			ModifyPersonParentsJson modifyJson) throws ApplicationThrowable;

	public GenericResponseJson deleteBiographicalPersonParents(
			ModifyPersonParentsJson modifyJson) throws ApplicationThrowable;

	public FindPersonChildrenJson findBiographicalPersonChildren(
			Integer peopleId) throws ApplicationThrowable;

	public GenericResponseJson modifyBiographicalPersonChild(
			ModifyPersonChildJson modifyJson) throws ApplicationThrowable;

	public GenericResponseJson addBiographicalPersonChild(
			ModifyPersonChildJson modifyJson) throws ApplicationThrowable;

	public GenericResponseJson deleteBiographicalPersonChild(
			ModifyPersonChildJson modifyJson) throws ApplicationThrowable;

	public FindPersonSpousesJson findBiographicalPersonSpouses(Integer personId)
			throws ApplicationThrowable;

	public GenericResponseJson modifyBiographicalPersonSpouse(
			ModifyPersonSpouseJson modifyJson) throws ApplicationThrowable;

	public GenericResponseJson addBiographicalPersonSpouse(
			ModifyPersonSpouseJson modifyJson) throws ApplicationThrowable;

	public GenericResponseJson deleteBiographicalPersonSpouse(
			ModifyPersonSpouseJson modifyJson) throws ApplicationThrowable;

	public GenericResponseJson deleteBiographicalPerson(Integer personId)
			throws ApplicationThrowable;

	public PersonPortraitJson findPersonPotraitDetails(Integer perssonId)
			throws ApplicationThrowable;

	public GenericResponseJson unDeleteBiographicalPerson(Integer personId)
			throws ApplicationThrowable;

	public GenericResponseJson modifyPersonPortraitDetails(
			PersonPortraitJson modifyPortrait) throws ApplicationThrowable;

	public GenericResponseJson uploadPersonPortrait(MultipartFile multFile,
			Integer personId) throws ApplicationThrowable;

	public OrganizationJson findHeadquarters(Integer personId)
			throws ApplicationThrowable;

	public GenericResponseJson addHeadquarter(AddHeadquarterJson head)
			throws ApplicationThrowable;

	public GenericResponseJson deleteOrganization(HeadquarterJson head)
			throws ApplicationThrowable;

	public List<Integer> findPeopleByPlace(Integer placeId)
			throws PersistenceException;

	public Integer setPeopleDeathDate() throws ApplicationThrowable;

	public List<BiographicalPeopleJson> findBiographicalPeople(
			List<Integer> personIds, Integer firstResult, Integer maxResult,
			String orderColumn, String ascOrDesc) throws ApplicationThrowable;

}