package org.medici.mia.service.people;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.persistence.PersistenceException;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.ModifyPersonJson;
import org.medici.mia.common.json.ParentJson;
import org.medici.mia.common.json.PersonJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.biographical.AddHeadquarterJson;
import org.medici.mia.common.json.biographical.AltNameJson;
import org.medici.mia.common.json.biographical.BiographicalPeopleJson;
import org.medici.mia.common.json.biographical.FindPersonChildrenJson;
import org.medici.mia.common.json.biographical.FindPersonParentsJson;
import org.medici.mia.common.json.biographical.FindPersonSpousesJson;
import org.medici.mia.common.json.biographical.FindPersonTitlesOccsJson;
import org.medici.mia.common.json.biographical.HeadquarterJson;
import org.medici.mia.common.json.biographical.ModifyPersonChildJson;
import org.medici.mia.common.json.biographical.ModifyPersonParentsJson;
import org.medici.mia.common.json.biographical.ModifyPersonSpouseJson;
import org.medici.mia.common.json.biographical.ModifyPersonTitlesOccsJson;
import org.medici.mia.common.json.biographical.OrganizationJson;
import org.medici.mia.common.json.biographical.PersonPortraitJson;
import org.medici.mia.common.json.biographical.PoLinkJson;
import org.medici.mia.common.json.biographical.SpouseJson;
import org.medici.mia.common.json.biographical.SpouseType;
import org.medici.mia.common.json.biographical.TitleOccJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.common.search.AdvancedSearchAbstract.Gender;
import org.medici.mia.common.search.AdvancedSearchAbstract.NameType;
import org.medici.mia.dao.altname.AltNameDAO;
import org.medici.mia.dao.marriage.MarriageDAO;
import org.medici.mia.dao.parent.ParentDAO;
import org.medici.mia.dao.people.BiographicalPeopleDAO;
import org.medici.mia.dao.people.OrganizationDAO;
import org.medici.mia.dao.people.PeopleDAO;
import org.medici.mia.dao.polink.PoLinkDAO;
import org.medici.mia.dao.titleoccslist.TitleOccsListDAO;
import org.medici.mia.domain.AltName;
import org.medici.mia.domain.BiographicalPeople;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.domain.Marriage;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.OrganizationEntity;
import org.medici.mia.domain.People;
import org.medici.mia.domain.PoLink;
import org.medici.mia.domain.TitleOccsList;
import org.medici.mia.domain.User;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.github.underscore.Predicate;
import com.github.underscore.U;

import de.danielbechler.diff.ObjectDifferBuilder;
import de.danielbechler.diff.node.DiffNode;
import de.danielbechler.diff.node.Visit;
import de.danielbechler.diff.selector.BeanPropertyElementSelector;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Service
class BiographicalPeopleServiceImpl implements BiographicalPeopleService {
	
	private final String APPELLATIVE_START_DELIMITER = "(";
	private final String APPELLATIVE_END_DELIMITER = ")";

	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private UserService userService;

	@Autowired
	private BiographicalPeopleDAO biographicalPeopleDao;

	@Autowired
	private AltNameDAO altNameDao;

	@Autowired
	private TitleOccsListDAO titleOccsListDao;

	@Autowired
	private PoLinkDAO poLinkDAO;

	@Autowired
	private ParentDAO parentDAO;

	@Autowired
	private PeopleDAO PeopleDao;

	@Autowired
	private MarriageDAO mariageDao;

	@Autowired
	private BiographicalPeopleDAO biographicalPeopleDAO;

	@Autowired
	private OrganizationDAO organizationDAO;

	@Autowired
	private HistoryLogService historyLogService;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public BiographicalPeopleJson findBiographicalPeople(Integer peopleId)
			throws ApplicationThrowable {
		try {
			BiographicalPeople people = getBiographicalPeopleDao().find(
					peopleId);
			BiographicalPeopleJson ppl = new BiographicalPeopleJson();
			if (people != null) {
				return ppl.toJson(people);
			}

			return null;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public Integer setPeopleDeathDate() throws ApplicationThrowable {
		try {
			return getBiographicalPeopleDao().setPeopleDeathDate();

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<Integer> findDocsRefToPeople(Integer peopleId)
			throws ApplicationThrowable {
		try {
			BiographicalPeople person = getBiographicalPeopleDao().find(
					peopleId);

			if (person != null) {
				List<Integer> docsId = new ArrayList<Integer>();
				for (MiaDocumentEntity doc : person.getMiaDocEntRefToPeople()) {
					docsId.add(doc.getDocumentEntityId());

				}
				return docsId;
			}

			return null;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public FindPersonTitlesOccsJson findPersonTitlesAndOccupations(
			Integer peopleId) throws ApplicationThrowable {
		try {
			BiographicalPeople people = getBiographicalPeopleDao().find(
					peopleId);
			FindPersonTitlesOccsJson ppl = new FindPersonTitlesOccsJson();

			ppl.setPersonId(peopleId);

			if (people != null) {
				Set<PoLink> poLinks = people.getPoLink();
				if (poLinks != null && !poLinks.isEmpty()) {
					List<TitleOccJson> tites = new ArrayList<TitleOccJson>();
					for (PoLink poLink : poLinks) {
						if (poLink.getTitleOccList() != null) {
							TitleOccJson title = new TitleOccJson();
							title.toJson(poLink);

							tites.add(title);

							if (poLink.getPreferredRole() != null
									&& poLink.getPreferredRole()) {
								ppl.setPreferredRole(title.getTitleOccId());
							}
						}

					}

					ppl.setTileOcc(tites);

				}

			}

			return ppl;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson modifyBiographicalPeople(
			BiographicalPeopleJson peopleToModify) throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {

			// Retrieving the Username
			User user = null;
			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
				// user = new User();
				// user.setAccount("testuser1");

			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}
			if (peopleToModify.getSucNum() != null) {
				if (peopleToModify.getSucNum().length() > 6) {
					resp.setMessage("Succ. Number can only be 6 letters long. You've passed "
							+ peopleToModify.getSucNum().length() + " letters.");
					return resp;
				}
			}

			boolean isFirstNameChanged = false;
			boolean isLastNameChanged = false;
			// boolean isPostLastChanged = false;
			boolean isSearchNameChanged = false;

			BiographicalPeople people = getBiographicalPeopleDao().find(
					peopleToModify.getPeopleId());

			final BiographicalPeopleJson oldjson = findBiographicalPeople(peopleToModify
					.getPeopleId());

			if (people == null) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("No People found to be modified.");
				return resp;
			}

			if (peopleToModify.getFirstName() != null
					&& !peopleToModify.getFirstName().equalsIgnoreCase(
							people.getFirst())) {
				// modify table tblAltName
				isFirstNameChanged = true;
				isSearchNameChanged = true;
			}

			if (peopleToModify.getLastName() != null
					&& !peopleToModify.getLastName().equalsIgnoreCase(
							people.getLast())) {
				// modify table tblAltName
				isLastNameChanged = true;
				isSearchNameChanged = true;
			}

			if (!isSearchNameChanged) {

				isSearchNameChanged = ((peopleToModify.getMiddlePrefix() != null && !peopleToModify
						.getMiddlePrefix().equalsIgnoreCase(
								people.getMidPrefix())))
						|| ((peopleToModify.getMiddleName() != null && !peopleToModify
								.getMiddleName().equalsIgnoreCase(
										people.getMiddle())))
						|| ((peopleToModify.getSucNum() != null && !peopleToModify
								.getSucNum().equalsIgnoreCase(
										people.getSucNum())));
			}

			mapEntityForModification(people, peopleToModify);
			people.setLastUpdateBy(user.getAccount());

			getBiographicalPeopleDao().merge(people);

			if (isFirstNameChanged) {
				AltName altName = new AltName();
				People p = new People();
				p.setPersonId(people.getPersonId());
				altName.setPerson(p);
				altName.setAltName(peopleToModify.getFirstName());
				altName.setNameType(NameType.Given.toString());
				getAltNameDao().insertAltName(altName);

			}

			if (isLastNameChanged) {

				AltName altName = new AltName();
				People p = new People();
				p.setPersonId(people.getPersonId());
				altName.setPerson(p);
				altName.setAltName(peopleToModify.getLastName());
				altName.setNameType(NameType.Family.toString());
				getAltNameDao().insertAltName(altName);

			}

			if (isSearchNameChanged) {

				AltName altNameSearchType = getAltNameDao()
						.findAltNameByNametype(people.getPersonId());
				if (altNameSearchType != null) {
					// $lastName, $firstName $sucNum $middlePrefix, $middleName
					StringBuffer b = new StringBuffer();
					if (people.getLast() != null) {
						b.append(people.getLast().toUpperCase() + " ");
					}
					if (people.getFirst() != null) {
						b.append(people.getFirst().toUpperCase() + " ");
					}
					if (people.getSucNum() != null) {
						b.append(people.getSucNum().toUpperCase() + " ");
					}
					if (people.getMiddle() != null) {
						b.append(people.getMiddle().toUpperCase());
					}
					if (people.getMidPrefix() != null) {
						b.append(people.getMidPrefix().toUpperCase() + " ");
					}

					altNameSearchType.setAltName(b.toString());
					getAltNameDao().merge(altNameSearchType);
				}

			}

			final BiographicalPeopleJson newjson = findBiographicalPeople(peopleToModify
					.getPeopleId());

			HashMap<String, Object> changes = new HashMap<String, Object>();
			HashMap<String, Object> category = new HashMap<String, Object>();
			HashMap<String, Object> action = new HashMap<String, Object>();
			final HashMap before = new HashMap();
			final HashMap after = new HashMap();
			DiffNode diff = ObjectDifferBuilder.buildDefault().compare(oldjson,
					newjson);
			diff.visit(new DiffNode.Visitor() {
				public void node(DiffNode node, Visit visit) {
					final Object baseValue = node.canonicalGet(oldjson);
					final Object workingValue = node.canonicalGet(newjson);
					if (node.hasChanges()
							&& !node.getPath().toString().equals("/")) {
						if (node.hasChildren()) {
							visit.dontGoDeeper();
						}
						before.put(node.getPath().getLastElementSelector(),
								baseValue);
						after.put(node.getPath().getLastElementSelector(),
								workingValue);

					}
				}
			});
			changes.put("before", before);
			changes.put("after", after);
			action.put("edit", changes);
			category.put("details", action);
			if (!(after.size() == 1
					&& after.containsKey(new BeanPropertyElementSelector(
							"lastUpdate")) || after.size() == 2
					&& after.containsKey(new BeanPropertyElementSelector(
							"lastUpdate"))
					&& after.containsKey(new BeanPropertyElementSelector(
							"lastUpdateBy")))) {
				historyLogService.registerAction(peopleToModify.getPeopleId(),
						HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.BIO, null, category);
			}

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("People modified.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	public void mapEntityForModification(BiographicalPeople people,
			BiographicalPeopleJson peopleToModify) {

		// Issue 416
		// JSON: mapNameLfStr -> $lastName, $firstName $sucNum $middlePrefix,
		// $middleName
		// ($postLasPrefix $postLast)

		// DB: '$last' , '$FIRST' '$SUCNUM' '$midprefix' '$middle' '$lastprefix'
		// ('$postlastprefix $postlast)
		StringBuilder mapNameLfStr = new StringBuilder();
		if (peopleToModify.getLastName() != null
				&& !peopleToModify.getLastName().equals("")) {
			mapNameLfStr.append(peopleToModify.getLastName());
			mapNameLfStr.append(", ");
		}
		if (peopleToModify.getFirstName() != null
				&& !peopleToModify.getFirstName().equals("")) {
			mapNameLfStr.append(peopleToModify.getFirstName());

		}
		// mapNameLfStr.append(peopleToModify.getLastName());
		// mapNameLfStr.append(", ");
		// mapNameLfStr.append(peopleToModify.getFirstName());

		// $sucNum
		if (peopleToModify.getSucNum() != null
				&& !peopleToModify.getSucNum().equals("")) {
			mapNameLfStr.append(" " + peopleToModify.getSucNum());
		}

		// $middlePrefix
		if (peopleToModify.getMiddlePrefix() != null
				&& !peopleToModify.getMiddlePrefix().equals("")) {
			mapNameLfStr.append(" " + peopleToModify.getMiddlePrefix());
		}

		// $middleName
		if (peopleToModify.getMiddleName() != null
				&& !peopleToModify.getMiddleName().equals("")) {
			mapNameLfStr.append(" " + peopleToModify.getMiddleName());
		}

		// $LasPrefix
		if (peopleToModify.getLastPrefix() != null
				&& !peopleToModify.getLastPrefix().equals("")) {
			mapNameLfStr.append(" " + peopleToModify.getLastPrefix());
		}

		// $postLasPrefix
		if (peopleToModify.getPostLastPrefix() != null
				&& !peopleToModify.getPostLastPrefix().equals("")) {
			mapNameLfStr.append(" " + peopleToModify.getPostLastPrefix());
		}

		// $postLast
		if (peopleToModify.getPostLast() != null
				&& !peopleToModify.getPostLast().equals("")) {
			mapNameLfStr.append(String.format(" %s%s%s", APPELLATIVE_START_DELIMITER,
					peopleToModify.getPostLast(), APPELLATIVE_END_DELIMITER));
		}

		if (!mapNameLfStr.toString().isEmpty()) {
			// Issue 416
			people.setMapNameLf(mapNameLfStr.toString());
		} else {
			people.setMapNameLf(peopleToModify.getMapNameLf());
		}

		people.setFirst(peopleToModify.getFirstName());
		people.setLast(peopleToModify.getLastName());
		people.setGender(peopleToModify.getGender());
		people.setLastPrefix(peopleToModify.getLastPrefix());
		people.setSucNum(peopleToModify.getSucNum());
		// Biographical fields
		people.setBornApprox(peopleToModify.getBornApprox());
		people.setBornDateBc(peopleToModify.getBornDateBc());

		people.setBornDay(peopleToModify.getBornDay());
		people.setBornYear(peopleToModify.getBornYear());
		people.setBornMonthNum(peopleToModify.getBornMonth());

		people.setBornPlaceId(peopleToModify.getBornPlaceId());
		people.setBornPlaceUnsure(peopleToModify.getBornPlaceUnsure());
		people.setDeathApprox(peopleToModify.getDeathApprox());
		people.setDeathDateBc(peopleToModify.getDeathDateBc());
		people.setDeathDay(peopleToModify.getDeathDay());
		people.setDeathYear(peopleToModify.getDeathYear());

		people.setActiveEnd(peopleToModify.getActiveEnd());
		people.setActiveStart(peopleToModify.getActiveStart());

		people.setDeathMonthNum(peopleToModify.getDeathMonth());
		people.setDeathPlaceUnsure(peopleToModify.getDeathPlaceUnsure());
		people.setDeathPlaceId(peopleToModify.getDeathPlaceId());

		people.setLastUpdate(new Date());

		people.setMiddle(peopleToModify.getMiddleName());
		people.setMidPrefix(peopleToModify.getMiddlePrefix());
		people.setPortrait(peopleToModify.getPortrait());
		people.setPortraitAuthor(peopleToModify.getPortraitAuthor());
		people.setPortraitSubject(peopleToModify.getPortraitSubject());
		people.setPortraitImageName(peopleToModify.getPortraitImageName());
		people.setPostLast(peopleToModify.getPostLast());
		people.setPostLastPrefix(peopleToModify.getPostLastPrefix());
		people.setStatus(peopleToModify.getStatus());

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public PersonJson findPersonNames(Integer peopleId)
			throws ApplicationThrowable {
		try {
			BiographicalPeople people = getBiographicalPeopleDao().find(
					peopleId);

			PersonJson personJson = new PersonJson();
			personJson.setPersonId(people.getPersonId());

			if (people != null && people.getAltName() != null
					&& !people.getAltName().isEmpty()) {
				List<AltNameJson> altNames = new ArrayList<AltNameJson>();
				for (AltName altname : people.getAltName()) {
					AltNameJson altnameJson = new AltNameJson();
					altnameJson.toJson(altname);
					altNames.add(altnameJson);
				}
				personJson.setPersonNames(altNames);
			}

			return personJson;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson addPersonTitleAndOccupation(
			ModifyPersonTitlesOccsJson modifyTitle) throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {

			User user = null;

			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}

			TitleOccsList title = getTitleOccsListDao().find(
					modifyTitle.getNewTitleAndOccupation());
			Integer poLinkId = getPoLinkDAO().addPersonTittleOcc(
					getPoLink(modifyTitle));
			if (poLinkId == 0) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("The title for this person exist in DB and Not added.");
				return resp;
			}

			getBiographicalPeopleDao().setPeopleModInf(
					modifyTitle.getPersonId(), user.getAccount());

			HashMap changes = new HashMap();
			HashMap category = new HashMap();
			HashMap action = new HashMap();
			changes.put("prefferedRole", !modifyTitle.getPreferredRole()
					.equals(0));
			changes.put("titleOcc", title.getTitleOcc());
			changes.put("poLink", modifyTitle.getPoLink());
			action.put("add", changes);
			category.put("titleOccupation", action);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("People modified.");
			historyLogService.registerAction(modifyTitle.getPersonId(),
					HistoryLogEntity.UserAction.EDIT,
					HistoryLogEntity.RecordType.BIO, null, category);

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("The new person title and occupation  added in DB.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson modifyPersonTitleAndOccupation(
			final ModifyPersonTitlesOccsJson modifyTitle)
			throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {

			// deletd the old title and ocuupation
			User user = null;

			BiographicalPeople oldPerson = getBiographicalPeopleDAO()
					.findPeopleById(modifyTitle.getPersonId());

			String oldPreferred = null;
			for (PoLink po : oldPerson.getPoLink()) {
				if (po.getPreferredRole()) {
					oldPreferred = po.getTitleOccList().getTitleOcc();
				}
			}

			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}
			FindPersonTitlesOccsJson oldtitle = findPersonTitlesAndOccupations(modifyTitle
					.getPersonId());
			final TitleOccJson oldjson = U.find(oldtitle.getTileOcc(),
					new Predicate<TitleOccJson>() {
						@Override
						public boolean test(TitleOccJson t) {
							return t.getTitleOccId().equals(
									modifyTitle.getOldTitleAndOccupation());
						}
					}).get();

			Integer deletedNum = getPoLinkDAO().modifyPersonTittleOcc(
					modifyTitle);
			if (deletedNum.equals(-1)) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("The old Title and Occ for this person doesn't exist in DB and NOT modified.");
				return resp;
			}

			if (deletedNum.equals(0)) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("The new Title and Occ for this person exist in DB and NOT modified.");
				return resp;
			}

			getBiographicalPeopleDao().setPeopleModInf(
					modifyTitle.getPersonId(), user.getAccount());

			final Integer oldPoId = modifyTitle.getOldTitleAndOccupation(), newPoId = modifyTitle
					.getNewTitleAndOccupation();
			BiographicalPeople newPerson = getBiographicalPeopleDAO()
					.findPeopleById(modifyTitle.getPersonId());

			String newPreferred = null;
			for (PoLink po : newPerson.getPoLink()) {
				if (po.getPreferredRole()) {
					newPreferred = po.getTitleOccList().getTitleOcc();
				}
			}

			FindPersonTitlesOccsJson newtitle = findPersonTitlesAndOccupations(modifyTitle
					.getPersonId());
			final TitleOccJson newjson = U.find(newtitle.getTileOcc(),
					new Predicate<TitleOccJson>() {
						@Override
						public boolean test(TitleOccJson t) {
							return t.getTitleOccId().equals(
									modifyTitle.getNewTitleAndOccupation());
						}
					}).get();

			HashMap changes = new HashMap();
			HashMap category = new HashMap();
			HashMap action = new HashMap();
			final HashMap before = new HashMap();
			final HashMap after = new HashMap();
			DiffNode diff = ObjectDifferBuilder.buildDefault().compare(oldjson,
					newjson);
			diff.visit(new DiffNode.Visitor() {
				public void node(DiffNode node, Visit visit) {
					final Object baseValue = node.canonicalGet(oldjson);
					final Object workingValue = node.canonicalGet(newjson);
					if (node.hasChanges()
							&& !node.getPath().toString().equals("/")) {
						if (!node.hasChildren()) {
							before.put(node.getPath().getLastElementSelector(),
									baseValue);
							after.put(node.getPath().getLastElementSelector(),
									workingValue);
						}
					}
				}
			});
			if (oldPreferred != null && newPreferred != null) {
				if (!oldPreferred.equals(newPreferred)) {
					before.put("preferred", oldPreferred);
					after.put("preferred", newPreferred);
				}
			}
			changes.put("before", before);
			changes.put("after", after);
			action.put("edit", changes);
			category.put("titleOccupation", action);

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("People modified.");
			if (!before.isEmpty()) {
				historyLogService.registerAction(modifyTitle.getPersonId(),
						HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.BIO, null, category);
			}

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("The Person title and occupation modified in DB.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	private PoLink getPoLink(ModifyPersonTitlesOccsJson modifyTitle) {
		// add new Title and occupation
		PoLink poLink = new PoLink();
		People person = new People();
		person.setPersonId(modifyTitle.getPersonId());
		poLink.setPerson(person);

		TitleOccsList titleOccList = new TitleOccsList();
		titleOccList.setTitleOccId(modifyTitle.getNewTitleAndOccupation());
		poLink.setTitleOcc(titleOccList);
		if (modifyTitle.getPreferredRole() != null
				&& modifyTitle.getPreferredRole() == 1) {
			poLink.setPreferredRole(true);
		} else {
			poLink.setPreferredRole(false);
		}

		PoLinkJson poLinkJson = modifyTitle.getPoLink();
		if (poLinkJson == null) {
			poLink.setEndApprox(false);
			poLink.setEndUns(false);
			poLink.setStartApprox(false);
			poLink.setStartUns(false);
			return poLink;
		}

		if (poLinkJson.getEndApprox() == null
				|| poLinkJson.getEndApprox().equals(0)) {
			poLink.setEndApprox(false);
		} else {
			poLink.setEndApprox(true);
		}

		if (poLinkJson.getEndUnsure() == null
				|| poLinkJson.getEndUnsure().equals(0)) {
			poLink.setEndUns(false);
		} else {
			poLink.setEndUns(true);
		}

		if (poLinkJson.getStartApprox() == null
				|| poLinkJson.getStartApprox().equals(0)) {
			poLink.setStartApprox(false);
		} else {
			poLink.setStartApprox(true);
		}

		if (poLinkJson.getStartUnsure() == null
				|| poLinkJson.getStartUnsure().equals(0)) {
			poLink.setStartUns(false);
		} else {
			poLink.setStartUns(true);
		}

		poLink.setStartDay(poLinkJson.getStartDay());
		poLink.setStartMonth(poLinkJson.getStartMonth());
		poLink.setStartYear(poLinkJson.getStartYear());
		poLink.setEndDay(poLinkJson.getEndDay());
		poLink.setEndMonth(poLinkJson.getEndMonth());
		poLink.setEndYear(poLinkJson.getEndYear());
		poLink.setDateCreated(new Date());

		return poLink;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson deletePersonTitleAndOccupation(
			ModifyPersonTitlesOccsJson modifyTitle) throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {

			User user = null;
			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}
			TitleOccsList title = getTitleOccsListDao().find(
					modifyTitle.getTitleAndOccupationToBeDeleted());

			Integer deletedNum = getPoLinkDAO().deletePersonTittleOcc(
					modifyTitle.getPersonId(),
					modifyTitle.getTitleAndOccupationToBeDeleted());
			if (deletedNum == 0) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("The title for this person doesnt exist in DB and Not deleted.");
				return resp;
			}

			HashMap changes = new HashMap();
			HashMap category = new HashMap();
			HashMap action = new HashMap();

			changes.put("titleOcc", title.getTitleOcc());
			changes.put("poLink", modifyTitle.getPoLink());
			action.put("delete", changes);
			category.put("titleOccupation", action);
			historyLogService.registerAction(modifyTitle.getPersonId(),
					HistoryLogEntity.UserAction.EDIT,
					HistoryLogEntity.RecordType.BIO, null, category);

			getBiographicalPeopleDao().setPeopleModInf(
					modifyTitle.getPersonId(), user.getAccount());

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("The person title and occupation deleted.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	public GenericResponseJson modifyPersonName(ModifyPersonJson personToModify)
			throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {
			User user = null;
			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}
			String message = getAltNameDao().modifyPersonName(personToModify);
			getBiographicalPeopleDao().setPeopleModInf(
					personToModify.getPersonId(), user.getAccount());

			resp.setMessage(message);
			resp.setStatus(StatusType.ok.toString());

			HashMap before = new HashMap();
			HashMap after = new HashMap();

			if (!personToModify.getNewPersonName().getAltName()
					.equals(personToModify.getOldPersonName().getAltName())) {
				after.put("altName", personToModify.getNewPersonName()
						.getAltName());
				before.put("altName", personToModify.getOldPersonName()
						.getAltName());
			}
			if (!personToModify.getNewPersonName().getNameType()
					.equals(personToModify.getOldPersonName().getNameType())) {
				after.put("nameType", personToModify.getNewPersonName()
						.getNameType());
				before.put("nameType", personToModify.getOldPersonName()
						.getNameType());
			}

			HashMap changes = new HashMap();
			HashMap category = new HashMap();
			HashMap action = new HashMap();
			changes.put("before", before);
			changes.put("after", after);
			action.put("edit", changes);
			category.put("altNames", action);

			if (!before.isEmpty()) {
				historyLogService.registerAction(personToModify.getPersonId(),
						HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.BIO, null, category);
			}
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("People modified.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	public GenericResponseJson addPersonName(ModifyPersonJson personToModify)
			throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Unknown errore.");
		try {

			User user = null;

			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}

			AltName altName = new AltName();
			altName.setAltName(personToModify.getNewPersonName().getAltName());
			altName.setNameType(personToModify.getNewPersonName().getNameType());
			People people = new People();
			people.setPersonId(personToModify.getPersonId());
			altName.setPerson(people);
			if (getAltNameDao().insertAltName(altName) > 0) {
				HashMap change = new HashMap();
				change.put("nameType", personToModify.getNewPersonName()
						.getNameType());
				change.put("altName", personToModify.getNewPersonName()
						.getAltName());
				HashMap changes = new HashMap();
				HashMap category = new HashMap();
				HashMap action = new HashMap();
				action.put("add", change);
				category.put("altNames", action);
				historyLogService.registerAction(personToModify.getPersonId(),
						HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.BIO, null, category);
				getBiographicalPeopleDao().setPeopleModInf(
						personToModify.getPersonId(), user.getAccount());
				resp.setMessage("Person Name added.");
				resp.setStatus(StatusType.ok.toString());
			} else {
				resp.setMessage("Person Name of this personId exist in DB. Person Name not added.");
				resp.setStatus(StatusType.w.toString());
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	public GenericResponseJson deletePersonName(
			final ModifyPersonJson personToModify) throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Unknown errore.");
		try {

			User user = null;

			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}

			if (getAltNameDao().deleteAltName(
					personToModify.getAltNameToBeDeleted(),
					personToModify.getPersonId()) > 0) {
				getBiographicalPeopleDao().setPeopleModInf(
						personToModify.getPersonId(), user.getAccount());

				AltNameJson alt = U.find(
						findPersonNames(personToModify.getPersonId())
								.getPersonNames(),
						new Predicate<AltNameJson>() {
							@Override
							public boolean test(AltNameJson altNameJson) {
								return altNameJson.getAltNameId().equals(
										personToModify.getAltNameToBeDeleted());
							}
						}).get();
				HashMap<String, String> change = new HashMap<String, String>();
				change.put("nameType", alt.getNameType());
				change.put("altName", alt.getAltName());

				HashMap<String, HashMap<String, HashMap<String, String>>> category = new HashMap<String, HashMap<String, HashMap<String, String>>>();
				HashMap<String, HashMap<String, String>> action = new HashMap<String, HashMap<String, String>>();
				action.put("delete", change);
				category.put("altNames", action);

				historyLogService.registerAction(personToModify.getPersonId(),
						HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.BIO, null, category);

				resp.setMessage("Person Name Deleted.");
				resp.setStatus(StatusType.ok.toString());
			} else {
				resp.setMessage("No person name of this personId found in DB. Person Name not deleted.");
				resp.setStatus(StatusType.w.toString());
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	// Title and Occupation

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<TitleOccJson> findTitlesAndOccs(String titleOccStr)
			throws ApplicationThrowable {
		try {

			List<TitleOccsList> titleOccs = getTitleOccsListDao()
					.findTitlesAndOccs(titleOccStr);
			if (titleOccs != null && !titleOccs.isEmpty()) {
				List<TitleOccJson> titleOccsJson = new ArrayList<TitleOccJson>();
				for (TitleOccsList titleOcc : titleOccs) {
					TitleOccJson titleOccJson = new TitleOccJson();
					titleOccJson.toJson(titleOcc);
					titleOccsJson.add(titleOccJson);
				}

				return titleOccsJson;
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return null;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public FindPersonParentsJson findBiographicalPersonParents(Integer peopleId)
			throws ApplicationThrowable {
		try {

			List<BiographicalPeople> peoples = getBiographicalPeopleDao()
					.findPersonParents(peopleId);

			if (peoples != null && !peoples.isEmpty()) {
				FindPersonParentsJson findParentJson = new FindPersonParentsJson();

				List<ParentJson> parenst = new ArrayList<ParentJson>();
				for (BiographicalPeople parent : peoples) {
					ParentJson par = new ParentJson();

					par.setParentId(parent.getPersonId());
					par.setGender(parent.getGender().toString());
					parenst.add(par);

				}
				findParentJson.setParents(parenst);
				findParentJson.setPersonId(peopleId);
				return findParentJson;
			}

			return null;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson modifyBiographicalPersonParents(
			ModifyPersonParentsJson modifyJson) throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {
			User user = null;
			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}
			Integer result = getParentDAO().modifyPersonParents(modifyJson);

			if (result > 0) {
				HashMap<String, String> before = new HashMap<String, String>();
				HashMap<String, String> after = new HashMap<String, String>();
				String oldParent = null, newParent = null, parentType = "mother";
				if (!modifyJson.getNewMotherId().equals(
						modifyJson.getOldMotherId())) {
					oldParent = findBiographicalPeople(
							modifyJson.getOldMotherId()).getMapNameLf();
					newParent = findBiographicalPeople(
							modifyJson.getNewMotherId()).getMapNameLf();
				} else if (!modifyJson.getNewFatherId().equals(
						modifyJson.getOldFatherId())) {
					oldParent = findBiographicalPeople(
							modifyJson.getOldFatherId()).getMapNameLf();
					newParent = findBiographicalPeople(
							modifyJson.getNewFatherId()).getMapNameLf();
					parentType = "father";
				}
				before.put(parentType, oldParent);
				after.put(parentType, newParent);
				HashMap<String, HashMap<String, String>> changes = new HashMap<String, HashMap<String, String>>();
				changes.put("before", before);
				changes.put("after", after);
				HashMap<String, HashMap<String, HashMap<String, HashMap<String, String>>>> category = new HashMap<String, HashMap<String, HashMap<String, HashMap<String, String>>>>();
				HashMap<String, HashMap<String, HashMap<String, String>>> action = new HashMap<String, HashMap<String, HashMap<String, String>>>();
				action.put("edit", changes);
				category.put("parents", action);
				if (!before.isEmpty()) {
					historyLogService.registerAction(modifyJson.getPersonId(),
							HistoryLogEntity.UserAction.EDIT,
							HistoryLogEntity.RecordType.BIO, null, category);
				}

				getBiographicalPeopleDao().setPeopleModInf(
						modifyJson.getPersonId(), user.getAccount());
				resp.setMessage(result + " parents is modified");
				resp.setStatus(StatusType.ok.toString());
			} else {
				resp.setMessage("No parents modified");
				resp.setStatus(StatusType.w.toString());
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson addBiographicalPersonParents(
			ModifyPersonParentsJson modifyJson) throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {
			User user = null;
			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}
			Integer result = getParentDAO().addPersonParents(modifyJson);

			if (result > 0) {
				getBiographicalPeopleDao().setPeopleModInf(
						modifyJson.getPersonId(), user.getAccount());
				String newParent = null, parentType = "mother";
				if (modifyJson.getNewFatherId() != null) {
					newParent = findBiographicalPeople(
							modifyJson.getNewFatherId()).getMapNameLf();
					parentType = "father";
				} else if (modifyJson.getNewMotherId() != null) {
					newParent = findBiographicalPeople(
							modifyJson.getNewMotherId()).getMapNameLf();
				}
				HashMap changes = new HashMap();
				changes.put(parentType, newParent);
				HashMap category = new HashMap();
				HashMap action = new HashMap();
				action.put("add", changes);
				category.put("parents", action);

				historyLogService.registerAction(modifyJson.getPersonId(),
						HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.BIO, null, category);

				resp.setMessage("New Parent is added");
				resp.setStatus(StatusType.ok.toString());
			} else {
				resp.setMessage("New parent is not added. New parent may exist in DB or both newFatherId and newMotherId are empties.");
				resp.setStatus(StatusType.w.toString());
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson deleteBiographicalPersonParents(
			ModifyPersonParentsJson modifyJson) throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Unknown errore.");
		try {
			Integer result = getParentDAO().deletePersonParents(modifyJson);

			User user = null;

			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}

			if (result > 0) {

				String deletedParent = findBiographicalPeople(
						modifyJson.getParentToBeDeleted()).getMapNameLf();
				String gender = findBiographicalPeople(
						modifyJson.getParentToBeDeleted()).getGender()
						.toString();
				String parentType = "parent";
				if (gender != null) {
					if (gender.equals("M")) {
						parentType = "father";
					} else if (gender.equals("F")) {
						parentType = "mother";
					}
				}
				HashMap changes = new HashMap();
				changes.put(parentType, deletedParent);
				HashMap category = new HashMap();
				HashMap action = new HashMap();
				action.put("delete", changes);
				category.put("parents", action);

				historyLogService.registerAction(modifyJson.getPersonId(),
						HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.BIO, null, category);

				getBiographicalPeopleDao().setPeopleModInf(
						modifyJson.getPersonId(), user.getAccount());
				resp.setMessage("Parent Deleted");
				resp.setStatus(StatusType.ok.toString());
			} else {
				resp.setMessage("No parent found in DB.");
				resp.setStatus(StatusType.w.toString());
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public FindPersonChildrenJson findBiographicalPersonChildren(
			Integer personId) throws ApplicationThrowable {

		if (personId == null || personId.equals("")) {
			return null;
		}

		try {
			FindPersonChildrenJson personChildren = new FindPersonChildrenJson();
			personChildren.setPersonId(personId);
			personChildren.setChildren(getParentDAO().findPersonChildren(
					personId));
			return personChildren;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson addBiographicalPersonChild(
			ModifyPersonChildJson modifyJson) throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Unknown errore.");
		try {
			Integer result = getParentDAO().addPersonChild(modifyJson);

			User user = null;
			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}

			if (result > 0) {

				String name = findBiographicalPeople(modifyJson.getNewChildId())
						.getMapNameLf();
				HashMap changes = new HashMap();
				changes.put("name", name);
				HashMap category = new HashMap();
				HashMap action = new HashMap();
				action.put("add", changes);
				category.put("children", action);

				historyLogService.registerAction(modifyJson.getPersonId(),
						HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.BIO, null, category);

				getBiographicalPeopleDao().setPeopleModInf(
						modifyJson.getPersonId(), user.getAccount());
				resp.setMessage("Child Added");
				resp.setStatus(StatusType.ok.toString());
			} else {
				resp.setMessage("The Person had this child. Child not added.");
				resp.setStatus(StatusType.w.toString());
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson deleteBiographicalPersonChild(
			ModifyPersonChildJson modifyJson) throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Unknown errore.");
		try {
			Integer result = getParentDAO().deletePersonChild(modifyJson);

			User user = null;
			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}

			if (result > 0) {

				String name = findBiographicalPeople(
						modifyJson.getChildToBeDeleted()).getMapNameLf();
				HashMap changes = new HashMap();
				changes.put("name", name);
				HashMap category = new HashMap();
				HashMap action = new HashMap();
				action.put("delete", changes);
				category.put("children", action);

				historyLogService.registerAction(modifyJson.getPersonId(),
						HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.BIO, null, category);

				getBiographicalPeopleDao().setPeopleModInf(
						modifyJson.getPersonId(), user.getAccount());
				resp.setMessage(result + " Child found in DB and Deleted.");
				resp.setStatus(StatusType.ok.toString());
			} else {
				resp.setMessage("Child didn't exist in DB.");
				resp.setStatus(StatusType.w.toString());
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson modifyBiographicalPersonChild(
			ModifyPersonChildJson modifyJson) throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {
			Integer result = getParentDAO().modifyPersonChild(modifyJson);
			User user = null;
			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}

			if (result > 0) {

				String oldChild = findBiographicalPeople(
						modifyJson.getOldChildId()).getMapNameLf();
				String newChild = findBiographicalPeople(
						modifyJson.getNewChildId()).getMapNameLf();

				HashMap before = new HashMap();
				HashMap after = new HashMap();
				if (newChild != null && oldChild != null) {
					if (!newChild.equals(oldChild)) {
						before.put("name", oldChild);
						after.put("name", newChild);
					}
				} else {
					before.put("name", oldChild);
					after.put("name", newChild);
				}
				HashMap changes = new HashMap();
				changes.put("before", before);
				changes.put("after", after);
				HashMap category = new HashMap();
				HashMap action = new HashMap();
				action.put("edit", changes);
				category.put("children", action);

				if (before.isEmpty()) {
					historyLogService.registerAction(modifyJson.getPersonId(),
							HistoryLogEntity.UserAction.EDIT,
							HistoryLogEntity.RecordType.BIO, null, category);
				}

				getBiographicalPeopleDao().setPeopleModInf(
						modifyJson.getPersonId(), user.getAccount());
				resp.setMessage(result + " Child is modified");
				resp.setStatus(StatusType.ok.toString());
			} else {
				resp.setMessage("Child not modified in DB");
				resp.setStatus(StatusType.w.toString());
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	// Person Spouses
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public FindPersonSpousesJson findBiographicalPersonSpouses(Integer personId)
			throws ApplicationThrowable {

		if (personId == null || personId.equals("")) {
			return null;
		}
		try {
			People person = getPeopleDao().find(personId);
			if (person == null) {
				return null;
			}

			List<Marriage> spouses = getMariageDao().findMarriagesPerson(
					personId, person.getGender());

			if (spouses == null || spouses.isEmpty()) {
				return null;
			}
			FindPersonSpousesJson personSpouses = new FindPersonSpousesJson();
			List<SpouseJson> spousesJson = new ArrayList<SpouseJson>();
			for (Marriage marriage : spouses) {
				SpouseJson spouseJson = new SpouseJson();
				if (marriage.getHusband() != null
						&& personId.equals(marriage.getHusband().getPersonId())) {
					spouseJson.setPersonId(marriage.getWife().getPersonId());
					spouseJson.setSpouseType(SpouseType.W.toString());
				} else {
					spouseJson.setPersonId(marriage.getHusband().getPersonId());
					spouseJson.setSpouseType(SpouseType.H.toString());
				}

				spousesJson.add(spouseJson);

			}

			personSpouses.setPersonId(personId);
			personSpouses.setSpouses(spousesJson);

			return personSpouses;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson modifyBiographicalPersonSpouse(
			ModifyPersonSpouseJson modifyJson) throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {
			Integer result = getMariageDao().modifyMarriagePerson(modifyJson);

			User user = null;
			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}

			if (result > 0) {

				String oldSpouse = findBiographicalPeople(
						modifyJson.getOldSpouse().getPersonId()).getMapNameLf();
				String newSpouse = findBiographicalPeople(
						modifyJson.getNewSpouse().getPersonId()).getMapNameLf();

				HashMap before = new HashMap();
				HashMap after = new HashMap();
				if (oldSpouse != null && oldSpouse != null) {
					if (!oldSpouse.equals(newSpouse)) {
						before.put("name", oldSpouse);
						after.put("name", newSpouse);
					}
				} else {
					before.put("name", oldSpouse);
					after.put("name", newSpouse);
				}
				HashMap changes = new HashMap();
				changes.put("before", before);
				changes.put("after", after);
				HashMap category = new HashMap();
				HashMap action = new HashMap();
				action.put("edit", changes);
				category.put("spouses", action);

				historyLogService.registerAction(modifyJson.getPersonId(),
						HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.BIO, null, category);

				getBiographicalPeopleDao().setPeopleModInf(
						modifyJson.getPersonId(), user.getAccount());
				resp.setMessage(result + " Spouse is modified");
				resp.setStatus(StatusType.ok.toString());
			} else {
				resp.setMessage("Person Spouse not modified in DB.");
				resp.setStatus(StatusType.w.toString());
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson addBiographicalPersonSpouse(
			ModifyPersonSpouseJson modifyJson) throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Unknown errore.");
		try {

			Integer result = getMariageDao().addMarriagePerson(modifyJson);

			User user = null;
			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}

			if (result > 0) {

				String name = findBiographicalPeople(
						modifyJson.getNewSpouse().getPersonId()).getMapNameLf();
				HashMap changes = new HashMap();
				changes.put("name", name);
				HashMap category = new HashMap();
				HashMap action = new HashMap();
				action.put("add", changes);
				category.put("spouses", action);

				historyLogService.registerAction(modifyJson.getPersonId(),
						HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.BIO, null, category);

				getBiographicalPeopleDao().setPeopleModInf(
						modifyJson.getPersonId(), user.getAccount());
				resp.setMessage("Spouse Added");
				resp.setStatus(StatusType.ok.toString());
			} else {
				resp.setMessage("The Person had this spouse. Spouse not added.");
				resp.setStatus(StatusType.w.toString());
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson deleteBiographicalPersonSpouse(
			ModifyPersonSpouseJson modifyJson) throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Unknown errore.");
		try {
			Integer result = getMariageDao().deleteMarriagePerson(modifyJson);
			User user = null;
			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}

			if (result > 0) {

				String name = findBiographicalPeople(
						modifyJson.getSpouseToBeDeleted()).getMapNameLf();
				HashMap changes = new HashMap();
				changes.put("name", name);
				HashMap category = new HashMap();
				HashMap action = new HashMap();
				action.put("delete", changes);
				category.put("spouses", action);

				historyLogService.registerAction(modifyJson.getPersonId(),
						HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.BIO, null, category);

				getBiographicalPeopleDao().setPeopleModInf(
						modifyJson.getPersonId(), user.getAccount());
				resp.setMessage(result + " Spouse found in DB and Deleted.");
				resp.setStatus(StatusType.ok.toString());
			} else {
				resp.setMessage("Spouse didn't exist in DB.");
				resp.setStatus(StatusType.w.toString());
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson deleteBiographicalPerson(Integer personId)
			throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Uncaught error");
		if (personId == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("personId is null.");
			return resp;
		}

		try {

			// Get the username logged in
			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());
			String owner = user.getAccount();
			if (owner == null) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("Person not deleted. Owner account from session in null.");
				return resp;
			}
			// Controll if user is administrator or staf
			boolean isAutorisedUser = getUserService()
					.isAccountAutorizedForDelete(owner);
			Integer deletePerson = getBiographicalPeopleDAO().deletePerson(
					personId, owner, isAutorisedUser);
			if (deletePerson.equals(0)) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("No Person found or person was deleted befor.");
				return resp;

			}
			if (deletePerson.equals(-1)) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("Person not deleted. The user is not Autorized to delete this person.");
				return resp;
			}

			historyLogService.registerAction(personId,
					HistoryLogEntity.UserAction.DELETE,
					HistoryLogEntity.RecordType.BIO, null, null);

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Person deleted.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson unDeleteBiographicalPerson(Integer personId)
			throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Uncaught error");
		if (personId == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("personId is null.");
			return resp;
		}

		try {

			// Get the username logged in
			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());
			String owner = user.getAccount();
			if (owner == null) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("Person not activated. Owner account from session in null.");
				return resp;
			}
			// Controll if user is administrator or staf
			boolean isAutorisedUser = getUserService()
					.isAccountAutorizedForDelete(owner);
			Integer deletePerson = getBiographicalPeopleDAO().undeletePerson(
					personId, owner, isAutorisedUser);
			if (deletePerson.equals(0)) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("No Person found or person was activated befor.");
				return resp;

			}
			if (deletePerson.equals(-1)) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("Person not activated. The user is not Autorized to activate this person.");
				return resp;
			}

			historyLogService.registerAction(personId,
					HistoryLogEntity.UserAction.DELETE,
					HistoryLogEntity.RecordType.BIO, null, null);

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Person activated.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public PersonPortraitJson findPersonPotraitDetails(Integer perssonId)
			throws ApplicationThrowable {
		try {
			BiographicalPeople entity = getBiographicalPeopleDao().find(
					perssonId);
			PersonPortraitJson personPortraitJson = new PersonPortraitJson();
			personPortraitJson.toJson(entity);

			return personPortraitJson;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson modifyPersonPortraitDetails(
			PersonPortraitJson modifyPortrait) throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Uncaught error");
		try {
			BiographicalPeople entity = getBiographicalPeopleDao().find(
					modifyPortrait.getPersonId());

			if (entity == null) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("No Person found.");
			}

			User user = null;

			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}

			HashMap before = new HashMap();
			HashMap after = new HashMap();
			if (entity.getPortraitSubject() != null
					&& modifyPortrait.getPortraitSubject() != null) {
				if (!entity.getPortraitSubject().equals(
						modifyPortrait.getPortraitSubject())) {
					before.put("portraitSubject", entity.getPortraitSubject());
					after.put("portraitSubject",
							modifyPortrait.getPortraitSubject());
				}
			} else {
				before.put("portraitSubject", entity.getPortraitSubject());
				after.put("portraitSubject",
						modifyPortrait.getPortraitSubject());
			}
			if (entity.getPortraitAuthor() != null
					&& modifyPortrait.getPortraitAuthor() != null) {
				if (!entity.getPortraitSubject().equals(
						modifyPortrait.getPortraitSubject())) {
					before.put("portraitAuthor", entity.getPortraitAuthor());
					after.put("portraitAuthor",
							modifyPortrait.getPortraitAuthor());
				}
			} else {
				before.put("portraitSubject", entity.getPortraitSubject());
				after.put("portraitSubject",
						modifyPortrait.getPortraitSubject());
			}
			if (!before.isEmpty()) {
				before.put("lastUpdateBy", entity.getLastUpdateBy());
				after.put("lastUpdateBy", user.getAccount());
				HashMap changes = new HashMap();
				changes.put("before", before);
				changes.put("after", after);
				HashMap category = new HashMap();
				HashMap action = new HashMap();
				action.put("edit", changes);
				category.put("portrait", action);

				historyLogService.registerAction(modifyPortrait.getPersonId(),
						HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.BIO, null, category);
			}

			entity.setPortraitSubject(modifyPortrait.getPortraitSubject());
			entity.setPortraitAuthor(modifyPortrait.getPortraitAuthor());
			entity.setLastUpdate(new Date());
			entity.setLastUpdateBy(user.getAccount());
			getBiographicalPeopleDao().merge(entity);

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Person modified.");

			return resp;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	public GenericResponseJson uploadPersonPortrait(MultipartFile multFile,
			Integer personId) throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Uncaught error");

		try {

			User user = null;

			BiographicalPeople person = getBiographicalPeopleDAO().find(
					personId);
			if (person == null) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Person not Found");
				return resp;
			}

			if (multFile != null && !multFile.isEmpty()) {

				try {
					user = getUserService().findUser(
							((UserDetails) SecurityContextHolder.getContext()
									.getAuthentication().getPrincipal())
									.getUsername());
				} catch (ApplicationThrowable applicationThrowable) {
					resp.setStatus(StatusType.ko.toString());
					resp.setMessage("Error retrieving the User logged in");
					return resp;
				}

				String imageDir = "";
				String fileName = "";
				String fileNameToWrite = "";
				String fileExtension = "";

				imageDir = ApplicationPropertyManager
						.getApplicationProperty("portrait.person.path");

				fileNameToWrite = imageDir
						+ org.medici.mia.common.util.FileUtils.OS_SLASH
						+ personId + user.getAccount()
						+ multFile.getOriginalFilename();

				fileExtension = FilenameUtils.getExtension(multFile
						.getOriginalFilename());

				if (fileExtension == null || fileExtension.isEmpty()) {
					fileExtension = "jpg";
				}

				// Filename in database
				fileName = personId + user.getAccount()
						+ multFile.getOriginalFilename() + "." + fileExtension;

				// Filename on disk
				fileNameToWrite = imageDir
						+ org.medici.mia.common.util.FileUtils.OS_SLASH
						+ personId + user.getAccount()
						+ multFile.getOriginalFilename() + "." + fileExtension;

				if (imageDir.isEmpty()) {
					resp.setStatus(StatusType.ko.toString());
					resp.setMessage("Imqge directory not exist.");
					return resp;
				}

				File uploadDir = new File(imageDir);
				if (!uploadDir.exists()) {
					logger.info("Creating directory: " + uploadDir.getName()
							+ " directory doesn't exist. It will be created");

					try {
						uploadDir.mkdirs();
						logger.info(uploadDir.getName() + " directory created.");
					} catch (Exception se) {
						logger.info("ERROR: Problem creatig DIR" + imageDir);
						resp.setStatus(StatusType.ko.toString());
						resp.setMessage("ERROR: Problem creatig DIR" + imageDir);
						return resp;
					}
				}

				person.setPortrait(Boolean.TRUE);
				person.setPortraitImageName(fileName);
				person.setLastUpdateBy(user.getAccount());
				person.setLastUpdate(new Date());

				getBiographicalPeopleDAO().merge(person);

				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File(fileNameToWrite), false));

				HashMap changes = new HashMap();
				changes.put("lastUpdateBy", user.getAccount());
				HashMap category = new HashMap();
				HashMap action = new HashMap();
				action.put("add", changes);
				category.put("portrait", action);

				historyLogService.registerAction(personId,
						HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.BIO, null, category);

				stream.write(multFile.getBytes());
				stream.close();

				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Portrait uploaded.");

			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public OrganizationJson findHeadquarters(Integer personId)
			throws ApplicationThrowable {

		OrganizationJson organization = null;

		try {
			BiographicalPeople biographicalPeople = getBiographicalPeopleDao()
					.find(personId);

			if (biographicalPeople == null) {
				return null;
			}
			organization = new OrganizationJson();
			organization.setPersonId(personId);

			if (biographicalPeople.getOrganizations() == null
					|| biographicalPeople.getOrganizations().isEmpty()) {
				return organization;
			}

			List<HeadquarterJson> headQuarters = new ArrayList<HeadquarterJson>();

			for (OrganizationEntity org : biographicalPeople.getOrganizations()) {
				if (org.getPlaceAllId() != null) {
					HeadquarterJson head = new HeadquarterJson();
					head.toJson(org);
					headQuarters.add(head);
				}
			}

			organization.setHeadquarters(headQuarters);

			return organization;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson addHeadquarter(AddHeadquarterJson head)
			throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Unknown errore.");
		try {

			List<OrganizationEntity> result = getOrganizationDAO()
					.findOrganization(head);
			if (result != null && !result.isEmpty()) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("headquarter exist in DB and not added.");
				return resp;
			}

			BiographicalPeople person = getBiographicalPeopleDAO()
					.findPeopleById(head.getPersonId());

			if (person == null) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("person doesn't exist in DB.");
				return resp;
			}

			if ((person.getGender() == null || !Gender.X.toString()
					.equalsIgnoreCase(person.getGender().toString()))) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("person is not with Gender X and heqdQuarter not added.");
				return resp;
			}

			User user = null;
			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}

			List<OrganizationEntity> orgs = getOrganizationDAO()
					.findOrganizationByPersonId(head.getPersonId());
			if (orgs != null && !orgs.isEmpty() && orgs.size() < 2
					&& orgs.get(0).getPlaceAllId() == null) {
				orgs.get(0).setPlaceAllId(head.getPlaceId());
				orgs.get(0).setYear(head.getYear());
				getOrganizationDAO().merge(orgs.get(0));
				getBiographicalPeopleDao().setPeopleModInf(head.getPersonId(),
						user.getAccount());
				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("headquarter added.");
				return resp;
			}

			OrganizationEntity org = new OrganizationEntity();
			org.setPersonId(head.getPersonId());
			org.setPlaceAllId(head.getPlaceId());
			org.setYear(head.getYear());
			getOrganizationDAO().insertOrganization(org);
			getBiographicalPeopleDao().setPeopleModInf(head.getPersonId(),
					user.getAccount());

			resp.setMessage("The headquarter added.");
			resp.setStatus(StatusType.ok.toString());

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	public GenericResponseJson deleteOrganization(HeadquarterJson head)
			throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Unknown errore.");
		try {

			Integer personId = getOrganizationDAO().deleteOrganization(
					head.getHeadquarterId());
			User user = null;
			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}

			if (personId == null || personId > 0) {
				if (personId > 0) {
					getBiographicalPeopleDao().setPeopleModInf(personId,
							user.getAccount());
				}

				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("headquarter Deleted.");
				return resp;
			} else {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("headquarter not found in DB.");
				return resp;
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Override
	public List<Integer> findPeopleByPlace(Integer placeId)
			throws PersistenceException {

		List<BiographicalPeople> people = getBiographicalPeopleDAO()
				.findPeopleByPlace(placeId);

		if (people == null || people.isEmpty()) {
			return null;
		}

		List<Integer> bios = new ArrayList<Integer>();

		for (BiographicalPeople p : people) {
			bios.add(p.getPersonId());
		}

		return bios;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<BiographicalPeopleJson> findBiographicalPeople(
			List<Integer> personIds, Integer firstResult, Integer maxResult,
			String orderColumn, String ascOrDesc) throws ApplicationThrowable {

		try {

			if (personIds == null || personIds.isEmpty()) {
				return null;
			}

			List<BiographicalPeople> entityList = getBiographicalPeopleDao()
					.findBiographicalPeople(personIds, firstResult, maxResult,
							orderColumn, ascOrDesc);
			if (entityList == null || entityList.isEmpty()) {
				return null;
			}

			List<BiographicalPeopleJson> peopleJson = new ArrayList<BiographicalPeopleJson>();

			for (BiographicalPeople e : entityList) {
				BiographicalPeopleJson personJson = new BiographicalPeopleJson();
				personJson.toJson(e);
				peopleJson.add(personJson);
			}

			return peopleJson;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public BiographicalPeopleDAO getBiographicalPeopleDao() {
		return biographicalPeopleDao;
	}

	public void setBiographicalPeopleDao(
			BiographicalPeopleDAO biographicalPeopleDao) {
		this.biographicalPeopleDao = biographicalPeopleDao;
	}

	public AltNameDAO getAltNameDao() {
		return altNameDao;
	}

	public void setAltNameDao(AltNameDAO altNameDao) {
		this.altNameDao = altNameDao;
	}

	public TitleOccsListDAO getTitleOccsListDao() {
		return titleOccsListDao;
	}

	public void setTitleOccsListDao(TitleOccsListDAO titleOccsListDao) {
		this.titleOccsListDao = titleOccsListDao;
	}

	public PoLinkDAO getPoLinkDAO() {
		return poLinkDAO;
	}

	public void setPoLinkDAO(PoLinkDAO poLinkDAO) {
		this.poLinkDAO = poLinkDAO;
	}

	public ParentDAO getParentDAO() {
		return parentDAO;
	}

	public void setParentDAO(ParentDAO parentDAO) {
		this.parentDAO = parentDAO;
	}

	public PeopleDAO getPeopleDao() {
		return PeopleDao;
	}

	public void setPeopleDao(PeopleDAO peopleDao) {
		PeopleDao = peopleDao;
	}

	public MarriageDAO getMariageDao() {
		return mariageDao;
	}

	public void setMariageDao(MarriageDAO mariageDao) {
		this.mariageDao = mariageDao;
	}

	public BiographicalPeopleDAO getBiographicalPeopleDAO() {
		return biographicalPeopleDAO;
	}

	public void setBiographicalPeopleDAO(
			BiographicalPeopleDAO biographicalPeopleDAO) {
		this.biographicalPeopleDAO = biographicalPeopleDAO;
	}

	public OrganizationDAO getOrganizationDAO() {
		return organizationDAO;
	}

	public void setOrganizationDAO(OrganizationDAO organizationDAO) {
		this.organizationDAO = organizationDAO;
	}

}
