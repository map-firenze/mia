package org.medici.mia.service.people;

import java.util.Map;

import org.medici.mia.common.json.document.DocumentPeopleJson;
import org.medici.mia.exception.ApplicationThrowable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public interface BiographicalDocumentService {

	public Integer getCountAllDocumentsPeople(Integer PersonId) throws ApplicationThrowable;

	public DocumentPeopleJson getDocumentsPeople(Integer personId)
			throws ApplicationThrowable;

	public Map<String, Map<String, Integer>> getDocumentsCategoryAndFieldCountPeople(
			Integer personId) throws ApplicationThrowable;
	
	public Map<String, Integer> getDocumentsCategoryCountPeople(Integer personId)
			throws ApplicationThrowable;

}