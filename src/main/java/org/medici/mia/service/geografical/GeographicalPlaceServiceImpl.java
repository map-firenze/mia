package org.medici.mia.service.geografical;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.PersistenceException;

import de.danielbechler.diff.ObjectDifferBuilder;
import de.danielbechler.diff.node.DiffNode;
import de.danielbechler.diff.node.Visit;
import de.danielbechler.diff.selector.BeanPropertyElementSelector;
import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.PlaceJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.TopicBaseJson;
import org.medici.mia.common.json.TopicPlaceRelDocJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.document.OfficialJson;
import org.medici.mia.common.json.geographical.FindGeoPlaceNameVariantJson;
import org.medici.mia.common.json.geographical.GeographicalPlaceJson;
import org.medici.mia.common.json.geographical.ModifyGeoPlaceNameVariantJson;
import org.medici.mia.common.json.geographical.PlaceNameVariantJson;
import org.medici.mia.dao.miadoc.TopicPlaceDao;
import org.medici.mia.dao.place.PlaceDAO;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.domain.Place;
import org.medici.mia.domain.TopicPlaceEntity;
import org.medici.mia.domain.User;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Service
class GeographicalPlaceServiceImpl implements GeographicalPlaceService {

	@Autowired
	private PlaceDAO PlaceDao;

	@Autowired
	private UserService userService;

	@Autowired
	private HistoryLogService historyLogService;

	@Autowired
	private TopicPlaceDao topicPlaceDao;

	@Autowired
	private MiaDocumentService miaDocumentService;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GeographicalPlaceJson findGeographicalPlace(Integer placeId,
			boolean isNotDeleted) throws ApplicationThrowable {
		try {

			Place placeEntity = getPlaceDao().findPlaceById(placeId);

			if (placeEntity == null) {
				return null;
			}

			if (isNotDeleted
					&& (placeEntity.getLogicalDelete() != null && placeEntity
							.getLogicalDelete())) {
				return null;
			}

			GeographicalPlaceJson place = new GeographicalPlaceJson();
			Integer principalPlaceId = null;

			if (placeEntity.getPrefFlag() != null
					&& placeEntity.getPrefFlag().equalsIgnoreCase(
							PlaceType.VARIANT.getValue())) {
				principalPlaceId = getPlaceDao()
						.findPrinicipalPlaceIdFromVariantPlaceId(
								placeEntity.getGeogKey());
			}
			return place.toJson(placeEntity, principalPlaceId);

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseDataJson<Integer> unDeleteGeographicalPlace(
			Integer placeId) throws ApplicationThrowable {

		GenericResponseDataJson<Integer> resp = new GenericResponseDataJson<Integer>();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {

			Place placeEntity = getPlaceDao().findPlaceById(placeId);
			if (placeEntity == null) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("No place found with plqceId.");
				return resp;
			}

			Integer principalPlaceId = null;
			if (placeEntity != null) {
				if (placeEntity.getPrefFlag() != null
						&& placeEntity.getPrefFlag().equalsIgnoreCase(
								PlaceType.VARIANT.getValue())) {
					principalPlaceId = getPlaceDao()
							.findPrinicipalPlaceIdFromVariantPlaceId(
									placeEntity.getGeogKey());

					resp.setStatus(StatusType.w.toString());
					resp.setMessage("This is a variant name of a geographical or topographical place and therefore cannot be deleted. Please delete it from the its Principal name record.");
					resp.setData(principalPlaceId);
					return resp;

				} else {

					placeEntity.setLogicalDelete(Boolean.FALSE);
					getPlaceDao().merge(placeEntity);

					resp.setStatus(StatusType.ok.toString());
					resp.setMessage("Place is activated.");
					resp.setData(placeEntity.getPlaceAllId());
				}

			}

			return resp;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	public PlaceDAO getPlaceDao() {
		return PlaceDao;
	}

	public void setPlaceDao(PlaceDAO placeDao) {
		PlaceDao = placeDao;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson modifyGeographicalPlace(
			GeographicalPlaceJson modifyJson) throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {

			// Get the username logged in
			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());

			final HashMap<String, Object> changes = new HashMap<String, Object>();
			final GeographicalPlaceJson oldjson = findGeographicalPlace(
					modifyJson.getPlaceId(), true);

			Integer modifyPlace = getPlaceDao().modifyGeographicalPlace(
					modifyJson, user);
			if (modifyPlace == 0) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("No People found to be modified.");
				return resp;
			}

			final GeographicalPlaceJson newjson = findGeographicalPlace(
					modifyJson.getPlaceId(), true);

			DiffNode diff = ObjectDifferBuilder.buildDefault().compare(oldjson,
					newjson);
			final HashMap<Object, Object> before = new HashMap<Object, Object>();
			final HashMap<Object, Object> after = new HashMap<Object, Object>();
			diff.visit(new DiffNode.Visitor() {
				public void node(DiffNode node, Visit visit) {
					final Object baseValue = node.canonicalGet(oldjson);
					final Object workingValue = node.canonicalGet(newjson);
					if (node.hasChanges()
							&& !node.getPath().toString().equals("/")) {
						if (node.hasChildren()) {
							visit.dontGoDeeper();
						}
						String key = node.getPath().getLastElementSelector()
								.toString();
						before.put(node.getPath().getLastElementSelector(),
								baseValue);
						after.put(node.getPath().getLastElementSelector(),
								workingValue);

					}
				}
			});
			changes.put("before", before);
			changes.put("after", after);
			HashMap<String, Object> category = new HashMap<String, Object>();
			HashMap<String, Object> action = new HashMap<String, Object>();
			action.put("edit", changes);
			category.put("details", action);
			if (!((after.containsKey(new BeanPropertyElementSelector(
					"lastUpdate")) && after.size() == 1) ^ (after
					.containsKey(new BeanPropertyElementSelector("lastUpdate"))
					&& after.containsKey(new BeanPropertyElementSelector(
							"lastUpdateBy")) && after.size() == 2))) {
				historyLogService.registerAction(modifyJson.getPlaceId(),
						HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.GEO, null, category);
			}

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Geographical Place modified.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	public List<GeographicalPlaceJson> findPlaceNameVariants(String placeName)
			throws PersistenceException {

		try {

			if (placeName == null || placeName.isEmpty()) {
				return null;
			}

			List<Place> placeEntities = getPlaceDao().findPlaceNameVariants(
					placeName);
			if (placeEntities != null && !placeEntities.isEmpty()) {
				List<GeographicalPlaceJson> placesJson = new ArrayList<GeographicalPlaceJson>();
				for (Place placeEntity : placeEntities) {
					GeographicalPlaceJson placeJson = new GeographicalPlaceJson();
					placeJson.toJson(placeEntity);
					placesJson.add(placeJson);
				}

				return placesJson;
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return null;
	}

	public FindGeoPlaceNameVariantJson findPlaceNameVariantsById(Integer placeId)
			throws PersistenceException {

		if (placeId == null || placeId.equals("")) {
			return null;
		}
		List<Place> placeEntities = getPlaceDao().findPlaceNameVariantsById(
				placeId);
		if (placeEntities != null && !placeEntities.isEmpty()) {
			List<PlaceNameVariantJson> placeNameVariants = new ArrayList<PlaceNameVariantJson>();
			for (Place placeEntity : placeEntities) {
				PlaceNameVariantJson placeNameVariant = new PlaceNameVariantJson();
				placeNameVariant.setPlaceName(placeEntity.getPlaceName());
				placeNameVariant.setPlaceAllId(placeEntity.getPlaceAllId());
				placeNameVariant.setPrefFlag(placeEntity.getPrefFlag());
				placeNameVariants.add(placeNameVariant);
			}

			FindGeoPlaceNameVariantJson findPlaceNameVariantsJson = new FindGeoPlaceNameVariantJson();
			findPlaceNameVariantsJson.setPlaceAllId(placeId);
			findPlaceNameVariantsJson.setPlaceNameVariants(placeNameVariants);
			return findPlaceNameVariantsJson;
		}

		return null;

	}

	public GenericResponseJson modifyGeoPlaceNameVariant(
			ModifyGeoPlaceNameVariantJson modifyJson)
			throws PersistenceException {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {

			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());

			final HashMap<String, Object> changes = new HashMap<String, Object>();

			Integer parentPlaceId = 0;
			for (Place place : getPlaceDao().findByGeogKey(
					getPlaceDao()
							.findPlaceById(
									modifyJson.getNewPlaceNameVariant()
											.getPlaceAllId()).getGeogKey())) {
				if (place.getPrefFlag().equals("P")) {
					parentPlaceId = place.getPlaceAllId();
				}
			}

			final GeographicalPlaceJson oldjson = findGeographicalPlace(
					modifyJson.getNewPlaceNameVariant().getPlaceAllId(), true);

			Integer result = getPlaceDao().modifyPlaceNameVariant(modifyJson,
					user);

			final GeographicalPlaceJson newjson = findGeographicalPlace(
					modifyJson.getNewPlaceNameVariant().getPlaceAllId(), true);

			DiffNode diff = ObjectDifferBuilder.buildDefault().compare(oldjson,
					newjson);
			final HashMap<Object, Object> before = new HashMap<Object, Object>();
			final HashMap<Object, Object> after = new HashMap<Object, Object>();
			diff.visit(new DiffNode.Visitor() {
				public void node(DiffNode node, Visit visit) {
					final Object baseValue = node.canonicalGet(oldjson);
					final Object workingValue = node.canonicalGet(newjson);
					if (node.hasChanges()
							&& !node.getPath().toString().equals("/")) {
						if (node.hasChildren()) {
							visit.dontGoDeeper();
						}
						before.put(node.getPath().getLastElementSelector(),
								baseValue);
						after.put(node.getPath().getLastElementSelector(),
								workingValue);

					}
				}
			});
			changes.put("before", before);
			changes.put("after", after);
			HashMap<String, Object> category = new HashMap<String, Object>();
			HashMap<String, Object> action = new HashMap<String, Object>();
			action.put("edit", changes);
			category.put("placeNameVariant", action);

			if (result > 0) {
				if (!((after.containsKey(new BeanPropertyElementSelector(
						"lastUpdate")) && after.size() == 1) ^ (after
						.containsKey(new BeanPropertyElementSelector(
								"lastUpdate"))
						&& after.containsKey(new BeanPropertyElementSelector(
								"lastUpdateBy")) && after.size() == 2))) {
					historyLogService.registerAction(parentPlaceId,
							HistoryLogEntity.UserAction.EDIT,
							HistoryLogEntity.RecordType.GEO, null, category);
				}
				resp.setMessage(result + " Place name variant is modified");
				resp.setStatus(StatusType.ok.toString());
			} else {
				resp.setMessage("Place name variant  not modified in DB.");
				resp.setStatus(StatusType.w.toString());
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	public GenericResponseJson addGeoPlaceNameVariant(
			ModifyGeoPlaceNameVariantJson modifyJson)
			throws PersistenceException {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {

			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());

			Integer result = getPlaceDao()
					.addPlaceNameVariant(modifyJson, user);

			if (result > 0) {
				HashMap<String, Object> changes = new HashMap<String, Object>();
				HashMap<String, String> change = new HashMap<String, String>();
				change.put("placeName", modifyJson.getNewPlaceNameVariant()
						.getPlaceName());
				changes.put("add", change);
				HashMap<String, Object> category = new HashMap<String, Object>();
				category.put("placeNameVariant", changes);
				historyLogService.registerAction(modifyJson.getPlaceAllId(),
						HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.GEO, null, category);
				resp.setMessage(result + " Place name variant is modified");
				resp.setStatus(StatusType.ok.toString());
			} else {
				resp.setMessage("Place name variant  not modified in DB.");
				resp.setStatus(StatusType.w.toString());
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	public Integer deletePlace(Integer placeId) {

		try {

			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());

			return getPlaceDao().deletePlace(placeId, user);

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	public GenericResponseJson deletePlaceVariant(Integer placeId) {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {

			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());

			String placeName = getPlaceDao().findPlaceById(placeId)
					.getPlaceName();

			Integer result = getPlaceDao().deletePlaceVariant(placeId, user);
			Integer parentPlaceId = 0;
			for (Place place : getPlaceDao().findByGeogKey(
					getPlaceDao().findPlaceById(placeId).getGeogKey())) {
				if (place.getPrefFlag().equals("P")) {
					parentPlaceId = place.getPlaceAllId();
				}

			}
			if (result > 0) {
				HashMap<String, Object> changes = new HashMap<String, Object>();
				HashMap<String, String> change = new HashMap<String, String>();
				change.put("placeName", placeName);
				changes.put("delete", change);
				HashMap<String, Object> category = new HashMap<String, Object>();
				category.put("placeNameVariant", changes);
				historyLogService.registerAction(parentPlaceId,
						HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.GEO, null, category);
				resp.setMessage(result + " Place name variant is deleted");
				resp.setStatus(StatusType.ok.toString());
			} else {
				resp.setMessage("Place name variant was already deleted or it was not a variant place or doesn't exist in DB.");
				resp.setStatus(StatusType.w.toString());
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public TopicPlaceRelDocJson findTopicsByPlace(Integer placeId)
			throws ApplicationThrowable {
		try {

			List<TopicPlaceEntity> topicPlaces = getTopicPlaceDao()
					.findTopicPlacesByPlaceId(placeId);

			if (topicPlaces == null || topicPlaces.isEmpty()) {
				return null;
			}
			List<Integer> docs = new ArrayList<Integer>();

			List<TopicBaseJson> topicsPlaces = new ArrayList<TopicBaseJson>();
			for (TopicPlaceEntity tp : topicPlaces) {
				if (!docs.contains(tp.getDocumentEntityId())) {
					docs.add(tp.getDocumentEntityId());

				}

				TopicBaseJson topic = new TopicBaseJson();
				topic.setPlaceId(tp.getPlaceId());
				topic.setTopicListId(tp.getTopicListId());

				if (!topicsPlaces.contains(topic)) {
					topicsPlaces.add(topic);
				}
			}

			TopicPlaceRelDocJson resp = new TopicPlaceRelDocJson();
			resp.setCountTotalDocuments(docs.size());
			resp.setTopics(topicsPlaces);

			return resp;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<DocumentJson> findDccumentsByPlaceAndTopic(Integer placeId,
			Integer topicListId) throws ApplicationThrowable {
		try {

			List<Integer> docIds = getTopicPlaceDao()
					.findDccumentsByPlaceAndTopic(placeId, topicListId);

			if (docIds == null || docIds.isEmpty()) {
				return null;
			}

			return getMiaDocumentService().findDocuments(docIds, null,null, "placeAllId", "asc");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public TopicPlaceDao getTopicPlaceDao() {
		return topicPlaceDao;
	}

	public void setTopicPlaceDao(TopicPlaceDao topicPlaceDao) {
		this.topicPlaceDao = topicPlaceDao;
	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

	public void setMiaDocumentService(MiaDocumentService miaDocumentService) {
		this.miaDocumentService = miaDocumentService;
	}

}
