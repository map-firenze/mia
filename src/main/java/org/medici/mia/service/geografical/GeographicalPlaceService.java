package org.medici.mia.service.geografical;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.TopicPlaceRelDocJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.geographical.FindGeoPlaceNameVariantJson;
import org.medici.mia.common.json.geographical.GeographicalPlaceJson;
import org.medici.mia.common.json.geographical.ModifyGeoPlaceNameVariantJson;
import org.medici.mia.exception.ApplicationThrowable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public interface GeographicalPlaceService {

	public GenericResponseJson modifyGeographicalPlace(
			GeographicalPlaceJson modifyJson) throws ApplicationThrowable;

	public GeographicalPlaceJson findGeographicalPlace(Integer placeId,
			boolean isNotDeleted) throws ApplicationThrowable;

	public List<GeographicalPlaceJson> findPlaceNameVariants(String placeName)
			throws PersistenceException;

	public FindGeoPlaceNameVariantJson findPlaceNameVariantsById(Integer placeId)
			throws PersistenceException;

	public GenericResponseJson modifyGeoPlaceNameVariant(
			ModifyGeoPlaceNameVariantJson modifyGeoPlace)
			throws PersistenceException;

	public GenericResponseJson addGeoPlaceNameVariant(
			ModifyGeoPlaceNameVariantJson modifyGeoPlace)
			throws PersistenceException;

	public Integer deletePlace(Integer placeId) throws PersistenceException;

	public GenericResponseJson deletePlaceVariant(Integer variantPlaceId);

	public GenericResponseDataJson<Integer> unDeleteGeographicalPlace(
			Integer placeId) throws ApplicationThrowable;

	public TopicPlaceRelDocJson findTopicsByPlace(Integer placeId)
			throws ApplicationThrowable;
	
	public List<DocumentJson> findDccumentsByPlaceAndTopic(Integer placeId,
			Integer topicListId) throws ApplicationThrowable;

}