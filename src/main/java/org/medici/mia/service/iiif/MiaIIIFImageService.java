package org.medici.mia.service.iiif;

import org.medici.mia.exception.ApplicationThrowable;
/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public interface MiaIIIFImageService {

	String getIIIFImageFileName(Integer uploadFileId)
			throws ApplicationThrowable;

}
