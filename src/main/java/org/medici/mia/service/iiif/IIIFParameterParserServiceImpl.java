package org.medici.mia.service.iiif;



import org.apache.log4j.Logger;
//import org.springframework.stereotype.Service;
import org.medici.mia.iiif.model.IIIFImageBitDepth;
import org.medici.mia.iiif.model.IIIFImageFormat;
import org.medici.mia.iiif.model.RegionParameters;
import org.medici.mia.iiif.model.RegionParametersImpl;
import org.medici.mia.iiif.model.ResizeParameters;
import org.medici.mia.iiif.model.ResizeParametersImpl;
import org.medici.mia.iiif.model.RotationParameters;
import org.medici.mia.iiif.model.RotationParametersImpl;
/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

//@Service
public class IIIFParameterParserServiceImpl implements IIIFParameterParserService {

	private static final Logger LOGGER = Logger.getLogger(IIIFParameterParserServiceImpl.class.toString());

	private float[] parseFloatValues(String values, int expectedSize) throws Exception {

		String[] groups = values.split(",");
		if (groups.length != expectedSize) { 
			LOGGER.warn("Invalid values definition. {} float numbers needed! "+ expectedSize);
			throw new Exception("Invalid values definition. " + expectedSize + " float numbers needed!");
		}
		float[] floatValues = new float[expectedSize];
		try {
			for (int i = 0; i < groups.length; i++) {
				floatValues[i] = Float.parseFloat(groups[i]);
			}
		}
		catch (NumberFormatException nfe) {
			LOGGER.warn("Invalid value number.");
			throw new Exception("Invalid value number. Must be integer or float");
		}
		return floatValues;
	}

	@Override
	public IIIFImageFormat parseIiifFormat(String targetFormat) throws Exception {

		IIIFImageFormat format = IIIFImageFormat.getByExtension(targetFormat);
		if (format == null) {
			throw new Exception();
		}
		return format;
	}

	@Override
	public IIIFImageBitDepth parseIiifQuality(String targetQuality) throws Exception {

		if ("bitonal".equalsIgnoreCase(targetQuality)) {
			return IIIFImageBitDepth.BITONAL;
		}
		else if ("gray".equalsIgnoreCase(targetQuality)) {
			return IIIFImageBitDepth.GRAYSCALE;
		}
		else if ("color".equalsIgnoreCase(targetQuality)) {
			return IIIFImageBitDepth.COLOR;
		}
		else if ("default".equalsIgnoreCase(targetQuality)) {
			return null;
		}
		else {
			throw new Exception();
		}

	}

	@Override
	public RegionParameters parseIiifRegion(String region) throws Exception {

		assert region != null;
		if ("full".equals(region)) {
			// The complete image is returned, without any cropping.
			return null; // indicates that no region has to be cropped
		}
		RegionParameters params = new RegionParametersImpl();
		if (region.startsWith("pct:")) {
			region = region.substring("pct:".length());
			params.setAbsolute(false);
		}
		else {
			params.setAbsolute(true);
		}
		float[] dimensions = parseFloatValues(region, 4);
		params.setHorizontalOffset(dimensions[0]);
		params.setVerticalOffset(dimensions[1]);
		params.setWidth(dimensions[2]);
		params.setHeight(dimensions[3]);
		return params;
	}

	@Override
	public RotationParameters parseIiifRotation(String rotation) throws Exception {

		assert rotation != null;
		RotationParameters params = new RotationParametersImpl();
		if (rotation.startsWith("!")) {
			params.setMirrorHorizontally(true);
			rotation = rotation.substring(1);
		}
		int degrees = parseIntegerValue(rotation);
		if (degrees < 0 || degrees > 360) {
			throw new Exception("The degrees of clockwise rotation must be between 0 and 360!");
		}
		params.setDegrees(degrees);
		return params;
	}

	@Override
	public ResizeParameters parseIiifSize(String size) throws Exception {

		assert size != null;
		if ("full".equals(size)) {
			// The extracted region is not scaled, and is returned at its full size.
			return null; // indicates no resizing
		}
		ResizeParameters params = new ResizeParametersImpl();
		if (size.startsWith("pct:")) {
			int scaleFactor = parseIntegerValue(size.substring("pct:".length()));
			params.setScaleFactor(scaleFactor);
		}
		else if (size.endsWith(",")) {
			int targetWidth = parseIntegerValue(size.substring(0, size.lastIndexOf(",")));
			params.setWidth(targetWidth);
		}
		else if (size.startsWith(",")) {
			int targetHeight = parseIntegerValue(size.substring(1));
			params.setHeight(targetHeight);
		}
		else if (size.startsWith("!")) {
			int[] values = parseIntegerValues(size.substring(1), 2);
			params.setMaxWidth(values[0]);
			params.setMaxHeight(values[1]);
		}
		else {
			int[] values = parseIntegerValues(size, 2);
			params.setWidth(values[0]);
			params.setHeight(values[1]);
		}
		return params;
	}

	private int parseIntegerValue(String value) throws Exception {

		try {
			return Integer.parseInt(value);
		}
		catch (NumberFormatException nfe) {
			LOGGER.warn("Invalid format.");
			throw new Exception("Invalid format. Must be an integer!");
		}
	}

	private int[] parseIntegerValues(String values, int expectedSize) throws Exception {

		String[] groups = values.split(",");
		if (groups.length != expectedSize) {
			LOGGER.warn("Invalid values definition. {} integer numbers needed! " + expectedSize);
			throw new Exception("Invalid values definition. " + expectedSize + " integer numbers needed!");
		}
		int[] integerValues = new int[expectedSize];
		try {
			for (int i = 0; i < groups.length; i++) {
				integerValues[i] = Integer.parseInt(groups[i]);
			}
		}
		catch (NumberFormatException nfe) {
			LOGGER.warn("Invalid value number.");
			throw new Exception("Invalid value number. Must be an integer.");
		}
		return integerValues;
	}

}
