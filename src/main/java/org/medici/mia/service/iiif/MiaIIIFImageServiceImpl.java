package org.medici.mia.service.iiif;

import org.apache.log4j.Logger;
import org.medici.mia.dao.uploadfile.UploadFileDAO;
import org.medici.mia.dao.uploadinfo.UploadInfoDAO;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.UploadInfoEntity;
import org.medici.mia.exception.ApplicationThrowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Service
@Transactional(readOnly = true)
public class MiaIIIFImageServiceImpl implements MiaIIIFImageService {
	
	private final Logger logger = Logger.getLogger(this.getClass());

	public static final String ERRORMSG_INCORRECT_INPUT = "ERROR: The Input value may be null or have no correct value.";
	public static final String WARNINGMSG_UPLOAD_CHANGETO_PUBLIC = "WARNING: The upload belongs to this file changed to public.";
	public static final String WARNINGMSG_PRIVACY_NOTUPDATED = "WARNING: The upload belongs to this file changed to public.";

	@Autowired
	private UploadInfoDAO uploadInfoDAO;

	@Autowired
	private UploadFileDAO uploadFileDAO;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public String getIIIFImageFileName(Integer uploadFileId)
			throws ApplicationThrowable {

		try {

			Integer uploadId = null;
			UploadInfoEntity uploadInfo = null;
			UploadFileEntity uploadFile = getUploadFileDAO().findUploadFile(
					uploadFileId);
			if (uploadFile != null) {
				uploadId = uploadFile.getIdTblUpload();
				uploadInfo = getUploadInfoDAO().getUploadById(uploadId);
			}
			String volume = "";
			String insert = "";

			if (uploadInfo != null) {
				
				if(uploadInfo
						.getVolumeEntity().getVolume()!=null){
					volume = uploadInfo
							.getVolumeEntity().getVolume();
				}
				
				if(uploadInfo
						.getInsertEntity()!=null){
					insert = uploadInfo
							.getInsertEntity().getInsertName();
				}
				
				

				String realPath = org.medici.mia.common.util.FileUtils
						.getRealPathIIIFImage(uploadInfo.getRepositoryEntity()
								.getGoogleLocation().getgPlaceId(), uploadInfo
								.getRepositoryEntity()
								.getRepositoryAbbreviation(), uploadInfo
								.getCollectionEntity()
								.getCollectionAbbreviation(), volume, insert);
				logger.info("realPath of the file: " + realPath + uploadFile.getFilename());

				return realPath + uploadFile.getFilename();
				

			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return null;
	}

	public UploadFileDAO getUploadFileDAO() {
		return uploadFileDAO;
	}

	public void setUploadFileDAO(UploadFileDAO uploadFileDAO) {
		this.uploadFileDAO = uploadFileDAO;
	}

	public UploadInfoDAO getUploadInfoDAO() {
		return uploadInfoDAO;
	}

	public void setUploadInfoDAO(UploadInfoDAO uploadInfoDAO) {
		this.uploadInfoDAO = uploadInfoDAO;
	}

}