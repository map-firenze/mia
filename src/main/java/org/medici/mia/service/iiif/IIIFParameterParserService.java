package org.medici.mia.service.iiif;

import org.medici.mia.iiif.model.IIIFImageBitDepth;
import org.medici.mia.iiif.model.IIIFImageFormat;
import org.medici.mia.iiif.model.RegionParameters;
import org.medici.mia.iiif.model.ResizeParameters;
import org.medici.mia.iiif.model.RotationParameters;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public interface IIIFParameterParserService {

	IIIFImageFormat parseIiifFormat(String targetFormat) throws Exception;

	IIIFImageBitDepth parseIiifQuality(String targetQuality) throws Exception;

	RegionParameters parseIiifRegion(String region) throws Exception;

	RotationParameters parseIiifRotation(String rotation) throws Exception;

	ResizeParameters parseIiifSize(String size) throws Exception;

}
