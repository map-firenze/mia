package org.medici.mia.service.iiif;

import org.medici.mia.iiif.model.IIIFImage;
import org.medici.mia.iiif.model.IIIFImageBitDepth;
import org.medici.mia.iiif.model.IIIFImageFormat;
import org.medici.mia.iiif.model.RegionParameters;
import org.medici.mia.iiif.model.ResizeParameters;
import org.medici.mia.iiif.model.RotationParameters;
/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public interface IIIFImageService {

//	ImageInfo getImageInfo(String identifier) throws Exception, UnsupportedOperationException;
//
//	Image processImage(String identifier, RegionParameters regionParameters, ResizeParameters sizeParameters, RotationParameters rotationParameters, ImageBitDepth bitDepthParameter, ImageFormat formatParameter) throws Exception;
	public IIIFImage processJAIImage(byte[] byteImage, RegionParameters regionParameters, ResizeParameters sizeParameters, RotationParameters rotationParameters, IIIFImageBitDepth bitDepthParameter, IIIFImageFormat formatParameter) throws Exception;


}
