package org.medici.mia.service.iiif;

import java.io.File;
import java.io.FileInputStream;

import org.apache.log4j.Logger;
import org.medici.mia.iiif.exception.InvalidParametersException;
import org.medici.mia.iiif.exception.ResourceNotFoundException;
import org.medici.mia.iiif.exception.UnsupportedFormatException;
import org.medici.mia.iiif.model.IIIFImage;
import org.medici.mia.iiif.model.IIIFImageBitDepth;
import org.medici.mia.iiif.model.IIIFImageFormat;
import org.medici.mia.iiif.model.IIIFImageImpl;
import org.medici.mia.iiif.model.RegionParameters;
import org.medici.mia.iiif.model.ResizeParameters;
import org.medici.mia.iiif.model.ResizeParametersImpl;
import org.medici.mia.iiif.model.RotationParameters;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Service
public class IIIFImageServiceImpl implements IIIFImageService {

	private static final Logger LOGGER = Logger
			.getLogger(IIIFImageServiceImpl.class.toString());

	// @Autowired
	// private List<ImageRepository> imageRepositories;

	// @Autowired(required = false)
	// private ImageSecurityService imageSecurityService;

	// @Override
	// public ImageInfo getImageInfo(String identifier) throws
	// UnsupportedFormatException, UnsupportedOperationException {
	//
	// if (imageSecurityService != null &&
	// !imageSecurityService.isAccessAllowed(identifier)) {
	// throw new ResourceNotFoundException(); // TODO maybe throw an explicitely
	// access disallowed exception
	// }
	// // FIXME: This is really ugly, but unfortunately there's no way to tell
	// from the identifier what
	// // format we're dealing with...
	// for (ImageRepository repo : this.imageRepositories) {
	// try {
	// ImageInfo info = repo.getImageInfo(identifier);
	// LOGGER.debug("Using " + repo.getClass().getName());
	// return info;
	// }
	// catch (Throwable repoNotWorking) {
	// }
	// }
	// throw new UnsupportedFormatException();
	// }

	// private Image getImage(String identifier, RegionParameters
	// regionParameters, ImageFormat outputFormat, ImageBitDepth
	// bitDepthParameter) throws UnsupportedFormatException,
	// InvalidParametersException, UnsupportedOperationException {
	//
	// for (ImageRepository repo : this.imageRepositories) {
	// if (!repo.supportsCropOperation(regionParameters) ||
	// !repo.supportsOutputFormat(outputFormat) ||
	// !repo.supportsBitDepth(bitDepthParameter)) {
	// continue;
	// }
	// try {
	// Image image = repo.getImage(identifier, regionParameters);
	// LOGGER.debug("Using " + repo.getClass().getName());
	// return image;
	// }
	// catch (InvalidParametersException e) {
	// throw e;
	// }
	// catch (Throwable repoNotWorking) {
	// }
	// }
	// throw new UnsupportedFormatException();
	// }

	private IIIFImage getJAIImage(byte[] byteImage,
			RegionParameters regionParameters)
			throws UnsupportedFormatException, InvalidParametersException,
			UnsupportedOperationException {

		try {
			IIIFImage image = new IIIFImageImpl(byteImage, regionParameters);

			return image;
		}

		catch (Throwable repoNotWorking) {
		}

		throw new UnsupportedFormatException();
	}

	// @Override
	// public Image processImage(String identifier, RegionParameters
	// regionParameters, ResizeParameters sizeParameters, RotationParameters
	// rotationParameters, ImageBitDepth bitDepthParameter, ImageFormat
	// formatParameter) throws Exception {
	//
	// if (imageSecurityService != null &&
	// !imageSecurityService.isAccessAllowed(identifier)) {
	// LOGGER.info("Access to image '{}' is not allowed! "+ identifier);
	// throw new ResourceNotFoundException(); // TODO maybe throw an explicitely
	// access disallowed exception
	// }
	// Image image = getImage(identifier, regionParameters, formatParameter,
	// bitDepthParameter);
	// if (image == null) {
	// throw new ResourceNotFoundException();
	// }
	// image = transformImage(image, regionParameters, sizeParameters,
	// rotationParameters, bitDepthParameter, formatParameter);
	// return image;
	// }

	@Override
	public IIIFImage processJAIImage(byte[] byteImage,
			RegionParameters regionParameters, ResizeParameters sizeParameters,
			RotationParameters rotationParameters,
			IIIFImageBitDepth bitDepthParameter, IIIFImageFormat formatParameter)
			throws Exception {

		IIIFImage image = getJAIImage(byteImage, regionParameters);
		if (image == null) {
			throw new ResourceNotFoundException();
		}
		image = transformImage(image, regionParameters, sizeParameters,
				rotationParameters, bitDepthParameter, formatParameter);
		return image;
	}

	private IIIFImage transformImage(IIIFImage image,
			RegionParameters regionParameters, ResizeParameters sizeParameters,
			RotationParameters rotationParameters,
			IIIFImageBitDepth bitDepthParameter, IIIFImageFormat formatParameter)
			throws Exception {

		// now do processing:
		if (regionParameters != null
				&& (image.getWidth() != regionParameters.getWidth() || image
						.getHeight() != regionParameters.getHeight())) {
			image = image.crop(regionParameters);
		}

		if (sizeParameters != null) {
			sizeParameters = new ResizeParametersImpl(sizeParameters,
					image.getWidth(), image.getHeight());
		}

		if (sizeParameters != null && sizeParameters.getMaxHeight() != -1
				&& sizeParameters.getMaxWidth() != -1) {
			image = image.scale(sizeParameters);
		}
		if (rotationParameters != null) {
			if (rotationParameters.isMirrorHorizontally()) {
				image = image.flipHorizontally();
			}
			if (rotationParameters.getDegrees() > 0) {
				image = image.rotate(rotationParameters.getDegrees());
			}
		}
		if (bitDepthParameter != null) {
			image = image.toDepth(bitDepthParameter);
		}
		if (!(formatParameter == image.getFormat())) {
			image = image.convert(formatParameter);
		}
		image.performTransformation();
		return image;
	}

	private byte[] readContentIntoByteArray() {

		File file = new File("U:/SW/iiif.jpg");
		FileInputStream fileInputStream = null;
		byte[] bFile = new byte[(int) file.length()];
		try {
			// convert file into array of bytes
			fileInputStream = new FileInputStream(file);
			fileInputStream.read(bFile);
			fileInputStream.close();
			for (int i = 0; i < bFile.length; i++) {
				System.out.print((char) bFile[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bFile;
	}

}
