
package org.medici.mia.service.upload;

import java.util.List;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;

import org.medici.mia.common.json.CollectionJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.GenericUploadResponseJson;
import org.medici.mia.common.json.RepositoryJson;
import org.medici.mia.controller.archive.RotateType;
import org.medici.mia.controller.upload.UploadBean;
import org.medici.mia.domain.CollectionEntity;
import org.medici.mia.domain.InsertEntity;
import org.medici.mia.domain.RepositoryEntity;
import org.medici.mia.domain.SeriesEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.User;
import org.medici.mia.domain.Volume;
import org.medici.mia.exception.ApplicationThrowable;
import org.springframework.web.multipart.MultipartFile;


public interface UploadService {
	
	public List<CollectionEntity> findCollectionsByName(String collectionName, Integer repositoryId) throws ApplicationThrowable;
	public List<CollectionEntity> findAllCollections(Integer repositoryId) throws ApplicationThrowable;
	public List<CollectionEntity> checkCollectionsByName(String collectionName, Integer repositoryId) throws ApplicationThrowable;
	public List<SeriesEntity> findSeriesByTitle(String seriesTitle, Integer collectionId) throws ApplicationThrowable;
	public List<SeriesEntity> findAllSeries(Integer collectionId) throws ApplicationThrowable;
	public List<Volume> findVolumeByName(String volumeName, Integer collectionId) throws ApplicationThrowable;
	public List<Volume> findVolumeByName(String volumeName, Integer collectionId, Integer limit) throws ApplicationThrowable;
	public List<Volume> findAllVolumes(Integer collectionId) throws ApplicationThrowable;
	public List<RepositoryEntity> findRepositoryByName(String repositoryName) throws ApplicationThrowable;	
	public List<RepositoryEntity> checkRepositoryByNameLocation (String repositoryName, String repositoryLocation) throws ApplicationThrowable;
	public List<RepositoryEntity> checkRepositoryByLocationAbbreviation (String repositoryLocation, String repositoryAbbreviation) throws ApplicationThrowable;
	public UploadBean insertUploadImage(UploadBean uploadBean) throws ApplicationThrowable;
	public List<InsertEntity> findInsertByName(String insertName, Integer volumeId) throws ApplicationThrowable;
	public List<InsertEntity> findAllInserts(Integer volumeId) throws ApplicationThrowable;
	public Future<List<GenericUploadResponseJson>> convertFiles(List<UploadFileEntity> uploadFileEntities, String filePath, boolean notCrop) throws ApplicationThrowable;
	public UploadBean addMyArchiveFiles(List<MultipartFile> files, Integer uploadInfoId, boolean showInHistory) throws ApplicationThrowable;
	public Integer replaceFiles(Integer oldUploadFileId, Integer newUploadFileId);
	public GenericResponseJson rotateImages(List<Integer> uploadFileIds, RotateType rotateType);
	public List<RepositoryJson> findAllRepositories() throws ApplicationThrowable;
	
	public InsertEntity findInsertById(Integer insertId) throws ApplicationThrowable;
	public UploadBean insertUploadOptionalFile(UploadBean uploadBean) throws ApplicationThrowable;
	
	public void SaveSharedUsers(List<String> accounts, Integer fileId, Integer uploadId, HttpServletRequest request) throws ApplicationThrowable;
	public List<User> findSharedUsersByDocumentId(Integer documentId) throws ApplicationThrowable;

	public List<User> findSharedUsers(Integer uploadId, Integer fileId) throws ApplicationThrowable;
	
	public List<User> findSharedUsersByUpdateId(Integer uploadId) throws ApplicationThrowable;
	public List<Integer> findSharedImages(Integer uploadId) throws ApplicationThrowable;
	
	public RepositoryEntity findRepositoryById(Integer repositoryId) throws ApplicationThrowable;
	void SaveSharedUsersWithDocumentId(List<String> accounts, Integer documentId, Boolean replaceSharedUsers,
			HttpServletRequest request) throws ApplicationThrowable;
	
	public CollectionEntity addCollection(CollectionJson collectionJson);
}
