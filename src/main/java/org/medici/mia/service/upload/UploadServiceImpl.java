package org.medici.mia.service.upload;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Future;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.log4j.Logger;
import org.medici.mia.common.json.CollectionJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.GenericUploadResponseJson;
import org.medici.mia.common.json.RepositoryJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.message.MessageSendJson;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.comparator.InsertEntityComparator;
import org.medici.mia.comparator.VolumeComparator;
import org.medici.mia.controller.archive.RotateType;
import org.medici.mia.controller.upload.UploadBean;
import org.medici.mia.dao.collection.CollectionDAO;
import org.medici.mia.dao.fileSharing.FileSharingDAO;
import org.medici.mia.dao.folio.FolioDao;
import org.medici.mia.dao.googlelocation.GoogleLocationDAO;
import org.medici.mia.dao.insert.InsertDAO;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.dao.repository.RepositoryDAO;
import org.medici.mia.dao.series.SeriesDAO;
import org.medici.mia.dao.uploadfile.UploadFileDAO;
import org.medici.mia.dao.uploadinfo.UploadInfoDAO;
import org.medici.mia.dao.user.UserDAO;
import org.medici.mia.dao.volume.VolumeDAO;
import org.medici.mia.domain.CollectionEntity;
import org.medici.mia.domain.FileSharing;
import org.medici.mia.domain.FolioEntity;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.domain.InsertEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.RepositoryEntity;
import org.medici.mia.domain.SeriesEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.UploadInfoEntity;
import org.medici.mia.domain.User;
import org.medici.mia.domain.Volume;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.message.MessageService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Service
@Transactional(readOnly = true)
public class UploadServiceImpl implements UploadService {

	private final Logger logger = Logger.getLogger(this.getClass());
	private static String UPLOAD_PATH = ApplicationPropertyManager.getApplicationProperty("upload.path");
	private static File tmpDir = new File(UPLOAD_PATH);
	private final static String OS_SLASH = org.medici.mia.common.util.FileUtils.OS_SLASH;

	@Autowired
	private CollectionDAO collectionDAO;

	@Autowired
	private RepositoryDAO repositoryDAO;

	@Autowired
	private GoogleLocationDAO googleLocationDAO;

	@Autowired
	private SeriesDAO seriesDAO;

	@Autowired
	private VolumeDAO volumeDAO;

	@Autowired
	private InsertDAO insertDAO;

	@Autowired
	private UploadInfoDAO uploadInfoDAO;

	@Autowired
	private UploadFileDAO uploadFileDAO;

	@Autowired
	private UserService userService;
	
	@Autowired
	private FileSharingDAO fileSharingDAO;
	
	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private FolioDao folioDao;
	
	@Autowired
	private GenericDocumentDao documentDAO;

	@Autowired
	private HistoryLogService historyLogService;
	
	@Autowired
	private MessageService messageService;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public UploadBean insertUploadImage(UploadBean uploadBean) throws ApplicationThrowable {
		String realPath = "";
		List<List<String>> roolbackList = null;
		if (!tmpDir.isDirectory()) {
			logger.error("ATTENTION: The temp directory doesn't exist!");
			throw new ApplicationThrowable();
		}

		if (uploadBean.getRepEntity() == null || uploadBean.getCollEntity() == null || uploadBean.getSeries() == null
				|| uploadBean.getVolume() == null || uploadBean.getInsert() == null) {
			logger.error("ATTENTION: The Repository || Collection || Volume || insert || series entity may be null.");
			throw new ApplicationThrowable();
		}

		try {

			String gPlaceId = uploadBean.getRepEntity().getLocation();

			Integer repInfoId = null;
			Integer collInfoId = null;
			Integer serInfoId = null;
			Integer volInfoId = null;
			Integer insInfoId = null;
			Integer uploadInfoId = null;

			if (uploadBean.getRepEntity().getRepositoryId() == null) {
				// Repository is new
				// Check if GoogleLocation in this new Repository is new

				if (getGoogleLocationDAO().getLocationById(gPlaceId) == null) {
					// googleLocation doesn't exist . inser google Location
					getGoogleLocationDAO().saveLocation(uploadBean.getGlEntity());
				}
				repInfoId = getRepositoryDAO().insertRepository(uploadBean.getRepEntity());

			} else {
				repInfoId = uploadBean.getRepEntity().getRepositoryId();

				// set of the Repo Abbreviation for the build path
				uploadBean.getRepEntity().setRepositoryAbbreviation(
						getRepositoryDAO().findRepoById(repInfoId).getRepositoryAbbreviation());

				// set of the GPlaceId from the repo Id for the build path
				gPlaceId = getRepositoryDAO().findRepoById(repInfoId).getLocation();
			}

			// Gestione Collection
			// Collection is new
			if (uploadBean.getCollEntity().getCollectionId() == null) {

				uploadBean.getCollEntity().setRepositoryId(repInfoId);

				// if a collection with same repository and collectionName not
				// exist in DB
				if (getCollectionDAO().checkCollectionByName(uploadBean.getCollEntity().getCollectionName(),
						uploadBean.getCollEntity().getRepositoryId()).size() == 0) {

					// Automatic creation of collection abbreviation
					StringBuilder abbrev = new StringBuilder();
					String collName = uploadBean.getCollEntity().getCollectionName();
					String[] nameList = collName.split(" ");
					for (String str : nameList) {
						abbrev.append(str.substring(0, 1).toUpperCase());
					}

					// If an abbreviation with same repository and same
					// collection already exist, add a _1
					Integer size = getCollectionDAO().findNumberOfCollectionAbbreviation(abbrev.toString(),
							uploadBean.getCollEntity().getRepositoryId());
					if (size != 0) {
						abbrev.append(size);
					}

					// Set of abbreviation
					uploadBean.getCollEntity().setCollectionAbbreviation(abbrev.toString());

					// Collection not exist, insert in DB
					collInfoId = getCollectionDAO().insertCollection(uploadBean.getCollEntity());
				} else {
					// a collection with the same name and repoId already exist
					// in DB, get the collId and the abbreviation and set as
					// values for upload
					collInfoId = getCollectionDAO()
							.checkCollectionByName(uploadBean.getCollEntity().getCollectionName(),
									uploadBean.getCollEntity().getRepositoryId())
							.get(0).getCollectionId();
					uploadBean.getCollEntity().setCollectionAbbreviation(
							getCollectionDAO().findAbbrevById(collInfoId).getCollectionAbbreviation());
				}

			} else {
				// Collection is not new
				collInfoId = uploadBean.getCollEntity().getCollectionId();

				// set of the Coll. Abbreviation for the build path
				uploadBean.getCollEntity().setCollectionAbbreviation(
						getCollectionDAO().findAbbrevById(collInfoId).getCollectionAbbreviation());
			}

			// Gestione Series
			if (uploadBean.getSeries().getSeriesId() == null) {
				// Collection is new
				uploadBean.getSeries().setCollection(collInfoId);

				// Validation of Series before insert
				// criteria: Cannot be two Series with the same Title and
				// Collection ID
				List<SeriesEntity> listByTitleCollId = getSeriesDAO().findMatchSeriesByTitle(
						uploadBean.getSeries().getTitle(), uploadBean.getSeries().getCollection());
				if (listByTitleCollId.size() != 0) {
					// Series already exist in DB, avoid duplicate using the Id
					// of the existing one
					SeriesEntity ser = listByTitleCollId.get(0);
					serInfoId = ser.getSeriesId();
				} else {
					// Series not exist, insert in DB
					serInfoId = getSeriesDAO().insertSeries(uploadBean.getSeries());
				}

			} else {
				serInfoId = uploadBean.getSeries().getSeriesId();
			}

			// Gestione Volume
			if (uploadBean.getVolume().getSummaryId() == null) {
				// Volume is new
				if (uploadBean.getVolume().getVolume() != null && !uploadBean.getVolume().getVolume().isEmpty()) {
					uploadBean.getVolume().setCollection(collInfoId);

					// Validation of Volume before insert
					// criteria: Cannot be two Volume with the same Name and
					// Collection ID
					List<Volume> listByNameCollId = getVolumeDAO().findMatchVolumeByName(
							uploadBean.getVolume().getVolume(), uploadBean.getVolume().getCollection());
					if (listByNameCollId.size() != 0) {
						// Volume already exist in DB, avoid duplicate using the
						// Id
						// of the existing one
						Volume vol = listByNameCollId.get(0);
						volInfoId = vol.getSummaryId();
					} else {
						// Volume not exist, insert in DB but control also
						// volume
						// field
						volInfoId = getVolumeDAO().insertVolume(uploadBean.getVolume());
					}
				}

			} else {
				volInfoId = uploadBean.getVolume().getSummaryId();

				// set of the Volume Abbreviation for the build path
				uploadBean.getVolume().setVolume(getVolumeDAO().find(volInfoId).getVolume());
			}

			// Gestione Insert
			if (uploadBean.getInsert().getInsertId() == null) {
				// Insert is new
				if (uploadBean.getInsert().getInsertName() != null
						&& !uploadBean.getInsert().getInsertName().isEmpty()) {
					uploadBean.getInsert().setVolume(volInfoId);

					// Validation of Insert before insert in DB
					// criteria: Cannot be two Insert with the same Name and
					// Volume
					// ID
					List<InsertEntity> listByNameVolId = getInsertDAO().findMatchInsertByName(
							uploadBean.getInsert().getInsertName(), uploadBean.getInsert().getVolume());
					if (listByNameVolId.size() != 0) {
						// Insert already exist in DB, avoid duplicate using the
						// Id
						// of the existing one
						InsertEntity ins = listByNameVolId.get(0);
						insInfoId = ins.getInsertId();
					} else {

						// Insert not exist, insert in DB
						insInfoId = getInsertDAO().insertInsert(uploadBean.getInsert());
					}
				}

			} else {
				insInfoId = uploadBean.getInsert().getInsertId();
			}

			// Insert to tblUpload
			UploadInfoEntity uploadInfo = new UploadInfoEntity("", uploadBean.getAeDescription(), repInfoId, collInfoId,
					serInfoId, volInfoId, insInfoId, uploadBean.getOwner(), new Date(), new Date(), 0, null, 0);

			uploadInfoId = getUploadInfoDAO().insertUplodInfo(uploadInfo);

			String aeName = getAEName(uploadBean.getAeDescription());
			User user = getUserService()
					.findUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
							.getUsername());

			getUploadInfoDAO().updateUpload(uploadInfoId, aeName, user.getAccount());

			// Build of the real path
			realPath = org.medici.mia.common.util.FileUtils.getRealPath(gPlaceId,
					uploadBean.getRepEntity().getRepositoryAbbreviation(),
					uploadBean.getCollEntity().getCollectionAbbreviation(), uploadBean.getVolume().getVolume(),
					uploadBean.getInsert().getInsertName());

			uploadBean.setRealPath(realPath);
			uploadBean.setUploadInfoId(uploadInfoId);

			roolbackList = insertFilestoFileSystem(uploadBean);

			getUploadFileDAO().insertUploadFiles(uploadBean.getUploadFileEntities());
			
			
			if (uploadBean.getFolioEntities() != null) {
				if (uploadBean.getFolioEntities().size() == uploadBean.getUploadFileEntities().size()) {
					for(int i = 0; i< uploadBean.getFolioEntities().size()  ; i++) {
						uploadBean.getFolioEntities().get(i).setUploadedFileId(uploadBean.getUploadFileEntities().get(i).getUploadFileId());
					}
					
					folioDao.insertFolios(uploadBean.getFolioEntities());
					
					if (uploadBean.getFolioEntities().size() >= 1) {
						String firstFolio = "";
						String lastFolio = "";
						if (uploadBean.getFolioEntities().size() == 1) {
							firstFolio = uploadBean.getFolioEntities().get(0).getFolioNumber();
							lastFolio = firstFolio;
						} else if (uploadBean.getFolioEntities().size() > 1) {
							firstFolio = uploadBean.getFolioEntities().get(0).getFolioNumber();
							lastFolio = uploadBean.getFolioEntities().get(uploadBean.getFolioEntities().size() - 1).getFolioNumber();
						}
						String collAbbr = uploadBean.getCollEntity().getCollectionAbbreviation();
						String volNumberF = uploadBean.getVolume().getVolume();
						String newTitle = collAbbr + "-" + volNumberF + " from folio " + firstFolio + " to folio " + lastFolio + "(imported from Bia)";
	
						getUploadInfoDAO().updateUpload(uploadInfoId, newTitle, user.getAccount());
					}

				}
				
			}

			deleteTempFiles();

		} catch (Throwable th) {
			if (roolbackList != null) {
				for (Iterator iterator = roolbackList.iterator(); iterator
						.hasNext();) {
					List<String> list = (List<String>) iterator.next();
					deleteFilesRoolBack(list);
				}
			}

			throw new ApplicationThrowable(th);
		}
		return uploadBean;

	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public void SaveSharedUsers(List<String> accounts, Integer fileId, Integer uploadId, HttpServletRequest request) throws ApplicationThrowable {
		try {
			List<UploadFileEntity> listFile = null;
			if(fileId == null) {
				listFile = getUploadFileDAO().findUploadFilesByUploadInfoId(uploadId);
			} else {
				listFile = new ArrayList<UploadFileEntity>();
				UploadFileEntity file = uploadFileDAO.find(fileId);
				listFile.add(file);
			}
			List<String> accountsOld = new ArrayList<String>();
			if(listFile.size() > 0){
				List<User> sharedUsersOld = fileSharingDAO.findSharedUsersByUpdateId(listFile.get(0).getUploadInfoEntity().getUploadInfoId());
				for(User u: sharedUsersOld){
					accountsOld.add(u.getAccount());
				}
			}
			for(int i = 0; i < listFile.size(); i++) {
				fileSharingDAO.deleteSharedUsers(uploadId, listFile.get(i).getUploadFileId());
			}
			final String username = userService.findUser(
	                ((UserDetails) SecurityContextHolder.getContext()
	                        .getAuthentication().getPrincipal()).getUsername()).getAccount();
			if(accounts.size() > 0 || accounts.isEmpty() == false) {
				UploadInfoEntity upload = getUploadInfoDAO().find(uploadId);
				for(int c = 0; c < accounts.size(); c++) {
					User user = getUserDAO().findUser(accounts.get(c)); 
					for(int i = 0; i < listFile.size(); i++) {
						FileSharing fileSharing = new FileSharing(upload, listFile.get(i), user);
						fileSharingDAO.saveSharedUsers(fileSharing);
					}
					sendMessageSharedUpload(user, username, request, uploadId);
				}
			}
			boolean equal = true;
			if(accounts.size() == accountsOld.size()) {
				for (int i = 0; i < accounts.size(); i++) {
					if (!accounts.get(i).equals(accountsOld.get(i))) {
						equal = false;
					}
				}
			} else {
				equal = false;
			}
			if(!equal){
				HashMap<String, Object> after = new HashMap<String, Object>();
				HashMap<String, Object> before = new HashMap<String, Object>();
				HashMap<String, Object> changes = new HashMap<String, Object>();
				HashMap<String, Object> action = new HashMap<String, Object>();
				HashMap<String, Object> category = new HashMap<String, Object>();
				after.put("accounts", accounts);
				before.put("accounts", accountsOld);

				changes.put("before", before);
				changes.put("after", after);
				action.put("edit", changes);
				category.put("shareAE", action);

				historyLogService.registerAction(
						uploadId, HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.AE, null, category);
			}
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public void SaveSharedUsersWithDocumentId(List<String> accounts, Integer documentId, Boolean replaceSharedUsers, HttpServletRequest request) throws ApplicationThrowable {
		try {
			List<UploadFileEntity> listFile = null;
			MiaDocumentEntity document = getDocumentDAO().find(documentId);
			if(document != null) {
				listFile = document.getUploadFileEntities();
				List<String> accountsOld = new ArrayList<String>();
				if(listFile.size() > 0){
					List<User> sharedUsersOld = fileSharingDAO.findSharedUsersByUpdateId(listFile.get(0).getUploadInfoEntity().getUploadInfoId());
					for(User u: sharedUsersOld){
						accountsOld.add(u.getAccount());
					}
				}
				if(replaceSharedUsers == true) {
					for(int i = 0; i < listFile.size(); i++) {
						fileSharingDAO.deleteSharedUsers(listFile.get(i).getIdTblUpload(), listFile.get(i).getUploadFileId());
					}
				}

				final String username = userService.findUser(
		                ((UserDetails) SecurityContextHolder.getContext()
		                        .getAuthentication().getPrincipal()).getUsername()).getAccount();
				
				if(accounts.size() > 0 || accounts.isEmpty() == false) {
					for(int c = 0; c < accounts.size(); c++) {
						User user = getUserDAO().findUser(accounts.get(c)); 
						for(int i = 0; i < listFile.size(); i++) {
							UploadInfoEntity upload = getUploadInfoDAO().find(listFile.get(i).getIdTblUpload());
							FileSharing fileSharing = new FileSharing(upload, listFile.get(i), user);
							fileSharingDAO.saveSharedUsers(fileSharing);
							//Send confirm shared message
						}
						sendMessageSharedDocument(user, username,request, documentId);
					}
				}
				
				boolean equal = true;
				if(accounts.size() == accountsOld.size()) {
					for (int i = 0; i < accounts.size(); i++) {
						if (!accounts.get(i).equals(accountsOld.get(i))) {
							equal = false;
						}
					}
				} else {
					equal = false;
				}
				if(!equal){
					HashMap<String, Object> after = new HashMap<String, Object>();
					HashMap<String, Object> before = new HashMap<String, Object>();
					HashMap<String, Object> changes = new HashMap<String, Object>();
					HashMap<String, Object> action = new HashMap<String, Object>();
					HashMap<String, Object> category = new HashMap<String, Object>();
					after.put("accounts", accounts);
					before.put("accounts", accountsOld);

					changes.put("before", before);
					changes.put("after", after);
					action.put("edit", changes);
					category.put("shareDocument", action);

					historyLogService.registerAction(
							documentId, HistoryLogEntity.UserAction.EDIT,
							HistoryLogEntity.RecordType.DE, null, category);
				}
			}
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}
	
	
	//Send confirm shared message for document
	private void sendMessageSharedDocument(User user, String currentUsername, HttpServletRequest request, Integer documentId ) {
        MessageSendJson json = new MessageSendJson();
        String docUrl = request.getScheme()+"://" +request.getServerName()+":" +request.getServerPort()+"/Mia/index.html#/mia/document-entity/"+ documentId;
        json.setAccount(currentUsername);
        json.setFrom(currentUsername);
        json.setTo(user.getAccount());
        json.setMessageSubject("CONTENT SHARING");
        json.setMessageText("User `"+currentUsername+"` shared a document.<br>"
                + "The document can be found here: <br/><a href="+ docUrl +">"+ docUrl +" </a><br/><br/>");
        messageService.sendNotification(user, json);
	}
	
	//Send confirm shared message for upload
	private void sendMessageSharedUpload(User user, String currentUsername, HttpServletRequest request, Integer uploadId ) {
        MessageSendJson json = new MessageSendJson();
        String aeUrl = request.getScheme()+"://" +request.getServerName()+":" +request.getServerPort()+"/Mia/index.html#/mia/archival-entity/"+ uploadId +"?page=1";
        json.setAccount(currentUsername);
        json.setFrom(currentUsername);
        json.setTo(user.getAccount());
        json.setMessageSubject("CONTENT SHARING");
        json.setMessageText("User `"+currentUsername+"` has shared an upload.<br>"
                + "The upload can be found here: <br/><a href="+ aeUrl +">"+ aeUrl +" </a><br/><br/>");
        messageService.sendNotification(user, json);
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	@Override
	public List<User> findSharedUsers(Integer uploadId, Integer fileId) 
			throws ApplicationThrowable {
		try {
			List<FileSharing> filesSharing = fileSharingDAO.getByUploadAndFile(uploadId, fileId);
			List<User> accounts = new ArrayList<User>();
			for(int c = 0; c < filesSharing.size(); c++) {
				accounts.add(filesSharing.get(c).getUser());
			}
			return accounts;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	@Override
	public List<Integer> findSharedImages(Integer uploadId) 
			throws ApplicationThrowable {
		try {
			List<UploadFileEntity> images = fileSharingDAO.findSharedByUpdateId(uploadId);
			List<Integer> imagesIds = new ArrayList<Integer>();
			for(int c = 0; c < images.size(); c++) {
				imagesIds.add(images.get(c).getUploadFileId());
			}
			return imagesIds;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	@Override
	public List<User> findSharedUsersByDocumentId(Integer documentId) throws ApplicationThrowable {
		try {
			MiaDocumentEntity document = getDocumentDAO().find(documentId);
			List<User> accounts = fileSharingDAO.findSharedUsersByUpdateId(document.getUploadFileEntities().get(0).getIdTblUpload());
			return accounts;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	@Override
	public List<User> findSharedUsersByUpdateId(Integer uploadId) 
			throws ApplicationThrowable {
		try {
			List<User> accounts = fileSharingDAO.findSharedUsersByUpdateId(uploadId);
			return accounts;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	private InsertEntity findFirstInsertInVolume(Integer volumeId) {
		List<InsertEntity> inserts = findInsertByName("", volumeId);
		if(inserts != null && !inserts.isEmpty()){
			for(InsertEntity insert : inserts){
				if(insert.getAeCount() > 0) return insert;
			}
			return inserts.get(0);
		}
		return null;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public UploadBean insertUploadOptionalFile(UploadBean uploadBean) throws ApplicationThrowable {
		String realPath = "";
		if (!tmpDir.isDirectory()) {
			logger.error("ATTENTION: The temp directory doesn't exist!");
			throw new ApplicationThrowable();
		}

		if (uploadBean.getRepEntity() == null) {
			logger.error("ATTENTION: The Repository entity not be null.");
			throw new ApplicationThrowable();
		}

		try {

			Integer repoIdNum = Integer.valueOf(uploadBean.getRepEntity().getRepositoryId());
			uploadBean.setRepEntity(getRepositoryDAO().findRepoById(repoIdNum));

			String gPlaceId = uploadBean.getRepEntity().getLocation();

			Integer insInfoId = null;
			Integer uploadInfoId = null;

			if (uploadBean.getOptionalImage() == "spine") {
				Integer volumeIdNum = Integer.valueOf(uploadBean.getVolume().getSummaryId());
				Volume volEntity = volumeDAO.find(volumeIdNum);
				uploadBean.setVolume(volEntity);
				uploadBean.setCollEntity(volEntity.getCollectionEntity());
				if(volEntity.getInsertEntities().size() > 0) {
					InsertEntity insert = findFirstInsertInVolume(volEntity.getSummaryId());
					if (insert != null) {
						insInfoId = insert.getInsertId();
						uploadBean.setInsert(insert);
					}
				}
			} else if (uploadBean.getOptionalImage() == "guardia") {
				Integer insertIdNum = Integer.valueOf(uploadBean.getInsert().getInsertId());
				InsertEntity insEntity = insertDAO.find(insertIdNum);
				uploadBean.setInsert(insEntity);
				Volume volEntity = volumeDAO.find(insEntity.getVolume());
				uploadBean.setVolume(volEntity);
				uploadBean.setCollEntity(volEntity.getCollectionEntity());
				insInfoId = insEntity.getInsertId();
			}

			List<UploadInfoEntity> updateLogicalDeleteList = null;
			if (uploadBean.getOptionalImage() == "spine") {
				updateLogicalDeleteList = getUploadInfoDAO().getUpdateByVolumeId(uploadBean.getVolume().getSummaryId());
			} else if (uploadBean.getOptionalImage() == "guardia") {
				updateLogicalDeleteList = getUploadInfoDAO().getUpdateByInsertId(uploadBean.getInsert().getInsertId());
			}

			if (updateLogicalDeleteList.size() > 0) {
				for (UploadInfoEntity u : updateLogicalDeleteList) {
					u.setLogicalDelete(1);
					u.setDateDeleted(new Date());
					getUploadInfoDAO().persist(u);
				}
			}

			// Insert to tblUpload
			UploadInfoEntity uploadInfo = new UploadInfoEntity("", uploadBean.getAeDescription(),
					uploadBean.getRepEntity().getRepositoryId(), uploadBean.getCollEntity().getCollectionId(), null,
					uploadBean.getVolume().getSummaryId(), insInfoId, uploadBean.getOwner(), new Date(), new Date(), 0,
					null, 0, uploadBean.getOptionalImage());

			uploadInfoId = getUploadInfoDAO().insertUplodInfo(uploadInfo);

			String aeName = getAEName(uploadBean.getAeDescription());
			User user = getUserService()
					.findUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
							.getUsername());

			getUploadInfoDAO().updateUpload(uploadInfoId, aeName, user.getAccount());

			// Build of the real path
			realPath = null;
			if (uploadBean.getOptionalImage() == "spine" && uploadBean.getInsert() == null) {
				realPath = org.medici.mia.common.util.FileUtils.getRealPath(gPlaceId,
						uploadBean.getRepEntity().getRepositoryAbbreviation(),
						uploadBean.getCollEntity().getCollectionAbbreviation(), uploadBean.getVolume().getVolume(), "");
			} else if (uploadBean.getOptionalImage() == "guardia" || uploadBean.getInsert() != null) {
				realPath = org.medici.mia.common.util.FileUtils.getRealPath(gPlaceId,
						uploadBean.getRepEntity().getRepositoryAbbreviation(),
						uploadBean.getCollEntity().getCollectionAbbreviation(), uploadBean.getVolume().getVolume(),
						uploadBean.getInsert().getInsertName());
			}

			uploadBean.setRealPath(realPath);
			uploadBean.setUploadInfoId(uploadInfoId);

			insertFilestoFileSystem(uploadBean);
			getUploadFileDAO().insertUploadFiles(uploadBean.getUploadFileEntities());

			deleteTempFiles();
			UploadFileEntity file = uploadBean.getUploadFileEntities().get(0);
			FolioEntity folio = new FolioEntity();
			folio.setFolioNumber(uploadBean.getOptionalImage() == null ? null : uploadBean.getOptionalImage().toUpperCase());
			folio.setNoNumb(false);
			folio.setUploadedFileId(file.getUploadFileId());
			folio.setUploadFileEntity(file);
			folioDao.insertFolio(folio);
			file.setFolioEntities(new ArrayList<FolioEntity>());
			file.getFolioEntities().add(folio);

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
		return uploadBean;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public UploadBean addMyArchiveFiles(List<MultipartFile> files, Integer uploadInfoId, boolean showInHistory) throws ApplicationThrowable {

		String realPath = "";
		UploadBean uploadBean = null;

		if (!tmpDir.isDirectory()) {
			logger.error("ATTENTION: The temp directory doesn't exist!");
			throw new ApplicationThrowable();
		}

		try {

			UploadInfoEntity uploadInfo = getUploadInfoDAO().getUploadById(uploadInfoId);

			if (uploadInfo == null)
				return null;
			uploadBean = new UploadBean();
			uploadBean.setRepEntity(uploadInfo.getRepositoryEntity());
			uploadBean.setCollEntity(uploadInfo.getCollectionEntity());
			uploadBean.setOwner(uploadInfo.getOwner());
			// If we need in the future insert volume ect ect we can set it

			// Build of the real path
			realPath = org.medici.mia.common.util.FileUtils.getRealPath(
					uploadInfo.getRepositoryEntity().getGoogleLocation().getgPlaceId(),
					uploadInfo.getRepositoryEntity().getRepositoryAbbreviation(),
					uploadInfo.getCollectionEntity().getCollectionAbbreviation(),
					uploadInfo.getVolumeEntity().getVolume(), uploadInfo.getInsertEntity() == null ? null : uploadInfo.getInsertEntity().getInsertName());

			uploadBean.setRealPath(realPath);
			uploadBean.setUploadInfoId(uploadInfoId);
			uploadBean.setFiles(files);

			List<UploadFileEntity> uploadFileEntities = uploadInfo.getUploadFileEntities();
			// Start section:
			// The imagerOrder for the new file needs to be evalute for all the
			// files with imageOrder != -1
			Integer count = new Integer(0);
			for (UploadFileEntity uploadFile : uploadFileEntities) {
				if (uploadFile.getImageOrder() != -1)
					count++;
			}
			// End Section
			if (uploadFileEntities != null && !uploadFileEntities.isEmpty()) {
				insertFilestoFileSystem(uploadBean, count + 1);
			} else {
				insertFilestoFileSystem(uploadBean);
			}
			getUploadFileDAO().insertUploadFiles(uploadBean.getUploadFileEntities());
			// Get the username logged in
			User user = getUserService()
					.findUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
							.getUsername());

			getUploadInfoDAO().updateDateModified(uploadInfoId, user.getAccount());

			deleteTempFiles();

			if (showInHistory) {
				HashMap<String, Object> changes = new HashMap<String, Object>();
				HashMap<String, HashMap> category = new HashMap<String, HashMap>();
				HashMap<String, HashMap> action = new HashMap<String, HashMap>();
				changes.put("files", files.size());
				action.put("add", changes);
				category.put("uploadFile", action);
				historyLogService.registerAction(
						uploadInfoId, HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.AE, null, category);
			}



		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
		return uploadBean;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<RepositoryEntity> findRepositoryByName(String repositoryName) throws ApplicationThrowable {
		try {
			return getRepositoryDAO().findByMatchingName(repositoryName);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public RepositoryEntity findRepositoryById(Integer repositoryId) throws ApplicationThrowable {
		try {
			return getRepositoryDAO().findRepoById(repositoryId);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<RepositoryEntity> checkRepositoryByNameLocation(String repositoryName, String repositoryLocation)
			throws ApplicationThrowable {
		try {
			return getRepositoryDAO().checkByMatchingNameLocation(repositoryName, repositoryLocation);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<RepositoryEntity> checkRepositoryByLocationAbbreviation(String repositoryLocation,
			String repositoryAbbreviation) throws ApplicationThrowable {
		try {
			return getRepositoryDAO().checkByMatchingLocationAbbreviation(repositoryLocation, repositoryAbbreviation);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<CollectionEntity> findCollectionsByName(String collectionName, Integer repositoryId)
			throws ApplicationThrowable {
		try {
			return getCollectionDAO().findCollectionByName(collectionName, repositoryId);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<CollectionEntity> findAllCollections(Integer repositoryId) throws ApplicationThrowable {
		try {
			return getCollectionDAO().findAllCollections(repositoryId);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<CollectionEntity> checkCollectionsByName(String collectionName, Integer repositoryId)
			throws ApplicationThrowable {
		try {
			return getCollectionDAO().checkCollectionByName(collectionName, repositoryId);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	public List<SeriesEntity> findSeriesByTitle(String seriesTitle, Integer collectionId) throws ApplicationThrowable {

		try {
			return getSeriesDAO().findSeriesByTitle(seriesTitle, collectionId);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	public List<SeriesEntity> findAllSeries(Integer collectionId) throws ApplicationThrowable {

		try {
			return getSeriesDAO().findAllSeries(collectionId);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	public List<Volume> findVolumeByName(String volumeName, Integer collectionId) throws ApplicationThrowable {

		try {
			List<Volume> volumes = getVolumeDAO().findVolumeByName(volumeName, collectionId);
			Collections.sort(volumes, new VolumeComparator());
			return volumes;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	public List<Volume> findVolumeByName(String volumeName, Integer collectionId, Integer limit) throws ApplicationThrowable {

		try {
			List<Volume> volumes = getVolumeDAO().findVolumeByName(volumeName, collectionId, limit);
			Collections.sort(volumes, new VolumeComparator());
			return volumes;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	public List<Volume> findAllVolumes(Integer collectionId) throws ApplicationThrowable {

		try {
			return getVolumeDAO().findAllVolumes(collectionId);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	public List<InsertEntity> findInsertByName(String insertName, Integer volumeId) throws ApplicationThrowable {

		try {
			List<InsertEntity> inserts = getInsertDAO().findInsertByName(insertName, volumeId);
			Collections.sort(inserts, new InsertEntityComparator());
			return inserts;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	public List<InsertEntity> findAllInsers(Integer volumeId) throws ApplicationThrowable {

		try {
			return getInsertDAO().findAllInserts(volumeId);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	public List<InsertEntity> findAllInserts(Integer volumeId) throws ApplicationThrowable {

		try {
			return getInsertDAO().findAllInserts(volumeId);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	public InsertEntity findInsertById(Integer insertId) throws ApplicationThrowable {

		try {
			return getInsertDAO().findInsertById(insertId);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Override
	@Async
	// public Future<Boolean> convertFiles(
	public Future<List<GenericUploadResponseJson>> convertFiles(List<UploadFileEntity> uploadFileEntities,
			String filePath, boolean notCrop) {

		List<GenericUploadResponseJson> resList = new ArrayList<GenericUploadResponseJson>();

		if (uploadFileEntities == null || uploadFileEntities.isEmpty()) {
			return new AsyncResult<List<GenericUploadResponseJson>>(resList);
		}

		for (UploadFileEntity uploadFile : uploadFileEntities) {

			GenericUploadResponseJson respUpload = new GenericUploadResponseJson();
			respUpload.setFileName(uploadFile.getFilename());
			respUpload.setOriginalFileName(uploadFile.getFileOriginalName());
			respUpload.setFileId(uploadFile.getUploadFileId());

			uploadFile.setTiledConversionProcess(org.medici.mia.common.util.FileUtils.THUMBS_CONVERSION_INPROGRESS);

			// 1. Convert files to Thumb format
			GenericResponseJson retThumb = convertFilesToThumb(uploadFile, filePath, notCrop);

			respUpload.setMessage(retThumb.getMessage());
			respUpload.setStatus(retThumb.getStatus());

			if (StatusType.ok.toString().equalsIgnoreCase(retThumb.getStatus())) {
				uploadFile.setTiledConversionProcess(org.medici.mia.common.util.FileUtils.THUMBS_CONVERSION_SUCCESS);

				uploadFile.setTiledConversionProcess(org.medici.mia.common.util.FileUtils.TIFFS_CONVERSION_INPROGRESS);

				// 2. Convert files to PTIFF format
				GenericResponseJson retTiff = convertFilesToPTIFF(uploadFile, filePath);
				respUpload.setMessage(retTiff.getMessage());
				respUpload.setStatus(retTiff.getStatus());

				if (StatusType.ok.toString().equalsIgnoreCase(retTiff.getStatus())) {
					uploadFile.setTiledConversionProcess(org.medici.mia.common.util.FileUtils.TIFFS_CONVERSION_SUCCESS);
				} else {
					uploadFile.setTiledConversionProcess(org.medici.mia.common.util.FileUtils.TIFFS_CONVERSION_ERROR);
					deleteThumb(filePath, uploadFile.getFilename());
				}

			} else {
				uploadFile.setTiledConversionProcess(org.medici.mia.common.util.FileUtils.THUMBS_CONVERSION_ERROR);
				// TODO: -DONE- delete thumb file in error from directory
				deleteThumb(filePath, uploadFile.getFilename());
			}

			resList.add(respUpload);

			getUploadFileDAO().updateUploadFileConvertField(uploadFile.getUploadFileId(),
					uploadFile.getTiledConversionProcess());

		}

		return new AsyncResult<List<GenericUploadResponseJson>>(resList);

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public Integer replaceFiles(Integer oldUploadFileId, Integer newUploadFileId) {

		try {
			HashMap<String, Object> changes = new HashMap<String, Object>();
			HashMap<String, HashMap> category = new HashMap<String, HashMap>();
			HashMap<String, HashMap> action = new HashMap<String, HashMap>();
			HashMap<String, Object> before = new HashMap<String, Object>();
			HashMap<String, Object> after = new HashMap<String, Object>();
			before.put("fileId", oldUploadFileId);
			after.put("fileId", newUploadFileId);
			changes.put("before", before);
			changes.put("after", after);
			action.put("add", changes);
			category.put("replaceFile", action);

			UploadFileEntity uploadFileEntity = getUploadFileDAO().findUploadFile(oldUploadFileId);

			historyLogService.registerAction(
					uploadFileEntity.getUploadInfoEntity().getUploadInfoId(), HistoryLogEntity.UserAction.EDIT,
					HistoryLogEntity.RecordType.AE, null, category);

			return getUploadFileDAO().replaceFiles(oldUploadFileId, newUploadFileId);

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Override
	public GenericResponseJson rotateImages(List<Integer> uploadFileIds, RotateType rotateType) {

		try {

			boolean statusOk = true;
			GenericResponseJson resp = new GenericResponseJson();
			List<UploadFileEntity> files = new ArrayList<UploadFileEntity>();
			String realPath = null;

			User user = getUserService()
					.findUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
							.getUsername());
			Integer uploadInfoId = null;

			for (Integer fileId : uploadFileIds) {
				UploadFileEntity uploadFile = getUploadFileDAO().findUploadFile(fileId);

				if (uploadFile != null) {

					UploadInfoEntity upload = uploadFile.getUploadInfoEntity();
					uploadInfoId = upload.getUploadInfoId();
					// Issue 618 If the inserEntity has no value, insertName is null
					String insName = null;
					if (upload.getInsertEntity() != null) {
						insName = upload.getInsertEntity().getInsertName();
					}
					
					realPath = org.medici.mia.common.util.FileUtils.getRealPath(
							upload.getRepositoryEntity().getLocation(),
							upload.getRepositoryEntity().getRepositoryAbbreviation(),
							upload.getCollectionEntity().getCollectionAbbreviation(),
							upload.getVolumeEntity().getVolume(), insName);

					resp = rotateFile(uploadFile, realPath, rotateType);
					if (resp.getStatus().equalsIgnoreCase(StatusType.ko.toString())) {
						statusOk = false;
					} else {
						files.add(uploadFile);
						upload.setModifiedBy(user.getAccount());
						upload.setDateModified(new Date());
						getUploadInfoDAO().merge(upload);
					}
				} else {

					logger.error("The file with upoloFileId " + fileId + "doesn't exist.");
					statusOk = false;
				}
			}

			convertFiles(files, realPath, false);

			if (statusOk) {
				HashMap<String, Object> changes = new HashMap<String, Object>();
				HashMap<String, HashMap> category = new HashMap<String, HashMap>();
				HashMap<String, HashMap> action = new HashMap<String, HashMap>();
				changes.put("direction", rotateType.getValue());
				changes.put("files", uploadFileIds.size());
				action.put("edit", changes);
				category.put("rotateFile", action);

				if(uploadInfoId != null) {
					historyLogService.registerAction(uploadInfoId, HistoryLogEntity.UserAction.EDIT,
							HistoryLogEntity.RecordType.AE, null, category);
				}

				resp.setMessage("All Image or Images rotated " + rotateType.getValue() + " degrees clockwise.");
				resp.setStatus(StatusType.ok.toString());
			} else {
				resp.setMessage("Some Images could have problem with rotation of " + rotateType.getValue()
						+ " degrees clockwise. See log for details");
				resp.setStatus(StatusType.w.toString());
			}

			return resp;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}
	
	@Transactional(readOnly = false)
	@Override
	public CollectionEntity addCollection(CollectionJson collectionJson) {
		
		if (collectionJson.getCollectionName() != null &&
				collectionJson.getCollectionName().isEmpty()) {
				return null;
		}

		if (collectionJson.getCollectionAbbreviation() != null &&
				collectionJson.getCollectionAbbreviation().isEmpty()) {
				return null;
		}
		
		CollectionEntity collectionEntity = createCollection(collectionJson);
		if (collectionEntity == null) {
			return null;
		}
		
		getCollectionDAO().insertCollection(collectionEntity);
		
		return collectionEntity;
	}
	
	private CollectionEntity createCollection(CollectionJson collectionJson) {
		
		Integer repositoryId = collectionJson.getRepositoryId();
		if (repositoryId == null) {
			return null;
		}
		
		String collectionName = collectionJson.getCollectionName();
		
		if (checkIfCollectionExists(collectionName, repositoryId)) {
			return null;
		}	
		
		RepositoryEntity repositoryEntity = findRepositoryById(
				repositoryId);
		CollectionEntity collectionEntity = new CollectionEntity();
		
		collectionEntity.setCollectionName(collectionJson.getCollectionName());
		collectionEntity.setCollectionDescription(
				collectionJson.getCollectionDescription());
		collectionEntity.setCollectionAbbreviation(
				collectionJson.getCollectionAbbreviation());
		collectionEntity.setRepositoryEntity(repositoryEntity);
		collectionEntity.setRepositoryId(repositoryEntity.getRepositoryId());
		collectionEntity.setStudyCollection(false);
		
		return collectionEntity;
	}
	
	private boolean checkIfCollectionExists(
			String collectionName, 
			Integer repositoryId) {
		
		if (collectionName == null || collectionName.isEmpty()) {
			return false;
		}
		
		List<CollectionEntity> collections = findCollectionsByName(
				collectionName, 
				repositoryId);
		
		return (collections != null && !collections.isEmpty());
	}
	

	/**
	 * return file to delete for rever upload
	 */
	private List<List<String>> insertFilestoFileSystem(UploadBean uploadBean, int imageOrder) throws ApplicationThrowable {

		try {

			if (uploadBean.getFiles() != null && !uploadBean.getFiles().isEmpty()) {
				// 1. Copy Files to tmp dir
				copyFilesToTmpDir(uploadBean.getFiles());
				// 2. Copy files to real path
				return copyFilesToRealPath(uploadBean, imageOrder);
			}
			if (uploadBean.getUploadfiles() != null && !uploadBean.getUploadfiles().isEmpty()) {
				// 3. Copy files from batch bia/mia
				return copyUploadFilesToRealPath(uploadBean, imageOrder);
			}
			
			return null;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	private List<List<String>> insertFilestoFileSystem(UploadBean uploadBean) throws ApplicationThrowable {

		return insertFilestoFileSystem(uploadBean, 1);

	}

	private void copyFilesToTmpDir(List<MultipartFile> files) {

		try {

			for (MultipartFile file : files) {

				byte[] bytes = file.getBytes();
				// Imporant NOTE - To prevent unallowed filenames crossplatform
				// removing some characters
				String cleanedFileName = file.getOriginalFilename().replaceAll("[^A-Za-z0-9._-]", "");
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File(tmpDir + OS_SLASH + cleanedFileName)));
				stream.write(bytes);
				stream.close();
			}
		} catch (Exception e) {
			deleteTempFiles();
			logger.error(e.getMessage());
			throw new ApplicationThrowable(e);
		}

	}

	private GenericResponseJson convertFilesToThumb(UploadFileEntity uploadFile, String filePath, boolean notCrop) {
		// ret return true if the conversion is OK, otherwise false
		GenericResponseJson resp = new GenericResponseJson();
		resp.setMessage("Convert files to thums not started.");
		resp.setStatus(StatusType.ko.toString());
		if (filePath == null || filePath.isEmpty()) {
			logger.error("The real path where copying the converted files is empty");
			resp.setMessage("The real path where copying the converted files is empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;

		} else if (uploadFile == null) {
			logger.error("The list of uplod   entity is empty.");
			resp.setMessage("The list of uplod   entity is empty..");
			resp.setStatus(StatusType.ko.toString());
			return resp;

		}

		String installationType = ApplicationPropertyManager.getApplicationProperty("installation.type");

		// Getting the properties for conversion process

		// FOR DEVELOPMENT ENV - Windows or LINUX - ImageMagick SECTION
		String uploadConverterName = ApplicationPropertyManager
				.getApplicationProperty("imagemagick.convert.executable");
		String uploadConverterPath = ApplicationPropertyManager.getApplicationProperty("imagemagick.convert.path");
		String uploadConverter = uploadConverterPath + uploadConverterName;

		// Getting OS Specific Options
		String uploadConverterResize = null;
		if (SystemUtils.IS_OS_WINDOWS) {
			uploadConverterResize = "\"250x250^\"";
		} else if (SystemUtils.IS_OS_LINUX) {
			uploadConverterResize = "250x250^ ";
		} else {
			uploadConverterResize = "250x250^ ";
		}
		String uploadConverterGravity = "";
		String uploadConverterCrop = "250x250+0+0 ";
		
		// FOR PRODUCTION ENV - LINUX - VIPS SECTION
		String vipsConverterPath = ApplicationPropertyManager.getApplicationProperty("vips.convert.path");
		String vipsThumbnailName = ApplicationPropertyManager.getApplicationProperty("vips.thumbnail.executable");

		String vipsThumbConverter = vipsConverterPath + vipsThumbnailName;

		String sourceFile = filePath + OS_SLASH + org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR + OS_SLASH
				+ uploadFile.getFilename();
		String desFile = filePath + OS_SLASH + org.medici.mia.common.util.FileUtils.THUMB_FILES_DIR + OS_SLASH
				+ uploadFile.getFilename();

		// If image is double page (width > height) or single page (width <
		// height)
		File file = new File(sourceFile);
		BufferedImage bimg = null;
		try {
			bimg = ImageIO.read(file);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		if (bimg != null) {
			int width = bimg.getWidth();
			int height = bimg.getHeight();

			if (width > height) {
				uploadConverterGravity = "Center";
				logger.info("DOUBLEPAGE");
			} else {
				uploadConverterGravity = "NorthEast";
				logger.info("SINGLEPAGE");
			}

			// If the file format is of type TIFF use this process
			String extension = org.apache.commons.io.FilenameUtils.getExtension(uploadFile.getFilename());
			if (extension.equalsIgnoreCase("tif") || extension.equalsIgnoreCase("tiff")) {
				// convert to jpeg before to create the thumb

				desFile = org.apache.commons.io.FilenameUtils.removeExtension(desFile);
				desFile += ".jpg";
				String[] command_ary3 = {};
				String[] vipsResize_cmd = {};
				String[] command_ary2 = { uploadConverter, sourceFile, "-quality", "90", desFile };
				if(notCrop == false) {
					String[]  command_ary1 = { uploadConverter, sourceFile, "-resize", uploadConverterResize, "-gravity",
							uploadConverterGravity, "-crop", uploadConverterCrop, "+repage", desFile };
					command_ary3= command_ary1;
					String[] vipsResize_cmd1 = { vipsThumbConverter, sourceFile, "--smartcrop", "attention", "-s", "250x250",
							"-o", desFile };
					vipsResize_cmd = vipsResize_cmd1;
				} else if(notCrop == true){
					String[]  command_ary1 = { uploadConverter, sourceFile, "-resize", uploadConverterResize, "-gravity",
							uploadConverterGravity,"+repage", desFile };
					command_ary3= command_ary1;
					String[] vipsResize_cmd1 = { vipsThumbConverter, sourceFile,  "-s", "250x","-o", desFile };  
					vipsResize_cmd = vipsResize_cmd1;
					/*String[] command_ary3 = { uploadConverter, desFile, "-resize", uploadConverterResize, "-gravity",
							uploadConverterGravity, "-crop", uploadConverterCrop, "+repage", desFile };*/
				}

				// vipsthumbnail 0018_C_0597_R.tif --smartcrop attention -s
				// 250x250 -o prova2.jpg



				try {
					if (installationType.equals("development")) {
						logger.info(installationType);
						ProcessBuilder pb2 = new ProcessBuilder(command_ary2);
						logger.info("Command for Thumbs convert: " + pb2.command());
						pb2.redirectErrorStream(true);
						pb2.directory(new File(uploadConverterPath));

						Process p2;

						p2 = pb2.start();
						BufferedReader br2 = new BufferedReader(new InputStreamReader(p2.getInputStream()));
						String line2 = null;
						while ((line2 = br2.readLine()) != null) {
							System.out.println(line2);
						}

						ProcessBuilder pb3 = new ProcessBuilder(command_ary3);
						logger.info("Command for Thumbs convert: " + pb3.command());
						pb3.redirectErrorStream(true);
						pb3.directory(new File(uploadConverterPath));

						Process p3;

						p3 = pb3.start();
						BufferedReader br3 = new BufferedReader(new InputStreamReader(p3.getInputStream()));
						String line3 = null;
						while ((line3 = br3.readLine()) != null) {
							System.out.println(line3);

						}

						// waitForReturn give 0 if the convert process success
						// and 1
						// if the JPEG file missed
						int waitForReturn = p3.waitFor();
						System.out.println("1" + waitForReturn);

						if (waitForReturn == 0) {
							getUploadFileDAO().updateUploadFileConvertField(uploadFile.getUploadFileId(),
									org.medici.mia.common.util.FileUtils.THUMBS_CONVERSION_SUCCESS);
							// ret = true;
							resp.setMessage("File converted to thumbs successfully (from TIFF with ImageMagick)");
							resp.setStatus(StatusType.ok.toString());
						}
						// Close all the buffers to release all used resources
						br2.close();
						br3.close();
						// Rename the file in thumb to have the extension as
						// original
						// file (We changed it to use jpeg in convert thumb
						// process)
						File originFile = new File(desFile);
						if (originFile.exists()) {
							desFile = org.apache.commons.io.FilenameUtils.removeExtension(desFile);
							desFile += "." + extension;
							File destFile = new File(desFile);
							originFile.renameTo(destFile);
						}
					}
					if (installationType.equals("production")) {
						logger.info(installationType);

						ProcessBuilder pb4 = new ProcessBuilder(vipsResize_cmd);
						logger.info("Command for Thumbs convert: " + pb4.command());
						pb4.redirectErrorStream(true);
						pb4.directory(new File(uploadConverterPath));

						Process p4;

						p4 = pb4.start();
						BufferedReader br4 = new BufferedReader(new InputStreamReader(p4.getInputStream()));
						String line2 = null;
						while ((line2 = br4.readLine()) != null) {
							System.out.println(line2);
						}
						// waitForReturn give 0 if the convert process success
						// and 1
						// if the JPEG file missed
						int waitForReturn = p4.waitFor();
						System.out.println("1" + waitForReturn);

						if (waitForReturn == 0) {
							getUploadFileDAO().updateUploadFileConvertField(uploadFile.getUploadFileId(),
									org.medici.mia.common.util.FileUtils.THUMBS_CONVERSION_SUCCESS);
							// ret = true;
							resp.setMessage("File converted to thumbs successfully (from TIFF with VIPS)");
							resp.setStatus(StatusType.ok.toString());
						}
						// Close all the buffers to release all used resources
						br4.close();
						// Rename the file in thumb to have the extension as
						// original
						// file (We changed it to use jpeg in convert thumb
						// process)
						File originFile = new File(desFile);
						if (originFile.exists()) {
							desFile = org.apache.commons.io.FilenameUtils.removeExtension(desFile);
							desFile += "." + extension;
							File destFile = new File(desFile);
							originFile.renameTo(destFile);
						}
					}

				} catch (Exception e) {
					logger.error(e.getMessage());

				}
			}
			// For all the other type of format (JPEG) use this convert process
			// for
			// thumb ( the original thumb convert process)
			else {

				String[] command_ary = {};
				String[] vipsResize_cmd = {};
				if(notCrop == false) {
					String[]  command_ary1 = { uploadConverter, sourceFile, "-resize", uploadConverterResize, "-gravity",
							uploadConverterGravity, "-crop", uploadConverterCrop, "+repage", desFile };
					command_ary= command_ary1;
					String[] vipsResize_cmd1 = { vipsThumbConverter, sourceFile, "--smartcrop", "attention", "-s", "250x250",
							"-o", desFile };
					vipsResize_cmd = vipsResize_cmd1;
				} else if(notCrop == true){
					String[]  command_ary1 = { uploadConverter, sourceFile, "-resize", uploadConverterResize, "-gravity",
							uploadConverterGravity,"+repage", desFile };
					command_ary= command_ary1;
					String[] vipsResize_cmd1 = { vipsThumbConverter, sourceFile,  "-s", "250x","-o", desFile };  
					vipsResize_cmd = vipsResize_cmd1;
					/*String[] command_ary3 = { uploadConverter, desFile, "-resize", uploadConverterResize, "-gravity",
							uploadConverterGravity, "-crop", uploadConverterCrop, "+repage", desFile };*/
				}
				// vipsthumbnail 0018_C_0597_R.tif --smartcrop attention -s
				// 250x250 -o prova2.jpg

				/*String[] vipsResize_cmd = { vipsThumbConverter, sourceFile, "--smartcrop", "attention", "-s", "250x250",
						"-o", desFile };*/

				try {
					if (installationType.equals("development")) {
						logger.info(installationType);
						ProcessBuilder pb2 = new ProcessBuilder(command_ary);
						logger.info("Command for Thumbs convert: " + pb2.command());
						pb2.redirectErrorStream(true);
						pb2.directory(new File(uploadConverterPath));

						Process p2;

						p2 = pb2.start();
						BufferedReader br = new BufferedReader(new InputStreamReader(p2.getInputStream()));
						String line = null;
						while ((line = br.readLine()) != null) {
							System.out.println(line);
						}

						// waitForReturn give 0 if the convert process success
						// and 1
						// if the JPEG file missed
						int waitForReturn = p2.waitFor();
						System.out.println("1" + waitForReturn);
						if (waitForReturn == 0) {
							getUploadFileDAO().updateUploadFileConvertField(uploadFile.getUploadFileId(),
									org.medici.mia.common.util.FileUtils.THUMBS_CONVERSION_SUCCESS);
							// ret = true;
							resp.setMessage("File converted to thumbs successfully (with ImageMagick)");
							resp.setStatus(StatusType.ok.toString());
						}
						// Close all the buffers to release all used resources
						br.close();
					}
					if (installationType.equals("production")) {
						logger.info(installationType);
						ProcessBuilder pb4 = new ProcessBuilder(vipsResize_cmd);
						logger.info("Command for Thumbs convert: " + pb4.command());
						pb4.redirectErrorStream(true);
						pb4.directory(new File(uploadConverterPath));

						Process p4;

						p4 = pb4.start();
						BufferedReader br4 = new BufferedReader(new InputStreamReader(p4.getInputStream()));
						String line = null;
						while ((line = br4.readLine()) != null) {
							System.out.println(line);
						}

						// waitForReturn give 0 if the convert process success
						// and 1
						// if the JPEG file missed
						int waitForReturn = p4.waitFor();
						System.out.println("1" + waitForReturn);
						if (waitForReturn == 0) {
							getUploadFileDAO().updateUploadFileConvertField(uploadFile.getUploadFileId(),
									org.medici.mia.common.util.FileUtils.THUMBS_CONVERSION_SUCCESS);
							// ret = true;
							resp.setMessage("File converted to thumbs successfully (with VIPS)");
							resp.setStatus(StatusType.ok.toString());
						}
						// Close all the buffers to release all used resources
						br4.close();

					}

				} catch (Exception e) {
					logger.error(e.getMessage());

				}
			}
			// Close all the buffers to release all used resources
			bimg.flush();
		} else {
			logger.error("Can't read the file from ImageIO");
			resp.setMessage("Convert file to Thumbs Process: Reading from ImageIO ERROR.");
			resp.setStatus(StatusType.ko.toString());
		}

		return resp;
	}

	// private boolean convertFilesToPTIFF(UploadFileEntity uploadFile,
	// String filePath) {
	private GenericUploadResponseJson convertFilesToPTIFF(UploadFileEntity uploadFile, String filePath) {

		GenericUploadResponseJson resp = new GenericUploadResponseJson();

		if (uploadFile == null) {
			logger.error("The list of uplod files entity is empty.");
			resp.setMessage("The list of uplod files entity is empty.");
			resp.setStatus(StatusType.ko.toString());
			resp.setFileName("No Name");
			return resp;
		}
		resp.setFileName(uploadFile.getFilename());
		resp.setOriginalFileName(uploadFile.getFileOriginalName());
		resp.setFileId(uploadFile.getUploadFileId());

		if (filePath == null || filePath.isEmpty()) {
			logger.error("The real path where copying the converted files is empty");
			resp.setMessage("The real path where copying the converted files is empty");
			resp.setStatus(StatusType.ko.toString());

			return resp;
		}

		String installationType = ApplicationPropertyManager.getApplicationProperty("installation.type");

		// FOR DEVELOPMENT ENV - Windows or LINUX - ImageMagick SECTION
		String uploadConverterName = ApplicationPropertyManager
				.getApplicationProperty("imagemagick.convert.executable");
		String uploadConverterPath = ApplicationPropertyManager.getApplicationProperty("imagemagick.convert.path");
		String uploadConverter = uploadConverterPath + uploadConverterName;
		String uploadConverterTitleGeometry = "tiff:tile-geometry=128x128";
		String uploadConverterAttr = "jpeg";
		String sourceFile = filePath + OS_SLASH + org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR + OS_SLASH
				+ uploadFile.getFilename();
		String desFile = "ptif:" + filePath + FilenameUtils.removeExtension(uploadFile.getFilename()) + ".tif";
		String tmpFile = filePath + OS_SLASH + org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR + OS_SLASH
				+ "tmp_" + uploadFile.getFilename();

		// FOR PRODUCTION ENV - LINUX - VIPS SECTION
		String vipsConverterPath = ApplicationPropertyManager.getApplicationProperty("vips.convert.path");
		String vipsConverterName = ApplicationPropertyManager.getApplicationProperty("vips.convert.executable");
		String vipsThumbnailName = ApplicationPropertyManager.getApplicationProperty("vips.thumbnail.executable");

		String vipsConverter = vipsConverterPath + vipsConverterName;
		String vipsThumbConverter = vipsConverterPath + vipsThumbnailName;

		// Vips Options
		String vipsConverterFunction = "tiffsave";
		// String vipsSourceFile = filePath + OS_SLASH
		// + org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR
		// + OS_SLASH + uploadFile.getFilename();
		String vipsDesFile = OS_SLASH + filePath + FilenameUtils.removeExtension(uploadFile.getFilename()) + ".tif";
		String vipsTmpFile = filePath + OS_SLASH + org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR + OS_SLASH
				+ "tmp_" + uploadFile.getFilename();

		// In some cases (landscape vs portrait) we need to resize the image
		// before to convert to tif
		File file = new File(sourceFile);
		BufferedImage bimg = null;
		try {
			bimg = ImageIO.read(file);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		if (bimg != null) {
			int width = bimg.getWidth();
			int height = bimg.getHeight();
			// If image has landscape orientation (so width > height) and widht>
			// 2000) then resize the image
			if ((width > height) && (width > 2000)) {
				// Resize image to width to $uploadToResize constraining
				// proportions
				String uploadToResize = "2000";

				// Development Env Commands
				String[] imageMagickResizeWidth_cmd = { uploadConverter, sourceFile, "-resize", uploadToResize,
						tmpFile };
				String[] imageMagickCreatePyrTiff_cmd = { uploadConverter, tmpFile, "-define",
						uploadConverterTitleGeometry, "-compress", uploadConverterAttr, desFile };

				// Production Env Commands
				String[] vipsResize_cmd = { vipsThumbConverter, "-s", uploadToResize, sourceFile, "-o", vipsTmpFile };
				String[] vipsCreatePyrTiff_cmd = { vipsConverter, vipsConverterFunction, vipsTmpFile, vipsDesFile,
						"--compression", "jpeg", "--Q", "90", "--tile", "--pyramid" };

				try {
					if (installationType.equals("development")) {
						logger.info(installationType);
						ProcessBuilder pb2 = new ProcessBuilder(imageMagickResizeWidth_cmd);
						logger.info(
								"ImageMagick: resize image before creating Pyramid Tiled Tiff (double page with width > 2000px) "
										+ pb2.command());
						pb2.redirectErrorStream(true);
						pb2.directory(new File(uploadConverterPath));

						Process p2;

						p2 = pb2.start();
						BufferedReader br2 = new BufferedReader(new InputStreamReader(p2.getInputStream()));
						String line2 = null;
						while ((line2 = br2.readLine()) != null) {
							System.out.println(line2);
						}

						ProcessBuilder pb3 = new ProcessBuilder(imageMagickCreatePyrTiff_cmd);
						logger.info(installationType);
						logger.info("ImageMagick: Create Pyramid Tiled Tiff: " + pb3.command());
						pb3.redirectErrorStream(true);
						pb3.directory(new File(uploadConverterPath));

						Process p3;

						p3 = pb3.start();
						BufferedReader br3 = new BufferedReader(new InputStreamReader(p3.getInputStream()));
						String line3 = null;
						while ((line3 = br3.readLine()) != null) {
							System.out.println(line3);
						}

						int waitForReturn = p3.waitFor();
						System.out.println("2" + waitForReturn);
						if (waitForReturn == 0) {
							getUploadFileDAO().updateUploadFileConvertField(uploadFile.getUploadFileId(),
									org.medici.mia.common.util.FileUtils.TIFFS_CONVERSION_SUCCESS);
							resp.setMessage(
									"Development Env - ImageMagick - Pyramid Tiled Tiff conversion process finished successfully.");
							resp.setStatus(StatusType.ok.toString());
						}

						// Close all the buffers to release all used resources
						br2.close();
						br3.close();
					}

					if (installationType.equals("production")) {

						logger.info(installationType);
						ProcessBuilder pb2 = new ProcessBuilder(vipsResize_cmd);
						logger.info(
								"VIPS: resize image before creating Pyramid Tiled Tiff (double page with width > 2000px)"
										+ pb2.command());
						pb2.redirectErrorStream(true);
						pb2.directory(new File(uploadConverterPath));

						Process p2;

						p2 = pb2.start();
						BufferedReader br2 = new BufferedReader(new InputStreamReader(p2.getInputStream()));
						String line2 = null;
						while ((line2 = br2.readLine()) != null) {
							System.out.println(line2);
						}

						ProcessBuilder pb4 = new ProcessBuilder(vipsCreatePyrTiff_cmd);
						logger.info("VIPS: Create Pyramid Tiled Tiff:  " + pb4.command());
						pb4.redirectErrorStream(true);
						pb4.directory(new File(uploadConverterPath));

						Process p4;

						p4 = pb4.start();
						BufferedReader br4 = new BufferedReader(new InputStreamReader(p4.getInputStream()));
						String line4 = null;
						while ((line4 = br4.readLine()) != null) {
							System.out.println(line4);
						}

						int waitForReturn = p4.waitFor();
						System.out.println("2" + waitForReturn);
						if (waitForReturn == 0) {
							getUploadFileDAO().updateUploadFileConvertField(uploadFile.getUploadFileId(),
									org.medici.mia.common.util.FileUtils.TIFFS_CONVERSION_SUCCESS);
							resp.setMessage(
									"Production Env - VIPS - Pyramid Tiled Tiff conversion process finished successfully.");
							resp.setStatus(StatusType.ok.toString());
						}

						// Close all the buffers to release all used resources
						br2.close();
						br4.close();
					}
				} catch (Exception e) {
					logger.error(e.getMessage());
				}

				// If image has portrait orientation (so width < height) and
				// height > 2829) then resize the image
			} else if ((width < height) && (height > 2829)) {
				// Resize image to height to $uploadToResize constraining
				// proportions
				String uploadToResize = "x2829";

				// Development Env Commands
				String[] imageMagickResizeHeight_cmd = { uploadConverter, sourceFile, "-resize", uploadToResize,
						tmpFile };
				String[] imageMagickCreatePyrTiff_cmd = { uploadConverter, tmpFile, "-define",
						uploadConverterTitleGeometry, "-compress", uploadConverterAttr, desFile };

				// Production Env Commands
				String[] vipsResizeHeight_cmd = { vipsThumbConverter, "-s", uploadToResize, sourceFile, "-o",
						vipsTmpFile };
				String[] vipsCreatePyrTiff_cmd = { vipsConverter, vipsConverterFunction, vipsTmpFile, vipsDesFile,
						"--compression", "jpeg", "--Q", "90", "--tile", "--pyramid" };

				try {
					if (installationType.equals("development")) {

						logger.info(installationType);
						ProcessBuilder pb2 = new ProcessBuilder(imageMagickResizeHeight_cmd);
						logger.info(
								"ImageMagick - resize image before creating Pyramid Tiled Tiff (single page with height > 2829px):  "
										+ pb2.command());
						pb2.redirectErrorStream(true);
						pb2.directory(new File(uploadConverterPath));

						Process p2;

						p2 = pb2.start();
						BufferedReader br2 = new BufferedReader(new InputStreamReader(p2.getInputStream()));
						String line2 = null;
						while ((line2 = br2.readLine()) != null) {
							System.out.println(line2);
						}

						ProcessBuilder pb3 = new ProcessBuilder(imageMagickCreatePyrTiff_cmd);
						logger.info("ImageMagick: Create Pyramid Tiled Tiff: " + pb3.command());
						pb3.redirectErrorStream(true);
						pb3.directory(new File(uploadConverterPath));

						Process p3;

						p3 = pb3.start();
						BufferedReader br3 = new BufferedReader(new InputStreamReader(p3.getInputStream()));
						String line3 = null;
						while ((line3 = br3.readLine()) != null) {
							System.out.println(line3);
						}

						int waitForReturn = p3.waitFor();
						System.out.println("2" + waitForReturn);
						if (waitForReturn == 0) {
							getUploadFileDAO().updateUploadFileConvertField(uploadFile.getUploadFileId(),
									org.medici.mia.common.util.FileUtils.TIFFS_CONVERSION_SUCCESS);
							resp.setMessage(
									"Development Env - ImageMagick - Pyramid Tiled Tiff conversion process finished successfully.");
							resp.setStatus(StatusType.ok.toString());
						}

						// Close all the buffers to release all used resources
						br2.close();
						br3.close();
					}

					if (installationType.equals("production")) {

						logger.info(installationType);
						ProcessBuilder pb2 = new ProcessBuilder(vipsResizeHeight_cmd);
						logger.info(
								"VIPS - resize image before creating Pyramid Tiled Tiff (single page with height > 2829px):  "
										+ pb2.command());
						pb2.redirectErrorStream(true);
						pb2.directory(new File(uploadConverterPath));

						Process p2;

						p2 = pb2.start();
						BufferedReader br2 = new BufferedReader(new InputStreamReader(p2.getInputStream()));
						String line2 = null;
						while ((line2 = br2.readLine()) != null) {
							System.out.println(line2);
						}

						ProcessBuilder pb4 = new ProcessBuilder(vipsCreatePyrTiff_cmd);
						logger.info("VIPS: Create Pyramid Tiled Tiff: " + pb4.command());
						pb4.redirectErrorStream(true);
						pb4.directory(new File(uploadConverterPath));

						Process p4;

						p4 = pb4.start();
						BufferedReader br4 = new BufferedReader(new InputStreamReader(p4.getInputStream()));
						String line4 = null;
						while ((line4 = br4.readLine()) != null) {
							System.out.println(line4);
						}

						int waitForReturn = p4.waitFor();
						System.out.println("2" + waitForReturn);
						if (waitForReturn == 0) {
							getUploadFileDAO().updateUploadFileConvertField(uploadFile.getUploadFileId(),
									org.medici.mia.common.util.FileUtils.TIFFS_CONVERSION_SUCCESS);
							resp.setMessage(
									"Production Env - VIPS - Pyramid Tiled Tiff conversion process finished successfully.");
							resp.setStatus(StatusType.ok.toString());
						}

						// Close all the buffers to release all used resources
						br2.close();
						br4.close();
					}
				} catch (Exception e) {
					logger.error(e.getMessage());
				}
			} else {
				// Fai solo il processo di tif (senza resize)
				String[] command_ary = { uploadConverter, sourceFile, "-define", uploadConverterTitleGeometry,
						"-compress", uploadConverterAttr, desFile };
				String[] command_ary4 = { vipsConverter, vipsConverterFunction, sourceFile, vipsDesFile,
						"--compression", "jpeg", "--Q", "90", "--tile", "--pyramid" };

				try {
					if (installationType.equals("development")) {
						logger.info(installationType);
						logger.info("ImageMagick - Tiff conversion without resize");
						ProcessBuilder pb2 = new ProcessBuilder(command_ary);
						logger.info("ImageMagick: Create Pyramid Tiled Tiff: " + pb2.command());
						pb2.redirectErrorStream(true);
						pb2.directory(new File(uploadConverterPath));

						Process p2;

						p2 = pb2.start();
						BufferedReader br = new BufferedReader(new InputStreamReader(p2.getInputStream()));
						String line = null;
						while ((line = br.readLine()) != null) {
							System.out.println(line);
						}

						int waitForReturn = p2.waitFor();
						System.out.println("2" + waitForReturn);
						if (waitForReturn == 0) {
							getUploadFileDAO().updateUploadFileConvertField(uploadFile.getUploadFileId(),
									org.medici.mia.common.util.FileUtils.TIFFS_CONVERSION_SUCCESS);
							resp.setMessage(
									"Development Env - ImageMagick - Pyramid Tiled Tiff conversion process finished successfully.");
							resp.setStatus(StatusType.ok.toString());
						}
						// Close all the buffers to release all used resources
						br.close();
					}
					if (installationType.equals("production")) {
						logger.info(installationType);
						logger.info("Tiff conversion without resize");
						ProcessBuilder pb2 = new ProcessBuilder(command_ary4);
						logger.info("VIPS - resize image before creating Pyramid Tiled Tiff: " + pb2.command());
						pb2.redirectErrorStream(true);
						pb2.directory(new File(uploadConverterPath));

						Process p2;

						p2 = pb2.start();
						BufferedReader br = new BufferedReader(new InputStreamReader(p2.getInputStream()));
						String line = null;
						while ((line = br.readLine()) != null) {
							System.out.println(line);
						}

						int waitForReturn = p2.waitFor();
						System.out.println("2" + waitForReturn);
						if (waitForReturn == 0) {
							getUploadFileDAO().updateUploadFileConvertField(uploadFile.getUploadFileId(),
									org.medici.mia.common.util.FileUtils.TIFFS_CONVERSION_SUCCESS);
							resp.setMessage(
									"Production Env - VIPS - Pyramid Tiled Tiff conversion process finished successfully.");
							resp.setStatus(StatusType.ok.toString());
						}
						// Close all the buffers to release all used resources
						br.close();
					}
				} catch (Exception e) {
					logger.error(e.getMessage());
				}

			}

			// Close all the buffers to release all used resources
			bimg.flush();
		} else {
			resp.setMessage("ERROR - Convert file to Pyramid Tiled Tiff process: Error Reading from ImageIO.");
			resp.setStatus(StatusType.ko.toString());
			resp.setFileName(uploadFile.getFilename());
		}

		// Delete tmp file for resize if exists
		File filetmp = new File(tmpFile);
		if (filetmp.exists()) {
			filetmp.delete();
		}
		return resp;

	}

	private List<List<String>> copyFilesToRealPath(UploadBean uploadBean, int imageOrder) {
		// Implementation of the process Copy files from tmp directory to real
		// directory.
		// list of filenames copied - useful for revert in case of exception
		List<String> copiedFileNameList = new ArrayList<String>();
		
		List<List<String>> returnList = new ArrayList<List<String>>();
		returnList.add(copiedFileNameList);
		// make the directories:
		if (uploadBean.getFiles() == null || uploadBean.getFiles().isEmpty()) {
			logger.info("Attention: There are no files to be downloaded.");
			return null;
		}

		try {
			File destDir = new File(uploadBean.getRealPath());
			File destDirOriginalFile = new File(uploadBean.getRealPath() + OS_SLASH
					+ org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR + OS_SLASH);
			File destDirThumbs = new File(uploadBean.getRealPath() + OS_SLASH + "thumbs" + OS_SLASH);

			if (!destDir.exists()) {
				boolean success = destDir.mkdirs();
				if (success)
					logger.info("Directories: " + destDir.getPath() + " created");
			}
			if (!destDirOriginalFile.exists()) {
				boolean success = destDirOriginalFile.mkdirs();
				if (success)
					logger.info("Directories: " + destDirOriginalFile.getPath() + " created");
			}
			if (!destDirThumbs.exists()) {
				boolean success = destDirThumbs.mkdirs();
				if (success)
					logger.info("Directories: " + destDirThumbs.getPath() + " created");
			}

			List<UploadFileEntity> uploadFileEntities = new ArrayList<UploadFileEntity>();

			for (MultipartFile file : uploadBean.getFiles()) {

				String cleanedFileName = file.getOriginalFilename().replaceAll("[^A-Za-z0-9._-]", "");
				String originalFileName = cleanedFileName;
				String desFileName = cleanedFileName;

				File srcFile = new File(String.valueOf(ApplicationPropertyManager.getApplicationProperty("upload.path"))
						+ originalFileName);

				File destFile = new File(uploadBean.getRealPath() + OS_SLASH
						+ org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR + OS_SLASH + desFileName);

				while (destFile.exists()) {
					desFileName = "1_" + desFileName;
					destFile = new File(uploadBean.getRealPath() + OS_SLASH
							+ org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR + OS_SLASH + desFileName);
				}

				FileUtils.copyFile(srcFile, destFile);
				UploadFileEntity uploadFile = new UploadFileEntity(desFileName, cleanedFileName,
						uploadBean.getUploadInfoId(), new Date(), new Date(), new Integer(imageOrder), 0, null, 0, 0);

				imageOrder++;
				// list of filenames updated (in case of 1_ is added to the
				// filename)
				copiedFileNameList.add(desFileName);

				uploadFileEntities.add(uploadFile);
			}

			uploadBean.setUploadFileEntities(uploadFileEntities);
			
			return null;
		} catch (Exception e) {
			deleteFilesInRealPath(copiedFileNameList, uploadBean.getRealPath());
			deleteTempFiles();
			logger.error(e.getMessage());
			throw new ApplicationThrowable(e);
		}

	}
	
	private List<List<String>> copyUploadFilesToRealPath(UploadBean uploadBean, int imageOrder) {
		if (uploadBean.getUploadfiles() == null) {
			return null;
		}
		// list of filenames copied - useful for revert in case of exception
		List<String> copiedFileNameList = new ArrayList<String>();
		List<String> copiedFileNameTifList = new ArrayList<String>();
		List<String> copiedFileNameMiniList = new ArrayList<String>();
		List<List<String>> returnList = new ArrayList<List<String>>();
		returnList.add(copiedFileNameList);
		returnList.add(copiedFileNameTifList);
		returnList.add(copiedFileNameMiniList);
		
		// make the directories:
		if (uploadBean.getUploadfiles() == null || uploadBean.getUploadfiles().isEmpty()) {
			logger.info("Attention: There are no files to be downloaded.");
			return null;
		}

		try {
			File destDir = new File(uploadBean.getRealPath());
			File destDirOriginalFile = new File(uploadBean.getRealPath() + OS_SLASH
					+ org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR + OS_SLASH);
			File destDirThumbs = new File(uploadBean.getRealPath() + OS_SLASH + "thumbs" + OS_SLASH);

			if (!destDir.exists()) {
				boolean success = destDir.mkdirs();
				if (success)
					logger.info("Directories: " + destDir.getPath() + " created");
			}
			if (!destDirOriginalFile.exists()) {
				boolean success = destDirOriginalFile.mkdirs();
				if (success)
					logger.info("Directories: " + destDirOriginalFile.getPath() + " created");
			}
			if (!destDirThumbs.exists()) {
				boolean success = destDirThumbs.mkdirs();
				if (success)
					logger.info("Directories: " + destDirThumbs.getPath() + " created");
			}

			List<UploadFileEntity> uploadFileEntities = new ArrayList<UploadFileEntity>();
			List<FolioEntity> folioEntities = new ArrayList<FolioEntity>();

			for (File file : uploadBean.getUploadfiles()) {

				
				File srcFileTif = file;
				File srcFileOriginal = file;
				File srcFileMini = file;

				String file4 = file.getParentFile().getAbsolutePath() + OS_SLASH + 
							org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR
							+  OS_SLASH + file.getName().replaceFirst(".tif", ".jpg");
				srcFileOriginal = new File(file4) ;
				
				file4 = file.getParentFile().getAbsolutePath() + OS_SLASH + 
						org.medici.mia.common.util.FileUtils.THUMB_FILES_DIR
						+  OS_SLASH + file.getName().replaceFirst(".tif", ".jpg");
				srcFileMini = new File(file4);
			
				String cleanedFileName = srcFileOriginal.getName().replaceAll("[^A-Za-z0-9._-]", "");
				String originalFileName = cleanedFileName;
				String desFileName = cleanedFileName;
				String desFileNameTif =desFileName.replaceFirst(".jpg", ".tif");
		    	

		

				File destFile = new File(uploadBean.getRealPath() + OS_SLASH
						+ org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR + OS_SLASH + desFileName);

				File destFileTif = new File(uploadBean.getRealPath() + OS_SLASH + OS_SLASH + desFileNameTif);

				File destFileMini = new File(uploadBean.getRealPath() + OS_SLASH
						+ org.medici.mia.common.util.FileUtils.THUMB_FILES_DIR + OS_SLASH + desFileName);

				
				while (destFile.exists() || destFileTif.exists() || destFileTif.exists()) {
					desFileName = "1_" + desFileName;
					destFile = new File(uploadBean.getRealPath() + OS_SLASH
							+ org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR + OS_SLASH + desFileName);
					destFileMini = new File(uploadBean.getRealPath() + OS_SLASH
							+ org.medici.mia.common.util.FileUtils.THUMB_FILES_DIR + OS_SLASH + desFileName);
					desFileNameTif = "1_" + desFileNameTif;
					destFileTif = new File(uploadBean.getRealPath() + OS_SLASH + desFileNameTif);

				}

				FileUtils.copyFile(srcFileMini, destFileMini);
				
				FileUtils.copyFile(srcFileOriginal, destFile);
				
				FileUtils.copyFile(srcFileTif, destFileTif);
				
				
				UploadFileEntity uploadFile = new UploadFileEntity(desFileName, cleanedFileName,
						uploadBean.getUploadInfoId(), new Date(), new Date(), new Integer(imageOrder), 0, null, 5, 0);

				imageOrder++;
				// list of filenames updated (in case of 1_ is added to the
				// filename)
				copiedFileNameList.add(destFile.getAbsolutePath());
				copiedFileNameTifList.add(destFileTif.getAbsolutePath());
				copiedFileNameMiniList.add(destFileMini.getAbsolutePath());

				
				
				org.medici.mia.domain.FolioEntity folioEnt = new org.medici.mia.domain.FolioEntity();
				folioEnt.setFolioId(null);
				
				//Conta gli _
				int endIndex = originalFileName.indexOf(".");
				if (originalFileName.indexOf("_R.") > 0) {
					folioEnt.setRectoverso("recto");
					endIndex = originalFileName.indexOf("_R.");
				} else if (originalFileName.indexOf("_V.") > 0) {
					folioEnt.setRectoverso("verso");
					endIndex = originalFileName.indexOf("_V.");
				} else {
					folioEnt.setRectoverso("");
				}
				int startIndex = originalFileName.indexOf("_");
				String pretext = "";
				if (originalFileName.indexOf("_C_") > 0) {
					startIndex = originalFileName.indexOf("_C_") +3;
				} else if (originalFileName.indexOf("_A_") > 0) {
					startIndex = originalFileName.indexOf("_A_") +3;
					pretext = "Allegato ";
				} else if (originalFileName.indexOf("_R_") > 0) {
					startIndex = originalFileName.indexOf("_R_") +3;
					pretext = "Rubricario ";
				}else if (originalFileName.indexOf("_G_") > 0) {
					startIndex = originalFileName.indexOf("_G_") +3;
					pretext = "Guardia ";
				}
				
				String foglioNumber = originalFileName.substring(startIndex, endIndex);
				
				String num = foglioNumber;
				String posttext = "";
				try {
					if (foglioNumber.indexOf('_') == -1) {
						num = Integer.valueOf(foglioNumber).toString();
					} else {
						num = Integer.valueOf(foglioNumber.substring(0, foglioNumber.indexOf('_'))).toString();
						posttext = foglioNumber.substring(foglioNumber.indexOf('_'));
						posttext = posttext.replace('_', ' ');
					}
				} catch (Exception e) {
					num = foglioNumber;
					num = num.replace('_', ' ');
				}
				
				folioEnt.setFolioNumber(pretext + num + posttext );
				folioEnt.setNoNumb(false);

				folioEnt.setUploadedFileId(null);
				
				
				folioEntities.add(folioEnt);
				
				uploadFileEntities.add(uploadFile);
				
			}

			uploadBean.setUploadFileEntities(uploadFileEntities);
			uploadBean.setFolioEntities(folioEntities);
		
			
			
			return returnList;

		} catch (Exception e) {
			//deleteFilesInRealPath(copiedFileNameList, uploadBean.getRealPath());
			//deleteFilesInRealPath(copiedFileNameList, uploadBean.getRealPath());
			//deleteFilesInRealPath(copiedFileNameList, uploadBean.getRealPath());
			
			logger.error(e.getMessage());
			throw new ApplicationThrowable(e);
		}

	}


	private void deleteTempFiles() {
		for (File file : tmpDir.listFiles()) {
			file.delete();
		}
	}

	private void deleteThumb(String path, String fileName) {
		File file = new File(
				path + OS_SLASH + org.medici.mia.common.util.FileUtils.THUMB_FILES_DIR + OS_SLASH + fileName);
		if (file.exists()) {
			file.delete();
		}
	}

	// Delete original files (jpeg) in real path if the copy won't work
	private void deleteFilesInRealPath(List<String> fileNameCopiedList, String path) {
		File files = new File(path + OS_SLASH + org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR + OS_SLASH);
		if (files.exists()) {
			for (String fileNameCopied : fileNameCopiedList) {
				for (File file : files.listFiles()) {
					if (fileNameCopied.equals(file.getName()))
						file.delete();
				}
			}
		}

	}
	
	private void deleteFilesRoolBack(List<String> fileNameCopiedList) {

		for (String fileNameCopied : fileNameCopiedList) {
			File file = new File(fileNameCopied);
			if (file.exists()) {
				file.delete();
			}
		}

	}

	
	private String getAEName(String description) {
		if (description == null || description.isEmpty()) {
			return "No Title";
		}
		String[] titles = description.split("\\s+");
		String aeName = "";
		int len = 3;
		if (titles.length < 3) {
			len = titles.length;
		}
		for (int i = 0; i < len - 1; i++) {
			aeName = aeName + titles[i] + " ";
		}

		aeName = aeName + titles[len - 1];
		return aeName;

	}

	private GenericResponseJson rotateFile(UploadFileEntity uploadFile, String filePath, RotateType rotateType) {

		GenericResponseJson resp = new GenericResponseJson();

		String installationType = ApplicationPropertyManager.getApplicationProperty("installation.type");

		// FOR DEVELOPMENT ENV - Windows or LINUX - ImageMagick SECTION
		String uploadConverterName = ApplicationPropertyManager
				.getApplicationProperty("imagemagick.convert.executable");
		String uploadConverterPath = ApplicationPropertyManager.getApplicationProperty("imagemagick.convert.path");
		String uploadConverter = uploadConverterPath + uploadConverterName;

		String sourceFile = filePath + OS_SLASH + org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR + OS_SLASH
				+ uploadFile.getFilename();
		String desFile = sourceFile;

		// FOR PRODUCTION ENV - LINUX - VIPS SECTION
		String renameFileExec = "/usr/bin/mv";
		String vipsConverterPath = ApplicationPropertyManager.getApplicationProperty("vips.convert.path");
		String vipsConverterName = ApplicationPropertyManager.getApplicationProperty("vips.convert.executable");
		String vipsThumbnailName = ApplicationPropertyManager.getApplicationProperty("vips.thumbnail.executable");

		String vipsConverter = vipsConverterPath + vipsConverterName;
		String vipsThumbConverter = vipsConverterPath + vipsThumbnailName;
		String vipsRotateDegrees = "";

		// Vips Options
		String vipsRotateFunction = "rot";
		// String vipsSourceFile = filePath + OS_SLASH
		// + org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR
		// + OS_SLASH + uploadFile.getFilename();
		String vipsSourceFile = filePath + OS_SLASH + org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR + OS_SLASH
				+ uploadFile.getFilename();
		String vipsTempFile = filePath + OS_SLASH + org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR + OS_SLASH
				+ "rotTmp.jpg";
		String vipsDesFile = vipsSourceFile;

		if (rotateType.getValue().equals("90")) {
			vipsRotateDegrees = "d90";
		}

		if (rotateType.getValue().equals("180")) {
			vipsRotateDegrees = "d180";
		}

		if (rotateType.getValue().equals("270")) {
			vipsRotateDegrees = "d270";
		}

		// In some cases (landscape vs portrait) we need to resize the image
		// before to convert to tif
		File file = new File(sourceFile);
		BufferedImage bimg = null;
		try {
			bimg = ImageIO.read(file);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		if (bimg != null) {

			String[] imageMagickRotate = { uploadConverter, sourceFile, "-rotate", rotateType.getValue(), desFile };

			String[] vipsRotate_cmd = { vipsConverter, vipsRotateFunction, vipsSourceFile, vipsTempFile,
					vipsRotateDegrees };

			String[] renameFile_cmd = { renameFileExec, vipsTempFile, vipsDesFile };

			try {
				if (installationType.equals("development")) {
					logger.info(installationType);
					ProcessBuilder pb2 = new ProcessBuilder(imageMagickRotate);
					logger.info("Development Env - ImageMagick - Rotating image" + rotateType.getValue() + " degrees"
							+ pb2.command());
					pb2.redirectErrorStream(true);
					pb2.directory(new File(uploadConverterPath));

					Process p2;

					p2 = pb2.start();
					BufferedReader br2 = new BufferedReader(new InputStreamReader(p2.getInputStream()));
					String line2 = null;
					while ((line2 = br2.readLine()) != null) {
						System.out.println(line2);
					}

					int waitForReturn = p2.waitFor();
					System.out.println("2" + waitForReturn);
					if (waitForReturn == 0) {
						getUploadFileDAO().updateUploadFileConvertField(uploadFile.getUploadFileId(),
								org.medici.mia.common.util.FileUtils.TIFFS_CONVERSION_SUCCESS);
						resp.setMessage("Development Env - ImageMagick - Rotate process finished successfully.");
						resp.setStatus(StatusType.ok.toString());
					}
					// Close all the buffers to release all used resources
					br2.close();
				}
				if (installationType.equals("production")) {
					logger.info(installationType);
					ProcessBuilder pb3 = new ProcessBuilder(vipsRotate_cmd);
					logger.info("Production Env - VIPS - ROTATING image" + rotateType.getValue() + " degrees"
							+ pb3.command());
					pb3.redirectErrorStream(true);
					pb3.directory(new File(uploadConverterPath));

					Process p3;

					p3 = pb3.start();
					BufferedReader br3 = new BufferedReader(new InputStreamReader(p3.getInputStream()));
					String line3 = null;
					while ((line3 = br3.readLine()) != null) {
						System.out.println(line3);
					}

					int waitForReturn = p3.waitFor();
					System.out.println("3" + waitForReturn);
					if (waitForReturn == 0) {
						getUploadFileDAO().updateUploadFileConvertField(uploadFile.getUploadFileId(),
								org.medici.mia.common.util.FileUtils.TIFFS_CONVERSION_SUCCESS);
						resp.setMessage("Production Env - VIPS - ROTATE process finished successfully.");
						resp.setStatus(StatusType.ok.toString());
					}

					ProcessBuilder pb4 = new ProcessBuilder(renameFile_cmd);
					logger.info("Production Env - VIPS - RENAMING image" + pb4.command());
					pb4.redirectErrorStream(true);
					pb4.directory(new File(uploadConverterPath));

					Process p4;

					p4 = pb4.start();
					BufferedReader br4 = new BufferedReader(new InputStreamReader(p4.getInputStream()));
					String line4 = null;
					while ((line4 = br4.readLine()) != null) {
						System.out.println(line4);
					}

					int waitForReturn2 = p4.waitFor();
					System.out.println("4" + waitForReturn2);
					if (waitForReturn2 == 0) {
						getUploadFileDAO().updateUploadFileConvertField(uploadFile.getUploadFileId(),
								org.medici.mia.common.util.FileUtils.TIFFS_CONVERSION_SUCCESS);
						resp.setMessage("Production Env - VIPS - RENAME image process finished successfully.");
						resp.setStatus(StatusType.ok.toString());
					}

					// Close all the buffers to release all used resources
					br3.close();
					br4.close();
				}

			} catch (Exception e) {
				logger.error(e.getMessage());
			}

			// Close all the buffers to release all used resources
			bimg.flush();
		} else {
			logger.error("Rotate file Process: Reading from ImageIO ERROR.");
			resp.setMessage("Rotate file Process: Reading from ImageIO ERROR.");
			resp.setStatus(StatusType.ko.toString());

		}

		return resp;
	}

	@Override
	public List<RepositoryJson> findAllRepositories() throws ApplicationThrowable {
		try {

			List<RepositoryEntity> repositories = getRepositoryDAO().findAllRepositories();
			if (repositories == null || repositories.isEmpty()) {
				return null;
			}
			List<RepositoryJson> repJsons = new ArrayList<RepositoryJson>();
			for (RepositoryEntity repEnt : repositories) {
				RepositoryJson json = new RepositoryJson();
				json.toJson(repEnt);
				repJsons.add(json);
			}

			return repJsons;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	public CollectionDAO getCollectionDAO() {
		return collectionDAO;
	}

	public void setCollectionDAO(CollectionDAO collectionDAO) {
		this.collectionDAO = collectionDAO;
	}

	public GoogleLocationDAO getGoogleLocationDAO() {
		return googleLocationDAO;
	}

	public void setGoogleLocationDAO(GoogleLocationDAO googleLocationDAO) {
		this.googleLocationDAO = googleLocationDAO;
	}

	public RepositoryDAO getRepositoryDAO() {
		return repositoryDAO;
	}

	public void setRepositoryDAO(RepositoryDAO repositoryDAO) {
		this.repositoryDAO = repositoryDAO;
	}

	public VolumeDAO getVolumeDAO() {
		return volumeDAO;
	}

	public void setVolumeDAO(VolumeDAO volumeDAO) {
		this.volumeDAO = volumeDAO;
	}

	public SeriesDAO getSeriesDAO() {
		return seriesDAO;
	}

	public void setSeriesDAO(SeriesDAO seriesDAO) {
		this.seriesDAO = seriesDAO;
	}

	public InsertDAO getInsertDAO() {
		return insertDAO;
	}

	public void setInsertDAO(InsertDAO insertDAO) {
		this.insertDAO = insertDAO;
	}

	public UploadInfoDAO getUploadInfoDAO() {
		return uploadInfoDAO;
	}

	public void setUploadInfoDAO(UploadInfoDAO uploadInfoDAO) {
		this.uploadInfoDAO = uploadInfoDAO;
	}

	public UploadFileDAO getUploadFileDAO() {
		return uploadFileDAO;
	}

	public void setUploadFileDAO(UploadFileDAO uploadFileDAO) {
		this.uploadFileDAO = uploadFileDAO;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	public UserDAO getUserDAO() {
		return userDAO;
	}
	public GenericDocumentDao getDocumentDAO() {
		return documentDAO;
	}

}