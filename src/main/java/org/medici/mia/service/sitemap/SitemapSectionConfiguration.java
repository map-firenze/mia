/*
 * SitemapSectionConfiguration.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.service.sitemap;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.medici.mia.common.util.HtmlUtils;
import org.medici.mia.domain.Sitemap;
import org.medici.mia.domain.Sitemap.ChangeFrequency;

/**
 * 
 * @author fsolfato
 *
 */
public class SitemapSectionConfiguration {
	
	private String baseUrl;
	private ChangeFrequency changeFrequency;
	private Double priority;
	
	public SitemapSectionConfiguration(
			String baseUrl, ChangeFrequency frequency, Double priority) {
		
		super();
		this.baseUrl = baseUrl;
		this.changeFrequency = frequency;
		this.priority = priority;
	}
	
	public List<Sitemap> createSitemaps(List<Object[]> sitemapsInfos) {
		List<Sitemap> sitemaps = new ArrayList<Sitemap>();
		
		if (sitemapsInfos != null && !sitemapsInfos.isEmpty()) {
	
			for (Object[] sitemapInfo : sitemapsInfos) {
				sitemaps.add(createSitemap(sitemapInfo));
			}
		}
		
		return sitemaps;
	}
	
	public Sitemap createSitemap(Object[] sitemapInfo) {
		
		Sitemap sitemap = new Sitemap();
		
		Integer id = (Integer) sitemapInfo[0];
		
		Date creationDate = null;
		if (sitemapInfo[1] != null) {
			creationDate = (Date) sitemapInfo[1];
		}
		
		Date lastModificationDate = null;
		if (sitemapInfo[2] != null) {
			lastModificationDate = (Date) sitemapInfo[2];
		}
		
		sitemap.setLocation(String.format("%s%s", baseUrl, id));
		sitemap.setDateCreated(creationDate);
		sitemap.setLastModification(lastModificationDate);
		sitemap.setChangeFrequency(changeFrequency);
		sitemap.setPriority(priority);
		
		return sitemap;
	}
	
	public String getBaseUrl() {
		return baseUrl;
	}
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	
	public ChangeFrequency getChangeFrequency() {
		return changeFrequency;
	}
	public void setChangeFrequency(ChangeFrequency frequency) {
		this.changeFrequency = frequency;
	}
	
	public Double getPriority() {
		return priority;
	}
	public void setPriority(Double priority) {
		this.priority = priority;
	}
}
