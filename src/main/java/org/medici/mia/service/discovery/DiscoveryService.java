package org.medici.mia.service.discovery;

import java.util.List;

import org.medici.mia.common.json.DiscoveryJson;
import org.medici.mia.common.json.DiscoveryUploadJson;
import org.medici.mia.domain.DiscoveryStatus;

public interface DiscoveryService {

	public DiscoveryJson findById(Integer id);

	public List<DiscoveryJson> findDiscoveries(String searchText, int limit, int offset, DiscoveryStatus status);
	
	public DiscoveryJson add(DiscoveryJson discovery);
	
	public DiscoveryJson remove(Integer id);
	
	public DiscoveryJson approve(Integer id);
	
	public DiscoveryJson reject(Integer id, String motivation);
	
	public DiscoveryJson publish(Integer id);
	
	public DiscoveryJson editRequest(Integer id, String motivation);
	
	public List<DiscoveryJson> findUserDiscoveries(String account);
	
	public Integer getDiscoveriesCount(DiscoveryStatus status);
	
	public DiscoveryUploadJson findDiscoveryUploadById(Integer id);
	
	public DiscoveryUploadJson removeDiscoveryUpload(Integer id, Integer discoveryUploadId);

	public DiscoveryJson getDefault(Integer documentId);
	
	public DiscoveryJson edit(DiscoveryJson discovery);
	
}
