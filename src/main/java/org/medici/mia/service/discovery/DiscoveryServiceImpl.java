package org.medici.mia.service.discovery;

import static org.medici.mia.service.discovery.DiscoveryServiceUtils.convertToJson;
import static org.medici.mia.service.discovery.DiscoveryServiceUtils.createMessage;
import static org.medici.mia.service.discovery.DiscoveryServiceUtils.getDiscoveryMessageBody;
import static org.medici.mia.service.discovery.DiscoveryServiceUtils.getTranscription;
import static org.medici.mia.service.discovery.DiscoveryServiceUtils.isAuthorized;
import static org.medici.mia.service.discovery.DiscoveryServiceUtils.isOwner;
import static org.medici.mia.service.discovery.DiscoveryServiceUtils.validateNewDocument;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.BooleanUtils;
import org.medici.mia.common.json.DiscoveryJson;
import org.medici.mia.common.json.DiscoveryUploadJson;
import org.medici.mia.dao.discovery.DiscoveryDao;
import org.medici.mia.dao.discoveryUpload.DiscoveryUploadDao;
import org.medici.mia.dao.emailmessageuser.EmailMessageUserDAO;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.dao.userauthority.UserAuthorityDAO;
import org.medici.mia.domain.Discovery;
import org.medici.mia.domain.DiscoveryStatus;
import org.medici.mia.domain.DiscoveryUpload;
import org.medici.mia.domain.EmailMessageUser;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.User;
import org.medici.mia.domain.UserAuthority.Authority;
import org.medici.mia.service.message.MessageService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DiscoveryServiceImpl implements DiscoveryService {

	@Autowired
	private DiscoveryDao dao;
	
	@Autowired
	private GenericDocumentDao documentDao;

	@Autowired
	private UserService userService;
	
	@Autowired
	private UserAuthorityDAO authorityDao;
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private EmailMessageUserDAO emailMessageDao;
	
	@Autowired
	private DiscoveryUploadDao discoveriesUploadDao;
	
	@Override
	public DiscoveryJson findById(Integer id) {
		Discovery result = dao.find(id);
		if( result == null || result.getLogicalDelete() == 1 ) {
			throw new IllegalArgumentException("Discovery not found with id " + id);
		}
		
		if( !isAuthorized( SecurityContextHolder.getContext().getAuthentication().getAuthorities() ) && !isOwner(result)  ) {
			throw new IllegalArgumentException("User is not authorized");
		}
		
		return convertToJson(result);
	}
	
	@Override
	public List<DiscoveryJson> findDiscoveries(String searchText, int limit, int offset, DiscoveryStatus status) {
		List<Discovery> discoveries = null;
		if( status == null ) {
			discoveries = dao.findDiscoveries(searchText, limit, offset);
		} else {
			discoveries = dao.findDiscoveries(status, limit, offset);
		}
		
		
		List<DiscoveryJson> result = new ArrayList<DiscoveryJson>();
		for( Discovery d : discoveries) {
			result.add(convertToJson(d));
		}
		
		return result;
	}

	@Override
	public DiscoveryJson add(DiscoveryJson discovery) {
		Discovery result = addNew(discovery);
		if( !DiscoveryStatus.DRAFT.equals(result.getStatus()) ) {
			throw new IllegalArgumentException("Discovery status must be Draft");
		}
		
		result.setStatus(DiscoveryStatus.SENT_TO_PEER_REVIEW);
		sendNotification(result);
		
		return convertToJson(result);
	}
	
	private Discovery addNew( DiscoveryJson discovery ) {
		Discovery result = convertToModel(discovery);
		validateNewDocument(result);
		result = dao.addNewDiscovery(result);
		
		return result;
	}
	
	private void sendNotification( Discovery discovery) {
		for( User user : getRecipientUsers( Authority.SPOTLIGHT_COORDINATORS, Authority.ADMINISTRATORS )) {
			messageService.send( createMessage(user.getAccount(), "PUBLICATION - PEER REVIEW REQUEST", "Please check:" + getDiscoveryMessageBody("spotlight-review-staff/"+discovery.getId())) );

			if(user.getMailNotification() != null && BooleanUtils.isTrue(user.getMailNotification())) {
				sendEmailMessage(discovery.getId(), user);
			}
			
		}
	}
	
	private void sendEmailMessage( Integer discoveryId, User userReceiver ) {
		EmailMessageUser emailMessage = new EmailMessageUser();
		emailMessage.setSubject("PUBLICATION - PEER REVIEW REQUEST");
		emailMessage.setBody("Please check:" + getDiscoveryMessageBody("spotlight-review-staff/"+discoveryId));
		emailMessage.setMailSended(false);
		emailMessage.setUser(userReceiver);
		
		emailMessageDao.persist(emailMessage);	
	}
	
	@Override
	public DiscoveryJson reject(Integer id, String motivation) {
		Discovery result = dao.find(id);
		
		if( !isAuthorized( SecurityContextHolder.getContext().getAuthentication().getAuthorities() ) ) {
			throw new IllegalArgumentException("User is not authorized");
		}
		
		if( !(DiscoveryStatus.SENT_TO_PEER_REVIEW.equals(result.getStatus()) || DiscoveryStatus.APPROVED.equals(result.getStatus())) ) {
			throw new IllegalArgumentException("Discovery status must be \"Sent to peer review\" or \"Approved\"");
		}
		
		result.setStatus(DiscoveryStatus.REJECTED);
		
		messageService.send( createMessage(result.getDocument().getCreatedBy(), "SUBMISSION FOR PUBLICATION REJECTED", "<br><br> It has been rejected for the following reasons: <br>" + motivation + "<br>" + "<b>If you would like to delete your submission please click here: </b>" + getDiscoveryMessageBody("document-entity/"+result.getDocument().getDocumentEntityId()+"/spotlight/"+result.getId()) + "(Your submission for publication is only visible to you)"));
				
		return convertToJson(result);
	}
	
	@Override
	public DiscoveryJson editRequest(Integer id, String motivation) {
		Discovery result = dao.find(id);
		if( !isAuthorized( SecurityContextHolder.getContext().getAuthentication().getAuthorities() ) ) {
			throw new IllegalArgumentException("User is not authorized");
		}
		
		if( !DiscoveryStatus.APPROVED.equals(result.getStatus()) && !DiscoveryStatus.SENT_TO_PEER_REVIEW.equals(result.getStatus()) ) {
			throw new IllegalArgumentException("Discovery status must be Sent to peer review or Approved");
		}
		
		result.setStatus(DiscoveryStatus.REOPENED);
		
		messageService.send( createMessage(result.getDocument().getCreatedBy(), "SUBMISSION FOR PUBLICATION EDITS NEEDED",  "<br> <b>Your submission for publication has been reviewed </b>" + getDiscoveryMessageBody("document-entity/"+result.getDocument().getDocumentEntityId()+"/spotlight/"+result.getId()+"/review") + "<br> and will be approved if resubmitted with the following edits: <br>" + motivation) );
				
		return convertToJson(result);
	}
	
	@Override
	public DiscoveryJson edit(DiscoveryJson json) {
		Discovery result = dao.find(json.getId());
		if( result == null || result.getLogicalDelete() == 1 ) {
			throw new IllegalArgumentException("Discovery not found with id " + json.getId());
		}

		if( !isAuthorized( SecurityContextHolder.getContext().getAuthentication().getAuthorities() ) && !isOwner(result) ) {
			throw new IllegalArgumentException("User is not authorized");
		}

		if( !DiscoveryStatus.REOPENED.equals(result.getStatus()) ) {
			throw new IllegalArgumentException("Discovery status must be Reopened");
		}
		
		try {
			result.setSubmissionDate(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(json.getSubmissionDate()));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		result.setTitle(json.getTitle());
		result.setBibliography(json.getBibliography());
		result.setWhyHistImp(json.getWhyHistImp());
		result.setShortNotice(json.getShortNotice());
		result.setDocument(documentDao.findDocumentById(json.getDocumentId()));
		result.setStatus(DiscoveryStatus.SENT_TO_PEER_REVIEW);
		result.setLinks(getDocuments(json.getLinks()));
		result.setDiscoveriesUpload(getUpdatedUploads(result.getDiscoveriesUpload(), json.getUploads()));
		
		sendNotification(result);

		return convertToJson(result);
	}
	
	private List<DiscoveryUpload> getUpdatedUploads( List<DiscoveryUpload> uploads, List<DiscoveryUploadJson> jsonUploads ) {
		for( DiscoveryUploadJson uploadJson : jsonUploads ) {
			if( uploadJson.getId() != null ) {
				if( uploadJson.getDeleted() ) {
					DiscoveryUpload upload = discoveriesUploadDao.find(uploadJson.getId());
					uploads.remove( upload );
					discoveriesUploadDao.remove(upload);
				}
			} else {
				uploads.add( convertToModel(uploadJson) );
			}
		}
		
		return uploads;
	}
	
	@Override
	public DiscoveryJson approve(Integer id) {
		Discovery result = dao.find(id);
		
		if( !isAuthorized( SecurityContextHolder.getContext().getAuthentication().getAuthorities() ) ) {
			throw new IllegalArgumentException("User is not authorized");
		}
		
		if( !DiscoveryStatus.SENT_TO_PEER_REVIEW.equals(result.getStatus()) ) {
			throw new IllegalArgumentException("Discovery status must be Sent to peer review");
		}
		
		result.setStatus(DiscoveryStatus.APPROVED);
				
		return convertToJson(result);
	}
	
	
	@Override
	public DiscoveryJson publish(Integer id) {
		Discovery result = dao.find(id);
		
		if( !isAuthorized( SecurityContextHolder.getContext().getAuthentication().getAuthorities() ) ) {
			throw new IllegalArgumentException("User is not authorized");
		}
		
		if( !DiscoveryStatus.APPROVED.equals(result.getStatus()) ) {
			throw new IllegalArgumentException("Discovery status must be Sent to peer review");
		}
		
		result.setStatus(DiscoveryStatus.PUBLISHED);
		
		messageService.send( createMessage(result.getDocument().getCreatedBy(), "YOUR DOCUMENT HAS BEEN PUBLISHED", "Your document has been published. <br> View it here:" + getDiscoveryMessageBody("discovery/"+result.getId())) );
				
		return convertToJson(result);
	}
	
	@Override
	public DiscoveryJson remove(Integer id) {
		Discovery discovery = dao.find(id);
		
		if( !isAuthorized( SecurityContextHolder.getContext().getAuthentication().getAuthorities() ) && !isOwner(discovery)  ) {
			throw new IllegalArgumentException("User is not authorized");
		}
		
		if( discovery == null ) {
			throw new IllegalArgumentException("Discovery not found with id " + id);
		}
		discovery.setLogicalDelete(1);
		
		return convertToJson(discovery);
	}
	
	
	@Override
	public List<DiscoveryJson> findUserDiscoveries(String account) {
		List<Discovery> discoveries = dao.findUserDiscoveries(account);
		
		List<DiscoveryJson> result = new ArrayList<DiscoveryJson>();
		for( Discovery d : discoveries) {
			result.add(convertToJson(d));
		}
		
		return result;
	}
	
	@Override
	public Integer getDiscoveriesCount(DiscoveryStatus status) {
		return dao.count(status);
	}
	
	@Override
	public DiscoveryUploadJson findDiscoveryUploadById(Integer id) {
		return convertToJson( discoveriesUploadDao.find(id) );
	}
	
	private Set<User> getRecipientUsers( Authority... authorities ) {
		Set<User> recipientUsers = new HashSet<User>();
		
		for( Authority a : authorities ) {
			recipientUsers.addAll(userService.findUsersByAuthority(authorityDao.find(a)));
		}
		
		return recipientUsers;
	}
	
	private Discovery convertToModel(DiscoveryJson json) {
		Discovery result = new Discovery();
		
		result.setTitle(json.getTitle());
		result.setBibliography(json.getBibliography());
		result.setWhyHistImp(json.getWhyHistImp());
		result.setShortNotice(json.getShortNotice());
		result.setSubmissionDate(new Date());
		result.setDocument(documentDao.findDocumentById(json.getDocumentId()));
		
		result.setLinks(getDocuments(json.getLinks()));
		result.setDiscoveriesUpload(getUploads(json.getUploads()));
		
		return result;
	}

	private List<MiaDocumentEntity> getDocuments( List<Integer> documentIds ) {
		List<MiaDocumentEntity> documents = new ArrayList<MiaDocumentEntity>();
		if( documentIds == null ) {
			return documents;
		}
		
		for( Integer id : documentIds ) {
			documents.add(documentDao.findDocumentById(id));
		}
		
		return documents;
	}
	
	private List<DiscoveryUpload> getUploads( List<DiscoveryUploadJson> jsonUploads ) {
		List<DiscoveryUpload> uploads = new ArrayList<DiscoveryUpload>();
		if( jsonUploads == null ) {
			return uploads;
		}
		
		for( DiscoveryUploadJson jsonUpload : jsonUploads ) {
			if( jsonUpload.getId() == null ) {
				uploads.add( convertToModel(jsonUpload) );
			} else {
				uploads.add(discoveriesUploadDao.find(jsonUpload.getId()));
			}
		}
		
		return uploads;
	}
	
	private static DiscoveryUpload convertToModel( DiscoveryUploadJson jsonUpload ) {
		DiscoveryUpload upload = new DiscoveryUpload();
		upload.setFileName(jsonUpload.getFileName());
		upload.setDescriptiveTitle(jsonUpload.getDescriptiveTitle());
		upload.setInternalFileName(jsonUpload.getInternalFileName());
		
		return upload;
	}
	
	
	@Override
	public DiscoveryUploadJson removeDiscoveryUpload(Integer id, Integer discoveryUploadId) {
		Discovery result = dao.find(id);
		if( result == null || result.getLogicalDelete() == 1 ) {
			throw new IllegalArgumentException("Discovery not found with id " + id);
		}
		
		if( !isAuthorized( SecurityContextHolder.getContext().getAuthentication().getAuthorities() ) && !isOwner(result)  ) {
			throw new IllegalArgumentException("User is not authorized");
		}
		
		DiscoveryUpload upload = discoveriesUploadDao.find(discoveryUploadId);
		if( upload == null) {
			throw new IllegalArgumentException("DiscoveryUpload not found with id " + id);
		}
		
		if( result.getDiscoveriesUpload().remove(upload) ) {
			discoveriesUploadDao.remove(upload);
		}
		
		return convertToJson(upload);
	}
	
	@Override
	public DiscoveryJson getDefault(Integer documentId) {
		MiaDocumentEntity document = documentDao.find(documentId);
		if( document == null ) {
			throw new IllegalArgumentException("Document not found with id " + document);
		}
		
		DiscoveryJson result = new DiscoveryJson();
		result.setTranscriptions(getTranscription(document.getDocumentTranscriptions()));
		result.setSynopsis(document.getGeneralNotesSynopsis());
		
		return result;
	}
	
}
