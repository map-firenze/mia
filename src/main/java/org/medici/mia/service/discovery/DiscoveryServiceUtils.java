/*
 * DiscoveryServiceUtils.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.service.discovery;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.medici.mia.common.json.DiscoveryJson;
import org.medici.mia.common.json.DiscoveryUploadJson;
import org.medici.mia.common.json.message.MessageSendJson;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.common.util.FileUtils;
import org.medici.mia.domain.Discovery;
import org.medici.mia.domain.DiscoveryUpload;
import org.medici.mia.domain.DocumentTranscriptionEntity;
import org.medici.mia.domain.FolioEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public class DiscoveryServiceUtils {

	static Boolean isOwner( Discovery discovery ) {
		return discovery.getDocument().getCreatedBy() != null && 
				discovery.getDocument().getCreatedBy().equals(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
	}

	static Boolean isAuthorized( Collection<GrantedAuthority> grantedAuthorities ) {
		Set<String> authorities = new HashSet<String>();
		for( GrantedAuthority ga : grantedAuthorities ) {
			authorities.add(ga.getAuthority());
		}
		
		return authorities.contains("ROLE_SPOTLIGHT_COORDINATORS") || authorities.contains("ROLE_ADMINISTRATORS");
	}
	
	static MessageSendJson createMessage( String account, String subject, String body) {
		MessageSendJson message = new MessageSendJson();
		message.setAccount(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
		message.setMessageSubject(subject);
		message.setMessageText(body);
		message.setTo(account);
		
		return message;
	}
	
	static String getDiscoveryMessageBody( String path ) {
		String url = new StringBuilder().append(ApplicationPropertyManager.getApplicationProperty("website.protocol"))
			.append("://")
			.append(ApplicationPropertyManager.getApplicationProperty("website.domain"))
			.append(ApplicationPropertyManager.getApplicationProperty("website.contextPath"))
			.append("index.html#/mia/" + path).toString();
		
		return new StringBuilder()
				.append("<br><a href=\"")
				.append(url + "\" target=\"_blank\" >")
				.append(url)
				.append("</a>").toString();
	}

	static void validateNewDocument(Discovery result) {
		MiaDocumentEntity document = result.getDocument();
		if(document == null) {
			throw new RuntimeException("Document not found");
		}
		
		if(document.getPrivacy() == 0) {
			throw new RuntimeException("Document is not static");
		}
		
		UserDetails currentUser = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
		if(!document.getCreatedBy().equals(currentUser.getUsername())) {
			throw new RuntimeException("Document is not owned by user");
		}
		
		for(UploadFileEntity upload : document.getUploadFileEntities()) {
			if(!upload.getUploadInfoEntity().getOwner().equals(currentUser.getUsername())) {
				throw new RuntimeException("Document Uploads are not owned by user");
			}
		}
	}

	static DiscoveryJson convertToJson(Discovery model) {
		DiscoveryJson result = new DiscoveryJson();
		
		result.setId(model.getId());
		result.setTitle(model.getTitle());
		result.setBibliography(model.getBibliography());
		result.setWhyHistImp(model.getWhyHistImp());
		result.setShortNotice(model.getShortNotice());
		result.setStatus(model.getStatus().getDisplayName());
		result.setLogicalDelete(model.getLogicalDelete() == 0 ? false : true);
		result.setDocumentId(model.getDocument().getDocumentEntityId());
		result.setImagePath(getDocumentImagePath(model));
		result.setSubmissionDate(model.getSubmissionDate() == null ? null : new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(model.getSubmissionDate()));
		result.setOwner(model.getDocument().getCreatedBy());
		result.setLinks(getDocumentIds(model));
		result.setUploads(getUploadJson(model));
		result.setTranscriptions(getTranscription(model.getDocument().getDocumentTranscriptions()));
		result.setSynopsis(model.getDocument().getGeneralNotesSynopsis());
		
		return result;
	}
	
	private static String getDocumentImagePath( Discovery model ) {
		List<UploadFileEntity> entites = model.getDocument().getUploadFileEntities();
		if( entites.isEmpty() ) {
			return "";
		}
		
		UploadFileEntity uploadFile = entites.get(0);
		String insName = null;
		if (uploadFile.getUploadInfoEntity().getInsertEntity() != null) {
			insName = uploadFile.getUploadInfoEntity().getInsertEntity().getInsertName();
		}
		String realPath = FileUtils.getRealPathJSONResponse(
						uploadFile.getUploadInfoEntity().getRepositoryEntity().getLocation(),
						uploadFile.getUploadInfoEntity().getRepositoryEntity().getRepositoryAbbreviation(),
						uploadFile.getUploadInfoEntity().getCollectionEntity().getCollectionAbbreviation(),
						uploadFile.getUploadInfoEntity().getVolumeEntity().getVolume(), insName);
		return realPath
				+ FileUtils.THUMB_FILES_DIR
				+ FileUtils.OS_SLASH
				+ uploadFile.getFilename() + "&WID=250";
	}
	
	static List<Integer> getDocumentIds( Discovery model ) {
		List<Integer> documentIds = new ArrayList<Integer>();
		for( MiaDocumentEntity doc : model.getLinks() ) {
			documentIds.add(doc.getDocumentEntityId());
		}
		
		return documentIds;
	}
	
	static List<DiscoveryUploadJson> getUploadJson( Discovery model ) {
		List<DiscoveryUploadJson> uploads = new ArrayList<DiscoveryUploadJson>();
		for( DiscoveryUpload d : model.getDiscoveriesUpload() ) {
			uploads.add(convertToJson(d));
		}
		
		return uploads;
	}
	
	static DiscoveryUploadJson convertToJson( DiscoveryUpload d ) {
		DiscoveryUploadJson upload = new DiscoveryUploadJson();
		upload.setId(d.getId());
		upload.setFileName(d.getFileName());
		upload.setDescriptiveTitle(d.getDescriptiveTitle());
		upload.setInternalFileName(d.getInternalFileName());
		upload.setFilePath( ApplicationPropertyManager.getApplicationProperty("discoveries.upload.path") );
		upload.setDeleted(false);
		
		return upload;
	}
	
	static String getTranscription(List<DocumentTranscriptionEntity> transcriptions) {
		StringBuilder sb = new StringBuilder();
		for( int i=0; i<transcriptions.size(); i++ ) {
			if( i!=0 ) {
				sb.append(", ");
			}
			
			sb.append("[" + getFolios(transcriptions.get(i)) + "] ");
			sb.append(transcriptions.get(i).getTranscription());
		}
		
		return sb.toString();
	}
	
	static String getFolios(DocumentTranscriptionEntity trans) {
		StringBuilder sb = new StringBuilder();
		for( int i=0; i<trans.getUploadFileEntity().getFolioEntities().size(); i++ ) {
			if( i!=0 ) {
				sb.append(", ");
			}
			
			FolioEntity folio = trans.getUploadFileEntity().getFolioEntities().get(i);
			
			sb.append(folio.getFolioNumber());
			sb.append(folio.getRectoverso() == null ? "v" : folio.getRectoverso().charAt(0));
		}
		return sb.toString();
	}
	
}
