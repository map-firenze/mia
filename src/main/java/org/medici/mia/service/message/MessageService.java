package org.medici.mia.service.message;

import java.util.List;

import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.message.FindInboxMessageJson;
import org.medici.mia.common.json.message.FindOutboxMessageJson;
import org.medici.mia.common.json.message.MessageSelectedJson;
import org.medici.mia.common.json.message.MessageSendJson;
import org.medici.mia.domain.AnnotationQuestion;
import org.medici.mia.domain.User;
import org.medici.mia.exception.ApplicationThrowable;

public interface MessageService {

	public FindInboxMessageJson findInboxMessages(String account,
			Integer startMesNum, Integer endMesNum, boolean isNewMessage)
			throws ApplicationThrowable;

	public FindOutboxMessageJson findOutboxMessages(String account,
			Integer startMesNum, Integer endMesNum) throws ApplicationThrowable;

	public GenericResponseJson send(MessageSendJson message)
			throws ApplicationThrowable;

	public GenericResponseJson deleteMessageById(Integer messageId)
			throws ApplicationThrowable;

	public MessageSelectedJson getMessage(Integer messageId)
			throws ApplicationThrowable;

	public GenericResponseJson deleteMultipleMessages(List<Integer> messageIds)
			throws ApplicationThrowable;

	public Boolean sendNotification(User user, MessageSendJson messageJson)
			throws ApplicationThrowable;

	public void onDocumentEdited(String account, String to, Integer documentId);

	public void onAnnotationQuestionAdded(List<AnnotationQuestion> newAnnotationQuestions);
}