package org.medici.mia.service.message;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.message.FindInboxMessageJson;
import org.medici.mia.common.json.message.FindOutboxMessageJson;
import org.medici.mia.common.json.message.MessageInboxJson;
import org.medici.mia.common.json.message.MessageOutboxJson;
import org.medici.mia.common.json.message.MessageSelectedJson;
import org.medici.mia.common.json.message.MessageSendJson;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.dao.emailmessageuser.EmailMessageUserDAO;
import org.medici.mia.dao.usermessage.UserMessageDAO;
import org.medici.mia.domain.Annotation;
import org.medici.mia.domain.AnnotationQuestion;
import org.medici.mia.domain.EmailMessageUser;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.User;
import org.medici.mia.domain.UserMessage;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.underscore.U;

/**
 *
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Service
class MessageServiceImpl implements MessageService {
	
	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private UserMessageDAO userMessageDAO;
	
	@Autowired
	private EmailMessageUserDAO emailMessageUserDAO;

	@Autowired
	private UserService userService;

	@Override
	public FindInboxMessageJson findInboxMessages(String account,
												  Integer page, Integer perPage, boolean isNewMessage)
			throws ApplicationThrowable {

		try {

			List<UserMessage> userMessages = getUserMessageDAO()
					.getInboxMessages(account, isNewMessage);

			if (userMessages == null || userMessages.isEmpty()) {
				return null;
			}

			List<MessageInboxJson> messagesJson = new ArrayList<MessageInboxJson>();

			getArraySubList(userMessages, page, perPage);

			for (UserMessage messEnt : getArraySubList(userMessages,
					page, perPage)) {

				MessageInboxJson messageInboxJson = new MessageInboxJson();
				messageInboxJson.toJson(messEnt);
				messagesJson.add(messageInboxJson);
			}

			FindInboxMessageJson findMessages = new FindInboxMessageJson();
			findMessages.setMessages(messagesJson);
			findMessages.setMessTotalCount(userMessages.size());

			return findMessages;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Override
	public FindOutboxMessageJson findOutboxMessages(String account,
													Integer page, Integer perPage) throws ApplicationThrowable {

		try {

			List<UserMessage> userMessages = getUserMessageDAO()
					.getOutboxMessages(account);

			if (userMessages == null || userMessages.isEmpty()) {
				return null;
			}

			List<MessageOutboxJson> messages = new ArrayList<MessageOutboxJson>();

			for (UserMessage messEnt : getArraySubList(userMessages,
					page, perPage)) {

				MessageOutboxJson messageOutboxJson = new MessageOutboxJson();
				messageOutboxJson.toJson(messEnt);
				messages.add(messageOutboxJson);
			}

			FindOutboxMessageJson findMessages = new FindOutboxMessageJson();
			findMessages.setMessages(messages);
			findMessages.setMessTotalCount(userMessages.size());

			return findMessages;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson send(MessageSendJson message)
			throws ApplicationThrowable

	{
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");

		UserMessage usermessageInbox = new UserMessage();
		UserMessage usermessageOutbox = new UserMessage();

		message.toEntityOutbox(usermessageOutbox);
		message.toEntityInbox(usermessageInbox);

		try {

			User userSender = new User();
			userSender.setAccount(message.getAccount());

			User userReceiver = new User();
			userReceiver.setAccount(message.getTo());

			usermessageOutbox.setUser(userSender);
			usermessageInbox.setUser(userReceiver);

			if(U.isEqual(userSender, userReceiver) || message.getMessageSubject().equals("ACCESS CONTENT REQUEST")){
				getUserMessageDAO().persist(usermessageInbox);
			} else {
				getUserMessageDAO().persist(usermessageOutbox);
				getUserMessageDAO().persist(usermessageInbox);
			}
			
			if(userReceiver.getMailNotification() != null && BooleanUtils.isTrue(userReceiver.getMailNotification())) {
				EmailMessageUser emailMessage = new EmailMessageUser();
				emailMessage.setSubject("You received a new message");
				emailMessage.setBody(MessageFormat.format("{0} sent you a message. Check your inbox to read it.", userSender.getAccount()));
				emailMessage.setMailSended(false);
				emailMessage.setUser(userReceiver);
				
				emailMessageUserDAO.persist(emailMessage);	
			}

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Message added in DB.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson deleteMessageById(Integer messageId)
			throws ApplicationThrowable

	{
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");

		try {

			UserMessage message = getUserMessageDAO()
					.findMessageById(messageId);
			if (message != null) {
				getUserMessageDAO().remove(message);
				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("Message deleted.");
				return resp;
			}

			resp.setStatus(StatusType.w.toString());
			resp.setMessage("Message not found in DB.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson deleteMultipleMessages(List<Integer> messageIds)
			throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		List<Integer> notDeleted = new ArrayList<Integer>();

		try {

			if (messageIds == null || messageIds.isEmpty()) {
				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("MessagesId list is empty.");
				return resp;
			}

			for (Integer messageId : messageIds) {
				UserMessage message = getUserMessageDAO().findMessageById(
						messageId);
				if (message != null) {
					getUserMessageDAO().remove(message);

				} else {
					notDeleted.add(messageId);
				}

			}

			if (notDeleted.isEmpty()) {
				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("All Message deleted.");
			} else {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("Messages deleted, but some messages with Ids "
						+ notDeleted.toString() + " not found in DB. ");
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public MessageSelectedJson getMessage(Integer messageId)
			throws ApplicationThrowable {

		try {

			UserMessage message = getUserMessageDAO()
					.findMessageById(messageId);

			if (message != null) {
				message.setReadedDate(new Date());
				getUserMessageDAO().merge(message);
				MessageSelectedJson mess = new MessageSelectedJson();
				mess.toJson(message);
				return mess;
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return null;

	}

	private List<UserMessage> getArraySubList(List<UserMessage> arr,
											  Integer page, Integer perPage) {

		final int limit = page * perPage;

		final int offset = limit - perPage;

		if (arr == null)
			return null;
		else if(arr.size() < offset)
			return arr.subList(0,0);

		if (arr.size() <= limit) {
			return arr.subList(offset, arr.size());
		} else {
			return arr.subList(offset, limit);
		}

	}

    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public Boolean sendNotification(User user, MessageSendJson messageJson) throws ApplicationThrowable {
	    try {
            UserMessage message = messageJson.toEntityInbox(new UserMessage());
            message.setUser(user);
            userMessageDAO.persist(message);
            if (user.getMailNotification() != null && BooleanUtils.isTrue(user.getMailNotification())) {
                EmailMessageUser emailMessage = new EmailMessageUser();
                emailMessage.setSubject("You received a new message");
                emailMessage.setBody(MessageFormat.format("{0} sent you a message. Check your inbox to read it.", messageJson.getAccount()));
                emailMessage.setMailSended(false);
                emailMessage.setUser(user);
                emailMessageUserDAO.persist(emailMessage);
            }
            return false;
        } catch (Throwable th) {
	        throw new ApplicationThrowable();
        }
	}

	public UserMessageDAO getUserMessageDAO() {
		return userMessageDAO;
	}

	public void setUserMessageDAO(UserMessageDAO userMessageDAO) {
		this.userMessageDAO = userMessageDAO;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	@Override
	public void onDocumentEdited(String account, String to, Integer documentId) {
		String docUrl = ApplicationPropertyManager.getApplicationProperty("website.protocol") + "://"
				+ ApplicationPropertyManager.getApplicationProperty("website.domain")
				+ ApplicationPropertyManager.getApplicationProperty("website.contextPath")
				+ "index.html#/mia/document-entity/" + documentId;
		onRecordEdited(account, to, docUrl);
	}

	private void onRecordEdited(String account, String to, String recordUrl) {
		try {
			MessageSendJson message = new MessageSendJson();
			message.setAccount(account);
			message.setMessageSubject(ApplicationPropertyManager.getApplicationProperty(
					"message.recordEditedNotification.subject"));
			message.setMessageText(ApplicationPropertyManager.getApplicationProperty(
					"message.recordEditedNotification.text", new String[] { account, recordUrl }, "{", "}"));
			message.setTo(to);

			User userSender = new User();
			userSender.setAccount(message.getAccount());

			User userReceiver = getUserService().findUser(message.getTo());

			UserMessage usermessageInbox = new UserMessage();
			message.toEntityInbox(usermessageInbox);
			usermessageInbox.setUser(userReceiver);

			getUserMessageDAO().persist(usermessageInbox);
			
			if (userReceiver.getMailNotification() != null && BooleanUtils.isTrue(userReceiver.getMailNotification())) {
				EmailMessageUser emailMessage = new EmailMessageUser();
				emailMessage.setSubject(ApplicationPropertyManager.getApplicationProperty(
						"mail.recordEditedNotification.subject"));
				emailMessage.setBody(ApplicationPropertyManager.getApplicationProperty(
						"mail.recordEditedNotification.text", new String[] { account, recordUrl }, "{", "}"));
				emailMessage.setMailSended(false);
				emailMessage.setUser(userReceiver);

				emailMessageUserDAO.persist(emailMessage);
			}
		} catch (Throwable t) {
			logger.error("An error occurred while sending notification on record edited", t);
		}
	}
	
	@Override
	public void onAnnotationQuestionAdded(List<AnnotationQuestion> newAnnotationQuestions) {
		if (newAnnotationQuestions != null) {
			for (AnnotationQuestion newAnnotationQuestion : newAnnotationQuestions) {
				Annotation annotation = newAnnotationQuestion.getAnnotation();
				String owner = annotation.getUser().getAccount();
				UploadFileEntity uploadFileEntity = annotation.getUploadFile();
				String url = ApplicationPropertyManager.getApplicationProperty("website.protocol") + "://"
						+ ApplicationPropertyManager.getApplicationProperty("website.domain")
						+ ApplicationPropertyManager.getApplicationProperty("website.contextPath")
						+ "json/src/ShowArchivalLocationInManuscriptViewer/show?volumeId="
						+ uploadFileEntity.getUploadInfoEntity().getVolume() + "&fileId="
						+ uploadFileEntity.getUploadFileId();
				Set<String> recipients = new HashSet<>(Arrays.asList(owner));
				for (AnnotationQuestion annotationQuestion : annotation.getQuestions()) {
					recipients.add(annotationQuestion.getUser().getAccount());
				}
				for (String to : recipients) {
					String account = newAnnotationQuestion.getUser().getAccount();
					if (!to.equals(account)) {
						onAnnotationQuestionAdded(account, to, url);
					}
				}
			}
		}
	}
	
	private void onAnnotationQuestionAdded(String account, String to, String url) {
		try {
			MessageSendJson message = new MessageSendJson();
			message.setAccount(account);
			message.setMessageSubject(ApplicationPropertyManager.getApplicationProperty(
					"message.annotationQuestionNotification.subject"));
			message.setMessageText(ApplicationPropertyManager.getApplicationProperty(
					"message.annotationQuestionNotification.text", new String[] { account, url }, "{", "}"));
			message.setTo(to);

			User userSender = new User();
			userSender.setAccount(message.getAccount());

			User userReceiver = getUserService().findUser(message.getTo());

			UserMessage usermessageInbox = new UserMessage();
			message.toEntityInbox(usermessageInbox);
			usermessageInbox.setUser(userReceiver);

			getUserMessageDAO().persist(usermessageInbox);
			
			if (userReceiver.getMailNotification() != null && BooleanUtils.isTrue(userReceiver.getMailNotification())) {
				EmailMessageUser emailMessage = new EmailMessageUser();
				emailMessage.setSubject(ApplicationPropertyManager.getApplicationProperty(
						"mail.annotationQuestionNotification.subject"));
				emailMessage.setBody(ApplicationPropertyManager.getApplicationProperty(
						"mail.annotationQuestionNotification.text", new String[] { account, url }, "{", "}"));
				emailMessage.setMailSended(false);
				emailMessage.setUser(userReceiver);

				emailMessageUserDAO.persist(emailMessage);
			}
		} catch (Throwable t) {
			logger.error("An error occurred while sending notification on annotation question added", t);
		}
	}

}