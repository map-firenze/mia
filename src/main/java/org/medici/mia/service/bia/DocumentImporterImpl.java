/*
 * VolBaseServiceImpl.java
 *
 * Developed by Medici Archive Project (2010-2012).
 *
 * This file is part of DocSources.
 *
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.service.bia;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NoResultException;
import javax.persistence.OneToMany;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.medici.mia.common.json.InsertDescriptionJson;
import org.medici.mia.common.json.InsertGuardiaJson;
import org.medici.mia.common.json.InsertGuardiaUpdateJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.VolumeDescriptionJson;
import org.medici.mia.common.json.VolumeSpineJson;
import org.medici.mia.common.json.VolumeSpineUpdateJson;
import org.medici.mia.common.json.document.DocumentFactoryJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.pagination.HistoryNavigator;
import org.medici.mia.common.pagination.Page;
import org.medici.mia.common.pagination.PaginationFilter;
import org.medici.mia.common.pagination.VolumeExplorer;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.common.util.DateUtils;
import org.medici.mia.common.util.DocumentUtils;
import org.medici.mia.common.util.ForumUtils;
import org.medici.mia.common.util.HtmlUtils;
import org.medici.mia.common.util.VolumeUtils;
import org.medici.mia.common.volume.FoliosInformations;
import org.medici.mia.common.volume.VolumeInsert;
import org.medici.mia.common.volume.VolumeSummary;
import org.medici.mia.dao.document.DocumentDAO;
import org.medici.mia.dao.forum.ForumDAO;
import org.medici.mia.dao.forumoption.ForumOptionDAO;
import org.medici.mia.dao.image.ImageDAO;
import org.medici.mia.dao.insert.InsertDAO;
import org.medici.mia.dao.miadoc.DocumentProducerDao;
import org.medici.mia.dao.miadoc.DocumentRefToPeopleDao;
import org.medici.mia.dao.miadoc.DocumentTranscriptionDAO;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.dao.miadoc.TopicPlaceDao;
import org.medici.mia.dao.month.MonthDAO;
import org.medici.mia.dao.schedone.SchedoneDAO;
import org.medici.mia.dao.serieslist.SeriesListDAO;
import org.medici.mia.dao.synextract.SynExtractDAO;
import org.medici.mia.dao.user.UserDAO;
import org.medici.mia.dao.userhistory.UserHistoryDAO;
import org.medici.mia.dao.usermarkedlist.UserMarkedListDAO;
import org.medici.mia.dao.usermarkedlistelement.UserMarkedListElementDAO;
import org.medici.mia.dao.volume.VolumeDAO;
import org.medici.mia.domain.Document;
import org.medici.mia.domain.DocumentRefToPeopleEntity;
import org.medici.mia.domain.EpLink;
import org.medici.mia.domain.EplToLink;
import org.medici.mia.domain.Forum;
import org.medici.mia.domain.ForumOption;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.domain.Image;
import org.medici.mia.domain.Image.ImageType;
import org.medici.mia.domain.BiblioRefEntity;
import org.medici.mia.domain.CorrespondenceEntity;
import org.medici.mia.domain.DocumentProducerJoinEntity;
import org.medici.mia.domain.DocumentTranscriptionEntity;
import org.medici.mia.domain.IGenericEntity;
import org.medici.mia.domain.InsertEntity;
import org.medici.mia.domain.LanguageEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.Month;
import org.medici.mia.domain.People;
import org.medici.mia.domain.Schedone;
import org.medici.mia.domain.SerieList;
import org.medici.mia.domain.SynExtract;
import org.medici.mia.domain.TopicPlaceEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.User;
import org.medici.mia.domain.UserHistory;
import org.medici.mia.domain.UserHistory.Action;
import org.medici.mia.domain.UserHistory.Category;
import org.medici.mia.domain.UserMarkedList;
import org.medici.mia.domain.UserMarkedListElement;
import org.medici.mia.domain.Volume;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.security.MiaUserDetailsImpl;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.miadoc.DocumentType;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import de.danielbechler.diff.ObjectDifferBuilder;
import de.danielbechler.diff.node.DiffNode;
import de.danielbechler.diff.node.Visit;
import de.danielbechler.diff.selector.BeanPropertyElementSelector;
import de.danielbechler.diff.selector.ElementSelector;

@Service
@Transactional(readOnly = true)
public class DocumentImporterImpl implements DocumentImporter {
	@Autowired
	private DocumentDAO documetDAO;
	@Autowired
	private ForumDAO forumDAO;
	@Autowired
	private ForumOptionDAO forumOptionDAO;
	@Autowired
	private ImageDAO imageDAO;
	private final Logger logger = Logger.getLogger(this.getClass());
	@Autowired
	private MonthDAO monthDAO;
	@Autowired
	private SchedoneDAO schedoneDAO;
	@Autowired
	private SeriesListDAO seriesListDAO;
	@Autowired
	private UserDAO userDAO;
	@Autowired
	private UserHistoryDAO userHistoryDAO;
	@Autowired
	private UserMarkedListDAO userMarkedListDAO;
	@Autowired
	private UserMarkedListElementDAO userMarkedListElementDAO;
	@Autowired
	private VolumeDAO volumeDAO;
	@Autowired
	private InsertDAO insertDAO;
	@Autowired
	private HistoryLogService historyLogService;

	@Autowired
	private GenericDocumentDao genericDocumentDao;

	@Autowired
	private DocumentProducerDao documentProducerDao;

	@Autowired
	private SynExtractDAO synExtractDAO;

	@Autowired
	private DocumentTranscriptionDAO documentTranscriptionDAO;
	
	@Autowired
	private TopicPlaceDao topicPlaceDao;
	
	@Autowired
	private DocumentRefToPeopleDao documentRefToPeopleDao;
	
	@Override
	public void updateMiaDocuments() {
		documetDAO.updateMiaDocuments();
	}
	@Override
	public List<Document> findDocumentsImportLock() {
		List<Document> docsBia = documetDAO.findDocumentsImportLock();
		return docsBia;
	}
	
	
	//@Transactional(readOnly=false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	@Override
	public void insertDocument(FileWriter errorFile ,Document documentBia) throws IOException, RuntimeException {
		//inert into tblDocumentEnts (documentoid , datacreazione, data modifica ) 
		//select entryid, 0000-00-00 , 0000-00-00 from tblDocuments where entryid non in 
		//(select documentoid from tblDocumentEnts )
		try {
			SynExtract synExtract = documentBia.getSynExtract();
			// Ricerca documento su mia
			MiaDocumentEntity miaDoc = genericDocumentDao
					.findDocumentById(documentBia.getEntryId());
	
			if (miaDoc != null) {
				if (documentBia.getImportDate() == null  || !miaDoc.getDateLastUpdate().after(
						documentBia.getImportDate())) {
					// Aggiorna MiaDocumentEntity
					try {
						updateDocumentFromBia(miaDoc, documentBia, synExtract);
						documentBia.setImportDate(new Date());
						//documentBia.setImportLock(true);
						documetDAO.merge(documentBia);
					} catch (Throwable e) {
						e.printStackTrace();
						errorFile.append("Errore nell'update del documento con ID: "+ documentBia.getEntryId() + ", con errore:  " +  e.getMessage() + "\n");
					}
				}
			} 
		}
		catch (Throwable e) {
			e.printStackTrace();
			errorFile.append("Errore nell'update del documento con ID: "+ documentBia.getEntryId() + ", con errore:  " +  e.getMessage() + "\n");
		}
	}
	

	private MiaDocumentEntity fromBia(MiaDocumentEntity miaDoc,
			Document documentBia, SynExtract synExtract) {
		miaDoc.setTypology("Letter");
		miaDoc.setCategory("correspondence");
		miaDoc.setPrivacy(0);
		miaDoc.setDocumentEntityId(documentBia.getEntryId());
		// miaDoc.setFromBIA(1);
		if (synExtract == null) {
			miaDoc.setDeTitle("");
		} else if (synExtract.getSynopsis() == null) {
			miaDoc.setDeTitle("-");
		} else if (synExtract.getSynopsis().length() < 60) {
			miaDoc.setDeTitle(synExtract.getSynopsis());
		} else {
			miaDoc.setDeTitle(synExtract.getSynopsis().substring(0,60));
		}
		miaDoc.setDateCreated(documentBia.getDateCreated());
		miaDoc.setCreatedBy(documentBia.getCreatedBy().getAccount());
		miaDoc.setDateLastUpdate(documentBia.getLastUpdate());
		if (documentBia.getLastUpdateBy() == null) {
			miaDoc.setLastUpdateBy(documentBia.getCreatedBy().getAccount());
		} else {
			miaDoc.setLastUpdateBy(documentBia.getLastUpdateBy().getAccount());
		}

		miaDoc.setDocYear(documentBia.getDocYear());
		if(documentBia.getDocMonthNum() != null) {
			miaDoc.setDocMonth(documentBia.getDocMonthNum().getMonthNum());
		}
		miaDoc.setDocDay(documentBia.getDocDay());

		miaDoc.setDocModernYear(documentBia.getYearModern());

		miaDoc.setDocUndated(documentBia.getUndated() ? 1 : 0);
		miaDoc.setDocDateUncertain(documentBia.getDateUns() ? 1 : 0);
		if (documentBia.getDateApprox() != null && documentBia.getDateApprox() != null) {
			if (!documentBia.getDateApprox().equalsIgnoreCase(documentBia.getDateNotes())) {
				miaDoc.setDocDateNotes("Date approx: " + documentBia.getDateApprox() + " - Date notes: " + documentBia.getDateNotes());
			} else {
				miaDoc.setDocDateNotes(documentBia.getDateApprox());
			}
		} else {
			if (documentBia.getDateApprox() != null ) {
				miaDoc.setDocDateNotes(documentBia.getDateApprox());
			}
			if (documentBia.getDateNotes() != null ) {
				miaDoc.setDocDateNotes(documentBia.getDateNotes());
			}
		}
		miaDoc.setFlgPrinted(0);
		miaDoc.setFlgLogicalDelete(documentBia.getLogicalDelete());
		miaDoc.setPlaceOfOriginUnsure(documentBia.getSenderPeopleUnsure() ? "1"
				: "0");
		miaDoc.setPlaceOfOrigin(documentBia.getSenderPlace() != null ? documentBia
				.getSenderPlace().getPlaceAllId() : null);

		if (synExtract != null) {
			miaDoc.setGeneralNotesSynopsis(synExtract.getSynopsis());
			String transSearch = "- " + synExtract.getDocExtract();
			miaDoc.setTransSearch(transSearch);
		}
		
		if (documentBia.getRecipientPeople() != null) {
			CorrespondenceEntity corrEnt = new CorrespondenceEntity();
			corrEnt.setDocumentEntityId(documentBia.getEntryId());
			corrEnt.setRecipient(documentBia.getRecipientPeople() != null ? documentBia
					.getRecipientPeople().getPersonId().toString() + ( documentBia.getRecipientPeopleUnsure() ? ":u" : ":s")
					: null);
			corrEnt.setRecipientPlace(documentBia.getRecipientPlace() != null ? documentBia
					.getRecipientPlace().getPlaceAllId() + ( documentBia.getRecipientPlaceUnsure() ? ":u" : ":s")
					: null);
			miaDoc.setCorrespondence(corrEnt);
		}

		
		return miaDoc;
	}

	private void updateBaseDocumentFromBia(MiaDocumentEntity miaDoc,
			Document documentBia, SynExtract synExtract) {
		
		miaDoc = fromBia(miaDoc, documentBia, synExtract);
		genericDocumentDao.updateDE(miaDoc, miaDoc.getLastUpdateBy(), documentBia.getLastUpdate());
	}
		
		
	private void updateDocumentFromBia(MiaDocumentEntity miaDoc,
			Document documentBia, SynExtract synExtract) {
		
		miaDoc = fromBia(miaDoc, documentBia, synExtract);

		genericDocumentDao.updateDE(miaDoc, miaDoc.getLastUpdateBy(), documentBia.getLastUpdate());
		
		if (documentBia.getEpLink() != null) {
		
		    List<DocumentRefToPeopleEntity> documentRefToPeoples = documentRefToPeopleDao.findDocRefToPeople(documentBia.getEntryId());
		    List<DocumentRefToPeopleEntity> documentRefToPeopleFromBia = createRefToPeople (miaDoc, documentBia);

			for (DocumentRefToPeopleEntity documentRefToPeopleEntity : documentRefToPeopleFromBia) {
				DocumentRefToPeopleEntity documentRefToPeople = findRefToPeople( documentRefToPeoples, documentRefToPeopleEntity);
				if (documentRefToPeople == null) {
					documentRefToPeopleDao.persist(documentRefToPeopleEntity);
				}
			}

		}
		
		
		
		if (documentBia.getSenderPeople() != null) {
			DocumentProducerJoinEntity prodJ =  null;
			try {
				prodJ =  documentProducerDao.findDocProducer(documentBia.getEntryId(), documentBia.getSenderPeople().getPersonId());
			} catch (NoResultException e) {
				 prodJ = null;
			}
			if (prodJ == null) {
				prodJ = new DocumentProducerJoinEntity();
				prodJ.setDocumentEntityId(documentBia.getEntryId());
				prodJ.setProducer(documentBia.getSenderPeople().getPersonId());
				prodJ.setProducerUnsure(documentBia.getSenderPeopleUnsure() ? "u" : "s");
				documentProducerDao.insertDocProducerNative(prodJ);
				//documentProducerDao.persist(prodJ);
			} else {
				prodJ.setDocumentEntityId(documentBia.getEntryId());
				prodJ.setProducer(documentBia.getSenderPeople().getPersonId());
				prodJ.setProducerUnsure(documentBia.getSenderPeopleUnsure() ? "u" : "s");
				documentProducerDao.persist(prodJ);
			}

		}
		
		DocumentTranscriptionEntity documentTranscriptionEntity =  null;
		if (synExtract != null) {
			try {
				 List<IGenericEntity>  listTrasc =  documentTranscriptionDAO.findEntities(documentBia.getEntryId());
				 if (listTrasc != null && listTrasc.size() > 0) {
					 documentTranscriptionEntity = (DocumentTranscriptionEntity)listTrasc.get(0);
				 }
			} catch (NoResultException e) {
				documentTranscriptionEntity = null;
			}
			if (documentTranscriptionEntity == null) {
				DocumentTranscriptionEntity dte = new DocumentTranscriptionEntity();
				dte.setDocument(miaDoc);
				dte.setDocumentEntityId(documentBia.getEntryId());
				dte.setTranscription(synExtract.getDocExtract());
				documentTranscriptionDAO.saveTranscription(dte);
			} else {
				if (synExtract.getDocExtract() != null) {
					if (!synExtract.getDocExtract().equalsIgnoreCase(documentTranscriptionEntity.getTranscription())) {
						documentTranscriptionEntity.setDocumentEntityId(documentBia.getEntryId());
						documentTranscriptionEntity.setDocument(miaDoc);
						documentTranscriptionEntity.setTranscription(synExtract.getDocExtract());
						documentTranscriptionDAO.persist(documentTranscriptionEntity);
					}
				}
			}
		}

		
		//Trascrizioni da mettere in update 
		//Topic da testare per aggiornare
		//date di aggiornamento
		
	    List<TopicPlaceEntity> topicsPlaces = topicPlaceDao.findTopicPlaces(documentBia.getEntryId());
	    List<TopicPlaceEntity> topicsPlacesFromBia = createTopic (miaDoc, documentBia);
	
	    /*
	     * Non cancellare nulla da MIA
		for (TopicPlaceEntity topicPlaceEntity : topicsPlaces) {
			if (findTopic( topicsPlacesFromBia, topicPlaceEntity) == null) {
				topicPlaceDao.remove(topicPlaceEntity);
			}
		}*/
		for (TopicPlaceEntity topicPlaceEntity : topicsPlacesFromBia) {
			TopicPlaceEntity topicFindTopic = findTopic( topicsPlaces, topicPlaceEntity);
			if (topicFindTopic == null) {
				topicPlaceDao.persist(topicPlaceEntity);
			}
		}

		
		
	}
	
	private TopicPlaceEntity findTopic(List<TopicPlaceEntity> topicsPlacesFromBia, TopicPlaceEntity topicPlaceEntityMia) {
		for (TopicPlaceEntity topicPlaceEntity : topicsPlacesFromBia) {
			if (topicPlaceEntityMia.getPlaceId().intValue() == topicPlaceEntity.getPlaceId().intValue() 
					&& topicPlaceEntityMia.getTopicListId().intValue() == topicPlaceEntity.getTopicListId().intValue()) {
				return topicPlaceEntityMia;
				//Non fare nulla
			}
		}
		return null; //Da rimuovere
		
	}
	
	
	private DocumentRefToPeopleEntity findRefToPeople(List<DocumentRefToPeopleEntity> refToPeopleBia, DocumentRefToPeopleEntity refToPeopleMia) {
		for (DocumentRefToPeopleEntity refToPeopleEntity : refToPeopleBia) {
			if (refToPeopleMia.getPeopleId().intValue() == refToPeopleEntity.getPeopleId().intValue() ) {
				return refToPeopleMia;
				//Non fare nulla
			}
		}
		return null; //Da rimuovere
		
	}
	
	
	private List<DocumentRefToPeopleEntity> createRefToPeople (MiaDocumentEntity miaDoc,
			Document documentBia) {
		List<DocumentRefToPeopleEntity> refToPeople = new ArrayList<DocumentRefToPeopleEntity>();
		Set<EpLink> eptLinks = documentBia.getEpLink();
		for (EpLink epLink : eptLinks) {
			if (epLink.getDocRole() == null) {
				DocumentRefToPeopleEntity ent = new DocumentRefToPeopleEntity();
				ent.setDocumentId(miaDoc.getDocumentEntityId());
				ent.setPeopleId(epLink.getPerson().getPersonId());
				ent.setUnSure(epLink.getAssignUnsure() ? "u" : "s");
				
				refToPeople.add(ent);
			}
		}
		return refToPeople;
	}
	
	
	private List<TopicPlaceEntity> createTopic (MiaDocumentEntity miaDoc,
			Document documentBia) {
		List<TopicPlaceEntity> topicsPlaces = new ArrayList<TopicPlaceEntity>();
		Set<EplToLink> eptToLinks = documentBia.getEplToLink(); 
		for (EplToLink eplToLink : eptToLinks) {
			TopicPlaceEntity topic = new TopicPlaceEntity();
			topic.setMiaDocumentEntity(miaDoc);
			topic.setDocumentEntityId(documentBia.getEntryId());
			topic.setPlaceEntity(eplToLink.getPlace());
			topic.setPlaceId(eplToLink.getPlace().getPlaceAllId());
			topic.setTopicListId(eplToLink.getTopic().getTopicId());
			topic.setTopicListEntity(eplToLink.getTopic());
			topicsPlaces.add(topic);
		}
		return topicsPlaces;
	}

	
	

	// Import da tblDocuments a tblDocumentsEnts.
	// Aggiungere un campo su tblDocuments per bloccoimport
	// Importo tblDocumentsEnts sino a che le date di tblDocuments sono nuove
	// nel caso su MIA c'è un aggiornamento il record non viene importato e
	// viene bloccato il flusso.
	//
	// Popolare la tabelle dei topic da tblEptolink a tblDocumentsTopicPlace
	//
	// Thread deve prendere tutti i documenti con imagefound a 0 e cercare
	// Se esiste un immagine
	// Foluemnum foliomod

	public void ckeckDocument(Document docBia, MiaDocumentEntity docMia) {
		if (docMia == null) {
			// Insert document in mia
			return;
		}
		if (docBia.getLastUpdate().after(docMia.getDateLastUpdate())) {
			// Todo update
		}

	}

}
