/*
 * VolBaseService.java
 * 
 * Developed by Medici Archive Project (2010-2012).
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.service.bia;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.InsertGuardiaJson;
import org.medici.mia.common.json.InsertGuardiaUpdateJson;
import org.medici.mia.common.json.VolumeDescriptionJson;
import org.medici.mia.common.json.VolumeSpineJson;
import org.medici.mia.common.json.VolumeSpineUpdateJson;
import org.medici.mia.common.pagination.HistoryNavigator;
import org.medici.mia.common.pagination.Page;
import org.medici.mia.common.pagination.PaginationFilter;
import org.medici.mia.common.pagination.VolumeExplorer;
import org.medici.mia.common.volume.VolumeSummary;
import org.medici.mia.domain.Document;
import org.medici.mia.domain.Forum;
import org.medici.mia.domain.InsertEntity;
import org.medici.mia.domain.Month;
import org.medici.mia.domain.Schedone;
import org.medici.mia.domain.SerieList;
import org.medici.mia.domain.Volume;
import org.medici.mia.exception.ApplicationThrowable;

/**


 * 
 */
public interface DocumentImporter {


	void updateMiaDocuments();

	List<Document> findDocumentsImportLock();

	void insertDocument(FileWriter errorFile, Document documentBia)
			throws IOException;
	
}
