/*
 * ImagePreviewServiceImpl.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.service.manuscriptviewer;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.medici.mia.common.json.folio.FolioJson;
import org.medici.mia.dao.uploadfile.UploadFileDAO;
import org.medici.mia.domain.FolioEntity;
import org.medici.mia.domain.ImagePreview;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.exception.ApplicationThrowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Service
@Transactional(readOnly = true)
public class ImagePreviewServiceImpl implements ImagePreviewService {

	@Autowired
	private UploadFileDAO uploadFileDAO;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public ImagePreview getImagePreview(Integer uploadFileId)
			throws ApplicationThrowable {

		ImagePreview image = new ImagePreview();
		// Getting the Upload File Entity
		UploadFileEntity uploadFile = getUploadFileDAO().findUploadFile(
				uploadFileId);

		if (uploadFile != null) {

			// Build of the real path
			// Issue 618 If the inserEntity has no value, insertName is null
			String insName = null;
			if (uploadFile.getUploadInfoEntity().getInsertEntity() != null) {
				insName = uploadFile.getUploadInfoEntity().getInsertEntity()
						.getInsertName();
			}
			String realPath = org.medici.mia.common.util.FileUtils
					.getRealPathImagePreview(uploadFile.getUploadInfoEntity()
							.getRepositoryEntity().getLocation(), uploadFile
							.getUploadInfoEntity().getRepositoryEntity()
							.getRepositoryAbbreviation(), uploadFile
							.getUploadInfoEntity().getCollectionEntity()
							.getCollectionAbbreviation(), uploadFile
							.getUploadInfoEntity().getVolumeEntity()
							.getVolume(), insName);

			// First way to change the filename format from jpg to tif
			String filename = uploadFile.getFilename();
			String filenameNoFormat = filename.substring(0,
					filename.lastIndexOf('.'));
			String filenameTif = filenameNoFormat + ".tif";
			// Second way to change the filename format from jpg to tif - It
			// works too
			String filenameNoFormat2 = FilenameUtils.removeExtension(filename);
			String filenameTif2 = filenameNoFormat2 + ".tif";

			// Setting the ImagePreview
			image.setRealPathTIF(realPath + filenameTif);
			image.setImageOrder(uploadFile.getImageOrder());
			image.setVolNum(uploadFile.getUploadInfoEntity().getVolume());
			if (uploadFile.getUploadInfoEntity().getRepositoryEntity() != null) {
				image.setRepositoryName(uploadFile.getUploadInfoEntity()
						.getRepositoryEntity().getRepositoryName());
			}
			if (uploadFile.getUploadInfoEntity().getCollectionEntity() != null) {
				image.setCollectionName(uploadFile.getUploadInfoEntity()
						.getCollectionEntity().getCollectionName());
			}

			if (uploadFile.getFolioEntities() != null
					&& !uploadFile.getFolioEntities().isEmpty()) {
				FolioJson firstFolioJson = new FolioJson();
				firstFolioJson.toJson(uploadFile.getFolioEntities().get(0));
				image.setFirstFolio(firstFolioJson);

				if (uploadFile.getFolioEntities().size() > 1) {
					FolioJson secondFolioJson = new FolioJson();
					secondFolioJson
							.toJson(uploadFile.getFolioEntities().get(1));
					image.setSecondFolio(secondFolioJson);
				}

			}
			if (uploadFile.getUploadInfoEntity().getInsertEntity() != null) {
				image.setInsertName(uploadFile.getUploadInfoEntity()
						.getInsertEntity().getInsertName());
			}
			if (uploadFile.getUploadInfoEntity().getSeriesEntity() != null) {
				image.setSeriesTitle(uploadFile.getUploadInfoEntity()
						.getSeriesEntity().getTitle());
			}
			if (uploadFile.getUploadInfoEntity().getVolumeEntity() != null) {
				image.setVolumeName(uploadFile.getUploadInfoEntity()
						.getVolumeEntity().getVolume());
			}

		} else {
			throw new ApplicationThrowable();
		}

		return image;
	}

	public UploadFileDAO getUploadFileDAO() {
		return uploadFileDAO;
	}

	public void setUploadFileDAO(UploadFileDAO uploadFileDAO) {
		this.uploadFileDAO = uploadFileDAO;
	}

}
