package org.medici.mia.service.genericsearch;

import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.complexsearch.DEComplexSearchJson;
import org.medici.mia.common.json.complexsearch.DEComplexSearchRespJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.search.AllCountWordSearchJson;
import org.medici.mia.common.json.search.AllCountWordSearchResponseJson;
import org.medici.mia.common.json.search.DEWordSearchJson;
import org.medici.mia.controller.genericsearch.DESearchPaginationParam;
import org.medici.mia.exception.ApplicationThrowable;

public interface GenericSearchService {

	public GenericResponseDataJson<HashMap<String, List<DocumentJson>>> findDEntitiesBySearchWords(
			DEWordSearchJson searchReq) throws ApplicationThrowable;

	public AllCountWordSearchResponseJson findAllCountBySearchWords(
			AllCountWordSearchJson searchReq) throws ApplicationThrowable;

	public AllCountWordSearchResponseJson findAllCount()
			throws ApplicationThrowable;

	public GenericResponseDataJson<DEComplexSearchRespJson> findDEntitiesByComplexSearch(
			DEComplexSearchJson searchReq, DESearchPaginationParam pagParam)
			throws ApplicationThrowable;

}
