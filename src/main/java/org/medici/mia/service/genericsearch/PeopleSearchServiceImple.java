/*
 * PeopleSearchServiceImple.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.service.genericsearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataCountJson;
import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.PeopleJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.complexsearch.PeopleComplexSearchJson;
import org.medici.mia.common.json.search.AllCountWordSearchJson;
import org.medici.mia.common.json.search.PeopleWordSearchJson;
import org.medici.mia.common.json.search.PeopleWordSearchType;
import org.medici.mia.controller.genericsearch.PeopleSearchPaginationParam;
import org.medici.mia.dao.altname.AltNameDAO;
import org.medici.mia.dao.people.BiographicalPeopleDAO;
import org.medici.mia.dao.people.PeopleDAO;
import org.medici.mia.domain.AltName;
import org.medici.mia.domain.BiographicalPeople;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @author user
 *
 */
@Service
public class PeopleSearchServiceImple implements PeopleSearchService {

	@Autowired
	private AltNameDAO altNameDAO;

	@Autowired
	private PeopleDAO peopleDAO;
	
	@Autowired
	private BiographicalPeopleDAO biographicalPeopleDAO;

	@Autowired
	private MiaDocumentService miaDocumentService;

	@Autowired
	private GenericSearchService searchService;

	@Override
	public GenericResponseDataJson<HashMap<String, List<PeopleJson>>> findPeopleBySearchWords(
			PeopleWordSearchJson searchReq) throws ApplicationThrowable {
		GenericResponseDataJson<HashMap<String, List<PeopleJson>>> toret = new GenericResponseDataJson<HashMap<String, List<PeopleJson>>>();

		boolean searchAllTypes = searchReq.getSearchType().contains(
				PeopleWordSearchType.ALL_NAME_TYPES.toString());

		HashMap<String, List<PeopleJson>> data = null;

		if (searchAllTypes) {

			List<PeopleJson> list = getMiaDocumentService().findPeople(
					searchReq.getSearchString());

			toret.setStatus(StatusType.ok.toString());
			toret.setMessage("Got " + list.size() + " people with name: "
					+ searchReq.getSearchString());
			data = new HashMap<String, List<PeopleJson>>();
			data.put("data", list);
			toret.setData(data);
		} else {

			List<AltName> names = getAltNameDAO().findAltNamesByWordSearch(
					searchReq);

			List<PeopleJson> people = new ArrayList<PeopleJson>();

			for (AltName name : names) {
				PeopleJson person = new PeopleJson();
				person.toJson(name);
				people.add(person);
			}

			toret.setStatus(StatusType.ok.toString());
			toret.setMessage("Got " + names.size() + " people with name: "
					+ searchReq.getSearchString());
			data = new HashMap<String, List<PeopleJson>>();
			data.put("data", people);
			toret.setData(data);

		}

		return toret;
	}

	@Override
	public GenericResponseDataCountJson<HashMap<String, List<PeopleJson>>> findPeopleByComplexSearchWords(
			PeopleComplexSearchJson searchReq,
			PeopleSearchPaginationParam pagParam) throws ApplicationThrowable {
		GenericResponseDataCountJson<HashMap<String, List<PeopleJson>>> toret = new GenericResponseDataCountJson<HashMap<String, List<PeopleJson>>>();

		HashMap<String, List<PeopleJson>> data = null;

		List<PeopleJson> list = getMiaDocumentService().findComplexPeople(
				searchReq, pagParam);

		toret.setStatus(StatusType.ok.toString());
		toret.setMessage("Got " + list.size() + " people ");
		data = new HashMap<String, List<PeopleJson>>();
		data.put("data", list);
		AllCountWordSearchJson searchJson = new AllCountWordSearchJson();
		searchJson.setSearchType("ALL_NAME_TYPES");
		searchJson.setPartialSearchWords(searchReq.getPartialSearchWords());
		searchJson.setMatchSearchWords(searchReq.getMatchSearchWords());
		toret.setCount(peopleDAO.getAllCountPeople(searchJson));
		toret.setData(data);

		return toret;
	}
	
	@Override
	public GenericResponseDataCountJson<HashMap<String, List<PeopleJson>>> 
	findPeopleByOwner(
			PeopleComplexSearchJson searchReq,
			PeopleSearchPaginationParam pagParam) throws ApplicationThrowable {
		GenericResponseDataCountJson<HashMap<String, List<PeopleJson>>> toret = new GenericResponseDataCountJson<HashMap<String, List<PeopleJson>>>();

		HashMap<String, List<PeopleJson>> data = null;

		List<PeopleJson> list = getMiaDocumentService().findComplexPeople(
				searchReq, pagParam);

		toret.setStatus(StatusType.ok.toString());
		toret.setMessage("Got " + list.size() + " people ");
		data = new HashMap<String, List<PeopleJson>>();
		data.put("data", list);
		
		toret.setCount(getPeopleCount(searchReq.getOwner()));
		toret.setData(data);

		return toret;
	}

	@Override
	public List<String> getPeopleIdBySearchWords(PeopleWordSearchJson searchReq)
			throws ApplicationThrowable {

		List<AltName> names = getAltNameDAO().findAltNamesByWordSearch(
				searchReq);

		List<String> peopleId = null;

		if (names != null && !names.isEmpty()) {

			peopleId = new ArrayList<String>();

			for (AltName altName : names) {

				peopleId.add(altName.getPerson().getPersonId().toString());

			}
		}

		return peopleId;
	}
	
	private Integer getPeopleCount(String account) {
		List<BiographicalPeople> results = biographicalPeopleDAO.
				getBiographicalPeopleCreateByUser(account);
		
		if (results == null) {
			return 0;
		}
		
		return results.size();
	}
	
	public AltNameDAO getAltNameDAO() {
		return altNameDAO;
	}

	public void setAltNameDAO(AltNameDAO altNameDAO) {
		this.altNameDAO = altNameDAO;
	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

	public void setMiaDocumentService(MiaDocumentService miaDocumentService) {
		this.miaDocumentService = miaDocumentService;
	}

}
