package org.medici.mia.service.genericsearch;

import java.util.Arrays;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.medici.mia.common.json.complexsearch.AEComplexSearchJson;
import org.medici.mia.common.json.complexsearch.DEComplexSearchJson;
import org.medici.mia.common.json.complexsearch.PeopleComplexSearchJson;
import org.medici.mia.common.json.complexsearch.PlaceComplexSearchJson;
import org.medici.mia.common.json.search.AEWordSearchType;
import org.medici.mia.common.json.search.AllCountWordSearchJson;
import org.medici.mia.common.json.search.DEWordSearchType;
import org.medici.mia.common.json.search.PeopleWordSearchJson;
import org.medici.mia.common.json.search.PeopleWordSearchType;
import org.medici.mia.domain.AltName;

import com.mysql.jdbc.StringUtils;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public class ComplexSearchUtils {

	private static final Logger logger = Logger
			.getLogger(ComplexSearchUtils.class);

	private static final String AE_WORD_SEARCH_QSTR_ONLY_MINE = " and owner = :owner";
	private static final String AE_WORD_SEARCH_QSTR_ONLY_THEIRS = " and owner <> :owner";

	private static final String DE_WORD_SEARCH_QSTR_ONLY_MINE = " and createdBy = :owner";
	private static final String DE_WORD_SEARCH_QSTR_ONLY_THEIRS = " and createdBy <> :owner";

	private static final String DE_NOT_DELETED_DOC = " and  flgLogicalDelete != 1";

	private static final String START_DE_QUERY_FOR_PRODUCER_ORDER_BY = "SELECT distinct * FROM tblDocumentEnts d"
			+ " LEFT JOIN tblDocumentsEntsPeople dp ON (dp.documentEntityId = d.documentEntityId)"
			+ " LEFT JOIN tblPeople p ON (dp.producer = p.personId) ";

	private static final String START_DE_QUERY = "SELECT distinct * FROM tblDocumentEnts d ";

	public static String getAEComplexQueryString(AEComplexSearchJson searchReq) {

		String queryString = null;
		
		// Search only by owner
		if (searchReq.getSearchType().equals(
				AEWordSearchType.AE_OWNER.toString())) {
			queryString = 
					  "SELECT DISTINCT * FROM tblUploads u "
					+ "WHERE u.owner = :owner "
					+ "AND u.logicalDelete = 0 ";
		}
		else {
			if (searchReq.getSearchType().equals(
					AEWordSearchType.AE_NAME.toString())) {
				queryString = getAEQuery(searchReq.getMatchSearchWords(),
						searchReq.getPartialSearchWords(), "aeName");
			} else if (searchReq.getSearchType().equals(
					AEWordSearchType.AE_DESCRIPTION.toString())) {

				queryString = getAEQuery(searchReq.getMatchSearchWords(),
						searchReq.getPartialSearchWords(), "aeDescription");

			} else if (searchReq.getSearchType().equals(
					AEWordSearchType.AE_NAME_AND_DESCRIPTION.toString())) {
				queryString = getAEQuery(searchReq.getMatchSearchWords(),
						searchReq.getPartialSearchWords());
			}
			if (searchReq.isOnlyMyArchivalEnties()) {
				queryString = queryString + AE_WORD_SEARCH_QSTR_ONLY_MINE;
			} else if (searchReq.isEveryoneElsesArchivalEntities()) {
				queryString = queryString + AE_WORD_SEARCH_QSTR_ONLY_THEIRS;
			}
		}
		
		return queryString;
	}
	
	public static String getAECountComplexQueryString(AEComplexSearchJson searchReq) {

		String queryString = null;
		if (searchReq.getSearchType().equals(
				AEWordSearchType.AE_NAME.toString())) {
			queryString = getAECountQuery(searchReq.getMatchSearchWords(),
					searchReq.getPartialSearchWords(), "aeName");
		} else if (searchReq.getSearchType().equals(
				AEWordSearchType.AE_DESCRIPTION.toString())) {

			queryString = getAECountQuery(searchReq.getMatchSearchWords(),
					searchReq.getPartialSearchWords(), "aeDescription");

		} else if (searchReq.getSearchType().equals(
				AEWordSearchType.AE_NAME_AND_DESCRIPTION.toString())) {
			queryString = getAECountQuery(searchReq.getMatchSearchWords(),
					searchReq.getPartialSearchWords());
		}
		if (searchReq.isOnlyMyArchivalEnties()) {
			queryString = queryString + AE_WORD_SEARCH_QSTR_ONLY_MINE;
		} else if (searchReq.isEveryoneElsesArchivalEntities()) {
			queryString = queryString + AE_WORD_SEARCH_QSTR_ONLY_THEIRS;
		}

		return queryString;
	}

	public static String getAllCountAEComplexQueryString(
			AllCountWordSearchJson searchReq) {

		return getAEAllCountQuery(searchReq.getMatchSearchWords(),
				searchReq.getPartialSearchWords());
	}

	private static String getAEQuery(String matchSearchWord,
			String likeSearchWord, String field) {

		StringBuilder jpaQuery = null;
		if (matchSearchWord != null && !matchSearchWord.isEmpty()) {

			jpaQuery = new StringBuilder();

			String[] matchwords = matchSearchWord.split("\\s+");
			jpaQuery.append("select distinct * from tblUploads where match("
					+ field + ") against ('");
			jpaQuery.append(matchwords[0] + "')");

			for (int i = 1; i < matchwords.length; i++) {
				jpaQuery.append(" AND match(" + field + ") against ('");
				jpaQuery.append(matchwords[i] + "')");

			}
		}

		if (likeSearchWord != null && !likeSearchWord.isEmpty()) {

			String[] likewords = likeSearchWord.split("\\s+");

			if (jpaQuery == null) {
				jpaQuery = new StringBuilder();
				jpaQuery.append("select distinct * from tblUploads  where ");
			} else {
				jpaQuery.append(" AND ");
			}

			jpaQuery.append(field + " like '%");
			jpaQuery.append(likewords[0] + "%'");

			for (int i = 1; i < likewords.length; i++) {
				jpaQuery.append(" AND " + field + " like '%");
				jpaQuery.append(likewords[i] + "%'");

			}
		}

		if (jpaQuery != null && !jpaQuery.equals("")) {
			jpaQuery.append(" AND logicalDelete != 1");
			logger.info(jpaQuery.toString());
			return jpaQuery.toString();
		}

		return null;

	}
	private static String getAECountQuery(String matchSearchWord,
			String likeSearchWord, String field) {

		StringBuilder jpaQuery = null;
		if (matchSearchWord != null && !matchSearchWord.isEmpty()) {

			jpaQuery = new StringBuilder();

			String[] matchwords = matchSearchWord.split("\\s+");
			jpaQuery.append("select distinct count(*) from tblUploads where match("
					+ field + ") against ('");
			jpaQuery.append(matchwords[0] + "')");

			for (int i = 1; i < matchwords.length; i++) {
				jpaQuery.append(" AND match(" + field + ") against ('");
				jpaQuery.append(matchwords[i] + "')");

			}
		}

		if (likeSearchWord != null && !likeSearchWord.isEmpty()) {

			String[] likewords = likeSearchWord.split("\\s+");

			if (jpaQuery == null) {
				jpaQuery = new StringBuilder();
				jpaQuery.append("select distinct count(*) from tblUploads  where ");
			} else {
				jpaQuery.append(" AND ");
			}

			jpaQuery.append(field + " like '%");
			jpaQuery.append(likewords[0] + "%'");

			for (int i = 1; i < likewords.length; i++) {
				jpaQuery.append(" AND " + field + " like '%");
				jpaQuery.append(likewords[i] + "%'");

			}
		}

		if (jpaQuery != null && !jpaQuery.equals("")) {
			jpaQuery.append(" AND logicalDelete != 1");
			logger.info(jpaQuery.toString());
			return jpaQuery.toString();
		}

		return null;

	}

	public static String getPeopleFromAltNameComplexQueryString(
			PeopleComplexSearchJson searchReq) {

		StringBuilder jpaQuery = null;
		if (searchReq.getMatchSearchWords() != null
				&& !searchReq.getMatchSearchWords().isEmpty()) {

			jpaQuery = new StringBuilder();

			String[] matchwords = searchReq.getMatchSearchWords().split("\\s+");
			jpaQuery.append("select distinct * from tblAltNames  where match("
					+ "altName" + ") against ('");
			jpaQuery.append(matchwords[0] + "')");

			for (int i = 1; i < matchwords.length; i++) {
				jpaQuery.append(" AND match(" + "altName" + ") against ('");
				jpaQuery.append(matchwords[i] + "')");

			}
		}

		if (searchReq.getPartialSearchWords() != null
				&& !searchReq.getPartialSearchWords().isEmpty()) {

			String[] likewords = searchReq.getPartialSearchWords()
					.split("\\s+");

			if (jpaQuery == null) {
				jpaQuery = new StringBuilder();
				jpaQuery.append("select distinct * from tblAltNames  where ");
			} else {
				jpaQuery.append(" AND ");
			}

			jpaQuery.append("altName" + " like '%");
			jpaQuery.append(likewords[0] + "%'");

			for (int i = 1; i < likewords.length; i++) {
				jpaQuery.append(" AND " + "altName" + " like '%");
				jpaQuery.append(likewords[i] + "%'");

			}
		}

		if (jpaQuery != null && !jpaQuery.equals("")) {
			if (!searchReq.getSearchType().contains(
					PeopleWordSearchType.ALL_NAME_TYPES.toString())) {
				jpaQuery.append(" AND ( ");
				for (int i = 0; i < searchReq.getSearchType().size() - 1; i++) {
					String nameType = searchReq.getSearchType().get(i);
					jpaQuery.append(" nameType = "
							+ jpaQuery.append(" nameType = " + "'" + nameType
									+ "'" + " OR "));
				}
				jpaQuery.append(" nameType = "
						+ "'"
						+ searchReq.getSearchType().get(
								searchReq.getSearchType().size() - 1) + "'");
				jpaQuery.append(" ) ");
			}

			logger.info(jpaQuery.toString());
			return jpaQuery.toString();
		}

		return null;

	}

	public static String getPlaceComplexQueryString(
			PlaceComplexSearchJson searchReq) {

		StringBuilder jpaQuery = null;
		if (searchReq.getMatchSearchWords() != null
				&& !searchReq.getMatchSearchWords().isEmpty()) {

			jpaQuery = new StringBuilder();

			String[] matchwords = searchReq.getMatchSearchWords().split("\\s+");
			jpaQuery.append("select distinct * from tblPlaces  where match("
					+ "placeName" + ") against ('");
			jpaQuery.append(matchwords[0] + "')");

			for (int i = 1; i < matchwords.length; i++) {
				jpaQuery.append(" AND match(" + "placeName" + ") against ('");
				jpaQuery.append(matchwords[i] + "')");

			}

		}

		if (searchReq.getPartialSearchWords() != null
				&& !searchReq.getPartialSearchWords().isEmpty()) {

			String[] likewords = searchReq.getPartialSearchWords()
					.split("\\s+");

			if (jpaQuery == null) {
				jpaQuery = new StringBuilder();
				jpaQuery.append("select distinct * from tblPlaces  where ");
			} else {
				jpaQuery.append(" AND ");
			}

			jpaQuery.append("placeName" + " like '%");
			jpaQuery.append(likewords[0] + "%'");

			for (int i = 1; i < likewords.length; i++) {
				jpaQuery.append(" AND " + "placeName" + " like '%");
				jpaQuery.append(likewords[i] + "%'");

			}
		}

		if (jpaQuery != null && !jpaQuery.equals("")) {
			if (searchReq.getPlaceType() != null
					&& !searchReq.getPlaceType().isEmpty()) {
				jpaQuery.append(" AND ( plType = ");
				jpaQuery.append(searchReq.getPlaceType());
				jpaQuery.append(" ) ");
			}

			jpaQuery.append(" AND logicalDelete = false");
			logger.info(jpaQuery.toString());

			return jpaQuery.toString();
		}

		return null;

	}

	public static String getPlaceAllCount(AllCountWordSearchJson searchReq) {

		StringBuilder jpaQuery = null;
		if (searchReq.getMatchSearchWords() != null
				&& !searchReq.getMatchSearchWords().isEmpty()) {

			jpaQuery = new StringBuilder();

			String[] matchwords = searchReq.getMatchSearchWords().split("\\s+");
			jpaQuery.append("select distinct PLACEALLID from tblPlaces  where match("
					+ "placeName" + ") against ('");
			jpaQuery.append(matchwords[0] + "')");

			for (int i = 1; i < matchwords.length; i++) {
				jpaQuery.append(" AND match(" + "placeName" + ") against ('");
				jpaQuery.append(matchwords[i] + "')");

			}

		}

		if (searchReq.getPartialSearchWords() != null
				&& !searchReq.getPartialSearchWords().isEmpty()) {

			String[] likewords = searchReq.getPartialSearchWords()
					.split("\\s+");

			if (jpaQuery == null) {
				jpaQuery = new StringBuilder();
				jpaQuery.append("select distinct * from tblPlaces  where ");
			} else {
				jpaQuery.append(" AND ");
			}

			jpaQuery.append("placeName" + " like '%");
			jpaQuery.append(likewords[0] + "%'");

			for (int i = 1; i < likewords.length; i++) {
				jpaQuery.append(" AND " + "placeName" + " like '%");
				jpaQuery.append(likewords[i] + "%'");

			}
		}

		if (jpaQuery != null && !jpaQuery.equals("")) {
			// if (searchReq.getPlaceType() != null
			// && !searchReq.getPlaceType().isEmpty()) {
			// jpaQuery.append(" AND ( plType = ");
			// jpaQuery.append(searchReq.getPlaceType());
			// jpaQuery.append(" ) ");
			// }

			jpaQuery.append(" AND logicalDelete = false");
			logger.info(jpaQuery.toString());

			return jpaQuery.toString();
		}

		return null;

	}


	private static String getMatchQueryForPeople(String[] matchwords){
		StringBuilder query = new StringBuilder();
		query.append("p.logicalDelete = 0 AND match(p.MAPNameLF) against ('");
		query.append(matchwords[0]);
		query.append("')");

		for (int i = 1; i < matchwords.length; i++) {
			query.append(" AND match(p.MAPNameLF) against ('");
			query.append(matchwords[0]);
			query.append("')");
		}
		return query.toString();
	}

	private static String getMatchQueryForAltNamesPeople(String[] matchwords){
		StringBuilder query = new StringBuilder();
		query.append("p.personid = alt.personid AND p.logicalDelete = 0 AND match(alt.altName) against ('");
		query.append(matchwords[0]);
		query.append("')");

		for (int i = 1; i < matchwords.length; i++) {
			query.append(" AND match(alt.altName) against ('");
			query.append(matchwords[0]);
			query.append("')");
		}
		return query.toString();
	}

	private static String getPartialQueryForPeople(String[] likewords){
		
		StringBuilder query = new StringBuilder();
		query.append("p.logicalDelete = 0 AND p.MAPNameLF LIKE '%");
		query.append(likewords[0]);
		query.append("%'");

		for (int i = 1; i < likewords.length; i++) {
			query.append(" AND p.MAPNameLF LIKE '%");
			query.append(likewords[i]);
			query.append("%'");
		}
		return query.toString();
	}

	private static String getPartialQueryForAltNamesPeople(String[] likewords){
		StringBuilder query = new StringBuilder();
		query.append("p.personId = alt.personId AND p.logicalDelete = 0 AND alt.altName LIKE '%");
		query.append(likewords[0]);
		query.append("%'");

		for (int i = 1; i < likewords.length; i++) {
			query.append(" AND alt.altName LIKE '%");
			query.append(likewords[i]);
			query.append("%'");
		}
		return query.toString();
	}

	public static String getPeopleComplexQueryString(PeopleComplexSearchJson searchReq) {

		StringBuilder jpaQuery = null;

		if (searchReq.getMatchSearchWords() == null
				|| !searchReq.getMatchSearchWords().isEmpty()
				|| searchReq.getPartialSearchWords() == null
				|| !searchReq.getPartialSearchWords().isEmpty()
				|| !StringUtils.isNullOrEmpty(searchReq.getOwner())) {

			jpaQuery = new StringBuilder();

			// Select distinct entities from unionized query
			jpaQuery.append("SELECT DISTINCT t.* FROM (");
			// Select from regular tblPeople, without relations
			jpaQuery.append("SELECT * FROM tblPeople p WHERE ");

			String matchWordsQuery = null;
			String partialWordsQuery = null;
			String ownerQuery = null;
			String[] matchwords = searchReq.getMatchSearchWords().split("\\s+");
			String[] likewords = searchReq.getPartialSearchWords().split("\\s+");

			// Check if we have match searchwords
			// if no, skip creating query for match
			if(searchReq.getMatchSearchWords() == null || !searchReq.getMatchSearchWords().isEmpty()) {
				matchWordsQuery = getMatchQueryForPeople(matchwords);
			}

			// Check if we have like searchwords
			// if no, skip creating query for like
			if(searchReq.getPartialSearchWords() == null || !searchReq.getPartialSearchWords().isEmpty()) {
				// if we have match query, then add AND at the start
				if(matchWordsQuery != null && !matchWordsQuery.isEmpty()){
					partialWordsQuery = " AND " + getPartialQueryForPeople(likewords);
				}
				else {
					partialWordsQuery = getPartialQueryForPeople(likewords);
				}
			}
			
			if (!StringUtils.isNullOrEmpty(searchReq.getOwner())) {
				ownerQuery = String.format(" p.createdBY = '%s' ", 
						searchReq.getOwner());
				if (!StringUtils.isNullOrEmpty(searchReq.getMatchSearchWords()) 
					||
					!StringUtils.isNullOrEmpty(searchReq.getPartialSearchWords())) 
				{
					ownerQuery = String.format(" AND p.createdBY = '%s' ", 
							searchReq.getOwner());
				}
			}

			// Append queries at the end
			if(matchWordsQuery != null) jpaQuery.append(matchWordsQuery);
			if(partialWordsQuery != null) jpaQuery.append(partialWordsQuery);
			if(ownerQuery != null) jpaQuery.append(ownerQuery);

			if (!StringUtils.isNullOrEmpty(searchReq.getMatchSearchWords()) || 
				!StringUtils.isNullOrEmpty(searchReq.getPartialSearchWords())) {
				// Unionize the queries
				jpaQuery.append(" UNION ");
				// Query tblAltNames and get entities by relation - personId column
				jpaQuery.append("SELECT p.* FROM  tblAltNames alt, tblPeople p WHERE ");

				// Check if we have match searchwords
				// if no, skip creating query for match
				if(searchReq.getMatchSearchWords() == null || !searchReq.getMatchSearchWords().isEmpty()) {
					matchWordsQuery = getMatchQueryForAltNamesPeople(matchwords);
				}

				// Check if we have like searchwords
				// if no, skip creating query for like
				if(searchReq.getPartialSearchWords() == null || !searchReq.getPartialSearchWords().isEmpty()) {
					if(matchWordsQuery != null && !matchWordsQuery.isEmpty()){
						partialWordsQuery = " AND " + getPartialQueryForAltNamesPeople(likewords);
					}
					else {
						partialWordsQuery = getPartialQueryForAltNamesPeople(likewords);
					}
				}

				// Check if we have like searchwords
				// if no, skip creating query for like
				if(matchWordsQuery != null) jpaQuery.append(matchWordsQuery);
				if(partialWordsQuery != null) jpaQuery.append(partialWordsQuery);
			}
			
			// Finish query construction by giving a table of unionized queries a name
			jpaQuery.append(") t");
		}


		if (jpaQuery != null && !jpaQuery.toString().isEmpty()) {


			logger.info("Query for MAPNameLF from tblPeople: "
					+ jpaQuery.toString());
			return jpaQuery.toString();
		}

		return null;

	}

	public static String getPeopleAllCount(AllCountWordSearchJson searchReq) {
		PeopleComplexSearchJson searchJson = new PeopleComplexSearchJson();
		searchJson.setMatchSearchWords(searchReq.getMatchSearchWords());
		searchJson.setPartialSearchWords(searchReq.getPartialSearchWords());
		searchJson.setSearchType(searchReq.getSearchType() == null ? null : Arrays.asList(searchReq.getSearchType().split("\\S+")));
		String query = getPeopleComplexQueryString(searchJson);
		if(query != null && !query.isEmpty()) {
			query = query.replaceAll("DISTINCT t\\.\\*", "COUNT(DISTINCT t.personId)");
		}
		return query;
	}


	public static String getPeopleFromTblPeopleComplexQueryString(
			PeopleComplexSearchJson searchReq) {

		StringBuilder jpaQuery = null;
		if (searchReq.getMatchSearchWords() != null
				&& !searchReq.getMatchSearchWords().isEmpty()) {

			jpaQuery = new StringBuilder();

			String[] matchwords = searchReq.getMatchSearchWords().split("\\s+");
			jpaQuery.append("select distinct * from tblPeople  where match("
					+ "MAPNameLF" + ") against ('");
			jpaQuery.append(matchwords[0] + "')");

			for (int i = 1; i < matchwords.length; i++) {
				jpaQuery.append(" AND match(" + "MAPNameLF" + ") against ('");
				jpaQuery.append(matchwords[i] + "')");

			}
		}

		if (searchReq.getPartialSearchWords() != null
				&& !searchReq.getPartialSearchWords().isEmpty()) {

			String[] likewords = searchReq.getPartialSearchWords()
					.split("\\s+");

			if (jpaQuery == null) {
				jpaQuery = new StringBuilder();
				jpaQuery.append("select distinct * from tblPeople  where ");
			} else {
				jpaQuery.append(" AND ");
			}

			jpaQuery.append("MAPNameLF" + " like '%");
			jpaQuery.append(likewords[0] + "%'");

			for (int i = 1; i < likewords.length; i++) {
				jpaQuery.append(" AND " + "MAPNameLF" + " like '%");
				jpaQuery.append(likewords[i] + "%'");

			}
		}

		if (jpaQuery != null && !jpaQuery.equals("")) {
			logger.info("Query for MAPNameLF from tblPeople: "
					+ jpaQuery.toString());
			return jpaQuery.toString();
		}

		return null;

	}

	public static String getPeopleAllCountFromTblPeople(
			AllCountWordSearchJson searchReq) {

		StringBuilder jpaQuery = null;
		if (searchReq.getMatchSearchWords() != null
				&& !searchReq.getMatchSearchWords().isEmpty()) {

			jpaQuery = new StringBuilder();

			String[] matchwords = searchReq.getMatchSearchWords().split("\\s+");
			jpaQuery.append("select distinct PERSONID from tblPeople  where match("
					+ "MAPNameLF" + ") against ('");
			jpaQuery.append(matchwords[0] + "')");

			for (int i = 1; i < matchwords.length; i++) {
				jpaQuery.append(" AND match(" + "MAPNameLF" + ") against ('");
				jpaQuery.append(matchwords[i] + "')");

			}
		}

		if (searchReq.getPartialSearchWords() != null
				&& !searchReq.getPartialSearchWords().isEmpty()) {

			String[] likewords = searchReq.getPartialSearchWords()
					.split("\\s+");

			if (jpaQuery == null) {
				jpaQuery = new StringBuilder();
				jpaQuery.append("select distinct PERSONID from tblPeople  where ");
			} else {
				jpaQuery.append(" AND ");
			}

			jpaQuery.append("MAPNameLF" + " like '%");
			jpaQuery.append(likewords[0] + "%'");

			for (int i = 1; i < likewords.length; i++) {
				jpaQuery.append(" AND " + "MAPNameLF" + " like '%");
				jpaQuery.append(likewords[i] + "%'");

			}
		}

		if (jpaQuery != null && !jpaQuery.equals("")) {
			logger.info("Query PeopleAllCount: " + jpaQuery.toString());
			jpaQuery.append(" and  LOGICALDELETE = 0");
			return jpaQuery.toString();
		}

		return null;

	}

	private static String getAEQuery(String matchSearchWord,
			String likeSearchWord) {

		StringBuilder jpaQuery = null;

		if (matchSearchWord != null && !matchSearchWord.isEmpty()) {

			jpaQuery = new StringBuilder();

			String[] matchwords = matchSearchWord.split("\\s+");
			jpaQuery.append("select distinct * from tblUploads where match("
					+ "aeName,aeDescription" + ") against ('");
			jpaQuery.append(matchwords[0] + "')");

			for (int i = 1; i < matchwords.length; i++) {
				jpaQuery.append(" AND match(" + "aeName,aeDescription"
						+ ") against ('");
				jpaQuery.append(matchwords[i] + "')");

			}
		}

		if (likeSearchWord != null && !likeSearchWord.isEmpty()) {

			String[] likewords = likeSearchWord.split("\\s+");

			if (jpaQuery == null) {
				jpaQuery = new StringBuilder();
				jpaQuery.append("select distinct * from tblUploads where ");
			} else {
				jpaQuery.append(" AND ");
			}

			jpaQuery.append("( aeName" + " like '%");
			jpaQuery.append(likewords[0] + "%'");
			jpaQuery.append(" OR aeDescription" + " like '%");
			jpaQuery.append(likewords[0] + "%' )");

			for (int i = 1; i < likewords.length; i++) {
				jpaQuery.append(" AND ");
				jpaQuery.append("( ");
				jpaQuery.append("aeName" + " like '%");
				jpaQuery.append(likewords[i] + "%'");
				jpaQuery.append(" OR aeDescription" + " like '%");
				jpaQuery.append(likewords[i] + "%'");
				jpaQuery.append(" )");

			}
		}

		if (jpaQuery != null && !jpaQuery.equals("")) {
			jpaQuery.append(" AND logicalDelete = false AND optionalImage is null ");
			logger.info("Querr AE search ALL: " + jpaQuery.toString());
			return jpaQuery.toString();
		}

		return null;

	}
	
	private static String getAECountQuery(String matchSearchWord,
			String likeSearchWord) {

		StringBuilder jpaQuery = null;

		if (matchSearchWord != null && !matchSearchWord.isEmpty()) {

			jpaQuery = new StringBuilder();

			String[] matchwords = matchSearchWord.split("\\s+");
			jpaQuery.append("select distinct count(*) from tblUploads where match("
					+ "aeName,aeDescription" + ") against ('");
			jpaQuery.append(matchwords[0] + "')");

			for (int i = 1; i < matchwords.length; i++) {
				jpaQuery.append(" AND match(" + "aeName,aeDescription"
						+ ") against ('");
				jpaQuery.append(matchwords[i] + "')");

			}
		}

		if (likeSearchWord != null && !likeSearchWord.isEmpty()) {

			String[] likewords = likeSearchWord.split("\\s+");

			if (jpaQuery == null) {
				jpaQuery = new StringBuilder();
				jpaQuery.append("select distinct count(*) from tblUploads where ");
			} else {
				jpaQuery.append(" AND ");
			}

			jpaQuery.append("( aeName" + " like '%");
			jpaQuery.append(likewords[0] + "%'");
			jpaQuery.append(" OR aeDescription" + " like '%");
			jpaQuery.append(likewords[0] + "%' )");

			for (int i = 1; i < likewords.length; i++) {
				jpaQuery.append(" AND ");
				jpaQuery.append("( ");
				jpaQuery.append("aeName" + " like '%");
				jpaQuery.append(likewords[i] + "%'");
				jpaQuery.append(" OR aeDescription" + " like '%");
				jpaQuery.append(likewords[i] + "%'");
				jpaQuery.append(" )");

			}
		}

		if (jpaQuery != null && !jpaQuery.equals("")) {
			jpaQuery.append(" AND logicalDelete = false AND optionalImage is null ");
			logger.info("Querr AE search ALL: " + jpaQuery.toString());
			return jpaQuery.toString();
		}

		return null;

	}

	private static String getAEAllCountQuery(String matchSearchWord,
			String likeSearchWord) {

		StringBuilder jpaQuery = null;

		if (matchSearchWord != null && !matchSearchWord.isEmpty()) {

			jpaQuery = new StringBuilder();

			String[] matchwords = matchSearchWord.split("\\s+");
			jpaQuery.append("select DISTINCT id from tblUploads where match("
					+ "aeName,aeDescription" + ") against ('");
			jpaQuery.append(matchwords[0] + "')");

			for (int i = 1; i < matchwords.length; i++) {
				jpaQuery.append(" AND match(" + "aeName,aeDescription"
						+ ") against ('");
				jpaQuery.append(matchwords[i] + "')");

			}
		}

		if (likeSearchWord != null && !likeSearchWord.isEmpty()) {

			String[] likewords = likeSearchWord.split("\\s+");

			if (jpaQuery == null) {
				jpaQuery = new StringBuilder();
				jpaQuery.append("select DISTINCT id from tblUploads where ");
			} else {
				jpaQuery.append(" AND ");
			}

			jpaQuery.append("( aeName" + " like '%");
			jpaQuery.append(likewords[0] + "%'");
			jpaQuery.append(" OR aeDescription" + " like '%");
			jpaQuery.append(likewords[0] + "%' )");

			for (int i = 1; i < likewords.length; i++) {
				jpaQuery.append(" AND ");
				jpaQuery.append("( ");
				jpaQuery.append("aeName" + " like '%");
				jpaQuery.append(likewords[i] + "%'");
				jpaQuery.append(" OR aeDescription" + " like '%");
				jpaQuery.append(likewords[i] + "%'");
				jpaQuery.append(" )");

			}
		}

		if (jpaQuery != null && !jpaQuery.equals("")) {
			jpaQuery.append(" AND logicalDelete != 1 AND optionalImage is null ");
			logger.info("Query getAEAllCountQuery: " + jpaQuery.toString());
			return jpaQuery.toString();
		}

		return null;

	}

	// DE search

	public static String getDEComplexQueryString(DEComplexSearchJson searchReq,
			boolean isOrderByProducer) {

		String queryString = null;
		if (searchReq.getSearchType().equals(
				DEWordSearchType.DE_SYNOPSIS.toString())) {
			queryString = getDEQuery(searchReq.getMatchSearchWords(),
					searchReq.getPartialSearchWords(), "generalNotesSynopsis", isOrderByProducer);
		} else if (searchReq.getSearchType().equals(
				DEWordSearchType.DE_TRANSCRIPTION.toString())) {

			queryString = getDEQuery(searchReq.getMatchSearchWords(),
					searchReq.getPartialSearchWords(), "transSearch", isOrderByProducer);

		} else if (searchReq.getSearchType().equals(
				DEWordSearchType.DE_TRANSCRIPTION_AND_SYNOPSIS.toString())) {
			queryString = getDEQuery(searchReq.getMatchSearchWords(),
					searchReq.getPartialSearchWords(), isOrderByProducer);
		} else {
			return null;
		}
		if (searchReq.isOnlyMyEntities()) {
			queryString = queryString + DE_WORD_SEARCH_QSTR_ONLY_MINE;
		} else if (searchReq.isEveryoneElsesEntities()) {
			queryString = queryString + DE_WORD_SEARCH_QSTR_ONLY_THEIRS;
		}

		return queryString;
	}

	// DE search

	public static String getDEComplexCountQueryString(
			DEComplexSearchJson searchReq) {

		String queryString = null;
		if (searchReq.getSearchType().equals(
				DEWordSearchType.DE_SYNOPSIS.toString())) {
			queryString = getDEQuery(searchReq.getMatchSearchWords(),
					searchReq.getPartialSearchWords(), "generalNotesSynopsis", false);
		} else if (searchReq.getSearchType().equals(
				DEWordSearchType.DE_TRANSCRIPTION.toString())) {

			queryString = getDEQuery(searchReq.getMatchSearchWords(),
					searchReq.getPartialSearchWords(), "transSearch", false);

		} else if (searchReq.getSearchType().equals(
				DEWordSearchType.DE_TRANSCRIPTION_AND_SYNOPSIS.toString())) {
			queryString = getDEQuery(searchReq.getMatchSearchWords(),
					searchReq.getPartialSearchWords(), false);
		} else {
			return null;
		}
		queryString = queryString == null ? null : queryString.replaceAll(
				"distinct \\*", "count(distinct d.documentEntityId)");
		if (searchReq.isOnlyMyEntities()) {
			queryString = queryString + DE_WORD_SEARCH_QSTR_ONLY_MINE;
		} else if (searchReq.isEveryoneElsesEntities()) {
			queryString = queryString + DE_WORD_SEARCH_QSTR_ONLY_THEIRS;
		}

		return queryString;
	}

	public static String getDEQuery(String matchSearchWord,
			String likeSearchWord, String field,  boolean isOrderByProducer) {

		StringBuilder jpaQuery = null;
		if (matchSearchWord != null && !matchSearchWord.isEmpty()) {

			jpaQuery = new StringBuilder();

			String[] matchwords = matchSearchWord.split("\\s+");
			field = "d."+field;
			if (isOrderByProducer) {
				jpaQuery.append(START_DE_QUERY_FOR_PRODUCER_ORDER_BY);
			} else {
				jpaQuery.append(START_DE_QUERY);
			}
			
			jpaQuery.append(" where match("
					+ field + ") against ('");
			jpaQuery.append(matchwords[0] + "')");

			for (int i = 1; i < matchwords.length; i++) {
				jpaQuery.append(" AND match(" + field + ") against ('");
				jpaQuery.append(matchwords[i] + "')");

			}
		}

		if (likeSearchWord != null && !likeSearchWord.isEmpty()) {

			String[] likewords = likeSearchWord.split("\\s+");

			if (jpaQuery == null) {
				jpaQuery = new StringBuilder();
				if (isOrderByProducer) {
					jpaQuery.append(START_DE_QUERY_FOR_PRODUCER_ORDER_BY);
				} else {
					jpaQuery.append(START_DE_QUERY);
				}
				jpaQuery.append(" where ");
			} else {
				jpaQuery.append(" AND ");
			}

			jpaQuery.append(field + " like '%");
			jpaQuery.append(likewords[0] + "%'");

			for (int i = 1; i < likewords.length; i++) {
				jpaQuery.append(" AND " + field + " like '%");
				jpaQuery.append(likewords[i] + "%'");

			}
		}

		if (jpaQuery != null && !jpaQuery.equals("")) {
			jpaQuery.append(DE_NOT_DELETED_DOC);
			logger.info(jpaQuery.toString());
			return jpaQuery.toString();
		}

		return null;

	}

	public static String getDEAllCountQuery(String matchSearchWord,
			String likeSearchWord, String field) {

		StringBuilder jpaQuery = null;
		if (matchSearchWord != null && !matchSearchWord.isEmpty()) {

			jpaQuery = new StringBuilder();

			String[] matchwords = matchSearchWord.split("\\s+");
			jpaQuery.append("select distinct documentEntityId from tblDocumentEnts  where match("
					+ field + ") against ('");
			jpaQuery.append(matchwords[0] + "')");

			for (int i = 1; i < matchwords.length; i++) {
				jpaQuery.append(" AND match(" + field + ") against ('");
				jpaQuery.append(matchwords[i] + "')");

			}
		}

		if (likeSearchWord != null && !likeSearchWord.isEmpty()) {

			String[] likewords = likeSearchWord.split("\\s+");

			if (jpaQuery == null) {
				jpaQuery = new StringBuilder();
				jpaQuery.append("select distinct documentEntityId from tblDocumentEnts  where ");
			} else {
				jpaQuery.append(" AND ");
			}

			jpaQuery.append(field + " like '%");
			jpaQuery.append(likewords[0] + "%'");

			for (int i = 1; i < likewords.length; i++) {
				jpaQuery.append(" AND " + field + " like '%");
				jpaQuery.append(likewords[i] + "%'");

			}
		}

		if (jpaQuery != null && !jpaQuery.equals("")) {
			jpaQuery.append(DE_NOT_DELETED_DOC);
			logger.info("getDEAllCountQuery: " + jpaQuery.toString());
			return jpaQuery.toString();
		}

		return null;

	}

	private static String getDEQuery(String matchSearchWord,
			String likeSearchWord, boolean isOrderByProducer) {

		StringBuilder jpaQuery = null;

		if (matchSearchWord != null && !matchSearchWord.isEmpty()) {

			jpaQuery = new StringBuilder();

			String[] matchwords = matchSearchWord.split("\\s+");
			if (isOrderByProducer) {
				jpaQuery.append(START_DE_QUERY_FOR_PRODUCER_ORDER_BY);
			} else {
				jpaQuery.append(START_DE_QUERY);
			}
			jpaQuery.append(" where match(" + "d.generalNotesSynopsis,d.transSearch"
					+ ") against ('");
			jpaQuery.append(matchwords[0] + "')");

			for (int i = 1; i < matchwords.length; i++) {
				jpaQuery.append(" AND match("
						+ "d.generalNotesSynopsis,d.transSearch"
						+ ") against ('");
				jpaQuery.append(matchwords[i] + "')");

			}
		}

		if (likeSearchWord != null && !likeSearchWord.isEmpty()) {

			String[] likewords = likeSearchWord.split("\\s+");

			if (jpaQuery == null) {
				jpaQuery = new StringBuilder();
				if (isOrderByProducer) {
					jpaQuery.append(START_DE_QUERY_FOR_PRODUCER_ORDER_BY);
				} else {
					jpaQuery.append(START_DE_QUERY);
				}
				
				jpaQuery.append(" where ");

			} else {
				jpaQuery.append(" AND ");
			}

			jpaQuery.append("( d.generalNotesSynopsis" + " like '%");
			jpaQuery.append(likewords[0] + "%'");
			jpaQuery.append(" OR d.transSearch" + " like '%");
			jpaQuery.append(likewords[0] + "%' )");

			for (int i = 1; i < likewords.length; i++) {
				jpaQuery.append(" AND ");
				jpaQuery.append("( ");
				jpaQuery.append("d.generalNotesSynopsis" + " like '%");
				jpaQuery.append(likewords[i] + "%'");
				jpaQuery.append(" OR d.transSearch" + " like '%");
				jpaQuery.append(likewords[i] + "%'");
				jpaQuery.append(" )");

			}
		}

		if (jpaQuery != null && !jpaQuery.equals("")) {
			// jpaQuery.append(DE_NOT_DELETED_DOC);
			logger.info("Query DE search ALL: " + jpaQuery.toString());
			return jpaQuery.toString();
		}

		return null;

	}

	public static Predicate getWhereClause(PeopleWordSearchJson searchReq,
			CriteriaBuilder cb, Root<AltName> altNameRoot) {

		String searchString = '%' + searchReq.getSearchString().trim() + '%';
		List<String> searchTypes = searchReq.getSearchType();
		boolean searchAllTypes = searchTypes
				.contains(PeopleWordSearchType.ALL_NAME_TYPES.toString());
		Predicate whereClause = cb.equal(cb.literal(true), cb.literal(false));
		for (String searchType : searchTypes) {
			PeopleWordSearchType type = PeopleWordSearchType
					.valueOf(searchType);
			if (searchAllTypes || type.equals(PeopleWordSearchType.APPELLATIVE)) {
				whereClause = cb.or(whereClause, cb.and(cb.like(
						altNameRoot.<String> get("altName"), searchString), cb
						.equal(altNameRoot.get("nameType"),
								PeopleWordSearchType.APPELLATIVE.toString())));
			}
			if (searchAllTypes || type.equals(PeopleWordSearchType.FAMILY)) {
				whereClause = cb.or(whereClause, cb.and(cb.like(
						altNameRoot.<String> get("altName"), searchString), cb
						.equal(altNameRoot.get("nameType"),
								PeopleWordSearchType.FAMILY.toString())));
			}
			if (searchAllTypes || type.equals(PeopleWordSearchType.GIVEN)) {
				whereClause = cb.or(whereClause, cb.and(cb.like(
						altNameRoot.<String> get("altName"), searchString), cb
						.equal(altNameRoot.get("nameType"),
								PeopleWordSearchType.GIVEN.toString())));
			}
			if (searchAllTypes || type.equals(PeopleWordSearchType.MAIDEN)) {
				whereClause = cb.or(whereClause, cb.and(cb.like(
						altNameRoot.<String> get("altName"), searchString), cb
						.equal(altNameRoot.get("nameType"),
								PeopleWordSearchType.MAIDEN.toString())));
			}
			if (searchAllTypes || type.equals(PeopleWordSearchType.PATRONYMIC)) {
				whereClause = cb.or(whereClause, cb.and(cb.like(
						altNameRoot.<String> get("altName"), searchString), cb
						.equal(altNameRoot.get("nameType"),
								PeopleWordSearchType.PATRONYMIC.toString())));
			}
		}
		return whereClause;
	}

}
