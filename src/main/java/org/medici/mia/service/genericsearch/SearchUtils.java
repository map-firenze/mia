package org.medici.mia.service.genericsearch;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.medici.mia.common.json.search.AEWordSearchJson;
import org.medici.mia.common.json.search.AEWordSearchType;
import org.medici.mia.common.json.search.AllCountWordSearchJson;
import org.medici.mia.common.json.search.AllCountWordSearchType;
import org.medici.mia.common.json.search.DEWordSearchJson;
import org.medici.mia.common.json.search.DEWordSearchType;
import org.medici.mia.common.json.search.PeopleWordSearchJson;
import org.medici.mia.common.json.search.PeopleWordSearchType;
import org.medici.mia.domain.AltName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public class SearchUtils {

	/**
	 * Size of a byte buffer to read/write file
	 */
	private static final String AE_WORD_SEARCH_QSTR_TYPE_1 = "select * from tblUploads e where match(aeName) against (:searchWords) ";
	private static final String AE_WORD_SEARCH_QSTR_TYPE_2 = "select * from tblUploads e where match(aeDescription) against (:searchWords) ";
	private static final String AE_WORD_SEARCH_QSTR_TYPE_3 = "select * from tblUploads e where match(aeName,aeDescription) against (:searchWords) ";

	private static final String AE_WORD_SEARCH_QSTR_ONLY_MINE = " and owner = :owner";
	private static final String AE_WORD_SEARCH_QSTR_ONLY_THEIRS = " and owner <> :owner";
//	private static final String AE_WORD_SEARCH_QSTR_ALL = " and owner = :owner";

	// Document search
	private static final String DE_WORD_SEARCH_QSTR_TYPE_2 = "select * from tblDocumentEnts d, tblDocTranscriptions t where d.documentEntityId = t.documentEntityId and match(t.transcription) against (:searchWords) ";
	private static final String DE_WORD_SEARCH_QSTR_TYPE_3 = "select * from tblDocumentEnts d where match(d.generalNotesSynopsis) against (:searchWords) ";
	private static final String DE_WORD_SEARCH_QSTR_TYPE_1 = "select * from tblDocumentEnts d, tblDocTranscriptions t where d.documentEntityId = t.documentEntityId and match(t.transcription) against (:searchWords) OR match(d.generalNotesSynopsis) against (:searchWords) ";
	private static final String DE_WORD_SEARCH_QSTR_ONLY_MINE = " and d.createdBy = :owner";
	private static final String DE_WORD_SEARCH_QSTR_ONLY_THEIRS = " and d.createdBy <> :owner";
//	private static final String DE_WORD_SEARCH_QSTR_ALL = " and createdBy = :owner";
	
	private static final String DE_NOT_DELETED_DOC = " and  d.flgLogicalDelete != 1";
	private static final String AE_NOT_DELETED_DOC = " and  e.logicalDelete != 1";
	
	private static final String ALLCOUNT_UPLOAD_WORD_SEARCH = "select count( * ) from tblUploads e where match(aeName,aeDescription) against (:searchWords) and  e.logicalDelete = 0 ";
	
	private static final String ALLCOUNT_UPLOAD_WORD = "select count( * ) from tblUploads e where  e.logicalDelete = 0";

	// All Count search
	public static String getQueryString(AEWordSearchJson searchReq) {

		String queryString = null;
		if (searchReq.getSearchType().equals(
				AEWordSearchType.AE_NAME.toString())) {
			queryString = AE_WORD_SEARCH_QSTR_TYPE_1;
		} else if (searchReq.getSearchType().equals(
				AEWordSearchType.AE_DESCRIPTION.toString())) {
			queryString = AE_WORD_SEARCH_QSTR_TYPE_2;
		} else if (searchReq.getSearchType().equals(
				AEWordSearchType.AE_NAME_AND_DESCRIPTION.toString())) {
			queryString = AE_WORD_SEARCH_QSTR_TYPE_3;
		}
		if (searchReq.isOnlyMyArchivalEnties()) {
			queryString = queryString + AE_WORD_SEARCH_QSTR_ONLY_MINE;
		} else if (searchReq.isEveryoneElsesArchivalEntities()) {
			queryString = queryString + AE_WORD_SEARCH_QSTR_ONLY_THEIRS;
		} 
//		  else {
//			queryString = queryString + AE_WORD_SEARCH_QSTR_ALL;
//		}
		
		queryString = queryString + AE_NOT_DELETED_DOC;

		return queryString;
	}

	public static String getQueryString(AllCountWordSearchJson searchReq) {

		String queryString = null;

		if (AllCountWordSearchType.ARCHIVAL_ENTITIES.toString()
				.equalsIgnoreCase(searchReq.getSearchType())) {
			queryString = ALLCOUNT_UPLOAD_WORD_SEARCH;

		} 
		
		return queryString;
	}
	
	public static String getQueryStringAll() {
		return ALLCOUNT_UPLOAD_WORD;
	}
	
	
// Note: searchType - (1: Transcription and Synopsis, 2: Transcription, 3: Synopsis) -//search module page 24
	public static String getQueryString(DEWordSearchJson searchReq) {

		String queryString = null;
		if (searchReq.getDeWordSearchType().equals(
				DEWordSearchType.DE_TRANSCRIPTION_AND_SYNOPSIS)) {
			queryString = DE_WORD_SEARCH_QSTR_TYPE_1;
		} else if (searchReq.getDeWordSearchType().equals(
				DEWordSearchType.DE_TRANSCRIPTION)) {
			queryString = DE_WORD_SEARCH_QSTR_TYPE_2;
		} else if (searchReq.getDeWordSearchType().equals(
				DEWordSearchType.DE_SYNOPSIS)) {
			queryString = DE_WORD_SEARCH_QSTR_TYPE_3;
		}
		if (searchReq.isOnlyMyEntities()) {
			queryString = queryString + DE_WORD_SEARCH_QSTR_ONLY_MINE;
		} else if (searchReq.isEveryoneElsesEntities()) {
			queryString = queryString + DE_WORD_SEARCH_QSTR_ONLY_THEIRS;
		} 
//		else {
//			queryString = queryString + DE_WORD_SEARCH_QSTR_ALL;
//		}
		
		queryString = queryString + DE_NOT_DELETED_DOC;

		return queryString;
	}

	public static Predicate getWhereClause(PeopleWordSearchJson searchReq,
			CriteriaBuilder cb, Root<AltName> altNameRoot) {

		String searchString = '%' + searchReq.getSearchString().trim() + '%';
		List<String> searchTypes = searchReq.getSearchType();
		boolean searchAllTypes = searchTypes
				.contains(PeopleWordSearchType.ALL_NAME_TYPES.toString());
		Predicate whereClause = cb.equal(cb.literal(true), cb.literal(false));
		for (String searchType : searchTypes) {
			PeopleWordSearchType type = PeopleWordSearchType
					.valueOf(searchType);
			if (searchAllTypes || type.equals(PeopleWordSearchType.APPELLATIVE)) {
				whereClause = cb.or(whereClause, cb.and(cb.like(
						altNameRoot.<String> get("altName"), searchString), cb
						.equal(altNameRoot.get("nameType"),
								PeopleWordSearchType.APPELLATIVE.toString())));
			}
			if (searchAllTypes || type.equals(PeopleWordSearchType.FAMILY)) {
				whereClause = cb.or(whereClause, cb.and(cb.like(
						altNameRoot.<String> get("altName"), searchString), cb
						.equal(altNameRoot.get("nameType"),
								PeopleWordSearchType.FAMILY.toString())));
			}
			if (searchAllTypes || type.equals(PeopleWordSearchType.GIVEN)) {
				whereClause = cb.or(whereClause, cb.and(cb.like(
						altNameRoot.<String> get("altName"), searchString), cb
						.equal(altNameRoot.get("nameType"),
								PeopleWordSearchType.GIVEN.toString())));
			}
			if (searchAllTypes || type.equals(PeopleWordSearchType.MAIDEN)) {
				whereClause = cb.or(whereClause, cb.and(cb.like(
						altNameRoot.<String> get("altName"), searchString), cb
						.equal(altNameRoot.get("nameType"),
								PeopleWordSearchType.MAIDEN.toString())));
			}
			if (searchAllTypes || type.equals(PeopleWordSearchType.PATRONYMIC)) {
				whereClause = cb.or(whereClause, cb.and(cb.like(
						altNameRoot.<String> get("altName"), searchString), cb
						.equal(altNameRoot.get("nameType"),
								PeopleWordSearchType.PATRONYMIC.toString())));
			}
		}
		return whereClause;
	}

//	private String getSynopsesQery(String searchWord) {
//		if(searchWord==null || searchWord.isEmpty()){
//			return null;
//		}
//		// String[] words = RegExUtils.splitPunctuationAndSpaceChars(word);
//		String[] words = searchWord.split(";");	
//		StringBuilder jpaQuery = new StringBuilder(
//				"select * from tblDocumentEnts d where match(generalNotesSynopsis) against ('");
//		jpaQuery.append(words[1] + "')");
//		int j = 0;
//		for (int i = 2; i < words.length - 1; i++) {
//			jpaQuery.append(" OR match(generalNotesSynopsis) against ('");
//			jpaQuery.append(words[i] + "')");
//			j = i;
//		}
//	
//		jpaQuery.append(" OR generalNotesSynopsis like '%");
//		jpaQuery.append(words[j + 1] + "%'");
//		jpaQuery.append(" AND flgLogicalDelete = false");
//		
//		return jpaQuery.toString();
//
//	}
}
