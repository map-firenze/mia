/*
 * PeopleSearchServiceImple.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.service.genericsearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataCountJson;
import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.complexsearch.PlaceComplexSearchJson;
import org.medici.mia.common.json.search.PlaceSearchJson;
import org.medici.mia.common.json.search.PlaceTypeJson;
import org.medici.mia.common.json.search.PlaceWordSearchJson;
import org.medici.mia.controller.genericsearch.AESearchPaginationParam;
import org.medici.mia.dao.place.PlaceDAO;
import org.medici.mia.dao.placetype.PlaceTypeDAO;
import org.medici.mia.dao.user.UserDAO;
import org.medici.mia.domain.Place;
import org.medici.mia.domain.PlaceType;
import org.medici.mia.domain.User;
import org.medici.mia.exception.ApplicationThrowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Service
public class PlaceSearchServiceImple implements PlaceSearchService {

	@Autowired
	private PlaceDAO placeDAO;

	@Autowired
	private PlaceTypeDAO placeTypeDAO;
	
	@Autowired
	private UserDAO userDAO;

	@Override
	public GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>> findPlaceByNameAndType(
			PlaceWordSearchJson placeSearchWord) throws ApplicationThrowable {
		GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>> toret = new GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>>();

		List<Place> places = getPlaceDAO().findPlacesByNameAndType(
				placeSearchWord);
		if (places == null || places.isEmpty()) {
			toret.setMessage("No place found in DB");
			toret.setStatus(StatusType.w.toString());

		} else {
			List<PlaceSearchJson> placesJson = new ArrayList<PlaceSearchJson>();

			for (Place place : places) {
				PlaceSearchJson placeJson = new PlaceSearchJson();
				placeJson.toJson(place);

				placesJson.add(placeJson);
			}
			toret.setStatus(StatusType.ok.toString());
			toret.setMessage("Got " + places.size()
					+ " places with searchReq: "
					+ placeSearchWord.getSearchString());
			HashMap<String, List<PlaceSearchJson>> data = new HashMap<String, List<PlaceSearchJson>>();
			data.put("data", placesJson);
			toret.setData(data);
		}

		return toret;
	}

	@Override
	public GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>> 
	findPlaceByComplexSearchNameAndType(
			PlaceComplexSearchJson placeSearchWord,
			AESearchPaginationParam pagParam) throws ApplicationThrowable {
		
		GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>> toret = 
		new GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>>();

		List<Place> places = getPlaceDAO()
				.findPlacesByComplexSearchNameAndType(placeSearchWord, pagParam);
		if (places == null || places.isEmpty()) {
			toret.setMessage("No place found in DB");
			toret.setStatus(StatusType.w.toString());

		} else {
			List<PlaceSearchJson> placesJson = new ArrayList<PlaceSearchJson>();

			for (Place place : places) {
				PlaceSearchJson placeJson = new PlaceSearchJson();
				placeJson.toJson(place);

				placesJson.add(placeJson);
			}
			
			toret.setStatus(StatusType.ok.toString());
			toret.setMessage("Got " + places.size() + " places  ");
			HashMap<String, List<PlaceSearchJson>> data = 
					new HashMap<String, List<PlaceSearchJson>>();
			
			data.put("data", placesJson);
			toret.setData(data);
		}

		return toret;
	}
	
	@Override
	public GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>> 
	findByOwner(
			PlaceComplexSearchJson searchRequest,
			AESearchPaginationParam paginationParameters) 
	throws ApplicationThrowable {
		
		GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>> 
		response
		= new GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>>();

		User user = userDAO.findUser(searchRequest.getOwner());
		if (user == null) {
			response.setMessage("No user found in DB");
			response.setStatus(StatusType.w.toString());
		}
		
		List<Place> places = getPlaceDAO().getPlacesCreatedByUser(
				user, paginationParameters);
		
		if (places == null || places.isEmpty()) {
			response.setMessage("No place found in DB");
			response.setStatus(StatusType.w.toString());

		} else {
			response = prepareResponse(places, user);
		}

		return response;
	}

	
	
	@Override
	public GenericResponseDataJson<HashMap<String, List<PlaceTypeJson>>> findPlaceTypes()
			throws ApplicationThrowable {
		GenericResponseDataJson<HashMap<String, List<PlaceTypeJson>>> toret = new GenericResponseDataJson<HashMap<String, List<PlaceTypeJson>>>();

		List<PlaceType> placeTypes = getPlaceTypeDAO().findPlaceTypes();

		if (placeTypes == null || placeTypes.isEmpty()) {
			toret.setMessage("No place type found in DB");
			toret.setStatus(StatusType.w.toString());

		} else {
			List<PlaceTypeJson> placeTypesJson = new ArrayList<PlaceTypeJson>();

			for (PlaceType placeType : placeTypes) {
				PlaceTypeJson placeTypeJson = new PlaceTypeJson();
				placeTypeJson.toJson(placeType);

				placeTypesJson.add(placeTypeJson);
			}
			toret.setStatus(StatusType.ok.toString());
			toret.setMessage("Got " + placeTypesJson.size() + " place types. ");
			HashMap<String, List<PlaceTypeJson>> data = new HashMap<String, List<PlaceTypeJson>>();
			data.put("data", placeTypesJson);
			toret.setData(data);
		}

		return toret;
	}
	
	private GenericResponseDataCountJson<HashMap<String, List<PlaceSearchJson>>>
	prepareResponse(List<Place> places, User user) {
		
		GenericResponseDataCountJson<HashMap<String, List<PlaceSearchJson>>> 
		response = new 
		GenericResponseDataCountJson<HashMap<String, List<PlaceSearchJson>>>();
		
		List<PlaceSearchJson> placesJson = new ArrayList<PlaceSearchJson>();

		for (Place place : places) {
			PlaceSearchJson placeJson = new PlaceSearchJson();
			placeJson.toJson(place);

			placesJson.add(placeJson);
		}
		
		response.setStatus(StatusType.ok.toString());
		response.setMessage("Got " + places.size() + " places  ");
		HashMap<String, List<PlaceSearchJson>> data = 
				new HashMap<String, List<PlaceSearchJson>>();
		
		data.put("data", placesJson);
		response.setData(data);
		response.setCount(getPlacesCount(user));
		
		return response;
	}
	
	private Integer getPlacesCount(User user) {
		List<Place> places = getPlaceDAO().getPlacesCreatedByUser(user);
		
		if (places == null) {
			return 0;
		}
		
		return places.size();
	}
	
	public PlaceDAO getPlaceDAO() {
		return placeDAO;
	}

	public void setPlaceDAO(PlaceDAO placeDAO) {
		this.placeDAO = placeDAO;
	}

	public PlaceTypeDAO getPlaceTypeDAO() {
		return placeTypeDAO;
	}

	public void setPlaceTypeDAO(PlaceTypeDAO placeTypeDAO) {
		this.placeTypeDAO = placeTypeDAO;
	}

}
