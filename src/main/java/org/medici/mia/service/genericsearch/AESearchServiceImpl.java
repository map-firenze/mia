package org.medici.mia.service.genericsearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.complexsearch.AEComplexSearchJson;
import org.medici.mia.common.json.complexsearch.AEComplexSearchResponseJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.search.AEArchivalSearchJson;
import org.medici.mia.common.json.search.AEWordSearchJson;
import org.medici.mia.common.util.DocumentPrivacyUtils;
import org.medici.mia.common.util.DocumentPrivacyUtils.PrivacyType;
import org.medici.mia.controller.archive.CountAESandDESJson;
import org.medici.mia.controller.archive.MyArchiveBean;
import org.medici.mia.controller.genericsearch.AESearchPaginationParam;
import org.medici.mia.dao.fileSharing.FileSharingDAO;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.dao.uploadfile.UploadFileDAO;
import org.medici.mia.dao.uploadinfo.UploadInfoDAO;
import org.medici.mia.dao.volume.VolumeDAO;
import org.medici.mia.domain.IGenericEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.UploadInfoEntity;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AESearchServiceImpl implements AESearchService {

	@Autowired
	private UploadInfoDAO uploadInfoDAO;

	@Autowired
	private GenericDocumentDao documendDAO;

	@Autowired
	private VolumeDAO volumeDAO;

	@Autowired
	private UploadFileDAO uploadFileDAO;

	@Autowired
	private UserService userService;

	@Autowired
	private MiaDocumentService miaDocumentService;

	@Autowired
	private FileSharingDAO fileSharingDao;

	@Override
	public GenericResponseDataJson<AEComplexSearchResponseJson> findAEntitiesByComplexSearch(
			AEComplexSearchJson searchRequest, 
			AESearchPaginationParam paginationParameters)
			throws ApplicationThrowable {
		
		GenericResponseDataJson<AEComplexSearchResponseJson> response = 
				new GenericResponseDataJson<AEComplexSearchResponseJson>();

		AEComplexSearchResponseJson aeResults = 
				getResults(searchRequest, paginationParameters);
		
		if (aeResults == null) {
			response.setMessage("No Archive Entity found in DB .");
			response.setStatus(StatusType.ok.toString());
			return response;
		}
		
		Integer count = getUploadInfoDAO().getUploadsCountByComplexSerach(
				searchRequest);
		aeResults.setCount(count);

		response.setData(aeResults);
		response.setStatus(StatusType.ok.toString());
		response.setMessage(count + " archival entities found");

		return response;
	}
	
	@Override
	public GenericResponseDataJson<AEComplexSearchResponseJson> findByOwner(
			AEComplexSearchJson searchRequest, 
			AESearchPaginationParam paginationParameters)
			throws ApplicationThrowable {
		
		GenericResponseDataJson<AEComplexSearchResponseJson> response = 
				new GenericResponseDataJson<AEComplexSearchResponseJson>();

		AEComplexSearchResponseJson aeResults = 
				getResults(searchRequest, paginationParameters);
		
		if (aeResults == null) {
			response.setMessage("No Archive Entity found in DB .");
			response.setStatus(StatusType.ok.toString());
			return response;
		}
		
		Integer count = getUploadsCount(searchRequest.getOwner());
		aeResults.setCount(count);
		
		response.setData(aeResults);
		response.setStatus(StatusType.ok.toString());
		response.setMessage(count + " archival entities found");

		return response;
	}

	private MyArchiveBean pruneIfPrivate(MyArchiveBean original) {
		PrivacyType privacy = DocumentPrivacyUtils.getPrivacyType(original
				.getOwner(), original.getFilePrivacy(), fileSharingDao
				.findSharedUsersByUpdateId(original.getUploadId()));
		if (privacy == PrivacyType.PUBLIC) {
			return original;
		}
		if (privacy == PrivacyType.PRIVATE_SHARED) {
			// sets 2 as the file privacy to model that the file is private but
			// i can access it
			original.setFilePrivacy(2);
			return original;
		}

		MyArchiveBean result = new MyArchiveBean();
		result.setRepository(original.getRepository());
		result.setCollection(original.getCollection());
		result.setUploadId(original.getUploadId());
		result.setFilePrivacy(original.getFilePrivacy());
		result.setOwner(original.getOwner());
		return result;
	}

	@Override
	public GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> findAEntitiesBySearchWords(
			AEWordSearchJson searchReq) throws ApplicationThrowable {
		GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> resp = new GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>>();

		//
		List<UploadInfoEntity> uploadInfo = getUploadInfoDAO()
				.getUploadsBySerachWords(searchReq);

		if (uploadInfo == null) {
			resp.setMessage("No Archive Entity found in DB .");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}
		List<MyArchiveBean> archives = new ArrayList<MyArchiveBean>();
		for (UploadInfoEntity entity : uploadInfo) {
			if (entity.getOptionalImage() == null
					|| entity.getOptionalImage().equals(null)) {
				MyArchiveBean archiveBean = new MyArchiveBean();
				archiveBean.toJson((UploadInfoEntity) entity);
				archives.add(pruneIfPrivate(archiveBean));

			}
		}
		/*
		 * for (IGenericEntity entity : uploadInfo) { MyArchiveBean archiveBean
		 * = new MyArchiveBean(); archiveBean.toJson((UploadInfoEntity) entity);
		 * archives.add(pruneIfPrivate(archiveBean));
		 * 
		 * }
		 */
		HashMap<String, List<MyArchiveBean>> aehash = new HashMap<String, List<MyArchiveBean>>();
		aehash.put("aentities", archives);
		resp.setData(aehash);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage(archives.size() + " archival entities found");

		return resp;

	}

	@Override
	public GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> findAEntitiesByArchivalInfo(
			AEArchivalSearchJson searchReq) throws ApplicationThrowable {

		GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> resp = new GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>>();
		List<UploadInfoEntity> uploadInfo = getUploadInfoDAO()
				.getUploadsByArchivalInfo(searchReq);

		if (uploadInfo == null) {
			resp.setMessage("No Archive Entity found in DB .");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}
		List<MyArchiveBean> archives = new ArrayList<MyArchiveBean>();
		for (IGenericEntity entity : uploadInfo) {
			MyArchiveBean archiveBean = new MyArchiveBean();
			UploadInfoEntity uploadInfoEnt = (UploadInfoEntity) entity;
			if ((uploadInfoEnt.getLogicalDelete() == null || uploadInfoEnt
					.getLogicalDelete().equals(0))
					&& uploadInfoEnt.getOptionalImage() == null) {
				archiveBean.toJson(uploadInfoEnt);
				archives.add(pruneIfPrivate(archiveBean));
			}

		}
		HashMap<String, List<MyArchiveBean>> aehash = new HashMap<String, List<MyArchiveBean>>();
		aehash.put("aentities", archives);
		resp.setData(aehash);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage(archives.size() + " archival entities found");

		return resp;
	}

	@Override
	public GenericResponseDataJson<HashMap<String, Integer>> countAEntitiesByArchivalInfo(
			AEArchivalSearchJson searchReq) throws ApplicationThrowable {

		GenericResponseDataJson<HashMap<String, Integer>> resp = new GenericResponseDataJson<HashMap<String, Integer>>();
		Integer records = getUploadInfoDAO().countUploadsByArchivalInfo(
				searchReq);

		if (records == null) {
			resp.setMessage("Errore during record count");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}

		HashMap<String, Integer> aehash = new HashMap<String, Integer>();
		aehash.put("recordsCount", records);
		resp.setData(aehash);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage(records + " archival entities found");

		return resp;
	}

	// Count Archival and Document Entities related to Volumes
	@Override
	public GenericResponseDataJson<CountAESandDESJson> countVolumeAESandDES(
			Integer volumeId) throws ApplicationThrowable {

		GenericResponseDataJson<CountAESandDESJson> resp = new GenericResponseDataJson<CountAESandDESJson>();
		CountAESandDESJson count = getUploadInfoDAO().countVolumeAESandDES(
				volumeId);

		if (count == null) {
			resp.setMessage("Errore during record count");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}
		resp.setData(count);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage("Got " + count.getTotalRecords()
				+ " records in total pertaining to this Volume");

		return resp;
	}

	// Count Archival and Document Entities related to Insert
	@Override
	public GenericResponseDataJson<CountAESandDESJson> countInsertAESandDES(
			Integer insertId) throws ApplicationThrowable {

		GenericResponseDataJson<CountAESandDESJson> resp = new GenericResponseDataJson<CountAESandDESJson>();
		CountAESandDESJson count = getUploadInfoDAO().countInsertAESandDES(
				insertId);

		if (count == null) {
			resp.setMessage("Errore during record count");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}

		resp.setData(count);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage("Got " + count.getTotalRecords()
				+ " records in total pertaining to this Insert");

		return resp;
	}

	// Find Archivial entities related to volume
	@Override
	public GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> findArchivalEntitiesRelatedToVolume(
			Integer volumeId) throws ApplicationThrowable {

		GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> resp = new GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>>();
		//
		List<UploadInfoEntity> uploadInfo = getUploadInfoDAO()
				.getArchivalRelatedToVolume(volumeId);

		if (uploadInfo == null) {
			resp.setMessage("No Archive Entity found in DB .");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}
		List<MyArchiveBean> archives = new ArrayList<MyArchiveBean>();
		for (IGenericEntity entity : uploadInfo) {
			MyArchiveBean archiveBean = new MyArchiveBean();
			archiveBean.toJson((UploadInfoEntity) entity);
			archives.add(pruneIfPrivate(archiveBean));

		}
		HashMap<String, List<MyArchiveBean>> aehash = new HashMap<String, List<MyArchiveBean>>();
		aehash.put("aentities", archives);
		resp.setData(aehash);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage("Got " + archives.size() + " records");

		return resp;
	}

	// find document entities related to volume
	@Override
	public GenericResponseDataJson<HashMap<String, List<DocumentJson>>> findDocumentEntitiesRelatedToVolume(
			Integer volumeId) throws ApplicationThrowable {

		GenericResponseDataJson<HashMap<String, List<DocumentJson>>> resp = new GenericResponseDataJson<HashMap<String, List<DocumentJson>>>();
		//
		List<MiaDocumentEntity> docEntities = getDocumendDAO()
				.findDocumentRelatedToVolume(volumeId);

		if (docEntities == null || docEntities.isEmpty()) {
			resp.setMessage("No Document Entity found in DB .");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}
		List<DocumentJson> douments = getMiaDocumentService()
				.createDocumentsJsonFromDocEntity(docEntities);
		HashMap<String, List<DocumentJson>> dehash = new HashMap<String, List<DocumentJson>>();
		dehash.put("documentEntities", douments);
		resp.setData(dehash);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage("Got " + douments.size() + " Documents in total");
		return resp;
	}

	// find archivial entities related to insert
	@Override
	public GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> findArchivalEntitiesRelatedToInsert(
			Integer insertId) throws ApplicationThrowable {

		GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> resp = new GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>>();
		//
		List<UploadInfoEntity> uploadInfo = getUploadInfoDAO()
				.getArchivialRelatedToInsert(insertId);

		if (uploadInfo == null) {
			resp.setMessage("No Archive Entity found in DB .");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}
		List<MyArchiveBean> archives = new ArrayList<MyArchiveBean>();
		for (IGenericEntity entity : uploadInfo) {
			MyArchiveBean archiveBean = new MyArchiveBean();
			archiveBean.toJson((UploadInfoEntity) entity);
			archives.add(pruneIfPrivate(archiveBean));

		}
		HashMap<String, List<MyArchiveBean>> aehash = new HashMap<String, List<MyArchiveBean>>();
		aehash.put("aentities", archives);
		resp.setData(aehash);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage("Got " + archives.size() + " records");

		return resp;
	}

	// find document entities related to insert
	@Override
	public GenericResponseDataJson<HashMap<String, List<DocumentJson>>> findDocumentEntitiesRelatedToInsert(
			Integer insertId) throws ApplicationThrowable {

		GenericResponseDataJson<HashMap<String, List<DocumentJson>>> resp = new GenericResponseDataJson<HashMap<String, List<DocumentJson>>>();

		List<MiaDocumentEntity> docEntities = getDocumendDAO()
				.findDocumentRelatedToInsert(insertId);

		if (docEntities == null || docEntities.isEmpty()) {
			resp.setMessage("No Document Entity found in DB .");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}
		List<DocumentJson> douments = getMiaDocumentService()
				.createDocumentsJsonFromDocEntity(docEntities);
		HashMap<String, List<DocumentJson>> dehash = new HashMap<String, List<DocumentJson>>();
		dehash.put("documentEntities", douments);
		resp.setData(dehash);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage("Got " + douments.size() + " Documents in total");
		return resp;
	}
	
	private AEComplexSearchResponseJson getResults(
			AEComplexSearchJson searchRequest, 
			AESearchPaginationParam paginationParameters) {
		
		List<UploadInfoEntity> uploadInfo = getUploadInfoDAO()
				.getUploadsByComplexSerach(searchRequest, paginationParameters);

		if (uploadInfo == null) {
			return null;
		}
		
		List<MyArchiveBean> archives = new ArrayList<MyArchiveBean>();

		for (UploadInfoEntity entity : uploadInfo) {
			MyArchiveBean archiveBean = new MyArchiveBean();
			archiveBean.toJson(entity);
			archives.add(pruneIfPrivate(archiveBean));
		}

		AEComplexSearchResponseJson aeResult = new AEComplexSearchResponseJson();
		aeResult.setAentities(archives);
		
		return aeResult;
	}
	
	private Integer getUploadsCount(String account) {
		List<UploadInfoEntity> results = 
				getUploadInfoDAO().getNotDeletedUploadsByOwner(account);
		
		if (results == null) {
			return 0;
		}
		else {
			return results.size();
		}
	}

	public UploadInfoDAO getUploadInfoDAO() {
		return uploadInfoDAO;
	}

	public GenericDocumentDao getDocumendDAO() {
		return documendDAO;
	}

	public void setUploadInfoDAO(UploadInfoDAO uploadInfoDAO) {
		this.uploadInfoDAO = uploadInfoDAO;
	}

	public UploadFileDAO getUploadFileDAO() {
		return uploadFileDAO;
	}

	public void setUploadFileDAO(UploadFileDAO uploadFileDAO) {
		this.uploadFileDAO = uploadFileDAO;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public VolumeDAO getVolumeDAO() {
		return volumeDAO;
	}

	public void setVolumeDAO(VolumeDAO volumeDAO) {
		this.volumeDAO = volumeDAO;
	}

	public GenericDocumentDao getDocumentDAO() {
		return documendDAO;
	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

}
