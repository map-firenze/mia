package org.medici.mia.service.genericsearch;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.complexsearch.DEComplexSearchJson;
import org.medici.mia.common.json.complexsearch.DEComplexSearchRespJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.search.AllCountWordSearchJson;
import org.medici.mia.common.json.search.AllCountWordSearchResponseJson;
import org.medici.mia.common.json.search.AllCountWordSearchType;
import org.medici.mia.common.json.search.DEWordSearchJson;
import org.medici.mia.comparator.DocumentJsonArchivalLocationAscComparator;
import org.medici.mia.comparator.DocumentJsonArchivalLocationDescComparator;
import org.medici.mia.controller.genericsearch.DESearchPaginationParam;
import org.medici.mia.dao.altname.AltNameDAO;
import org.medici.mia.dao.miadoc.DocumentTranscriptionDAO;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.dao.people.PeopleDAO;
import org.medici.mia.dao.place.PlaceDAO;
import org.medici.mia.dao.uploadfile.UploadFileDAO;
import org.medici.mia.dao.uploadinfo.UploadInfoDAO;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GenericSearchServiceImpl implements GenericSearchService {

	@Autowired
	private MiaDocumentService miaDocumentService;

	@Autowired
	private GenericDocumentDao documentDAO;

	@Autowired
	private UploadInfoDAO uploadInfoDAO;

	@Autowired
	private UploadFileDAO uploadFileDAO;

	@Autowired
	private UserService userService;

	@Autowired
	private AltNameDAO altNameDAO;

	@Autowired
	private PlaceDAO placeDAO;

	@Autowired
	private PeopleDAO peopleDAO;

	@Autowired
	private DocumentTranscriptionDAO documentTranscriptionDAO;

	@Override
	public GenericResponseDataJson<HashMap<String, List<DocumentJson>>> findDEntitiesBySearchWords(
			DEWordSearchJson searchReq) throws ApplicationThrowable {
		GenericResponseDataJson<HashMap<String, List<DocumentJson>>> resp = new GenericResponseDataJson<HashMap<String, List<DocumentJson>>>();

		List<MiaDocumentEntity> docEntities = getDocumentDAO()
				.getDocumentsBySerachWords(searchReq);
		if (docEntities == null || docEntities.isEmpty()) {
			resp.setMessage("No Document Entity found in DB .");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}
		List<DocumentJson> douments = getMiaDocumentService()
				.createDocumentsJsonFromDocEntity(docEntities);
		HashMap<String, List<DocumentJson>> dehash = new HashMap<String, List<DocumentJson>>();
		dehash.put("documentEntities", douments);
		resp.setData(dehash);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage(douments.size() + " Document entities found");

		return resp;

	}

	@Override
	public GenericResponseDataJson<DEComplexSearchRespJson> findDEntitiesByComplexSearch(
			DEComplexSearchJson searchReq, DESearchPaginationParam pagParam)
			throws ApplicationThrowable {
		GenericResponseDataJson<DEComplexSearchRespJson> resp = new GenericResponseDataJson<DEComplexSearchRespJson>();

		String queryString = ComplexSearchUtils
				.getDEComplexQueryString(searchReq, pagParam.isOrderByProducer());

		String countQueryString = ComplexSearchUtils.getDEComplexCountQueryString(searchReq);

		List<MiaDocumentEntity> docEntities = getDocumentDAO()
				.getDocumentsByComplexSerach(searchReq, queryString, pagParam);
		if (docEntities == null || docEntities.isEmpty()) {
			resp.setMessage("No Document Entity found in DB");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}
		List<DocumentJson> documents = getMiaDocumentService()
				.createDocumentsJsonFromDocEntity(docEntities);
		
		if (pagParam.isOrderByArchivalLocation()) {
			if ("asc".equalsIgnoreCase(pagParam.getAscOrdDesc())) {
				Collections.sort(documents, new DocumentJsonArchivalLocationAscComparator());
			} else if ("desc".equalsIgnoreCase(pagParam.getAscOrdDesc())) {
				Collections.sort(documents, new DocumentJsonArchivalLocationDescComparator());
			}
			documents = documents.subList(pagParam.getFirstResult(),
					documents.size() < pagParam.getFirstResult() + pagParam.getMaxResult() ? documents.size()
							: pagParam.getFirstResult() + pagParam.getMaxResult());
		}
		
		DEComplexSearchRespJson docs = new DEComplexSearchRespJson();
		docs.setDocs(documents);

		if(documents!=null){
			docs.setCount(Integer.valueOf(getDocumentDAO().countDocumentsByComplexSerach(searchReq, countQueryString).intValue()));
		}else{
			docs.setCount(0);
		}
		
		HashMap<String, List<DocumentJson>> dehash = new HashMap<String, List<DocumentJson>>();
		
		resp.setData(docs);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage(documents.size()
				+ " Document entities found");

		return resp;

	}

	public AllCountWordSearchResponseJson findAllCountBySearchWords(
			AllCountWordSearchJson searchReq) throws ApplicationThrowable {
		AllCountWordSearchResponseJson countRes = new AllCountWordSearchResponseJson();

		boolean isAll = false;
		if (AllCountWordSearchType.ALL.toString().equalsIgnoreCase(
				searchReq.getSearchType())) {
			isAll = true;
		}
		if (AllCountWordSearchType.ARCHIVAL_ENTITIES.toString()
				.equalsIgnoreCase(searchReq.getSearchType()) || isAll) {
			searchReq.setSearchType(AllCountWordSearchType.ARCHIVAL_ENTITIES
					.toString());
			countRes.setArchivalEntities(getUploadInfoDAO()
					.getAllCountUploadInfosBySerachWords(searchReq));

		}

		if (AllCountWordSearchType.TRANSCRIPTIONS.toString().equalsIgnoreCase(
				searchReq.getSearchType())
				|| isAll) {

			countRes.setTranscriptions(getDocumentDAO()
					.getDEAllCountTranscriptionAndSynopsis(searchReq,
							"transSearch").size());

		}
		if (AllCountWordSearchType.SYNOPSES.toString().equalsIgnoreCase(
				searchReq.getSearchType())
				|| isAll) {

			// countRes.setSynopses(getDocumentDAO()
			// .getAllCountSynopsesBySerachWords(
			// searchReq.getPartialSearchWords()));
			countRes.setSynopses(getDocumentDAO()
					.getDEAllCountTranscriptionAndSynopsis(searchReq,
							"generalNotesSynopsis").size());
		}

		if (AllCountWordSearchType.DE_TRANSCRIPTION_AND_SYNOPSIS.toString()
				.equalsIgnoreCase(searchReq.getSearchType())) {

			countRes.setTranscriptions(getDocumentDAO()
					.getDEAllCountTranscriptionAndSynopsis(searchReq,
							"transSearch").size());

			countRes.setSynopses(getDocumentDAO()
					.getDEAllCountTranscriptionAndSynopsis(searchReq,
							"generalNotesSynopsis").size());
		}

		if (AllCountWordSearchType.PEOPLE.toString().equalsIgnoreCase(
				searchReq.getSearchType())
				|| isAll) {
			// countRes.setPeople(getPeopleDAO().getAllCountPeopleBySerachWords(
			// searchReq.getPartialSearchWords()));
			countRes.setPeople(getPeopleDAO().getAllCountPeople(searchReq));
		}

		if (AllCountWordSearchType.PLACES.toString().equalsIgnoreCase(
				searchReq.getSearchType())
				|| isAll) {
			// countRes.setPlaces(getPlaceDAO().getAllCountPlacesBySerachWords(
			// searchReq.getPartialSearchWords()));

			countRes.setPlaces(getPlaceDAO().getAllCountPlace(searchReq).size());
		}
		return countRes;

	}

	@Override
	public AllCountWordSearchResponseJson findAllCount()
			throws ApplicationThrowable {
		AllCountWordSearchResponseJson countRes = new AllCountWordSearchResponseJson();

		countRes.setArchivalEntities(getUploadInfoDAO()
				.getAllCountUploadInfos());

		countRes.setTranscriptions(getDocumentTranscriptionDAO()
				.getAllCountTranscription());

		countRes.setSynopses(getDocumentDAO().getAllCountSynopses());

		countRes.setPeople(getPeopleDAO().getAllCountPeople());

		countRes.setPlaces(getPlaceDAO().getAllCountPlaces());

		return countRes;

	}

	public GenericDocumentDao getDocumentDAO() {
		return documentDAO;
	}

	public void setDocumentDAO(GenericDocumentDao documentDAO) {
		this.documentDAO = documentDAO;
	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

	public void setMiaDocumentService(MiaDocumentService miaDocumentService) {
		this.miaDocumentService = miaDocumentService;
	}

	public UploadInfoDAO getUploadInfoDAO() {
		return uploadInfoDAO;
	}

	public void setUploadInfoDAO(UploadInfoDAO uploadInfoDAO) {
		this.uploadInfoDAO = uploadInfoDAO;
	}

	public UploadFileDAO getUploadFileDAO() {
		return uploadFileDAO;
	}

	public void setUploadFileDAO(UploadFileDAO uploadFileDAO) {
		this.uploadFileDAO = uploadFileDAO;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public AltNameDAO getAltNameDAO() {
		return altNameDAO;
	}

	public void setAltNameDAO(AltNameDAO altNameDAO) {
		this.altNameDAO = altNameDAO;
	}

	public PlaceDAO getPlaceDAO() {
		return placeDAO;
	}

	public void setPlaceDAO(PlaceDAO placeDAO) {
		this.placeDAO = placeDAO;
	}

	public PeopleDAO getPeopleDAO() {
		return peopleDAO;
	}

	public void setPeopleDAO(PeopleDAO peopleDAO) {
		this.peopleDAO = peopleDAO;
	}

	public DocumentTranscriptionDAO getDocumentTranscriptionDAO() {
		return documentTranscriptionDAO;
	}

	public void setDocumentTranscriptionDAO(
			DocumentTranscriptionDAO documentTranscriptionDAO) {
		this.documentTranscriptionDAO = documentTranscriptionDAO;
	}

}
