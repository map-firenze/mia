/*
 * PeopleSearchService.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.service.genericsearch;

import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.complexsearch.PlaceComplexSearchJson;
import org.medici.mia.common.json.search.PlaceSearchJson;
import org.medici.mia.common.json.search.PlaceTypeJson;
import org.medici.mia.common.json.search.PlaceWordSearchJson;
import org.medici.mia.controller.genericsearch.AESearchPaginationParam;
import org.medici.mia.exception.ApplicationThrowable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public interface PlaceSearchService {

	public GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>> 
	findPlaceByNameAndType(
			PlaceWordSearchJson placeSearchWord) throws ApplicationThrowable;

	public GenericResponseDataJson<HashMap<String, List<PlaceTypeJson>>> 
	findPlaceTypes()
			throws ApplicationThrowable;

	public GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>> 
	findPlaceByComplexSearchNameAndType(
			PlaceComplexSearchJson placeSearchWord,
			AESearchPaginationParam pagParam) throws ApplicationThrowable;
	
	public GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>> 
	findByOwner(
			PlaceComplexSearchJson placeSearchWord,
			AESearchPaginationParam pagParam) throws ApplicationThrowable;
}
