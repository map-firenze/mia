package org.medici.mia.service.genericsearch;

import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.complexsearch.AEComplexSearchJson;
import org.medici.mia.common.json.complexsearch.AEComplexSearchResponseJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.search.AEArchivalSearchJson;
import org.medici.mia.common.json.search.AEWordSearchJson;
import org.medici.mia.controller.archive.CountAESandDESJson;
import org.medici.mia.controller.archive.MyArchiveBean;
import org.medici.mia.controller.genericsearch.AESearchPaginationParam;
import org.medici.mia.exception.ApplicationThrowable;

public interface AESearchService {

	public GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> 
	findAEntitiesBySearchWords(
			AEWordSearchJson searchReq) throws ApplicationThrowable;
	
	public GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> 
	findAEntitiesByArchivalInfo(
			AEArchivalSearchJson searchReq) throws ApplicationThrowable;

	public GenericResponseDataJson<HashMap<String, Integer>> 
	countAEntitiesByArchivalInfo(
			AEArchivalSearchJson searchReq);
	
	public GenericResponseDataJson<AEComplexSearchResponseJson> 
	findAEntitiesByComplexSearch(
			AEComplexSearchJson searchReq, AESearchPaginationParam pagParam) 
	throws ApplicationThrowable;
	
	public GenericResponseDataJson<AEComplexSearchResponseJson> findByOwner(
			AEComplexSearchJson searchReq, AESearchPaginationParam pagParam)
	throws ApplicationThrowable;
	
	public GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> 
	findArchivalEntitiesRelatedToVolume(
			Integer volumeId) throws ApplicationThrowable;
	
	public GenericResponseDataJson<HashMap<String, List<DocumentJson>>> 
	findDocumentEntitiesRelatedToVolume(
			Integer volumeId) throws ApplicationThrowable;
	
	public GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> 
	findArchivalEntitiesRelatedToInsert(
			Integer insertId) throws ApplicationThrowable;
	
	public GenericResponseDataJson<HashMap<String, List<DocumentJson>>> 
	findDocumentEntitiesRelatedToInsert(
			Integer insertId) throws ApplicationThrowable;

	public GenericResponseDataJson<CountAESandDESJson> countVolumeAESandDES(
			Integer volumeId);

	public GenericResponseDataJson<CountAESandDESJson> countInsertAESandDES(
			Integer insertId);
	
	
}
