package org.medici.mia.service.historylog;

import java.util.HashMap;

import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.document.DocumentTranscriptionJson;
import org.medici.mia.common.json.historylog.LastAccessedRecordJson;
import org.medici.mia.common.json.historylog.MyResearchHistoryJson;
import org.medici.mia.common.json.historylog.RegisterLogActionJson;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.exception.ApplicationThrowable;

public interface HistoryLogService {

	public GenericResponseJson registerLoaAction(
			RegisterLogActionJson registerLogActionJson)
			throws ApplicationThrowable;
	
	public LastAccessedRecordJson getLastAccessedRecords(
			String account)
			throws ApplicationThrowable;

	public GenericResponseJson registerAction(
			Integer id, HistoryLogEntity.UserAction actionType, HistoryLogEntity.RecordType recordType,
			String accessedFrom, Object serialized)
			throws ApplicationThrowable;

	@SuppressWarnings("rawtypes")
	public HashMap getActionsHistory(
			Integer id, String recordType, Integer page, Integer perPage)
			throws ApplicationThrowable;

	@SuppressWarnings("rawtypes")
	public HashMap getActionsHistory(
			Integer page, Integer perPage)
			throws ApplicationThrowable;

	public MyResearchHistoryJson getMyResearchHistory(String account, Integer firstResult, Integer maxResult,
			String orderColumn, String ascOrDesc, String type) throws ApplicationThrowable;

	public Integer deleteMyResearchHistory(String account) throws ApplicationThrowable;
	
	public void registerTranscriptionModification(
			MiaDocumentEntity documentEntity, 
			DocumentTranscriptionJson editedTranscription);
	
	public void registerDocumentCreation(Integer documentEntityId);
}