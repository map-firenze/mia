package org.medici.mia.service.historylog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.medici.mia.common.json.CollectionJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.InsertDescriptionJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.UploadFileJson;
import org.medici.mia.common.json.VolumeDescriptionJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.document.DocumentTranscriptionJson;
import org.medici.mia.common.json.document.GenericDocumentJson;
import org.medici.mia.common.json.folio.FolioJson;
import org.medici.mia.common.json.historylog.ActionHistoryJson;
import org.medici.mia.common.json.historylog.HistoryLogJson;
import org.medici.mia.common.json.historylog.LastAccessedRecordJson;
import org.medici.mia.common.json.historylog.MyResearchHistoryJson;
import org.medici.mia.common.json.historylog.RegisterLogActionJson;
import org.medici.mia.comparator.HistoryLogJsonArchivalLocationAscComparator;
import org.medici.mia.comparator.HistoryLogJsonArchivalLocationDescComparator;
import org.medici.mia.dao.historylog.HistoyLogDAO;
import org.medici.mia.domain.DocumentTranscriptionEntity;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.HistoryLogEntity.RecordType;
import org.medici.mia.domain.HistoryLogEntity.UserAction;
import org.medici.mia.domain.User;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 *
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Service
class HistoryLogServiceImpl implements HistoryLogService {

	@Autowired
	private MiaDocumentService miaDocumentService;
	
	@Autowired
	private UserService userService;

	@Autowired
	private HistoyLogDAO histoyLogDAO;

	@Override
	public GenericResponseJson registerLoaAction(
			RegisterLogActionJson registerLogActionJson)
			throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {

			// Retrieving the Username
			User user = null;
			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}

			HistoryLogEntity entity = new HistoryLogEntity();
			entity.setAction(UserAction.valueOf(registerLogActionJson
					.getAction()));
			entity.setRecordType(RecordType.valueOf(registerLogActionJson
					.getRecordType()));
			entity.setEntryId(registerLogActionJson.getEntryId());
			entity.setUser(user);
			entity.setDateAndTime(new Date());

			getHistoyLogDAO().registerLoaAction(entity);

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("User Action "
					+ registerLogActionJson.getRecordType() + " registerd.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	@Override
	public LastAccessedRecordJson getLastAccessedRecords(String account)
			throws ApplicationThrowable {

		LastAccessedRecordJson lastAccessedRecordJson = new LastAccessedRecordJson();
		try {

			lastAccessedRecordJson.toJson(getHistoyLogDAO().getLogAction(
					account, RecordType.VOL));

			lastAccessedRecordJson.toJson(getHistoyLogDAO().getLogAction(
					account, RecordType.AE));

			lastAccessedRecordJson.toJson(getHistoyLogDAO().getLogAction(
					account, RecordType.DE));

			lastAccessedRecordJson.toJson(getHistoyLogDAO().getLogAction(
					account, RecordType.BIO));

			lastAccessedRecordJson.toJson(getHistoyLogDAO().getLogAction(
					account, RecordType.GEO));

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return lastAccessedRecordJson;

	}

	@Override
	public GenericResponseJson registerAction(Integer id, UserAction actionType, RecordType recordType, String accessedFrom, Object serialized) throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown error");
		try {
			// Retrieving the Username
			User user;
			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}
			Gson gson = new GsonBuilder().serializeNulls().create();

			HistoryLogEntity entity = new HistoryLogEntity();
			entity.setAction(actionType);
			entity.setRecordType(recordType);
			entity.setEntryId(id);
			entity.setUser(user);
			entity.setDateAndTime(new Date());
			entity.setAccessedFrom(accessedFrom);
			entity.setSerializedData(gson.toJson(serialized));

			getHistoyLogDAO().registerLoaAction(entity);

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("User Action "
					+ actionType.toString() + " registered");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
		return resp;
	}

	@Override
	public HashMap<String, Object> getActionsHistory(Integer id, String recordType, Integer page, Integer perPage)
			throws ApplicationThrowable {
		HashMap<String, Object> resp = new HashMap<String, Object>();
		resp.put("message","Unknown error");
		resp.put("status", StatusType.w.toString());
		resp.put("count", 0);
		resp.put("data", new ArrayList());
		try {
			boolean isRecTypeValid = false;
			for(RecordType type: RecordType.values()){
				if(type.name().toUpperCase().equals(recordType.toUpperCase())){
					isRecTypeValid = true;
				}
			}
			if(!isRecTypeValid){
				resp.put("status", StatusType.w.toString()); // Bad request
				resp.put("message", "Record type \"" + recordType + "\" is invalid");
                resp.put("count", 0);
                resp.put("data", new ArrayList());
				return resp;
			}
			List<HistoryLogEntity> historyList = getHistoyLogDAO()
					.getActionsHistory(id, RecordType.valueOf(recordType), page, perPage);
			List<ActionHistoryJson> jsonsList = new ArrayList<ActionHistoryJson>();
			for(HistoryLogEntity historyEntity: historyList){
				ActionHistoryJson actionHistoryJson = new ActionHistoryJson();
				if(actionHistoryJson.toJson(historyEntity) != null){
					jsonsList.add(actionHistoryJson);
				}
			}
			resp.put("message","Got " + jsonsList.size() + " records");
			resp.put("status", StatusType.ok.toString());
			resp.put("count", getHistoyLogDAO().getActionsHistoryCount(id, RecordType.valueOf(recordType)));
			resp.put("data", jsonsList);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
		return resp;
	}

	@Override
	public HashMap<String, Object> getActionsHistory(Integer page, Integer perPage)
			throws ApplicationThrowable {
		HashMap<String, Object> resp = new HashMap<String, Object>();
		resp.put("message","Unknown error");
		resp.put("status", StatusType.w.toString());
		resp.put("count", 0);
		resp.put("data", new ArrayList());
		try {
			List<HistoryLogEntity> historyList = getHistoyLogDAO()
					.getActionsHistory(page, perPage);
			List<ActionHistoryJson> jsonsList = new ArrayList<ActionHistoryJson>();
			for(HistoryLogEntity historyEntity: historyList){
				ActionHistoryJson actionHistoryJson = new ActionHistoryJson();
				if(actionHistoryJson.toJson(historyEntity) != null){
					jsonsList.add(actionHistoryJson);
				}
			}
			resp.put("message","Got " + jsonsList.size() + " records");
			resp.put("status", StatusType.ok.toString());
			resp.put("count", getHistoyLogDAO().getActionsHistoryCount());
			resp.put("data", jsonsList);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
		return resp;
	}

	@Override
	public MyResearchHistoryJson getMyResearchHistory(String account, Integer firstResult, Integer maxResult,
			String orderColumn, String ascOrDesc, String type) throws ApplicationThrowable {
		try {
			List<Object[]> historyList = getHistoyLogDAO().getHistoryItems(account, type, firstResult, maxResult,
					orderColumn, ascOrDesc);
			List<HistoryLogJson> jsonsList = new ArrayList<>();
			Map<Integer, DocumentJson> map = new HashMap<Integer, DocumentJson>();
			for (Object[] historyEntity : historyList) {
				HistoryLogJson historyLogJson = new HistoryLogJson();
				if (historyLogJson.toJson(historyEntity) != null) {
					switch (RecordType.of(historyLogJson.getType())) {
					case DE:
						Integer documentId = historyLogJson.getId();
						DocumentJson documentJson = map.get(documentId);
						if (documentJson == null) {
							documentJson = miaDocumentService.findDocumentById(documentId);
							map.put(documentId, documentJson);
						}
						GenericDocumentJson genericDocumentJson = (GenericDocumentJson) documentJson;
						CollectionJson collection = genericDocumentJson.getCollection();
						if (collection != null) {
							historyLogJson.setCollectionAbbreviation(collection.getCollectionAbbreviation());
						}
						VolumeDescriptionJson volume = genericDocumentJson.getVolume();
						if (volume != null) {
							historyLogJson.setVolumeNo(volume.getVolumeNo());
						}
						InsertDescriptionJson insert = genericDocumentJson.getInsert();
						if (insert != null) {
							historyLogJson.setInsertNo(insert.getInsertNo());
						}
						List<UploadFileJson> uploadFiles = genericDocumentJson.getUploadFiles();
						if (!uploadFiles.isEmpty()) {
							List<FolioJson> folios = uploadFiles.get(0).getFolios();
							boolean foliosPresent = !folios.isEmpty();
							historyLogJson.setFoliosPresent(foliosPresent);
							if (foliosPresent) {
								FolioJson folioJson = folios.get(0);
								if (folioJson != null) {
									historyLogJson.setFolioNumber(folioJson.getFolioNumber());
									historyLogJson.setRectoVerso(folioJson.getRectoverso());
								}
							}
						}
					default:
						break;
					}
					jsonsList.add(historyLogJson);
				}
			}
			Integer count = null;
			if (orderColumn.equalsIgnoreCase("archivalLocation")) {
				if ("asc".equalsIgnoreCase(ascOrDesc)) {
					Collections.sort(jsonsList, new HistoryLogJsonArchivalLocationAscComparator());
				} else if ("desc".equalsIgnoreCase(ascOrDesc)) {
					Collections.sort(jsonsList, new HistoryLogJsonArchivalLocationDescComparator());
				}
				count = jsonsList.size();
				jsonsList = jsonsList.subList(firstResult,
						jsonsList.size() < firstResult + maxResult ? jsonsList.size() : firstResult + maxResult);
			}
			if (count == null) {
				count = getHistoyLogDAO().getHistoryItemsCount(account, type).intValue();
			}
			return new MyResearchHistoryJson(jsonsList, count);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}
	
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Override
	public Integer deleteMyResearchHistory(String account) throws ApplicationThrowable {
		try {
			return getHistoyLogDAO().deleteHistory(account);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}
	
	@Override
	public void registerTranscriptionModification(
			MiaDocumentEntity documentEntity, 
			DocumentTranscriptionJson editedTranscription) {
		
		if (documentEntity == null) {
			return;
		}
		
		DocumentTranscriptionEntity originalTranscription = null;
		if (editedTranscription.getDocTranscriptionId() != null) {
			originalTranscription = findTranscription(
	    		documentEntity.getDocumentTranscriptions(),
	    		Integer.parseInt(editedTranscription.getDocTranscriptionId()));
		}
		
	    if (originalTranscription == null) {
	    	registerDocumentCreation(documentEntity.getDocumentEntityId());
	    	
	    	return;
	    }
		
	    @SuppressWarnings("rawtypes")
		HashMap<String, HashMap> editInfos = prepareEditInfos(
	    		originalTranscription, 
	    		editedTranscription);

	   registerAction(
	    		documentEntity.getDocumentEntityId(),
	    		HistoryLogEntity.UserAction.EDIT, 
	   		 	HistoryLogEntity.RecordType.DE, 
	   		 	null,
	   		 	editInfos);
	}
	
	@Override 
	public void registerDocumentCreation(Integer documentEntityId) {
		registerAction(documentEntityId,
				HistoryLogEntity.UserAction.CREATE,
				HistoryLogEntity.RecordType.DE, null, null);
	}
	
	@SuppressWarnings("rawtypes")
	private HashMap<String, HashMap> prepareEditInfos(
			DocumentTranscriptionEntity originalTranscription,
			DocumentTranscriptionJson editedTranscription) {
		
		HashMap<String, Object> before = new HashMap<String, Object>();
		HashMap<String, Object> after = new HashMap<String, Object>();
		HashMap<String, HashMap> changes = new HashMap<String, HashMap>();
		HashMap<String, HashMap> action = new HashMap<String, HashMap>();
		HashMap<String, HashMap> editInfos = new HashMap<String, HashMap>();
		
		before.put("transcription", originalTranscription.getTranscription());
		before.put("id", originalTranscription.getDocTranscriptionId());
		  
		after.put("transcription", editedTranscription.getTranscription()); 
		after.put(
				"id", 
				Integer.parseInt(editedTranscription.getDocTranscriptionId())); 
		
		changes.put("before", before);
		changes.put("after", after); 
		
		action.put("edit", changes);
		
		editInfos.put("documentTranscription", action);
	  
		return editInfos;
	}

	private DocumentTranscriptionEntity findTranscription(
			List<DocumentTranscriptionEntity> transcriptions,
			Integer transcriptionId) {
		
		DocumentTranscriptionEntity targetTranscription = null;
		
	    List<DocumentTranscriptionEntity> results = transcriptions.stream()
	    		.filter(transcription -> 
	    		transcription.getDocTranscriptionId().equals(transcriptionId))
	    		.collect(Collectors.toList());
	    
	    if (transcriptions != null && transcriptions.size() > 0) {
	    	targetTranscription = results.get(0);
	    }
	    
	    return targetTranscription;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public HistoyLogDAO getHistoyLogDAO() {
		return histoyLogDAO;
	}

	public void setHistoyLogDAO(HistoyLogDAO histoyLogDAO) {
		this.histoyLogDAO = histoyLogDAO;
	}
}