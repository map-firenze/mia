package org.medici.mia.service.volumeinsert;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.PersistenceException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.InsertJson;
import org.medici.mia.common.json.ModifyPersonJson;
import org.medici.mia.common.json.ParentJson;
import org.medici.mia.common.json.PersonJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.VolumeJson;
import org.medici.mia.common.json.biographical.AddHeadquarterJson;
import org.medici.mia.common.json.biographical.AltNameJson;
import org.medici.mia.common.json.biographical.BiographicalPeopleJson;
import org.medici.mia.common.json.biographical.FindPersonChildrenJson;
import org.medici.mia.common.json.biographical.FindPersonParentsJson;
import org.medici.mia.common.json.biographical.FindPersonSpousesJson;
import org.medici.mia.common.json.biographical.FindPersonTitlesOccsJson;
import org.medici.mia.common.json.biographical.HeadquarterJson;
import org.medici.mia.common.json.biographical.ModifyPersonChildJson;
import org.medici.mia.common.json.biographical.ModifyPersonParentsJson;
import org.medici.mia.common.json.biographical.ModifyPersonSpouseJson;
import org.medici.mia.common.json.biographical.ModifyPersonTitlesOccsJson;
import org.medici.mia.common.json.biographical.OrganizationJson;
import org.medici.mia.common.json.biographical.PersonPortraitJson;
import org.medici.mia.common.json.biographical.PoLinkJson;
import org.medici.mia.common.json.biographical.SpouseJson;
import org.medici.mia.common.json.biographical.SpouseType;
import org.medici.mia.common.json.biographical.TitleOccJson;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.common.search.AdvancedSearchAbstract.Gender;
import org.medici.mia.common.search.AdvancedSearchAbstract.NameType;
import org.medici.mia.common.util.DateUtils;
import org.medici.mia.dao.altname.AltNameDAO;
import org.medici.mia.dao.document.DocumentDAO;
import org.medici.mia.dao.insert.InsertDAO;
import org.medici.mia.dao.marriage.MarriageDAO;
import org.medici.mia.dao.parent.ParentDAO;
import org.medici.mia.dao.people.BiographicalPeopleDAO;
import org.medici.mia.dao.people.OrganizationDAO;
import org.medici.mia.dao.people.PeopleDAO;
import org.medici.mia.dao.polink.PoLinkDAO;
import org.medici.mia.dao.titleoccslist.TitleOccsListDAO;
import org.medici.mia.dao.user.UserDAO;
import org.medici.mia.dao.userhistory.UserHistoryDAO;
import org.medici.mia.dao.volume.VolumeDAO;
import org.medici.mia.domain.AltName;
import org.medici.mia.domain.BiographicalPeople;
import org.medici.mia.domain.Document;
import org.medici.mia.domain.InsertEntity;
import org.medici.mia.domain.Marriage;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.Month;
import org.medici.mia.domain.OrganizationEntity;
import org.medici.mia.domain.People;
import org.medici.mia.domain.PoLink;
import org.medici.mia.domain.TitleOccsList;
import org.medici.mia.domain.UploadInfoEntity;
import org.medici.mia.domain.User;
import org.medici.mia.domain.UserHistory;
import org.medici.mia.domain.Volume;
import org.medici.mia.domain.UserHistory.Action;
import org.medici.mia.domain.UserHistory.Category;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.security.MiaUserDetailsImpl;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
class VolumeInsertDescriptionServiceImpl implements VolumeInsertDescriptionService {

	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private UserService userService;
	
	@Autowired
	private UserDAO userDAO;

	@Autowired
	private VolumeDAO volumeDAO;

	@Autowired
	private InsertDAO insertDAO;
	@Autowired
	private UserHistoryDAO userHistoryDAO;
	@Autowired
	private DocumentDAO documentDAO;
	
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Override
	public InsertEntity addNewInsert(InsertEntity insert) throws ApplicationThrowable {
		try {
			// Setting primary key to null to permit persist operation, otherwise jpa will throw a Persistence Object Expcetion
			insert.setInsertId(null);
	

			// Retrieves every object references
			if (insert.getVolume() != null) {
				insert.setVolumeEntity(volumeDAO.find(insert.getVolume()));
			}

			//Setting fields that are defined as nullable = false
			User user = userDAO.findUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()); 
			insert.setCreatedBy(user);
			insert.setLogicalDelete(false);
			insert.setLastUpdateBy(user);
			insert.setDateCreated(new Date());
			insert.setDateLastUpdate(new Date());
			
			
			insert.setLogicalDelete(Boolean.FALSE);
	

			insertDAO.persist(insert);
			
			return insert;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public InsertEntity getInsertByVolumeAndInsertName(InsertEntity  insert) throws ApplicationThrowable {
		List<InsertEntity> listByNameVolId = insertDAO
				.findMatchInsertByName(
						insert.getInsertName(),
						insert.getVolume());
		if (listByNameVolId.size() != 0) {
			return listByNameVolId.get(0);
			
		}
		return null;
	}



	@Override
	public HashMap<String, List<Integer>> checkDataBeforeDeleteVolumeById(Integer volumeId) throws ApplicationThrowable {

		HashMap<String, List<Integer>> maps = new HashMap<String, List<Integer>>();
		Volume volumeToDelete = null;

		volumeToDelete = volumeDAO.find(volumeId);
		if (volumeToDelete == null) {
			return null;
		}

    //Restituire l'elenco dei documenti collegati
	List<UploadInfoEntity> docs = volumeToDelete.getUploadInfoEntities();
	if (docs != null && docs.size() > 0 ) {
		List<Integer> list = new ArrayList<Integer>();
		for (Iterator iterator = docs.iterator(); iterator.hasNext();) {
			UploadInfoEntity document = (UploadInfoEntity) iterator.next();
			if (document.getLogicalDelete() != 1) {
				list.add(document.getUploadInfoId());
			}
		}
		maps.put("archivalEntities", list);
	}

	//NEssun insert e nessun document
	List<InsertEntity> ins = volumeToDelete.getInsertEntities();
	if (docs != null && docs.size() > 0 ) {
		List<Integer> list = new ArrayList<Integer>();
		for (Iterator iterator = ins.iterator(); iterator.hasNext();) {
			InsertEntity document = (InsertEntity) iterator.next();
			if (!document.getLogicalDelete()) {
				list.add(document.getInsertId());
			}
		}
		maps.put("inserts", list);
	}

	
	//It should not be possible to delete a volume record if any Inserts or Archival Entities or Document Entities are connected
	return maps;
	
	}
	
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Override
	public GenericResponseJson deleteVolumeById(Integer volumeId) throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {

			User user = null;
			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}
			


			Volume volumeToDelete = null;
			try {
				volumeToDelete = volumeDAO.find(volumeId);
			} catch (Throwable th) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("No volume with id: " + volumeId + " found");
				return resp;
			}

			volumeToDelete.setLastUpdate(new Date());
			volumeToDelete.setLogicalDelete(Boolean.TRUE);
			volumeToDelete.setDeletedBy(user);
			volumeToDelete.setDateDeleted(new Date());
			

			try {
				volumeDAO.merge(volumeToDelete);
				
				//Log azione delete
//				userHistoryDAO.persist(new UserHistory(user, "Delete volume", Action.DELETE, Category.VOLUME, volumeToDelete));
			} catch (Throwable th) {
				throw new ApplicationThrowable(th);
			}

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Volume deleted.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}
	
	@Override
	public HashMap<String, List<Integer>> checkDataBeforeDeleteInsertById(Integer insertId) throws ApplicationThrowable {

		HashMap<String, List<Integer>> maps = new HashMap<String, List<Integer>>();
		InsertEntity insertToDelete = null;

		insertToDelete = insertDAO.find(insertId);
		if (insertToDelete == null) {
			return null;
		}
	    //Restituire l'elenco dei documenti collegati
		List<UploadInfoEntity> docs = insertToDelete.getUploadInfoEntities();
		if (docs != null && docs.size() > 0 ) {
			List<Integer> list = new ArrayList<Integer>();
			for (Iterator iterator = docs.iterator(); iterator.hasNext();) {
				UploadInfoEntity document = (UploadInfoEntity) iterator.next();
				if (document.getLogicalDelete() != 1) {
					list.add(document.getUploadInfoId());
				}
			}
			maps.put("archivalEntities", list);
		}

		//It should not be possible to delete a volume record if any Inserts or Archival Entities or Document Entities are connected
		return maps;
	
	}
	
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Override
	public GenericResponseJson deleteInsertById(Integer insertId) throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {

			User user = null;
			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Error retrieving the User logged in");
				return resp;
			}

			//nessun documento collegato

			
			InsertEntity insertToDelete = null;
			try {
				insertToDelete = insertDAO.find(insertId);
			} catch (Throwable th) {
				throw new ApplicationThrowable(th);
			}

			insertToDelete.setDateLastUpdate(new Date());
			insertToDelete.setLogicalDelete(Boolean.TRUE);
			insertToDelete.setDateDeleted(new Date());
			insertToDelete.setDeletedBy(user);

			try {
				insertDAO.merge(insertToDelete);
				
				//Log azione delete
			//	userHistoryDAO.persist(new UserHistory(user, "Delete insert", Action.DELETE, Category.INSERT, volumeToDelete));
			} catch (Throwable th) {
				throw new ApplicationThrowable(th);
			}

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Insert deleted.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}


	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}


}
