package org.medici.mia.service.volumeinsert;

import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.domain.InsertEntity;
import org.medici.mia.exception.ApplicationThrowable;

public interface VolumeInsertDescriptionService {

	public GenericResponseJson deleteVolumeById(Integer volumeId) throws ApplicationThrowable;
	
	public GenericResponseJson deleteInsertById(Integer insertId) throws ApplicationThrowable;

	InsertEntity addNewInsert(InsertEntity insert) throws ApplicationThrowable;

	InsertEntity getInsertByVolumeAndInsertName(InsertEntity insert)
			throws ApplicationThrowable;

	HashMap<String, List<Integer>> checkDataBeforeDeleteVolumeById(
			Integer volumeId) throws ApplicationThrowable;

	HashMap<String, List<Integer>> checkDataBeforeDeleteInsertById(
			Integer insertId) throws ApplicationThrowable;
	
	
	

}