package org.medici.mia.service.useractivity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.advancedsearch.AdvancedSearchFactoryJson;
import org.medici.mia.common.json.advancedsearch.GenericAdvancedSearchJson;
import org.medici.mia.common.json.advancedsearch.IAdvancedSearchJson;
import org.medici.mia.common.json.news.MyNewsfeedItemJson;
import org.medici.mia.common.json.news.NewsAEJson;
import org.medici.mia.common.json.news.NewsDocumentJson;
import org.medici.mia.common.json.news.NewsPeopleJson;
import org.medici.mia.common.json.news.NewsPlaceJson;
import org.medici.mia.common.json.news.NewsUserJson;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.dao.people.PeopleDAO;
import org.medici.mia.dao.place.PlaceDAO;
import org.medici.mia.dao.uploadinfo.UploadInfoDAO;
import org.medici.mia.dao.userpreference.UserPreferenceDAO;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.People;
import org.medici.mia.domain.Place;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.UploadInfoEntity;
import org.medici.mia.domain.User;
import org.medici.mia.domain.UserPreference;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.user.ValidateUserForUploads;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Service
class NewsServiceImpl implements NewsService {
	
	@Autowired
	private UserPreferenceDAO userPreferenceDAO;

	@Autowired
	private GenericDocumentDao documentDAO;

	@Autowired
	private UploadInfoDAO uploadInfoDAO;

	@Autowired
	private PeopleDAO peopleDao;

	@Autowired
	private PlaceDAO placeDao;
	
	@Autowired
	private ValidateUserForUploads validateUserForUploads;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public NewsUserJson findNews(Date date, int numberOfUpdates) throws ApplicationThrowable {

		try {
			UserDetails currentUser = ((UserDetails) SecurityContextHolder.getContext()
					.getAuthentication().getPrincipal());
			NewsUserJson UserActivityjson = new NewsUserJson();
			List<MiaDocumentEntity> docEntities = getDocumentDAO()
					.getNews(date, numberOfUpdates);
			if (docEntities != null && !docEntities.isEmpty()) {
				List<NewsDocumentJson> documents = new ArrayList<NewsDocumentJson>();
				for (MiaDocumentEntity docEntitiy : docEntities) {
					NewsDocumentJson json = new NewsDocumentJson();
					json.toJson(docEntitiy);
					if (json.getActivityDate() != null) {
						documents.add(json);
					}
				}
				UserActivityjson.setDocuments(documents);
			}

			List<UploadInfoEntity> aeEntities = getUploadInfoDAO()
					.getNews(date, numberOfUpdates);
			if (aeEntities != null && !aeEntities.isEmpty()) {

				List<NewsAEJson> aes = new ArrayList<NewsAEJson>();
				for (UploadInfoEntity aeEntity : aeEntities) {
					if(aeEntity.getOptionalImage() == null) {
						List<UploadFileEntity> fileCanWiew = new ArrayList<UploadFileEntity>();
						for(UploadFileEntity file : aeEntity.getUploadFileEntities()) {
							if(file.getFilePrivacy() == 1) {
								if(validateUserForUploads.isSharedFileWithMe(aeEntity.getUploadInfoId(), currentUser, file.getUploadFileId()) || validateUserForUploads.isAdminOrOwnerOrFellows(aeEntity.getOwner(),currentUser)) {
									fileCanWiew.add(file);
								}
							} else {
								fileCanWiew.add(file);
							}
						}
						if(fileCanWiew.size() > 0) {
							aeEntity.setUploadFileEntities(fileCanWiew);
							NewsAEJson json = new NewsAEJson();
							json.toJson(aeEntity);
							if (json.getActivityDate() != null) {
								aes.add(json);
							}
						}

					}
				}
				UserActivityjson.setAes(aes);
			}

			List<People> peopleEntitiy = getPeopleDao().getNews(date);
			if (peopleEntitiy != null && !peopleEntitiy.isEmpty()) {
				List<NewsPeopleJson> peopleJson = new ArrayList<NewsPeopleJson>();
				for (People peopleEnt : peopleEntitiy) {
					NewsPeopleJson json = new NewsPeopleJson();
					json.toJson(peopleEnt);
					if (json.getActivityDate() != null) {
						peopleJson.add(json);
					}
				}
				UserActivityjson.setPeople(peopleJson);
			}

			List<Place> placeEntities = getPlaceDao().getNews(date);
			if (placeEntities != null && !placeEntities.isEmpty()) {
				List<NewsPlaceJson> placesJson = new ArrayList<NewsPlaceJson>();
				for (Place placeEnt : placeEntities) {
					NewsPlaceJson json = new NewsPlaceJson();
					json.toJson(placeEnt);
					if (json.getActivityDate() != null) {
						placesJson.add(json);
					}
				}
				UserActivityjson.setPlaces(placesJson);
			}

			return UserActivityjson;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Override
	public List<MyNewsfeedItemJson> getMyNewsfeed(User user) {
		List<MyNewsfeedItemJson> listJson = new ArrayList<>();
		try {
			List<UserPreference> list = getUserPreferenceDAO().getUserPreferences(user);
			for (UserPreference item : list) {
				MyNewsfeedItemJson itemJson = new MyNewsfeedItemJson().toJson(item);
				itemJson.setMatchingDocuments(countMatchingDocuments(itemJson.getSearchFilter(), user));
				listJson.add(itemJson);
			}
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
		return listJson;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseDataJson<MyNewsfeedItemJson> createMyNewsfeedItem(User user,
			MyNewsfeedItemJson myNewsfeedItemJson) {
		GenericResponseDataJson<MyNewsfeedItemJson> resp = new GenericResponseDataJson<MyNewsfeedItemJson>();
		try {
			Integer newsfeedMaxNumber = NumberUtils
					.createInteger(ApplicationPropertyManager.getApplicationProperty("newsfeed.max.number"));

			Long count = getUserPreferenceDAO().getUserPreferenceCount(user);
			if (count >= newsfeedMaxNumber) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("maximum number of allowed items is " + newsfeedMaxNumber);
			} else {
				UserPreference entity = new UserPreference();
				entity.setUser(user);
				entity.setTitle(myNewsfeedItemJson.getTitle());
				entity.setSerializedData(
						new ObjectMapper().writerFor(new TypeReference<List<GenericAdvancedSearchJson>>() {
						}).writeValueAsString(myNewsfeedItemJson.getSearchFilter()));
				getUserPreferenceDAO().persist(entity);

				MyNewsfeedItemJson itemJson = new MyNewsfeedItemJson().toJson(entity);
				itemJson.setMatchingDocuments(countMatchingDocuments(itemJson.getSearchFilter(), user));

				resp.setData(itemJson);
				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("item successfully created");
			}
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
		return resp;
	}

	private Integer countMatchingDocuments(List<GenericAdvancedSearchJson> adSearches, User user) {

		List<String> queryList = new ArrayList<String>();

		for (GenericAdvancedSearchJson advancedSearchJson : adSearches) {
			advancedSearchJson.setNewsfeedUser(user);
			IAdvancedSearchJson search = AdvancedSearchFactoryJson.getAdvancedSearchJson(advancedSearchJson);
			if (search != null && advancedSearchJson.isActiveFilter()) {
				List<String> queries = (search.getQueries());
				if (queries != null && !queries.isEmpty()) {
					for (String query : queries) {
						queryList.add(query);

					}
				}
			}
		}
		List<Integer> docIds = getDocumentDAO().countDocumentsForAdvancedSearch(queryList);
		return docIds == null ? 0 : docIds.size();
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public void deleteMyNewsfeedItem(User user, Integer id) {
		try {
			getUserPreferenceDAO().deleteUserPreference(user, id);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	public UserPreferenceDAO getUserPreferenceDAO() {
		return userPreferenceDAO;
	}

	public void setUserPreferenceDAO(UserPreferenceDAO userPreferenceDAO) {
		this.userPreferenceDAO = userPreferenceDAO;
	}

	public GenericDocumentDao getDocumentDAO() {
		return documentDAO;
	}

	public void setDocumentDAO(GenericDocumentDao documentDAO) {
		this.documentDAO = documentDAO;
	}

	public UploadInfoDAO getUploadInfoDAO() {
		return uploadInfoDAO;
	}

	public void setUploadInfoDAO(UploadInfoDAO uploadInfoDAO) {
		this.uploadInfoDAO = uploadInfoDAO;
	}

	public PeopleDAO getPeopleDao() {
		return peopleDao;
	}

	public void setPeopleDao(PeopleDAO peopleDao) {
		this.peopleDao = peopleDao;
	}

	public PlaceDAO getPlaceDao() {
		return placeDao;
	}

	public void setPlaceDao(PlaceDAO placeDao) {
		this.placeDao = placeDao;
	}
	
}