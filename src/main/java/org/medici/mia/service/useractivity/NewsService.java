package org.medici.mia.service.useractivity;

import java.util.Date;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.news.MyNewsfeedItemJson;
import org.medici.mia.common.json.news.NewsUserJson;
import org.medici.mia.domain.User;
import org.medici.mia.exception.ApplicationThrowable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public interface NewsService {

	public NewsUserJson findNews(Date date, int limit)
			throws ApplicationThrowable;

	public List<MyNewsfeedItemJson> getMyNewsfeed(User user);

	public GenericResponseDataJson<MyNewsfeedItemJson> createMyNewsfeedItem(User user,
			MyNewsfeedItemJson myNewsfeedItemJson);

	public void deleteMyNewsfeedItem(User user, Integer id);

}