package org.medici.mia.service.advancedsearch;

import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.biographical.BiographicalPeopleJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.exception.ApplicationThrowable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public interface AdvancedSearchService {

	public GenericResponseDataJson<Integer> countDocumentsAdvancedSearch(
			List<String> searchList) throws ApplicationThrowable;

	public GenericResponseDataJson<List<DocumentJson>> getDocumentsAdvancedSearch(
			List<String> searchList, Integer firstResult, Integer maxResilt,
			String orderColumn, String ascOrDesc) throws ApplicationThrowable;

	public GenericResponseDataJson<Integer> countPeopleAdvancedSearch(
			List<String> queryList) throws ApplicationThrowable;

	public GenericResponseDataJson<List<BiographicalPeopleJson>> getPeopleAdvancedSearch(
			List<String> queryList, Integer firstResult, Integer maxResult,
			String orderColumn, String ascOrDesc) throws ApplicationThrowable;

}
