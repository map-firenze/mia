package org.medici.mia.service.advancedsearch;

import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.biographical.BiographicalPeopleJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.dao.altname.AltNameDAO;
import org.medici.mia.dao.miadoc.DocumentTranscriptionDAO;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.dao.people.BiographicalPeopleDAO;
import org.medici.mia.dao.people.PeopleDAO;
import org.medici.mia.dao.place.PlaceDAO;
import org.medici.mia.dao.uploadfile.UploadFileDAO;
import org.medici.mia.dao.uploadinfo.UploadInfoDAO;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.people.BiographicalPeopleService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Service
public class AdvancedSearchServiceImpl implements AdvancedSearchService {

	@Autowired
	private MiaDocumentService miaDocumentService;

	@Autowired
	private BiographicalPeopleService biographicalPeopleService;

	@Autowired
	private GenericDocumentDao documentDAO;

	@Autowired
	private UploadInfoDAO uploadInfoDAO;

	@Autowired
	private UploadFileDAO uploadFileDAO;

	@Autowired
	private UserService userService;

	@Autowired
	private AltNameDAO altNameDAO;

	@Autowired
	private PlaceDAO placeDAO;

	@Autowired
	private PeopleDAO peopleDAO;

	@Autowired
	private DocumentTranscriptionDAO documentTranscriptionDAO;

	@Autowired
	private BiographicalPeopleDAO biographicalPeopleDAO;

	@Override
	public GenericResponseDataJson<Integer> countDocumentsAdvancedSearch(
			List<String> queryList) throws ApplicationThrowable {
		GenericResponseDataJson<Integer> resp = new GenericResponseDataJson<Integer>();

		List<Integer> docIds = getDocumentDAO()
				.countDocumentsForAdvancedSearch(queryList);
		if (docIds == null || docIds.isEmpty()) {
			resp.setMessage("No search list found in DB .");
			resp.setStatus(StatusType.ok.toString());
			resp.setData(0);
			return resp;
		}

		resp.setData(docIds.size());
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage(docIds.size() + " Document entities found");

		return resp;

	}

	@Override
	public GenericResponseDataJson<Integer> countPeopleAdvancedSearch(
			List<String> queryList) throws ApplicationThrowable {
		GenericResponseDataJson<Integer> resp = new GenericResponseDataJson<Integer>();

		List<Integer> docIds = getDocumentDAO()
				.countDocumentsForAdvancedSearch(queryList);
		if (docIds == null || docIds.isEmpty()) {
			resp.setMessage("No search list found in DB .");
			resp.setStatus(StatusType.ok.toString());
			resp.setData(0);
			return resp;
		}

		resp.setData(docIds.size());
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage(docIds.size() + " People entities found");

		return resp;

	}

	@Override
	public GenericResponseDataJson<List<DocumentJson>> getDocumentsAdvancedSearch(
			List<String> queryList, Integer firstResult, Integer maxResult,
			String orderColumn, String ascOrDesc) throws ApplicationThrowable {
		GenericResponseDataJson<List<DocumentJson>> resp = new GenericResponseDataJson<List<DocumentJson>>();

		List<Integer> docIds = getDocumentDAO()
				.countDocumentsForAdvancedSearch(queryList);
		if (docIds == null || docIds.isEmpty()) {
			resp.setMessage("No search list found in DB .");
			resp.setStatus(StatusType.w.toString());
			return resp;
		}

		resp.setMessage("Search list found in DB ." + docIds.size());
		resp.setStatus(StatusType.ok.toString());
		resp.setData(getMiaDocumentService().findDocuments(docIds, firstResult,
				maxResult, orderColumn, ascOrDesc));
		return resp;

	}

	@Override
	public GenericResponseDataJson<List<BiographicalPeopleJson>> getPeopleAdvancedSearch(
			List<String> queryList, Integer firstResult, Integer maxResult,
			String orderColumn, String ascOrDesc) throws ApplicationThrowable {
		GenericResponseDataJson<List<BiographicalPeopleJson>> resp = new GenericResponseDataJson<List<BiographicalPeopleJson>>();

		List<Integer> personIds = getBiographicalPeopleDAO()
				.countPeopleForAdvancedSearch(queryList);
		if (personIds == null || personIds.isEmpty()) {
			resp.setMessage("No search list found in DB .");
			resp.setStatus(StatusType.w.toString());
			return resp;
		}

		resp.setMessage("Search list found in DB ." + personIds.size());
		resp.setStatus(StatusType.ok.toString());
		resp.setData(getBiographicalPeopleService().findBiographicalPeople(
				personIds, firstResult, maxResult, orderColumn, ascOrDesc));
		return resp;

	}

	public GenericDocumentDao getDocumentDAO() {
		return documentDAO;
	}

	public void setDocumentDAO(GenericDocumentDao documentDAO) {
		this.documentDAO = documentDAO;
	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

	public void setMiaDocumentService(MiaDocumentService miaDocumentService) {
		this.miaDocumentService = miaDocumentService;
	}

	public UploadInfoDAO getUploadInfoDAO() {
		return uploadInfoDAO;
	}

	public void setUploadInfoDAO(UploadInfoDAO uploadInfoDAO) {
		this.uploadInfoDAO = uploadInfoDAO;
	}

	public UploadFileDAO getUploadFileDAO() {
		return uploadFileDAO;
	}

	public void setUploadFileDAO(UploadFileDAO uploadFileDAO) {
		this.uploadFileDAO = uploadFileDAO;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public AltNameDAO getAltNameDAO() {
		return altNameDAO;
	}

	public void setAltNameDAO(AltNameDAO altNameDAO) {
		this.altNameDAO = altNameDAO;
	}

	public PlaceDAO getPlaceDAO() {
		return placeDAO;
	}

	public void setPlaceDAO(PlaceDAO placeDAO) {
		this.placeDAO = placeDAO;
	}

	public PeopleDAO getPeopleDAO() {
		return peopleDAO;
	}

	public void setPeopleDAO(PeopleDAO peopleDAO) {
		this.peopleDAO = peopleDAO;
	}

	public DocumentTranscriptionDAO getDocumentTranscriptionDAO() {
		return documentTranscriptionDAO;
	}

	public void setDocumentTranscriptionDAO(
			DocumentTranscriptionDAO documentTranscriptionDAO) {
		this.documentTranscriptionDAO = documentTranscriptionDAO;
	}

	public BiographicalPeopleDAO getBiographicalPeopleDAO() {
		return biographicalPeopleDAO;
	}

	public void setBiographicalPeopleDAO(
			BiographicalPeopleDAO biographicalPeopleDAO) {
		this.biographicalPeopleDAO = biographicalPeopleDAO;
	}

	public BiographicalPeopleService getBiographicalPeopleService() {
		return biographicalPeopleService;
	}

	public void setBiographicalPeopleService(
			BiographicalPeopleService biographicalPeopleService) {
		this.biographicalPeopleService = biographicalPeopleService;
	}

}
