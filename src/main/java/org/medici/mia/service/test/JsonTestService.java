/*
 * JsonTestService.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.service.test;

import org.medici.mia.controller.test.JsonTest;
import org.medici.mia.domain.CollectionEntity;
import org.medici.mia.domain.GoogleLocation;
import org.medici.mia.domain.RepositoryEntity;
import org.medici.mia.domain.SeriesEntity;
import org.medici.mia.exception.ApplicationThrowable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public interface JsonTestService {

	public void save(JsonTest jsonTest) throws ApplicationThrowable;

	public void delete(JsonTest jsonTest) throws ApplicationThrowable;

	public void update(JsonTest jsonTest) throws ApplicationThrowable;

	public void retrieve(JsonTest jsonTest) throws ApplicationThrowable;

	public Integer insertRepository(RepositoryEntity repEntity,
			CollectionEntity collEntity) throws ApplicationThrowable;

	public Integer insertCollection(CollectionEntity collEntity)
			throws ApplicationThrowable;

	public String insertUploadFiles(GoogleLocation gl, RepositoryEntity rep,
			CollectionEntity coll, SeriesEntity ser) throws ApplicationThrowable;
	
	void testAsync() throws ApplicationThrowable;
}
