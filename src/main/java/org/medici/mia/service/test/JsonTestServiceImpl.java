package org.medici.mia.service.test;

import org.apache.log4j.Logger;
import org.medici.mia.controller.test.JsonTest;
import org.medici.mia.dao.test.JsonTestDAO;
import org.medici.mia.domain.CollectionEntity;
import org.medici.mia.domain.GoogleLocation;
import org.medici.mia.domain.RepositoryEntity;
import org.medici.mia.domain.SeriesEntity;
import org.medici.mia.exception.ApplicationThrowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Service
@Transactional(readOnly = true, rollbackFor = ApplicationThrowable.class)
public class JsonTestServiceImpl implements JsonTestService {

	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired(required = false)
	@Qualifier(value = "jsonTestDAOJpaImpl")
	private JsonTestDAO jsonTestDAO;

	// @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Override
	public void save(JsonTest jsonTest) throws ApplicationThrowable {

		logger.info("SUCCESS: Inside JsonTestService as Save service - Json Values on Service are:"
				+ jsonTest.getId() + " " + jsonTest.getName());
		getJsonTestDAO().save(jsonTest);

	}

	public void delete(JsonTest jsonTest) throws ApplicationThrowable {

		logger.info("SUCCESS: Inside JsonTestService as Delete service - Json Values on Service are:"
				+ jsonTest.getId() + " " + jsonTest.getName());
		getJsonTestDAO().delete(jsonTest);

	}

	public void update(JsonTest jsonTest) throws ApplicationThrowable {

		logger.info("SUCCESS: Inside JsonTestService as Update service - Json Values on Service are:"
				+ jsonTest.getId() + " " + jsonTest.getName());
		getJsonTestDAO().update(jsonTest);

	}

	public void retrieve(JsonTest jsonTest) throws ApplicationThrowable {

		logger.info("SUCCESS: Inside JsonTestService as Retrieve service - Json Values on Service are:"
				+ jsonTest.getId() + " " + jsonTest.getName());
		getJsonTestDAO().retrieve(jsonTest);

	}

	public Integer insertRepository(RepositoryEntity repEntity,
			CollectionEntity collEntity) throws ApplicationThrowable {

		getJsonTestDAO().insertRepository(repEntity);
		Integer repId = repEntity.getRepositoryId();
		collEntity.setRepositoryId(repId);
		getJsonTestDAO().insertCollection(collEntity);
		return collEntity.getCollectionId();

	}

	public Integer insertCollection(CollectionEntity collEntity)
			throws ApplicationThrowable {

		getJsonTestDAO().insertCollection(collEntity);
		return collEntity.getCollectionId();

	}

	public String insertUploadFiles(GoogleLocation gl, RepositoryEntity rep,
			CollectionEntity coll, SeriesEntity ser)
			throws ApplicationThrowable {

		String finalPath = "";
		Integer repInfoId = null;
		Integer collInfoId = null;
		Integer serInfoId = null;
		if (rep.getRepositoryId() == null) {
			// Repository is new
			// Check if GoogleLocation in this new Repository is new

			if (getJsonTestDAO().getGoogleLocation(rep.getLocation()) == null) {
				// googleLocation doesn't exist . inser google Location
				getJsonTestDAO().insertGoogleLocation(gl);
			}

			repInfoId = getJsonTestDAO().insertRepository(rep);

		} else {
			//
			repInfoId = rep.getRepositoryId();
		}

		// Gestione Collection
		if (coll.getCollectionId() == null) {
			// Collection is new
			coll.setRepositoryId(repInfoId);
			collInfoId = getJsonTestDAO().insertCollection(coll);

		} else {
			collInfoId = coll.getCollectionId();
		}

		// Gestione Series
		if (ser.getSeriesId() == null) {
			// Series is new
			ser.setCollection(collInfoId);
			serInfoId = getJsonTestDAO().insertSeries(ser);

		} else {
			serInfoId = ser.getSeriesId();
		}

		finalPath = String.valueOf(repInfoId) + "-"
				+ String.valueOf(collInfoId) + "-" + String.valueOf(serInfoId);

		return finalPath;

	}

	@Override
	@Async
	public void testAsync() throws ApplicationThrowable {
		try {
			doSth();
		} catch (Exception e) {
			e.getMessage();
		}
	}

	private void doSth() throws ApplicationThrowable, Exception {

		for (int i = 0; i < 10; i++) {

			Thread.sleep(5000);
			System.out.println("we are inside doSth");
		}

	}

	public JsonTestDAO getJsonTestDAO() {
		return jsonTestDAO;
	}

	public void setJsonTestDAO(JsonTestDAO jsonTestDAO) {
		this.jsonTestDAO = jsonTestDAO;
	}

}
