package org.medici.mia.service.folio;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.folio.FolioJson;
import org.medici.mia.common.util.FileUtils;
import org.medici.mia.dao.collection.CollectionDAO;
import org.medici.mia.dao.folio.FolioDao;
import org.medici.mia.dao.insert.InsertDAO;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.dao.uploadfile.UploadFileDAO;
import org.medici.mia.dao.uploadinfo.UploadInfoDAO;
import org.medici.mia.dao.user.UserDAO;
import org.medici.mia.dao.volume.VolumeDAO;
import org.medici.mia.domain.CollectionEntity;
import org.medici.mia.domain.FolioEntity;
import org.medici.mia.domain.InsertEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.UploadInfoEntity;
import org.medici.mia.domain.User;
import org.medici.mia.domain.Volume;
import org.medici.mia.exception.ApplicationThrowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Service
@Transactional(readOnly = true)
public class FolioServiceImpl implements FolioService {

	public static final String ERRORMSG_INCORRECT_INPUT = "ERROR: The Input value may be null or have no correct value.";
	public static final String WARNINGMSG_UPLOAD_CHANGETO_PUBLIC = "WARNING: The upload belongs to this file changed to public.";
	public static final String WARNINGMSG_PRIVACY_NOTUPDATED = "WARNING: The upload belongs to this file changed to public.";

	@Autowired
	private UploadInfoDAO uploadInfoDAO;

	@Autowired
	private CollectionDAO collectionDAO;

	@Autowired
	private VolumeDAO volumeDAO;

	@Autowired
	private InsertDAO insertDAO;
	
	@Autowired
	private GenericDocumentDao genericDocumentDAO;
	
	@Autowired
	private UploadFileDAO uploadFileDAO;
	
	@Autowired
	private FolioDao folioDAO;
	
	@Autowired
	private UserDAO userDAO;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public UploadCollectionJson getUploads(Integer id, UploadCollectionType type)
			throws ApplicationThrowable {

		UploadCollectionJson resp = new UploadCollectionJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Unknown errore.");

		try {
			if (UploadCollectionType.INSERT.equals(type)) {

				InsertEntity insert = getInsertDAO().find(id);
				if (insert == null) {
					resp.setMessage("Insert not found in DB.");
					resp.setStatus(StatusType.w.toString());
					return resp;
				}

				resp = getUploadCollectionsInsert(insert
						.getUploadInfoEntities());

				if (insert.getVolumeEntity() != null
						&& insert.getVolumeEntity().getCollection() != null) {
					CollectionEntity col = getCollectionDAO().find(
							insert.getVolumeEntity().getCollection());
					if (col != null) {
						resp.setColectionId(col.getCollectionId());
						resp.setRepositoryId(col.getRepositoryId());
					}

				}

			} else if (UploadCollectionType.VOLUME.equals(type)) {

				Volume volume = getVolumeDAO().find(id);
				if (volume == null) {
					resp.setMessage("Volume not found in DB.");
					resp.setStatus(StatusType.w.toString());
					return resp;
				}

				resp = getUploadCollectionsVolume(volume
						.getUploadInfoEntities());

				if (volume.getCollection() != null) {
					CollectionEntity col = getCollectionDAO().find(
							volume.getCollection());
					if (col != null) {
						resp.setColectionId(volume.getCollection());
						resp.setRepositoryId(col.getRepositoryId());
					}

				}
			}

		} catch (Exception th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	private UploadCollectionJson getUploadCollectionsVolume(
			List<UploadInfoEntity> uploads) throws ApplicationThrowable {

		UploadCollectionJson resp = new UploadCollectionJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Unknown errore.");

		try {

			if (uploads == null || uploads.isEmpty()) {
				resp.setMessage("Uploads are not present.");
				resp.setStatus(StatusType.w.toString());
				return resp;
			}

			Map<Integer, ThumbsFileJson> thumbsFileMap = new HashMap<Integer, ThumbsFileJson>();
			Map<Folio, Integer> folioMap = new HashMap<Folio, Integer>();
			ThumbsFileJson fileJson = null;

			for (UploadInfoEntity upload : uploads) {

				List<UploadFileEntity> uploadFiles = upload
						.getUploadFileEntities();

				if (uploadFiles != null && !uploadFiles.isEmpty()) {

					for (UploadFileEntity uploadFile : uploadFiles) {

						boolean isFolioExist = false;
						List<UploadCollectionFolioJson> folios = new ArrayList<UploadCollectionFolioJson>();

						// Set Folios
						if (uploadFile.getFolioEntities() != null
								&& !uploadFile.getFolioEntities().isEmpty()) {

							for (FolioEntity folio : uploadFile
									.getFolioEntities()) {
								if (folio.getFolioNumber() == null
										&& folio.getRectoverso() == null) {
									isFolioExist = true;
									break;
								} else {
									Folio fnrv = new Folio(
											folio.getFolioNumber(),
											folio.getRectoverso(),
											folio.getNoNumb());

									if (folioMap.containsKey(fnrv)) {
										if (thumbsFileMap.get(folioMap
												.get(fnrv)) != null) {
											thumbsFileMap.get(
													folioMap.get(fnrv))
													.setOtherVersions(true);
											isFolioExist = true;
										}

										break;
									} else {
										folioMap.put(fnrv,
												uploadFile.getUploadFileId());
										UploadCollectionFolioJson allCollectionsFolio = new UploadCollectionFolioJson();
										allCollectionsFolio.toJson(folio);
										folios.add(allCollectionsFolio);
									}

								}
							}

						}

						if ((!isFolioExist && !folios.isEmpty())) {
							fileJson = new ThumbsFileJson();
							fileJson.setFolios(folios);
							fileJson.setOtherVersions(false);
							fileJson.setUploadFileId(uploadFile
									.getUploadFileId());
							fileJson.setFileName(uploadFile.getFilename());
							fileJson.setOwner(uploadFile.getUploadInfoEntity()
									.getOwner());
							fileJson.setCollation(false);
							fileJson.setCaseStudies(false);
							fileJson.setPrivacy(uploadFile.getFilePrivacy());

							String insName = null;
							if (upload.getInsertEntity() != null) {
								insName = upload.getInsertEntity()
										.getInsertName();
							}
							String realPath = org.medici.mia.common.util.FileUtils
									.getRealPathJSONResponse(
											upload.getRepositoryEntity()
													.getLocation(),
											upload.getRepositoryEntity()
													.getRepositoryAbbreviation(),
											upload.getCollectionEntity()
													.getCollectionAbbreviation(),
											upload.getVolumeEntity()
													.getVolume(), insName);
							fileJson.setFilePath(realPath
									+ FileUtils.THUMB_FILES_DIR
									+ FileUtils.OS_SLASH
									+ uploadFile.getFilename() + "&WID=250");
							thumbsFileMap.put(uploadFile.getUploadFileId(),
									fileJson);

						}

					}

				}

			}

			resp.setThumbsFiles(new ArrayList<ThumbsFileJson>(thumbsFileMap
					.values()));
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Received All" + thumbsFileMap.size()
					+ " collections for Volume.");

		} catch (Exception th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	private UploadCollectionJson getUploadCollectionsInsert(
			List<UploadInfoEntity> uploads) throws ApplicationThrowable {

		UploadCollectionJson resp = new UploadCollectionJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Unknown errore.");

		try {

			if (uploads == null || uploads.isEmpty()) {
				resp.setMessage("No upload found in DB.");
				resp.setStatus(StatusType.w.toString());
				return resp;
			}

			Map<Integer, ThumbsFileJson> thumbsFileMap = new HashMap<Integer, ThumbsFileJson>();
			Map<Folio, Integer> folioMap = new HashMap<Folio, Integer>();
			ThumbsFileJson fileJson = null;

			for (UploadInfoEntity upload : uploads) {

				List<UploadFileEntity> uploadFiles = upload
						.getUploadFileEntities();

				if (uploadFiles != null && !uploadFiles.isEmpty()) {

					for (UploadFileEntity uploadFile : uploadFiles) {

						boolean isFolioExist = false;
						List<UploadCollectionFolioJson> folios = new ArrayList<UploadCollectionFolioJson>();

						// Set Folios
						if (uploadFile.getFolioEntities() != null
								&& !uploadFile.getFolioEntities().isEmpty()) {

							for (FolioEntity folio : uploadFile
									.getFolioEntities()) {

								if (folio.getFolioNumber() == null
										&& folio.getRectoverso() == null) {
									isFolioExist = true;
									break;
								} else {
									Folio fnrv = new Folio(
											folio.getFolioNumber(),
											folio.getRectoverso(),
											folio.getNoNumb());
									if (folioMap.containsKey(fnrv)) {
										if (thumbsFileMap.get(folioMap
												.get(fnrv)) != null) {
											thumbsFileMap.get(
													folioMap.get(fnrv))
													.setOtherVersions(true);
											isFolioExist = true;
										}
										break;
									} else {
										folioMap.put(fnrv,
												uploadFile.getUploadFileId());
										UploadCollectionFolioJson allCollectionsFolio = new UploadCollectionFolioJson();
										allCollectionsFolio.toJson(folio);
										folios.add(allCollectionsFolio);
									}
								}
							}
						}

						if (!isFolioExist && !folios.isEmpty()) {
							fileJson = new ThumbsFileJson();
							fileJson.setFolios(folios);
							fileJson.setOtherVersions(false);
							fileJson.setUploadFileId(uploadFile
									.getUploadFileId());
							fileJson.setFileName(uploadFile.getFilename());
							fileJson.setOwner(uploadFile.getUploadInfoEntity()
									.getOwner());
							fileJson.setCollation(false);
							fileJson.setCaseStudies(false);
							fileJson.setPrivacy(uploadFile.getFilePrivacy());

							String insName = null;
							if (upload.getInsertEntity() != null) {
								insName = upload.getInsertEntity()
										.getInsertName();
							}
							String realPath = org.medici.mia.common.util.FileUtils
									.getRealPathJSONResponse(
											upload.getRepositoryEntity()
													.getLocation(),
											upload.getRepositoryEntity()
													.getRepositoryAbbreviation(),
											upload.getCollectionEntity()
													.getCollectionAbbreviation(),
											upload.getVolumeEntity()
													.getVolume(), insName);
							fileJson.setFilePath(realPath
									+ FileUtils.THUMB_FILES_DIR
									+ FileUtils.OS_SLASH
									+ uploadFile.getFilename() + "&WID=250");
							thumbsFileMap.put(uploadFile.getUploadFileId(),
									fileJson);

						}

					}

				}

			}

			resp.setThumbsFiles(new ArrayList<ThumbsFileJson>(thumbsFileMap
					.values()));
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Received All" + thumbsFileMap.size()
					+ " collections for Insert.");

		} catch (Exception th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}
	
	@Override
	public List<FolioJson> findFoliosByDocument(Integer documentEntityId) {
		if (documentEntityId == null) {
			return null;
		}
		
		MiaDocumentEntity documentEntity = 
				getGenericDocumentDAO().find(documentEntityId);
		
		if (documentEntity == null) {
			return null;
		}
		
		Set<FolioJson> foliosJsonSet = new HashSet<>();
		documentEntity.getUploadFileEntities().stream()
		.forEach((uploadFile) -> {
			uploadFile.getFolioEntities().forEach((folio) -> {
				foliosJsonSet.add(createFolioJson(folio));
			});
		});
		
		return new ArrayList<FolioJson>(foliosJsonSet);
	}
	
	@Override
	public FolioJson addFolioToNoImageDocument(
			FolioJson folioJson,
			Integer documentEntityId) {
		
		MiaDocumentEntity miaDocumentEntity = 
				getGenericDocumentDAO().find(documentEntityId);
		
		if (miaDocumentEntity.getUploadFileEntities().isEmpty()) {
			return null;
		}
		
		if (folioAlreadyExists(folioJson, documentEntityId)) {
			return null;
		}
	
		return addFolioToDocument(folioJson, miaDocumentEntity);
	}
	
	@Override
	public Integer removeFolioFromNoImageDocument(
			Integer folioId, 
			Integer documentEntityId) {
		
		FolioEntity folioEntity = getFolioDAO().findById(folioId);
		MiaDocumentEntity miaDocumentEntity = getGenericDocumentDAO().
				find(documentEntityId);
		
		if (folioEntity == null || miaDocumentEntity == null) {
			return null;
		}
		
		UploadFileEntity uploadFileEntity = getUploadFileDAO().find(
				folioEntity.getUploadedFileId());
		
		if (uploadFileEntity == null) {
			return null;
		}
		
		boolean folioIsRemoved = removeFolioFromDocument(
				folioEntity, 
				miaDocumentEntity);
		
		return folioIsRemoved ? folioEntity.getFolioId() : null;
	}
	
	private FolioJson createFolioJson(FolioEntity folioEntity) {
		FolioJson folioJson = new FolioJson();
		UploadFileEntity uploadFileEntity = folioEntity.getUploadFileEntity();
		
		folioJson.setFolioId(folioEntity.getFolioId());
		folioJson.setFolioNumber(folioEntity.getFolioNumber());
		folioJson.setRectoverso(folioEntity.getRectoverso());
		folioJson.setNoNumb(folioEntity.getNoNumb());
		folioJson.setUploadFileId(uploadFileEntity.getUploadFileId());
		folioJson.setImgName(uploadFileEntity.getFilename());
		
		return folioJson;
	}
	
	private boolean folioAlreadyExists(FolioJson folioJson, Integer documentEntityId) {
		List<FolioJson> foliosJson = findFoliosByDocument(documentEntityId);
		for (int index = 0; index < foliosJson.size(); index++) {
			if (areSameFolio(folioJson, foliosJson.get(index))) {
				return true;
			}
		}
		
		return false;
	}
	
	private boolean areSameFolio(FolioJson folioA, FolioJson folioB) {
		return ((!folioA.getNoNumb() && !folioB.getNoNumb()) &&
		(folioA.getFolioNumber().equalsIgnoreCase(folioB.getFolioNumber())) && 
		(folioA.getRectoverso().equalsIgnoreCase(folioB.getRectoverso())));
	}
	
	private FolioJson addFolioToDocument(
			FolioJson inputFolio, 
			MiaDocumentEntity miaDocumentEntity) {
		
		FolioJson folioJson = new FolioJson();
		folioJson.setFolioNumber(inputFolio.getFolioNumber());
		folioJson.setRectoverso(inputFolio.getRectoverso());
		folioJson.setNoNumb(inputFolio.getNoNumb());
		folioJson.setImgName(inputFolio.getImgName());
		folioJson.setCanView(inputFolio.getCanView());
		
		// Connects Folio to UploadFile
		UploadFileEntity uploadFileEntity = createFileUpload(miaDocumentEntity);
		folioJson.setUploadFileId(uploadFileEntity.getUploadFileId());
		
		// Connects UploadFile to Document
		miaDocumentEntity.getUploadFileEntities().add(uploadFileEntity);
		
		updateDocumentHistory(miaDocumentEntity);
		
		Integer folioId = getFolioDAO().insertFolio(folioJson);
		folioJson.setFolioId(folioId);
		
		return folioJson;
	}
	
	private UploadFileEntity createFileUpload(
			MiaDocumentEntity miaDocumentEntity) {
		
		Integer uploadId = miaDocumentEntity.getUploadFileEntities().get(0).
				getIdTblUpload();
		
		return getUploadFileDAO().createNoImageUploadFile(uploadId);
	}
	
	private void updateDocumentHistory(MiaDocumentEntity miaDocumentEntity) {
		User user = getUserDAO().findUser(
				((UserDetails) SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()).getUsername());
		String owner = user.getAccount();
		// Modifies last user update
		getGenericDocumentDAO().updateDE(miaDocumentEntity, owner, new Date());
	}
	
	private boolean removeFolioFromDocument(
			FolioEntity folioEntity,
			MiaDocumentEntity miaDocumentEntity) {
		
		List<UploadFileEntity> uploadFileEntities = 
				miaDocumentEntity.getUploadFileEntities();
		boolean folioIsRemoved = false;
		
		// At least a folio must be present 
		if (uploadFileEntities.size() <= 1) {
			return folioIsRemoved;
		}
		
		for (int index = 0; index < uploadFileEntities.size(); index++) {
			
			if (uploadFileEntities.get(index).getUploadFileId().equals(
					folioEntity.getUploadedFileId())) {
				uploadFileEntities.remove(index);
				folioIsRemoved = true;
				break;
			}
		}
		
		if (folioIsRemoved) {
			updateDocumentHistory(miaDocumentEntity);
		}
		
		return folioIsRemoved;
	}

	public UploadInfoDAO getUploadInfoDAO() {
		return uploadInfoDAO;
	}

	public void setUploadInfoDAO(UploadInfoDAO uploadInfoDAO) {
		this.uploadInfoDAO = uploadInfoDAO;
	}

	public VolumeDAO getVolumeDAO() {
		return volumeDAO;
	}

	public void setVolumeDAO(VolumeDAO volumeDAO) {
		this.volumeDAO = volumeDAO;
	}

	public CollectionDAO getCollectionDAO() {
		return collectionDAO;
	}

	public void setCollectionDAO(CollectionDAO collectionDAO) {
		this.collectionDAO = collectionDAO;
	}

	public InsertDAO getInsertDAO() {
		return insertDAO;
	}

	public void setInsertDAO(InsertDAO insertDAO) {
		this.insertDAO = insertDAO;
	}

	public GenericDocumentDao getGenericDocumentDAO() {
		return genericDocumentDAO;
	}

	public void setGenericDocumentDAO(GenericDocumentDao genericDocumentDAO) {
		this.genericDocumentDAO = genericDocumentDAO;
	}

	public UploadFileDAO getUploadFileDAO() {
		return uploadFileDAO;
	}

	public void setUploadFileDAO(UploadFileDAO uploadFileDAO) {
		this.uploadFileDAO = uploadFileDAO;
	}

	public FolioDao getFolioDAO() {
		return folioDAO;
	}

	public void setFolioDAO(FolioDao folioDAO) {
		this.folioDAO = folioDAO;
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

}