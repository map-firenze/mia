package org.medici.mia.service.folio;

public enum UploadCollectionType {

	VOLUME, INSERT;

}
