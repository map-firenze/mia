package org.medici.mia.service.folio;

import java.io.Serializable;
import java.util.List;

import org.medici.mia.common.json.GenericResponseJson;


/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class UploadCollectionJson extends GenericResponseJson implements
		Serializable {

	private static final long serialVersionUID = 1L;

	private List<ThumbsFileJson> thumbsFiles;
	private Integer colectionId;
	private Integer repositoryId;

	public List<ThumbsFileJson> getThumbsFiles() {
		return thumbsFiles;
	}

	public void setThumbsFiles(List<ThumbsFileJson> thumbsFiles) {
		this.thumbsFiles = thumbsFiles;
	}

	public Integer getColectionId() {
		return colectionId;
	}

	public void setColectionId(Integer colectionId) {
		this.colectionId = colectionId;
	}

	public Integer getRepositoryId() {
		return repositoryId;
	}

	public void setRepositoryId(Integer repositoryId) {
		this.repositoryId = repositoryId;
	}

}
