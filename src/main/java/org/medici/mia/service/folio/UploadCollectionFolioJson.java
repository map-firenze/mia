package org.medici.mia.service.folio;

import java.io.Serializable;

import org.medici.mia.domain.FolioEntity;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class UploadCollectionFolioJson implements Serializable {

	private static final long serialVersionUID = 1199199301715371481L;

	private Integer folioId;
	private String folioNumber;
	private String rectoverso;

	public Integer getFolioId() {
		return folioId;
	}

	public void setFolioId(Integer folioId) {
		this.folioId = folioId;
	}

	public String getFolioNumber() {
		return folioNumber;
	}

	public void setFolioNumber(String folioNumber) {
		this.folioNumber = folioNumber;
	}

	public String getRectoverso() {
		return rectoverso;
	}

	public void setRectoverso(String rectoverso) {
		this.rectoverso = rectoverso;
	}

	public UploadCollectionFolioJson toJson(FolioEntity folio) {
		this.setFolioId(folio.getFolioId());
		this.setFolioNumber(folio.getFolioNumber());
		this.setRectoverso(folio.getRectoverso());

		return this;
	}

}
