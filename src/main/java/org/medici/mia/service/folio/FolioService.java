package org.medici.mia.service.folio;

import java.util.List;

import org.medici.mia.common.json.folio.FolioJson;
import org.medici.mia.exception.ApplicationThrowable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public interface FolioService {

	public UploadCollectionJson getUploads(Integer id, UploadCollectionType type)
			throws ApplicationThrowable;

	public List<FolioJson> findFoliosByDocument(Integer documentEntityId);
	
	public FolioJson addFolioToNoImageDocument(
			FolioJson folioJson,
			Integer documentEntityId);
	
	public Integer removeFolioFromNoImageDocument(
			Integer folioId, 
			Integer documentEntityId);
}
