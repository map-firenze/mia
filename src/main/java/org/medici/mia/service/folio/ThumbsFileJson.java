package org.medici.mia.service.folio;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class ThumbsFileJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private String filePath;
	private Integer uploadFileId;
	private Integer privacy;
	private List<UploadCollectionFolioJson> folios;
	private Integer folioRectoVerso;
	private String fileName;
	private String owner;
	private Boolean collation;
	private Boolean caseStudies;
	private Boolean otherVersions;

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Integer getUploadFileId() {
		return uploadFileId;
	}

	public void setUploadFileId(Integer uploadFileId) {
		this.uploadFileId = uploadFileId;
	}

	public Integer getPrivacy() {
		return privacy;
	}

	public void setPrivacy(Integer privacy) {
		this.privacy = privacy;
	}

	public List<UploadCollectionFolioJson> getFolios() {
		return folios;
	}

	public void setFolios(List<UploadCollectionFolioJson> folios) {
		this.folios = folios;
	}

	public Integer getFolioRectoVerso() {
		return folioRectoVerso;
	}

	public void setFolioRectoVerso(Integer folioRectoVerso) {
		this.folioRectoVerso = folioRectoVerso;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Boolean getCollation() {
		return collation;
	}

	public void setCollation(Boolean collation) {
		this.collation = collation;
	}

	public Boolean getCaseStudies() {
		return caseStudies;
	}

	public void setCaseStudies(Boolean caseStudies) {
		this.caseStudies = caseStudies;
	}

	public Boolean getOtherVersions() {
		return otherVersions;
	}

	public void setOtherVersions(Boolean otherVersions) {
		this.otherVersions = otherVersions;
	}

	// otherVersions means if volume is used in other files the value should be
	// true.

	// public AllCollectionsThumbsFileJson toJson(UploadFileEntity entity) {
	//
	// this.set(user.getAccount());
	// this.setFirstName(user.getFirstName());
	// this.setLastName(user.getLastName());
	// this.setOrganization(user.getOrganization());
	// this.setEmail(user.getMail());
	// this.setCity(user.getCity());
	// this.setCountry(user.getCountry());
	//
	// return this;
	// }
	//
	//

}
