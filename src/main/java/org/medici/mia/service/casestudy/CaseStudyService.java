package org.medici.mia.service.casestudy;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.UserJson;
import org.medici.mia.common.json.UserProfileJson;
import org.medici.mia.common.json.casestudy.*;
import org.medici.mia.domain.*;
import org.medici.mia.exception.ApplicationThrowable;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;

public interface CaseStudyService {
    CaseStudy findCaseStudy(Integer id) throws ApplicationThrowable;
    boolean validateUser(CaseStudy cs);
    boolean validateUserForDelete(Object obj);
    CaseStudyJson createCaseStudy(CreateCaseStudyJson createJson) throws ApplicationThrowable;
    Boolean deleteCaseStudy(CaseStudy caseStudy) throws ApplicationThrowable;
    CaseStudyJson modifyCaseStudy(CaseStudy caseStudy, EditCaseStudyJson modifyJson) throws ApplicationThrowable;
//    CaseStudyJson restoreCaseStudy(CaseStudy caseStudy) throws ApplicationThrowable;
    List<CaseStudyJson> getCaseStudiesByUser(User user) throws ApplicationThrowable;
    CaseStudyJson getCaseStudyById(Integer id) throws ApplicationThrowable;
    List<UserProfileJson> getUserGroup(CaseStudy caseStudy) throws ApplicationThrowable;
    List<UserProfileJson> addUserToGroup(CaseStudy caseStudy, User user) throws ApplicationThrowable;
    List<UserProfileJson> removeUserFromGroup(CaseStudy caseStudy, User user) throws ApplicationThrowable;
    public List<UserProfileJson> updateUserGroup(CaseStudy caseStudy, List<String> accounts) throws ApplicationThrowable;
    List<CaseStudyFolderJson> findFoldersByCaseStudyId(Integer id) throws ApplicationThrowable;
    CaseStudyFolderJson addFolder(CaseStudy caseStudy, CreateCaseStudyFolderJson folderJson) throws ApplicationThrowable;
    CaseStudyFolderJson editFolder(CaseStudyFolder folder, EditCaseStudyJson modifyJson) throws ApplicationThrowable;
    Boolean removeFolder(CaseStudy caseStudy, CaseStudyFolder folder) throws ApplicationThrowable;
    List<CaseStudyItemJson> findItemsByFolderId(Integer id) throws ApplicationThrowable;
    CaseStudyItemJson addItem(CaseStudyFolder folder, CreateCaseStudyItemJson itemJson) throws ApplicationThrowable;
    CaseStudyItemJson editItem (CaseStudyItem item, EditCaseStudyItemJson modifyJson) throws ApplicationThrowable;
    Boolean removeItem (CaseStudyFolder folder, CaseStudyItem item) throws ApplicationThrowable;
    CaseStudy findByFolderId(Integer folderId) throws ApplicationThrowable;
    CaseStudy findByItemId(Integer itemId) throws ApplicationThrowable;
    List<CaseStudyBAALJson> findByDocuments(List<MiaDocumentEntity> documents) throws ApplicationThrowable;
    CaseStudyItemJson uploadFile(String title, MultipartFile file, CaseStudyFolder folder) throws ApplicationThrowable;
    GenericResponseDataJson<List<CaseStudyFolderJson>> reorderEntities(CaseStudy caseStudy, ReorderCSJson reorderJson) throws ApplicationThrowable;
    void deleteAttachedItems(Integer id, CaseStudyItem.EntityType type) throws ApplicationThrowable;
    List<CaseStudyJson> findAttachedCS(Integer id, CaseStudyItem.EntityType type) throws ApplicationThrowable;
    Boolean isDocumentSharedWithMe(Integer documentId) throws ApplicationThrowable;
}
