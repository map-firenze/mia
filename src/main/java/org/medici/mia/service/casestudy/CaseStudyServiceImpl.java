package org.medici.mia.service.casestudy;

import com.github.underscore.BiFunction;
import com.github.underscore.Function;
import com.github.underscore.Predicate;
import com.github.underscore.U;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.UserJson;
import org.medici.mia.common.json.UserProfileJson;
import org.medici.mia.common.json.biographical.BiographicalPeopleJson;
import org.medici.mia.common.json.casestudy.*;
import org.medici.mia.common.json.geographical.GeographicalPlaceJson;
import org.medici.mia.common.json.message.MessageSendJson;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.dao.casestudy.CaseStudyDAO;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.dao.people.BiographicalPeopleDAO;
import org.medici.mia.dao.place.PlaceDAO;
import org.medici.mia.dao.userrole.UserRoleDAO;
import org.medici.mia.domain.*;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.message.MessageService;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.PersistenceException;
import java.io.*;
import java.util.*;

@Service
public class CaseStudyServiceImpl implements CaseStudyService {

    @Autowired
    private UserService userService;

    @Autowired
    private CaseStudyDAO caseStudyDAO;

    @Autowired
    private GenericDocumentDao documentDAO;

    @Autowired
    private BiographicalPeopleDAO peopleDAO;

    @Autowired
    private PlaceDAO placeDAO;

    @Autowired
    private MiaDocumentService miaDocumentService;

    @Autowired
    private UserRoleDAO userRoleDAO;
    
    @Autowired
    private MessageService messageService;

    @Override
    public CaseStudy findCaseStudy(Integer id) throws ApplicationThrowable {
        return caseStudyDAO.find(id);
    }

    @Override
    public boolean validateUser(CaseStudy cs)
    {
        User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        final String account = user.getAccount();
        List<UserRole> userRoles = userRoleDAO.findUserRoles(account);
        int i = 0;
        Boolean isAutorized = Boolean.FALSE;
        while (i < userRoles.size() && !isAutorized) {
            if (userRoles.get(i).containsAuthority(UserAuthority.Authority.ADMINISTRATORS)) {
                isAutorized = Boolean.TRUE;
            }
            i++;
        }
        return isAutorized || cs.getUserGroup() != null && U.any(cs.getUserGroup(), new Predicate<User>() {
            @Override
            public boolean test(User u) {
                return u.getAccount().equals(account);
            }
        }) || cs.getCreatedBy().getAccount().equals(account);
    }

    @Override
    public boolean validateUserForDelete(Object obj)
    {
        String createdBy = null;
        String createdByCS = null;
        if(obj instanceof CaseStudy){
            createdBy = ((CaseStudy) obj).getCreatedBy().getAccount();
            createdByCS = ((CaseStudy) obj).getCreatedBy().getAccount();
        } else if(obj instanceof CaseStudyFolder){
            createdBy = ((CaseStudyFolder) obj).getCreatedBy().getAccount();
            createdByCS = ((CaseStudyFolder) obj).getCaseStudy().getCreatedBy().getAccount();
        } else if (obj instanceof CaseStudyItem){
            createdBy = ((CaseStudyItem) obj).getCreatedBy().getAccount();
            createdByCS = ((CaseStudyItem) obj).getFolder().getCaseStudy().getCreatedBy().getAccount();
        } else {
            return false;
        }
        User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        final String account = user.getAccount();
        List<UserRole> userRoles = userRoleDAO.findUserRoles(account);
        int i = 0;
        Boolean isAutorized = Boolean.FALSE;
        while (i < userRoles.size() && !isAutorized) {
            if (userRoles.get(i).containsAuthority(UserAuthority.Authority.ADMINISTRATORS)) {
                isAutorized = Boolean.TRUE;
            }
            i++;
        }
        return isAutorized || account.equals(createdBy) || account.equals(createdByCS);
    }

    @Transactional
    @Override
    public CaseStudyJson createCaseStudy(CreateCaseStudyJson createJson) throws ApplicationThrowable {
        User createdBy = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        CaseStudy caseStudy = createJson.toEntity(createdBy);
        caseStudyDAO.persist(caseStudy);
        caseStudy.getUserGroup().add(createdBy);
        return CaseStudyJson.toJson(caseStudy);
    }

    @Transactional
    @Override
    public Boolean deleteCaseStudy(CaseStudy caseStudy) throws ApplicationThrowable {
        try {
            caseStudyDAO.removeCaseStudy(caseStudy);
        } catch (PersistenceException ex){
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public CaseStudyJson modifyCaseStudy(CaseStudy caseStudy, EditCaseStudyJson modifyJson) throws ApplicationThrowable {
        caseStudy.setTitle(modifyJson.getTitle());
        caseStudy.setDescription(modifyJson.getDescription());
        if(modifyJson.getNotes() != null){
            caseStudy.setNotes(modifyJson.getNotes());
        }
        caseStudyDAO.merge(caseStudy);
        return CaseStudyJson.toJson(caseStudy);
    }

    @Override
    public List<CaseStudyJson> getCaseStudiesByUser(User user) throws ApplicationThrowable {
        List<CaseStudy> caseStudies = caseStudyDAO.findStudiesByUser(user);
        if(caseStudies == null){
            return new ArrayList<CaseStudyJson>();
        }
        return U.map(caseStudies, new Function<CaseStudy, CaseStudyJson>() {
            @Override
            public CaseStudyJson apply(CaseStudy caseStudy) {
                return CaseStudyJson.toJson(caseStudy);
            }
        });
    }

    @Override
    public CaseStudyJson getCaseStudyById(Integer id) throws ApplicationThrowable {
        CaseStudy caseStudy = caseStudyDAO.find(id);
        if(caseStudy == null) return null;
        return CaseStudyJson.toJson(caseStudy);
    }

    @Override
    public List<UserProfileJson> getUserGroup(CaseStudy caseStudy) throws ApplicationThrowable {
        return U.map(caseStudy.getUserGroup(), new Function<User, UserProfileJson>() {
            @Override
            public UserProfileJson apply(User user) {
                return new UserProfileJson().toJson(user);
            }
        });
    }

    @Transactional
    @Override
    public List<UserProfileJson> addUserToGroup(CaseStudy caseStudy, User user) throws ApplicationThrowable {
        caseStudy.getUserGroup().add(user);
        caseStudyDAO.merge(caseStudy);
        return U.map(caseStudy.getUserGroup(), new Function<User, UserProfileJson>() {
            @Override
            public UserProfileJson apply(User user) {
                return new UserProfileJson().toJson(user);
            }
        });
    }

    @Transactional
    @Override
    public List<UserProfileJson> removeUserFromGroup(CaseStudy caseStudy, User user) throws ApplicationThrowable {
        caseStudy.getUserGroup().remove(user);
        caseStudyDAO.merge(caseStudy);
        return U.map(caseStudy.getUserGroup(), new Function<User, UserProfileJson>() {
            @Override
            public UserProfileJson apply(User user) {
                return new UserProfileJson().toJson(user);
            }
        });
    }

    @Transactional
    @Override
    public List<UserProfileJson> updateUserGroup(CaseStudy caseStudy, List<String> accounts) throws ApplicationThrowable {
        List<String> userGroup = U.reduce(caseStudy.getUserGroup(), new BiFunction<List<String>, User, List<String>>() {
            @Override
            public List<String> apply(List<String> strings, User user) {
                strings.add(user.getAccount());
                return strings;
            }
        }, new ArrayList<String>());
        accounts = U.uniq(accounts);
        accounts.remove(caseStudy.getCreatedBy().getAccount());
        userGroup.remove(caseStudy.getCreatedBy().getAccount());
        List<String> accountsToAdd = U.difference(accounts, userGroup);
        userGroup.removeAll(accounts);
        List<String> accountsToDelete = userGroup;
        List<User> usersToDelete = new ArrayList<User>();
        List<User> usersToAdd = new ArrayList<User>();
		final String username = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername()).getAccount();
        for(String account: accountsToAdd){
            usersToAdd.add(userService.findUser(account));
            User userToSendMessage = userService.findUser(account);
            //Send message to confirm join group study
            sendMessageJoinGroupCaseStudy(caseStudy.getTitle(), username, userToSendMessage);
            
        }
        for(String account: accountsToDelete){
            usersToDelete.add(userService.findUser(account));
        }
        caseStudy.getUserGroup().removeAll(usersToDelete);
        caseStudy.getUserGroup().addAll(usersToAdd);
        return U.map(caseStudy.getUserGroup(), new Function<User, UserProfileJson>() {
            @Override
            public UserProfileJson apply(User user) {
                return new UserProfileJson().toJson(user);
            }
        });
    }
    
    //Send message join in group study
    private void sendMessageJoinGroupCaseStudy(String caseStudyName, String currentUsername,User user) {
    	 MessageSendJson json = new MessageSendJson();
         json.setAccount(currentUsername);
         json.setFrom(currentUsername);
         json.setTo(user.getAccount());
         json.setMessageSubject("STUDY GROUP PARTICIPATION");
         json.setMessageText("You have been added to a case study.<br>"
         		 + "Look at your case study list for the <b>" + caseStudyName +"</b>");
         messageService.sendNotification(user, json);
    }

    private boolean isEntityExist(Integer id, CaseStudyItem.EntityType type){
        switch (type){
            case DE:
                MiaDocumentEntity doc = documentDAO.findDocumentById(id);
                return !(doc == null || doc.getFlgLogicalDelete() != null && doc.getFlgLogicalDelete());
            case GEO:
                Place place = placeDAO.findPlaceById(id);
                return !(place == null || place.getLogicalDelete() != null && place.getLogicalDelete());
            case BIO:
                BiographicalPeople people = peopleDAO.findPeopleById(id);
                return !(people == null || people.getLogicalDelete() != null && people.getLogicalDelete());
            case LINK:
                return true;
            case FILE:
                return true;
            default:
                return false;
        }
    }

    private Object polymorphAssoc(CaseStudyItem item){
        Object entity = null;
        HashMap<String, String> map = new HashMap<String, String>();
        switch (item.getEntityType()){
            case DE:
                MiaDocumentEntity doc = documentDAO.findDocumentById(item.getEntityId());
                if(doc == null) return null;
                List<MiaDocumentEntity> docs = new ArrayList<MiaDocumentEntity>();
                docs.add(doc);
                entity = miaDocumentService.createDocumentsJsonFromDocEntity(docs).get(0);
                break;
            case GEO:
                Place place = placeDAO.findPlaceById(item.getEntityId());
                if(place == null) return null;
                entity = new GeographicalPlaceJson().toJson(place);
                break;
            case BIO:
                BiographicalPeople people = peopleDAO.findPeopleById(item.getEntityId());
                if(people == null || people.getLogicalDelete() != null && people.getLogicalDelete()) return null;
                entity = new BiographicalPeopleJson().toJson(people);
                break;
            case LINK:
                if(item.getLink() == null) return null;
                map.put("link", item.getLink());
                entity = map;
                break;
            case FILE:
                if(item.getPath() == null) return null;
                File file = new File(item.getPath());
                if(!file.exists()){
                    map.put("path", null);
                }
                map.put("path", "/src/case_study/getItemFile.do?id="+item.getId());
                entity = map;
            default:
                break;
        }
        return entity;
    }

    @Override
    public List<CaseStudyFolderJson> findFoldersByCaseStudyId(Integer id) throws ApplicationThrowable {
        List<CaseStudyFolderJson> jsons = new ArrayList<CaseStudyFolderJson>();
        List<CaseStudyFolder> folders = caseStudyDAO.findFoldersByStudyId(id);
        if(folders == null || folders.isEmpty()){
            return jsons;
        }
        for(CaseStudyFolder folder : folders){
            CaseStudyFolderJson json = CaseStudyFolderJson.toJson(folder);
            List<CaseStudyItemJson> itemJsons = new ArrayList<CaseStudyItemJson>();
            for(CaseStudyItem item : folder.getItems()){
                CaseStudyItemJson itemJson = CaseStudyItemJson.toJson(item, polymorphAssoc(item));
                itemJsons.add(itemJson);
            }
            json.setItems(U.sortBy(itemJsons, new Function<CaseStudyItemJson, Comparable>() {
                @Override
                public Comparable apply(CaseStudyItemJson item) {
                    return item.getOrder();
                }
            }));

            jsons.add(json);
        }
        return jsons;
    }

    @Transactional
    @Override
    public CaseStudyFolderJson addFolder(CaseStudy caseStudy, CreateCaseStudyFolderJson folderJson) throws ApplicationThrowable {
        User createdBy = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        CaseStudyFolder folder = folderJson.toEntity(caseStudy);
        folder.setCreatedBy(createdBy);
        folder.setCreatedAt(new Date());
        caseStudyDAO.saveEntity(folder);
        return CaseStudyFolderJson.toJson(folder);
    }

    @Transactional
    @Override
    public CaseStudyFolderJson editFolder(CaseStudyFolder folder, EditCaseStudyJson modifyJson) throws ApplicationThrowable {
        folder.setTitle(modifyJson.getTitle());
        caseStudyDAO.mergeEntity(folder);
        CaseStudyFolderJson json = CaseStudyFolderJson.toJson(folder);
        List<CaseStudyItemJson> itemJsons = new ArrayList<CaseStudyItemJson>();
        for(CaseStudyItem item : folder.getItems()){
            CaseStudyItemJson itemJson = CaseStudyItemJson.toJson(item, polymorphAssoc(item));
            itemJsons.add(itemJson);
        }
        json.setItems(itemJsons);
        return json;
    }

    @Transactional
    @Override
    public Boolean removeFolder(CaseStudy caseStudy, CaseStudyFolder folder) throws ApplicationThrowable {
        caseStudy.getFolders().remove(folder);
        caseStudyDAO.merge(caseStudy);
        return caseStudyDAO.removeFolder(folder);
    }

    @Override
    public List<CaseStudyItemJson> findItemsByFolderId(Integer id) throws ApplicationThrowable {
        return null;
    }

    @Transactional
    @Override
    public CaseStudyItemJson addItem(CaseStudyFolder folder, CreateCaseStudyItemJson itemJson) throws ApplicationThrowable {
        User createdBy = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        if(!isEntityExist(itemJson.getEntityId(), CaseStudyItem.EntityType.valueOf(itemJson.getEntityType()))){
            return null;
        }
        CaseStudyItem item = itemJson.toEntity(folder);
        item.setCreatedAt(new Date());
        item.setCreatedBy(createdBy);
        caseStudyDAO.saveEntity(item);
        return CaseStudyItemJson.toJson(item, polymorphAssoc(item));
    }

    @Transactional
    @Override
    public CaseStudyItemJson editItem(CaseStudyItem item, EditCaseStudyItemJson modifyJson) throws ApplicationThrowable {
        if(modifyJson.getTitle() != null){
            item.setTitle(modifyJson.getTitle());
        }
        if(modifyJson.getLink() != null){
            item.setLink(modifyJson.getLink());
        }
        caseStudyDAO.mergeEntity(item);
        return CaseStudyItemJson.toJson(item, polymorphAssoc(item));
    }

    @Transactional
    @Override
    public Boolean removeItem(CaseStudyFolder folder, CaseStudyItem item) throws ApplicationThrowable {
        folder.getItems().remove(item);
        caseStudyDAO.removeItem(item);
        return true;
    }

    @Override
    public Boolean isDocumentSharedWithMe(Integer documentId) throws ApplicationThrowable {
        return caseStudyDAO.isDocumentSharedWithMe(documentId);
    }

    @Override
    public CaseStudy findByFolderId(Integer folderId) throws ApplicationThrowable {
        return null;
    }

    @Override
    public CaseStudy findByItemId(Integer itemId) throws ApplicationThrowable {
        return null;
    }

    @Override
    public List<CaseStudyBAALJson> findByDocuments(List<MiaDocumentEntity> documents) throws ApplicationThrowable {
        User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        List<CaseStudy> found = new ArrayList<CaseStudy>();
        final WeakHashMap<Integer, WeakHashMap<String, Integer>> foundFolderIds = new WeakHashMap<Integer, WeakHashMap<String, Integer>>();
        for(MiaDocumentEntity d: documents){
            if(d.getFlgLogicalDelete() != null && d.getFlgLogicalDelete()){
                continue;
            }
            List<CaseStudy> caseStudies = caseStudyDAO.findByDocumentId(d.getDocumentEntityId(), user);

            if(caseStudies != null && !caseStudies.isEmpty()){
                found.addAll(caseStudies);
            }
            List<CaseStudyFolder> folders = caseStudyDAO.findFoldersByDocumentId(d.getDocumentEntityId(), user);
            if(folders != null && !folders.isEmpty()) {
                for (CaseStudyFolder folder : folders) {
                    if(!foundFolderIds.containsKey(folder.getCaseStudy().getId())) {
                        WeakHashMap<String, Integer> ids = new WeakHashMap<String, Integer>();
                        ids.put("folderId", folder.getId());
                        ids.put("documentId", d.getDocumentEntityId());
                        foundFolderIds.put(folder.getCaseStudy().getId(), ids);
                    }
                }
            }
        }
        return U.chain(found).uniq().map(new Function<CaseStudy, CaseStudyBAALJson>() {
            @Override
            public CaseStudyBAALJson apply(CaseStudy caseStudy) {
                return CaseStudyBAALJson.toJson(caseStudy, foundFolderIds.containsKey(caseStudy.getId()) ? foundFolderIds.get(caseStudy.getId()) : null);
            }
        }).value();
    }

    @Transactional
    @Override
    public CaseStudyItemJson uploadFile(String title, MultipartFile file, CaseStudyFolder folder) throws ApplicationThrowable {

            String uploadDirectory = ApplicationPropertyManager
                    .getApplicationProperty("casestudy.upload.path");

            String fileName = RandomStringUtils.random(20, true, true) + "."
                    + FilenameUtils.getExtension(file.getOriginalFilename());

            String path = uploadDirectory
                    + org.medici.mia.common.util.FileUtils.OS_SLASH
                    + fileName;

            File uploadDir = new File(uploadDirectory);
            if (!uploadDir.exists()) {
                try {
                    uploadDir.mkdirs();
                } catch (Exception se) {
                    return null;
                }
            }
            try {
                BufferedOutputStream stream = new BufferedOutputStream(
                        new FileOutputStream(new File(path), false));
                stream.write(file.getBytes());
                stream.close();
            } catch (Exception e){
                throw new ApplicationThrowable(e);
            }

        User createdBy = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        CaseStudyItem item = new CaseStudyItem(title,
                null,
                CaseStudyItem.EntityType.FILE,
                folder, new Date(),
                0,
                null);
        item.setPath(path);
        item.setCreatedBy(createdBy);
        caseStudyDAO.saveEntity(item);
        return CaseStudyItemJson.toJson(item, polymorphAssoc(item));
    }

    @Transactional
    @Override
    public GenericResponseDataJson<List<CaseStudyFolderJson>> reorderEntities(final CaseStudy caseStudy, ReorderCSJson reorderJson) throws ApplicationThrowable{
        GenericResponseDataJson<List<CaseStudyFolderJson>> resp = new GenericResponseDataJson<List<CaseStudyFolderJson>>();
        resp.setStatus("error");
        resp.setData(new ArrayList<CaseStudyFolderJson>());
        if(reorderJson.getType().equals("ITEM")){
            CaseStudyFolder folder = caseStudyDAO.findFolder(reorderJson.getFolderId());
            for(ReorderItemCSJson reorderItem: reorderJson.getEntities()){
                CaseStudyItem item = caseStudyDAO.findItem(reorderItem.getId());
                if(item == null){
                    resp.setMessage("Cannot find Case Study item with id = "+reorderItem.getId());
                    return resp;
                }
                final CaseStudyItem.EntityType type = item.getEntityType();
                final Integer id = item.getEntityId();
                if(!item.getFolder().getId().equals(folder.getId())){
                    if(id != null && U.any(folder.getItems(), new Predicate<CaseStudyItem>() {
                        @Override
                        public boolean test(CaseStudyItem caseStudyItem) {
                            return caseStudyItem.getEntityType().equals(type) && caseStudyItem.getEntityId().equals(id);
                        }
                    })){
                        resp.setMessage("Such entity already exists in folder `"+folder.getTitle()+"`");
                        return resp;
                    }
                    item.getFolder().getItems().remove(item);
                    folder.getItems().add(item);
                }
                item.setFolder(folder);
                item.setOrder(reorderItem.getPosition());
                caseStudyDAO.saveEntity(item);
            }
            caseStudyDAO.saveEntity(folder);
        } else if(reorderJson.getType().equals("FOLDER")){
            for(ReorderItemCSJson reorderItem: reorderJson.getEntities()){
                CaseStudyFolder folder = caseStudyDAO.findFolder(reorderItem.getId());
                if(folder == null){
                    resp.setMessage("Cannot find Case Study folder with id = "+reorderItem.getId());
                    return resp;
                }
                if(!folder.getCaseStudy().getId().equals(caseStudy.getId())){
                    resp.setMessage("Cannot move folders between Case Studies");
                    return resp;
                }
                folder.setOrder(reorderItem.getPosition());
                caseStudyDAO.saveEntity(folder);
            }
        }
        resp.setMessage("Successfully reordered entities");
        resp.setStatus("success");
        resp.setData(findFoldersByCaseStudyId(caseStudy.getId()));
        return resp;
    }



    @Transactional
    @Override
    public void deleteAttachedItems(Integer id, CaseStudyItem.EntityType type){
        List<CaseStudyItem> items = caseStudyDAO.findAttachedItems(id, type);
        for (CaseStudyItem item : items) {
            removeItem(item.getFolder(), item);
        }
    }

    @Override
    public List<CaseStudyJson> findAttachedCS(Integer id, CaseStudyItem.EntityType type) throws ApplicationThrowable {
        List<CaseStudy> caseStudies = caseStudyDAO.findAttachedCS(id, type);
        if(caseStudies == null){
            return null;
        }
        return U.map(caseStudies, new Function<CaseStudy, CaseStudyJson>() {
            @Override
            public CaseStudyJson apply(CaseStudy caseStudy) {
                return CaseStudyJson.toJson(caseStudy);
            }
        });
    }
}
