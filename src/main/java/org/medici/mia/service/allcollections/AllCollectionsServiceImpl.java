package org.medici.mia.service.allcollections;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.github.underscore.*;

import org.apache.log4j.Logger;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.allcollections.AllCollectionsFolioJson;
import org.medici.mia.common.json.allcollections.AllCollectionsThumbsFileJson;
import org.medici.mia.common.json.allcollections.AllCollectionsVolumeJson;
import org.medici.mia.common.json.casestudy.CaseStudyBAALJson;
import org.medici.mia.common.json.casestudy.CaseStudyJson;
import org.medici.mia.common.json.folio.FolioJson;
import org.medici.mia.common.util.FileUtils;
import org.medici.mia.dao.collection.CollectionDAO;
import org.medici.mia.dao.googlelocation.GoogleLocationDAO;
import org.medici.mia.dao.insert.InsertDAO;
import org.medici.mia.dao.miadoc.DocumentFileDao;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.dao.repository.RepositoryDAO;
import org.medici.mia.dao.series.SeriesDAO;
import org.medici.mia.dao.uploadfile.UploadFileDAO;
import org.medici.mia.dao.uploadinfo.UploadInfoDAO;
import org.medici.mia.dao.volume.VolumeDAO;
import org.medici.mia.domain.*;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.casestudy.CaseStudyService;
import org.medici.mia.service.collation.CollationService;
import org.medici.mia.service.upload.UploadService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Service
@Transactional(readOnly = true)
public class AllCollectionsServiceImpl implements AllCollectionsService {

	private final Logger logger = Logger.getLogger(this.getClass());
	public static final String ERRORMSG_INCORRECT_INPUT = "ERROR: The Input value may be null or have no correct value.";
	public static final String WARNINGMSG_UPLOAD_CHANGETO_PUBLIC = "WARNING: The upload belongs to this file changed to public.";
	public static final String WARNINGMSG_PRIVACY_NOTUPDATED = "WARNING: The upload belongs to this file changed to public.";

	@Autowired
	private UploadInfoDAO uploadInfoDAO;

	@Autowired
	private UploadFileDAO uploadFileDAO;

	@Autowired
	private UploadService uploadService;

	@Autowired
	private UserService userService;

	@Autowired
	private RepositoryDAO repositoryDAO;

	@Autowired
	private CollectionDAO collectionDAO;

	@Autowired
	private GoogleLocationDAO googleLocationDAO;

	@Autowired
	private SeriesDAO seriesDAO;

	@Autowired
	private VolumeDAO volumeDAO;

	@Autowired
	private InsertDAO insertDAO;

	@Autowired
	private DocumentFileDao docFileDao;

	@Autowired
	private GenericDocumentDao genericDocumentDao;

	@Autowired
	private CollationService collationService;

	@Autowired
	private CaseStudyService caseStudyService;

	@PersistenceContext
	private EntityManager entityManager;

	private List<AllCollectionsThumbsFileJson> paginateList(List<AllCollectionsThumbsFileJson> thumbFiles, Integer page, Integer perPage) {
		Integer limit = page * perPage;
		Integer offset = limit - perPage;
		if(offset > thumbFiles.size()){
			return thumbFiles.subList(0,0);
		} else if(limit > thumbFiles.size()){
			return thumbFiles.subList(offset, thumbFiles.size());
		} else {
			return thumbFiles.subList(offset, limit);
		}
	}

	private List<AllCollectionsThumbsFileJson> sortThumbs(List<AllCollectionsThumbsFileJson> thumbFiles) {
		final List<String> numParams = Arrays.asList("BIS", "TER", "QUA", "QUI", "SEX", "SEP", "OCT", "NON", "DEC", "UND", "DOD", "TRE", "QTD", "QID", "SED");
		final List<String> nonNumParams = Arrays.asList("SPINE", "COSTOLA", "COPERTA", "GUARDIA");

		@SuppressWarnings("unchecked")
		List<AllCollectionsFolioJson> folios = U.chain(
				U.reduce(thumbFiles, new BiFunction<List<AllCollectionsFolioJson>, AllCollectionsThumbsFileJson, List<AllCollectionsFolioJson>>() {
					@Override
					public List<AllCollectionsFolioJson> apply(List<AllCollectionsFolioJson> memo, AllCollectionsThumbsFileJson thumb) {
						memo.addAll(thumb.getFolios());
						return memo ;
					}
				}, new ArrayList<AllCollectionsFolioJson>()))
				.uniq()
				.sortBy(new Function<AllCollectionsFolioJson, Comparable>() {
					@Override
					public Comparable apply(AllCollectionsFolioJson f) {
						if(f.getRectoverso() == null) {
							return "z";
						}
						return f.getRectoverso();
					}
				})
				.sortBy(new Function<AllCollectionsFolioJson, Comparable>() {
					@Override
					public Comparable apply(AllCollectionsFolioJson f) {
						if(f.getFolioNumber() != null){
							return f.getFolioNumber().toLowerCase();
						}
						return "z";
					}
				})
				.sortBy(new Function<AllCollectionsFolioJson, Comparable>() {
					@Override
					public Comparable apply(AllCollectionsFolioJson f) {
						if(f.getFolioNumber()==null){
							return 0;
						}
						Pattern p = Pattern.compile("^[A-Za-z]+\\s*(\\d*)");
						Matcher m = p.matcher(f.getFolioNumber());
						if(m.find()){
							try {
								return Integer.valueOf(m.group(1));
							}
							catch (Exception e) {
								return 0;
							}
						}
						return 0;
					}
				})
				.sortBy(new Function<AllCollectionsFolioJson, Comparable>() {
					@Override
					public Comparable apply(AllCollectionsFolioJson f) {
						if(f.getFolioNumber()==null){
							return 0;
						}
						Pattern p = Pattern.compile("^\\d+\\s*([A-Za-z]+$)");
						Matcher m = p.matcher(f.getFolioNumber());
						if(m.find()){
							try {
								return U.indexOf(numParams, m.group(1).toUpperCase());
							}
							catch (Exception e) {
								return 0;
							}
						}
						return 0;
					}
				})
				.sortBy(new Function<AllCollectionsFolioJson, Comparable>() {
					@Override
					public Comparable apply(AllCollectionsFolioJson f) {
						if(f.getFolioNumber() != null){
							return f.getFolioNumber().toLowerCase();
						}
						return "z";
					}
				})
				.sortBy(new Function<AllCollectionsFolioJson, Comparable>() {
					@Override
					public Comparable apply(AllCollectionsFolioJson f) {
						if(f.getFolioNumber()==null){
							return 0;
						}
						Pattern p = Pattern.compile("^\\d+");
						Matcher m = p.matcher(f.getFolioNumber());
						if(m.find()) {
							return Integer.parseInt(m.group(0));
						} else {
							return 0;
						}
					}
				})
				.sortBy(new Function<AllCollectionsFolioJson, Comparable>() {
					@Override
					public Comparable apply(AllCollectionsFolioJson f) {
						if(f.getFolioNumber()==null){
							return 0;
						}
						Pattern p = Pattern.compile("(^[A-Za-z]+)\\s*\\d*");
						Matcher m = p.matcher(f.getFolioNumber());
						if(m.find()){
							try {
								return U.indexOf(nonNumParams, m.group(1).toUpperCase());
							}
							catch (Exception e) {
								return 0;
							}
						}
						return 0;
					}
				})
				.value();

		List<AllCollectionsFolioJson> nonNumberedFolios = new ArrayList<AllCollectionsFolioJson>();
		List<AllCollectionsFolioJson> letterNumberedFolios = new ArrayList<AllCollectionsFolioJson>();
		Integer startIndex = 0;

		Pattern p = Pattern.compile("(^[A-Za-z]+)\\s*\\d*");

		for(AllCollectionsFolioJson f : folios) {
			for(String param: nonNumParams) {
				if(f.getFolioNumber()==null){
					continue;
				}
				if (f.getFolioNumber().toUpperCase().contains(param.toUpperCase())) {
					nonNumberedFolios.add(f);
					startIndex++;
				}
			}
		}
		folios.removeAll(nonNumberedFolios);

		List<AllCollectionsFolioJson> collationFolios = new ArrayList<AllCollectionsFolioJson>();

		Pattern collationRegex = Pattern.compile("^.*COLLATION$");

		for(AllCollectionsFolioJson f : folios) {
			if(f.getFolioNumber()==null){
				continue;
			}
			Matcher m = collationRegex.matcher(f.getFolioNumber());
			if(m.find()){
				collationFolios.add(f);
			}
		}

		folios.removeAll(collationFolios);

		for(AllCollectionsFolioJson f : folios) {
			if(f.getFolioNumber() == null){
				continue;
			}
			Matcher m = p.matcher(f.getFolioNumber());
			if(m.find()){
				letterNumberedFolios.add(f);
			}
		}

		folios.removeAll(letterNumberedFolios);

		letterNumberedFolios = U.chain(letterNumberedFolios)
				.sortBy(new Function<AllCollectionsFolioJson, Comparable>() {
					@Override
					public Comparable apply(AllCollectionsFolioJson f) {
						if(f.getFolioNumber()==null){
							return 0;
						}
						Pattern p = Pattern.compile("^[A-Za-z]+\\s*(\\d*)");
						Matcher m = p.matcher(f.getFolioNumber());
						if(m.find()){
							try {
								return Integer.valueOf(m.group(1));
							}
							catch (Exception e) {
								return 0;
							}
						}
						return 0;
					}
				})
				.sortBy(new Function<AllCollectionsFolioJson, Comparable>() {
					@Override
					public Comparable apply(AllCollectionsFolioJson f) {
						if(f.getFolioNumber()==null){
							return "z";
						}
						Pattern p = Pattern.compile("(^[A-Za-z]+)\\s*\\d*");
						Matcher m = p.matcher(f.getFolioNumber());
						if(m.find()){
							return m.group(1);
						}
						return "z";
					}
				})
				.value();

		folios.addAll(0, nonNumberedFolios);
		folios.addAll(startIndex, letterNumberedFolios);

		for(AllCollectionsFolioJson collationFolio: collationFolios){
			final AllCollectionsFolioJson col = collationFolio;
			int pos = U.findIndex(folios, new Predicate<AllCollectionsFolioJson>() {
				@Override
				public boolean test(AllCollectionsFolioJson f) {
					Pattern r = Pattern.compile("(^.*)COLLATION$");
					try {
						Matcher m = r.matcher(col.getFolioNumber());
						if(m.find()) {
							if (m.group(1).trim().equals(f.getFolioNumber())) {
								if(f.getRectoverso() == null && col.getRectoverso() == null) return true;
								else if(f.getRectoverso() != null && col.getRectoverso() != null){
									if(f.getRectoverso().equals(col.getRectoverso())){
										return true;
									}
								} else {
									return false;
								}
							}
						} else {
							return false;
						}
					} catch(Exception ex){
						return false;
					}
					return false;
				}
			});
			if(pos >= 0){
				if(pos + 1 < folios.size())
					folios.add(pos + 1, collationFolio);
				else folios.add(folios.size(), collationFolio);
			} else {
				folios.add(0, collationFolio);
			}
		}

		List<AllCollectionsThumbsFileJson> thumbsSorted = new ArrayList<AllCollectionsThumbsFileJson>();

		for(AllCollectionsFolioJson f: folios) {
			if(f.getFolioNumber() == null){
				continue;
			}
			for(AllCollectionsThumbsFileJson t: thumbFiles) {
				boolean flag = false;
				for(AllCollectionsFolioJson tf: t.getFolios()) {
					if (U.isEqual(f, tf)) {
						flag = true;
					}
				}
				if(flag){
					if(thumbsSorted.size() == 0){
						thumbsSorted.add(t);
					} else if(thumbsSorted.size() > 0) {
						if(!U.isEqual(t, thumbsSorted.get(thumbsSorted.size()-1))) {
							thumbsSorted.add(t);
						}
					}
				}
			}
		}

		return thumbsSorted;

	}


	private AllCollectionsThumbsFileJson serializeUploadFile(UploadFileEntity uploadFile, UploadInfoEntity upload){

		AllCollectionsThumbsFileJson fileJson = new AllCollectionsThumbsFileJson();
		List<AllCollectionsFolioJson> folios = new ArrayList<AllCollectionsFolioJson>();
		for(FolioEntity f: uploadFile.getFolioEntities()){
			AllCollectionsFolioJson folioJson = new AllCollectionsFolioJson();
			folios.add(folioJson.toJson(f));
		}
		fileJson.setFolios(folios);
		fileJson.setOtherVersions(new ArrayList<Object>());
		fileJson.setUploadFileId(uploadFile
				.getUploadFileId());
		fileJson.setFileName(uploadFile.getFilename());
		fileJson.setOwner(uploadFile.getUploadInfoEntity()
				.getOwner());
		if(uploadFile.getMiaDocumentEntities() != null && !uploadFile.getMiaDocumentEntities().isEmpty()) {
			fileJson.setCaseStudies(caseStudyService.findByDocuments(uploadFile.getMiaDocumentEntities()));
		} else {
			fileJson.setCaseStudies(new ArrayList<CaseStudyBAALJson>());
		}
		fileJson.setPrivacy(uploadFile.getFilePrivacy());
		fileJson.setUploadId(uploadFile.getUploadInfoEntity().getUploadInfoId());
		fileJson.setCollations(collationService.findCollationsForUploadFile(uploadFile, upload.getUploadInfoId()));
		if(fileJson.getCollations().size() > 0){
			fileJson.setCollationType((String) fileJson.getCollations().get(0).get("type").toString());
		}

		String insName = null;
		if (uploadFile.getUploadInfoEntity().getInsertEntity() != null) {
			insName = uploadFile.getUploadInfoEntity().getInsertEntity()
					.getInsertName();
		}
		String realPath = org.medici.mia.common.util.FileUtils
				.getRealPathJSONResponse(
						uploadFile.getUploadInfoEntity().getRepositoryEntity()
								.getLocation(),
						uploadFile.getUploadInfoEntity().getRepositoryEntity()
								.getRepositoryAbbreviation(),
						uploadFile.getUploadInfoEntity().getCollectionEntity()
								.getCollectionAbbreviation(),
						uploadFile.getUploadInfoEntity().getVolumeEntity()
								.getVolume(), insName);
		fileJson.setFilePath(realPath
				+ FileUtils.THUMB_FILES_DIR
				+ FileUtils.OS_SLASH
				+ uploadFile.getFilename() + "&WID=250");
		return fileJson;



	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public AllCollectionsVolumeJson getAllCollectionsVolume(Integer volumeId, Integer page, Integer perPage)
			throws ApplicationThrowable {

		AllCollectionsVolumeJson resp = new AllCollectionsVolumeJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Unknown errore.");

		try {

			Volume volume = getVolumeDAO().find(volumeId);

			if (volume == null) {
				resp.setMessage("Volume not found in DB.");
				resp.setStatus(StatusType.w.toString());
				return resp;
			}

			List<UploadInfoEntity> uploads = U.reduce(volume.getUploadInfoEntities(), new BiFunction<List<UploadInfoEntity>, UploadInfoEntity, List<UploadInfoEntity>>() {
				@Override
				public List<UploadInfoEntity> apply(List<UploadInfoEntity> uploadInfoEntities, UploadInfoEntity uploadInfoEntity) {
					if(uploadInfoEntity.getLogicalDelete() != null && uploadInfoEntity.getLogicalDelete() != 1) uploadInfoEntities.add(uploadInfoEntity);
					return uploadInfoEntities;
				}
			}, new ArrayList<UploadInfoEntity>());

			if (uploads == null || uploads.isEmpty()) {
				resp.setMessage("Uploads are not present.");
				resp.setStatus(StatusType.w.toString());
				return resp;
			}

			entityManager.setFlushMode(FlushModeType.COMMIT);

			uploads.get(0).getUploadFileEntities().addAll(collationService.findCollatedFilesInVolume(volumeId));

			Map<Integer, AllCollectionsThumbsFileJson> thumbsFileMap = new HashMap<Integer, AllCollectionsThumbsFileJson>();
			Map<FolioNumRectVers, Integer> folioMap = new HashMap<FolioNumRectVers, Integer>();
			List<AllCollectionsThumbsFileJson> collationDupes = new ArrayList<AllCollectionsThumbsFileJson>();

			for (Iterator<UploadInfoEntity> uploadFileEntityIterator = uploads.iterator(); uploadFileEntityIterator.hasNext();) {
				UploadInfoEntity upload = uploadFileEntityIterator.next();

				thumbsFIleMapExtractor(thumbsFileMap, folioMap, collationDupes, upload);

			}

			if (volume.getCollection() != null) {
				CollectionEntity col = getCollectionDAO().find(volume.getCollection());
				if (col != null) {
					resp.setColectionId(col.getCollectionId());
					resp.setRepositoryId(col.getRepositoryId());
					resp.setCollectionName(col.getCollectionName());
					resp.setInsertNo(null);
					resp.setVolumeNo(volume.getVolume());
					resp.setVolumeId(volume.getSummaryId());
					resp.setInsertId(null);
				}

			}

			if(entityManager.isOpen()) entityManager.clear();

			List<AllCollectionsThumbsFileJson> thumbFiles = new ArrayList<AllCollectionsThumbsFileJson>(thumbsFileMap.values());
			thumbFiles.addAll(collationDupes);
			resp.setCount(thumbFiles.size());
			thumbFiles = this.sortThumbs(thumbFiles);
			if(page != null && perPage != null){
				if(page > 0 && perPage > 0) {
					thumbFiles = this.paginateList(thumbFiles, page, perPage);
				}
			}

			resp.setThumbsFiles(thumbFiles);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Received All " + thumbsFileMap.size()
					+ " collections for Volume.");

		} catch (Exception th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public AllCollectionsVolumeJson getAllCollectionsInsert(Integer insertId, Integer page, Integer perPage)
			throws ApplicationThrowable {

		AllCollectionsVolumeJson resp = new AllCollectionsVolumeJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Unknown errore.");

		try {

			InsertEntity insert = getInsertDAO().find(insertId);

			if (insert == null) {
				resp.setMessage("Insert not found in DB.");
				resp.setStatus(StatusType.w.toString());
				return resp;
			}

			List<UploadInfoEntity> uploads = U.reduce(insert.getUploadInfoEntities(), new BiFunction<List<UploadInfoEntity>, UploadInfoEntity, List<UploadInfoEntity>>() {
				@Override
				public List<UploadInfoEntity> apply(List<UploadInfoEntity> uploadInfoEntities, UploadInfoEntity uploadInfoEntity) {
					if(uploadInfoEntity.getLogicalDelete() != null && uploadInfoEntity.getLogicalDelete() != 1) uploadInfoEntities.add(uploadInfoEntity);
					return uploadInfoEntities;
				}
			}, new ArrayList<UploadInfoEntity>());

			if (uploads == null || uploads.isEmpty()) {
				resp.setMessage("Volume not used in any upload.");
				resp.setStatus(StatusType.w.toString());
				return resp;
			}

			entityManager.setFlushMode(FlushModeType.COMMIT);

			uploads.get(0).getUploadFileEntities().addAll(collationService.findCollatedFilesInInsert(insertId));

			Map<Integer, AllCollectionsThumbsFileJson> thumbsFileMap = new HashMap<Integer, AllCollectionsThumbsFileJson>();
			Map<FolioNumRectVers, Integer> folioMap = new HashMap<FolioNumRectVers, Integer>();
			List<AllCollectionsThumbsFileJson> collationDupes = new ArrayList<AllCollectionsThumbsFileJson>();


			for (Iterator<UploadInfoEntity> uploadFileEntityIterator = uploads.iterator(); uploadFileEntityIterator.hasNext();) {
				UploadInfoEntity upload = uploadFileEntityIterator.next();

				thumbsFIleMapExtractor(thumbsFileMap, folioMap, collationDupes, upload);

			}
			if(entityManager.isOpen()) entityManager.close();

			if (insert.getVolumeEntity() != null && insert.getVolumeEntity().getCollection() != null) {
				setDataToResp(resp, insert);
			}

			List<AllCollectionsThumbsFileJson> thumbFiles = new ArrayList<AllCollectionsThumbsFileJson>(thumbsFileMap.values());
			thumbFiles.addAll(collationDupes);
			resp.setCount(thumbFiles.size());
			thumbFiles = this.sortThumbs(thumbFiles);
			if(page != null && perPage != null){
				if(page > 0 && perPage > 0) {
					thumbFiles = this.paginateList(thumbFiles, page, perPage);
				}
			}

			resp.setThumbsFiles(thumbFiles);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Received All " + thumbsFileMap.size()
					+ " collections for Insert.");

		} catch (Exception th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	private void setDataToResp(AllCollectionsVolumeJson resp, InsertEntity insert) {
		CollectionEntity col = getCollectionDAO().find(insert.getVolumeEntity().getCollection());
		if (col != null) {
			resp.setColectionId(col.getCollectionId());
			resp.setRepositoryId(col.getRepositoryId());
			resp.setCollectionName(col.getCollectionName());
			resp.setInsertNo(insert.getInsertName());
			resp.setVolumeNo(insert.getVolumeEntity().getVolume());
			resp.setVolumeId(insert.getVolumeEntity().getSummaryId());
			resp.setInsertId(insert.getInsertId());
		}
	}

	private void thumbsFIleMapExtractor(Map<Integer, AllCollectionsThumbsFileJson> thumbsFileMap, Map<FolioNumRectVers, Integer> folioMap, List<AllCollectionsThumbsFileJson> collationDupes, UploadInfoEntity upload) {
		AllCollectionsThumbsFileJson fileJson;
		List<UploadFileEntity> uploadFiles = upload.getUploadFileEntities();

		if (uploadFiles != null && !uploadFiles.isEmpty() && upload.getLogicalDelete() != 1) {

			for (UploadFileEntity uploadFile : uploadFiles) {

				List<AllCollectionsFolioJson> folios = getAllCollectionsFolioJsons(thumbsFileMap, folioMap, collationDupes, upload, uploadFile);

				if (!folios.isEmpty()) {
					fileJson = getAllCollectionsThumbsFileJson(upload, uploadFile, folios);
					thumbsFileMap.put(uploadFile.getUploadFileId(), fileJson);

				}

			}

		}
	}

	private List<AllCollectionsFolioJson> getAllCollectionsFolioJsons(Map<Integer, AllCollectionsThumbsFileJson> thumbsFileMap, Map<FolioNumRectVers, Integer> folioMap, List<AllCollectionsThumbsFileJson> collationDupes, UploadInfoEntity upload, UploadFileEntity uploadFile) {
		List<AllCollectionsFolioJson> folios = new ArrayList<AllCollectionsFolioJson>();

		// Set Folios
		if (uploadFile.getLogicalDelete() != 1 && uploadFile.getFolioEntities() != null && !uploadFile.getFolioEntities().isEmpty()) {

			for (FolioEntity folio : uploadFile.getFolioEntities()) {

				if (foliosIsNotContainsNumberedFolio(uploadFile.getFolioEntities()))
					break;

				FolioNumRectVers fnrv = new FolioNumRectVers(
						folio.getFolioNumber(),
						folio.getRectoverso(),
						folio.getNoNumb());
				if (folioMap.containsKey(fnrv)) {
					if (thumbsFileMap.get(folioMap.get(fnrv)) != null) {
						List<Object> upl;
						upl = thumbsFileMap.get(
								folioMap.get(fnrv)).getOtherVersions();
						if (thumbsFileMap.get(folioMap.get(fnrv)).getCollationType() == null
								|| thumbsFileMap.get(folioMap.get(fnrv)).getCollationType().equals("ATTACHMENT")) {
							upl.add(this.serializeUploadFile(uploadFile, upload));
							thumbsFileMap.get(folioMap.get(fnrv)).setOtherVersions(upl);
						} else if (thumbsFileMap.get(folioMap.get(fnrv)).getCollations().size() > 0
								&& thumbsFileMap.get(folioMap.get(fnrv)).getCollationType().equals("LACUNA")) {
							AllCollectionsThumbsFileJson tempUpl =this.serializeUploadFile(uploadFile, upload);
							if (!(Boolean) thumbsFileMap.get(folioMap.get(fnrv)).getCollations().get(0).get("isParent")) {
								upl.add(this.serializeUploadFile(uploadFile, upload));
								thumbsFileMap.get(folioMap.get(fnrv)).setOtherVersions(upl);
							} else if(!(tempUpl.getCollations() == null || tempUpl.getCollations().isEmpty())
									&& tempUpl.getCollationType().equals("LACUNA")
									&& (Boolean) tempUpl.getCollations().get(0).get("isParent")){
								collationDupes.add(tempUpl);
							}
						} else {
							collationDupes.add(this.serializeUploadFile(uploadFile, upload));
						}
					}
				} else {
					folioMap.put(fnrv, uploadFile.getUploadFileId());
					AllCollectionsFolioJson allCollectionsFolio = new AllCollectionsFolioJson();
					allCollectionsFolio.toJson(folio);
					folios.add(allCollectionsFolio);
				}
			}

		}
		return folios;
	}

	private AllCollectionsThumbsFileJson getAllCollectionsThumbsFileJson(UploadInfoEntity upload, UploadFileEntity uploadFile, List<AllCollectionsFolioJson> folios) {
		AllCollectionsThumbsFileJson fileJson;
		fileJson = new AllCollectionsThumbsFileJson();
		fileJson.setFolios(folios);
		fileJson.setOtherVersions(new ArrayList<Object>());
		fileJson.setUploadFileId(uploadFile.getUploadFileId());
		fileJson.setUploadId(uploadFile.getUploadInfoEntity().getUploadInfoId());
		fileJson.setFileName(uploadFile.getFilename());
		fileJson.setOwner(uploadFile.getUploadInfoEntity().getOwner());
		if(uploadFile.getMiaDocumentEntities() != null && !uploadFile.getMiaDocumentEntities().isEmpty()) {
			fileJson.setCaseStudies(caseStudyService.findByDocuments(uploadFile.getMiaDocumentEntities()));
		} else {
			fileJson.setCaseStudies(new ArrayList<CaseStudyBAALJson>());
		}
		fileJson.setPrivacy(uploadFile.getFilePrivacy());
		fileJson.setCollations(collationService.findCollationsForUploadFile(uploadFile, upload.getUploadInfoId()));
		if (fileJson.getCollations().size() > 0) {
			fileJson.setCollationType((String) fileJson.getCollations().get(0).get("type").toString());
		}

		String insName = null;
		if (uploadFile.getUploadInfoEntity().getInsertEntity() != null) {
			insName = uploadFile.getUploadInfoEntity().getInsertEntity().getInsertName();
		}
		String realPath = FileUtils
				.getRealPathJSONResponse(
						uploadFile.getUploadInfoEntity().getRepositoryEntity().getLocation(),
						uploadFile.getUploadInfoEntity().getRepositoryEntity().getRepositoryAbbreviation(),
						uploadFile.getUploadInfoEntity().getCollectionEntity().getCollectionAbbreviation(),
						uploadFile.getUploadInfoEntity().getVolumeEntity().getVolume(), insName);
		fileJson.setFilePath(realPath
				+ FileUtils.THUMB_FILES_DIR
				+ FileUtils.OS_SLASH
				+ uploadFile.getFilename() + "&WID=250");
		return fileJson;
	}

	private boolean foliosIsNotContainsNumberedFolio(List<FolioEntity> folioEntities) {
		if (folioEntities == null || folioEntities.isEmpty())
			return true;
		for(FolioEntity folioEntity : folioEntities) {
			if (folioEntity.getNoNumb() == false)
				return false;
		}
		return true;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public AllCollectionsVolumeJson getAllCollectionsNotNumberedVolume(Integer volumeId, Integer page, Integer perPage)
			throws ApplicationThrowable {

		AllCollectionsVolumeJson resp = new AllCollectionsVolumeJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Unknown errore.");

		try {

			Volume volume = getVolumeDAO().find(volumeId);

			if (volume == null) {
				resp.setMessage("Volume not found in DB.");
				resp.setStatus(StatusType.w.toString());
				return resp;
			}

			List<UploadInfoEntity> uploads = volume.getUploadInfoEntities();

			if (uploads == null || uploads.isEmpty()) {
				resp.setMessage("Uploads are not present.");
				resp.setStatus(StatusType.w.toString());
				return resp;
			}

			List<AllCollectionsThumbsFileJson> thumbFiles = new ArrayList<AllCollectionsThumbsFileJson>();

			for (UploadInfoEntity upload : uploads) {

				List<UploadFileEntity> uploadFiles = upload
						.getUploadFileEntities();

				if (uploadFiles != null && !uploadFiles.isEmpty()) {

					List<UploadFileEntity> uploadFileEntities = U.reduce(upload.getUploadFileEntities(), new BiFunction<List<UploadFileEntity>, UploadFileEntity, List<UploadFileEntity>>() {
						@Override
						public List<UploadFileEntity> apply(List<UploadFileEntity> memo, UploadFileEntity t) {
							if(t.getFolioEntities() == null){
								memo.add(t);
							} else if(t.getFolioEntities().isEmpty()){
								memo.add(t);
							} else if(t.getFolioEntities().get(0).getNoNumb() || t.getFolioEntities().get(0).getFolioNumber() == null){
								memo.add(t);
							}
							return memo;
						}
					}, new ArrayList<UploadFileEntity>());

					for(UploadFileEntity u: uploadFileEntities){
						thumbFiles.add(this.serializeUploadFile(u, upload));
					}

				}

			}

			if (volume.getCollection() != null) {
				CollectionEntity col = getCollectionDAO().find(
						volume.getCollection());
				if (col != null) {
					resp.setColectionId(volume.getCollection());
					resp.setRepositoryId(col.getRepositoryId());
					resp.setCollectionName(col.getCollectionName());
					resp.setVolumeNo(volume.getVolume());
					resp.setVolumeId(volume.getSummaryId());
					resp.setInsertNo(null);
					resp.setInsertId(null);
				}

			}
			resp.setCount(thumbFiles.size());
//			Pagination for thumbfiles

			if(page != null && perPage != null){
				if(page > 0 && perPage > 0) {
					thumbFiles = this.paginateList(thumbFiles, page, perPage);
				}
			}

			resp.setThumbsFiles(thumbFiles);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Received All " + thumbFiles.size()
					+ " collections for Volume.");

		} catch (Exception th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public AllCollectionsVolumeJson getAllCollectionsNotNumberedInsert(Integer insertId, Integer page, Integer perPage)
			throws ApplicationThrowable {

		AllCollectionsVolumeJson resp = new AllCollectionsVolumeJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Unknown errore.");

		try {

			InsertEntity insert = getInsertDAO().find(insertId);

			if (insert == null) {
				resp.setMessage("Insert not found in DB.");
				resp.setStatus(StatusType.w.toString());
				return resp;
			}

			List<UploadInfoEntity> uploads = insert.getUploadInfoEntities();

			if (uploads == null || uploads.isEmpty()) {
				resp.setMessage("Volume not used in any upload.");
				resp.setStatus(StatusType.w.toString());
				return resp;
			}

			List<AllCollectionsThumbsFileJson> thumbFiles = new ArrayList<AllCollectionsThumbsFileJson>();


			for (UploadInfoEntity upload : uploads) {

				List<UploadFileEntity> uploadFiles = upload
						.getUploadFileEntities();

				if (uploadFiles != null && !uploadFiles.isEmpty()) {

					List<UploadFileEntity> uploadFileEntities = U.reduce(upload.getUploadFileEntities(), new BiFunction<List<UploadFileEntity>, UploadFileEntity, List<UploadFileEntity>>() {
						@Override
						public List<UploadFileEntity> apply(List<UploadFileEntity> memo, UploadFileEntity t) {
							if(t.getFolioEntities() == null){
								memo.add(t);
							} else if(t.getFolioEntities().isEmpty()){
								memo.add(t);
							} else if(t.getFolioEntities().get(0).getNoNumb() || t.getFolioEntities().get(0).getFolioNumber() == null){
								memo.add(t);
							}
							return memo;
						}
					}, new ArrayList<UploadFileEntity>());

					for(UploadFileEntity u: uploadFileEntities){
						thumbFiles.add(this.serializeUploadFile(u, upload));
					}

				}

			}

			if (insert.getVolumeEntity() != null && insert.getVolumeEntity().getCollection() != null) {
				setDataToResp(resp, insert);
			}
			resp.setCount(thumbFiles.size());
//			Pagination for the thumbfiles

			if(page != null && perPage != null){
				if(page > 0 && perPage > 0) {
					thumbFiles = this.paginateList(thumbFiles, page, perPage);
				}
			}

			resp.setThumbsFiles(thumbFiles);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Received All " + thumbFiles.size()
					+ " collections for Insert.");

		} catch (Exception th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}


	private List<AllCollectionsThumbsFileJson> constructRange(List<AllCollectionsThumbsFileJson> thumbs, AllCollectionsThumbsFileJson foundThumb) throws ApplicationThrowable {
		List<AllCollectionsThumbsFileJson> found = new ArrayList<AllCollectionsThumbsFileJson>();
		if(foundThumb != null && thumbs.size() > 0){
			int index = thumbs.indexOf(foundThumb);
			int start = index - 2 < 0 ? 0 : index - 2;
			int end = index + 3 > thumbs.size() ? thumbs.size() : index + 3;
			if(start == 0){
				end = start + 5 > thumbs.size() ? thumbs.size(): 5;
			}
			if(end == thumbs.size()){
				start = thumbs.size() - 5 < 0 ? 0 : thumbs.size() - 5;
			}
			if(start >= 0 && end <= thumbs.size() && start <= end){
				found = thumbs.subList(start, end);
			}
		}
		return found;
	}

	private List<AllCollectionsThumbsFileJson> findExactFolios(List<AllCollectionsThumbsFileJson> thumbs, FolioJson q) throws ApplicationThrowable{
		AllCollectionsThumbsFileJson foundThumb = null;
		for(AllCollectionsThumbsFileJson t: thumbs){
			if(foundThumb != null) break;
			for(AllCollectionsFolioJson f: t.getFolios()){
				if(f.getFolioNumber().toLowerCase().equals(q.getFolioNumber())){
					if(f.getRectoverso() == null && q.getRectoverso() == null) {
						foundThumb = t;
					} else if(f.getRectoverso() != null && f.getRectoverso().equals(q.getRectoverso())){
						foundThumb = t;
					}
				}
			}
		}
		return constructRange(thumbs, foundThumb);
	}

	@Override
	public AllCollectionsVolumeJson findFoliosInVolume(Integer volumeId, String q, Integer id, Boolean exact) throws ApplicationThrowable {
		AllCollectionsVolumeJson resp = getAllCollectionsVolume(volumeId, null, null);
		List<AllCollectionsThumbsFileJson> jsons = resp.getThumbsFiles();
		List<AllCollectionsThumbsFileJson> found = new ArrayList<AllCollectionsThumbsFileJson>();
		if(jsons == null){
			return resp;
		}
		if(id != null){
			for(AllCollectionsThumbsFileJson json : jsons){
				if(json.getUploadFileId().equals(id)){
					found = constructRange(jsons, json);
					break;
				}
			}
			resp.setMessage("Found " + found.size() + " files with id = `"+ id +"`");
			resp.setThumbsFiles(found);
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}
		final FolioJson folio = new FolioJson();
		if(q == null){
			resp.setMessage("Found " + resp.getThumbsFiles().size() + " files with empty query string");
			resp.setStatus(StatusType.ok.toString());
		} else {
			if(q.length() == 0){
				resp.setMessage("Found " + resp.getThumbsFiles().size() + " files with empty query string");
				resp.setStatus(StatusType.ok.toString());
				return resp;
			}
			q = q.toLowerCase();
			Pattern p = Pattern.compile("^(.*)\\s(recto|verso)*$");
			Matcher m = p.matcher(q);
			if(m.find()){
				folio.setFolioNumber(m.group(1));
				if(m.groupCount() == 2){
					folio.setRectoverso(m.group(2));
				}
			} else {
				folio.setFolioNumber(q);
			}
		}
		if(exact && q != null) {
			found = findExactFolios(jsons, folio);
		} else if(!exact && q != null) {
			found = U.chain(jsons).filter(new Predicate<AllCollectionsThumbsFileJson>() {
				@Override
				public boolean test(AllCollectionsThumbsFileJson t) {
					if(t.getCollationType() != null && t.getCollations() != null){
						if(t.getCollationType().toUpperCase().equals("LACUNA") && t.getCollations().size() > 0){
							if(t.getCollations().get(0).get("isParent") != null && (Boolean)t.getCollations().get(0).get("isParent")){
								return false;
							}
						}
					}
					for(AllCollectionsFolioJson f : t.getFolios()){
						if(f.getFolioNumber().toLowerCase().contains(folio.getFolioNumber())){
							if(folio.getRectoverso() == null){
								return true;
							} else if (f.getRectoverso() != null && folio.getRectoverso().toLowerCase().equals((f.getRectoverso().toLowerCase()))){
								return true;
							}
						}
					}
					return false;
				}
			}).uniq().value();
			resp.setMessage("Found " + found.size() + " files with query string = `"+ q +"`");
			resp.setThumbsFiles(found);
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}
		resp.setMessage("Found " + found.size() + " files with query string = `"+ q +"`");
		resp.setThumbsFiles(found);
		resp.setStatus(StatusType.ok.toString());
		return resp;
	}

	@Override
	public AllCollectionsVolumeJson findFoliosInInsert(Integer insertId, String q, Integer id, Boolean exact) throws ApplicationThrowable {
		AllCollectionsVolumeJson resp = getAllCollectionsInsert(insertId, null, null);
		List<AllCollectionsThumbsFileJson> jsons = resp.getThumbsFiles();
		List<AllCollectionsThumbsFileJson> found = new ArrayList<AllCollectionsThumbsFileJson>();
		if(jsons == null){
			return resp;
		}
		if(id != null){
			for(AllCollectionsThumbsFileJson json : jsons){
				if(json.getUploadFileId().equals(id)){
					found = constructRange(jsons, json);
					break;
				}
			}
			resp.setMessage("Found " + found.size() + " files with id = `"+ id +"`");
			resp.setThumbsFiles(found);
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}
		final FolioJson folio = new FolioJson();
		if(q == null){
			resp.setMessage("Found " + resp.getThumbsFiles().size() + " files with empty query string");
			resp.setStatus(StatusType.ok.toString());
		} else {
			if(q.length() == 0 || q.trim().length() == 0){
				resp.setMessage("Found " + 0 + " files with empty query string");
				resp.setStatus(StatusType.ok.toString());
				resp.setThumbsFiles(new ArrayList<AllCollectionsThumbsFileJson>());
				return resp;
			}
			q = q.toLowerCase();
			Pattern p = Pattern.compile("^(.*)\\s(recto|verso)*$");
			Matcher m = p.matcher(q);
			if(m.find()){
				folio.setFolioNumber(m.group(1));
				if(m.groupCount() == 2){
					folio.setRectoverso(m.group(2));
				}
			} else {
				folio.setFolioNumber(q);
			}

		}
		if(exact && q != null) {
			found = findExactFolios(jsons, folio);
		} else if(!exact && q != null) {
			found = U.chain(jsons).filter(new Predicate<AllCollectionsThumbsFileJson>() {
				@Override
				public boolean test(AllCollectionsThumbsFileJson t) {
					if(t.getCollationType() != null && t.getCollations() != null){
						if(t.getCollationType().toUpperCase().equals("LACUNA") && t.getCollations().size() > 0){
							if(t.getCollations().get(0).get("isParent") != null && (Boolean)t.getCollations().get(0).get("isParent")){
								return false;
							}
						}
					}
					for(AllCollectionsFolioJson f : t.getFolios()){
						if(f.getFolioNumber().toLowerCase().contains(folio.getFolioNumber())){
							if(folio.getRectoverso() == null){
								return true;
							} else if (f.getRectoverso() != null && folio.getRectoverso().toLowerCase().equals((f.getRectoverso().toLowerCase()))){
								return true;
							}
						}
					}
					return false;
				}
			}).uniq().value();
			resp.setMessage("Found " + found.size() + " files with query string = `"+ q +"`");
			resp.setThumbsFiles(found);
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}
		resp.setMessage("Found " + found.size() + " files with query string = `"+ q +"`");
		resp.setThumbsFiles(found);
		resp.setStatus(StatusType.ok.toString());
		return resp;
	}

	public VolumeDAO getVolumeDAO() {
		return volumeDAO;
	}

	public void setVolumeDAO(VolumeDAO volumeDAO) {
		this.volumeDAO = volumeDAO;
	}

	public CollectionDAO getCollectionDAO() {
		return collectionDAO;
	}

	public void setCollectionDAO(CollectionDAO collectionDAO) {
		this.collectionDAO = collectionDAO;
	}

	public InsertDAO getInsertDAO() {
		return insertDAO;
	}

	public void setInsertDAO(InsertDAO insertDAO) {
		this.insertDAO = insertDAO;
	}

}