package org.medici.mia.service.allcollections;

import org.medici.mia.common.json.allcollections.AllCollectionsVolumeJson;
import org.medici.mia.exception.ApplicationThrowable;

/**
 *
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public interface AllCollectionsService {

	AllCollectionsVolumeJson getAllCollectionsVolume(Integer insertId, Integer page, Integer perPage)
			throws ApplicationThrowable;

	AllCollectionsVolumeJson getAllCollectionsInsert(Integer insertId, Integer page, Integer perPage)
			throws ApplicationThrowable;

	AllCollectionsVolumeJson getAllCollectionsNotNumberedVolume(Integer insertId, Integer page, Integer perPage)
			throws ApplicationThrowable;

	AllCollectionsVolumeJson getAllCollectionsNotNumberedInsert(Integer insertId, Integer page, Integer perPage)
			throws ApplicationThrowable;

	AllCollectionsVolumeJson findFoliosInVolume(Integer volumeId, String q, Integer id, Boolean exact)
		throws ApplicationThrowable;

	AllCollectionsVolumeJson findFoliosInInsert(Integer insertId, String q, Integer id, Boolean exact)
		throws ApplicationThrowable;
}
