package org.medici.mia.service.miadoc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.PlaceBaseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.document.DocumentFactoryJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.document.FindPlaceForDocJson;
import org.medici.mia.common.json.document.ModifyPlaceForDocJson;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.common.util.DocumentPrivacyUtils;
import org.medici.mia.dao.documentfield.DocumentFieldDAO;
import org.medici.mia.dao.miadoc.DocumentProducerDao;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.User;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.geografical.GeographicalPlaceService;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.message.MessageService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Service
class ModifyDocumentPlaceServiceImpl implements ModifyDocumentPlaceService {

	@Autowired
	private GenericDocumentDao documentDAO;

	@Autowired
	private DocumentProducerDao documentProducerDao;

	@Autowired
	private DocumentFieldDAO documentFieldDao;

	@Autowired
	private GenericDocumentDao genericDocumentDao;

	@Autowired
	private UserService userService;

	@Autowired
	private HistoryLogService historyLogService;

	@Autowired
	private GeographicalPlaceService geographicalPlaceService;
	
	@Autowired
	private MessageService messageService;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<FindPlaceForDocJson> findDocumentPlaces(Integer documentId,
			String placeOfOriginName) throws ApplicationThrowable {
		List<FindPlaceForDocJson> places = null;

		try {

			MiaDocumentEntity docEntity = getDocumentDAO().findDocumentById(
					documentId);

			if (docEntity == null)
				return null;

			places = new ArrayList<FindPlaceForDocJson>();
			places.add(getModifyPlaceOfOrigin(docEntity, placeOfOriginName));
			DocumentJson doc = DocumentFactoryJson.getDocument(docEntity
					.getCategory());

			List<FindPlaceForDocJson> findPlaceForDoc = doc
					.findPlaceJson(docEntity);

			if (findPlaceForDoc != null) {
				places.addAll(findPlaceForDoc);
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return places;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson modifyDocumentPlaces(
			ModifyPlaceForDocJson modifyPlace, String placeOfOriginName)
			throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");

		try {

			if (modifyPlace.getType() != null) {
				String placeType = modifyPlace.getType();
				placeType = placeType.replaceAll("\\s+", "");
				modifyPlace.setType(placeType);
			}

			MiaDocumentEntity docEntity = getDocumentDAO().findDocumentById(
					Integer.valueOf(modifyPlace.getDocumentId()));
			if (docEntity == null) {
				resp.setMessage("No document found in DB with documentId "
						+ modifyPlace.getDocumentId());
				resp.setStatus(StatusType.w.toString());
				return resp;
			}

			if (placeOfOriginName != null
					&& placeOfOriginName
							.equalsIgnoreCase(modifyPlace.getType())) {
				getGenericDocumentDao().modifyPlaceOfOrigin(modifyPlace);
			} else {
				DocumentJson doc = DocumentFactoryJson.getDocument(docEntity
						.getCategory());
				docEntity = doc.getModifiedDocumentEnt(docEntity, modifyPlace);
			}
			
			if (docEntity.getErrorMsg() != null
					&& !docEntity.getErrorMsg().isEmpty()) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage(docEntity.getErrorMsg());
				return resp;

			}

			HashMap<String, Object> changes = new HashMap<String, Object>();
			HashMap<String, HashMap> category = new HashMap<String, HashMap>();
			HashMap<String, HashMap> action = new HashMap<String, HashMap>();
			HashMap<String, Object> before = new HashMap<String, Object>();
			HashMap<String, Object> after = new HashMap<String, Object>();
			if(modifyPlace.getIdPlaceToDelete() != null && modifyPlace.getNewPlace() == null){
				changes.put("placeName", geographicalPlaceService
								.findGeographicalPlace(modifyPlace.getIdPlaceToDelete(), true)
								.getPlaceName());

				changes.put("type", modifyPlace.getType());
				action.put("delete", changes);
			} else if(modifyPlace.getIdPlaceToDelete() == null){
				changes.put("placeName", geographicalPlaceService
						.findGeographicalPlace(modifyPlace.getNewPlace().getId(), true)
						.getPlaceName());
				changes.put("type", modifyPlace.getType());
				action.put("add", changes);
			} else {
				if(!modifyPlace.getNewPlace().getId().equals(modifyPlace.getIdPlaceToDelete())) {
					before.put("placeName", geographicalPlaceService
							.findGeographicalPlace(modifyPlace.getIdPlaceToDelete(), true)
							.getPlaceName());
					before.put("type", modifyPlace.getType());
					after.put("placeName", geographicalPlaceService
							.findGeographicalPlace(modifyPlace.getNewPlace().getId(), true)
							.getPlaceName());
					after.put("type", modifyPlace.getType());
					changes.put("before", before);
					changes.put("after", after);
					action.put("edit", changes);
				}
			}
			category.put("documentPlaces", action);

			historyLogService.registerAction(
					Integer.valueOf(modifyPlace.getDocumentId()), HistoryLogEntity.UserAction.EDIT,
					HistoryLogEntity.RecordType.DE, null, category);
			
			// Get the username logged in
			UserDetails currentUser = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();

			if (!currentUser.getUsername().equals(docEntity.getCreatedBy())
					&& !DocumentPrivacyUtils.isUserAdmin(currentUser)
					&& !DocumentPrivacyUtils.isOnsiteFellows(currentUser)) {
				messageService.onDocumentEdited(currentUser.getUsername(), docEntity.getCreatedBy(),
						docEntity.getDocumentEntityId());
			}

			docEntity.setLastUpdateBy(currentUser.getUsername());

			docEntity.setDateLastUpdate(new Date());
			getGenericDocumentDao().merge(docEntity);

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Document Modified.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	private FindPlaceForDocJson getModifyPlaceOfOrigin(
			MiaDocumentEntity docEntity, String placeNameOfAllCateories) {

		FindPlaceForDocJson modifyPlaceJson = new FindPlaceForDocJson();
		List<PlaceBaseJson> basePlaces = null;
		modifyPlaceJson.setType(placeNameOfAllCateories);
		if (docEntity.getPlaceOfOriginEntity() != null) {
			basePlaces = new ArrayList<PlaceBaseJson>();
			PlaceBaseJson placeBase = new PlaceBaseJson();
			placeBase.setUnsure(docEntity.getPlaceOfOriginUnsure());
			placeBase.setId(docEntity.getPlaceOfOriginEntity() == null ? null : docEntity.getPlaceOfOriginEntity().getPlaceAllId());
			placeBase.setPrefFlag(docEntity.getPlaceOfOriginEntity() == null ? null : docEntity.getPlaceOfOriginEntity().getPrefFlag());
			basePlaces.add(placeBase);
		}

		modifyPlaceJson.setPlaces(basePlaces);

		return modifyPlaceJson;
	}

	public GenericDocumentDao getDocumentDAO() {
		return documentDAO;
	}

	public void setDocumentDAO(GenericDocumentDao documentDAO) {
		this.documentDAO = documentDAO;
	}

	public DocumentProducerDao getDocumentProducerDao() {
		return documentProducerDao;
	}

	public void setDocumentProducerDao(DocumentProducerDao documentProducerDao) {
		this.documentProducerDao = documentProducerDao;
	}

	public DocumentFieldDAO getDocumentFieldDao() {
		return documentFieldDao;
	}

	public void setDocumentFieldDao(DocumentFieldDAO documentFieldDao) {
		this.documentFieldDao = documentFieldDao;
	}

	public GenericDocumentDao getGenericDocumentDao() {
		return genericDocumentDao;
	}

	public void setGenericDocumentDao(GenericDocumentDao genericDocumentDao) {
		this.genericDocumentDao = genericDocumentDao;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
