package org.medici.mia.service.miadoc;

import javax.persistence.PersistenceException;

import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.exception.ApplicationThrowable;

public interface ModifyDocumentRelatedDocService {

	public GenericResponseJson modifyRelatedDocsForDoc(Integer documentId,
			Integer newDocId, Integer oldDocId) throws ApplicationThrowable;

	public GenericResponseJson addRelatedDocsForDoc(Integer documentId,
			Integer newDocId) throws ApplicationThrowable;

	public GenericResponseJson deleteRelatedDocsForDoc(Integer documentId,
			Integer docToDeleted) throws PersistenceException;

}