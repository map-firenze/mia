package org.medici.mia.service.miadoc;

import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.dao.miadoc.DocumentRelatedDocsDao;
import org.medici.mia.exception.ApplicationThrowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
class ModifyDocumentRelatedDocServiceImpl implements
		ModifyDocumentRelatedDocService {

	@Autowired
	private DocumentRelatedDocsDao documentRelatedDocsDao;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson modifyRelatedDocsForDoc(Integer documentId,
			Integer newDocId, Integer oldDocId) throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Unknown errore.");

		try {

			getDocumentRelatedDocsDao().modifyRelatedDocsForDoc(documentId,
					newDocId, oldDocId);

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Related Document of document Modified.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson addRelatedDocsForDoc(Integer documentId,
			Integer newDocId) throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Unknown errore.");

		try {

			getDocumentRelatedDocsDao().addRelatedDocsForDoc(documentId,
					newDocId);

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Related Document of document Modified.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson deleteRelatedDocsForDoc(Integer documentId,
			Integer docToBeDeleted) throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Unknown errore.");

		try {

			getDocumentRelatedDocsDao().deleteRelatedDocsForDoc(documentId,
					docToBeDeleted);

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Related Document of document Modified.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	public DocumentRelatedDocsDao getDocumentRelatedDocsDao() {
		return documentRelatedDocsDao;
	}

	public void setDocumentRelatedDocsDao(
			DocumentRelatedDocsDao documentRelatedDocsDao) {
		this.documentRelatedDocsDao = documentRelatedDocsDao;
	}

}
