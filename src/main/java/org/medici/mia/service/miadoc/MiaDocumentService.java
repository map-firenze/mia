package org.medici.mia.service.miadoc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.medici.mia.common.json.CreatePlaceJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.LanguageJson;
import org.medici.mia.common.json.PeopleBaseJson;
import org.medici.mia.common.json.PeopleJson;
import org.medici.mia.common.json.PeopleRelatedToDocumentJson;
import org.medici.mia.common.json.PlaceJson;
import org.medici.mia.common.json.TopicJson;
import org.medici.mia.common.json.complexsearch.PeopleComplexSearchJson;
import org.medici.mia.common.json.document.CreateDEResponseJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.document.DocumentMetaDataJson;
import org.medici.mia.common.json.document.DocumentPeopleJson;
import org.medici.mia.common.json.document.DocumentTranscriptionJson;
import org.medici.mia.common.json.document.ModifyImageJson;
import org.medici.mia.common.json.document.TopicPlaceJson;
import org.medici.mia.common.json.folio.FolioJson;
import org.medici.mia.common.json.folio.FolioResponseJson;
import org.medici.mia.controller.genericsearch.PeopleSearchPaginationParam;
import org.medici.mia.domain.*;
import org.medici.mia.exception.ApplicationThrowable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public interface MiaDocumentService {

	public DocumentJson findDocumentById(Integer documentId)
			throws ApplicationThrowable;

	// public List<DocumentJson> findDocuments(List<Integer> documentId)
	// throws ApplicationThrowable;

	public List<DocumentFieldEntity> findDocumentFields()
			throws ApplicationThrowable;

	public Integer countDocumentsByUploadFileId(Integer uploadFileId) throws ApplicationThrowable;

	public List<DocumentJson> findDocumentsByUploadFileId(Integer uploadFileId)
			throws ApplicationThrowable;

	public List<DocumentJson> findDocumentsByUser(String userName)
			throws ApplicationThrowable;

	public List<DocumentJson> findDocuments(boolean isOwner)
			throws ApplicationThrowable;

	public List<DocumentJson> findDocuments(boolean isOwner, Integer page,
			Integer perPage) throws ApplicationThrowable;

	public List<DocumentJson> findNotDeletedDocuments()
			throws ApplicationThrowable;

	public CreateDEResponseJson createDE(DocumentJson document)
			throws ApplicationThrowable;

	public GenericResponseJson modifyDE(DocumentJson document)
			throws ApplicationThrowable;

	public List<DocumentTypologyEntity> findDocumentTypologies()
			throws ApplicationThrowable;

	public List<String> findDocumentTypologiesByCategory(String category)
			throws ApplicationThrowable;

	public List<PeopleJson> findPeople(String peopleName)
			throws ApplicationThrowable;

	public PeopleJson findPeopleById(Integer peopleId)
			throws ApplicationThrowable;

	public Integer insertPeople(PeopleJson peopleToAdd)
			throws ApplicationThrowable;

	public List<PlaceJson> findPlaces(String placeName)
			throws ApplicationThrowable;

	public Integer insertPlace(CreatePlaceJson placeToAdd)
			throws ApplicationThrowable;

	public List<PlaceType> findPlaceTypes() throws ApplicationThrowable;

	public PlaceJson findPlaceById(Integer placeId) throws ApplicationThrowable;

	public List<TopicJson> findTopics() throws ApplicationThrowable;

	public TopicJson findTopicById(Integer topicId) throws ApplicationThrowable;

	public List<FolioJson> checkFolio(Integer uploadFileId)
			throws ApplicationThrowable;

	public List<FolioResponseJson> saveFolio(List<FolioJson> folioToSave)
			throws ApplicationThrowable;

	public GenericResponseJson saveOrModifyTranscriptions(
			List<DocumentTranscriptionJson> transcriptionsJson)
			throws ApplicationThrowable;

	public GenericResponseJson saveTranscription(String transcription,
			Integer documententId) throws ApplicationThrowable;

	public GenericResponseJson saveSynopsis(String synopsis,
			Integer documententId) throws ApplicationThrowable;

	public List<LanguageJson> findLanguages() throws ApplicationThrowable;

	public MiaDocumentEntity findDocumentEntityById(Integer documentId);

	public GenericResponseJson deleteDE(Integer documentId);

	public PlaceJson findPrinicipalPlaceByPlaceId(Integer placeId)
			throws ApplicationThrowable;

	public DocumentPeopleJson getDocumentsPeople(Integer personId)
			throws ApplicationThrowable;

	public Map<String, Map<String, Integer>> getDocumentsCountPeople(
			Integer personId) throws ApplicationThrowable;

	public List<DocumentJson> createDocumentsJsonFromDocEntity(
			List<MiaDocumentEntity> documentEntities)
			throws ApplicationThrowable;

	public List<PlaceJson> getPrinciplaPlaces(List<PlaceJson> places)
			throws ApplicationThrowable;

	public List<TopicPlaceJson> getPrinciplaTopicPlaces(
			List<TopicPlaceJson> places) throws ApplicationThrowable;

	public List<Integer> findDocumentIdsByPeople(List<String> people)
			throws ApplicationThrowable;

	public List<Integer> findDocumentIdsByPlaces(List<String> places)
			throws ApplicationThrowable;

	public GenericResponseJson modifyImages(ModifyImageJson modifyImageJson)
			throws ApplicationThrowable;

	public Integer findPrinicipalPlaceIdByVariantPlaceId(Integer placeId)
			throws ApplicationThrowable;

	public List<PeopleBaseJson> findPeopleRelatedtoDocument(Integer documentId)
			throws ApplicationThrowable;

	public GenericResponseJson addPeopleRelatedtoDocument(
			PeopleRelatedToDocumentJson json) throws ApplicationThrowable;

	public GenericResponseJson deletePeopleRelatedtoDocument(
			PeopleRelatedToDocumentJson json) throws ApplicationThrowable;

	public GenericResponseJson modifyPeopleRelatedtoDocument(
			PeopleRelatedToDocumentJson json) throws ApplicationThrowable;

	public Integer deleteFolio(FolioJson folioToDelete)
			throws ApplicationThrowable;

	public GenericResponseJson saveTranscription(String transcription,
			Integer documententId, Integer uploadedFileId)
			throws ApplicationThrowable;

	public List<DocumentTranscriptionEntity> getDocumentTranscriptions(
			Integer documentId) throws ApplicationThrowable;

	public Integer updateTransSearch(Integer documentId)
			throws ApplicationThrowable;

	public Integer updateAllTransSearch() throws ApplicationThrowable;

	public List<PeopleJson> findComplexPeople(
			PeopleComplexSearchJson searchReq,
			PeopleSearchPaginationParam pagParam) throws ApplicationThrowable;

	public Integer updateDocumentPrivacyById(Integer documentId, Integer privacy)
			throws ApplicationThrowable;

	public GenericResponseJson unDeleteDE(Integer documentEntityId)
			throws ApplicationThrowable;

	public List<DocumentJson> findDocumentsByPlaceOforigin(Integer placeOforigin)
			throws ApplicationThrowable;

	public List<HashMap> findDocumentsInVolume(Volume vol, Boolean notChildren)
			throws ApplicationThrowable;

	public List<HashMap> findDocumentsInInsert(InsertEntity ins,
			Boolean notChildren) throws ApplicationThrowable;

	void associateImagesDocuments() throws ApplicationThrowable;
	
	void updateDocumentsPrivacy() throws ApplicationThrowable;
	
	void removePrivateImagesFromDocuments() throws ApplicationThrowable;

	public DocumentJson findDocumentByEntity(MiaDocumentEntity docEntity)
			throws ApplicationThrowable;

	public List<DocumentJson> findDocuments(List<Integer> docIds,
			Integer firstResult, Integer maxResult, String orderColumn,
			String ascOrDesc) throws ApplicationThrowable;

	public Long countUserOwnDocuments(User user)
			throws ApplicationThrowable;

	public MiaDocumentEntity findDocumentEntity(Integer id) throws ApplicationThrowable;

	public List<String> findUsersRelatedDocOfImageId(Integer uploadFileId);
	
	public Integer createNoImageDocument(
			DocumentMetaDataJson documentMetaDataJson);

}