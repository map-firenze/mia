package org.medici.mia.service.miadoc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.medici.mia.common.json.PeopleBaseJson;
import org.medici.mia.common.json.PlaceBaseJson;
import org.medici.mia.common.json.document.ModifyPeopleForDocJson;
import org.medici.mia.common.json.document.ModifyPlaceForDocJson;

public class MiaDocumentUtils {

	public static String deletePeople(String peoplesStr, String peopleDeleted) {

		String modifiedPeoplesStr = "";
		if (peoplesStr != null && !peoplesStr.isEmpty()) {
			List<String> peoples = new ArrayList<String>();
			for (String peopleIdStr : (List<String>) Arrays.asList(peoplesStr
					.split(","))) {
				String[] peoplesIdArr = peopleIdStr.split(":");
				if (peoplesIdArr != null && peoplesIdArr.length > 0) {
					if (!peoplesIdArr[0].equalsIgnoreCase(peopleDeleted)) {
						peoples.add(peopleIdStr);
					}
				}
			}
			for (String peopleStr : peoples) {
				modifiedPeoplesStr = modifiedPeoplesStr + peopleStr + ",";
			}
		}
		return modifiedPeoplesStr;
	}

	public static String deletePlace(String placesStr, String placeDeleted) {

		String modifiedPlacesStr = "";
		if (placesStr != null && !placesStr.isEmpty()) {
			List<String> places = new ArrayList<String>();
			for (String peopleIdStr : (List<String>) Arrays.asList(placesStr
					.split(","))) {
				String[] placesIdArr = placesStr.split(":");
				if (placesIdArr != null && placesIdArr.length > 0) {
					if (!placesIdArr[0].equalsIgnoreCase(placeDeleted)) {
						places.add(peopleIdStr);
					}
				}
			}
			for (String placeStr : places) {
				modifiedPlacesStr = modifiedPlacesStr + placeStr + ",";
			}
		}
		return modifiedPlacesStr;
	}

	public static String deletePeople(String peoplesStr,
			PeopleBaseJson peopleDeleted) {
		return deletePeople(peoplesStr, String.valueOf(peopleDeleted.getId()));
	}

	public static String deletePlace(String placesStr,
			PlaceBaseJson placeDeleted) {
		return deletePeople(placesStr, String.valueOf(placeDeleted.getId()));
	}

	public static String addPeople(String peoplesStr, PeopleBaseJson peopleAdded) {

		if (peoplesStr != null && !peoplesStr.isEmpty()) {
			for (String peopleIdStr : (List<String>) Arrays.asList(peoplesStr
					.split(","))) {
				String[] peoplesIdArr = peopleIdStr.split(":");
				if (peoplesIdArr != null && peoplesIdArr.length > 0) {
					if (peoplesIdArr[0].equalsIgnoreCase(String
							.valueOf(peopleAdded.getId()))) {
						return peoplesStr;
					}
				}
			}
		} else {
			peoplesStr = "";
		}

		return peoplesStr + String.valueOf(peopleAdded.getId()) + ":"
				+ peopleAdded.getUnsure() + ",";
	}

	public static String addPlace(String placesStr, PlaceBaseJson placeAdded) {

		if (placesStr != null && !placesStr.isEmpty()) {
			for (String placesIdStr : (List<String>) Arrays.asList(placesStr
					.split(","))) {
				String[] placesIdArr = placesIdStr.split(":");
				if (placesIdArr != null && placesIdArr.length > 0) {
					if (placesIdArr[0].equalsIgnoreCase(String
							.valueOf(placeAdded.getId()))) {
						return placesStr;
					}
				}
			}
		} else {
			placesStr = "";
		}

		return placesStr + String.valueOf(placeAdded.getId()) + ":"
				+ placeAdded.getUnsure() + ",";
	}

	public static String modifyPeopleForDoc(String peoplesStr,
			ModifyPeopleForDocJson modifyPeopleForDoc) {

		String deletedPeoplesStr = "";

		if (modifyPeopleForDoc == null)
			return null;

		if (modifyPeopleForDoc.getIdPeopleToDelete() != null) {
			deletedPeoplesStr = deletePeople(peoplesStr,
					String.valueOf(modifyPeopleForDoc.getIdPeopleToDelete()));
		} else {
			deletedPeoplesStr = peoplesStr;
		}

		if (modifyPeopleForDoc.getNewPeople() != null) {
			return addPeople(deletedPeoplesStr,
					modifyPeopleForDoc.getNewPeople());
		}

		return deletedPeoplesStr;
	}

	public static String modifyPlaceForDoc(String placesStr,
			ModifyPlaceForDocJson modifyPlaceForDoc) {

		String deletedPlacesStr = "";

		if (modifyPlaceForDoc == null)
			return null;

		if (modifyPlaceForDoc.getIdPlaceToDelete() != null) {
			deletedPlacesStr = deletePeople(placesStr,
					String.valueOf(modifyPlaceForDoc.getIdPlaceToDelete()));
		} else {
			deletedPlacesStr = placesStr;
		}

		if (modifyPlaceForDoc.getNewPlace() != null) {
			return addPlace(deletedPlacesStr, modifyPlaceForDoc.getNewPlace());
		}

		return deletedPlacesStr;
	}

	public static List<PeopleBaseJson> getPeopleBases(String peoplesStr) {

		if (peoplesStr == null || peoplesStr.isEmpty())
			return null;

		ArrayList<PeopleBaseJson> peopleBases = new ArrayList<PeopleBaseJson>();

		for (String peopleIdStr : (List<String>) Arrays.asList(peoplesStr
				.split(","))) {

			PeopleBaseJson peopleBase = new PeopleBaseJson();
			String[] peoplesIdArr = peopleIdStr.split(":");
			if (peoplesIdArr[0] != null || !peoplesIdArr[0].isEmpty()) {
				peopleBase.setId(Integer.valueOf(peoplesIdArr[0]));
			}
			if (peoplesIdArr.length > 1) {
				peopleBase.setUnsure(peoplesIdArr[1]);
			}

			peopleBases.add(peopleBase);
		}

		return peopleBases;

	}

	public static List<PlaceBaseJson> getPlaceBases(String placesStr) {

		if (placesStr == null || placesStr.isEmpty())
			return null;

		ArrayList<PlaceBaseJson> placeBases = new ArrayList<PlaceBaseJson>();

		for (String placeStr : (List<String>) Arrays.asList(placesStr
				.split(","))) {

			PlaceBaseJson placeBase = new PlaceBaseJson();
			String[] placesIdArr = placeStr.split(":");
			if (placesIdArr[0] != null || !placesIdArr[0].isEmpty()) {
				placeBase.setId(Integer.valueOf(placesIdArr[0]));
			}
			if (placesIdArr.length > 1) {
				placeBase.setUnsure(placesIdArr[1]);
			}

			placeBases.add(placeBase);
		}

		return placeBases;

	}

}
