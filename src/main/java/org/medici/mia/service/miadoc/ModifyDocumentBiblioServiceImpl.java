package org.medici.mia.service.miadoc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.document.BiblioBaseJson;
import org.medici.mia.common.json.document.FindBiblioForDocJson;
import org.medici.mia.common.json.document.ModifyBiblioForDocJson;
import org.medici.mia.common.util.DocumentPrivacyUtils;
import org.medici.mia.dao.miadoc.DocumentBiblioRefDao;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.domain.BiblioRefEntity;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.User;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.message.MessageService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Service
class ModifyDocumentBiblioServiceImpl implements ModifyDocumentBiblioService {

	@Autowired
	private DocumentBiblioRefDao documentBiblioRefDao;

	@Autowired
	private UserService userService;

	@Autowired
	private GenericDocumentDao genericDocumentDao;

	@Autowired
	private HistoryLogService historyLogService;
	
	@Autowired
	private MessageService messageService;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public FindBiblioForDocJson findDocumentBiblios(Integer documentId)
			throws ApplicationThrowable {

		FindBiblioForDocJson biblioForDoc = new FindBiblioForDocJson();

		try {

			biblioForDoc.setDocumentId(String.valueOf(documentId));

			List<BiblioRefEntity> entities = getDocumentBiblioRefDao()
					.findBiblios(documentId);

			if (entities == null)
				return null;
			List<BiblioBaseJson> biblios = new ArrayList<BiblioBaseJson>();
			for (BiblioRefEntity entity : entities) {
				BiblioBaseJson biblio = new BiblioBaseJson();
				biblio.setBiblioId(entity.getIdRef());
				biblio.setLink(entity.getPermalink());
				biblio.setName(entity.getName());
				biblios.add(biblio);
			}

			biblioForDoc.setBiblios(biblios);

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return biblioForDoc;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson modifyDocumentBiblio(
			ModifyBiblioForDocJson modifyBiblio) throws ApplicationThrowable {
		GenericResponseDataJson resp = new GenericResponseDataJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");

		try {

			getDocumentBiblioRefDao().modifyBiblio(modifyBiblio);

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("BiblioGraf of document Modified.");

			// Get the username logged in
			UserDetails currentUser = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();

			MiaDocumentEntity doc = getGenericDocumentDao()
					.findDocumentById(Integer.valueOf(modifyBiblio.getDocumentId()));
			if (!currentUser.getUsername().equals(doc.getCreatedBy()) && !DocumentPrivacyUtils.isUserAdmin(currentUser)
					&& !DocumentPrivacyUtils.isOnsiteFellows(currentUser)) {
				messageService.onDocumentEdited(currentUser.getUsername(), doc.getCreatedBy(),
						doc.getDocumentEntityId());
			}

			getGenericDocumentDao().setDocumentModInf(
					Integer.valueOf(modifyBiblio.getDocumentId()),
					currentUser.getUsername());

			HashMap<String, Object> changes = new HashMap<String, Object>();
			HashMap<String, HashMap> category = new HashMap<String, HashMap>();
			HashMap<String, HashMap> action = new HashMap<String, HashMap>();
			if (modifyBiblio.getNewBiblio() != null) {
				changes.put("link", modifyBiblio.getNewBiblio().getLink());
				changes.put("name", modifyBiblio.getNewBiblio().getName());
				action.put("add", changes);
			} else {
				if (modifyBiblio.getIdBiblioToBeDeleted() != null) {
					changes.put("biblioId", modifyBiblio.getIdBiblioToBeDeleted());
					action.put("delete", changes);
				}
			}
			if(!action.isEmpty()) {
				category.put("documentBiblio", action);
				historyLogService.registerAction(
						Integer.valueOf(modifyBiblio.getDocumentId()), HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.DE, null, category);
			}
			resp.setData(modifyBiblio.getNewBiblio());

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	public DocumentBiblioRefDao getDocumentBiblioRefDao() {
		return documentBiblioRefDao;
	}

	public void setDocumentBiblioRefDao(
			DocumentBiblioRefDao documentBiblioRefDao) {
		this.documentBiblioRefDao = documentBiblioRefDao;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public GenericDocumentDao getGenericDocumentDao() {
		return genericDocumentDao;
	}

	public void setGenericDocumentDao(GenericDocumentDao genericDocumentDao) {
		this.genericDocumentDao = genericDocumentDao;
	}

}
