package org.medici.mia.service.miadoc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;

import org.apache.xmlbeans.impl.piccolo.xml.DocumentEntity;
import org.medici.mia.common.json.CollectionJson;
import org.medici.mia.common.json.CreatePlaceJson;
import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.LanguageJson;
import org.medici.mia.common.json.PeopleBaseJson;
import org.medici.mia.common.json.PeopleJson;
import org.medici.mia.common.json.PeopleRelatedToDocumentJson;
import org.medici.mia.common.json.PlaceJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.TopicJson;
import org.medici.mia.common.json.UploadFileJson;
import org.medici.mia.common.json.VolumeDescriptionJson;
import org.medici.mia.common.json.complexsearch.PeopleComplexSearchJson;
import org.medici.mia.common.json.document.CorrespondenceJson;
import org.medici.mia.common.json.document.CreateDEResponseJson;
import org.medici.mia.common.json.document.DocPeopleFieldJson;
import org.medici.mia.common.json.document.DocumentFactoryJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.document.DocumentMetaDataJson;
import org.medici.mia.common.json.document.DocumentPeopleFieldsJson;
import org.medici.mia.common.json.document.DocumentPeopleJson;
import org.medici.mia.common.json.document.DocumentTranscriptionJson;
import org.medici.mia.common.json.document.GenericDocumentJson;
import org.medici.mia.common.json.document.ModifyImageJson;
import org.medici.mia.common.json.document.NewsJson;
import org.medici.mia.common.json.document.TopicPlaceJson;
import org.medici.mia.common.json.folio.FolioJson;
import org.medici.mia.common.json.folio.FolioResponseJson;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.common.search.AdvancedSearchAbstract.Gender;
import org.medici.mia.common.util.DocumentPrivacyUtils;
import org.medici.mia.common.util.DocumentPrivacyUtils.PrivacyType;
import org.medici.mia.common.util.NativeQueryUtils;
import org.medici.mia.comparator.DocumentJsonArchivalLocationAscComparator;
import org.medici.mia.comparator.DocumentJsonArchivalLocationDescComparator;
import org.medici.mia.controller.genericsearch.PeopleSearchPaginationParam;
import org.medici.mia.dao.altname.AltNameDAO;
import org.medici.mia.dao.casestudy.CaseStudyDAO;
import org.medici.mia.dao.collation.CollationDAO;
import org.medici.mia.dao.collection.CollectionDAO;
import org.medici.mia.dao.document.DocumentDAO;
import org.medici.mia.dao.documentTypology.DocumentTypologyDAO;
import org.medici.mia.dao.documentfield.DocumentFieldDAO;
import org.medici.mia.dao.fileSharing.FileSharingDAO;
import org.medici.mia.dao.folio.FolioDao;
import org.medici.mia.dao.miadoc.DocumentBiblioRefDao;
import org.medici.mia.dao.miadoc.DocumentFileDao;
import org.medici.mia.dao.miadoc.DocumentLanguageDao;
import org.medici.mia.dao.miadoc.DocumentProducerDao;
import org.medici.mia.dao.miadoc.DocumentRefToPeopleDao;
import org.medici.mia.dao.miadoc.DocumentRelatedDocsDao;
import org.medici.mia.dao.miadoc.DocumentTranscriptionDAO;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.dao.miadoc.LanguageDao;
import org.medici.mia.dao.miadoc.TopicPlaceDao;
import org.medici.mia.dao.people.OrganizationDAO;
import org.medici.mia.dao.people.PeopleDAO;
import org.medici.mia.dao.place.PlaceDAO;
import org.medici.mia.dao.placetype.PlaceTypeDAO;
import org.medici.mia.dao.repository.RepositoryDAO;
import org.medici.mia.dao.topicslist.TopicsListDAO;
import org.medici.mia.dao.uploadfile.UploadFileDAO;
import org.medici.mia.dao.uploadinfo.UploadInfoDAO;
import org.medici.mia.dao.volume.VolumeDAO;
import org.medici.mia.domain.AltName;
import org.medici.mia.domain.BiblioRefEntity;
import org.medici.mia.domain.CollectionEntity;
import org.medici.mia.domain.Document;
import org.medici.mia.domain.Document.RectoVerso;
import org.medici.mia.domain.DocumentFieldEntity;
import org.medici.mia.domain.DocumentFileJoinEntity;
import org.medici.mia.domain.DocumentProducerJoinEntity;
import org.medici.mia.domain.DocumentRefToPeopleEntity;
import org.medici.mia.domain.DocumentTranscriptionEntity;
import org.medici.mia.domain.DocumentTypologyEntity;
import org.medici.mia.domain.FileSharing;
import org.medici.mia.domain.FolioEntity;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.domain.IGenericEntity;
import org.medici.mia.domain.InsertEntity;
import org.medici.mia.domain.LanguageEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.People;
import org.medici.mia.domain.Place;
import org.medici.mia.domain.PlaceType;
import org.medici.mia.domain.RepositoryEntity;
import org.medici.mia.domain.TopicList;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.UploadInfoEntity;
import org.medici.mia.domain.User;
import org.medici.mia.domain.Volume;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.security.MiaUserDetailsImpl;
import org.medici.mia.service.geobase.GeoBaseService;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.message.MessageService;
import org.medici.mia.service.people.BiographicalPeopleService;
import org.medici.mia.service.upload.UploadService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.underscore.U;
import com.mysql.jdbc.StringUtils;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Service
class MiaDocumentServiceImpl implements MiaDocumentService {

	@Autowired
	private GenericDocumentDao documentDAO;

	@Autowired
	private FolioDao folioDao;

	@Autowired
	private DocumentFileDao docFileDao;

	@Autowired
	private UserService userService;

	@Autowired
	private UploadFileDAO uploadFileDAO;

	@Autowired
	private UploadInfoDAO uploadInfoDAO;

	@Autowired
	private DocumentTypologyDAO docTypologyDAO;

	@Autowired
	private PeopleDAO peopleDao;

	@Autowired
	private PlaceDAO placeDao;

	@Autowired
	private DocumentFieldDAO documentFieldDao;

	@Autowired
	private DocumentProducerDao documentProducerDao;

	@Autowired
	private TopicsListDAO topicsListDao;

	@Autowired
	private TopicPlaceDao topicPlaceDao;

	@Autowired
	private GeoBaseService geoBaseService;

	@Autowired
	private PlaceTypeDAO placeTypeDao;

	@Autowired
	private DocumentRefToPeopleDao documentRefToPeopleDao;

	@Autowired
	private DocumentRelatedDocsDao documentRelatedDocsDao;

	@Autowired
	private DocumentBiblioRefDao documentBiblioRefDao;

	@Autowired
	private DocumentLanguageDao documentLanguageDao;

	@Autowired
	private AltNameDAO altNameDAO;

	@Autowired
	private LanguageDao languageDAO;

	@Autowired
	private DocumentTranscriptionDAO documentTranscriptionDAO;

	@Autowired
	private DocumentFileDao documentFileDao;

	@Autowired
	private BiographicalPeopleService biographicalPeopleService;

	@Autowired
	private OrganizationDAO organizationDAO;

	@Autowired
	private GenericDocumentDao documentDao;

	@Autowired
	private MiaDocumentService miaDocumentService;

	@Autowired
	private GenericDocumentDao genericDocumentDao;

	@Autowired
	private AutowireCapableBeanFactory autowireCapableBeanFactory;

	@Autowired
	private HistoryLogService historyLogService;

	@Autowired
	private FileSharingDAO fileSharingDao;

	@Autowired
	private CollationDAO collationDAO;

	@Autowired
	private CaseStudyDAO caseStudyDAO;

	@Autowired
	private DocumentDAO documetDAO;
	
	@Autowired
	private UploadService uploadService;
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private RepositoryDAO repositoryDAO;
	
	@Autowired 
	private CollectionDAO collectionDAO;
	
	@Autowired
	private VolumeDAO volumeDAO;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public DocumentJson findDocumentById(Integer documentId)
			throws ApplicationThrowable {

		try {

			MiaDocumentEntity docEntity = getDocumentDAO().findDocumentById(
					documentId);

			if (docEntity == null) {
				GenericDocumentJson res = new GenericDocumentJson();
				return res;
			}

			return getDocumentJson(docEntity);
			
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public DocumentJson findDocumentByEntity(MiaDocumentEntity docEntity)
			throws ApplicationThrowable {

		try {

			if (docEntity == null) {
				GenericDocumentJson res = new GenericDocumentJson();
				return res;
			}

			return getDocumentJson(docEntity);

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public Integer updateDocumentPrivacyById(Integer documentId, Integer privacy)
			throws ApplicationThrowable {

		try {

			MiaDocumentEntity docEntity = getDocumentDAO().findDocumentById(
					documentId);
			if (privacy == null) {
				privacy = docEntity.getPrivacy();
			}
			if (privacy == 0) {
				privacy = 1;
			} else if (privacy == 1) {
				privacy = 0;
			}
			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());
			List<UploadFileEntity> uploadFiles = docEntity.getUploadFileEntities();
	
			for(UploadFileEntity filesUpload : uploadFiles) {
				getUploadFileDAO().updateUploadFilePrivacyField(filesUpload.getUploadFileId(), privacy);
				UploadInfoEntity uploadEnt = getUploadInfoDAO().find(filesUpload.getIdTblUpload());
				Integer privacyUpload = 0;
				for(UploadFileEntity filesOfUpload: uploadEnt.getUploadFileEntities()) {
					if(filesOfUpload.getFilePrivacy() != 0) {
						privacyUpload = 1;
					}
				}
				if(privacyUpload != uploadEnt.getFilePrivacy()) {
					getUploadInfoDAO().updateUploadInfoPrivacyField(uploadEnt.getUploadInfoId(),
							privacyUpload, user.getAccount());
				}
			}

			HashMap<String, HashMap> changes = new HashMap<String, HashMap>();
			HashMap<String, HashMap> category = new HashMap<String, HashMap>();
			HashMap<String, HashMap> action = new HashMap<String, HashMap>();
			HashMap<Object, Object> before = new HashMap<Object, Object>();
			HashMap<Object, Object> after = new HashMap<Object, Object>();

			before.put("privacy", privacy == 1 ? "public" : "private");
			after.put("privacy", privacy == 0 ? "public" : "private");
			changes.put("before", before);
			changes.put("after", after);
			action.put("edit", changes);
			category.put("documentPrivacy", action);

			historyLogService.registerAction(documentId,
					HistoryLogEntity.UserAction.EDIT,
					HistoryLogEntity.RecordType.DE, null, category);

			return getDocumentDAO().updateDocument(documentId, privacy);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<DocumentJson> findDocuments(List<Integer> docIds,
			Integer firstResult, Integer maxResult, String orderColumn,
			String ascOrDesc) throws ApplicationThrowable {

		try {

			if (docIds == null || docIds.isEmpty()) {
				return null;
			}

			List<MiaDocumentEntity> entityList = getGenericDocumentDao()
					.findDocuments(docIds, firstResult, maxResult, orderColumn,
							ascOrDesc);

			if (entityList == null || entityList.isEmpty()) {
				return null;
			}

			List<DocumentJson> docsJson = new ArrayList<DocumentJson>();

			for (MiaDocumentEntity e : entityList) {
				DocumentJson doc = findDocumentByEntity(e);
				if (doc != null) {
					docsJson.add(doc);
				}
			}

			if (orderColumn.equalsIgnoreCase("archivalLocation")) {
				if ("asc".equalsIgnoreCase(ascOrDesc)) {
					Collections.sort(docsJson, new DocumentJsonArchivalLocationAscComparator());
				} else if ("desc".equalsIgnoreCase(ascOrDesc)) {
					Collections.sort(docsJson, new DocumentJsonArchivalLocationDescComparator());
				}
				docsJson = docsJson.subList(firstResult,
						docsJson.size() < firstResult + maxResult ? docsJson.size() : firstResult + maxResult);
			}
			
			return docsJson;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	private DocumentJson getDocumentJson(MiaDocumentEntity docEntity)
			throws ApplicationThrowable {

		if (docEntity == null) {
			return null;
		}

		setProducerUnsureField(docEntity);

		setTransientFields(docEntity);
		DocumentJson doc = DocumentFactoryJson.getDocument(docEntity
				.getCategory());
		UserDetails currentUser = ((UserDetails) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal());
		autowireCapableBeanFactory.autowireBean(doc);
		if ((DocumentPrivacyUtils.getPrivacyType(docEntity.getCreatedBy(),
				docEntity.getPrivacy()) == PrivacyType.PUBLIC)) {
			if (this.isAbleToViewDoc(docEntity, currentUser) == true) {
				((GenericDocumentJson) doc).setCanDeleteAndChangePrivacy(true);
				((GenericDocumentJson) doc).setCanModify(true);
			} else {
				((GenericDocumentJson) doc).setCanDeleteAndChangePrivacy(false);
				((GenericDocumentJson) doc).setCanModify(false);
			}
			((GenericDocumentJson) doc).setCanView(true);
			// if it's private, init normally
			doc.init(docEntity);
			// ADD Synopsis
			((GenericDocumentJson) doc).setGeneralNoteSynopsis(docEntity
					.getGeneralNotesSynopsis());
		} else {
			// else, if it's private, check if a sharing exists for at least one image or it's shared in case study
			if(caseStudyDAO.isDocumentSharedWithMe(docEntity.getDocumentEntityId())){
//				doc.init(docEntity);
//				// sets the privacy with value = 2 to model private but shared
//				((GenericDocumentJson)doc).setPrivacy(2);
//				//ADD Synopsis
//				((GenericDocumentJson)doc).setGeneralNoteSynopsis(docEntity.getGeneralNotesSynopsis());
//				((GenericDocumentJson)doc).setCanView(true);
				for (UploadFileEntity upload : docEntity.getUploadFileEntities()) {

					doc.initPrivate(docEntity);

					if (this.isAbleToViewDoc(docEntity, currentUser) == true) {
						((GenericDocumentJson) doc).setCanView(true);
						((GenericDocumentJson) doc)
								.setCanDeleteAndChangePrivacy(true);
						((GenericDocumentJson) doc).setCanModify(true);
					} else {
						((GenericDocumentJson) doc).setCanView(false);
						((GenericDocumentJson) doc)
								.setCanDeleteAndChangePrivacy(false);
						((GenericDocumentJson) doc).setCanModify(false);
						for (UploadFileJson file : ((GenericDocumentJson) doc)
								.getUploadFiles()) {
							file.setFilePath(null);
						}
					}

					List<FileSharing> fileSharings = fileSharingDao
							.getByUploadAndFile(upload.getIdTblUpload(),
									upload.getUploadFileId());
					if (DocumentPrivacyUtils.isSharedWithMe(fileSharings)) {
						((GenericDocumentJson) doc).init(docEntity);
						((GenericDocumentJson) doc).setCanView(true);
						((GenericDocumentJson) doc)
								.setCanDeleteAndChangePrivacy(true);
						((GenericDocumentJson) doc).setCanModify(false);
						// sets the privacy with value = 2 to model private but
						// shared
						((GenericDocumentJson) doc).setPrivacy(2);
						// ADD Synopsis
						((GenericDocumentJson) doc)
								.setGeneralNoteSynopsis(docEntity
										.getGeneralNotesSynopsis());
						break;
					}
				}				
				
			} else {
				for (UploadFileEntity upload : docEntity.getUploadFileEntities()) {

					doc.initPrivate(docEntity);

					if (this.isAbleToViewDoc(docEntity, currentUser) == true) {
						((GenericDocumentJson) doc).setCanView(true);
						((GenericDocumentJson) doc)
								.setCanDeleteAndChangePrivacy(true);
						((GenericDocumentJson) doc).setCanModify(true);
					} else {
						((GenericDocumentJson) doc).setCanView(false);
						((GenericDocumentJson) doc)
								.setCanDeleteAndChangePrivacy(false);
						((GenericDocumentJson) doc).setCanModify(false);
						for (UploadFileJson file : ((GenericDocumentJson) doc)
								.getUploadFiles()) {
							file.setFilePath(null);
						}
					}

					List<FileSharing> fileSharings = fileSharingDao
							.getByUploadAndFile(upload.getIdTblUpload(),
									upload.getUploadFileId());
					if (DocumentPrivacyUtils.isSharedWithMe(fileSharings)) {
						((GenericDocumentJson) doc).init(docEntity);
						((GenericDocumentJson) doc).setCanView(true);
						((GenericDocumentJson) doc)
								.setCanDeleteAndChangePrivacy(true);
						((GenericDocumentJson) doc).setCanModify(false);
						// sets the privacy with value = 2 to model private but
						// shared
						((GenericDocumentJson) doc).setPrivacy(2);
						// ADD Synopsis
						((GenericDocumentJson) doc)
								.setGeneralNoteSynopsis(docEntity
										.getGeneralNotesSynopsis());
						break;
					}
				}
			}
		}

		// If Correspondence
		if (doc instanceof CorrespondenceJson) {
			CorrespondenceJson correspondenceJson = (CorrespondenceJson) doc;
			if (docEntity.getDocumentEntityId() < 50000) {
				// documento Bia
				if (correspondenceJson.getUploadFiles().size() < 1) {
					Document biaDoc = documetDAO.find(docEntity.getDocumentEntityId());
					if (biaDoc != null) {
						String folioNumber = biaDoc.getTranscribeFolioNum() != null
								? biaDoc.getTranscribeFolioNum().toString()
								: null;
						String folioMod = folioNumber + (biaDoc.getTranscribeFolioMod() != null
								? " " + biaDoc.getTranscribeFolioMod().toString()
								: "");
						String rectoverso = "recto";
						if (biaDoc.getTranscribeFolioRectoVerso() != null) {
							rectoverso = biaDoc.getTranscribeFolioRectoVerso().equals(RectoVerso.N) ? "notapplicable"
									: (biaDoc.getTranscribeFolioRectoVerso().equals(RectoVerso.V) ? "verso" : "recto");
						}
						if (folioNumber == null) {
							folioNumber = biaDoc.getFolioNum() != null ? biaDoc.getFolioNum().toString() : null;
							folioMod = folioNumber
									+ (biaDoc.getFolioMod() != null ? " " + biaDoc.getFolioMod().toString() : "");
							rectoverso = "recto";
							if (biaDoc.getFolioRectoVerso() != null) {
								rectoverso = biaDoc.getFolioRectoVerso().equals(RectoVerso.N) ? "notapplicable"
										: (biaDoc.getFolioRectoVerso().equals(RectoVerso.V) ? "verso" : "recto");
							}
						}

						String repositoryId = "1";

						// Mediceo del Principato

						CollectionEntity collectionEntity = null;
						List<CollectionEntity> collEntity = uploadService
								.findAllCollections(Integer.valueOf(repositoryId));
						for (CollectionEntity collectionEntity2 : collEntity) {
							if (collectionEntity2.getCollectionAbbreviation().equalsIgnoreCase("MDP")) {
								collectionEntity = collectionEntity2;
								break;
							}
						}
						CollectionJson collection = new CollectionJson();
						collection.setCollectionName(collectionEntity.getCollectionName());
						collection.setCollectionAbbreviation(collectionEntity.getCollectionAbbreviation());
						collection.setCollectionDescription(collectionEntity.getCollectionDescription());
						collection.setCollectionId(collectionEntity.getCollectionId().toString());
						collection.setStudyCollection(collectionEntity.getStudyCollection());
						correspondenceJson.setCollection(collection);

						List<UploadFileJson> uploadFiles = new ArrayList<UploadFileJson>();

						UploadFileJson entity = new UploadFileJson();
						entity.setUploadFileId(-1);
						entity.setImageOrder(0);
						entity.setDocumentId(docEntity.getDocumentEntityId());
						entity.setFilePath("/Mia/images/1024/img_document.png");
						entity.setUploadFileId(-1);
						entity.setInserName(biaDoc.getInsertNum());
						Volume volume = biaDoc.getVolume();
						entity.setVolumeName(volume.getVolume());
						entity.setCollectionName(collectionEntity.getCollectionName());
						entity.setSeriesName("");
						List<FolioJson> folios = new ArrayList<FolioJson>();

						FolioJson folio = new FolioJson();
						folio.setFolioId(-1);
						folio.setImgName("");
						if (folioNumber == null || folioNumber.equals("")) {
							folio.setNoNumb(true);
						} else {
							folio.setNoNumb(false);
							folio.setRectoverso(rectoverso);
						}
						folio.setFolioNumber(folioNumber);
						folio.setUploadFileId(-1);

						folios.add(folio);
						entity.setFolios(folios);

						uploadFiles.add(entity);
						correspondenceJson.setUploadFiles(uploadFiles);

						if (correspondenceJson.getTranscriptions() != null) {
							if (correspondenceJson.getTranscriptions().size() == 1) {
								if (correspondenceJson.getTranscriptions().get(0).getUploadedFileId() == null) {
									correspondenceJson.getTranscriptions().get(0).setUploadedFileId("-1");
								}
							}
							correspondenceJson.setVolumeId(volume.getSummaryId());
							correspondenceJson.setVolume(new VolumeDescriptionJson().toJson(volume, null));
						}

					}
				}

				/*
				 * entity.setF
				 *
				 * "uploadFiles" { "uploadFileId":1497, "imageOrder":6, "documentId":73,
				 * "filePath":
				 * "/Mia/mview/IIPImageServer.do?FIF=ASFI-ChIJrdbSgKZWKhMRAyrH7xd51ZM/MDP/1/thumbs/0036_C_024_R.jpg&WID=250"
				 * , "uploadInfoId":103, "inserName":null, "volumeName":"1",
				 * "collectionName":"Mediceo del Principato", "seriesName":"", "folios":[ {
				 * "imgName":"0036_C_024_R.jpg", "noNumb":false, "folioId":1026,
				 * "folioNumber":"24", "rectoverso":"recto", "uploadFileId":1497 } ] }
				 */
			}

		}
		// If News
		if (doc instanceof NewsJson) {
			NewsJson newsJson = (NewsJson) doc;
			if (docEntity.getDocumentEntityId() < 50000) {
				// documento Bia
				if (newsJson.getUploadFiles().size() < 1) {
					Document biaDoc = documetDAO.find(docEntity.getDocumentEntityId());
					if (biaDoc != null) {
						String folioNumber = biaDoc.getTranscribeFolioNum() != null
								? biaDoc.getTranscribeFolioNum().toString()
								: null;
						String folioMod = folioNumber + (biaDoc.getTranscribeFolioMod() != null
								? " " + biaDoc.getTranscribeFolioMod().toString()
								: "");
						String rectoverso = "recto";
						if (biaDoc.getTranscribeFolioRectoVerso() != null) {
							rectoverso = biaDoc.getTranscribeFolioRectoVerso().equals(RectoVerso.N) ? "notapplicable"
									: (biaDoc.getTranscribeFolioRectoVerso().equals(RectoVerso.V) ? "verso" : "recto");
						}
						if (folioNumber == null) {
							folioNumber = biaDoc.getFolioNum() != null ? biaDoc.getFolioNum().toString() : null;
							folioMod = folioNumber
									+ (biaDoc.getFolioMod() != null ? " " + biaDoc.getFolioMod().toString() : "");
							rectoverso = "recto";
							if (biaDoc.getFolioRectoVerso() != null) {
								rectoverso = biaDoc.getFolioRectoVerso().equals(RectoVerso.N) ? "notapplicable"
										: (biaDoc.getFolioRectoVerso().equals(RectoVerso.V) ? "verso" : "recto");
							}
						}

						String repositoryId = "1";

						// Mediceo del Principato

						CollectionEntity collectionEntity = null;
						List<CollectionEntity> collEntity = uploadService
								.findAllCollections(Integer.valueOf(repositoryId));
						for (CollectionEntity collectionEntity2 : collEntity) {
							if (collectionEntity2.getCollectionAbbreviation().equalsIgnoreCase("MDP")) {
								collectionEntity = collectionEntity2;
								break;
							}
						}
						CollectionJson collection = new CollectionJson();
						collection.setCollectionName(collectionEntity.getCollectionName());
						collection.setCollectionAbbreviation(collectionEntity.getCollectionAbbreviation());
						collection.setCollectionDescription(collectionEntity.getCollectionDescription());
						collection.setCollectionId(collectionEntity.getCollectionId().toString());
						collection.setStudyCollection(collectionEntity.getStudyCollection());
						newsJson.setCollection(collection);

						List<UploadFileJson> uploadFiles = new ArrayList<UploadFileJson>();

						UploadFileJson entity = new UploadFileJson();
						entity.setUploadFileId(-1);
						entity.setImageOrder(0);
						entity.setDocumentId(docEntity.getDocumentEntityId());
						entity.setFilePath("/Mia/images/1024/img_document.png");
						entity.setUploadFileId(-1);
						entity.setInserName(biaDoc.getInsertNum());
						Volume volume = biaDoc.getVolume();
						entity.setVolumeName(volume.getVolume());
						entity.setCollectionName(collectionEntity.getCollectionName());
						entity.setSeriesName("");
						List<FolioJson> folios = new ArrayList<FolioJson>();

						FolioJson folio = new FolioJson();
						folio.setFolioId(-1);
						folio.setImgName("");
						if (folioNumber == null || folioNumber.equals("")) {
							folio.setNoNumb(true);
						} else {
							folio.setNoNumb(false);
							folio.setRectoverso(rectoverso);
						}
						folio.setFolioNumber(folioNumber);
						folio.setUploadFileId(-1);

						folios.add(folio);
						entity.setFolios(folios);

						uploadFiles.add(entity);
						newsJson.setUploadFiles(uploadFiles);

						if (newsJson.getTranscriptions() != null) {
							if (newsJson.getTranscriptions().size() == 1) {
								if (newsJson.getTranscriptions().get(0).getUploadedFileId() == null) {
									newsJson.getTranscriptions().get(0).setUploadedFileId("-1");
								}
							}
							newsJson.setVolumeId(volume.getSummaryId());
							newsJson.setVolume(new VolumeDescriptionJson().toJson(volume, null));
						}

					}
				}

				/*
				 * "uploadFiles" { "uploadFileId":1497, "imageOrder":6, "documentId":73,
				 * "filePath":
				 * "/Mia/mview/IIPImageServer.do?FIF=ASFI-ChIJrdbSgKZWKhMRAyrH7xd51ZM/MDP/1/thumbs/0036_C_024_R.jpg&WID=250"
				 * , "uploadInfoId":103, "inserName":null, "volumeName":"1",
				 * "collectionName":"Mediceo del Principato", "seriesName":"", "folios":[ {
				 * "imgName":"0036_C_024_R.jpg", "noNumb":false, "folioId":1026,
				 * "folioNumber":"24", "rectoverso":"recto", "uploadFileId":1497 } ] }
				 */
			}

		}
		return doc;

	}

	private Boolean isAbleToViewDoc(MiaDocumentEntity docEntity,
			UserDetails currentUser) {

		if (currentUser != null) {
			if (docEntity.getCreatedBy().equals(currentUser.getUsername())) {
				return true;
			} else {
				if (DocumentPrivacyUtils.isUserAdmin(currentUser)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public List<DocumentFieldEntity> findDocumentFields()
			throws ApplicationThrowable {
		try {

			return getDocumentFieldDao().getDocumentFields();

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}
	
	@Override
	public Integer countDocumentsByUploadFileId(Integer uploadFileId) throws ApplicationThrowable {
		return genericDocumentDao.countDocumentsByUploadFileId(uploadFileId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<DocumentJson> findDocumentsByUploadFileId(Integer uploadFileId)
			throws ApplicationThrowable {

		try {

			List<DocumentJson> list = new ArrayList<DocumentJson>();
			List<Integer> docsId = getDocumentDAO().findDocsIdByUploadFileId(
					uploadFileId);
			if (docsId != null) {
				for (Integer docId : docsId) {
					DocumentJson findDocument = this.findDocumentById(docId);
					list.add(findDocument);
				}
			}
			return list;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<DocumentJson> findDocuments(boolean isOwner)
			throws ApplicationThrowable {

		try {

			List<DocumentJson> list = new ArrayList<DocumentJson>();
			List<MiaDocumentEntity> docs = getDocumentDAO().findDocuments();
			if (docs != null) {
				for (MiaDocumentEntity docEntity : docs) {
					if (isOwner) {
						// Retrieving the Username
						User user = getUserService().findUser(
								((UserDetails) SecurityContextHolder
										.getContext().getAuthentication()
										.getPrincipal()).getUsername());
						if (user.getAccount().equals(docEntity.getCreatedBy())) {
							list.add(getDocumentJson(docEntity));
						}
					} else {
						list.add(getDocumentJson(docEntity));
					}
				}
			}
			return list;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	public List<DocumentJson> findDocuments(boolean isOwner, Integer page,
			Integer perPage) throws ApplicationThrowable {

		try {
			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());
			List<DocumentJson> list = new ArrayList<DocumentJson>();
			List<MiaDocumentEntity> docs = getDocumentDAO()
					.findUserOwnDocuments(user, page, perPage);
			if (docs != null && !docs.isEmpty()) {
				for (MiaDocumentEntity doc : docs) {
					list.add(getDocumentJson(doc));
				}
			}
			return list;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<DocumentJson> findNotDeletedDocuments()
			throws ApplicationThrowable {

		try {

			List<DocumentJson> list = new ArrayList<DocumentJson>();
			List<MiaDocumentEntity> docs = getDocumentDAO().findDocuments();
			if (docs != null && !docs.isEmpty()) {
				for (MiaDocumentEntity docEntity : docs) {
					if (docEntity.getFlgLogicalDelete() == null
							|| !docEntity.getFlgLogicalDelete()) {
						list.add(getDocumentJson(docEntity));
					}
				}
			}
			return list;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<LanguageJson> findLanguages() throws ApplicationThrowable {

		try {

			List<LanguageJson> list = new ArrayList<LanguageJson>();
			List<LanguageEntity> langs = getLanguageDAO().findLanguages();
			if (langs != null) {
				for (LanguageEntity langEnt : langs) {
					LanguageJson langJson = new LanguageJson();
					langJson.setId(langEnt.getId());
					langJson.setLanguage(langEnt.getLanguage());
					list.add(langJson);
				}
			}
			return list;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<DocumentJson> findDocumentsByUser(String userName)
			throws ApplicationThrowable {

		try {

			List<DocumentJson> list = new ArrayList<DocumentJson>();
			List<MiaDocumentEntity> docs = getDocumentDAO()
					.findDocumentsByUser(userName);
			if (docs != null) {
				for (MiaDocumentEntity doc : docs) {
					DocumentJson findDocument = this.findDocumentById(doc
							.getDocumentEntityId());
					list.add(findDocument);
				}
			}
			return list;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public CreateDEResponseJson createDE(DocumentJson document)
			throws ApplicationThrowable {

		Integer docId = null;
		CreateDEResponseJson resp = new CreateDEResponseJson();

		try {

			if (document.getCategory() == null
					|| document.getCategory().isEmpty()) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Typology is empty");
				return resp;
			}
			DocumentJson doc = DocumentFactoryJson.getDocument(document
					.getCategory());

			MiaDocumentEntity docEnt = doc.getMiaDocumentEntity(document);

			if (docEnt == null) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Unable to construct MiaDocumentEntity");
				return resp;
			}

			// Set of createdBy and dateCreated fields
			// Retrieving the Username
			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());
			String owner = user.getAccount();
			docEnt.setCreatedBy(owner);
			docEnt.setDateCreated(new Date());
			;

			List<UploadFileEntity> uploadFiles = docEnt.getUploadFileEntities();
			docEnt.setUploadFileEntities(null);

			docEnt.setPrivacy(0);
			List<People> producers = docEnt.getProducers();
			docEnt.setProducers(null);
			if (uploadFiles != null && !uploadFiles.isEmpty()) {
				for (UploadFileEntity uploadFile : uploadFiles) {
					Integer privacy = getUploadFileDAO().findFilePrivacy(uploadFile.getUploadFileId());
					
					if(privacy != null && privacy.equals(1)) {
						docEnt.setPrivacy(1);
					} 
					
					// Carica uploadFile server
					UploadFileEntity uploadFileDb = uploadFileDAO
							.find(uploadFile.getUploadFileId());
					List<String> ownerUploadList = uploadInfoDAO.findOwnerById(uploadFileDb.getIdTblUpload());
					if(ownerUploadList != null && ownerUploadList.size() >0 ) {
						if(!ownerUploadList.get(0).equals(owner) && uploadFileDb.getFilePrivacy().equals(1)) {
							resp.setMessage("Unable to create a document with a private image.");
							return resp;
						}
					}
//					if (uploadFileDb != null
//							&& uploadFileDb.getFilePrivacy() != null
//							&& uploadFileDb.getFilePrivacy().equals(1)) {
//						docEnt.setPrivacy(1);
//						break;
//					}
				}
			}

			// Set flgLogicalDelete to 0 -
			docEnt.setFlgLogicalDelete(Boolean.FALSE);

			docId = getDocumentDAO().insertDE(docEnt);

			if (uploadFiles != null && !uploadFiles.isEmpty()) {

				String foliosWarningMessage = "";

				for (UploadFileEntity uploadFile : uploadFiles) {
					// Carica uploadFile server

					for (FolioEntity folio : uploadFile.getFolioEntities()) {

						if (folio != null) {
							folio.setUploadedFileId(uploadFile
									.getUploadFileId());

							Integer folioId = getFolioDao()
									.insertOrUpdateFolio(folio);

							if (folioId != null && folioId == 0) {
								foliosWarningMessage = foliosWarningMessage
										+ "Folio of image "
										+ uploadFile.getUploadFileId()
										+ " exist and not added. ";

							}

						}

					}

					if (!foliosWarningMessage.isEmpty()) {
						resp.setStatus(StatusType.w.toString());
						resp.setMessage(foliosWarningMessage);
					}
					// Linking UploadFile and Document in
					// tblDocumentsUploadedFiles
					DocumentFileJoinEntity docuFileEntity = new DocumentFileJoinEntity(
							docId, uploadFile.getUploadFileId());
					getDocFileDao().insertDocFile(docuFileEntity);

				}

			}

			// Linking Producers and Document in tblDocumentEntsPeople
			if (producers != null && !producers.isEmpty()) {
				for (People producer : producers) {
					if (producer.getPersonId() != null && docId != null) {
						DocumentProducerJoinEntity docuProducerEntity = new DocumentProducerJoinEntity(
								docId, producer.getPersonId(),
								producer.getUnsure());
						getDocumentProducerDao().insertDocProducer(
								docuProducerEntity);
					}
				}
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		// Handling response
		if (docId != null) {
			historyLogService.registerAction(docId,
					HistoryLogEntity.UserAction.CREATE,
					HistoryLogEntity.RecordType.DE, null, null);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Document " + docId + " persisted in DB");
			resp.setDocId(docId);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Document not inserted in DB");
			return resp;
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson modifyDE(DocumentJson document)
			throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		Integer docId = null;

		try {

			if (document.getCategory() == null
					|| document.getCategory().isEmpty()) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Typology is empty");
				return resp;
			}

			DocumentJson doc = DocumentFactoryJson.getDocument(document
					.getCategory());

			MiaDocumentEntity docEntToUpdate = doc
					.getMiaDocumentEntity(document);

			if (docEntToUpdate == null) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Unable to construct MiaDocumentEntity");
				return resp;
			}
			if (docEntToUpdate.getDocumentEntityId() == null
					|| docEntToUpdate.getDocumentEntityId().equals("")) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("MiaDocumentEntity has not ID");
				return resp;
			}

			// Find document persisted to update some fixed fields
			MiaDocumentEntity docEntPersisted = getDocumentDAO()
					.findDocumentById(docEntToUpdate.getDocumentEntityId());
			docEntToUpdate.setCreatedBy(docEntPersisted.getCreatedBy());
			docEntToUpdate.setDateCreated(docEntPersisted.getDateCreated());

			// Set of lastUpdateBy and dateLastUpdate fields
			// Retrieving the Username
			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());
			String owner = user.getAccount();
			docEntToUpdate.setLastUpdateBy(owner);
			docEntToUpdate.setDateLastUpdate(new Date());

			// unlink uploadFiles
			List<UploadFileEntity> uploadFiles = docEntToUpdate
					.getUploadFileEntities();
			docEntToUpdate.setUploadFileEntities(null);

			// unlink producers
			List<People> producers = docEntToUpdate.getProducers();
			docEntToUpdate.setProducers(null);

			// unlink people referred to doc
			List<People> refToPeople = docEntToUpdate.getRefToPeople();
			docEntToUpdate.setRefToPeople(null);

			// unlink Bibliographic references from the doc
			List<BiblioRefEntity> biblioRefs = docEntToUpdate.getBiblioRefs();
			docEntToUpdate.setBiblioRefs(null);

			// unlink languages from doc
			List<LanguageEntity> languages = docEntToUpdate.getLanguages();
			docEntToUpdate.setLanguages(null);

			// unlink related documents
			List<MiaDocumentEntity> relatedDocs = docEntToUpdate
					.getRelatedDocs();
			docEntToUpdate.setRelatedDocs(null);

			// Persist and unlink topicPlaces
			getTopicPlaceDao()
					.insertTopicPlace(docEntToUpdate.getTopicPlaces());
			docEntToUpdate.setTopicPlaces(null);

			docId = getDocumentDAO().updateDE(docEntToUpdate,
					user.getAccount(), new Date());
			System.out.println(docId);

			// Update Producers and Document in tblDocumentEntsPeople
			if (producers != null && !producers.isEmpty()) {
				getDocumentProducerDao().modifyProducers(producers, docId);
			}

			// Insert or Update people referred to a Document in
			// tblDocumentEntsPeople
			if (refToPeople != null && !refToPeople.isEmpty()) {
				getDocumentRefToPeopleDao().modifyDocRefToPeople(refToPeople,
						docId);
			}

			// Insert or update related documents
			if (relatedDocs != null && !relatedDocs.isEmpty()) {
				getDocumentRelatedDocsDao().modifyDocRelatedDocs(relatedDocs,
						docId);
			}

			// Insert or update bibliographic refererences
			if (biblioRefs != null && !biblioRefs.isEmpty()) {
				getDocumentBiblioRefDao().modifyBiblioRefs(biblioRefs, docId);
			}

			// Insert or update languages
			if (languages != null && !languages.isEmpty()) {
				List<Integer> langIds = new ArrayList<Integer>();
				for (LanguageEntity language : languages) {
					langIds.add(language.getId());
				}
				getDocumentLanguageDao().modifyLanguages(langIds, docId);
			}

			// Update uploadFiles and Document in tblDocumentsUploadedFiles
			// Plus add new folios if exist
			if (uploadFiles != null && !uploadFiles.isEmpty()) {

				String foliosWarningMessage = "";
				List<DocumentFileJoinEntity> docuFiles = new ArrayList<DocumentFileJoinEntity>();
				for (UploadFileEntity uploadFile : uploadFiles) {

					for (FolioEntity folio : uploadFile.getFolioEntities()) {

						if (folio != null) {
							folio.setUploadedFileId(uploadFile
									.getUploadFileId());
							// adding new folios if exist
							Integer folioId = getFolioDao().insertFolio(folio);
							if (folioId != null && folioId == 0) {
								foliosWarningMessage = foliosWarningMessage
										+ "Folio of image "
										+ uploadFile.getUploadFileId()
										+ " exist and not added. ";
							}
						}

					}

					if (!foliosWarningMessage.isEmpty()) {
						resp.setStatus(StatusType.w.toString());
						resp.setMessage(foliosWarningMessage);
					}
					// Updating UploadFile and Document in
					// tblDocumentsUploadedFiles - part 1
					DocumentFileJoinEntity docuFileEntity = new DocumentFileJoinEntity(
							docId, uploadFile.getUploadFileId());
					docuFiles.add(docuFileEntity);
				}
				// Updating UploadFile and Document in tblDocumentsUploadedFiles
				// - part 2
				getDocFileDao().modifyDocFiles(docuFiles, docId);
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		// Handling response
		if (docId != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Document " + docId + " updated in DB");
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Document not updated in DB");
			return resp;
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<DocumentTypologyEntity> findDocumentTypologies()
			throws ApplicationThrowable {
		try {
			return getDocTypologyDAO().findAll();
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<TopicJson> findTopics() throws ApplicationThrowable {
		try {
			List<TopicList> topicList = getTopicsListDao().findTopicsListForUsers();
			List<TopicJson> topicsJson = new ArrayList<TopicJson>();
			if (topicList != null && !topicList.isEmpty()) {
				for (TopicList topic : topicList) {
					TopicJson topicJson = new TopicJson();
					topicJson.setTopicId(topic.getTopicId());
					topicJson.setTopicTitle(topic.getTopicTitle());
					topicJson.setDescription(topic.getDescription());
					topicsJson.add(topicJson);
				}
			}
			return topicsJson;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public TopicJson findTopicById(Integer topicId) throws ApplicationThrowable {
		try {
			TopicList topicList = getTopicsListDao().find(topicId);
			TopicJson topicJson = new TopicJson();
			if (topicList != null) {
				topicJson.setTopicId(topicList.getTopicId());
				topicJson.setTopicTitle(topicList.getTopicTitle());
				topicJson.setDescription(topicList.getDescription());
			}
			return topicJson;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<String> findDocumentTypologiesByCategory(String category)
			throws ApplicationThrowable {
		try {
			List<DocumentTypologyEntity> docTypeList = getDocTypologyDAO()
					.findByCategory(category);
			List<String> retList = new ArrayList<String>();
			for (DocumentTypologyEntity docType : docTypeList) {
				if (docType.getTypologyName() != null
						&& !docType.getTypologyName().isEmpty()) {
					retList.add(docType.getTypologyName());
				}
			}
			return retList;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<PeopleJson> findPeople(String peopleName)
			throws ApplicationThrowable {
		try {
			List<People> peopleList = null;

			peopleList = getPeopleDao().getPersons(peopleName);
			if (peopleList != null && !peopleList.isEmpty()) {
				List<PeopleJson> peopleJson = new ArrayList<PeopleJson>();
				for (People people : peopleList) {
					PeopleJson ppl = new PeopleJson();
					ppl.toJson(people);
					peopleJson.add(ppl);

				}

				return peopleJson;

			}

			return null;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<PeopleJson> findComplexPeople(
			PeopleComplexSearchJson searchReq,
			PeopleSearchPaginationParam pagParam) throws ApplicationThrowable {
		try {
			List<PeopleJson> peopleJson = new ArrayList<PeopleJson>();
			List<Integer> personIds = new ArrayList<Integer>();
//			List<People> peopleListFromAltname = getPeopleDao()
//					.getComplexPersonsFromAltName(searchReq, pagParam);
//
//			if (peopleListFromAltname != null
//					&& !peopleListFromAltname.isEmpty()) {
//
//				peopleJson = new ArrayList<PeopleJson>();
//				for (People person : peopleListFromAltname) {
//
//					personIds.add(person.getPersonId());
//
//					PeopleJson ppl = new PeopleJson();
//					ppl.toJson(person);
//					peopleJson.add(ppl);
//
//				}
//
//			}

			List<People> peopleListFromTblPeople = getPeopleDao()
					.getComplexPeopleFromTblPeople(searchReq, pagParam);

			if (peopleListFromTblPeople != null
					&& !peopleListFromTblPeople.isEmpty()) {

				for (People person : peopleListFromTblPeople) {

					if (!personIds.contains(person.getPersonId())) {

						PeopleJson ppl = new PeopleJson();
						ppl.toJson(person);
						peopleJson.add(ppl);
					}

				}

			}

			return peopleJson;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public PeopleJson findPeopleById(Integer peopleId)
			throws ApplicationThrowable {
		try {
			People people = getPeopleDao().find(peopleId);
			PeopleJson ppl = new PeopleJson();
			if (people != null) {
				ppl.setId(people.getPersonId());
				ppl.setMapNameLf(people.getMapNameLf());
				ppl.setGender(people.getGender());
				ppl.setActiveStart(people.getActiveStart());
				ppl.setActiveEnd(people.getActiveEnd());
				ppl.setBornYear(people.getBornYear());
				ppl.setDeathYear(people.getDeathYear());
			}
			return ppl;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public Integer insertPeople(PeopleJson peopleToAdd)
			throws ApplicationThrowable {
		try {
			if (peopleToAdd != null) {
				People people = new People();
				StringBuilder mapNameLfStr = new StringBuilder();
				
				if (!StringUtils.isNullOrEmpty(peopleToAdd.getLastName())) {
					mapNameLfStr.append(capitalize(peopleToAdd.getLastName()));
				}
				
				if (peopleToAdd.getGender() != null
						&& !Gender.X.toString().equalsIgnoreCase(
								peopleToAdd.getGender().toString())) {
					
					if (!StringUtils.isNullOrEmpty(peopleToAdd.getFirstName())){
						
						// If last name exist add comma before first name
						if (!StringUtils.isNullOrEmpty(
								peopleToAdd.getLastName())) {
							mapNameLfStr.append(", ");
						} 
						
						mapNameLfStr.append(capitalize(
								peopleToAdd.getFirstName()));
					}
				}
				
				if (peopleToAdd.getSucNum() != null
						&& !peopleToAdd.getSucNum().equals("")) {
					mapNameLfStr.append(" " + peopleToAdd.getSucNum());
				}
				if (peopleToAdd.getLastPrefix() != null
						&& !peopleToAdd.getLastPrefix().equals("")) {
					mapNameLfStr.append(" " + peopleToAdd.getLastPrefix());
				}
				people.setMapNameLf(mapNameLfStr.toString());
				people.setFirst(peopleToAdd.getFirstName());
				people.setLast(peopleToAdd.getLastName());
				people.setGender(peopleToAdd.getGender());
				people.setLastPrefix(peopleToAdd.getLastPrefix());
				people.setSucNum(peopleToAdd.getSucNum());
				people.setBornApprox(false);
				people.setBornDateBc(false);
				people.setBornPlaceUnsure(false);
				people.setDeathApprox(false);
				people.setDeathDateBc(false);
				people.setDeathPlaceUnsure(false);
				people.setLogicalDelete(false);
				people.setDateCreated(new Date());
				people.setLastUpdate(new Date());
				people.setPortrait(false);
				User user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
				people.setCreatedBy(user);
				people.setLastUpdateBy(user);
				getPeopleDao().persist(people);

				// Adding Alt name entities
				AltName altNameGiven = new AltName();
				altNameGiven.setPerson(people);
				altNameGiven.setAltName(peopleToAdd.getFirstName());
				if (peopleToAdd.getLastPrefix() != null) {
					altNameGiven.setNamePrefix(peopleToAdd.getLastPrefix());
				}
				altNameGiven.setNameType("Given");

				AltName altNameFamily = new AltName();
				altNameFamily.setPerson(people);
				altNameFamily.setAltName(peopleToAdd.getLastName());
				if (peopleToAdd.getLastPrefix() != null) {
					altNameFamily.setNamePrefix(peopleToAdd.getLastPrefix());
				}
				altNameFamily.setNameType("Family");

				AltName altNameSearchName = new AltName();
				altNameSearchName.setPerson(people);
				altNameSearchName.setAltName(peopleToAdd.getFirstName() + " "
						+ peopleToAdd.getLastName());
				if (peopleToAdd.getLastPrefix() != null) {
					altNameSearchName
							.setNamePrefix(peopleToAdd.getLastPrefix());
				}
				altNameSearchName.setNameType("SearchName");

				getAltNameDAO().persist(altNameGiven);
				getAltNameDAO().persist(altNameFamily);
				getAltNameDAO().persist(altNameSearchName);

				return people.getPersonId();
			} else {
				return null;
			}
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public Integer insertPlace(CreatePlaceJson placeToAdd)
			throws ApplicationThrowable {
		try {

			if (placeToAdd != null) {
				Place place = new Place();
				// set basic values
				// (MIA accept only place of type MAPSITE)
				place.setPlSource("MAPSITE");
				Integer geogKey = getGeoBaseService().findNewGeogKey(
						place.getPlSource());
				place.setGeogKey(geogKey);
				place.setPlaceName(placeToAdd.getPlaceName());
				place.setPlType(placeToAdd.getPlType());
				place.setPrefFlag("P");
				// find and set the Parent Place
				Place parentPlace = getPlaceDao().findPlaceById(
						placeToAdd.getPlaceParentId());
				if (parentPlace != null) {
					place.setParentPlace(parentPlace);
					place.setPlParent(parentPlace.getPlaceName());
					place.setParentType(parentPlace.getPlType());
					place.setPlParentSubjectId(parentPlace.getGeogKey());
					place.setPlParentTermId(parentPlace.getPlaceNameId());
					if (parentPlace.getParentPlace() != null) {
						place.setgParent(parentPlace.getParentPlace()
								.getPlaceName());
						place.setGpType(parentPlace.getParentPlace()
								.getPlType());
						if (parentPlace.getParentPlace().getParentPlace() != null) {
							place.setGgp(parentPlace.getParentPlace()
									.getParentPlace().getPlaceName());
							place.setGgpType(parentPlace.getParentPlace()
									.getParentPlace().getPlType());

							if (parentPlace.getParentPlace().getParentPlace()
									.getParentPlace() != null) {
								place.setGp2(parentPlace.getParentPlace()
										.getParentPlace().getParentPlace()
										.getPlaceName());
								place.setGp2Ttype(parentPlace.getParentPlace()
										.getParentPlace().getParentPlace()
										.getPlType());
							}
						}

					}

				}
				if (parentPlace != null) {
					place.setPlParentTermId(parentPlace.getPlaceNameId());
					place.setPlParentSubjectId(parentPlace.getGeogKey());
				}
				// set full names
				place.setPlaceNameFull(place.getPlaceName() + " / "
						+ place.getPlParent() + " / " + place.getgParent()
						+ " / " + place.getGgp());
				place.setPlNameFullPlType(place.getPlaceName() + " ("
						+ place.getPlType() + ") / " + place.getPlParent()
						+ " (" + place.getParentType() + ") / "
						+ place.getgParent() + " (" + place.getGpType()
						+ ") / " + place.getGgp() + " (" + place.getGgpType()
						+ ") /" + place.getGp2() + " (" + place.getGp2Ttype()
						+ ")");
				// set other values
				place.setDateEntered(new Date());
				place.setPlacesMemo(placeToAdd.getPlaceNotes());
				place.setAddlRes(false);
				place.setTermAccent(placeToAdd.getPlaceNameWithAccent());
				place.setResearcher(((MiaUserDetailsImpl) SecurityContextHolder
						.getContext().getAuthentication().getPrincipal())
						.getInitials());
				place.setDateCreated(new Date());
				place.setLastUpdate(new Date());
				place.setAddlRes(false);
				place.setLogicalDelete(Boolean.FALSE);
				User user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
				place.setCreatedBy(user);
				place.setLastUpdateBy(user);
				getPlaceDao().persist(place);
				return place.getPlaceAllId();
			} else
				return null;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<PlaceJson> findPlaces(String placeName)
			throws ApplicationThrowable {
		try {
			List<PlaceJson> listRet = new ArrayList<PlaceJson>();
			List<Place> placeList = getPlaceDao().findPlaces(placeName);
			for (Place place : placeList) {
				if (place.getLogicalDelete() == null
						|| !place.getLogicalDelete()) {
					PlaceJson plc = new PlaceJson();
					plc.setId(place.getPlaceAllId());
					plc.setPlaceNameId(place.getPlaceNameId());
					plc.setPlaceName(place.getPlaceNameFull());
					plc.setPlType(place.getPlType());
					plc.setPrefFlag(place.getPrefFlag());
					listRet.add(plc);
				}
			}
			return listRet;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public PlaceJson findPlaceById(Integer placeId) throws ApplicationThrowable {
		try {
			Place place = getPlaceDao().find(placeId);
			PlaceJson plc = new PlaceJson();
			if (place != null) {
				plc.setId(place.getPlaceAllId());
				plc.setPlaceNameId(place.getPlaceNameId());
				plc.setPlaceName(place.getPlaceNameFull());
				plc.setPlType(place.getPlType());
				plc.setPrefFlag(place.getPrefFlag());
			}
			return plc;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<FolioJson> checkFolio(Integer uploadFileId)
			throws ApplicationThrowable {
		try {

			UserDetails user = ((UserDetails) SecurityContextHolder
					.getContext().getAuthentication().getPrincipal());
			List<FolioJson> foliosRet = new ArrayList<FolioJson>();
			List<FolioEntity> folios = getFolioDao().findFoliosByUploadFileId(
					uploadFileId);
			for (FolioEntity folio : folios) {
				FolioJson fol = new FolioJson();
				fol.toJson(folio);
				if (this.isAbleToViewUploadFile(folio.getUploadedFileId(), user)) {
					fol.setCanView(true);
				} else {
					fol.setCanView(false);
					fol.setImgName(null);
					fol.setFolioNumber(null);
					fol.setRectoverso(null);
					fol.setNoNumb(null);
				}
				foliosRet.add(fol);
			}
			return foliosRet;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	private Boolean isAbleToViewUploadFile(Integer uploadFileId,
			UserDetails currentUser) {
		if (currentUser != null && uploadFileId != null) {
			if (uploadFileDAO.isOwnerOrPublic(currentUser.getUsername(),
					uploadFileId)) {
				return true;
			} else {
				if (DocumentPrivacyUtils.isUserAdmin(currentUser)
						|| DocumentPrivacyUtils.isOnsiteFellows(currentUser)) {
					return true;
				} else {
					List<FileSharing> isShared = fileSharingDao
							.findByUploadFileIdAndAccount(uploadFileId,
									currentUser.getUsername());
					if (isShared.size() > 0) {
						return true;
					}
				}
			}
		}
		return false;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<FolioResponseJson> saveFolio(List<FolioJson> folioToSave)
			throws ApplicationThrowable {
		try {
			List<FolioResponseJson> ret = null;

			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());

			List<FolioEntity> foliosToAdd = new ArrayList<FolioEntity>();

			for (FolioJson folio : folioToSave) {
				FolioEntity folioEn = new FolioEntity();
				foliosToAdd.add(folio.toEntity(folioEn));
			}

			ret = getFolioDao().insertAllFolios(foliosToAdd);

			Integer uploadInfoId = null;

			if (ret != null && !ret.isEmpty()) {

				UploadFileEntity uploadFile = getUploadFileDAO().find(
						folioToSave.get(0).getUploadFileId());

				if (uploadFile != null) {
					UploadInfoEntity upload = uploadFile.getUploadInfoEntity();
					uploadInfoId = upload.getUploadInfoId();
					upload.setModifiedBy(user.getAccount());
					upload.setDateModified(new Date());
					getUploadInfoDAO().merge(upload);
				}

			}

			HashMap<Object, Object> changes = new HashMap<Object, Object>();
			HashMap<String, HashMap> category = new HashMap<String, HashMap>();
			HashMap<String, HashMap> action = new HashMap<String, HashMap>();
			changes.put("folios", folioToSave);
			action.put("edit", changes);
			category.put("fileMetadata", action);
			if (uploadInfoId != null) {
				historyLogService.registerAction(uploadInfoId,
						HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.AE, null, category);
			}

			return ret;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public Integer deleteFolio(FolioJson folioToDelete)
			throws ApplicationThrowable {

		try {

			if (folioToDelete == null || folioToDelete.getFolioId() == null
					|| folioToDelete.getUploadFileId() == null) {
				return 0;
			}

			Integer ret = getFolioDao().deleteFolio(folioToDelete);
			if (ret == 1) {
				// Get the username logged in
				User user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());

				if (folioToDelete.getUploadFileId() != null) {
					UploadFileEntity uploadFile = getUploadFileDAO().find(
							folioToDelete.getUploadFileId());

					if (uploadFile != null) {
						UploadInfoEntity upload = uploadFile
								.getUploadInfoEntity();
						upload.setModifiedBy(user.getAccount());
						upload.setDateModified(new Date());
						getUploadInfoDAO().merge(upload);
					}
				}

			}

			return ret;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<PlaceType> findPlaceTypes() throws ApplicationThrowable {
		try {
			return getPlaceTypeDao().findPlaceTypes();
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	private void setCorrTransientField(MiaDocumentEntity docEntity) {
		if (docEntity.getCorrespondence().getRecipient() != null
				&& !docEntity.getCorrespondence().getRecipient().isEmpty()) {

			ArrayList<People> peoples = new ArrayList<People>();

			for (String peopleIdStr : (List<String>) Arrays.asList(docEntity
					.getCorrespondence().getRecipient().split(","))) {
				// retrieve people entity and Setting unsure field
				String[] peoplesIdArr = peopleIdStr.split(":");
				String peopleId = peoplesIdArr[0];
				People peopleEnt = getPeopleDao().findPeopleById(
						Integer.valueOf(peopleId));
				if (peopleEnt != null) {
					if (peoplesIdArr.length > 1) {
						peopleEnt.setUnsure(peoplesIdArr[1]);
					}
					peoples.add(peopleEnt);
				}

			}
			docEntity.getCorrespondence().setRecipients(peoples);
		}

		if (docEntity.getCorrespondence().getRecipientPlace() != null
				&& !docEntity.getCorrespondence().getRecipientPlace().isEmpty()) {

			ArrayList<Place> places = new ArrayList<Place>();

			for (String placeIdStr : (List<String>) Arrays.asList(docEntity
					.getCorrespondence().getRecipientPlace().split(","))) {
				// retrieve place entity and Setting unsure field
				String[] placesIdArr = placeIdStr.split(":");
				if (placesIdArr != null && placesIdArr.length > 0
						&& placesIdArr[0] != null
						&& !"".equalsIgnoreCase(placesIdArr[0])) {
					String placeId = placesIdArr[0];
					Place placeEnt = getPlaceDao().findPlaceById(
							Integer.valueOf(placeId));
					if (placeEnt != null) {
						if (placesIdArr.length > 1) {
							placeEnt.setUnsure(placesIdArr[1]);
						}
						places.add(placeEnt);
					}
				}

			}
			docEntity.getCorrespondence().setRecipientPlaces(places);
		}

	}

	private void setTransientFields(MiaDocumentEntity docEntity) {

		if (docEntity.getCorrespondence() != null) {
			setCorrTransientField(docEntity);
		} else if (docEntity.getNotarial() != null) {
			setNotarialTransientField(docEntity);
		} else if (docEntity.getOfficial() != null) {
			setOfficialTransientField(docEntity);
		} else if (docEntity.getInventory() != null) {
			setInventoryTransientField(docEntity);
		} else if (docEntity.getFiscal() != null) {
			setFiscalTransientField(docEntity);
		} else if (docEntity.getFinancial() != null) {
			setFinancialTransientField(docEntity);
		} else if (docEntity.getMiscellaneous() != null) {
			setMiscellaneousTransientField(docEntity);
		} else if (docEntity.getLiterary() != null) {
			setLiteraryTransientField(docEntity);
		} else if (docEntity.getNews() != null) {
			setNewsTransientField(docEntity);
		}
	}

	private void setNotarialTransientField(MiaDocumentEntity docEntity) {
		if (docEntity.getNotarial().getContractee() != null
				&& !docEntity.getNotarial().getContractee().isEmpty()) {

			ArrayList<People> contractees = new ArrayList<People>();
			for (String peopleIdStr : (List<String>) Arrays.asList(docEntity
					.getNotarial().getContractee().split(","))) {
				// retrieve people entity and Setting unsure field
				String[] peoplesIdArr = peopleIdStr.split(":");
				String peopleId = peoplesIdArr[0];
				People peopleEnt = getPeopleDao().findPeopleById(
						Integer.valueOf(peopleId));
				if (peopleEnt != null) {
					if (peoplesIdArr.length > 1) {
						peopleEnt.setUnsure(peoplesIdArr[1]);
					}
					contractees.add(peopleEnt);
				}

			}
			docEntity.getNotarial().setContractees(contractees);
		}

		if (docEntity.getNotarial().getContractor() != null
				&& !docEntity.getNotarial().getContractor().isEmpty()) {

			ArrayList<People> contractors = new ArrayList<People>();
			for (String peopleIdStr : (List<String>) Arrays.asList(docEntity
					.getNotarial().getContractor().split(","))) {
				// retrieve people entity and Setting unsure field
				String[] peoplesIdArr = peopleIdStr.split(":");
				String peopleId = peoplesIdArr[0];
				People peopleEnt = getPeopleDao().findPeopleById(
						Integer.valueOf(peopleId));
				if (peopleEnt != null) {
					if (peoplesIdArr.length > 1) {
						peopleEnt.setUnsure(peoplesIdArr[1]);
					}
					contractors.add(peopleEnt);
				}
			}
			docEntity.getNotarial().setContractors(contractors);
		}

	}

	private void setOfficialTransientField(MiaDocumentEntity docEntity) {
		if (docEntity.getOfficial().getPrinter() != null
				&& !docEntity.getOfficial().getPrinter().isEmpty()) {

			ArrayList<People> printers = new ArrayList<People>();
			for (String peopleIdStr : (List<String>) Arrays.asList(docEntity
					.getOfficial().getPrinter().split(","))) {
				String[] peoplesIdArr = peopleIdStr.split(":");
				String peopleId = peoplesIdArr[0];
				People peopleEnt = getPeopleDao().findPeopleById(
						Integer.valueOf(peopleId));
				if (peopleEnt != null) {
					if (peoplesIdArr.length > 1) {
						peopleEnt.setUnsure(peoplesIdArr[1]);
					}
					printers.add(peopleEnt);
				}

			}
			docEntity.getOfficial().setPrinters(printers);
		}

		if (docEntity.getOfficial().getPrinterPlace() != null
				&& !docEntity.getOfficial().getPrinterPlace().isEmpty()) {

			ArrayList<Place> printerPlaces = new ArrayList<Place>();
			for (String placeIdStr : (List<String>) Arrays.asList(docEntity
					.getOfficial().getPrinterPlace().split(","))) {
				String[] placesIdArr = placeIdStr.split(":");
				String placeId = placesIdArr[0];
				Place placeEnt = getPlaceDao().findPlaceById(
						Integer.valueOf(placeId));
				if (placeEnt != null) {
					if (placesIdArr.length > 1) {
						placeEnt.setUnsure(placesIdArr[1]);
					}
					printerPlaces.add(placeEnt);
				}
			}
			docEntity.getOfficial().setPrinterPlaces(printerPlaces);
		}

	}

	private void setInventoryTransientField(MiaDocumentEntity docEntity) {
		if (docEntity.getInventory().getCommissioner() != null
				&& !docEntity.getInventory().getCommissioner().isEmpty()) {

			ArrayList<People> commissioners = new ArrayList<People>();
			for (String peopleIdStr : (List<String>) Arrays.asList(docEntity
					.getInventory().getCommissioner().split(","))) {
				String[] peoplesIdArr = peopleIdStr.split(":");
				String peopleId = peoplesIdArr[0];
				People peopleEnt = getPeopleDao().findPeopleById(
						Integer.valueOf(peopleId));
				if (peopleEnt != null) {
					if (peoplesIdArr.length > 1) {
						peopleEnt.setUnsure(peoplesIdArr[1]);
					}
					commissioners.add(peopleEnt);
				}
			}
			docEntity.getInventory().setCommissioners(commissioners);
		}
	}

	private void setFiscalTransientField(MiaDocumentEntity docEntity) {
		if (docEntity.getFiscal().getGonfaloneDistrict() != null
				&& !docEntity.getFiscal().getGonfaloneDistrict().isEmpty()) {

			ArrayList<Place> gonfaloneDistricts = new ArrayList<Place>();
			for (String placeIdStr : (List<String>) Arrays.asList(docEntity
					.getFiscal().getGonfaloneDistrict().split(","))) {
				String[] placesIdArr = placeIdStr.split(":");
				String placeId = placesIdArr[0];
				Place placeEnt = getPlaceDao().findPlaceById(
						Integer.valueOf(placeId));
				if (placeEnt != null) {
					if (placesIdArr.length > 1) {
						placeEnt.setUnsure(placesIdArr[1]);
					}
					gonfaloneDistricts.add(placeEnt);
				}
			}
			docEntity.getFiscal().setGonfaloneDistricts(gonfaloneDistricts);
		}
	}

	private void setFinancialTransientField(MiaDocumentEntity docEntity) {
		if (docEntity.getFinancial().getCommissionerOfAccount() != null
				&& !docEntity.getFinancial().getCommissionerOfAccount()
						.isEmpty()) {

			ArrayList<People> commissionersOfAccount = new ArrayList<People>();
			for (String peopleIdStr : (List<String>) Arrays.asList(docEntity
					.getFinancial().getCommissionerOfAccount().split(","))) {
				String[] peoplesIdArr = peopleIdStr.split(":");
				String peopleId = peoplesIdArr[0];
				People peopleEnt = getPeopleDao().findPeopleById(
						Integer.valueOf(peopleId));
				if (peopleEnt != null) {
					if (peoplesIdArr.length > 1) {
						peopleEnt.setUnsure(peoplesIdArr[1]);
					}
					commissionersOfAccount.add(peopleEnt);
				}
			}
			docEntity.getFinancial().setCommissionersOfAccount(
					commissionersOfAccount);
		}
	}

	private void setMiscellaneousTransientField(MiaDocumentEntity docEntity) {
		if (docEntity.getMiscellaneous().getPrinter() != null
				&& !docEntity.getMiscellaneous().getPrinter().isEmpty()) {

			ArrayList<People> printers = new ArrayList<People>();
			for (String peopleIdStr : (List<String>) Arrays.asList(docEntity
					.getMiscellaneous().getPrinter().split(","))) {
				String[] peoplesIdArr = peopleIdStr.split(":");
				String peopleId = peoplesIdArr[0];
				People peopleEnt = getPeopleDao().findPeopleById(
						Integer.valueOf(peopleId));
				if (peopleEnt != null) {
					if (peoplesIdArr.length > 1) {
						peopleEnt.setUnsure(peoplesIdArr[1]);
					}
					printers.add(peopleEnt);
				}

			}
			docEntity.getMiscellaneous().setPrinters(printers);
		}

		if (docEntity.getMiscellaneous().getPrinterPlace() != null
				&& !docEntity.getMiscellaneous().getPrinterPlace().isEmpty()) {

			ArrayList<Place> printerPlaces = new ArrayList<Place>();
			for (String placeIdStr : (List<String>) Arrays.asList(docEntity
					.getMiscellaneous().getPrinterPlace().split(","))) {
				String[] placesIdArr = placeIdStr.split(":");
				String placeId = placesIdArr[0];
				Place placeEnt = getPlaceDao().findPlaceById(
						Integer.valueOf(placeId));
				if (placeEnt != null) {
					if (placesIdArr.length > 1) {
						placeEnt.setUnsure(placesIdArr[1]);
					}
					printerPlaces.add(placeEnt);
				}

			}
			docEntity.getMiscellaneous().setPrinterPlaces(printerPlaces);
		}
	}

	private void setLiteraryTransientField(MiaDocumentEntity docEntity) {
		if (docEntity.getLiterary().getDedicatee() != null
				&& !docEntity.getLiterary().getDedicatee().isEmpty()) {

			ArrayList<People> dedicatees = new ArrayList<People>();
			for (String peopleIdStr : (List<String>) Arrays.asList(docEntity
					.getLiterary().getDedicatee().split(","))) {
				String[] peoplesIdArr = peopleIdStr.split(":");
				String peopleId = peoplesIdArr[0];
				People peopleEnt = getPeopleDao().findPeopleById(
						Integer.valueOf(peopleId));
				if (peopleEnt != null) {
					if (peoplesIdArr.length > 1) {
						peopleEnt.setUnsure(peoplesIdArr[1]);
					}
					dedicatees.add(peopleEnt);
				}
			}
			docEntity.getLiterary().setDedicatees(dedicatees);
		}

		if (docEntity.getLiterary().getPrinter() != null
				&& !docEntity.getLiterary().getPrinter().isEmpty()) {

			ArrayList<People> printers = new ArrayList<People>();
			for (String peopleIdStr : (List<String>) Arrays.asList(docEntity
					.getLiterary().getPrinter().split(","))) {
				String[] peoplesIdArr = peopleIdStr.split(":");
				String peopleId = peoplesIdArr[0];
				People peopleEnt = getPeopleDao().findPeopleById(
						Integer.valueOf(peopleId));
				if (peopleEnt != null) {
					if (peoplesIdArr.length > 1) {
						peopleEnt.setUnsure(peoplesIdArr[1]);
					}
					printers.add(peopleEnt);
				}

			}
			docEntity.getLiterary().setPrinters(printers);
		}

		if (docEntity.getLiterary().getPrinterPlace() != null
				&& !docEntity.getLiterary().getPrinterPlace().isEmpty()) {

			ArrayList<Place> printerPlaces = new ArrayList<Place>();
			for (String placeIdStr : (List<String>) Arrays.asList(docEntity
					.getLiterary().getPrinterPlace().split(","))) {
				String[] placesIdArr = placeIdStr.split(":");
				String placeId = placesIdArr[0];
				Place placeEnt = getPlaceDao().findPlaceById(
						Integer.valueOf(placeId));
				if (placeEnt != null) {
					if (placesIdArr.length > 1) {
						placeEnt.setUnsure(placesIdArr[1]);
					}
					printerPlaces.add(placeEnt);
				}

			}
			docEntity.getLiterary().setPrinterPlaces(printerPlaces);
		}
	}

	private void setNewsTransientField(MiaDocumentEntity docEntity) {
		if (docEntity.getNews().getNewsFrom() != null
				&& !docEntity.getNews().getNewsFrom().isEmpty()) {

			ArrayList<Place> newsFroms = new ArrayList<Place>();
			for (String placeIdStr : (List<String>) Arrays.asList(docEntity
					.getNews().getNewsFrom().split(","))) {
				String[] placesIdArr = placeIdStr.split(":");
				String placeId = placesIdArr[0];
				Place placeEnt = getPlaceDao().findPlaceById(
						Integer.valueOf(placeId));
				if (placeEnt != null) {
					if (placesIdArr.length > 1) {
						placeEnt.setUnsure(placesIdArr[1]);
					}
					newsFroms.add(placeEnt);
				}

			}
			docEntity.getNews().setNewsFroms(newsFroms);
		}

		if (docEntity.getNews().getPrinter() != null
				&& !docEntity.getNews().getPrinter().isEmpty()) {

			ArrayList<People> printers = new ArrayList<People>();
			for (String peopleIdStr : (List<String>) Arrays.asList(docEntity
					.getNews().getPrinter().split(","))) {
				String[] peoplesIdArr = peopleIdStr.split(":");
				String peopleId = peoplesIdArr[0];
				People peopleEnt = getPeopleDao().findPeopleById(
						Integer.valueOf(peopleId));
				if (peopleEnt != null) {
					if (peoplesIdArr.length > 1) {
						peopleEnt.setUnsure(peoplesIdArr[1]);
					}
					printers.add(peopleEnt);
				}

			}
			docEntity.getNews().setPrinters(printers);
		}

		if (docEntity.getNews().getPrinterPlace() != null
				&& !docEntity.getNews().getPrinterPlace().isEmpty()) {

			ArrayList<Place> printerPlaces = new ArrayList<Place>();
			for (String placeIdStr : (List<String>) Arrays.asList(docEntity
					.getNews().getPrinterPlace().split(","))) {
				String[] placesIdArr = placeIdStr.split(":");
				String placeId = placesIdArr[0];
				Place placeEnt = getPlaceDao().findPlaceById(
						Integer.valueOf(placeId));
				if (placeEnt != null) {
					if (placesIdArr.length > 1) {
						placeEnt.setUnsure(placesIdArr[1]);
					}
					printerPlaces.add(placeEnt);
				}

			}
			docEntity.getNews().setPrinterPlaces(printerPlaces);
		}
	}

	private void setProducerUnsureField(MiaDocumentEntity docEntity) {

		for (People producer : docEntity.getProducers()) {
			DocumentProducerJoinEntity docProducerJoinEntity = getDocumentProducerDao()
					.findDocProducer(docEntity.getDocumentEntityId(),
							producer.getPersonId());

			producer.setUnsure(docProducerJoinEntity.getProducerUnsure());
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson saveSynopsis(String generalNotesSynopsis,
			Integer documententId) throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Uncaught error");
		try {
			MiaDocumentEntity docentity = getDocumentDAO().find(documententId);
			if (docentity == null) {
				resp.setMessage("No document, associated with given documentId, found. ");
			} else {

				getDocumentDAO().saveSynopsis(generalNotesSynopsis, docentity);
				resp.setStatus(StatusType.ok.toString());
				resp.setMessage(null);

			}
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	@Override
	public MiaDocumentEntity findDocumentEntityById(Integer documentId)
			throws ApplicationThrowable {

		try {
			return getDocumentDAO().findDocumentById(documentId);

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseDataJson saveOrModifyTranscriptions(
			List<DocumentTranscriptionJson> transcriptionsJson)
			throws ApplicationThrowable {

		GenericResponseDataJson resp = new GenericResponseDataJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Uncaught error");

		if (transcriptionsJson == null || transcriptionsJson.isEmpty()) {

			resp.setStatus(StatusType.w.toString());
			resp.setMessage("The transcription list received by Fe is null or empty.");
			return resp;
		}

		String documentId = transcriptionsJson.get(0).getDocumentId();
		if (documentId == null || documentId.isEmpty()) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("The documentId received by FE may be null or empty.");
			return resp;
		}

		try {

			MiaDocumentEntity docentity = getDocumentDAO().find(
					Integer.valueOf(documentId));

			if (docentity == null) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("No document, associated with given documentId, found in DB.");
				return resp;
			}

			boolean isModify = false;
			String documentTranscriptionId = transcriptionsJson.get(0)
					.getDocTranscriptionId();
			if (documentTranscriptionId != null
					&& !documentTranscriptionId.isEmpty()) {
				isModify = true;
			}
			Boolean allowMultipleTranscriptions = Boolean
					.valueOf(ApplicationPropertyManager.getApplicationProperty("folio.transcription.allowMultiple"));

			ArrayList<DocumentTranscriptionEntity> docTranscriptionEntities = new ArrayList<DocumentTranscriptionEntity>();
			for (DocumentTranscriptionJson transJson : transcriptionsJson) {

				DocumentTranscriptionEntity entity = new DocumentTranscriptionEntity();
				entity.setTranscription(transJson.getTranscription());

				Integer uploadedFileId = null;
				if (transJson.getUploadedFileId() != null) {
					uploadedFileId = Integer.valueOf(transJson.getUploadedFileId());
					entity.setUploadedFileId(uploadedFileId);
				}
				Integer documentEntityId = null;
				if (transJson.getDocumentId() != null) {
					documentEntityId = Integer.valueOf(transJson.getDocumentId());
					entity.setDocumentEntityId(documentEntityId);
				}
				Integer docTranscriptionId = null;
				if (transJson.getDocTranscriptionId() != null
						&& !transJson.getDocTranscriptionId().equalsIgnoreCase("")) {
					docTranscriptionId = Integer.valueOf(transJson.getDocTranscriptionId());
					entity.setDocTranscriptionId(docTranscriptionId);
				}
				if (docTranscriptionId == null && !Boolean.TRUE.equals(allowMultipleTranscriptions)) {
					try {
						List<IGenericEntity> listTrasc = getDocumentTranscriptionDAO()
								.findExistingEntities(documentEntityId, uploadedFileId);
						if (listTrasc != null && listTrasc.size() > 0) {
							DocumentTranscriptionEntity documentTranscriptionEntity = (DocumentTranscriptionEntity) listTrasc
									.get(0);
							DocumentTranscriptionJson json = new DocumentTranscriptionJson();
							json.toJson(documentTranscriptionEntity);
							resp.setData(Arrays.asList(json));
							resp.setStatus(StatusType.w.toString());
							resp.setMessage("The transcription already exists in DB.");
							return resp;
						}
					} catch (NoResultException e) {
						// No document exists
					}
				}
				
				docTranscriptionEntities.add(entity);

			}

			getDocumentTranscriptionDAO().saveOrModifyTranscriptions(
					docTranscriptionEntities, isModify);
			// Modified by AL 06052018 Issue:432
			int i = 0;
			for (DocumentTranscriptionJson transJson : transcriptionsJson) {
				transJson
						.setDocTranscriptionId(docTranscriptionEntities.get(i)
								.getDocTranscriptionId() != null ? docTranscriptionEntities
								.get(i).getDocTranscriptionId().toString()
								: null);
				i++;
			}
			// /
			resp.setStatus(StatusType.ok.toString());

			// Get the username logged in
			UserDetails currentUser = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();

			if (!currentUser.getUsername().equals(docentity.getCreatedBy())
					&& !DocumentPrivacyUtils.isUserAdmin(currentUser)
					&& !DocumentPrivacyUtils.isOnsiteFellows(currentUser)) {
				messageService.onDocumentEdited(currentUser.getUsername(), docentity.getCreatedBy(),
						docentity.getDocumentEntityId());
			}
			getDocumentDAO().setDocumentModInf(Integer.valueOf(documentId), currentUser.getUsername());

			List<DocumentTranscriptionJson> jsons = new ArrayList<DocumentTranscriptionJson>();
			for (DocumentTranscriptionEntity tr : docTranscriptionEntities) {
				DocumentTranscriptionJson json = new DocumentTranscriptionJson();
				json.toJson(tr);
				jsons.add(json);
			}

			resp.setData(jsons);

			if (isModify) {
				resp.setMessage("The transciptions modified in DB.");
			} else {
				resp.setMessage("The transciptions added in DB.");
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
		return resp;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson saveTranscription(String transcription,
			Integer documententId) throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Uncaught error");
		try {
			MiaDocumentEntity docentity = getDocumentDAO().find(documententId);
			if (docentity == null) {
				resp.setMessage("No document, associated with given documentId, found. ");
			} else {
				DocumentTranscriptionEntity entity = new DocumentTranscriptionEntity();
				entity.setTranscription(transcription);
				entity.setDocumentEntityId(documententId);
				getDocumentTranscriptionDAO().saveTranscription(entity);
				resp.setStatus(StatusType.ok.toString());
				resp.setMessage(null);

			}
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson saveTranscription(String transcription,
			Integer documententId, Integer uploadedFileId)
			throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Uncaught error");
		try {
			MiaDocumentEntity docentity = getDocumentDAO().find(documententId);
			if (docentity == null) {
				resp.setMessage("No document, associated with given documentId, found. ");
			} else {
				DocumentTranscriptionEntity entity = new DocumentTranscriptionEntity();
				entity.setTranscription(transcription);
				entity.setDocumentEntityId(documententId);
				entity.setUploadedFileId(uploadedFileId);
				getDocumentTranscriptionDAO().saveTranscription(entity);
				resp.setStatus(StatusType.ok.toString());
				resp.setMessage(null);

			}
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	@Override
	public GenericResponseJson deleteDE(Integer documentEntityId) {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Uncaught error");
		if (documentEntityId == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("documentEntityId is null.");
			return resp;
		}

		try {

			// Get the username logged in
			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());
			String owner = user.getAccount();
			if (owner == null) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("Document not deleted. Owner account from session in null.");
				return resp;
			}
			// ISSUE 168 - controll if user is administrator or staf
			boolean isAutorisedUser = getUserService()
					.isAccountAutorizedForDelete(owner);
			Integer deleteDe = getDocumentDAO().deleteDE(documentEntityId,
					owner, isAutorisedUser);
			if (deleteDe == null) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("No Document found or Document was deleted befor.");
				return resp;

			}
			if (deleteDe == 0) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("Document not deleted. The user is not the owner of document.");
				return resp;
			}

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Document deleted.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	@Override
	public GenericResponseJson unDeleteDE(Integer documentEntityId)
			throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Uncaught error");
		if (documentEntityId == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("documentEntityId is null.");
			return resp;
		}

		try {

			// Get the username logged in
			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());
			String owner = user.getAccount();
			if (owner == null) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("Document not deleted. Owner account from session in null.");
				return resp;
			}

			MiaDocumentEntity doc;
			try {
				doc = getDocumentDAO().findDocumentById(documentEntityId);
			} catch (NoResultException e) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("No Document found.");
				return resp;
			}

			doc.setLastUpdateBy(owner);
			doc.setFlgLogicalDelete(Boolean.FALSE);
			doc.setDateLastUpdate(new Date());

			getDocumentDAO().merge(doc);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Document undeleted.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public PlaceJson findPrinicipalPlaceByPlaceId(Integer placeId)
			throws ApplicationThrowable {
		try {
			Place place = getPlaceDao().findPrinicipalPlaceByPlaceId(placeId);
			PlaceJson plc = new PlaceJson();
			if (place != null) {
				plc.setId(place.getPlaceAllId());
				plc.setPlaceNameId(place.getPlaceNameId());
				plc.setPlaceName(place.getPlaceNameFull());
				plc.setPlType(place.getPlType());
				plc.setPrefFlag(place.getPrefFlag());
			}
			return plc;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public Integer findPrinicipalPlaceIdByVariantPlaceId(Integer placeId)
			throws ApplicationThrowable {
		try {
			Place place = getPlaceDao().findPrinicipalPlaceByPlaceId(placeId);

			if (place != null) {
				return place.getPlaceAllId();
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return null;
	}

	@Transactional(readOnly = false)
	@Override
	public void associateImagesDocuments() throws ApplicationThrowable {
		try {
			getDocumentDAO().associateImagesDocuments();
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}
	
	@Transactional(readOnly = false)
	@Override
	public void updateDocumentsPrivacy() throws ApplicationThrowable {
		try {
			getDocumentDAO().updateDocumentsPrivacy();
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}
	
	@Transactional(readOnly = false)
	@Override
	public void removePrivateImagesFromDocuments() throws ApplicationThrowable {
		try {
			getDocumentDAO().removePrivateImagesFromDocuments();
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Override
	public DocumentPeopleJson getDocumentsPeople(Integer personId)
			throws ApplicationThrowable {

		DocumentPeopleJson docPeopleJson = new DocumentPeopleJson();
		docPeopleJson.setPersonId(personId);
		List<DocumentPeopleFieldsJson> docPeopleList = new ArrayList<DocumentPeopleFieldsJson>();
		try {

			List<MiaDocumentEntity> documentEnts = getDocumentDAO()
					.findDocuments();

			if (documentEnts == null || documentEnts.isEmpty()) {
				return docPeopleJson;
			}

			List<DocumentFieldEntity> documentFields = getDocumentFieldDao()
					.getDocumentFields();

			for (MiaDocumentEntity docEnt : documentEnts) {

				DocumentJson docJson = DocumentFactoryJson.getDocument(docEnt
						.getCategory());

				List<DocPeopleFieldJson> fields = docJson.getDocPeopleFields(
						personId, docEnt, documentFields);

				if (fields != null && !fields.isEmpty()) {

					DocumentPeopleFieldsJson docPeople = new DocumentPeopleFieldsJson();
					docPeople.setFields(fields);
					docPeople.setCategory(docEnt.getCategory());
					docPeople.setTypology(docEnt.getTypology());
					docPeople.setDocumentId(docEnt.getDocumentEntityId());
					docPeople.setDeTitle(docEnt.getDeTitle());
					if (docEnt.getDateCreated() != null) {
						docPeople.setDateCreated(docEnt.getDateCreated()
								.toString());
					}
					if (docEnt.getDateLastUpdate() != null) {
						docPeople.setDateLastUpdate(docEnt.getDateLastUpdate()
								.toString());
					}

					docPeopleList.add(docPeople);
				}

			}
			docPeopleJson.setDocPeople(docPeopleList);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return docPeopleJson;
	}

	@Override
	public Map<String, Map<String, Integer>> getDocumentsCountPeople(
			Integer personId) throws ApplicationThrowable {

		DocumentPeopleJson docPeopleJson = getDocumentsPeople(personId);
		if (docPeopleJson == null || docPeopleJson.getDocPeople() == null
				|| docPeopleJson.getDocPeople().isEmpty()) {
			return null;
		}

		Map<String, Map<String, Integer>> resMap = new HashMap<String, Map<String, Integer>>();

		try {

			for (DocumentPeopleFieldsJson json : docPeopleJson.getDocPeople()) {

				if (!resMap.containsKey(json.getCategory())) {
					resMap.put(json.getCategory(),
							new HashMap<String, Integer>());
				}

				Map<String, Integer> fieldMap = resMap.get(json.getCategory());

				if (json.getFields() != null && !json.getFields().isEmpty()) {

					for (DocPeopleFieldJson fieldJson : json.getFields()) {
						Integer count = 0;
						if (fieldMap.containsKey(fieldJson.getFieldName())) {
							count = fieldMap.get(fieldJson.getFieldName());
						}
						fieldMap.put(fieldJson.getFieldName(), count + 1);
					}

					resMap.put(json.getCategory(), fieldMap);
				}
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resMap;
	}

	@Override
	public List<DocumentJson> createDocumentsJsonFromDocEntity(
			List<MiaDocumentEntity> docs) throws ApplicationThrowable {

		if (docs == null || docs.isEmpty()) {
			return null;
		}

		try {
			List<DocumentJson> list = new ArrayList<DocumentJson>();
			for (MiaDocumentEntity docEntity : docs) {
				list.add(getDocumentJson(docEntity));
			}

			return list;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Override
	public List<PlaceJson> getPrinciplaPlaces(List<PlaceJson> places)
			throws ApplicationThrowable {

		if (places == null || places.isEmpty())
			return places;
		List<PlaceJson> jsons = new ArrayList<PlaceJson>();
		for (PlaceJson placeJson : places) {
			if (!"P".equalsIgnoreCase(placeJson.getPrefFlag())) {
				Place p = getPlaceDao().findPrinicipalPlaceByPlaceId(
						placeJson.getId());
				if (p != null) {
					PlaceJson json = new PlaceJson();
					p.setUnsure(placeJson.getUnsure());
					json.toJson(p);
					jsons.add(json);
				}
			} else {
				jsons.add(placeJson);
			}

		}

		return jsons;

	}

	@Override
	public List<TopicPlaceJson> getPrinciplaTopicPlaces(
			List<TopicPlaceJson> topicPlaces) throws ApplicationThrowable {

		if (topicPlaces == null || topicPlaces.isEmpty())
			return topicPlaces;
		List<TopicPlaceJson> jsons = new ArrayList<TopicPlaceJson>();
		for (TopicPlaceJson topicPlaceJson : topicPlaces) {
			if (!"P".equalsIgnoreCase(topicPlaceJson.getPrefFlag())) {
				Place p = getPlaceDao().findPrinicipalPlaceByPlaceId(
						topicPlaceJson.getPlaceId());
				if (p != null) {
					topicPlaceJson.toJson(p);
				}
			}
			jsons.add(topicPlaceJson);

		}

		return jsons;

	}

	@Override
	public List<Integer> findDocumentIdsByPeople(List<String> people)
			throws ApplicationThrowable {

		return getDocumentDAO().finDocumentsByPeople(
				NativeQueryUtils.findDocumentsByPeopleQuery(people));

	}

	@Override
	public List<Integer> findDocumentIdsByPlaces(List<String> places)
			throws ApplicationThrowable {

		return getDocumentDAO().finDocumentsByPlaces(
				NativeQueryUtils.findDocumentsByPlacesQuery(places));

	}

	@Override
	public GenericResponseJson modifyImages(ModifyImageJson modifyImageJson)
			throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Uncaught error");
		if (modifyImageJson == null || modifyImageJson.getDocumentId() == null
				|| modifyImageJson.getModifyImagesInDe() == null
				|| modifyImageJson.getModifyImagesInDe().isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("documentEntityId  is null.");
			return resp;
		}

		try {

			Integer ret = getDocumentFileDao().modifyDocFiles(modifyImageJson);

			if (ret == 0) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("No Images added to document.");
				return resp;
			}

			String transToRemove = getDocumentTranscriptionDAO()
					.getDocTransFilesToRemove(modifyImageJson);

			if (transToRemove != null && !transToRemove.isEmpty()) {

				MiaDocumentEntity doc = getGenericDocumentDao()
						.findDocumentById(modifyImageJson.getDocumentId());

				if (doc != null) {
					doc.setTransSearch(transToRemove);
					getGenericDocumentDao().merge(doc);
				}
			}

			HashMap<Object, Object> changes = new HashMap<Object, Object>();
			HashMap<String, HashMap> category = new HashMap<String, HashMap>();
			HashMap<String, HashMap> action = new HashMap<String, HashMap>();
			changes.put("images", modifyImageJson.getModifyImagesInDe());
			action.put("add", changes);
			category.put("documentImages", action);

			historyLogService.registerAction(modifyImageJson.getDocumentId(),
					HistoryLogEntity.UserAction.EDIT,
					HistoryLogEntity.RecordType.DE, null, category);

			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());

			getDocumentDAO().setDocumentModInf(modifyImageJson.getDocumentId(),
					user.getAccount());

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage(ret + " images added to Document.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;
	}

	@Override
	public List<PeopleBaseJson> findPeopleRelatedtoDocument(Integer documentId)
			throws ApplicationThrowable {

		try {
			List<DocumentRefToPeopleEntity> docs = getDocumentRefToPeopleDao()
					.findDocRefToPeople(documentId);

			if (docs == null || docs.isEmpty()) {
				return null;
			}

			List<PeopleBaseJson> people = new ArrayList<PeopleBaseJson>();
			for (DocumentRefToPeopleEntity entity : docs) {

				PeopleBaseJson personJson = new PeopleBaseJson();
				personJson.toJson(entity);
				people.add(personJson);

			}

			return people;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	public GenericResponseJson addPeopleRelatedtoDocument(
			PeopleRelatedToDocumentJson json) throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Uncaught error");
		if (json == null || json.getDocumentId() == null
				|| json.getNewPersonCitedInDoc() == null) {

			return resp;
		}

		try {

			DocumentRefToPeopleEntity entity = new DocumentRefToPeopleEntity();
			entity.setDocumentId(json.getDocumentId());
			entity.setPeopleId(json.getNewPersonCitedInDoc().getId());
			entity.setUnSure(json.getNewPersonCitedInDoc().getUnsure());

			Integer ret = getDocumentRefToPeopleDao().insertDocRefToPeople(
					entity);

			if (ret == 0) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("This person is already indexed in this document.");
				return resp;
			}
			
			// Get the username logged in
			UserDetails currentUser = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			MiaDocumentEntity doc = getGenericDocumentDao().findDocumentById(Integer.valueOf(json.getDocumentId()));
			if (!currentUser.getUsername().equals(doc.getCreatedBy()) && !DocumentPrivacyUtils.isUserAdmin(currentUser)
					&& !DocumentPrivacyUtils.isOnsiteFellows(currentUser)) {
				messageService.onDocumentEdited(currentUser.getUsername(), doc.getCreatedBy(),
						doc.getDocumentEntityId());
			}
			getDocumentDao().setDocumentModInf(Integer.valueOf(json.getDocumentId()), currentUser.getUsername());

			HashMap<Object, Object> changes = new HashMap<Object, Object>();
			HashMap<String, HashMap> category = new HashMap<String, HashMap>();
			HashMap<String, HashMap> action = new HashMap<String, HashMap>();
			changes.put(
					"personName",
					biographicalPeopleService.findBiographicalPeople(
							json.getNewPersonCitedInDoc().getId())
							.getMapNameLf());
			changes.put("type", "related");
			changes.put("unsure", json.getNewPersonCitedInDoc().getUnsure());
			action.put("add", changes);

			category.put("documentPeople", action);

			historyLogService.registerAction(
					Integer.valueOf(json.getDocumentId()),
					HistoryLogEntity.UserAction.EDIT,
					HistoryLogEntity.RecordType.DE, null, category);

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage(ret + " PeopleRelatedtoDocument added.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	public GenericResponseJson deletePeopleRelatedtoDocument(
			PeopleRelatedToDocumentJson json) throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Uncaught error");
		if (json == null || json.getDocumentId() == null
				|| json.getDeletePersonCitedInDoc() == null) {

			return resp;
		}

		try {

			Integer ret = getDocumentRefToPeopleDao().deleteDocRefToPeople(
					json.getDocumentId(), json.getDeletePersonCitedInDoc());

			if (ret == 0) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("PeopleRelatedtoDocument doesn't exist in DB. No entity deleted.");
				return resp;
			}

			// Get the username logged in
			UserDetails currentUser = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			MiaDocumentEntity doc = getGenericDocumentDao().findDocumentById(Integer.valueOf(json.getDocumentId()));
			if (!currentUser.getUsername().equals(doc.getCreatedBy()) && !DocumentPrivacyUtils.isUserAdmin(currentUser)
					&& !DocumentPrivacyUtils.isOnsiteFellows(currentUser)) {
				messageService.onDocumentEdited(currentUser.getUsername(), doc.getCreatedBy(),
						doc.getDocumentEntityId());
			}
			getDocumentDao().setDocumentModInf(Integer.valueOf(json.getDocumentId()), currentUser.getUsername());

			HashMap<Object, Object> changes = new HashMap<Object, Object>();
			HashMap<String, HashMap> category = new HashMap<String, HashMap>();
			HashMap<String, HashMap> action = new HashMap<String, HashMap>();
			changes.put("personName", biographicalPeopleService
					.findBiographicalPeople(json.getDeletePersonCitedInDoc())
					.getMapNameLf());
			changes.put("type", "related");
			action.put("delete", changes);

			category.put("documentPeople", action);

			historyLogService.registerAction(json.getDocumentId(),
					HistoryLogEntity.UserAction.EDIT,
					HistoryLogEntity.RecordType.DE, null, category);

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage(" PeopleRelatedtoDocument deleted.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	public GenericResponseJson modifyPeopleRelatedtoDocument(
			PeopleRelatedToDocumentJson json) throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		resp.setMessage("Uncaught error");
		if (json == null || json.getDocumentId() == null
				|| json.getNewPersonCitedInDoc() == null
				|| json.getOldPersonCitedInDoc() == null) {

			return resp;
		}

		try {

			DocumentRefToPeopleEntity entity = new DocumentRefToPeopleEntity();
			entity.setDocumentId(json.getDocumentId());
			entity.setPeopleId(json.getNewPersonCitedInDoc().getId());
			entity.setUnSure(json.getNewPersonCitedInDoc().getUnsure());

			Integer ret = getDocumentRefToPeopleDao()
					.modifyDocRefToPeople(json);

			if (ret == 0) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("This old person for modification doen't exist in DB or New Person to be adde is already exist in DB.");
				return resp;
			}

			// Get the username logged in
			UserDetails currentUser = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			MiaDocumentEntity doc = getGenericDocumentDao().findDocumentById(Integer.valueOf(json.getDocumentId()));
			if (!currentUser.getUsername().equals(doc.getCreatedBy()) && !DocumentPrivacyUtils.isUserAdmin(currentUser)
					&& !DocumentPrivacyUtils.isOnsiteFellows(currentUser)) {
				messageService.onDocumentEdited(currentUser.getUsername(), doc.getCreatedBy(),
						doc.getDocumentEntityId());
			}
			getDocumentDao().setDocumentModInf(Integer.valueOf(json.getDocumentId()), currentUser.getUsername());

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage(ret + " PeopleRelatedtoDocument modified.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	public List<DocumentTranscriptionEntity> getDocumentTranscriptions(
			Integer documentId) throws ApplicationThrowable {
		try {
			ArrayList<DocumentTranscriptionEntity> ret = new ArrayList<DocumentTranscriptionEntity>();
			List<IGenericEntity> list = documentTranscriptionDAO
					.findEntities(documentId);
			for (Iterator iterator = list.iterator(); iterator.hasNext();) {
				DocumentTranscriptionEntity entity = (DocumentTranscriptionEntity) iterator
						.next();
				ret.add(entity);
			}
			return ret;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	public Integer updateTransSearch(Integer documentId)
			throws ApplicationThrowable {
		try {

			if (documentId == null)
				return -1;

			List<IGenericEntity> list = documentTranscriptionDAO
					.findEntities(documentId);
			if (list == null || list.isEmpty()) {
				return 0;
			}
			String transSearch = "";
			for (IGenericEntity docTrans : list) {
				if (((DocumentTranscriptionEntity) docTrans).getTranscription() != null
						&& !((DocumentTranscriptionEntity) docTrans)
								.getTranscription().isEmpty()) {
					transSearch = transSearch
							+ " - "
							+ ((DocumentTranscriptionEntity) docTrans)
									.getTranscription();
				}
			}

			return getDocumentDao().setModifiedTransSearch(documentId,
					transSearch);
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	public Integer updateAllTransSearch() throws ApplicationThrowable {
		try {

			List<DocumentTranscriptionEntity> list = documentTranscriptionDAO
					.findAllDocTrans();

			if (list == null || list.isEmpty()) {
				return 0;
			}

			Map<Integer, String> docTrans = new HashMap<Integer, String>();

			for (DocumentTranscriptionEntity dt : list) {
				if (docTrans.containsKey(dt.getDocumentEntityId())) {
					// String transSearch =
					// docTrans.get(dt.getDocumentEntityId())
					// + " - " + dt.getTranscription();
					docTrans.put(
							dt.getDocumentEntityId(),
							docTrans.get(dt.getDocumentEntityId()) + " - "
									+ dt.getTranscription());

				} else {
					docTrans.put(dt.getDocumentEntityId(),
							dt.getTranscription());
				}

			}

			for (Integer documentId : docTrans.keySet()) {
				getDocumentDao().setModifiedTransSearch(documentId,
						docTrans.get(documentId));
			}

			return 1;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<DocumentJson> findDocumentsByPlaceOforigin(Integer placeOfOrigin)
			throws ApplicationThrowable {

		try {

			List<MiaDocumentEntity> docsEnt = getDocumentDAO()
					.findDocumentsByPlaceOfOrigin(placeOfOrigin);

			if (docsEnt == null || docsEnt.isEmpty()) {
				return null;
			}

			List<DocumentJson> docsJson = new ArrayList<DocumentJson>();

			for (MiaDocumentEntity docEntity : docsEnt) {
				if (!docEntity.getFlgLogicalDelete()) {
					DocumentJson doc = getDocumentJson(docEntity);
					if (doc != null) {
						docsJson.add(doc);
					}
				}

			}

			return docsJson;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	private HashMap<String, Object> serializeDocument(MiaDocumentEntity doc) {
		HashMap<String, Object> json = new HashMap<String, Object>();
		json.put("typology", doc.getTypology());
		json.put("deTitle", doc.getDeTitle());
		json.put("documentId", doc.getDocumentEntityId());
		json.put("docYear", doc.getDocYear());
		json.put("docMonth", doc.getDocMonth());
		json.put("docDay", doc.getDocDay());
		return json;
	}

	@SuppressWarnings("rawtypes")
	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public List<HashMap> findDocumentsInVolume(Volume vol, Boolean notChildren)
			throws ApplicationThrowable {
		List<HashMap> jsons = new ArrayList<HashMap>();
		for (UploadInfoEntity u : vol.getUploadInfoEntities()) {
			if (u.getInsert() != null)
				continue;
			for (UploadFileEntity f : u.getUploadFileEntities()) {
				for (MiaDocumentEntity d : f.getMiaDocumentEntities()) {
					if ((d.getFlgLogicalDelete() == null || !d.getFlgLogicalDelete())
						&& (d.getPrivacy() == null || d.getPrivacy() == 0)) {
						if (notChildren) {
							if (!collationDAO.checkIfAlreadyChild(d
									.getDocumentEntityId())) {
								jsons.add(serializeDocument(d));
							}
						} else {
							jsons.add(serializeDocument(d));
						}
					}
				}
			}
		}
		jsons = U.uniq(jsons);
		return jsons;
	}

	@Override
	public List<HashMap> findDocumentsInInsert(InsertEntity ins,
			Boolean notChildren) throws ApplicationThrowable {
		List<HashMap> jsons = new ArrayList<HashMap>();
		for (UploadInfoEntity u : ins.getUploadInfoEntities()) {
			if (u.getInsert() == null)
				continue;
			for (UploadFileEntity f : u.getUploadFileEntities()) {
				for (MiaDocumentEntity d : f.getMiaDocumentEntities()) {
					if ((d.getFlgLogicalDelete() == null || !d.getFlgLogicalDelete())
						&& (d.getPrivacy() == null || d.getPrivacy() == 0)) {
						if (notChildren) {
							if (!collationDAO.checkIfAlreadyChild(d
									.getDocumentEntityId())) {
								jsons.add(serializeDocument(d));
							}
						} else {
							jsons.add(serializeDocument(d));
						}
					}
				}
			}
		}
		jsons = U.uniq(jsons);
		return jsons;
	}

	@Override
	public Long countUserOwnDocuments(User user) throws ApplicationThrowable{
		return genericDocumentDao.countUserOwnDocuments(user);
	}

	@Override
	public MiaDocumentEntity findDocumentEntity(Integer id) throws ApplicationThrowable {
		return genericDocumentDao.find(id);
	}
	
	@Transactional(readOnly = false)
	@Override
	public Integer createNoImageDocument(
			DocumentMetaDataJson documentMetaDataJson) {
		
		UploadFileEntity uploadFileEntity = createNoImageUploadFile(
				documentMetaDataJson);
		
		FolioJson folioJson = prepareFolioJson(
				documentMetaDataJson, 
				uploadFileEntity);
		Integer folioId = getFolioDao().insertFolio(folioJson);
		
		if (folioId == null) { 
			return null;
		}
		
		GenericDocumentJson documentJson = documentMetaDataJson.getDocumentData();
		List<UploadFileEntity> uploadsFileEntities = new ArrayList<UploadFileEntity>();
		uploadsFileEntities.add(uploadFileEntity);
		
		MiaDocumentEntity documentEntity = createNoImageDocumentEntity(
				documentJson, uploadsFileEntities);
		
		if (documentEntity == null) {
			return null;
		}
		
		Integer documentEntityId =  documentDAO.insertDE(documentEntity);	
		historyLogService.registerDocumentCreation(documentEntityId);
		
		return documentEntityId;
	}
	
	private UploadFileEntity createNoImageUploadFile(
			DocumentMetaDataJson documentMetaDataJson) {
		
		UploadInfoEntity uploadInfoEntity = getUploadInfoDAO()
				.createNoImageUpload(
						documentMetaDataJson.getRepositoryId(),
						documentMetaDataJson.getCollectionId(),
						documentMetaDataJson.getVolumeId(),
						documentMetaDataJson.getInsertId());
				
		return getUploadFileDAO().
				createNoImageUploadFile(uploadInfoEntity.getUploadInfoId());
	}
	
	private FolioJson prepareFolioJson(
			DocumentMetaDataJson documentMetaDataJson,
			UploadFileEntity uploadFileEntity) {
		
		FolioJson folioJson = new FolioJson();
		folioJson.setFolioNumber(documentMetaDataJson.getFolioNumber());
		folioJson.setRectoverso(documentMetaDataJson.getRectoverso());
		folioJson.setFolioNumber(documentMetaDataJson.getFolioNumber());
		folioJson.setNoNumb(documentMetaDataJson.getNoNumb());
		
		folioJson.setUploadFileId(uploadFileEntity.getUploadFileId());
		
		return folioJson;
	}
	
	private MiaDocumentEntity createNoImageDocumentEntity(
			DocumentJson documentJson,
			List<UploadFileEntity> uploadsFileEntities) {
		
		MiaDocumentEntity documentEntity = null;
		String documentCategory = documentJson.getCategory();
		
		if (documentJson.getCategory() == null || 
			documentJson.getCategory().isEmpty()) {
			return documentEntity;
		}
		
		DocumentJson json = DocumentFactoryJson.getDocument(documentCategory);
		documentEntity = json.getMiaDocumentEntity(documentJson);

		if (documentEntity == null) {
			return documentEntity;
		}

		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()).getUsername());
		String owner = user.getAccount();
		
		documentEntity.setCreatedBy(owner);
		documentEntity.setDateCreated(new Date());
		documentEntity.setUploadFileEntities(uploadsFileEntities);
		documentEntity.setProducers(new ArrayList<People>());
		documentEntity.setPrivacy(0);
		documentEntity.setFlgLogicalDelete(Boolean.FALSE);
		
		return documentEntity;
	}
	

	public DocumentTranscriptionDAO getDocumentTranscriptionDAO() {
		return documentTranscriptionDAO;
	}

	public void setDocumentTranscriptionDAO(
			DocumentTranscriptionDAO documentTranscriptionDAO) {
		this.documentTranscriptionDAO = documentTranscriptionDAO;
	}

	public LanguageDao getLanguageDAO() {
		return languageDAO;
	}

	public void setLanguageDAO(LanguageDao languageDAO) {
		this.languageDAO = languageDAO;
	}

	public DocumentProducerDao getDocumentProducerDao() {
		return documentProducerDao;
	}

	public void setDocumentProducerDao(DocumentProducerDao documentProducerDao) {
		this.documentProducerDao = documentProducerDao;
	}

	public TopicsListDAO getTopicsListDao() {
		return topicsListDao;
	}

	public void setTopicsListDao(TopicsListDAO topicsListDao) {
		this.topicsListDao = topicsListDao;
	}

	public TopicPlaceDao getTopicPlaceDao() {
		return topicPlaceDao;
	}

	public void setTopicPlaceDao(TopicPlaceDao topicPlaceDao) {
		this.topicPlaceDao = topicPlaceDao;
	}

	public GeoBaseService getGeoBaseService() {
		return geoBaseService;
	}

	public void setGeoBaseService(GeoBaseService geoBaseService) {
		this.geoBaseService = geoBaseService;
	}

	public PlaceTypeDAO getPlaceTypeDao() {
		return placeTypeDao;
	}

	public void setPlaceTypeDao(PlaceTypeDAO placeTypeDao) {
		this.placeTypeDao = placeTypeDao;
	}

	public DocumentRefToPeopleDao getDocumentRefToPeopleDao() {
		return documentRefToPeopleDao;
	}

	public void setDocumentRefToPeopleDao(
			DocumentRefToPeopleDao documentRefToPeopleDao) {
		this.documentRefToPeopleDao = documentRefToPeopleDao;
	}

	public DocumentRelatedDocsDao getDocumentRelatedDocsDao() {
		return documentRelatedDocsDao;
	}

	public void setDocumentRelatedDocsDao(
			DocumentRelatedDocsDao documentRelatedDocsDao) {
		this.documentRelatedDocsDao = documentRelatedDocsDao;
	}

	public DocumentBiblioRefDao getDocumentBiblioRefDao() {
		return documentBiblioRefDao;
	}

	public void setDocumentBiblioRefDao(
			DocumentBiblioRefDao documentBiblioRefDao) {
		this.documentBiblioRefDao = documentBiblioRefDao;
	}

	public DocumentLanguageDao getDocumentLanguageDao() {
		return documentLanguageDao;
	}

	public void setDocumentLanguageDao(DocumentLanguageDao documentLanguageDao) {
		this.documentLanguageDao = documentLanguageDao;
	}

	public AltNameDAO getAltNameDAO() {
		return altNameDAO;
	}

	public void setAltNameDAO(AltNameDAO altNameDAO) {
		this.altNameDAO = altNameDAO;
	}

	public GenericDocumentDao getDocumentDAO() {
		return documentDAO;
	}

	public void setDocumentDAO(GenericDocumentDao documentDAO) {
		this.documentDAO = documentDAO;
	}

	public FolioDao getFolioDao() {
		return folioDao;
	}

	public void setFolioDao(FolioDao folioDao) {
		this.folioDao = folioDao;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public UploadFileDAO getUploadFileDAO() {
		return uploadFileDAO;
	}

	public void setUploadFileDAO(UploadFileDAO uploadFileDAO) {
		this.uploadFileDAO = uploadFileDAO;
	}

	public DocumentFileDao getDocFileDao() {
		return docFileDao;
	}

	public void setDocFileDao(DocumentFileDao docFileDao) {
		this.docFileDao = docFileDao;
	}

	public DocumentTypologyDAO getDocTypologyDAO() {
		return docTypologyDAO;
	}

	public void setDocTypologyDAO(DocumentTypologyDAO docTypologyDAO) {
		this.docTypologyDAO = docTypologyDAO;
	}

	public PeopleDAO getPeopleDao() {
		return peopleDao;
	}

	public void setPeopleDao(PeopleDAO peopleDao) {
		this.peopleDao = peopleDao;
	}

	public PlaceDAO getPlaceDao() {
		return placeDao;
	}

	public void setPlaceDao(PlaceDAO placeDao) {
		this.placeDao = placeDao;
	}

	public DocumentFieldDAO getDocumentFieldDao() {
		return documentFieldDao;
	}

	public void setDocumentFieldDao(DocumentFieldDAO documentFieldDao) {
		this.documentFieldDao = documentFieldDao;
	}

	public DocumentFileDao getDocumentFileDao() {
		return documentFileDao;
	}

	public void setDocumentFileDao(DocumentFileDao documentFileDao) {
		this.documentFileDao = documentFileDao;
	}

	public BiographicalPeopleService getBiographicalPeopleService() {
		return biographicalPeopleService;
	}

	public void setBiographicalPeopleService(
			BiographicalPeopleService biographicalPeopleService) {
		this.biographicalPeopleService = biographicalPeopleService;
	}

	public OrganizationDAO getOrganizationDAO() {
		return organizationDAO;
	}

	public void setOrganizationDAO(OrganizationDAO organizationDAO) {
		this.organizationDAO = organizationDAO;
	}

	public UploadInfoDAO getUploadInfoDAO() {
		return uploadInfoDAO;
	}

	public void setUploadInfoDAO(UploadInfoDAO uploadInfoDAO) {
		this.uploadInfoDAO = uploadInfoDAO;
	}

	public GenericDocumentDao getDocumentDao() {
		return documentDao;
	}

	public void setDocumentDao(GenericDocumentDao documentDao) {
		this.documentDao = documentDao;
	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

	public void setMiaDocumentService(MiaDocumentService miaDocumentService) {
		this.miaDocumentService = miaDocumentService;
	}

	public GenericDocumentDao getGenericDocumentDao() {
		return genericDocumentDao;
	}

	public void setGenericDocumentDao(GenericDocumentDao genericDocumentDao) {
		this.genericDocumentDao = genericDocumentDao;
	}
	
	public RepositoryDAO getRepositoryDAO() {
		return repositoryDAO;
	}
	
	public void setRepositoryDAO(RepositoryDAO repositoryDAO) {
		this.repositoryDAO = repositoryDAO;
	}
	
	public CollectionDAO getCollectionDAO() {
		return collectionDAO;
	}
	
	public void setCollectionDAO(CollectionDAO collectionDAO) {
		this.collectionDAO = collectionDAO;
	}
	
	public VolumeDAO getVolumeDAO() {
		return volumeDAO;
	}
	
	public void setVolumeDAO(VolumeDAO volumeDAO) {
		this.volumeDAO = volumeDAO;
	}

	@Override
	public List<String> findUsersRelatedDocOfImageId(Integer uploadFileId) {
		return documentDAO.findUsersRelatedDocOfImageId(uploadFileId);
	}

	private String capitalize(String input) {
		return input.trim().substring(0, 1).toUpperCase() + 
				input.trim().substring(1).toLowerCase();
	}
}