package org.medici.mia.service.miadoc;
/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public enum PlaceFieldType {
	RecipientPlace,
	SenderPlace,
	PlaceOfCreation,
	printerPlace,
	gonfaloneDistrict,
	placeofOccupation,
	QuartiereQuarter,
	taxpayersOccupation,
	PlaceOfIssue,
	PlaceOfCompilation,
	newsFrom;

}
