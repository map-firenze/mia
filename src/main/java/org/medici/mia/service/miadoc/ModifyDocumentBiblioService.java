package org.medici.mia.service.miadoc;

import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.document.FindBiblioForDocJson;
import org.medici.mia.common.json.document.ModifyBiblioForDocJson;
import org.medici.mia.exception.ApplicationThrowable;

public interface ModifyDocumentBiblioService {

	public FindBiblioForDocJson findDocumentBiblios(Integer documentId)
			throws ApplicationThrowable;

	public GenericResponseJson modifyDocumentBiblio(
			ModifyBiblioForDocJson modifyBiblio)
			throws ApplicationThrowable;

}