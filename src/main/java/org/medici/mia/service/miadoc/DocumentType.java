package org.medici.mia.service.miadoc;

public enum DocumentType {	
	correspondence,
	notarialRecords,
	officialRecords,
	inventories,
	fiscalRecords,
	financialRecords,
	literaryWorks,
	news,
	miscellaneous;
}
