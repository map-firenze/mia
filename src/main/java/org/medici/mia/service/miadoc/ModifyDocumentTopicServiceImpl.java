package org.medici.mia.service.miadoc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.TopicPlaceBaseJson;
import org.medici.mia.common.json.document.FindTopicPlaceForDocJson;
import org.medici.mia.common.json.document.ModifyTopicPlaceForDocJson;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.common.util.DocumentPrivacyUtils;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.dao.miadoc.TopicPlaceDao;
import org.medici.mia.dao.topicslist.TopicsListDAO;
import org.medici.mia.domain.*;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.geografical.GeographicalPlaceService;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.message.MessageService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
class ModifyDocumentTopicServiceImpl implements ModifyDocumentTopicService {

	@Autowired
	private TopicPlaceDao TopicPlaceDao;

	@Autowired
	private UserService userService;

	@Autowired
	private GenericDocumentDao genericDocumentDao;

	@Autowired
	private HistoryLogService historyLogService;

	@Autowired
	private GeographicalPlaceService geographicalPlaceService;

	@Autowired
	private TopicsListDAO topicsListDAO;

	@Autowired
	private MessageService messageService;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public FindTopicPlaceForDocJson findDocumentTopicPlaces(Integer documentId)
			throws ApplicationThrowable {

		FindTopicPlaceForDocJson topicPlaceForDoc = new FindTopicPlaceForDocJson();

		try {

			topicPlaceForDoc.setDocumentId(String.valueOf(documentId));

			List<TopicPlaceEntity> entities = getTopicPlaceDao()
					.findTopicPlaces(documentId);

			if (entities == null)
				return null;
			List<TopicPlaceBaseJson> topicPlaces = new ArrayList<TopicPlaceBaseJson>();
			for (TopicPlaceEntity entity : entities) {
				TopicPlaceBaseJson topicPlace = new TopicPlaceBaseJson();
				topicPlace.setPlaceId(entity.getPlaceId());
				topicPlace.setTopicListId(entity.getTopicListId());
				if (entity.getPlaceEntity() != null) {
					topicPlace.setUnsure(entity.getPlaceEntity().getUnsure());
					topicPlace.setPrefFlag(entity.getPlaceEntity()
							.getPrefFlag());

				}
				topicPlaces.add(topicPlace);
			}

			topicPlaceForDoc.setTopicPlaces(topicPlaces);

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return topicPlaceForDoc;

	}

	public TopicPlaceDao getTopicPlaceDao() {
		return TopicPlaceDao;
	}

	public void setTopicPlaceDao(TopicPlaceDao topicPlaceDao) {
		TopicPlaceDao = topicPlaceDao;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson modifyDocumentTopicPlaces(
			ModifyTopicPlaceForDocJson modifyTopicPlace)
			throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");

		try {

			getTopicPlaceDao().modifyTopicPlaces(modifyTopicPlace);

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("TopicPlace of document Modified.");

			HashMap<String, Object> changes = new HashMap<String, Object>();
			HashMap<String, HashMap> category = new HashMap<String, HashMap>();
			HashMap<String, HashMap> action = new HashMap<String, HashMap>();
			HashMap<String, Object> before = new HashMap<String, Object>();
			HashMap<String, Object> after = new HashMap<String, Object>();
			if(modifyTopicPlace.getTopicPlaceToBeDeleted() != null){
				changes.put("placeName", geographicalPlaceService
						.findGeographicalPlace(modifyTopicPlace.getTopicPlaceToBeDeleted().getPlaceId(), true)
						.getPlaceName());
				changes.put("topicName", topicsListDAO.find(modifyTopicPlace.getTopicPlaceToBeDeleted().getTopicListId())
                        .getTopicTitle());
				action.put("delete", changes);
			} else if(modifyTopicPlace.getOldTopicPlace() == null){
				changes.put("placeName", geographicalPlaceService
						.findGeographicalPlace(modifyTopicPlace.getNewTopicPlace().getPlaceId(), true)
						.getPlaceName());
				changes.put("topicName", topicsListDAO.find(modifyTopicPlace.getNewTopicPlace().getTopicListId()).getTopicTitle());
				action.put("add", changes);
			} else {
				if(modifyTopicPlace.getOldTopicPlace().getPlaceId().equals(modifyTopicPlace.getNewTopicPlace().getPlaceId())
						|| modifyTopicPlace.getNewTopicPlace().getTopicListId().equals(modifyTopicPlace.getOldTopicPlace().getTopicListId())) {
					before.put("placeName", geographicalPlaceService
							.findGeographicalPlace(modifyTopicPlace.getOldTopicPlace().getPlaceId(), true)
							.getPlaceName());
					before.put("topicName", topicsListDAO.find(modifyTopicPlace.getOldTopicPlace().getTopicListId())
							.getTopicTitle());
					after.put("placeName", geographicalPlaceService
							.findGeographicalPlace(modifyTopicPlace.getNewTopicPlace().getPlaceId(), true)
							.getPlaceName());
					after.put("topicName", topicsListDAO.find(modifyTopicPlace.getNewTopicPlace().getTopicListId()).getTopicTitle());
					changes.put("before", before);
					changes.put("after", after);
					action.put("edit", changes);
				}
			}
			category.put("documentTopics", action);

			historyLogService.registerAction(
					Integer.valueOf(modifyTopicPlace.getDocumentId()), HistoryLogEntity.UserAction.EDIT,
					HistoryLogEntity.RecordType.DE, null, category);

			// Get the username logged in
			UserDetails currentUser = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();

			MiaDocumentEntity doc = getGenericDocumentDao()
					.findDocumentById(Integer.valueOf(modifyTopicPlace.getDocumentId()));
			if (!currentUser.getUsername().equals(doc.getCreatedBy()) && !DocumentPrivacyUtils.isUserAdmin(currentUser)
					&& !DocumentPrivacyUtils.isOnsiteFellows(currentUser)) {
				messageService.onDocumentEdited(currentUser.getUsername(), doc.getCreatedBy(),
						doc.getDocumentEntityId());
			}
			getGenericDocumentDao().setDocumentModInf(Integer.valueOf(modifyTopicPlace.getDocumentId()),
					currentUser.getUsername());

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public GenericDocumentDao getGenericDocumentDao() {
		return genericDocumentDao;
	}

	public void setGenericDocumentDao(GenericDocumentDao genericDocumentDao) {
		this.genericDocumentDao = genericDocumentDao;
	}

}
