package org.medici.mia.service.miadoc;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.document.IModifyDocJson;
import org.medici.mia.domain.EntityType;
import org.medici.mia.exception.ApplicationThrowable;

public interface ModifyDocumentService {

	public List<IModifyDocJson> findEntities(Integer documentId, JsonType jsonType)
			throws ApplicationThrowable;

	public GenericResponseJson modifyEntities(List<IModifyDocJson> jsons,
			EntityType entityType) throws ApplicationThrowable;
	
	public List<Integer> findNotFoundDocsForRelatedDocuments(List<Integer> docsId)
			throws PersistenceException;

}