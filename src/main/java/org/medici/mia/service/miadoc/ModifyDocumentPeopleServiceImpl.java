package org.medici.mia.service.miadoc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.PeopleBaseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.document.DocumentFactoryJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.document.FindPeopleForDocJson;
import org.medici.mia.common.json.document.ModifyPeopleForDocJson;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.common.util.DocumentPrivacyUtils;
import org.medici.mia.dao.documentfield.DocumentFieldDAO;
import org.medici.mia.dao.miadoc.DocumentProducerDao;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.domain.*;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.message.MessageService;
import org.medici.mia.service.people.BiographicalPeopleService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
class ModifyDocumentPeopleServiceImpl implements ModifyDocumentPeopleService {

	@Autowired
	private GenericDocumentDao documentDAO;

	@Autowired
	private DocumentProducerDao documentProducerDao;

	@Autowired
	private DocumentFieldDAO documentFieldDao;

	@Autowired
	private GenericDocumentDao genericDocumentDao;

	@Autowired
	private UserService userService;

	@Autowired
	private HistoryLogService historyLogService;

	@Autowired
	private BiographicalPeopleService peopleService;
	
	@Autowired
	private MessageService messageService;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<FindPeopleForDocJson> findDocumentPeoples(Integer documentId,
			String category) throws ApplicationThrowable {
		List<FindPeopleForDocJson> peoples = null;

		try {

			MiaDocumentEntity docEntity = getDocumentDAO().findDocumentById(
					documentId);

			if (docEntity == null)
				return null;

			peoples = new ArrayList<FindPeopleForDocJson>();
			peoples.add(getModifyPeopleProducer(docEntity));
			DocumentJson doc = DocumentFactoryJson.getDocument(docEntity
					.getCategory());
			if (doc.findPeopleJson(docEntity) != null) {
				peoples.addAll(doc.findPeopleJson(docEntity));
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return peoples;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson modifyDocumentPeople(
			ModifyPeopleForDocJson modifyPeople) throws ApplicationThrowable {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");

		try {

			MiaDocumentEntity docEntity = getDocumentDAO().findDocumentById(
					Integer.valueOf(modifyPeople.getDocumentId()));
			if (docEntity == null) {
				resp.setMessage("No document found in DB with documentId "
						+ modifyPeople.getDocumentId());
				resp.setStatus(StatusType.w.toString());
				return resp;

			}

			// Get the username logged in
			UserDetails currentUser = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			
			String producerName = getProducerName(docEntity.getCategory());

			if (producerName != null
					&& modifyPeople.getType() != null
					&& producerName.replaceAll(" ", "").equalsIgnoreCase(
							modifyPeople.getType().replaceAll(" ", ""))) {

				HashMap<String, Object> changes = new HashMap<String, Object>();
				HashMap<String, HashMap> category = new HashMap<String, HashMap>();
				HashMap<String, HashMap> action = new HashMap<String, HashMap>();
				HashMap<String, Object> before = new HashMap<String, Object>();
				HashMap<String, Object> after = new HashMap<String, Object>();
				if(modifyPeople.getIdPeopleToDelete() != null && modifyPeople.getNewPeople() == null){
					changes.put("personName", peopleService
							.findBiographicalPeople(modifyPeople.getIdPeopleToDelete()).getMapNameLf());
					changes.put("type", modifyPeople.getType());
					action.put("delete", changes);
				} else {
					changes.put("personName", peopleService
							.findBiographicalPeople(modifyPeople.getNewPeople().getId()).getMapNameLf());
					changes.put("type", modifyPeople.getType());
					changes.put("unsure", modifyPeople.getNewPeople().getUnsure());
					action.put("add", changes);
				}

				category.put("documentPeople", action);

				historyLogService.registerAction(
						Integer.valueOf(modifyPeople.getDocumentId()), HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.DE, null, category);

				modifyProducer(modifyPeople);
			} else {
				DocumentJson doc = DocumentFactoryJson.getDocument(docEntity
						.getCategory());
				docEntity = doc.getModifiedDocumentEnt(docEntity, modifyPeople);
				if (docEntity.getErrorMsg() != null
						&& !docEntity.getErrorMsg().isEmpty()) {
					resp.setStatus(StatusType.ko.toString());
					resp.setMessage(docEntity.getErrorMsg());
					return resp;

				}

				HashMap<String, Object> changes = new HashMap<String, Object>();
				HashMap<String, HashMap> category = new HashMap<String, HashMap>();
				HashMap<String, HashMap> action = new HashMap<String, HashMap>();
				HashMap<String, Object> before = new HashMap<String, Object>();
				HashMap<String, Object> after = new HashMap<String, Object>();
                if(modifyPeople.getIdPeopleToDelete() != null && modifyPeople.getNewPeople() == null){
                    changes.put("personName", peopleService
                            .findBiographicalPeople(modifyPeople.getIdPeopleToDelete()).getMapNameLf());
                    changes.put("type", modifyPeople.getType());
                    action.put("delete", changes);
                } else {
                    changes.put("personName", peopleService
                            .findBiographicalPeople(modifyPeople.getNewPeople().getId()).getMapNameLf());
                    changes.put("type", modifyPeople.getType());
                    changes.put("unsure", modifyPeople.getNewPeople().getUnsure());
                    action.put("add", changes);
                }

                category.put("documentPeople", action);

                historyLogService.registerAction(
                        Integer.valueOf(modifyPeople.getDocumentId()), HistoryLogEntity.UserAction.EDIT,
                        HistoryLogEntity.RecordType.DE, null, category);

				docEntity.setLastUpdateBy(currentUser.getUsername());
				docEntity.setDateLastUpdate(new Date());
				
				getGenericDocumentDao().merge(docEntity);
			}

			if (!currentUser.getUsername().equals(docEntity.getCreatedBy())
					&& !DocumentPrivacyUtils.isUserAdmin(currentUser)
					&& !DocumentPrivacyUtils.isOnsiteFellows(currentUser)) {
				messageService.onDocumentEdited(currentUser.getUsername(), docEntity.getCreatedBy(),
						docEntity.getDocumentEntityId());
			}

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Document Modified.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	private FindPeopleForDocJson getModifyPeopleProducer(
			MiaDocumentEntity docEntity) {

		FindPeopleForDocJson modifyPeopleJson = new FindPeopleForDocJson();
		List<PeopleBaseJson> basePeoples = null;

		modifyPeopleJson.setType(getProducerName(docEntity.getCategory()));

		if (docEntity.getProducers() != null
				&& !docEntity.getProducers().isEmpty()) {

			basePeoples = new ArrayList<PeopleBaseJson>();
			for (People producer : docEntity.getProducers()) {
				PeopleBaseJson peopleBase = new PeopleBaseJson();
				DocumentProducerJoinEntity docProducerJoinEntity = getDocumentProducerDao()
						.findDocProducer(docEntity.getDocumentEntityId(),
								producer.getPersonId());
				peopleBase.setUnsure(docProducerJoinEntity.getProducerUnsure());
				peopleBase.setId(producer.getPersonId());
				basePeoples.add(peopleBase);

			}
		}

		modifyPeopleJson.setPeoples(basePeoples);

		return modifyPeopleJson;
	}

	private String getProducerName(String category) {

		List<DocumentFieldEntity> fields = getDocumentFieldDao()
				.getDocumentFieldsByCategory(category);

		for (DocumentFieldEntity filed : fields) {
			if (filed.getFieldBeName().equalsIgnoreCase("producers")) {
				return filed.getFieldName();

			}

		}

		return "producers";
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	private void modifyProducer(ModifyPeopleForDocJson modifyPeople)
			throws ApplicationThrowable {
		try {
			
			getDocumentProducerDao().modifyProducerForDoc(modifyPeople);
			// Get the username logged in
			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());

			getGenericDocumentDao().setDocumentModInf(
					Integer.valueOf(modifyPeople.getDocumentId()),
					user.getAccount());
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	public GenericDocumentDao getDocumentDAO() {
		return documentDAO;
	}

	public void setDocumentDAO(GenericDocumentDao documentDAO) {
		this.documentDAO = documentDAO;
	}

	public DocumentProducerDao getDocumentProducerDao() {
		return documentProducerDao;
	}

	public void setDocumentProducerDao(DocumentProducerDao documentProducerDao) {
		this.documentProducerDao = documentProducerDao;
	}

	public DocumentFieldDAO getDocumentFieldDao() {
		return documentFieldDao;
	}

	public void setDocumentFieldDao(DocumentFieldDAO documentFieldDao) {
		this.documentFieldDao = documentFieldDao;
	}

	public GenericDocumentDao getGenericDocumentDao() {
		return genericDocumentDao;
	}

	public void setGenericDocumentDao(GenericDocumentDao genericDocumentDao) {
		this.genericDocumentDao = genericDocumentDao;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
