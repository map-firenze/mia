package org.medici.mia.service.miadoc;

import java.util.List;

import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.document.FindPeopleForDocJson;
import org.medici.mia.common.json.document.ModifyPeopleForDocJson;
import org.medici.mia.exception.ApplicationThrowable;

public interface ModifyDocumentPeopleService {

	public List<FindPeopleForDocJson> findDocumentPeoples(Integer documentId,
			String category) throws ApplicationThrowable;

	public GenericResponseJson modifyDocumentPeople(
			ModifyPeopleForDocJson modPeople) throws ApplicationThrowable;

}