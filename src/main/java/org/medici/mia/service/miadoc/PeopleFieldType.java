package org.medici.mia.service.miadoc;
/** 
 * 
 * @author Shadab Bigdel (<a href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public enum PeopleFieldType {
	Recipient, Contractor, Contractee, Printer, Commissioner, Taxpayer, CommissionerofAccount, Dedicatee, Compiler, Producers;

}
