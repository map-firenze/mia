package org.medici.mia.service.miadoc;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.document.FactoryJson;
import org.medici.mia.common.json.document.IModifyDocJson;
import org.medici.mia.common.json.message.MessageSendJson;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.common.util.DocumentPrivacyUtils;
import org.medici.mia.dao.miadoc.DocumentTranscriptionDAO;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.dao.miadoc.LanguageDao;
import org.medici.mia.dao.miadoc.TopicPlaceDao;
import org.medici.mia.domain.EntityType;
import org.medici.mia.domain.IGenericEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.User;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.message.MessageService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
class ModifyDocumentServiceImpl implements ModifyDocumentService {

	private final String RESPONSE_MESSAGE = "The number of modified entities: ";

	@Autowired
	private DocumentTranscriptionDAO documentTranscriptionDAO;

	@Autowired
	private GenericDocumentDao genericDocumentDAO;

	@Autowired
	private LanguageDao languageDAO;

	@Autowired
	private TopicPlaceDao topicPlaceDAO;

	@Autowired
	private UserService userService;

	@Autowired
	private MiaDocumentService miaDocumentService;

	@Autowired
	private MessageService messageService;
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<IModifyDocJson> findEntities(Integer entityId, JsonType JsonType)
			throws ApplicationThrowable {
		try {

			List<IGenericEntity> ents = find(JsonType, entityId);
			if (ents == null || ents.isEmpty()) {
				return null;
			}
			List<IModifyDocJson> jsonList = new ArrayList<IModifyDocJson>();

			for (IGenericEntity entity : ents) {
				IModifyDocJson json = FactoryJson.getJson(JsonType.toString());
				if (json == null)
					return null;
				json.toJson(entity);
				jsonList.add(json);

			}

			return jsonList;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson modifyEntities(List<IModifyDocJson> jsons,
			EntityType entityType) throws ApplicationThrowable {

		try {
			GenericResponseJson resp = new GenericResponseJson();
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Uncaught error");

			if (jsons == null || jsons.isEmpty()) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("The transcription list received by Fe is null or empty.");
				return resp;
			}
			Integer modifiedEntities = 0;
			modifiedEntities = modify(entityType, jsons);

			// Get the username logged in
			UserDetails currentUser = (UserDetails) SecurityContextHolder.getContext()
					.getAuthentication().getPrincipal();
			
			for (IModifyDocJson json : jsons) {
				MiaDocumentEntity doc = getGenericDocumentDAO().findDocumentById(Integer.valueOf(json.getDocumentId()));
				if (!currentUser.getUsername().equals(doc.getCreatedBy())
						&& !DocumentPrivacyUtils.isUserAdmin(currentUser)
						&& !DocumentPrivacyUtils.isOnsiteFellows(currentUser)) {
					messageService.onDocumentEdited(currentUser.getUsername(), doc.getCreatedBy(),
							doc.getDocumentEntityId());
				}
				getGenericDocumentDAO().setDocumentModInf(Integer.valueOf(json.getDocumentId()),
						currentUser.getUsername());
				// Populate TransSearch column
				getMiaDocumentService().updateTransSearch(Integer.valueOf(json.getDocumentId()));
			}

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage(RESPONSE_MESSAGE + modifiedEntities);

			return resp;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	private Integer modify(EntityType entityType, List<IModifyDocJson> jsons) {

		Integer modifiedEntities = 0;
		if (jsons != null && !jsons.isEmpty()) {
			switch (entityType) {
			case transcription:
				modifiedEntities = getDocumentTranscriptionDAO()
						.modifyEntities(jsons);
				break;
			case genericdocument:
				modifiedEntities = getGenericDocumentDAO()
						.modifyEntities(jsons);
				break;
			case topic:
				modifiedEntities = getTopicPlaceDAO().modifyEntities(jsons);
				break;
			default:
				break;
			}
		}
		return modifiedEntities;
	}

	private List<IGenericEntity> find(JsonType jsonType, Integer entityId) {

		List<IGenericEntity> genericEntities = null;
		if (entityId == null)
			return null;

		switch (jsonType) {
		case transcription:
			genericEntities = getDocumentTranscriptionDAO().findEntities(
					entityId);
			break;
		case synopsis:
			genericEntities = getGenericDocumentDAO().findEntities(entityId);
			break;
		case chineseSynopsis:
			genericEntities = getGenericDocumentDAO().findEntities(entityId);
			break;
		case basicDescription:
			genericEntities = getGenericDocumentDAO().findEntities(entityId);
		case category:
			genericEntities = getGenericDocumentDAO().findEntities(entityId);
		case documentLanguage:
			genericEntities = getGenericDocumentDAO().findEntities(entityId);
			break;
		case topic:
			genericEntities = getTopicPlaceDAO().findEntities(entityId);
			break;
		case relatedDocs:
			genericEntities = getGenericDocumentDAO().findEntities(entityId);
			break;
		default:
			break;
		}

		return genericEntities;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<Integer> findNotFoundDocsForRelatedDocuments(
			List<Integer> docsId) throws PersistenceException {
		List<Integer> docs = new ArrayList<Integer>();
		for (Integer docId : docsId) {
			if (getGenericDocumentDAO().findDocumentById(docId) == null) {
				docs.add(docId);
			}
		}
		return docs;
	}

	public DocumentTranscriptionDAO getDocumentTranscriptionDAO() {
		return documentTranscriptionDAO;
	}

	public void setDocumentTranscriptionDAO(
			DocumentTranscriptionDAO documentTranscriptionDAO) {
		this.documentTranscriptionDAO = documentTranscriptionDAO;
	}

	public GenericDocumentDao getGenericDocumentDAO() {
		return genericDocumentDAO;
	}

	public void setGenericDocumentDAO(GenericDocumentDao genericDocumentDAO) {
		this.genericDocumentDAO = genericDocumentDAO;
	}

	public LanguageDao getLanguageDAO() {
		return languageDAO;
	}

	public void setLanguageDAO(LanguageDao languageDAO) {
		this.languageDAO = languageDAO;
	}

	public TopicPlaceDao getTopicPlaceDAO() {
		return topicPlaceDAO;
	}

	public void setTopicPlaceDAO(TopicPlaceDao topicPlaceDAO) {
		this.topicPlaceDAO = topicPlaceDAO;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

	public void setMiaDocumentService(MiaDocumentService miaDocumentService) {
		this.miaDocumentService = miaDocumentService;
	}

}
