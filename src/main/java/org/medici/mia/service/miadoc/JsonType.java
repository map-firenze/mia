package org.medici.mia.service.miadoc;

public enum JsonType {
	transcription, synopsis, chineseSynopsis, basicDescription, category, 
	documentLanguage, language, topic, relatedDocs;
}
