package org.medici.mia.service.miadoc;

import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.document.FindTopicPlaceForDocJson;
import org.medici.mia.common.json.document.ModifyTopicPlaceForDocJson;
import org.medici.mia.exception.ApplicationThrowable;

public interface ModifyDocumentTopicService {

	public FindTopicPlaceForDocJson findDocumentTopicPlaces(Integer documentId)
			throws ApplicationThrowable;

	public GenericResponseJson modifyDocumentTopicPlaces(
			ModifyTopicPlaceForDocJson modifyTopicPlace)
			throws ApplicationThrowable;

}