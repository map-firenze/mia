package org.medici.mia.service.miadoc;

import java.util.List;

import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.document.FindPlaceForDocJson;
import org.medici.mia.common.json.document.ModifyPlaceForDocJson;
import org.medici.mia.exception.ApplicationThrowable;

public interface ModifyDocumentPlaceService {

	public List<FindPlaceForDocJson> findDocumentPlaces(Integer documentId,
			String category) throws ApplicationThrowable;

	public GenericResponseJson modifyDocumentPlaces(
			ModifyPlaceForDocJson modPlace, String placeOfOriginName)
			throws ApplicationThrowable;

}