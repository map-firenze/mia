package org.medici.mia.service.titleocc;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.biographical.BiographicalPeopleJson;
import org.medici.mia.common.json.titleocc.ModifyTitleOccRoleJson;
import org.medici.mia.common.json.titleocc.RoleCatJson;
import org.medici.mia.common.json.titleocc.TitleOccRoleJson;
import org.medici.mia.exception.ApplicationThrowable;

public interface TitleAndOccupationService {

	public TitleOccRoleJson findTitleAndOccupation(Integer titleOccId)
			throws ApplicationThrowable;

	public GenericResponseJson addNewTitleOccupation(
			TitleOccRoleJson titleOccRoleJson) throws ApplicationThrowable;

	public GenericResponseJson modifyTitleOccupation(
			ModifyTitleOccRoleJson modifyTitleOccRoleJson)
			throws ApplicationThrowable;

	public GenericResponseDataJson<List<BiographicalPeopleJson>> deleteTitleOccupation(Integer titleOccId)
			throws ApplicationThrowable;

	public List<RoleCatJson> findRoleCategories() throws ApplicationThrowable;
	
	public List<Integer> findPersonsByTitleOcc(Integer titleOccId)
			throws PersistenceException;
	
	

}