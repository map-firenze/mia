package org.medici.mia.service.titleocc;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.biographical.BiographicalPeopleJson;
import org.medici.mia.common.json.titleocc.ModifyTitleOccRoleJson;
import org.medici.mia.common.json.titleocc.RoleCatJson;
import org.medici.mia.common.json.titleocc.TitleOccRoleJson;
import org.medici.mia.dao.people.BiographicalPeopleDAO;
import org.medici.mia.dao.polink.PoLinkDAO;
import org.medici.mia.dao.rolecat.RoleCatDAO;
import org.medici.mia.dao.titleoccslist.TitleOccsListDAO;
import org.medici.mia.domain.BiographicalPeople;
import org.medici.mia.domain.RoleCat;
import org.medici.mia.domain.TitleOccsList;
import org.medici.mia.exception.ApplicationThrowable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
class TitleAndOccupationServiceImpl implements TitleAndOccupationService {
	@Autowired
	private TitleOccsListDAO titleOccsListDao;

	@Autowired
	private PoLinkDAO poLinkDAO;

	@Autowired
	private RoleCatDAO roleCatDAO;

	@Autowired
	private BiographicalPeopleDAO biographicalPeopleDAO;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public TitleOccRoleJson findTitleAndOccupation(Integer titleOccId)
			throws ApplicationThrowable {
		try {
			TitleOccsList titleOcc = getTitleOccsListDao().find(titleOccId);
			TitleOccRoleJson titleOccJson = new TitleOccRoleJson();
			titleOccJson.toJson(titleOcc);
			if (titleOcc != null) {
				return titleOccJson;
			}

			return null;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson addNewTitleOccupation(
			TitleOccRoleJson titleOccRoleJson) throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {

			Integer count = getTitleOccsListDao().addNewTitleOccupation(
					titleOccRoleJson);
			if (count == 0) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("The title with this title name and catRoleId exist in DB and Not added.");
				return resp;
			}

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("The new title and occupation  added in DB.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson modifyTitleOccupation(
			ModifyTitleOccRoleJson modifyTitleOccRoleJson)
			throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {

			Integer count = getTitleOccsListDao().modifyTitleOccupation(
					modifyTitleOccRoleJson);
			if (count == 0) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("The title with this old title name and old catRoleId not exist in DB and Not modified.");
				return resp;
			}

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("The title and occupation modified in DB.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseDataJson<List<BiographicalPeopleJson>> deleteTitleOccupation(
			Integer titleOccId) throws ApplicationThrowable {

		GenericResponseDataJson<List<BiographicalPeopleJson>> resp = new GenericResponseDataJson<List<BiographicalPeopleJson>>();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {

			// Control per title used in person

			List<Integer> personIds = getPoLinkDAO().findPersonsByTitleOcc(
					titleOccId);
			if (personIds != null && !personIds.isEmpty()) {

				List<BiographicalPeople> bioPeople = getBiographicalPeopleDAO()
						.findBiographicalPeopleById(personIds);

				if (bioPeople != null && !bioPeople.isEmpty()) {
					List<BiographicalPeopleJson> bioPeopleJson = new ArrayList<BiographicalPeopleJson>();

					for (BiographicalPeople bioPerson : bioPeople) {
						BiographicalPeopleJson bioPersonJson = new BiographicalPeopleJson();
						bioPersonJson.toJson(bioPerson);
						bioPeopleJson.add(bioPersonJson);

					}

					resp.setData(bioPeopleJson);

				}

				resp.setStatus(StatusType.w.toString());
				resp.setMessage("The TitleAndOccupation is used in people.");
				return resp;

			}

			Integer count = getTitleOccsListDao().deleteTitleOccupation(
					titleOccId);
			if (count == 0) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("The title with this tittleOccId doesnt exist in DB and Not deleted.");
				return resp;
			}

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("The title and occupation deleted in DB.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return resp;

	}

	public List<RoleCatJson> findRoleCategories() throws ApplicationThrowable {
		try {
			List<RoleCat> roleCatEnts = getRoleCatDAO().getAllRoleCat();
			if (roleCatEnts == null || roleCatEnts.isEmpty()) {
				return null;
			}
			List<RoleCatJson> roleCategories = new ArrayList<RoleCatJson>();
			for (RoleCat roleCatEnt : roleCatEnts) {
				RoleCatJson roleCat = new RoleCatJson();
				roleCat.toJson(roleCatEnt);
				roleCategories.add(roleCat);
			}

			return roleCategories;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	public List<Integer> findPersonsByTitleOcc(Integer titleOccId)
			throws PersistenceException {

		try {

			return getPoLinkDAO().findPersonsByTitleOcc(titleOccId);

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	public TitleOccsListDAO getTitleOccsListDao() {
		return titleOccsListDao;
	}

	public void setTitleOccsListDao(TitleOccsListDAO titleOccsListDao) {
		this.titleOccsListDao = titleOccsListDao;
	}

	public RoleCatDAO getRoleCatDAO() {
		return roleCatDAO;
	}

	public void setRoleCatDAO(RoleCatDAO roleCatDAO) {
		this.roleCatDAO = roleCatDAO;
	}

	public PoLinkDAO getPoLinkDAO() {
		return poLinkDAO;
	}

	public void setPoLinkDAO(PoLinkDAO poLinkDAO) {
		this.poLinkDAO = poLinkDAO;
	}

	public BiographicalPeopleDAO getBiographicalPeopleDAO() {
		return biographicalPeopleDAO;
	}

	public void setBiographicalPeopleDAO(
			BiographicalPeopleDAO biographicalPeopleDAO) {
		this.biographicalPeopleDAO = biographicalPeopleDAO;
	}

}
