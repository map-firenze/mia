package org.medici.mia.service.comment;

import com.github.underscore.Function;
import com.github.underscore.Predicate;
import com.github.underscore.U;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.comment.CommentJson;
import org.medici.mia.common.json.comment.CreateCommentJson;
import org.medici.mia.common.json.message.CommentReportMessageJson;
import org.medici.mia.common.json.message.MessageSendJson;
import org.medici.mia.dao.comment.CommentDAO;
import org.medici.mia.dao.insert.InsertDAO;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.dao.people.BiographicalPeopleDAO;
import org.medici.mia.dao.people.PeopleDAO;
import org.medici.mia.dao.place.PlaceDAO;
import org.medici.mia.dao.uploadinfo.UploadInfoDAO;
import org.medici.mia.dao.user.UserDAO;
import org.medici.mia.dao.userrole.UserRoleDAO;
import org.medici.mia.dao.volume.VolumeDAO;
import org.medici.mia.domain.*;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.message.MessageService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static org.medici.mia.domain.Comment.RecordType.DE;

public class CommentServiceImpl implements CommentService {

    @Autowired
    private UserService userService;

    @Autowired
    private CommentDAO commentDAO;

    @Autowired
    private GenericDocumentDao documentDAO;

    @Autowired
    private UploadInfoDAO uploadInfoDAO;

    @Autowired
    private BiographicalPeopleDAO peopleDAO;

    @Autowired
    private PlaceDAO placeDAO;

    @Autowired
    private UserRoleDAO userRoleDAO;

    @Autowired
    private VolumeDAO volumeDAO;

    @Autowired
    private InsertDAO insertDAO;

    @Autowired
    private MessageService messageService;

    @Autowired
    private UserDAO userDAO;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Comment findComment(Integer id) throws ApplicationThrowable {
        return commentDAO.find(id);
    }

    @Override
    public Comment findActiveComment(Integer id) throws ApplicationThrowable {
        return commentDAO.findById(id);
    }

    @Override
    public Boolean isAuthorized(String account)
            throws ApplicationThrowable {
        try {
            List<UserRole> userRoles = userRoleDAO.findUserRoles(account);
            int i = 0;
            Boolean isAuthorized = Boolean.FALSE;
            while (i < userRoles.size() && !isAuthorized) {
                if (userRoles.get(i)
                        .containsAuthority(UserAuthority.Authority.ADMINISTRATORS)
                        || userRoles.get(i).containsAuthority(
                        UserAuthority.Authority.COMMUNITY_COORDINATORS)
                        || userRoles.get(i).containsAuthority(
                        UserAuthority.Authority.ONSITE_FELLOWS)) {
                    isAuthorized = Boolean.TRUE;
                }
                i++;
            }
            return isAuthorized;
        } catch (Throwable th) {
            throw new ApplicationThrowable(th);
        }
    }

    @Override
    public Boolean isAuthorizedForDelete(String account)
            throws ApplicationThrowable {
        try {
            List<UserRole> userRoles = userRoleDAO.findUserRoles(account);
            int i = 0;
            Boolean isAuthorized = Boolean.FALSE;
            while (i < userRoles.size() && !isAuthorized) {
                if (userRoles.get(i)
                        .containsAuthority(UserAuthority.Authority.ADMINISTRATORS)
                        || userRoles.get(i).containsAuthority(
                        UserAuthority.Authority.COMMUNITY_COORDINATORS)) {
                    isAuthorized = Boolean.TRUE;
                }
                i++;
            }
            return isAuthorized;
        } catch (Throwable th) {
            throw new ApplicationThrowable(th);
        }
    }

    private void updateCounter(Integer id, Comment.RecordType type){
        boolean valid = false;
        switch (type) {
            case AE:
                UploadInfoEntity upl = uploadInfoDAO.getUploadById(id);
                if (upl != null) {
                    if(upl.getCommentsCount() == null){
                        upl.setCommentsCount(1);
                    } else {
                        upl.setCommentsCount(upl.getCommentsCount()+1);
                    }
                    uploadInfoDAO.merge(upl);
                }
                break;
            case DE:
                MiaDocumentEntity doc = documentDAO.findDocumentById(id);
                if (doc != null) {
                    if(doc.getCommentsCount() == null){
                        doc.setCommentsCount(1);
                    } else {
                        doc.setCommentsCount(doc.getCommentsCount()+1);
                    }
                    documentDAO.merge(doc);
                }
                break;

            case GEO:
                Place plc = placeDAO.findPlaceById(id);
                if (plc != null) {
                    if(plc.getCommentsCount() == null){
                        plc.setCommentsCount(1);
                    } else {
                        plc.setCommentsCount(plc.getCommentsCount()+1);
                    }
                    placeDAO.merge(plc);
                }
                break;

            case BIO:
                BiographicalPeople psn = peopleDAO.findPeopleById(id);
                if (psn != null) {
                    if(psn.getCommentsCount() == null){
                        psn.setCommentsCount(1);
                    } else {
                        psn.setCommentsCount(psn.getCommentsCount()+1);
                    }
                    peopleDAO.merge(psn);
                }
                break;

            case COLL:
                break;

            case CS:
                break;
        }
    }

    @Override
    public CommentJson setPhoto(CommentJson json, Comment comment)
            throws ApplicationThrowable{
        boolean isImage = userService.isUserPortrait(
                comment.getCreatedBy().getPortraitImageName());
        if (isImage) {
            json.setPhoto("/Mia/user/ShowPortraitUser.do?account="
                    + comment.getCreatedBy().getAccount());
        } else {
            json.setPhoto("/Mia/images/1024/img_user.png");
        }
        return json;
    }

    @Override
    @Transactional
    public CommentJson createComment(CreateCommentJson createJson) throws ApplicationThrowable {
        User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        Comment comment = createJson.toEntity(user);
        updateCounter(createJson.getRecordId(), Comment.RecordType.valueOf(createJson.getRecordType()));
        if(comment.getParent() != null && comment.getChildren().size() > 0){
            return null;
        }
        commentDAO.saveComment(comment);
        CommentJson json = new CommentJson().toJson(comment, user);
        return setPhoto(json, comment);
    }

    @Override
    @Transactional
    public CommentJson replyToComment(CreateCommentJson createJson, Integer id) throws ApplicationThrowable {
        User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        Comment parent = commentDAO.find(id);
        Comment comment;
        if(createJson.getRepliedToId() != null){
            Comment repliedTo = commentDAO.find(createJson.getRepliedToId());
            if(repliedTo != null){
                comment = createJson.toEntity(user, parent, repliedTo);
            } else {
                comment = createJson.toEntity(user, parent);
            }
        } else {
            comment = createJson.toEntity(user, parent);
        }
        if(!commentDAO.saveComment(comment)){
            return null;
        }
        CommentJson json = new CommentJson().toJson(comment, user);
        return setPhoto(json, comment);
    }

    @Override
    @Transactional
    public CommentJson modifyComment(Comment comment, String text, Comment repliedTo, String mentions) throws ApplicationThrowable {
        User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        comment.setText(text);
        comment.setRepliedTo(repliedTo);
        comment.setModifiedAt(new Date());
        comment.setModifiedBy(user);
        if(mentions == null || !mentions.equals("no_field")) {
            comment.setMentions(mentions);
        }
        commentDAO.merge(comment);
        CommentJson json = new CommentJson().toJson(comment, user);
        return setPhoto(json, comment);
    }

    @Override
    public List<CommentJson> findByRecord(Integer id, Comment.RecordType type) throws ApplicationThrowable {
        User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        List<Comment> comments = commentDAO.findByRecord(id, type);
        List<CommentJson> jsons = new ArrayList<CommentJson>();
        comments = U.chain(comments).sortBy(new Function<Comment, Comparable>() {
            @Override
            public Comparable apply(Comment c) {
                return c.getCreatedAt();
            }
        }).reverse().value();
        for(Comment c: comments){
            if(!user.getAccount().equals(c.getCreatedBy().getAccount()) && c.getLogicalDelete() && !isAuthorized(user.getAccount())) continue;
            CommentJson json = new CommentJson().toJson(c, user);
            jsons.add(setPhoto(json, c));
        }
        return jsons;
    }

    @Override
    public List<CommentJson> findById(Integer id) throws ApplicationThrowable {
        entityManager.setFlushMode(FlushModeType.COMMIT);
        final User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        List<Comment> children = commentDAO.find(id).getChildren();
        List<Comment> comments = new ArrayList<Comment>();

        for(Comment c: children){
            if(!user.getAccount().equals(c.getCreatedBy().getAccount()) && c.getLogicalDelete() && !isAuthorized(user.getAccount())) continue;
            comments.add(c);
        }
        List<CommentJson> jsons = U.chain(comments).sortBy(new Function<Comment, Comparable>() {
            @Override
            public Comparable apply(Comment c) {
                return c.getCreatedAt();
            }
        }).map(new Function<Comment, CommentJson>() {
            public CommentJson apply(Comment c){
                CommentJson json = new CommentJson().toJson(c, user);
                return setPhoto(json, c);
            }
        }).value();

        if(entityManager.isOpen()) entityManager.close();
        return jsons;
    }

    @Override
    @Transactional
    public CommentJson deleteComment(Integer id) throws ApplicationThrowable {
        Comment comment = commentDAO.find(id);
        comment.setLogicalDelete(true);
        User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        comment.setDeletedAt(new Date());
        comment.setDeletedBy(user);
        try {
            commentDAO.merge(comment);
            return setPhoto(new CommentJson().toJson(comment, user), comment);
        } catch (PersistenceException ex){
            return null;
        }
    }

    @Override
    @Transactional
    public CommentJson undeleteComment(Integer id) throws ApplicationThrowable {
        Comment comment = commentDAO.find(id);
        comment.setLogicalDelete(false);
        User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        comment.setDeletedAt(null);
        comment.setDeletedBy(null);
        try {
            commentDAO.merge(comment);
            return setPhoto(new CommentJson().toJson(comment, user), comment);
        } catch (PersistenceException ex){
            return null;
        }
    }

    @Override
    public Boolean validateRecord(Integer id, Comment.RecordType type){
        boolean valid = false;
        switch (type){
            case AE:
                UploadInfoEntity upl = uploadInfoDAO.getUploadById(id);
                if(upl != null){
                    if(upl.getLogicalDelete() != null && upl.getLogicalDelete() == 0){
                        valid = true;
                    } else if(upl.getLogicalDelete() == null){
                        valid = true;
                    }
                }
                break;
            case DE:
                MiaDocumentEntity doc = documentDAO.findDocumentById(id);
                if(doc != null){
                    if(doc.getFlgLogicalDelete() != null && !doc.getFlgLogicalDelete()){
                        valid = true;
                    } else if(doc.getFlgLogicalDelete() == null) {
                        valid = true;
                    }
                }
                break;

            case GEO:
                Place plc = placeDAO.findPlaceById(id);
                if(plc != null){
                    if(plc.getLogicalDelete() != null && !plc.getLogicalDelete()){
                        valid = true;
                    } else if(plc.getLogicalDelete() == null){
                        valid = true;
                    }
                }
                break;

            case BIO:
                BiographicalPeople psn = peopleDAO.findPeopleById(id);
                if(psn != null){
                    if(psn.getLogicalDelete() != null && !psn.getLogicalDelete()){
                        valid = true;
                    } else if(psn.getLogicalDelete() == null){
                        valid = true;
                    }
                }
                break;

            case VOL:
                Volume vol = volumeDAO.find(id);
                if(vol != null){
                    if(vol.getLogicalDelete() != null && !vol.getLogicalDelete()){
                        valid = true;
                    } else if(vol.getLogicalDelete() == null){
                        valid = true;
                    }
                }
                break;

            case INS:
                InsertEntity ins = insertDAO.findInsertById(id);
                if(ins != null){
                    if(ins.getLogicalDelete() != null && !ins.getLogicalDelete()){
                        valid = true;
                    } else if(ins.getLogicalDelete() == null){
                        valid = true;
                    }
                }
                break;

            case COLL:
                break;

            case CS:
                break;


        }
        return valid;
    }

    @Override
    public GenericResponseJson reportComment(Comment comment, CommentReportMessageJson messageJson) throws ApplicationThrowable {
        GenericResponseJson response = new GenericResponseJson();
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final String username = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername()).getAccount();
        if(comment.getCreatedBy().getAccount().equals(username)){
            response.setMessage("You cannot report your own comment");
            response.setStatus("error");
            return response;
        }
        Gson gson = new Gson();
        List<String> reportedBy;
        try {
            Type listOfStrings = new TypeToken<List<String>>() {
            }.getType();
            reportedBy = gson.fromJson(comment.getReportedBy(), listOfStrings);
            if(reportedBy == null){
                reportedBy = new ArrayList<String>();
            }
            if(U.any(reportedBy, new Predicate<String>() {
                @Override
                public boolean test(String reporter) {
                    return reporter.equalsIgnoreCase(username);
                }
            })){
                response.setMessage("You have already reported this comment");
                response.setStatus("error");
                return response;
            }
            reportedBy.add(username);
            comment.setReportedBy(gson.toJson(reportedBy));
        } catch (JsonSyntaxException e) {
            reportedBy = new ArrayList<String>();
            reportedBy.add(username);
            comment.setReportedBy(gson.toJson(reportedBy));
        }
        List<User> admins = userDAO.getUserAdministrators();
        for(User user: admins){
            MessageSendJson json = new MessageSendJson();
            json.setAccount(username);
            json.setFrom(username);
            json.setTo(user.getAccount());
            json.setMessageSubject("Comment with id = "+comment.getId()+" has been reported");
            json.setMessageText("User `"+username+"` reported comment as inappropriate.<br>"
                    + "Comment #"+comment.getId()+" made by user `"+comment.getCreatedBy().getAccount()+"` at "+sdf.format(comment.getCreatedAt())
                    + "<br>Text of the comment:<br>\"" + comment.getText() + "\"<br>"
                    + "Please, review it <a href=\""+messageJson.getUrl()+"\">here</a>");
            messageService.sendNotification(user, json);
        }
        response.setStatus("success");
        response.setMessage("Comment with id = "+comment.getId()+" got successfully reported");
        return response;
    }
}
