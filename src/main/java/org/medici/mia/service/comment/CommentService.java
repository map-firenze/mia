package org.medici.mia.service.comment;

import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.comment.CommentJson;
import org.medici.mia.common.json.comment.CreateCommentJson;
import org.medici.mia.common.json.message.CommentReportMessageJson;
import org.medici.mia.domain.Comment;
import org.medici.mia.exception.ApplicationThrowable;

import java.util.HashMap;
import java.util.List;

public interface CommentService {
    public Comment findComment(Integer id) throws ApplicationThrowable;
    public Comment findActiveComment(Integer id) throws ApplicationThrowable;
    public CommentJson createComment(CreateCommentJson createJson) throws ApplicationThrowable;
    public CommentJson replyToComment(CreateCommentJson createJson, Integer id) throws ApplicationThrowable;
    public CommentJson modifyComment(Comment comment, String text, Comment repliedTo, String mentions) throws ApplicationThrowable;
    public List<CommentJson> findByRecord(Integer id, Comment.RecordType type) throws ApplicationThrowable;
    public CommentJson deleteComment(Integer id) throws ApplicationThrowable;
    public CommentJson undeleteComment(Integer id) throws ApplicationThrowable;
    public List<CommentJson> findById(Integer id) throws ApplicationThrowable;
    public CommentJson setPhoto(CommentJson json, Comment comment) throws ApplicationThrowable;
    public Boolean validateRecord(Integer id, Comment.RecordType type);
    public Boolean isAuthorized(String account) throws ApplicationThrowable;
    public Boolean isAuthorizedForDelete(String account) throws ApplicationThrowable;
    public GenericResponseJson reportComment(Comment comment, CommentReportMessageJson messageJson) throws ApplicationThrowable;
}
