package org.medici.mia.service.myarchive;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.medici.mia.common.json.CollectionJson;
import org.medici.mia.common.json.InsertJson;
import org.medici.mia.common.json.PaginationJson;
import org.medici.mia.common.json.RepositoryJson;
import org.medici.mia.common.json.SeriesJson;
import org.medici.mia.common.json.VolumeJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.folio.FolioJson;
import org.medici.mia.common.util.DocumentPrivacyUtils;
import org.medici.mia.common.util.FileUtils;
import org.medici.mia.controller.archive.BrowseArchivalLocationJson;
import org.medici.mia.controller.archive.MyArchiveBean;
import org.medici.mia.controller.archive.MyArchiveFile;
import org.medici.mia.dao.fileSharing.FileSharingDAO;
import org.medici.mia.dao.uploadfile.UploadFileDAO;
import org.medici.mia.dao.uploadinfo.UploadInfoDAO;
import org.medici.mia.domain.*;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.user.UserService;
import org.medici.mia.service.user.ValidateUserForUploads;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class GeneralArchiveServiceImpl implements GeneralArchiveService {

	private final Logger logger = Logger.getLogger(this.getClass());
	public static final String ERRORMSG_INCORRECT_INPUT = "ERROR: The Input value may be null or have no correct value.";
	public static final String WARNINGMSG_UPLOAD_CHANGETO_PUBLIC = "WARNING: The upload belongs to this file changed to public.";
	public static final String WARNINGMSG_PRIVACY_NOTUPDATED = "WARNING: The upload belongs to this file changed to public.";

	@Autowired
	private UploadInfoDAO uploadInfoDAO;

	@Autowired
	private UploadFileDAO uploadFileDAO;

	@Autowired
	private UserService userService;

	@Autowired
	private ValidateUserForUploads validateUserForUploads;

	@Autowired
	private MiaDocumentService miaDocumentService;

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

	public void setMiaDocumentService(MiaDocumentService miaDocumentService) {
		this.miaDocumentService = miaDocumentService;
	}

	protected MyArchiveBean getArchive(UploadInfoEntity upload, int numOfThumbsPerRow, int pageForFile) {
		if (upload == null) {
			return null;
		}

		MyArchiveBean archiveBean = null;

		try {
			User user = userService
					.findUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
							.getUsername());
			archiveBean = new MyArchiveBean();

			archiveBean.setUploadId(upload.getUploadInfoId());
			archiveBean.setAeName(upload.getAeName());
			archiveBean.setOwner(upload.getOwner());

			archiveBean.setAeDescription(upload.getAeDescription());

			RepositoryJson repository = new RepositoryJson();
			repository.setRepositoryId(upload.getRepositoryEntity().getRepositoryId().toString());
			repository.setRepositoryName(upload.getRepositoryEntity().getRepositoryName());
			repository.setRepositoryAbbreviation(upload.getRepositoryEntity().getRepositoryAbbreviation());
			repository.setRepositoryDescription(upload.getRepositoryEntity().getRepositoryDescription());
			repository.setRepositoryLocation(upload.getRepositoryEntity().getGoogleLocation().getgPlaceId());
			repository.setRepositoryCity(upload.getRepositoryEntity().getGoogleLocation().getCity());
			repository.setRepositoryCountry(upload.getRepositoryEntity().getGoogleLocation().getCountry());
			archiveBean.setRepository(repository);

			CollectionJson collection = new CollectionJson();
			collection.setCollectionName(upload.getCollectionEntity().getCollectionName());
			collection.setCollectionAbbreviation(upload.getCollectionEntity().getCollectionAbbreviation());
			collection.setCollectionDescription(upload.getCollectionEntity().getCollectionDescription());
			collection.setCollectionId(upload.getCollectionEntity().getCollectionId().toString());
			collection.setStudyCollection(upload.getCollectionEntity().getStudyCollection());
			archiveBean.setCollection(collection);

			SeriesJson series = new SeriesJson();
			if (upload.getSeriesEntity() != null) {
				series.setSeriesName(upload.getSeriesEntity().getTitle());
				series.setSeriesSubtitle(upload.getSeriesEntity().getSubtitle1());
				series.setSeriesId(upload.getSeriesEntity().getSeriesId().toString());
			}
			archiveBean.setSeries(series);

			archiveBean.setLogicalDelete(upload.getLogicalDelete());

			VolumeJson volume = new VolumeJson();
			volume.setVolumeName(upload.getVolumeEntity().getVolume());
			volume.setVolumeId(upload.getVolumeEntity().getSummaryId().toString());
			volume.setAeCount(upload.getVolumeEntity().getAeCount());
			volume.setHasInserts(false);
			if (upload.getVolumeEntity().getInsertEntities() != null) {
				boolean hasIns = false;
				for (InsertEntity ins : upload.getVolumeEntity().getInsertEntities()) {
					if (hasIns)
						break;
					if (ins.getLogicalDelete() != null) {
						if (!ins.getLogicalDelete()) {
							hasIns = true;
						}
					}
				}
				volume.setHasInserts(hasIns);
			}
			archiveBean.setVolume(volume);

			InsertJson insert = new InsertJson();
			if (upload.getInsertEntity() != null && upload.getInsertEntity().getInsertId() != null) {
				insert.setInsertName(upload.getInsertEntity().getInsertName());
				insert.setInsertId(upload.getInsertEntity().getInsertId().toString());
				insert.setAeCount(upload.getInsertEntity().getAeCount());
			}
			archiveBean.setInsert(insert);

			// Issue 618 If the inserEntity has no value, insertName is null
			String insName = null;
			if (upload.getInsertEntity() != null) {
				insName = upload.getInsertEntity().getInsertName();
			}

			// Build of the real path
			String realPath = org.medici.mia.common.util.FileUtils.getRealPathJSONResponse(
					upload.getRepositoryEntity().getLocation(),
					upload.getRepositoryEntity().getRepositoryAbbreviation(),
					upload.getCollectionEntity().getCollectionAbbreviation(), upload.getVolumeEntity().getVolume(),
					insName);

			if (upload.getUploadFileEntities() != null && !upload.getUploadFileEntities().isEmpty()) {
				List<UploadFileEntity> ufEntities = upload.getUploadFileEntities();

				List<MyArchiveFile> thumbsFiles = new ArrayList<MyArchiveFile>();

				PaginationJson pagination = new PaginationJson();
				pagination.setTotalPagesForSingleAe(calculateBlockNumber(numOfThumbsPerRow, ufEntities.size()));

				int start = (pageForFile - 1) * numOfThumbsPerRow;
				if (start > ufEntities.size())
					start = 0;
				int end = start + numOfThumbsPerRow;
				if (end > ufEntities.size() || numOfThumbsPerRow == 0)
					end = ufEntities.size();

				UserDetails currentUser = ((UserDetails) SecurityContextHolder.getContext().getAuthentication()
						.getPrincipal());
				for (int i = start; i < end; i++) {

					UploadFileEntity uf = ufEntities.get(i);
					MyArchiveFile thumbsFile = new MyArchiveFile();

					thumbsFile.setImageOrder(uf.getImageOrder());
					thumbsFile.setConverted(uf.getTiledConversionProcess());
					thumbsFile.setFullScreenView("");
					thumbsFile.setPrivacy(uf.getFilePrivacy());
					thumbsFile.setUploadFileId(uf.getUploadFileId());
					thumbsFile.setFilePath(
							realPath + FileUtils.THUMB_FILES_DIR + FileUtils.OS_SLASH + uf.getFilename() + "&WID=250");
					thumbsFile.setFileName(uf.getFileOriginalName());
					if (uf.getFilePrivacy() == 1) {
						if (validateUserForUploads.isSharedFileWithMe(upload.getUploadInfoId(), currentUser,
								uf.getUploadFileId())
								|| validateUserForUploads.isAdminOrOwner(upload.getOwner(), currentUser)) {
							thumbsFile.setCanView(true);
						} else {
							thumbsFile.setCanView(false);
							thumbsFile.setFilePath(null);
							thumbsFile.setFileName(null);
						}
					} else {
						thumbsFile.setCanView(true);
					}
					thumbsFile.setFolios(convertFolioEntityListToFolioJsonList(uf.getFolioEntities()));
					thumbsFiles.add(thumbsFile);
				}

				// archiveBean.setOriginalFiles(originalFiles);
				archiveBean.setThumbsFiles(thumbsFiles);
				archiveBean.setPagination(pagination);

			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return archiveBean;

	}

	protected MyArchiveBean getArchivePaginated(UploadInfoEntity upload, int numOfThumbsPerRow, int pageForFile) {
		if (upload == null) {
			return null;
		}

		MyArchiveBean archiveBean = null;

		try {
			User user = userService
					.findUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
							.getUsername());
			archiveBean = new MyArchiveBean();

			archiveBean.setUploadId(upload.getUploadInfoId());
			archiveBean.setAeName(upload.getAeName());
			archiveBean.setOwner(upload.getOwner());

			archiveBean.setAeDescription(upload.getAeDescription());

			RepositoryJson repository = new RepositoryJson();
			repository.setRepositoryId(upload.getRepositoryEntity().getRepositoryId().toString());
			repository.setRepositoryName(upload.getRepositoryEntity().getRepositoryName());
			repository.setRepositoryAbbreviation(upload.getRepositoryEntity().getRepositoryAbbreviation());
			repository.setRepositoryDescription(upload.getRepositoryEntity().getRepositoryDescription());
			repository.setRepositoryLocation(upload.getRepositoryEntity().getGoogleLocation().getgPlaceId());
			repository.setRepositoryCity(upload.getRepositoryEntity().getGoogleLocation().getCity());
			repository.setRepositoryCountry(upload.getRepositoryEntity().getGoogleLocation().getCountry());
			archiveBean.setRepository(repository);

			CollectionJson collection = new CollectionJson();
			collection.setCollectionName(upload.getCollectionEntity().getCollectionName());
			collection.setCollectionAbbreviation(upload.getCollectionEntity().getCollectionAbbreviation());
			collection.setCollectionDescription(upload.getCollectionEntity().getCollectionDescription());
			collection.setCollectionId(upload.getCollectionEntity().getCollectionId().toString());
			collection.setStudyCollection(upload.getCollectionEntity().getStudyCollection());
			archiveBean.setCollection(collection);

			SeriesJson series = new SeriesJson();
			if (upload.getSeriesEntity() != null) {
				series.setSeriesName(upload.getSeriesEntity().getTitle());
				series.setSeriesSubtitle(upload.getSeriesEntity().getSubtitle1());
				series.setSeriesId(upload.getSeriesEntity().getSeriesId().toString());
			}
			archiveBean.setSeries(series);

			archiveBean.setLogicalDelete(upload.getLogicalDelete());

			VolumeJson volume = new VolumeJson();
			volume.setVolumeName(upload.getVolumeEntity().getVolume());
			volume.setVolumeId(upload.getVolumeEntity().getSummaryId().toString());
			volume.setAeCount(upload.getVolumeEntity().getAeCount());
			volume.setHasInserts(false);
			if (upload.getVolumeEntity().getInsertEntities() != null) {
				boolean hasIns = false;
				for (InsertEntity ins : upload.getVolumeEntity().getInsertEntities()) {
					if (hasIns)
						break;
					if (ins.getLogicalDelete() != null) {
						if (!ins.getLogicalDelete()) {
							hasIns = true;
						}
					}
				}
				volume.setHasInserts(hasIns);
			}
			archiveBean.setVolume(volume);

			InsertJson insert = new InsertJson();
			if (upload.getInsertEntity() != null && upload.getInsertEntity().getInsertId() != null) {
				insert.setInsertName(upload.getInsertEntity().getInsertName());
				insert.setInsertId(upload.getInsertEntity().getInsertId().toString());
				insert.setAeCount(upload.getInsertEntity().getAeCount());
			}
			archiveBean.setInsert(insert);

			// Issue 618 If the inserEntity has no value, insertName is null
			String insName = null;
			if (upload.getInsertEntity() != null) {
				insName = upload.getInsertEntity().getInsertName();
			}

			// Build of the real path
			String realPath = org.medici.mia.common.util.FileUtils.getRealPathJSONResponse(
					upload.getRepositoryEntity().getLocation(),
					upload.getRepositoryEntity().getRepositoryAbbreviation(),
					upload.getCollectionEntity().getCollectionAbbreviation(), upload.getVolumeEntity().getVolume(),
					insName);

			if (upload.getUploadFileEntities() != null && !upload.getUploadFileEntities().isEmpty()) {
				List<UploadFileEntity> ufEntities = upload.getUploadFileEntities();

				List<MyArchiveFile> thumbsFiles = new ArrayList<MyArchiveFile>();

				PaginationJson pagination = new PaginationJson();
				pagination.setTotalPagesForSingleAe(calculateBlockNumber(numOfThumbsPerRow, ufEntities.size()));

				UserDetails currentUser = ((UserDetails) SecurityContextHolder.getContext().getAuthentication()
						.getPrincipal());
				for (UploadFileEntity uf : ufEntities) {

					MyArchiveFile thumbsFile = new MyArchiveFile();

					thumbsFile.setImageOrder(uf.getImageOrder());
					thumbsFile.setConverted(uf.getTiledConversionProcess());
					thumbsFile.setFullScreenView("");
					thumbsFile.setPrivacy(uf.getFilePrivacy());
					thumbsFile.setUploadFileId(uf.getUploadFileId());
					thumbsFile.setFilePath(
							realPath + FileUtils.THUMB_FILES_DIR + FileUtils.OS_SLASH + uf.getFilename() + "&WID=250");
					thumbsFile.setFileName(uf.getFileOriginalName());
					Boolean canCreateDoc = true;
					if (uf.getFilePrivacy() == 0) {
						List<String> owners = getMiaDocumentService()
								.findUsersRelatedDocOfImageId(uf.getUploadFileId());
						if (owners != null && owners.size() > 0) {
							for (String o : owners) {
								if (!o.equals(user.getAccount())) {
									canCreateDoc = false;
								}
							}
						}
					}
					thumbsFile.setCanSetPrivate(canCreateDoc);
					if (uf.getFilePrivacy() == 1) {
						if (validateUserForUploads.isSharedFileWithMe(upload.getUploadInfoId(), currentUser,
								uf.getUploadFileId())
								|| validateUserForUploads.isAdminOrOwnerOrFellows(upload.getOwner(), currentUser)) {
							thumbsFile.setCanView(true);
						} else {
							thumbsFile.setCanView(false);
							thumbsFile.setFilePath(null);
							thumbsFile.setFileName(null);
						}
					} else {
						thumbsFile.setCanView(true);
					}
					thumbsFile.setFolios(convertFolioEntityListToFolioJsonList(uf.getFolioEntities()));
					thumbsFiles.add(thumbsFile);
				}

				// archiveBean.setOriginalFiles(originalFiles);
				archiveBean.setThumbsFiles(thumbsFiles);
				archiveBean.setPagination(pagination);

			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return archiveBean;

	}

	private List<FolioJson> convertFolioEntityListToFolioJsonList(List<FolioEntity> folioEntities) {
		if (folioEntities == null || folioEntities.isEmpty())
			return new ArrayList<FolioJson>();
		List<FolioJson> foliosList = new ArrayList<FolioJson>();
		for (FolioEntity folio : folioEntities) {
			FolioJson folioJson = new FolioJson().toJson(folio);
			foliosList.add(folioJson);
		}
		return foliosList;
	}

	private int calculateBlockNumber(int partialNumber, Integer TotalNumber) {
		// check for the integrity of the division
		if (partialNumber != 0) {
			if (TotalNumber % partialNumber == 0)
				return TotalNumber / partialNumber;
			else
				return TotalNumber / partialNumber + 1;
		} else {
			return 0;
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<MyArchiveBean> getGeneralArchives(BrowseArchivalLocationJson locationJson, Integer numOfAePerPage,
			Integer numOfThumbsPerRow, Integer page) throws ApplicationThrowable {

		List<MyArchiveBean> archives = new ArrayList<MyArchiveBean>();

		try {

			List<UploadInfoEntity> uploads = getUploadInfoDAO().getUploadsByLocationForArchive(locationJson);

			if (uploads == null || uploads.isEmpty()) {
				return archives;
			}
			for (UploadInfoEntity upload : uploads) {
				Integer uploadInfoId = upload.getUploadInfoId();
				List<UploadFileEntity> uploadFileEntities = getUploadFileDAO().findUploadFileForArchive(uploadInfoId);
				upload.setUploadFileEntities(uploadFileEntities);
			}

			// Check if the page exists
			if (page > calculateBlockNumber(numOfAePerPage, uploads.size()) || page == 0) {
				logger.info("Page searched not found");
				return archives;
			}

			int start = (page - 1) * numOfAePerPage;
			if (start > uploads.size())
				start = 0;
			int end = start + numOfAePerPage;
			if (end > uploads.size())
				end = uploads.size();

			for (int i = start; i < end; i++) {
				MyArchiveBean archiveBean = getArchive(uploads.get(i), numOfThumbsPerRow, 1);

				if (archiveBean != null) {
					if (archiveBean.getPagination() != null) {
						archiveBean.getPagination()
								.setTotalPagesForAllAes(calculateBlockNumber(numOfAePerPage, uploads.size()));
					} else {
						PaginationJson pagination = new PaginationJson();
						pagination.setTotalPagesForAllAes(calculateBlockNumber(numOfAePerPage, uploads.size()));
						archiveBean.setPagination(pagination);

					}
					archives.add(archiveBean);
				}

			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return archives;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<MyArchiveBean> getGeneralArchives(BrowseArchivalLocationJson locationJson) throws ApplicationThrowable {

		List<MyArchiveBean> archives = new ArrayList<MyArchiveBean>();

		try {

			List<UploadInfoEntity> uploads = getUploadInfoDAO().getUploadsByLocationForArchive(locationJson);

			if (uploads == null || uploads.isEmpty()) {
				return archives;
			}
			for (UploadInfoEntity upload : uploads) {
				Integer uploadInfoId = upload.getUploadInfoId();
				List<UploadFileEntity> uploadFileEntities = getUploadFileDAO().findUploadFileForArchive(uploadInfoId);
				upload.setUploadFileEntities(uploadFileEntities);
			}

			for (UploadInfoEntity upload : uploads) {
				MyArchiveBean archiveBean = getArchive(upload, 0, 1);

				if (archiveBean != null)
					archives.add(archiveBean);

			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return archives;
	}

	public UploadInfoDAO getUploadInfoDAO() {
		return uploadInfoDAO;
	}

	public void setUploadInfoDAO(UploadInfoDAO uploadInfoDAO) {
		this.uploadInfoDAO = uploadInfoDAO;
	}

	public UploadFileDAO getUploadFileDAO() {
		return uploadFileDAO;
	}

	public void setUploadFileDAO(UploadFileDAO uploadFileDAO) {
		this.uploadFileDAO = uploadFileDAO;
	}

}