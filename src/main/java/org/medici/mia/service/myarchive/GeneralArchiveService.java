package org.medici.mia.service.myarchive;

import java.util.List;

import org.medici.mia.controller.archive.BrowseArchivalLocationJson;
import org.medici.mia.controller.archive.MyArchiveBean;
import org.medici.mia.exception.ApplicationThrowable;

public interface GeneralArchiveService {

	public List<MyArchiveBean> getGeneralArchives(
			BrowseArchivalLocationJson locationJson, Integer numOfAePerPage,
			Integer numOfThumbsPerRow, Integer page)
			throws ApplicationThrowable;

	public List<MyArchiveBean> getGeneralArchives(
			BrowseArchivalLocationJson locationJson)
			throws ApplicationThrowable;
}
