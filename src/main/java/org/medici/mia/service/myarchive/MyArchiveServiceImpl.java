package org.medici.mia.service.myarchive;

import java.io.File;
import java.util.*;
import java.util.concurrent.Future;

import com.github.underscore.Predicate;
import com.github.underscore.U;
import de.danielbechler.diff.ObjectDifferBuilder;
import de.danielbechler.diff.node.DiffNode;
import de.danielbechler.diff.node.Visit;
import org.apache.log4j.Logger;
import org.hibernate.sql.Insert;
import org.medici.mia.common.json.DocumentAEJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.GenericSetPrivacyResponseJson;
import org.medici.mia.common.json.GenericUploadResponseJson;
import org.medici.mia.common.json.PaginationJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.folio.FolioJson;
import org.medici.mia.common.util.FileUtils;
import org.medici.mia.controller.archive.MyArchiveBean;
import org.medici.mia.controller.archive.MyArchiveFile;
import org.medici.mia.controller.upload.UploadBean;
import org.medici.mia.dao.collection.CollectionDAO;
import org.medici.mia.dao.googlelocation.GoogleLocationDAO;
import org.medici.mia.dao.insert.InsertDAO;
import org.medici.mia.dao.miadoc.DocumentFileDao;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.dao.repository.RepositoryDAO;
import org.medici.mia.dao.series.SeriesDAO;
import org.medici.mia.dao.uploadfile.UploadFileDAO;
import org.medici.mia.dao.uploadinfo.UploadInfoDAO;
import org.medici.mia.dao.volume.VolumeDAO;
import org.medici.mia.domain.*;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.upload.UploadService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class MyArchiveServiceImpl extends GeneralArchiveServiceImpl implements MyArchiveService {

	private final Logger logger = Logger.getLogger(this.getClass());
	public static final String ERRORMSG_INCORRECT_INPUT = "ERROR: The Input value may be null or have no correct value.";
	public static final String WARNINGMSG_UPLOAD_CHANGETO_PUBLIC = "WARNING: The upload belongs to this file changed to public.";
	public static final String WARNINGMSG_PRIVACY_NOTUPDATED = "WARNING: The upload belongs to this file changed to public.";

	@Autowired
	private HistoryLogService historyLogService;

	@Autowired
	private UploadInfoDAO uploadInfoDAO;

	@Autowired
	private UploadFileDAO uploadFileDAO;

	@Autowired
	private UploadService uploadService;

	@Autowired
	private UserService userService;

	@Autowired
	private RepositoryDAO repositoryDAO;

	@Autowired
	private CollectionDAO collectionDAO;

	@Autowired
	private GoogleLocationDAO googleLocationDAO;

	@Autowired
	private SeriesDAO seriesDAO;

	@Autowired
	private VolumeDAO volumeDAO;

	@Autowired
	private InsertDAO insertDAO;

	@Autowired
	private DocumentFileDao docFileDao;

	@Autowired
	private GenericDocumentDao genericDocumentDao;

	@Autowired
	private MiaDocumentService miaDocumentService;

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

	public void setMiaDocumentService(MiaDocumentService miaDocumentService) {
		this.miaDocumentService = miaDocumentService;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<MyArchiveBean> getAllArchives(String owner) throws ApplicationThrowable {

		List<MyArchiveBean> archives = new ArrayList<MyArchiveBean>();

		try {

			List<UploadInfoEntity> uploads = getUploadInfoDAO().getUploadsByOwner(owner);
			if (uploads != null && !uploads.isEmpty()) {

				for (UploadInfoEntity upload : uploads) {
					MyArchiveBean archiveBean = getArchive(upload, 0, 1);

					if (archiveBean != null)
						archives.add(archiveBean);

				}
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return archives;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<MyArchiveBean> getArchives(String owner, Integer numOfAePerPage, Integer numOfThumbsPerRow,
			Integer page) throws ApplicationThrowable {

		List<MyArchiveBean> archives = new ArrayList<MyArchiveBean>();

		try {

			// In this section we get all the UploadInfo and UploadFile entities
			// and clean them from the ones who have logicalDelete=0
			List<UploadInfoEntity> uploads = getUploadInfoDAO().getUploadsByOwnerForArchive(owner);
			if (uploads == null || uploads.isEmpty()) {
				return archives;
			}
			for (UploadInfoEntity upload : uploads) {
				Integer uploadInfoId = upload.getUploadInfoId();
				List<UploadFileEntity> uploadFileEntities = getUploadFileDAO().findUploadFileForArchive(uploadInfoId);
				upload.setUploadFileEntities(uploadFileEntities);
			}
			// End upper section

			// Check if the page exists
			if (page > calculateBlockNumber(numOfAePerPage, uploads.size()) || page == 0) {
				logger.info("Page searched not found");
				return archives;
			}

			int start = (page - 1) * numOfAePerPage;
			if (start > uploads.size())
				start = 0;
			int end = start + numOfAePerPage;
			if (end > uploads.size())
				end = uploads.size();

			for (int i = start; i < end; i++) {
				MyArchiveBean archiveBean = getArchive(uploads.get(i), numOfThumbsPerRow, 1);

				if (archiveBean != null) {
					if (archiveBean.getPagination() != null) {
						archiveBean.getPagination()
								.setTotalPagesForAllAes(calculateBlockNumber(numOfAePerPage, uploads.size()));
					} else {
						PaginationJson pagination = new PaginationJson();
						pagination.setTotalPagesForAllAes(calculateBlockNumber(numOfAePerPage, uploads.size()));
						archiveBean.setPagination(pagination);

					}
					archives.add(archiveBean);
				}

			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return archives;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<MyArchiveBean> getArchivesPaginated(String owner, Integer numOfAePerPage, Integer numOfThumbsPerRow,
			Integer page) throws ApplicationThrowable {

		List<MyArchiveBean> archives = new ArrayList<MyArchiveBean>();

		try {

			// In this section we get all the UploadInfo and UploadFile entities
			// and clean them from the ones who have logicalDelete=0
			if (page <= 0) {
				logger.info("Page searched not found");
				return archives;
			}
			List<UploadInfoEntity> uploads = getUploadInfoDAO().getUploadsByOwnerForArchive(owner, page,
					numOfAePerPage);
			if (uploads == null || uploads.isEmpty()) {
				return archives;
			}
			for (UploadInfoEntity upload : uploads) {
				Integer uploadInfoId = upload.getUploadInfoId();
				List<UploadFileEntity> uploadFileEntities = getUploadFileDAO().findUploadFileForArchive(uploadInfoId, 1,
						numOfThumbsPerRow);
				upload.setUploadFileEntities(uploadFileEntities);
			}
			// End upper section

			for (UploadInfoEntity upload : uploads) {
				MyArchiveBean archiveBean = getArchivePaginated(upload, numOfThumbsPerRow, 1);

				if (archiveBean != null) {
					if (archiveBean.getPagination() != null) {
						archiveBean.getPagination()
								.setTotalPagesForAllAes(calculateBlockNumber(numOfAePerPage, uploads.size()));
					} else {
						PaginationJson pagination = new PaginationJson();
						pagination.setTotalPagesForAllAes(calculateBlockNumber(numOfAePerPage, uploads.size()));
						archiveBean.setPagination(pagination);

					}
					archives.add(archiveBean);
				}

			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return archives;
	}

	@Override
	public HashMap<String, Object> getVolumeAEsWithNotNumberedImages(Integer volumeId, Integer AEsPerPage,
			Integer ThumbsPerPage, Integer page) throws ApplicationThrowable {
		List<MyArchiveBean> archives = new ArrayList<MyArchiveBean>();
		HashMap<String, Object> resp = new HashMap<String, Object>();
		try {

			// In this section we get all the UploadInfo and UploadFile entities
			// and clean them from the ones who have logicalDelete=0
			List<UploadInfoEntity> uploads = getUploadInfoDAO().getUploadByVolumeId(volumeId);

			if (uploads == null || uploads.isEmpty()) {
				resp.put("count", 0);
				resp.put("aentities", new ArrayList<Integer>());
				return resp;
			}
			for (UploadInfoEntity upload : uploads) {
				Integer uploadInfoId = upload.getUploadInfoId();
				List<UploadFileEntity> uploadFileEntities = getUploadFileDAO().findUploadFileForArchive(uploadInfoId);
				upload.setUploadFileEntities(uploadFileEntities);
			}
			// End upper section

			// Check if the page exists
			if (page > calculateBlockNumber(AEsPerPage, uploads.size()) || page == 0) {
				logger.info("Page searched not found");
				resp.put("aentities", new ArrayList<Integer>());
				resp.put("count", uploads.size());
				return resp;
			}

			Integer count = 0;
			Integer numberedImages = 0;

			for (UploadInfoEntity upload : uploads) {
				if (upload.getOptionalImage() != null && (upload.getOptionalImage().equalsIgnoreCase("spine")
						|| upload.getOptionalImage().equalsIgnoreCase("guardia")))
					continue;
				boolean hasNotNumberedFiles = false;
				for (UploadFileEntity uploadFile : upload.getUploadFileEntities()) {
					if (someFolioIsNumbered(uploadFile.getFolioEntities())) {
						numberedImages++;
					}
					if (someFolioIsNoNumbOrToBeNumbered(uploadFile.getFolioEntities())) {
						hasNotNumberedFiles = true;
					}
				}
				if (!hasNotNumberedFiles) {
					continue;
				}
				MyArchiveBean archiveBean = removeNumberedImageFromArchive(
						getArchive(upload, ThumbsPerPage + numberedImages, 1));
				if (archiveBean.getThumbsFiles().size() > ThumbsPerPage)
					archiveBean.setThumbsFiles(archiveBean.getThumbsFiles().subList(0, ThumbsPerPage));

				if (archiveBean != null) {
					if (archiveBean.getPagination() != null) {
						archiveBean.getPagination()
								.setTotalPagesForAllAes(calculateBlockNumber(ThumbsPerPage, uploads.size()));
					} else {
						PaginationJson pagination = new PaginationJson();
						pagination.setTotalPagesForAllAes(calculateBlockNumber(AEsPerPage, uploads.size()));
						archiveBean.setPagination(pagination);

					}
					archives.add(archiveBean);
					count++;
				}
			}
			if (!(page <= 0 || AEsPerPage <= 0)) {
				int start = (page - 1) * AEsPerPage;
				if (start > archives.size())
					start = 0;
				int end = start + AEsPerPage;
				if (end > archives.size())
					end = archives.size();
				archives = archives.subList(start, end);
			}
			resp.put("count", count);

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
		resp.put("aentities", archives);

		return resp;
	}

	@Override
	public HashMap getInsertAEsWithNotNumberedImages(Integer insertId, Integer AEsPerPage, Integer ThumbsPerPage,
			Integer page) throws ApplicationThrowable {
		List<MyArchiveBean> archives = new ArrayList<MyArchiveBean>();
		HashMap<String, Object> resp = new HashMap<String, Object>();
		try {

			// In this section we get all the UploadInfo and UploadFile entities
			// and clean them from the ones who have logicalDelete=0
			List<UploadInfoEntity> uploads = getUploadInfoDAO().getUploadByInsertId(insertId);
			if (uploads == null || uploads.isEmpty()) {
				resp.put("aentities", new ArrayList<Integer>());
				resp.put("count", 0);
				return resp;
			}
			for (UploadInfoEntity upload : uploads) {
				Integer uploadInfoId = upload.getUploadInfoId();
				List<UploadFileEntity> uploadFileEntities = getUploadFileDAO().findUploadFileForArchive(uploadInfoId);
				upload.setUploadFileEntities(uploadFileEntities);
			}
			// End upper section

			// Check if the page exists
			if (page > calculateBlockNumber(AEsPerPage, uploads.size()) || page == 0) {
				logger.info("Page searched not found");
				resp.put("aentities", new ArrayList<Integer>());
				resp.put("count", uploads.size());
				return resp;
			}

			Integer count = 0;
			Integer numberedImages = 0;

			for (UploadInfoEntity upload : uploads) {
				if (upload.getOptionalImage() != null && (upload.getOptionalImage().equalsIgnoreCase("spine")
						|| upload.getOptionalImage().equalsIgnoreCase("guardia")))
					continue;
				boolean hasNotNumberedFiles = false;
				for (UploadFileEntity uploadFile : upload.getUploadFileEntities()) {
					if (someFolioIsNumbered(uploadFile.getFolioEntities())) {
						numberedImages++;
					}
					if (someFolioIsNoNumbOrToBeNumbered(uploadFile.getFolioEntities())) {
						hasNotNumberedFiles = true;
					}
				}
				if (!hasNotNumberedFiles) {
					continue;
				}
				MyArchiveBean archiveBean = removeNumberedImageFromArchive(
						getArchive(upload, ThumbsPerPage + numberedImages, 1));
				if (archiveBean.getThumbsFiles().size() > ThumbsPerPage + 1)
					archiveBean.setThumbsFiles(archiveBean.getThumbsFiles().subList(0, ThumbsPerPage));

				if (archiveBean != null) {
					if (archiveBean.getPagination() != null) {
						archiveBean.getPagination()
								.setTotalPagesForAllAes(calculateBlockNumber(ThumbsPerPage, uploads.size()));
					} else {
						PaginationJson pagination = new PaginationJson();
						pagination.setTotalPagesForAllAes(calculateBlockNumber(AEsPerPage, uploads.size()));
						archiveBean.setPagination(pagination);

					}
					archives.add(archiveBean);
					count++;
				}
			}
			if (!(page <= 0 || AEsPerPage <= 0)) {
				int start = (page - 1) * AEsPerPage;
				if (start > archives.size())
					start = 0;
				int end = start + AEsPerPage;
				if (end > archives.size())
					end = archives.size();
				archives = archives.subList(start, end);
			}
			resp.put("count", count);

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
		resp.put("aentities", archives);
		return resp;
	}

	private MyArchiveBean removeNumberedImageFromArchive(MyArchiveBean archive) {
		for (Iterator<MyArchiveFile> myArchiveFileIterator = archive.getThumbsFiles().iterator(); myArchiveFileIterator
				.hasNext();) {
			MyArchiveFile archiveFile = myArchiveFileIterator.next();
			if (archiveFile.getFolios().isEmpty() || archiveFile.getFolios() == null)
				myArchiveFileIterator.hasNext();
			else {
				boolean noNumberedIsPresent = false;
				for (Iterator<FolioJson> folioJsonIterator = archiveFile.getFolios().iterator(); folioJsonIterator
						.hasNext();) {
					FolioJson folioJson = folioJsonIterator.next();
					if (folioJson.getNoNumb() == true)
						noNumberedIsPresent = true;
				}
				if (!noNumberedIsPresent)
					myArchiveFileIterator.remove();
			}
		}
		if (archive.getThumbsFiles().isEmpty() || archive.getThumbsFiles() == null)
			return null;
		return archive;
	}

	private boolean someFolioIsNoNumbOrToBeNumbered(List<FolioEntity> folioEntities) {
		if (folioEntities == null || folioEntities.size() == 0)
			return true;
		for (FolioEntity folioEntity : folioEntities) {
			if (folioEntity.getNoNumb() == true)
				return true;
			continue;
		}
		return false;
	}

	private boolean someFolioIsNumbered(List<FolioEntity> folioEntities) {
		if (folioEntities == null || folioEntities.size() == 0)
			return false;
		for (FolioEntity folioEntity : folioEntities) {
			if (folioEntity.getNoNumb() == false)
				return true;
			continue;
		}
		return false;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public MyArchiveBean getArchiveByUploadId(Integer uploadId, Integer numOfThumbsPerRow, Integer pageForFile)
			throws ApplicationThrowable {

		MyArchiveBean archiveBean = null;

		try {
			// In this section we get all the UploadInfo and UploadFile entities
			// and clean them from the ones who have logicalDelete=0
			UploadInfoEntity upload = getUploadInfoDAO().getUploadById(uploadId);

			if (upload != null) {
				if (upload.getOptionalImage() == null) {
					List<UploadFileEntity> listUploadFiles = getUploadFileDAO().findUploadFileForArchive(uploadId);

					upload.setUploadFileEntities(listUploadFiles);

				} else {
					logger.error("The UploadInfo searched with uploadId= " + uploadId + " has optional Image != null");
					// throw new ApplicationThrowable();
					return null;
				}

			} else {
				logger.error("The UploadInfo searched with uploadId= " + uploadId + " has been deleted");
				// throw new ApplicationThrowable();
				return null;
			}

			// Check if the page exists
			int blocks = calculateBlockNumber(numOfThumbsPerRow, upload.getUploadFileEntities().size());
			if (pageForFile > blocks || pageForFile == 0) {
				logger.info("No images attached to this file - is this AE deleted?");
				// return archiveBean;
			}

			archiveBean = getArchive(upload, numOfThumbsPerRow, pageForFile);
			if (archiveBean != null) {
				if (archiveBean.getPagination() != null) {
					archiveBean.getPagination().setTotalPagesForAllAes(1);
				} else {
					PaginationJson pagination = new PaginationJson();
					pagination.setTotalPagesForAllAes(1);
					archiveBean.setPagination(pagination);

				}

			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return archiveBean;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public MyArchiveBean getArchiveByUploadIdPaginated(Integer uploadId, Integer numOfThumbsPerRow, Integer pageForFile)
			throws ApplicationThrowable {

		MyArchiveBean archiveBean = null;

		try {
			// In this section we get all the UploadInfo and UploadFile entities
			// and clean them from the ones who have logicalDelete=0
			UploadInfoEntity upload = getUploadInfoDAO().getUploadById(uploadId);

			if (upload != null) {
				if (upload.getOptionalImage() == null) {
					List<UploadFileEntity> listUploadFiles = getUploadFileDAO().findUploadFileForArchive(uploadId,
							pageForFile, numOfThumbsPerRow);

					upload.setUploadFileEntities(listUploadFiles);

				} else if (!upload.getOptionalImage().equalsIgnoreCase(
						UploadFileEntity.NO_IMAGE)){
					logger.error("The UploadInfo searched with uploadId= " + uploadId + " has optional Image != null");
					// throw new ApplicationThrowable();
					return null;
				}

			} else {
				logger.error("The UploadInfo searched with uploadId= " + uploadId + " has been deleted");
				// throw new ApplicationThrowable();
				return null;
			}

			// Check if the page exists
			int blocks = calculateBlockNumber(numOfThumbsPerRow, uploadFileDAO.countUploadFileForArchive(uploadId));
			if (pageForFile > blocks || pageForFile == 0) {
				logger.info("No images attached to this file - is this AE deleted?");
				// return archiveBean;
			}

			archiveBean = getArchivePaginated(upload, numOfThumbsPerRow, pageForFile);
			
			if (archiveBean != null) {
				if (archiveBean.getPagination() != null) {
					archiveBean.getPagination().setTotalPagesForSingleAe(blocks);
					archiveBean.getPagination().setTotalPagesForAllAes(1);
				} else {
					PaginationJson pagination = new PaginationJson();
					pagination.setTotalPagesForAllAes(1);
					archiveBean.setPagination(pagination);
				}
				
				archiveBean.setNoImage(false);
				if (upload.getOptionalImage() != null && 
					upload.getOptionalImage().equalsIgnoreCase(
							UploadFileEntity.NO_IMAGE)) {
					archiveBean.setNoImage(true);
				}
				
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return archiveBean;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public MyArchiveBean getArchiveByUploadFileId(Integer uploadFileId, Integer numOfThumbsPerRow, Integer pageForFile)
			throws ApplicationThrowable {

		MyArchiveBean archiveBean = null;

		try {
			// In this section we get all the UploadInfo and UploadFile entities
			// and clean them from the ones who have logicalDelete=0
			Integer uploadId = null;
			UploadInfoEntity upload = null;
			UploadFileEntity uploadFile = getUploadFileDAO().findUploadFile(uploadFileId);
			if (uploadFile != null) {
				uploadId = uploadFile.getIdTblUpload();

				upload = getUploadInfoDAO().getUploadById(uploadId);
			}

			if (upload != null) {
				List<UploadFileEntity> listUploadFiles = getUploadFileDAO().findUploadFileForArchive(uploadId);

				upload.setUploadFileEntities(listUploadFiles);

			} else {
				logger.error("The UploadInfo searched with uploadId= " + uploadId + " has been deleted");
				// throw new ApplicationThrowable();
				return null;
			}

			// Check if the page exists
			int blocks = calculateBlockNumber(numOfThumbsPerRow, upload.getUploadFileEntities().size());
			if (pageForFile > blocks || pageForFile == 0) {
				logger.info("Page searched not found");
				return archiveBean;
			}

			archiveBean = getArchive(upload, numOfThumbsPerRow, pageForFile);
			if (archiveBean != null) {
				if (archiveBean.getPagination() != null) {
					archiveBean.getPagination().setTotalPagesForAllAes(1);
				} else {
					PaginationJson pagination = new PaginationJson();
					pagination.setTotalPagesForAllAes(1);
					archiveBean.setPagination(pagination);

				}

			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return archiveBean;
	}

	@Override
	public Integer getArchiveFilesCountByUploadFileId(Integer uploadFileId) throws ApplicationThrowable {

		MyArchiveBean archiveBean = null;

		try {
			// In this section we get all the UploadInfo and UploadFile entities
			// and clean them from the ones who have logicalDelete=0
			Integer uploadId = null;
			UploadInfoEntity upload = null;
			UploadFileEntity uploadFile = getUploadFileDAO().findUploadFile(uploadFileId);
			if (uploadFile != null) {
				uploadId = uploadFile.getIdTblUpload();

				upload = getUploadInfoDAO().getUploadById(uploadId);
			}

			if (upload != null) {
				List<UploadFileEntity> listUploadFiles = getUploadFileDAO().findUploadFileForArchive(uploadId);

				if (listUploadFiles == null || listUploadFiles.isEmpty())
					return 0;
				else
					return listUploadFiles.size();

			}
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return 0;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericSetPrivacyResponseJson setUploadFilePrivacy(Integer uploadFileId, Integer privacyValue)
			throws ApplicationThrowable {

		int ret = 0;
		String message = "";
		GenericSetPrivacyResponseJson res = new GenericSetPrivacyResponseJson();
		res.setStatus(StatusType.ko.toString());
		res.setPrivacy(String.valueOf(privacyValue));

		// MyArchiveBean myArchive = null;
		if (uploadFileId == null || privacyValue == null) {
			logger.error("uploadFileId or privacyValue may have null value");
			res.setMessage(ERRORMSG_INCORRECT_INPUT);
			return res;
		}

		try {
			// Set the privacy on DB
			UploadFileEntity uploadFile = getUploadFileDAO().find(uploadFileId);
			List<DocumentJson> documentsFromImage = getMiaDocumentService().findDocumentsByUploadFileId(uploadFileId);
			User user = getUserService()
					.findUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
							.getUsername());
			if (documentsFromImage != null && documentsFromImage.size() > 0 && privacyValue == 1) {
				List<String> owners = getMiaDocumentService().findUsersRelatedDocOfImageId(uploadFileId);
				if (owners != null && owners.size() > 0) {
					for (String o : owners) {
						if (!o.equals(user.getAccount())) {
							res.setMessage("There are documents associated with this image.");
							return res;
						}
					}
				}
			}
			if (uploadFile == null) {
				res.setMessage("The file in the tblFileUpload does't exist. tblFileUpload not updated.");
				return res;
			}
			List<UploadFileEntity> uploadFiles = getUploadFileDAO()
					.findUploadFileForArchive(uploadFile.getIdTblUpload());
			if (uploadFile.getFilePrivacy() == privacyValue) {
				res.setMessage(
						"The privacy of file in the tblFileUpload is the same of privacy value received by FE. tblFileUpload not updated.");
				return res;
			}

			ret = getUploadFileDAO().updateUploadFilePrivacyField(uploadFileId, privacyValue);
			// update dateModified of tblUpload (because we modified at least
			// one of his own file)
			// Get the username logged in
			getUploadInfoDAO().updateDateModified(uploadFile.getUploadInfoEntity().getUploadInfoId(),
					user.getAccount());
			if (ret > 0) {
				// Control if it's upload is private

				if (uploadFile.getUploadInfoEntity() == null) {
					res.setMessage("tblUploadeFile updated but there is no uploadInfo inside upoadFile.");
					res.setStatus(StatusType.w.toString());
					return res;
				}

				Integer fileInfoPrivacyDB = uploadFile.getUploadInfoEntity().getFilePrivacy();
				if (fileInfoPrivacyDB == null) {
					fileInfoPrivacyDB = 0;
				}

				List<MiaDocumentEntity> documents = uploadFile.getMiaDocumentEntities();
				if (privacyValue == 0) {

					for (int i = 0; i < documents.size(); i++) {
						List<UploadFileEntity> files = documents.get(i).getUploadFileEntities();
						boolean isPrivate = false;
						for (int j = 0; j < files.size(); j++) {
							if (files.get(j).getFilePrivacy() == 1
									&& !files.get(j).getUploadFileId().equals(uploadFileId)) {
								isPrivate = true;
							}
						}
						if (isPrivate == false) {
							getGenericDocumentDao().updateDocument(documents.get(i).getDocumentEntityId(), 0);
						}
					}

					if (fileInfoPrivacyDB > 0) {

						// Set fileprivacy of the tblUploadefile public
						getUploadInfoDAO().updateUploadInfoPrivacyField(
								uploadFile.getUploadInfoEntity().getUploadInfoId(), 0, user.getAccount());

					}
				}
				// Control if it's upload is public and uploadFile is going to
				// be set to private and all the other remaining uploadFiles are
				// private
				// in this case the upload need to change in private, cause
				// every files became private
				else if (privacyValue == 1) {
					for (int i = 0; i < documents.size(); i++) {
						getGenericDocumentDao().updateDocument(documents.get(i).getDocumentEntityId(), 1);

					}
					// Check if the uploadFiles are all private, in this case
					// temp value will be 1 at the end of the cycle
					// if there is at least one file with privacy 0, temp will
					// be 0
					Integer temp = new Integer(1);
					for (UploadFileEntity uploadFileEntity : uploadFiles) {
						if (uploadFileEntity.getUploadFileId() != uploadFileId
								&& uploadFileEntity.getFilePrivacy() == 0) {
							temp = new Integer(0);
						}
					}

					if (fileInfoPrivacyDB == 0 && temp == 1) {

						getUploadInfoDAO().updateUploadInfoPrivacyField(
								uploadFile.getUploadInfoEntity().getUploadInfoId(), 1, user.getAccount());
					}

				}

				res.setStatus(StatusType.ok.toString());
				message = "privacy of tblUpload updated. ";

				HashMap<String, HashMap> changes = new HashMap<String, HashMap>();
				HashMap<String, HashMap> category = new HashMap<String, HashMap>();
				HashMap<String, HashMap> action = new HashMap<String, HashMap>();
				HashMap<Object, Object> before = new HashMap<Object, Object>();
				HashMap<Object, Object> after = new HashMap<Object, Object>();
				after.put("privacy", privacyValue == 0 ? "public" : "private");
				before.put("privacy", privacyValue != 0 ? "public" : "private");
				after.put("files", uploadFileId);
				before.put("files", uploadFileId);
				changes.put("before", before);
				changes.put("after", after);
				action.put("edit", changes);
				category.put("upload", action);

				historyLogService.registerAction(uploadFile.getUploadInfoEntity().getUploadInfoId(),
						HistoryLogEntity.UserAction.EDIT, HistoryLogEntity.RecordType.AE, null, category);

			} else {
				res.setStatus(StatusType.w.toString());
				res.setPrivacy(String.valueOf(uploadFile.getFilePrivacy()));
				message = "privacy of tblUpload Not updated. ";
			}

			res.setMessage(message);

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return res;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericSetPrivacyResponseJson setUploadPrivacy(Integer uploadInfoId, Integer privacyValue)
			throws ApplicationThrowable {

		UploadInfoEntity uploadInfoEntity = null;
		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

		int updatedFiles = 0;
		String message = "";
		GenericSetPrivacyResponseJson res = new GenericSetPrivacyResponseJson();
		res.setStatus(StatusType.ko.toString());
		res.setPrivacy(String.valueOf(privacyValue));

		try {

			if (uploadInfoId == null || privacyValue == null) {
				res.setMessage(ERRORMSG_INCORRECT_INPUT);
				return res;

			}
			// Getting the UploadFile list
			uploadInfoEntity = getUploadInfoDAO().find(uploadInfoId);
			// Set privacy of tblUploaded
			if (uploadInfoEntity == null) {
				res.setMessage("No upload in the tblUploaded find.");
				return res;

			}
			if (uploadInfoEntity.getUploadFileEntities() == null
					&& uploadInfoEntity.getUploadFileEntities().isEmpty()) {
				res.setMessage("No uploadFile in the tblFileUpload find.");
				return res;

			}
			// First upload privacy of the tblUploaded.
			if (uploadInfoEntity.getFilePrivacy() != privacyValue) {
				int ret = getUploadInfoDAO().updateUploadInfoPrivacyField(uploadInfoId, privacyValue,
						user.getAccount());
				if (ret > 0) {
					List<UploadFileEntity> files = uploadInfoEntity.getUploadFileEntities();
					for (int i = 0; i < files.size(); i++) {

						if (privacyValue == 1) {
							List<MiaDocumentEntity> documents = files.get(i).getMiaDocumentEntities();
							if (documents != null && documents.size() > 0) {
								Boolean canSetPrivacy = true;
								for (int j = 0; j < documents.size(); j++) {
									if (documents.get(j).getFlgLogicalDelete() == false
											&& !documents.get(j).getCreatedBy().equals(user.getAccount())) {
										canSetPrivacy = false;
									}
								}
								if (canSetPrivacy) {
									if (files.get(i).getFilePrivacy() != privacyValue) {
										int returnVal = getUploadFileDAO().updateUploadFilePrivacyField(
												files.get(i).getUploadFileId(), privacyValue);
										// If some update fails, end method
										updatedFiles = updatedFiles + returnVal;

									}
								}
							} else {
								int returnVal = getUploadFileDAO()
										.updateUploadFilePrivacyField(files.get(i).getUploadFileId(), privacyValue);
								// If some update fails, end method
								updatedFiles = updatedFiles + returnVal;
							}
						} else {
							if (files.get(i).getFilePrivacy() != privacyValue) {
								int returnVal = getUploadFileDAO()
										.updateUploadFilePrivacyField(files.get(i).getUploadFileId(), privacyValue);
								// If some update fails, end method
								updatedFiles = updatedFiles + returnVal;

							}
						}

					}
					message = "privacy of tblUpload updated. ";
				} else {
					res.setPrivacy(String.valueOf(uploadInfoEntity.getFilePrivacy()));
					message = "privacy of tblUpload Not updated. ";
				}

			} else {
				message = "Privacy of tblUpload is the same of the privacyValue from FE. Privacy of tblUpload Not updated.";

			}

			/*
			 * for (UploadFileEntity uploadFileEntity :
			 * uploadInfoEntity.getUploadFileEntities()) { // Updating the privacy of the
			 * UploadFile list if (uploadFileEntity.getFilePrivacy() != privacyValue) { int
			 * ret = getUploadFileDAO().updateUploadFilePrivacyField(uploadFileEntity.
			 * getUploadFileId(), privacyValue); // If some update fails, end method
			 * updatedFiles = updatedFiles + ret;
			 * 
			 * } // message = message + "Privacy of " // + String.valueOf(updatedFiles) +
			 * " files updated.";
			 * 
			 * }
			 */

			if (updatedFiles > 0) {

				HashMap<String, HashMap> changes = new HashMap<String, HashMap>();
				HashMap<String, HashMap> category = new HashMap<String, HashMap>();
				HashMap<String, HashMap> action = new HashMap<String, HashMap>();
				HashMap<Object, Object> before = new HashMap<Object, Object>();
				HashMap<Object, Object> after = new HashMap<Object, Object>();
				after.put("privacy", privacyValue == 0 ? "public" : "private");
				before.put("privacy", privacyValue != 0 ? "public" : "private");
				changes.put("before", before);
				changes.put("after", after);
				action.put("edit", changes);
				category.put("upload", action);

				historyLogService.registerAction(uploadInfoId, HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.AE, null, category);

				message = message + "Privacy of " + String.valueOf(updatedFiles) + " files updated.";
				res.setStatus(StatusType.ok.toString());
			} else {
				message = message + "No files privacy in tblFileUplod updated.";
				res.setStatus(StatusType.w.toString());
			}
			res.setMessage(message);

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return res;

	}

	// This method set the privacy only for tblUpload, but DO NOT update the
	// privacy of tblUploadedFiles linked to it.
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericSetPrivacyResponseJson setUploadPrivacyForAddNewFile(Integer uploadInfoId, Integer privacyValue)
			throws ApplicationThrowable {

		// MyArchiveBean myArchive = null;
		UploadInfoEntity uploadInfoEntity = null;
		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());

		String message = "";
		GenericSetPrivacyResponseJson res = new GenericSetPrivacyResponseJson();
		res.setStatus(StatusType.ko.toString());
		res.setPrivacy(String.valueOf(privacyValue));

		try {

			if (uploadInfoId == null || privacyValue == null) {
				res.setMessage(ERRORMSG_INCORRECT_INPUT);
				return res;

			}
			// Getting the UploadFile list
			uploadInfoEntity = getUploadInfoDAO().find(uploadInfoId);
			// Set privacy of tblUploaded
			if (uploadInfoEntity == null) {
				res.setMessage("No upload in the tblUploaded find.");
				return res;

			}
			if (uploadInfoEntity.getUploadFileEntities() == null
					&& uploadInfoEntity.getUploadFileEntities().isEmpty()) {
				res.setMessage("No uploadFile in the tblFileUpload find.");
				return res;

			}
			// First upload privacy of the tblUploaded.
			Integer filePrivacyInfoDB = uploadInfoEntity.getFilePrivacy();
			if (filePrivacyInfoDB == null) {
				filePrivacyInfoDB = 0;
			}
			if (filePrivacyInfoDB != privacyValue) {
				int ret = getUploadInfoDAO().updateUploadInfoPrivacyField(uploadInfoId, privacyValue,
						user.getAccount());
				if (ret > 0) {
					message = "privacy of tblUpload updated. ";
					res.setStatus(StatusType.ok.toString());
				} else {
					res.setPrivacy("Value of privacu in tblUploads" + String.valueOf(filePrivacyInfoDB));
					message = "privacy of tblUpload Not updated. ";
				}

			} else {
				message = "Privacy of tblUpload is the same of the privacyValue from FE. Privacy of tblUpload Not updated.";

			}

			res.setMessage(message);

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

		return res;

	}

	// This method is never used, but it could be useful later.
	public Integer getUploadFilePrivacy(Integer uploadFileId) throws ApplicationThrowable {
		Integer ret = null;
		if (uploadFileId != null) {
			UploadFileEntity uploadFile = getUploadFileDAO().find(uploadFileId);
			Integer priVal = uploadFile.getFilePrivacy();
			ret = priVal;
		}
		return ret;
	}

	public Integer getUploadInfoPrivacy(Integer uploadInfoId) throws ApplicationThrowable {
		Integer ret = null;
		if (uploadInfoId != null) {
			UploadInfoEntity uploadInfo = getUploadInfoDAO().find(uploadInfoId);
			if (uploadInfo.getFilePrivacy() == null)
				return 0;
			Integer priVal = uploadInfo.getFilePrivacy();
			ret = priVal;
		}
		return ret;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericUploadResponseJson repairArchive(Integer uploadFileId) throws ApplicationThrowable {

		List<GenericUploadResponseJson> result = null;
		UploadFileEntity uploadFile = null;
		GenericUploadResponseJson resp = new GenericUploadResponseJson();
		resp.setStatus(StatusType.ko.toString());
		try {
			uploadFile = getUploadFileDAO().findUploadFile(uploadFileId);
			if (uploadFile == null) {
				logger.warn("No file in tblUploadedFiles for repairing found!");
				resp.setMessage(ERRORMSG_INCORRECT_INPUT);
				return resp;
			}
			UploadInfoEntity upload = uploadFile.getUploadInfoEntity();
			if (upload == null) {
				logger.warn("No upload in tblUploads for this uploadFile for repair found!");
				resp.setMessage(ERRORMSG_INCORRECT_INPUT);
				return resp;
			}
			// Build of the real path

			// Issue 618 If the inserEntity has no value, insertName is null
			String insName = null;
			if (upload.getInsertEntity() != null) {
				insName = upload.getInsertEntity().getInsertName();
			}

			String realPath = org.medici.mia.common.util.FileUtils.getRealPath(
					upload.getRepositoryEntity().getLocation(),
					upload.getRepositoryEntity().getRepositoryAbbreviation(),
					upload.getCollectionEntity().getCollectionAbbreviation(), upload.getVolumeEntity().getVolume(),
					insName);

			List<UploadFileEntity> uploadFileEntities = new ArrayList<UploadFileEntity>();

			uploadFileEntities.add(uploadFile);
			resp.setFileName(uploadFile.getFilename());

			Future<List<GenericUploadResponseJson>> futureRes = getUploadService().convertFiles(uploadFileEntities,
					realPath, false);
			result = futureRes.get();

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
		if (result != null && !result.isEmpty()) {
			// update dateModified fields of tblUpload and tblUploadedFile
			// Get the username logged in
			User user = getUserService()
					.findUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
							.getUsername());
			GenericUploadResponseJson genericRes = result.get(0);
			getUploadInfoDAO().updateDateModified(uploadFile.getIdTblUpload(), user.getAccount());
			getUploadFileDAO().updateDateModified(uploadFile.getUploadFileId());
			resp.setMessage(genericRes.getMessage());
			resp.setStatus(genericRes.getStatus());
			return resp;

		}

		return resp;

	}

	private int calculateBlockNumber(int partialNumber, Integer TotalNumber) {
		// check for the integrity of the division
		if (partialNumber != 0) {
			if (TotalNumber % partialNumber == 0)
				return TotalNumber / partialNumber;
			else
				return TotalNumber / partialNumber + 1;
		} else {
			return 0;
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson updateImageOrder(Integer uploadFileId, int newImageOrder) throws ApplicationThrowable {

		boolean ret = false;
		boolean ret2 = false;
		GenericResponseJson res = new GenericResponseJson();
		res.setStatus(StatusType.ko.toString());

		if (uploadFileId == null || newImageOrder == 0 || newImageOrder == -1) {
			logger.error("uploadFileId or newImageOrder may have null value");
			res.setMessage(ERRORMSG_INCORRECT_INPUT);
			return res;
		}

		UploadFileEntity uploadFile = getUploadFileDAO().findUploadFile(uploadFileId);
		int oldImageOrder = uploadFile.getImageOrder();

		if (uploadFile == null || oldImageOrder == newImageOrder) {
			res.setMessage(
					"The imageOrder of file in the tblFileUpload is the same of imageOrder value received by FE. tblFileUpload not updated.");
			return res;
		}

		ret = getUploadFileDAO().updateUploadFilesImageField(getUpdatedFileList(
				uploadFile.getUploadInfoEntity().getUploadFileEntities(), oldImageOrder, newImageOrder));

		Integer uploadInfoId = uploadFile.getIdTblUpload();
		// Get the username logged in
		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
		ret2 = getUploadInfoDAO().updateDateModified(uploadInfoId, user.getAccount());

		if (ret == true && ret2 == true) {
			HashMap<String, Object> changes = new HashMap<String, Object>();
			HashMap<String, HashMap> category = new HashMap<String, HashMap>();
			HashMap<String, HashMap> action = new HashMap<String, HashMap>();
			changes.put("position", newImageOrder);
			changes.put("fileID", uploadFileId);
			action.put("edit", changes);
			category.put("reorderFile", action);

			historyLogService.registerAction(uploadInfoId, HistoryLogEntity.UserAction.EDIT,
					HistoryLogEntity.RecordType.AE, null, category);

			res.setStatus(StatusType.ok.toString());
			res.setMessage("imageOrder of tblUploadedFiles updated. ");
		} else {
			res.setStatus(StatusType.w.toString());
			res.setMessage("privacy of tblUpload Not updated. ");
		}

		return res;
	}

	public List<UploadFileEntity> getUpdatedFileList(List<UploadFileEntity> fileEntities, int oldImageOrder,
			int newImageOrder) {

		List<UploadFileEntity> fileUpdatedList = new ArrayList<UploadFileEntity>();

		for (UploadFileEntity fileEntity : fileEntities) {
			if (fileEntity.getImageOrder() == oldImageOrder) {
				fileEntity.setImageOrder(newImageOrder);
				fileUpdatedList.add(fileEntity);

			} else if (oldImageOrder < newImageOrder && fileEntity.getImageOrder() > oldImageOrder
					&& fileEntity.getImageOrder() < newImageOrder + 1) {
				fileEntity.setImageOrder(fileEntity.getImageOrder() - 1);
				fileUpdatedList.add(fileEntity);

			} else if (oldImageOrder > newImageOrder && fileEntity.getImageOrder() < oldImageOrder
					&& fileEntity.getImageOrder() > newImageOrder - 1) {
				fileEntity.setImageOrder(fileEntity.getImageOrder() + 1);
				fileUpdatedList.add(fileEntity);

			}

		}

		return fileUpdatedList;

	}

	public String[] downloadFile(Integer uploadFileId) {
		String[] returnValues = new String[2];
		UploadFileEntity uploadFile = getUploadFileDAO().findUploadFile(uploadFileId);
		if (uploadFile != null) {
			String insertName = null;

			if (uploadFile.getUploadInfoEntity().getInsertEntity() != null) {
				insertName = uploadFile.getUploadInfoEntity().getInsertEntity().getInsertName();
			}
			String path = org.medici.mia.common.util.FileUtils.getRealPath(
					uploadFile.getUploadInfoEntity().getRepositoryEntity().getLocation(),
					uploadFile.getUploadInfoEntity().getRepositoryEntity().getRepositoryAbbreviation(),
					uploadFile.getUploadInfoEntity().getCollectionEntity().getCollectionAbbreviation(),
					uploadFile.getUploadInfoEntity().getVolumeEntity().getVolume(), insertName);
			returnValues[0] = path + FileUtils.ORIGINAL_FILES_DIR + FileUtils.OS_SLASH;
			returnValues[1] = uploadFile.getFilename();
		}
		return returnValues;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson deleteFileInAE(Integer uploadFileId) {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		// Get the username logged in
		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
		String owner = user.getAccount();

		if (owner == null || owner.isEmpty()) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("User trying to delete the file is not the owner");
			return resp;
		}

		// Get the UploadFileEntity
		UploadFileEntity uploadFile = getUploadFileDAO().findUploadFile(uploadFileId);

		if (uploadFile == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage(ERRORMSG_INCORRECT_INPUT);
			return resp;
		}
		// Check if the user that is deleting the file is the user who is logged
		// in
		if (owner.equals(uploadFile.getUploadInfoEntity().getOwner())
				|| getUserService().isAccountAutorizedForDelete(user.getAccount())) {
			int oldImageOrder = uploadFile.getImageOrder();

			// Update the imageOrders of the UploadFileEntity list
			List<UploadFileEntity> uploadFileEntities = uploadFile.getUploadInfoEntity().getUploadFileEntities();
			for (UploadFileEntity uploadFileEntity : uploadFileEntities) {
				if (uploadFileEntity.getImageOrder().intValue() > oldImageOrder) {
					int newImageOrder = uploadFileEntity.getImageOrder().intValue();
					newImageOrder--;
					uploadFileEntity.setImageOrder(new Integer(newImageOrder));
				}
			}

			// Update of logicalDelete and dateDelete and imageOrder
			boolean ret = getUploadFileDAO().updateLogicalDelete(uploadFileId);
			if (ret == true) {

				UploadInfoEntity upload = uploadFile.getUploadInfoEntity();
				upload.setDateModified(new Date());
				upload.setModifiedBy(user.getAccount());
				getUploadInfoDAO().merge(upload);

				HashMap<String, Object> changes = new HashMap<String, Object>();
				HashMap<String, HashMap> category = new HashMap<String, HashMap>();
				HashMap<String, HashMap> action = new HashMap<String, HashMap>();
				changes.put("files", uploadFileId);
				action.put("delete", changes);
				category.put("uploadFile", action);

				historyLogService.registerAction(upload.getUploadInfoId(), HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.AE, null, category);

				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("Delete file in AE successfully completed");

			}
		}
		return resp;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson deleteAE(Integer uploadInfoId) throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		// Get the username logged in
		try {
			User user = getUserService()
					.findUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
							.getUsername());
			String owner = user.getAccount();

			// Get the UploadInfoEntity e UploadFileEntity list
			UploadInfoEntity uploadInfoEntity = getUploadInfoDAO().getUploadById(uploadInfoId);

			if (uploadInfoEntity == null) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Archive Entity with id = " + uploadInfoId + " does not exist");
				return resp;
			}
			if (uploadInfoEntity.getLogicalDelete() != null && uploadInfoEntity.getLogicalDelete() != 0) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Archive Entity with id = " + uploadInfoId + " is already deleted");
				return resp;
			}
			List<UploadFileEntity> uploadFileEntities = uploadInfoEntity.getUploadFileEntities();

			// Check if the user that is deleting the file is the user who is
			// logged
			// in
			if (uploadFileEntities != null && !uploadFileEntities.isEmpty()) {
				// Update logicalDelete, dateDelete and imageOrders of the
				// UploadFileEntity list

				boolean ret = getUploadFileDAO().uploadFilesDelete(uploadFileEntities);
				if (!ret) {
					resp.setMessage("UPDATE UploadFileEntity was not successfull");
					return resp;
				}
			}

			boolean ret2 = false;

			// Update logicalDelete and dateDelete of UploadInfo
			ret2 = getUploadInfoDAO().updateLogicalDelete(uploadInfoId, user.getAccount());

			if (ret2) {
				Volume volume = uploadInfoEntity.getVolumeEntity();
				InsertEntity insert = uploadInfoEntity.getInsertEntity();
				if (insert != null) {
					insert.setAeCount(insert.getAeCount() == 0 ? 0 : insert.getAeCount() - 1);
				} else {
					volume.setAeCount(volume.getAeCount() == 0 ? 0 : volume.getAeCount() - 1);
				}
				historyLogService.registerAction(uploadInfoId, HistoryLogEntity.UserAction.DELETE,
						HistoryLogEntity.RecordType.AE, null, null);
				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("Delete AE and all his files successfully completed");
			} else {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("Problems during the delete of AE, the AE or some files are NOT correctly deleted");
			}
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
		return resp;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson editAEMetadata(Integer uploadInfoId, UploadBean newUploadBean, String aeName)
			throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());

		if (uploadInfoId == null || newUploadBean == null) {
			resp.setMessage("Upload Id or upload bean or aeName non è presente.");
			return resp;
		}

		// Getting oldUpload info and real paths for moving files
		try {
			UploadInfoEntity oldUploadInfo = getUploadInfoDAO().getUploadById(

					uploadInfoId);

			newUploadBean.setInsert(newUploadBean.getInsert().getInsertId() == null ? null : newUploadBean.getInsert());

			final MyArchiveBean oldjson = new MyArchiveBean();
			oldjson.toArchiveEntityJson(oldUploadInfo, 0, 1);

			// Issue 618 If the inserEntity has no v alue, insertName is null
			String insName = null;
			if (oldUploadInfo.getInsertEntity() != null) {
				insName = oldUploadInfo.getInsertEntity().getInsertName();
			}

			String oldRealPath = org.medici.mia.common.util.FileUtils.getRealPath(
					oldUploadInfo.getRepositoryEntity().getLocation(),
					oldUploadInfo.getRepositoryEntity().getRepositoryAbbreviation(),
					oldUploadInfo.getCollectionEntity().getCollectionAbbreviation(),
					oldUploadInfo.getVolumeEntity().getVolume(), insName);

			logger.info("Edit AE metadata - Old AE path: " + oldRealPath);

			String newCollAbbrev = getCollAbbr(newUploadBean);

			// Issue 618 If the inserEntity has no value, insertName is null
			String insNameNew = null;
			if (newUploadBean.getInsert() != null) {
				insNameNew = newUploadBean.getInsert().getInsertName();
			}

			String newRealPath = org.medici.mia.common.util.FileUtils.getRealPath(
					newUploadBean.getGlEntity().getgPlaceId(), newUploadBean.getRepEntity().getRepositoryAbbreviation(),
					newCollAbbrev, newUploadBean.getVolume().getVolume(), insNameNew);

			logger.info("Edit AE metadata - New AE path: " + newRealPath);

			// Updating the DB:
			Integer repInfoId = getRepInfoId(newUploadBean, resp);
			if (repInfoId == null) {
				resp.setMessage("Repository non è presente.");
				return resp;
			}
			Integer collInfoId = getColInfoId(newUploadBean, resp, repInfoId, newCollAbbrev);
			Integer serInfoId = getSeriesId(newUploadBean, collInfoId);
			Integer volInfoId = getVolumeId(newUploadBean, collInfoId);
			Integer insInfoId = getInsInfoId(newUploadBean, volInfoId);

			// Update tblUploads with all the IDs and new values
			// Updating tblUploads

			User user = getUserService()
					.findUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
							.getUsername());

			boolean ret = getUploadInfoDAO().updateUploadInfo(uploadInfoId, aeName, newUploadBean.getAeDescription(),
					repInfoId, collInfoId, serInfoId, volInfoId, insInfoId, user.getAccount());

			// moving to new folder if update finished successfully.
			org.medici.mia.common.util.FileUtils.moveToNewDir(oldRealPath, newRealPath,
					getUploadFileDAO().findUploadFilesByUploadInfoId(uploadInfoId));

			final MyArchiveBean newjson = new MyArchiveBean();
			newjson.toArchiveEntityJson(oldUploadInfo, 0, 1);

			// Handling return
			if (ret == true) {
				Volume newVolume = volumeDAO.find(volInfoId);
				InsertEntity newInsert = null;
				if (insInfoId != null) {
					newInsert = insertDAO.findInsertById(insInfoId);
				}
				Volume oldVolume = oldUploadInfo.getVolumeEntity();
				InsertEntity oldInsert = oldUploadInfo.getInsertEntity();

				if (oldInsert != null) {
					oldInsert.setAeCount(oldInsert.getAeCount() == 0 ? 0 : oldInsert.getAeCount() - 1);
				} else if (oldVolume != null) {
					oldVolume.setAeCount(oldVolume.getAeCount() == 0 ? 0 : oldVolume.getAeCount() - 1);
				}

				if (newInsert != null) {
					newInsert.setAeCount(newInsert.getAeCount() + 1);
				} else if (newVolume != null) {
					newVolume.setAeCount(newVolume.getAeCount() + 1);
				}

				HashMap<String, HashMap> changes = new HashMap<String, HashMap>();
				HashMap<String, HashMap> category = new HashMap<String, HashMap>();
				HashMap<String, HashMap> action = new HashMap<String, HashMap>();
				final HashMap<Object, Object> before = new HashMap<Object, Object>();
				final HashMap<Object, Object> after = new HashMap<Object, Object>();
				DiffNode diff = ObjectDifferBuilder.buildDefault().compare(oldjson, newjson);
				diff.visit(new DiffNode.Visitor() {
					public void node(DiffNode node, Visit visit) {
						final Object baseValue = node.canonicalGet(oldjson);
						final Object workingValue = node.canonicalGet(newjson);
						if (node.hasChanges() && !node.getPath().toString().equals("/")) {
							if (!node.hasChildren()) {
								before.put(node.getPath().getLastElementSelector(), baseValue);
								after.put(node.getPath().getLastElementSelector(), workingValue);
							} else {
								visit.dontGoDeeper();
							}
						}
					}
				});
				if (newUploadBean.getInsert() != null || oldjson.getInsert() != null) {
					if (newUploadBean.getInsert() != null && oldjson.getInsert() != null) {
						if (!newUploadBean.getVolume().getSummaryId()
								.equals(Integer.valueOf(oldjson.getVolume().getVolumeId()))) {
							before.put("insert", oldjson.getInsert().getInsertName());
							after.put("insert", newUploadBean.getInsert().getInsertName());
						}
					}
//                    if (newUploadBean == null) {
//                        before.put("insert", oldjson.getInsert().getInsertName());
//                        after.put("insert", null);
//                    }
					if (oldjson.getVolume() == null) {
						before.put("insert", null);
						after.put("insert", newUploadBean.getInsert().getInsertName());
					}
				}
				if (newUploadBean.getVolume() != null || oldjson.getVolume() != null) {
					if (!newUploadBean.getVolume().getSummaryId()
							.equals(Integer.valueOf(oldjson.getVolume().getVolumeId()))) {
						before.put("volume", oldjson.getVolume().getVolumeName());
						after.put("volume", newUploadBean.getVolume().getVolume());
					}
//					if (newUploadBean == null) {
//						before.put("volume", oldjson.getVolume().getVolumeName());
//						after.put("volume", null);
//					}
					if (oldjson.getVolume() == null) {
						before.put("volume", null);
						after.put("volume", newUploadBean.getVolume().getVolume());
					}
				}
				if (newUploadBean.getCollEntity() != null || oldjson.getCollection() != null) {
					if (newUploadBean.getCollEntity() != null && oldjson.getCollection() != null) {
						if (!newUploadBean.getCollEntity().getCollectionId()
								.equals(Integer.valueOf(oldjson.getCollection().getCollectionId()))) {
							before.put("collection", oldjson.getCollection().getCollectionName());
							after.put("collection", newUploadBean.getCollEntity().getCollectionName());
						}
					}
					if (oldjson.getCollection() == null) {
						before.put("collection", null);
						after.put("collection", newUploadBean.getCollEntity().getCollectionName());
					}
					if (newUploadBean.getCollEntity() == null) {
						before.put("collection", oldjson.getCollection().getCollectionName());
						after.put("collection", null);
					}
				}
				if (newUploadBean.getSeries().getSeriesId() != null || oldjson.getSeries().getSeriesId() != null) {
					if (newUploadBean.getSeries().getSeriesId() != null && oldjson.getSeries().getSeriesId() != null) {
						if (!newUploadBean.getSeries().getSeriesId()
								.equals(Integer.valueOf(oldjson.getSeries().getSeriesId()))) {
							before.put("category", oldjson.getSeries().getSeriesName());
							after.put("category", newUploadBean.getSeries().getTitle());
						}
					}
					if (oldjson.getSeries().getSeriesId() == null) {
						before.put("category", null);
						after.put("category", newUploadBean.getSeries().getTitle());
					}
					if (newUploadBean.getSeries().getSeriesId() == null
							&& newUploadBean.getSeries().getTitle().length() == 0
							&& oldjson.getSeries().getSeriesName().length() > 0) {
						before.put("category", oldjson.getSeries().getSeriesName());
						after.put("category", null);
					}
				}

				changes.put("before", before);
				changes.put("after", after);
				action.put("edit", changes);
				category.put("metadata", action);

				if (before.size() > 0 && after.size() > 0) {
					historyLogService.registerAction(uploadInfoId, HistoryLogEntity.UserAction.EDIT,
							HistoryLogEntity.RecordType.AE, null, category);
				}

				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("Edit AE metadata successfully completed");
			} else {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Edit AE metadata NOT completed");
			}

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
		return resp;
	}

	private Integer getSeriesId(UploadBean newUploadBean, Integer collInfoId) throws ApplicationThrowable {

		Integer serInfoId = null;
		if (newUploadBean.getSeries().getTitle() == null) {
			newUploadBean.getSeries().setTitle("");
			newUploadBean.getSeries().setSubtitle1("");
		}
		newUploadBean.getSeries().setCollection(collInfoId);
		if (newUploadBean.getSeries().getSeriesId() == null) {
			// New Series
			// Validation of Series before insert
			// criteria: Cannot be two Series with the same Title and Collection
			// Id
			List<SeriesEntity> listByTitleCollId = getSeriesDAO().findMatchSeriesByTitle(
					newUploadBean.getSeries().getTitle(), newUploadBean.getSeries().getCollection());
			if (listByTitleCollId.size() != 0) {
				// Series already exist in DB, avoid duplicate using the Id of
				// the existing one
				SeriesEntity ser = listByTitleCollId.get(0);
				serInfoId = ser.getSeriesId();
			} else {
				// Series not exist, insert in DB
				serInfoId = getSeriesDAO().insertSeries(newUploadBean.getSeries());
			}
		} else {
			getSeriesDAO().mergeSeries(newUploadBean.getSeries());
			serInfoId = newUploadBean.getSeries().getSeriesId();
		}
		return serInfoId;

	}

	private Integer getVolumeId(UploadBean newUploadBean, Integer collInfoId) throws ApplicationThrowable {

		Integer volInfoId = null;

		if (newUploadBean.getVolume().getVolume() == null) {
			newUploadBean.getVolume().setVolume("");
		}
		newUploadBean.getVolume().setCollection(collInfoId);

		if (newUploadBean.getVolume().getSummaryId() == null) {

			// Validation of Volume before insert
			// criteria: Cannot be two Volume with the same Name and Collection
			// ID
			List<Volume> listByNameCollId = getVolumeDAO().findMatchVolumeByName(newUploadBean.getVolume().getVolume(),
					newUploadBean.getVolume().getCollection());
			if (listByNameCollId.size() != 0) {
				// Volume already exist in DB, avoid duplicate using the Id of
				// the existing one
				Volume vol = listByNameCollId.get(0);
				volInfoId = vol.getSummaryId();
			} else {
				// Volume not exist, insert in DB
				volInfoId = getVolumeDAO().insertVolume(newUploadBean.getVolume());
			}
		} else {
			getVolumeDAO().mergeVolume(newUploadBean.getVolume());
			volInfoId = newUploadBean.getVolume().getSummaryId();
		}

		return volInfoId;
	}

	private Integer getInsInfoId(UploadBean newUploadBean, Integer volInfoId) throws ApplicationThrowable {

		Integer insInfoId = null;

		// Issue 618 If the inserEntity has no value, insertName is null

		if (newUploadBean == null || newUploadBean.getInsert() == null) {
			return null;
		}

		newUploadBean.getInsert().setVolume(volInfoId);

		if (newUploadBean.getInsert().getInsertName() == null) {
			newUploadBean.getInsert().setInsertName("");
		}

		if (newUploadBean.getInsert().getInsertId() == null) {
			// New Insert!
			// Validation of Insert before insert in DB
			// criteria: Cannot be two Insert with the same Name and Volume ID
			List<InsertEntity> listByNameVolId = getInsertDAO().findMatchInsertByName(
					newUploadBean.getInsert().getInsertName(), newUploadBean.getInsert().getVolume());
			if (listByNameVolId != null && !listByNameVolId.isEmpty()) {
				// Insert already exist in DB, avoid duplicate using the Id of
				// the existing one
				insInfoId = listByNameVolId.get(0).getInsertId();
			} else {
				// Insert not exist, insert in DB
				InsertEntity insertNew = newUploadBean.getInsert();
				User user = getUserService()
						.findUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
								.getUsername());
				insertNew.setCreatedBy(user);
				insertNew.setDateCreated(new Date());
				if (insertNew.getLogicalDelete() == null) {
					insertNew.setLogicalDelete(false);
					;
				}
				insInfoId = getInsertDAO().insertInsert(insertNew);
			}
		} else {
			// Change of insert
			getInsertDAO().mergeInsert(newUploadBean.getInsert());
			insInfoId = newUploadBean.getInsert().getInsertId();

		}

		return insInfoId;

	}

	public String getCollAbbr(UploadBean uploadBean) {

		String newAbbr = uploadBean.getCollEntity().getCollectionAbbreviation();
		if (newAbbr == null || newAbbr.isEmpty()) {
			// Automatic creation of collection abbreviation

			StringBuilder abbrev = new StringBuilder();
			String collName = uploadBean.getCollEntity().getCollectionName();
			String[] nameList = collName.split(" ");
			for (String str : nameList) {
				abbrev.append(str.substring(0, 1).toUpperCase());
			}

			// If an abbreviation with same repository and same
			// collection already exist, add a _1
			Integer size = getCollectionDAO().findNumberOfCollectionAbbreviation(abbrev.toString(),
					uploadBean.getCollEntity().getRepositoryId());
			if (size != 0) {
				abbrev.append(size);
			}

			// Set of abbreviation
			newAbbr = abbrev.toString();
		}

		return newAbbr;
	}

	private Integer getRepInfoId(UploadBean upload, GenericResponseJson resp) {
		Integer repInfoId = null;
		if (upload.getRepEntity().getRepositoryId() == null) {
			if (upload.getGlEntity().getgPlaceId() == null || upload.getGlEntity().getgPlaceId().isEmpty()) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Google Location Id cant be wmty or null.");
				return null;
			}

			if (getGoogleLocationDAO().getLocationById(upload.getGlEntity().getgPlaceId()) == null) {
				// googleLocation doesn't exist . inser google Location
				getGoogleLocationDAO().saveLocation(upload.getGlEntity());
			}

			// Repository not exist, insert in DB - Validation of Repository
			// done in FE
			repInfoId = getRepositoryDAO().insertRepository(upload.getRepEntity());
		} else {
			repInfoId = upload.getRepEntity().getRepositoryId();
		}

		return repInfoId;

	}

	private Integer getColInfoId(UploadBean newUploadBean, GenericResponseJson resp, Integer repInfoId,
			String newCollAbbrev) {
		Integer collInfoId = null;
		// Edit of Collection
		if (newUploadBean.getCollEntity().getCollectionId() == null) {
			// Set repository field on collection Entity
			newUploadBean.getCollEntity().setRepositoryId(repInfoId);
			// Collection not exist, insert in DB - Validation of Collection in
			// FE

			newUploadBean.getCollEntity().setCollectionAbbreviation(newCollAbbrev);

			if (newUploadBean.getCollEntity().getCollectionDescription() == null) {
				newUploadBean.getCollEntity().setCollectionDescription("");
			}

			collInfoId = getCollectionDAO().insertCollection(newUploadBean.getCollEntity());
		} else {
			collInfoId = newUploadBean.getCollEntity().getCollectionId();
		}

		return collInfoId;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public DocumentAEJson checkDocForDeleteFileInAE(Integer uploadFileId) throws ApplicationThrowable {

		DocumentAEJson resp = new DocumentAEJson();

		// Get the username logged in
		try {
			User user = getUserService()
					.findUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
							.getUsername());
			String owner = user.getAccount();

			if (owner == null || owner.isEmpty()) {
				return resp;
			}

			// Get the UploadInfoEntity e UploadFileEntity list

			UploadFileEntity uploadFileEntity = getUploadFileDAO().findUploadFile(uploadFileId);
			if (uploadFileEntity == null || uploadFileEntity.getUploadInfoEntity() == null) {
				return null;
			}

			UploadInfoEntity uploadInfoEntity = getUploadInfoDAO()
					.getUploadById(uploadFileEntity.getUploadInfoEntity().getUploadInfoId());

			if (uploadInfoEntity == null) {
				return null;
			}

			if (!owner.equals(uploadInfoEntity.getOwner())) {
				resp.setIsOwner("NO");
				return resp;

			}

			resp.setIsOwner("SI");
			// Controlling for the existance of document
			List<Integer> documentIds = new ArrayList<Integer>();

			List<DocumentFileJoinEntity> files = getDocFileDao().findDocumentsByFileId(uploadFileId);
			if (files != null && !files.isEmpty()) {
				for (DocumentFileJoinEntity docJoinFile : files) {
					Boolean isLogicalDelete = getGenericDocumentDao().findDocLogicalDelete(docJoinFile.getDocumentId());
					if (!isLogicalDelete && !documentIds.contains(docJoinFile.getDocumentId())) {
						documentIds.add(docJoinFile.getDocumentId());
					}

				}
			}

			if (documentIds != null && !documentIds.isEmpty()) {
				resp.setDocumentIds(documentIds);
			}

			return resp;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public DocumentAEJson checkDocForDeleteAE(Integer uploadInfoId) throws ApplicationThrowable {

		DocumentAEJson resp = new DocumentAEJson();

		// Get the username logged in
		try {
			User user = getUserService()
					.findUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
							.getUsername());
			String owner = user.getAccount();

			if (owner == null || owner.isEmpty()) {
				return resp;
			}

			// Get the UploadInfoEntity e UploadFileEntity list
			UploadInfoEntity uploadInfoEntity = getUploadInfoDAO().getUploadById(uploadInfoId);

			if (uploadInfoEntity == null) {
				return null;
			}

			if (!owner.equals(uploadInfoEntity.getOwner())) {
				resp.setIsOwner("NO");
			} else {
				resp.setIsOwner("SI");
			}
			List<UploadFileEntity> uploadFileEntities = uploadInfoEntity.getUploadFileEntities();
			if (uploadFileEntities == null || uploadFileEntities.isEmpty()) {
				return null;
			}
			// Controlling for the existance of document
			List<Integer> documentIds = new ArrayList<Integer>();
			for (UploadFileEntity uploadFile : uploadFileEntities) {
				List<DocumentFileJoinEntity> files = getDocFileDao()
						.findDocumentsByFileId(uploadFile.getUploadFileId());
				if (files != null && !files.isEmpty()) {
					for (DocumentFileJoinEntity docJoinFile : files) {
						Boolean isLogicalDelete = getGenericDocumentDao()
								.findDocLogicalDelete(docJoinFile.getDocumentId());
						if (!isLogicalDelete && !documentIds.contains(docJoinFile.getDocumentId())) {
							documentIds.add(docJoinFile.getDocumentId());
						}

					}
				}
			}

			if (documentIds != null && !documentIds.isEmpty()) {
				resp.setDocumentIds(documentIds);
			}

			return resp;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GenericResponseJson unDeleteAE(Integer uploadInfoId) throws ApplicationThrowable {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		// Get the username logged in
		try {

			// Get the UploadInfoEntity e UploadFileEntity list
			UploadInfoEntity uploadInfoEntity = getUploadInfoDAO().getUploadById(uploadInfoId);

			if (uploadInfoEntity == null) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("Archive Entity with id = " + uploadInfoId + " does not exist");
				return resp;
			}

			String oldRealPath = org.medici.mia.common.util.FileUtils.getRealPath(
					uploadInfoEntity.getRepositoryEntity().getLocation(),
					uploadInfoEntity.getRepositoryEntity().getRepositoryAbbreviation(),
					uploadInfoEntity.getCollectionEntity().getCollectionAbbreviation(),
					uploadInfoEntity.getVolumeEntity().getVolume(), uploadInfoEntity.getInsertEntity() == null ? null
							: uploadInfoEntity.getInsertEntity().getInsertName());

			User user = getUserService()
					.findUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
							.getUsername());

			Volume volume = uploadInfoEntity.getVolumeEntity();
			InsertEntity insert = uploadInfoEntity.getInsertEntity();

			// if user restores AE in the volume, but this volume now has inserts, AE should
			// be placed in the same volume
			// in the insert named "Restored"
			if (uploadInfoEntity.getLogicalDelete() != 0) {
				if (insert == null) {
					boolean hasInserts = U.any(volume.getInsertEntities(), new Predicate<InsertEntity>() {
						@Override
						public boolean test(InsertEntity ins) {
							if (ins.getLogicalDelete() == null) {
								return true;
							}
							return !ins.getLogicalDelete();
						}
					});
					if (hasInserts) {
						InsertEntity restoredVault = null;
						for (InsertEntity ins : volume.getInsertEntities()) {
							if (ins.getInsertName().equals("Restored")) {
								restoredVault = ins;
							}
						}
						if (restoredVault == null) {
							restoredVault = new InsertEntity();
							restoredVault.setInsertName("Restored");
							restoredVault.setVolume(volume.getSummaryId());
							restoredVault.setVolumeEntity(volume);
							restoredVault.setLogicalDelete(false);
							restoredVault.setCreatedBy(user);
							restoredVault.setDateCreated(new Date());
							insertDAO.persist(restoredVault);
						}
						uploadInfoEntity.setInsert(restoredVault.getInsertId());
						uploadInfoEntity.setInsertEntity(restoredVault);
						restoredVault.setAeCount(restoredVault.getAeCount() + 1);
					} else {
						volume.setAeCount(volume.getAeCount() + 1);
					}
				} else {
					insert.setAeCount(insert.getAeCount() + 1);
				}
			} else {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("Archive Entity with id = " + uploadInfoId + " is already active");
				return resp;
			}

			if (uploadInfoEntity.getUploadFileEntities() != null
					&& !uploadInfoEntity.getUploadFileEntities().isEmpty()) {
				for (UploadFileEntity file : uploadInfoEntity.getUploadFileEntities()) {
					file.setLogicalDelete(0);
					file.setDateDeleted(null);
				}
			}

			String newRealPath = org.medici.mia.common.util.FileUtils.getRealPath(
					uploadInfoEntity.getRepositoryEntity().getLocation(),
					uploadInfoEntity.getRepositoryEntity().getRepositoryAbbreviation(),
					uploadInfoEntity.getCollectionEntity().getCollectionAbbreviation(),
					uploadInfoEntity.getVolumeEntity().getVolume(), uploadInfoEntity.getInsertEntity() == null ? null
							: uploadInfoEntity.getInsertEntity().getInsertName());

			if (!newRealPath.equals(oldRealPath)) {
				org.medici.mia.common.util.FileUtils.moveToNewDir(oldRealPath, newRealPath,
						getUploadFileDAO().findUploadFilesByUploadInfoId(uploadInfoId));
			}

			uploadInfoEntity.setLogicalDelete(0);
			uploadInfoEntity.setModifiedBy(user.getAccount());
			uploadInfoEntity.setDateModified(new Date());
			uploadInfoEntity.setDateDeleted(null);

			getUploadInfoDAO().merge(uploadInfoEntity);

			historyLogService.registerAction(uploadInfoId, HistoryLogEntity.UserAction.EDIT,
					HistoryLogEntity.RecordType.AE, null, null);

			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Archive unDeleted.");

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
		return resp;
	}

	@Override
	public Volume findVolumeById(Integer id) throws ApplicationThrowable {
		return volumeDAO.find(id);
	}

	@Override
	public InsertEntity findInsertById(Integer id) throws ApplicationThrowable {
		return insertDAO.findInsertById(id);
	}

	@Override
	public CollectionEntity findCollectionById(Integer id) throws ApplicationThrowable {
		return collectionDAO.find(id);
	}

	@Override
	public List<InsertEntity> findInsertByName(String name, Integer volumeId) throws ApplicationThrowable {
		return insertDAO.findInsertByName(name, volumeId);
	}

	@Override
	public List<Volume> findVolumeByName(String name, Integer collectionId) throws ApplicationThrowable {
		return volumeDAO.findVolumeByName(name, collectionId);
	}

	@Override
	public void saveVolume(Volume entity) throws ApplicationThrowable {
		volumeDAO.persist(entity);
	}

	@Override
	public void saveInsert(InsertEntity entity) throws ApplicationThrowable {
		insertDAO.persist(entity);
	}

	@Override
	public Integer countAEsForOwner(String owner) throws ApplicationThrowable {
		Integer count = uploadInfoDAO.countUploadsByOwnerForArchive(owner);
		if (count == null)
			return 0;
		return count;
	}

	@Override
	public Integer countImagesForAE(Integer uploadId) throws ApplicationThrowable {
		Integer count = uploadFileDAO.countUploadFileForArchive(uploadId);
		if (count == null)
			return 0;
		return count;
	}

	@Override
	public HashMap<String, Object> findPageForFile(Integer uploadId, Integer fileId, Integer perPage)
			throws ApplicationThrowable {
		Integer page = uploadInfoDAO.findPageForFile(uploadId, fileId, perPage);
		if (page == null)
			return null;
		HashMap<String, Object> responseData = new HashMap<String, Object>();
		responseData.put("page", page);
		return responseData;
	}
	
	@Override
	public UploadFileEntity getLastUploadFileEntity(Integer uploadId) {
		return getUploadFileDAO().findLastUploadFileByUploadInfoId(uploadId);
	}


	public UploadInfoDAO getUploadInfoDAO() {
		return uploadInfoDAO;
	}

	public void setUploadInfoDAO(UploadInfoDAO uploadInfoDAO) {
		this.uploadInfoDAO = uploadInfoDAO;
	}

	public UploadFileDAO getUploadFileDAO() {
		return uploadFileDAO;
	}

	public void setUploadFileDAO(UploadFileDAO uploadFileDAO) {
		this.uploadFileDAO = uploadFileDAO;
	}

	public UploadService getUploadService() {
		return uploadService;
	}

	public void setUploadService(UploadService uploadService) {
		this.uploadService = uploadService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public RepositoryDAO getRepositoryDAO() {
		return repositoryDAO;
	}

	public void setRepositoryDAO(RepositoryDAO repositoryDAO) {
		this.repositoryDAO = repositoryDAO;
	}

	public CollectionDAO getCollectionDAO() {
		return collectionDAO;
	}

	public void setCollectionDAO(CollectionDAO collectionDAO) {
		this.collectionDAO = collectionDAO;
	}

	public GoogleLocationDAO getGoogleLocationDAO() {
		return googleLocationDAO;
	}

	public void setGoogleLocationDAO(GoogleLocationDAO googleLocationDAO) {
		this.googleLocationDAO = googleLocationDAO;
	}

	public SeriesDAO getSeriesDAO() {
		return seriesDAO;
	}

	public void setSeriesDAO(SeriesDAO seriesDAO) {
		this.seriesDAO = seriesDAO;
	}

	public VolumeDAO getVolumeDAO() {
		return volumeDAO;
	}

	public void setVolumeDAO(VolumeDAO volumeDAO) {
		this.volumeDAO = volumeDAO;
	}

	public InsertDAO getInsertDAO() {
		return insertDAO;
	}

	public void setInsertDAO(InsertDAO insertDAO) {
		this.insertDAO = insertDAO;
	}

	public DocumentFileDao getDocFileDao() {
		return docFileDao;
	}

	public void setDocFileDao(DocumentFileDao docFileDao) {
		this.docFileDao = docFileDao;
	}

	public GenericDocumentDao getGenericDocumentDao() {
		return genericDocumentDao;
	}

	public void setGenericDocumentDao(GenericDocumentDao genericDocumentDao) {
		this.genericDocumentDao = genericDocumentDao;
	}

}