
package org.medici.mia.service.myarchive;

import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.DocumentAEJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.GenericSetPrivacyResponseJson;
import org.medici.mia.common.json.GenericUploadResponseJson;
import org.medici.mia.controller.archive.MyArchiveBean;
import org.medici.mia.controller.upload.UploadBean;
import org.medici.mia.domain.CollectionEntity;
import org.medici.mia.domain.InsertEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.Volume;
import org.medici.mia.exception.ApplicationThrowable;


public interface MyArchiveService extends GeneralArchiveService{


	public List<MyArchiveBean> getAllArchives(String owner) throws ApplicationThrowable;
	public List<MyArchiveBean> getArchives(String owner, Integer numOfAePerPage, Integer numOfThumbsPerRow, Integer page) throws ApplicationThrowable;	
	public MyArchiveBean getArchiveByUploadId(Integer uploadIdInteger, Integer numOfThumbsPerRow, Integer pageForFile) throws ApplicationThrowable;	
	public MyArchiveBean getArchiveByUploadFileId(Integer uploadFileIdInteger, Integer numOfThumbsPerRow, Integer pageForFile) throws ApplicationThrowable;	
	public GenericUploadResponseJson repairArchive(Integer uploadId) throws ApplicationThrowable;
	public GenericSetPrivacyResponseJson setUploadFilePrivacy(Integer uploadFileId, Integer privacyValue) throws ApplicationThrowable;
	public GenericSetPrivacyResponseJson setUploadPrivacy(Integer uploadInfoId, Integer privacyValue) throws ApplicationThrowable;
	public GenericSetPrivacyResponseJson setUploadPrivacyForAddNewFile (Integer uploadInfoId, Integer privacyValue) throws ApplicationThrowable;
	public Integer getUploadFilePrivacy(Integer uploadFileId) throws ApplicationThrowable;
	public Integer getUploadInfoPrivacy(Integer uploadInfoId) throws ApplicationThrowable;
	public GenericResponseJson updateImageOrder(Integer uploadFileId, int newImageOrder) throws ApplicationThrowable;
	public String[] downloadFile(Integer uploadFileId) throws ApplicationThrowable;
	public GenericResponseJson deleteFileInAE(Integer uploadFileId) throws ApplicationThrowable;
	public GenericResponseJson deleteAE(Integer uploadInfoId) throws ApplicationThrowable;
	public GenericResponseJson editAEMetadata(Integer uploadInfoId, UploadBean newUploadBean, String aeName) throws ApplicationThrowable;
	public DocumentAEJson checkDocForDeleteAE(Integer uploadInfoId) throws ApplicationThrowable;
	public DocumentAEJson checkDocForDeleteFileInAE(Integer uploadInfoId) throws ApplicationThrowable;
	public GenericResponseJson unDeleteAE(Integer uploadInfoId) throws ApplicationThrowable;
	public HashMap getVolumeAEsWithNotNumberedImages(Integer volumeId, Integer AEsPerPage, Integer ThumbsPerPage, Integer page) throws ApplicationThrowable;
	public HashMap getInsertAEsWithNotNumberedImages(Integer insertId, Integer AEsPerPage, Integer ThumbsPerPage, Integer page) throws ApplicationThrowable;
	public HashMap<String, Object> findPageForFile(Integer uploadId, Integer fileId, Integer perPage) throws ApplicationThrowable;
	public MyArchiveBean getArchiveByUploadIdPaginated(Integer uploadId, Integer numOfThumbsPerRow, Integer pageForFile) throws ApplicationThrowable;
	public List<MyArchiveBean> getArchivesPaginated(String owner, Integer numOfAePerPage, Integer numOfThumbsPerRow, Integer page) throws ApplicationThrowable;
	public Integer countAEsForOwner(String owner) throws ApplicationThrowable;
	public Integer countImagesForAE(Integer uploadId) throws ApplicationThrowable;
	public Volume findVolumeById(Integer id) throws ApplicationThrowable;
	public List<Volume> findVolumeByName(String name, Integer collectionId) throws ApplicationThrowable;
	public InsertEntity findInsertById(Integer id) throws ApplicationThrowable;
	public List<InsertEntity> findInsertByName(String name, Integer volumeId) throws ApplicationThrowable;
	public CollectionEntity findCollectionById(Integer id) throws ApplicationThrowable;
	public void saveVolume(Volume entity) throws ApplicationThrowable;
	public void saveInsert(InsertEntity entity) throws ApplicationThrowable;
	public Integer getArchiveFilesCountByUploadFileId(Integer uploadFileId) throws ApplicationThrowable;
	public UploadFileEntity getLastUploadFileEntity(Integer uploadId);
	
}
