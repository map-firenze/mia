package org.medici.mia.service.collation;

import org.medici.mia.common.json.collation.*;
import org.medici.mia.domain.Collation;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.exception.ApplicationThrowable;

import java.util.HashMap;
import java.util.List;

public interface CollationService {
    CollationJson createAttachment(CreateAttachmentCollationJson json)
            throws ApplicationThrowable;
    CollationJson createLacuna(CreateLacunaCollationJson json)
            throws ApplicationThrowable;
    List<CollationJson> findAttachmentsByDocumentId(Integer documentId)
            throws ApplicationThrowable;
    CollationJson modifyCollation(Collation collation, HashMap<String, Object> modifyJson)
            throws ApplicationThrowable;
    List<HashMap> findCollationsForUploadFile(UploadFileEntity file, Integer uploadInfoId)
            throws ApplicationThrowable;
    Boolean deleteCollation(Collation collation)
            throws ApplicationThrowable;
    Boolean undeleteCollation(Collation collation)
            throws ApplicationThrowable;
    List<UploadFileEntity> findCollatedFilesInVolume(Integer volumeId)
            throws ApplicationThrowable;
    List<UploadFileEntity> findCollatedFilesInInsert(Integer insertId)
            throws ApplicationThrowable;
    List<CollationJson> findLacunaByDocumentId(Integer documentId)
            throws ApplicationThrowable;
    MiaDocumentEntity findDocumentById(Integer documentId)
            throws ApplicationThrowable;
    List<Collation> findByDocumentId(Integer documentId)
            throws ApplicationThrowable;
    List<Collation> findByDocumentId(Integer documentId, Collation.CollationType type)
            throws ApplicationThrowable;
    Collation findById(Integer id)
            throws ApplicationThrowable;
    Boolean checkIfAlreadyParent(Integer documentId)
            throws ApplicationThrowable;
    Boolean checkIfAlreadyChild(Integer documentId)
            throws ApplicationThrowable;
    Boolean checkIfAttachmentExists(Integer childId, Integer parentId)
            throws ApplicationThrowable;
}
