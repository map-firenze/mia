package org.medici.mia.service.collation;

import com.github.underscore.U;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.internal.LinkedTreeMap;
import org.medici.mia.common.json.collation.*;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.dao.collation.CollationDAO;
import org.medici.mia.dao.insert.InsertDAO;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.dao.uploadfile.UploadFileDAO;
import org.medici.mia.dao.volume.VolumeDAO;
import org.medici.mia.domain.*;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.folio.Folio;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.PersistenceException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CollationServiceImpl implements CollationService {

    @Autowired
    private GenericDocumentDao documentDAO;

    @Autowired
    private CollationDAO collationDAO;

    @Autowired
    private UserService userService;

    @Autowired
    private MiaDocumentService miaDocumentService;

    @Autowired
    private UploadFileDAO uploadFileDAO;

    @Autowired
    private VolumeDAO volumeDAO;

    @Autowired
    private InsertDAO insertDAO;

    @Override
    public MiaDocumentEntity findDocumentById(Integer documentId) throws ApplicationThrowable {
        return documentDAO.findDocumentById(documentId);
    }

    @Override
    public List<Collation> findByDocumentId(Integer documentId) throws ApplicationThrowable {
        return collationDAO.findByDocumentId(documentId);
    }

    @Override
    public List<Collation> findByDocumentId(Integer documentId, Collation.CollationType type) throws ApplicationThrowable {
        return collationDAO.findByDocumentId(documentId, type);
    }

    @Override
    public Collation findById(Integer id) throws ApplicationThrowable {
        return collationDAO.find(id);
    }

    @Override
    public Boolean checkIfAlreadyParent(Integer documentId) throws ApplicationThrowable {
        return collationDAO.checkIfAlreadyParent(documentId);
    }

    @Override
    public Boolean checkIfAlreadyChild(Integer documentId) throws ApplicationThrowable {
        return collationDAO.checkIfAlreadyChild(documentId);
    }

    @Override
    public Boolean checkIfAttachmentExists(Integer childId, Integer parentId) throws ApplicationThrowable {
        return collationDAO.checkIfAttachmentExists(childId, parentId);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public CollationJson createAttachment(CreateAttachmentCollationJson createJson)
            throws ApplicationThrowable{
        List<MiaDocumentEntity> entities = new ArrayList<MiaDocumentEntity>();
        MiaDocumentEntity childDocument = documentDAO.findDocumentById(createJson.getChildDocumentId());
        MiaDocumentEntity parentDocument = documentDAO.findDocumentById(createJson.getParentDocumentId());
        entities.add(childDocument);
        entities.add(parentDocument);
        User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        Collation collation = createJson.toEntity(entities, user);
        collationDAO.saveCollation(collation);
        CollationJson json = new CollationJson();
        List<DocumentJson> documentJsons = miaDocumentService.createDocumentsJsonFromDocEntity(entities);
        json.toJson(collation, documentJsons);
        return json;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public CollationJson createLacuna(CreateLacunaCollationJson createJson)
            throws ApplicationThrowable{
        MiaDocumentEntity childDocument = documentDAO.findDocumentById(createJson.getChildDocumentId());
        UploadFileEntity file = uploadFileDAO.findUploadFile(createJson.getUploadFileId());
        HashMap<String, Object> volInsHash = new HashMap<String, Object>();
        if(createJson.getInsertId() != null){
            volInsHash.put("parentInsert", insertDAO.findInsertById(createJson.getInsertId()));
        }
        if(createJson.getVolumeId() != null){
            volInsHash.put("parentVolume", volumeDAO.find(createJson.getVolumeId()));
        }
        if(childDocument != null){
            if(childDocument.getUploadFileEntities() != null){
                if(childDocument.getUploadFileEntities().size() > 0){
                    if(childDocument.getUploadFileEntities().get(0)
                            .getUploadInfoEntity() != null){
                            volInsHash.put("childVolume", childDocument.getUploadFileEntities().get(0)
                                    .getUploadInfoEntity().getVolumeEntity());
                    }
                }
            }
        }

        if(childDocument != null){
            if(childDocument.getUploadFileEntities() != null){
                if(childDocument.getUploadFileEntities().size() > 0){
                    if(childDocument.getUploadFileEntities().get(0)
                            .getUploadInfoEntity() != null) {
                            volInsHash.put("childInsert", childDocument.getUploadFileEntities().get(0)
                                    .getUploadInfoEntity().getInsertEntity());
                    }
                }
            }
        }
        if(!volInsHash.containsKey("childInsert")) volInsHash.put("childInsert", null);
        if(!volInsHash.containsKey("childVolume")) volInsHash.put("childVolume", null);
        if(!volInsHash.containsKey("parentInsert")) volInsHash.put("parentInsert", null);
        if(!volInsHash.containsKey("parentVolume")) volInsHash.put("parentVolume", null);


        User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        Collation collation = createJson.toEntity(childDocument, user, file, volInsHash);
        collationDAO.saveCollation(collation);
        CollationJson json = new CollationJson();
        List<MiaDocumentEntity> docs = new ArrayList<MiaDocumentEntity>();
        docs.add(collation.getChildDocument());
        List<DocumentJson> documentJsons = miaDocumentService.createDocumentsJsonFromDocEntity(docs);
        json.toJson(collation, documentJsons);
        return json;
    }

    @Override
    public List<CollationJson> findAttachmentsByDocumentId(Integer documentId)
            throws ApplicationThrowable{
        List<Collation> collations = collationDAO.findByDocumentId(documentId, Collation.CollationType.ATTACHMENT);
        List<CollationJson> jsons = new ArrayList<CollationJson>();
        if(collations.size() == 0){
            return jsons;
        }
        for(Collation collation : collations){
            if(collation.getType().equals(Collation.CollationType.ATTACHMENT)) {

                CollationJson json = new CollationJson();
                List<MiaDocumentEntity> docs = new ArrayList<MiaDocumentEntity>();
                docs.add(collation.getChildDocument());
                docs.add(collation.getParentDocument());
                List<DocumentJson> documentJsons = miaDocumentService.createDocumentsJsonFromDocEntity(docs);
                json.toJson(collation, documentJsons);
                jsons.add(json);
            }
        }
        return jsons;
    }

    @Override
    public List<CollationJson> findLacunaByDocumentId(Integer documentId)
            throws ApplicationThrowable{
        List<Collation> collations = collationDAO.findByDocumentId(documentId, Collation.CollationType.LACUNA);
        List<CollationJson> jsons = new ArrayList<CollationJson>();
        if(collations.size() == 0){
            return jsons;
        }
        for(Collation collation : collations){
            if(collation.getType().equals(Collation.CollationType.ATTACHMENT)) {
                CollationJson json = new CollationJson();
                List<MiaDocumentEntity> docs = new ArrayList<MiaDocumentEntity>();
                docs.add(collation.getChildDocument());
                docs.add(collation.getParentDocument());
                List<DocumentJson> documentJsons = miaDocumentService.createDocumentsJsonFromDocEntity(docs);
                json.toJson(collation, documentJsons);
                jsons.add(json);
            }
        }
        return jsons;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public CollationJson modifyCollation(Collation collation, HashMap<String, Object> modifyJson)
            throws ApplicationThrowable{
        try {
            if(modifyJson.containsKey("unsure")){
                if(modifyJson.get("unsure") instanceof Boolean) {
                    collation.setUnsure((Boolean) (modifyJson.get("unsure") == null ? false : modifyJson.get("unsure")));
                }
            }
            if(modifyJson.containsKey("title")){
                collation.setTitle(modifyJson.get("title") == null ? null : modifyJson.get("title").toString());
            }
            if(modifyJson.containsKey("description")){
                collation.setDescription(modifyJson.get("description") == null ? null :modifyJson.get("description").toString());
            }

            collationDAO.merge(collation);
            CollationJson json = new CollationJson();
            List<MiaDocumentEntity> docs = new ArrayList<MiaDocumentEntity>();
            docs.add(collation.getChildDocument());
            docs.add(collation.getParentDocument());
            List<DocumentJson> documentJsons = miaDocumentService.createDocumentsJsonFromDocEntity(docs);
            json.toJson(collation, documentJsons);
            return json;
        } catch (Exception e){
            return null;
        }
    }

    @Override
    public List<HashMap> findCollationsForUploadFile(UploadFileEntity file, Integer uploadInfoId) throws ApplicationThrowable {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        List<HashMap> jsons = new ArrayList<HashMap>();
        List<Collation> collations = new ArrayList<Collation>();
        for(MiaDocumentEntity doc : file.getMiaDocumentEntities()){
            collations.addAll(collationDAO.findByDocumentId(doc.getDocumentEntityId()));
        }
        for(Collation c: collations){
            HashMap<String, Object> json = new HashMap<String, Object>();
            json.put("id", c.getId());
            json.put("parentDocumentId", c.getParentDocument() == null ? null : c.getParentDocument().getDocumentEntityId());
            json.put("childDocumentId", c.getChildDocument() == null ? null : c.getChildDocument().getDocumentEntityId());
            json.put("title", c.getTitle());
            json.put("dateCreated", c.getCreated() == null ? null : sdf.format(c.getCreated()));
            json.put("createdBy", c.getCreatedBy() == null ? null : c.getCreatedBy().getAccount());
            json.put("type", c.getType());
            json.put("isParent", false);
            json.put("parentVolumeId", c.getParentVolume() == null ? null : c.getParentVolume().getSummaryId());
            json.put("childVolumeId", c.getChildVolume() == null ? null : c.getChildVolume().getSummaryId());
            json.put("childInsertId", c.getChildInsert() == null ? null : c.getChildInsert().getInsertId());
            json.put("parentInsertId", c.getParentInsert() == null ? null : c.getParentInsert().getInsertId());
            json.put("parentVolumeName", c.getParentVolume() == null ? null : c.getParentVolume().getVolume());
            json.put("childVolumeName", c.getChildVolume() == null ? null : c.getChildVolume().getVolume());
            json.put("childInsertName", c.getChildInsert() == null ? null : c.getChildInsert().getInsertName());
            json.put("parentInsertName", c.getParentInsert() == null ? null : c.getParentInsert().getInsertName());
            json.put("unsure", c.getUnsure());
            json.put("description", c.getDescription());
            if(c.getParentDocument() != null){
                if(c.getParentDocument().getUploadFileEntities().size() > 0){
                    UploadInfoEntity u = c.getChildDocument().getUploadFileEntities().get(0).getUploadInfoEntity();
                    json.put("parentCollectionName", u.getCollectionEntity() == null ? null : u.getCollectionEntity().getCollectionName());
                    json.put("parentVolumeId", u.getVolumeEntity() == null ? null : u.getVolumeEntity().getSummaryId());
                    json.put("parentVolumeName", u.getVolume() == null ? null : u.getVolumeEntity().getVolume());
                    json.put("parentInsertId", u.getInsertEntity() == null ? null : u.getInsertEntity().getInsertId());
                    json.put("parentInsertName", u.getInsertEntity() == null ? null : u.getInsertEntity().getInsertName());
                }
            } else if(c.getParentVolume() != null){
                json.put("parentCollectionName", c.getParentVolume().getCollectionEntity().getCollectionName());
            } else if(c.getParentInsert() != null){
                json.put("parentCollectionName", c.getParentInsert().getVolumeEntity().getCollectionEntity().getCollectionName());
                json.put("parentVolumeId", c.getParentInsert().getVolumeEntity().getSummaryId());
                json.put("parentVolumeName", c.getParentInsert().getVolumeEntity().getVolume());
            }
            if(c.getChildDocument() != null){
                if(c.getChildDocument().getUploadFileEntities().size() > 0){
                    UploadInfoEntity u = c.getChildDocument().getUploadFileEntities().get(0).getUploadInfoEntity();
                    json.put("childCollectionName", u.getCollectionEntity() == null ? null : u.getCollectionEntity().getCollectionName());
                    json.put("childVolumeId", u.getVolumeEntity() == null ? null : u.getVolumeEntity().getSummaryId());
                    json.put("childVolumeName", u.getVolume() == null ? null : u.getVolumeEntity().getVolume());
                    json.put("childInsertId", u.getInsertEntity() == null ? null : u.getInsertEntity().getInsertId());
                    json.put("childInsertName", u.getInsertEntity() == null ? null : u.getInsertEntity().getInsertName());
                }
            } else if(c.getChildVolume() != null){
                json.put("childCollectionName", c.getChildVolume().getCollectionEntity().getCollectionName());
            } else if(c.getParentInsert() != null){
                json.put("childCollectionName", c.getChildInsert().getVolumeEntity().getCollectionEntity().getCollectionName());
                json.put("childVolumeId", c.getChildInsert().getVolumeEntity().getSummaryId());
                json.put("childVolumeName", c.getChildInsert().getVolumeEntity().getVolume());
            }
            json.put("childDocumentTitle", c.getChildDocument() == null ? null : c.getChildDocument().getDeTitle());
            json.put("parentDocumentTitle", c.getParentDocument() == null ? null : c.getParentDocument().getDeTitle());
            if(file.getUploadInfoEntity() != null){
                if(file.getUploadInfoEntity().getVolumeEntity() != null){
                    if(file.getUploadInfoEntity().getVolumeEntity().getSummaryId().equals(json.get("parentVolumeId"))){
                        json.put("isParent", true);
                    }
                }
            }
            if(file.getUploadInfoEntity() != null){
                if(file.getUploadInfoEntity().getInsertEntity() != null){
                    if(file.getUploadInfoEntity().getInsertEntity().getInsertId().equals(json.get("parentInsertId"))){
                        json.put("isParent", true);
                    }
                }
            }
            if(!file.getUploadInfoEntity().getUploadInfoId().equals(uploadInfoId)){
                json.put("isParent", true);
            }
            jsons.add(json);
        }
        for(MiaDocumentEntity doc : file.getMiaDocumentEntities()){
            for(HashMap json: jsons){
                if(doc.getDocumentEntityId().equals(json.get("parentDocumentId"))){
                    json.put("isParent", true);
                }
            }
        }

        jsons = U.uniq(jsons);
        return jsons;
    }

    @Override
    public List<UploadFileEntity> findCollatedFilesInVolume(Integer volumeId) throws ApplicationThrowable {
        List<UploadFileEntity> files = new ArrayList<UploadFileEntity>();
        List<Collation> collations = collationDAO.findByParentVolumeId(volumeId);
        Gson gson = new Gson();

        for(Collation c : collations){
            if(c.getFile() != null) {
                List<FolioEntity> folios = new ArrayList<FolioEntity>();
                try {
                    List<LinkedTreeMap> serializedFolios = (List<LinkedTreeMap>) gson.fromJson(c.getFolioNumber(), List.class);
                    for (LinkedTreeMap f : serializedFolios) {
                        FolioEntity folio = new FolioEntity();
                        folio.setFolioNumber((String) f.get("number") + " COLLATION");
                        folio.setRectoverso((String) f.get("rv"));
                        folio.setNoNumb(false);
                        folio.setUploadedFileId(c.getFile().getUploadFileId());
                        folios.add(folio);
                    }
                } catch(JsonSyntaxException ex){
                    FolioEntity folio = new FolioEntity();
                    folio.setFolioNumber("COLLATION");
                    folio.setRectoverso(null);
                    folio.setNoNumb(false);
                    folio.setUploadedFileId(c.getFile().getUploadFileId());
                    folios.add(folio);
                }
                c.getFile().setFolioEntities(folios);
                files.add(c.getFile());
            }
        }
        return files;
    }

    @Override
    public List<UploadFileEntity> findCollatedFilesInInsert(Integer insertId) throws ApplicationThrowable {
        List<UploadFileEntity> files = new ArrayList<UploadFileEntity>();
        List<Collation> collations = collationDAO.findByParentInsertId(insertId);
        Gson gson = new Gson();
        for(Collation c : collations){
            if(c.getFile() != null) {
                List<FolioEntity> folios = new ArrayList<FolioEntity>();
                List<LinkedTreeMap> serializedFolios = (List<LinkedTreeMap>) gson.fromJson(c.getFolioNumber(), List.class);
                for(LinkedTreeMap f: serializedFolios){
                    FolioEntity folio = new FolioEntity();
                    folio.setFolioNumber((String) f.get("number") + " COLLATION");
                    folio.setRectoverso((String) f.get("rv"));
                    folio.setNoNumb(false);
                    folio.setUploadedFileId(c.getFile().getUploadFileId());
                    folios.add(folio);
                }
                c.getFile().setFolioEntities(folios);
                files.add(c.getFile());
            }
        }
        return files;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public Boolean deleteCollation(Collation collation)
            throws ApplicationThrowable {
        try {
            collation.setLogicalDelete(true);
            collationDAO.remove(collation);
            return true;
        } catch (PersistenceException p){
            return false;
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public Boolean undeleteCollation(Collation collation)
            throws ApplicationThrowable {
        try {
            collation.setLogicalDelete(false);
            collationDAO.merge(collation);
            return true;
        } catch (Exception e){
            return false;
        }
    }
}
