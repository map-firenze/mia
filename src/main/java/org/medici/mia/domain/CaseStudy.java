package org.medici.mia.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "tblCaseStudy")
public class CaseStudy {
    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "notes")
    private String notes;

    @OneToMany(mappedBy = "caseStudy", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<CaseStudyFolder> folders;

    @ManyToMany
    @JoinTable(name = "tblCaseStudyUsers",
            joinColumns = @JoinColumn(name = "case_study_id"),
            inverseJoinColumns = @JoinColumn(name = "user_account"))
    private List<User> userGroup = new ArrayList<User>();

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @OneToOne
    @JoinColumn(name = "created_by")
    private User createdBy;

    public CaseStudy() {
    }

    public CaseStudy(String title, String description, String notes, Date createdAt, User createdBy) {
        this.title = title;
        this.description = description;
        this.notes = notes;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public List<CaseStudyFolder> getFolders() {
        return folders;
    }

    public void setFolders(List<CaseStudyFolder> folders) {
        this.folders = folders;
    }

    public List<User> getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(List<User> userGroup) {
        this.userGroup = userGroup;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }
}
