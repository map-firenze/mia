package org.medici.mia.domain;

public class FactoryEntity {
	public static IGenericEntity getEntity(String entityName) {
		if (EntityType.transcription.toString().equalsIgnoreCase(entityName)) {
			return new DocumentTranscriptionEntity();
		} else if (EntityType.genericdocument.toString().equalsIgnoreCase(
				entityName)) {
			return new MiaDocumentEntity();
		} else if (EntityType.topic.toString().equalsIgnoreCase(entityName)) {
			return new TopicPlaceEntity();
		}

		return null;
	}

}
