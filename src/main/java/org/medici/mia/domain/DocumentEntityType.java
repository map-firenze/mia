package org.medici.mia.domain;


public enum DocumentEntityType {
  DocumentEntity,
  CorrespondenceEntity,
  NotarialEntity,
  OfficialEntity,
  InventoryEntity,
  FiscalEntity,
  FinancialEntity,
  MiscellaneousEntity,
  LiteraryEntity,
  NewsEntity
}
