package org.medici.mia.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author User
 *
 */
@Entity
@Table(name = "\"tblDocumentEntsRefToPeople\"")
public class DocumentRefToPeopleEntity implements Serializable {

	private static final long serialVersionUID = -8041057138126692411L;

	@Id
	@Column(name = "documentId", length = 254, nullable = false)
	private Integer documentId;

	@Id
	@Column(name = "peopleId", length = 254, nullable = false)
	private Integer peopleId;

	@Column(name = "unSure")
	private String unSure;

	public DocumentRefToPeopleEntity(Integer documentId, Integer peopleId) {
		super();
		this.documentId = documentId;
		this.peopleId = peopleId;
	}

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	public Integer getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(Integer peopleId) {
		this.peopleId = peopleId;
	}

	public DocumentRefToPeopleEntity() {
		super();
	}

	public String getUnSure() {
		return unSure;
	}

	public void setUnSure(String unSure) {
		this.unSure = unSure;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((documentId == null) ? 0 : documentId.hashCode());
		result = prime * result
				+ ((peopleId == null) ? 0 : peopleId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocumentRefToPeopleEntity other = (DocumentRefToPeopleEntity) obj;
		if (documentId == null) {
			if (other.documentId != null)
				return false;
		} else if (!documentId.equals(other.documentId))
			return false;
		if (peopleId == null) {
			if (other.peopleId != null)
				return false;
		} else if (!peopleId.equals(other.peopleId))
			return false;
		return true;
	}

}