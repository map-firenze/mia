package org.medici.mia.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Entity
@Table(name = "\"tblUploads\"")
public class UploadInfoEntity implements IGenericEntity, Serializable {

	private static final long serialVersionUID = -4214011714265192038L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"", length = 254, nullable = false)
	private Integer uploadInfoId;

	@Column(name = "aeName")
	private String aeName;

	@Column(name = "aeDescription")
	private String aeDescription;

	@Column(name = "repository")
	private Integer repository;

	@Column(name = "collection")
	private Integer collection;

	@Column(name = "series")
	private Integer series;

	@Column(name = "volume")
	private Integer volume;

	@Column(name = "insertN")
	private Integer insertId;

	@Column(name = "owner")
	private String owner;

	@Column(name = "modifiedBy")
	private String modifiedBy;

	@Column(name = "optionalImage")
	private String optionalImage;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dateCreated", nullable = false)
	private Date dateCreated;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dateModified", nullable = false)
	private Date dateModified;

	@Column(name = "\"logicalDelete\"", nullable = false)
	private Integer logicalDelete;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "\"dateDeleted\"", nullable = false)
	private Date dateDeleted;

	@Column(name = "filePrivacy")
	private Integer filePrivacy;

	@Column(name = "commentsCount")
	private Integer commentsCount;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "repository", nullable = true)
	private RepositoryEntity repositoryEntity;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "collection", nullable = true)
	private CollectionEntity collectionEntity;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "series", nullable = true)
	private SeriesEntity seriesEntity;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "volume", nullable = true)
	private Volume volumeEntity;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "insertN", nullable = true)
	private InsertEntity insertEntity;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "uploadInfoEntity", cascade = { CascadeType.ALL })
	private List<UploadFileEntity> uploadFileEntities;

	public UploadInfoEntity() {
		super();
	}

	public UploadInfoEntity(String aeName, String aeDescription,
			Integer repository, Integer collection, Integer series,
			Integer volume, Integer insertId, String owner, Date dateCreated,
			Date dateModified, Integer logicalDelete, Date dateDeleted,
			Integer filePrivacy) {
		super();
		this.aeName = aeName;
		this.aeDescription = aeDescription;
		this.repository = repository;
		this.collection = collection;
		this.series = series;
		this.volume = volume;
		this.insertId = insertId;
		this.owner = owner;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.logicalDelete = logicalDelete;
		this.dateDeleted = dateDeleted;
		this.filePrivacy = filePrivacy;
	}
	
	public UploadInfoEntity(String aeName, String aeDescription,
			Integer repository, Integer collection, Integer series,
			Integer volume, Integer insertId, String owner, Date dateCreated,
			Date dateModified, Integer logicalDelete, Date dateDeleted,
			Integer filePrivacy, String optionalImage) {
		super();
		this.aeName = aeName;
		this.aeDescription = aeDescription;
		this.repository = repository;
		this.collection = collection;
		this.series = series;
		this.volume = volume;
		this.insertId = insertId;
		this.owner = owner;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.logicalDelete = logicalDelete;
		this.dateDeleted = dateDeleted;
		this.filePrivacy = filePrivacy;
		this.optionalImage = optionalImage;
	}
	
	public String getOptionalImage() {
		return optionalImage;
	}

	public void setOptionalImage(String optionalImage) {
		this.optionalImage = optionalImage;
	}

	public Integer getUploadInfoId() {
		return uploadInfoId;
	}

	public void setUploadInfoId(Integer uploadInfoId) {
		this.uploadInfoId = uploadInfoId;
	}

	public String getAeName() {
		return aeName;
	}

	public void setAeName(String aeName) {
		this.aeName = aeName;
	}

	public String getAeDescription() {
		return aeDescription;
	}

	public void setAeDescription(String aeDescription) {
		this.aeDescription = aeDescription;
	}

	public Integer getFilePrivacy() {
		return filePrivacy;
	}

	public void setFilePrivacy(Integer filePrivacy) {
		this.filePrivacy = filePrivacy;
	}

	public Integer getRepository() {
		return repository;
	}

	public void setRepository(Integer repository) {
		this.repository = repository;
	}

	public Integer getCollection() {
		return collection;
	}

	public void setCollection(Integer collection) {
		this.collection = collection;
	}

	public Integer getSeries() {
		return series;
	}

	public void setSeries(Integer series) {
		this.series = series;
	}

	public Integer getVolume() {
		return volume;
	}

	public void setVolume(Integer volume) {
		this.volume = volume;
	}

	public Integer getInsert() {
		return insertId;
	}

	public void setInsert(Integer insertId) {
		this.insertId = insertId;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	public RepositoryEntity getRepositoryEntity() {
		return repositoryEntity;
	}

	public void setRepositoryEntity(RepositoryEntity repositoryEntity) {
		this.repositoryEntity = repositoryEntity;
	}

	public CollectionEntity getCollectionEntity() {
		return collectionEntity;
	}

	public void setCollectionEntity(CollectionEntity collectionEntity) {
		this.collectionEntity = collectionEntity;
	}

	public SeriesEntity getSeriesEntity() {
		return seriesEntity;
	}

	public void setSeriesEntity(SeriesEntity seriesEntity) {
		this.seriesEntity = seriesEntity;
	}

	public Volume getVolumeEntity() {
		return volumeEntity;
	}

	public void setVolumeEntity(Volume volumeEntity) {
		this.volumeEntity = volumeEntity;
	}

	public List<UploadFileEntity> getUploadFileEntities() {
		return uploadFileEntities;
	}

	public void setUploadFileEntities(List<UploadFileEntity> uploadFileEntities) {
		this.uploadFileEntities = uploadFileEntities;
	}

	public Integer getInsertId() {
		return insertId;
	}

	public void setInsertId(Integer insertId) {
		this.insertId = insertId;
	}

	public InsertEntity getInsertEntity() {
		return insertEntity;
	}

	public void setInsertEntity(InsertEntity insertEntity) {
		this.insertEntity = insertEntity;
	}

	public Integer getLogicalDelete() {
		return logicalDelete;
	}

	public void setLogicalDelete(Integer logicalDelete) {
		this.logicalDelete = logicalDelete;
	}

	public Date getDateDeleted() {
		return dateDeleted;
	}

	public void setDateDeleted(Date dateDeleted) {
		this.dateDeleted = dateDeleted;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Integer getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(Integer commentsCount) {
		this.commentsCount = commentsCount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((collection == null) ? 0 : collection.hashCode());
		result = prime * result
				+ ((insertId == null) ? 0 : insertId.hashCode());
		result = prime * result
				+ ((repository == null) ? 0 : repository.hashCode());
		result = prime * result + ((series == null) ? 0 : series.hashCode());
		result = prime * result + ((volume == null) ? 0 : volume.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UploadInfoEntity other = (UploadInfoEntity) obj;
		if (collection == null) {
			if (other.collection != null)
				return false;
		} else if (!collection.equals(other.collection))
			return false;
		if (insertId == null) {
			if (other.insertId != null)
				return false;
		} else if (!insertId.equals(other.insertId))
			return false;
		if (repository == null) {
			if (other.repository != null)
				return false;
		} else if (!repository.equals(other.repository))
			return false;
		if (series == null) {
			if (other.series != null)
				return false;
		} else if (!series.equals(other.series))
			return false;
		if (volume == null) {
			if (other.volume != null)
				return false;
		} else if (!volume.equals(other.volume))
			return false;
		return true;
	}

}
