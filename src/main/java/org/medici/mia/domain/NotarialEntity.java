package org.medici.mia.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 
 * @author utente
 *
 */
@Entity
@Table(name = "\"tblDocNotarialRecords\"")
public class NotarialEntity implements Serializable {

	private static final long serialVersionUID = 4651235729374098215L;

	@Id
	@Column(name = "documentEntityId", length = 254, nullable = false)
	private Integer documentEntityId;

	@Column(name = "contractor")
	private String contractor;

	@Column(name = "contractee")
	private String contractee;

	@Transient
	private List<People> contractors;

	@Transient
	private List<People> contractees;

	public NotarialEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getDocumentEntityId() {
		return documentEntityId;
	}

	public void setDocumentEntityId(Integer documentEntityId) {
		this.documentEntityId = documentEntityId;
	}

	public String getContractor() {
		return contractor;
	}

	public void setContractor(String contractor) {
		this.contractor = contractor;
	}

	public String getContractee() {
		return contractee;
	}

	public void setContractee(String contractee) {
		this.contractee = contractee;
	}

	public List<People> getContractors() {
		return contractors;
	}

	public void setContractors(List<People> contractors) {
		this.contractors = contractors;
	}

	public List<People> getContractees() {
		return contractees;
	}

	public void setContractees(List<People> contractees) {
		this.contractees = contractees;
	}

}
