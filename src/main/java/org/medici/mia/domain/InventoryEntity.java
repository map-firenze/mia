package org.medici.mia.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 
 * @author User
 *
 */
@Entity
@Table(name = "\"tblDocInventories\"")
public class InventoryEntity implements Serializable{

	private static final long serialVersionUID = 750063357748829127L;

	@Id
	@Column(name = "documentEntityId", length = 254, nullable = false)
	private Integer documentEntityId;

	@Column(name = "commissioner")
	private String commissioner;
	
	@Transient
	private List<People> commissioners;

	public InventoryEntity() {
		super();
	}

	public Integer getDocumentEntityId() {
		return documentEntityId;
	}

	public void setDocumentEntityId(Integer documentEntityId) {
		this.documentEntityId = documentEntityId;
	}

	public String getCommissioner() {
		return commissioner;
	}

	public void setCommissioner(String commissioner) {
		this.commissioner = commissioner;
	}

	public List<People> getCommissioners() {
		return commissioners;
	}

	public void setCommissioners(List<People> commissioners) {
		this.commissioners = commissioners;
	}
	
}
