/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
package org.medici.mia.domain;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Entity
@Table(name = "\"tblPeople\"")
public class BiographicalPeople implements Serializable {

	private static final long serialVersionUID = -6007789289980534157L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PERSONID", length = 10, nullable = false)
	private Integer personId;

	@Column(name = "MAPNameLF", length = 150)
	private String mapNameLf;

	@Column(name = "GENDER", length = 1)
	@Enumerated(EnumType.STRING)
	private Gender gender;

	@Column(name = "ACTIVESTART", length = 50)
	private String activeStart;

	@Column(name = "ACTIVEEND", length = 50)
	private String activeEnd;

	@Column(name = "BYEAR", length = 4, nullable = true)
	private Integer bornYear;

	@Column(name = "BMONTHNUM", nullable = true)
	private Integer bornMonthNum;

	@Column(name = "BDAY", length = 3, columnDefinition = "TINYINT")
	private Integer bornDay;

	@Column(name = "BORNDATE", length = 10)
	private Integer bornDate;

	@Column(name = "BPLACEID")
	private Integer bornPlaceId;

	@Column(name = "BPLACE", length = 50)
	private String bPlace;

	@Column(name = "DYEAR", length = 4, nullable = true)
	private Integer deathYear;

	@Column(name = "DMONTHNUM", nullable = true)
	private Integer deathMonthNum;

	@Column(name = "DDAY", length = 3, columnDefinition = "TINYINT")
	private Integer deathDay;

	@Column(name = "DEATHDATE", length = 10)
	private Integer deathDate;

	@Column(name = "DPLACEID")
	private Integer deathPlaceId;

	@Column(name = "DPLACE", length = 50)
	private String dPlace;

	@Column(name = "\"FIRST\"", length = 50)
	private String first;

	@Column(name = "SUCNUM", length = 6)
	private String sucNum;

	@Column(name = "\"MIDDLE\"", length = 50)
	private String middle;

	@Column(name = "MIDPREFIX", length = 50)
	private String midPrefix;

	@Column(name = "LAST", length = 50)
	private String last;

	@Column(name = "LASTPREFIX", length = 50)
	private String lastPrefix;

	@Column(name = "POSTLAST", length = 50)
	private String postLast;

	@Column(name = "POSTLASTPREFIX", length = 50)
	private String postLastPrefix;

	@Column(name = "BAPPROX", length = 1, columnDefinition = "TINYINT", nullable = false)
	private Boolean bornApprox;

	@Column(name = "BDATEBC", length = 1, columnDefinition = "TINYINT", nullable = false)
	private Boolean bornDateBc;

	@Column(name = "BPLACEUNS", length = 1, columnDefinition = "TINYINT", nullable = false)
	private Boolean bornPlaceUnsure;

	@Column(name = "DAPPROX", length = 1, columnDefinition = "TINYINT", nullable = false)
	private Boolean deathApprox;

	@Column(name = "DYEARBC", length = 1, columnDefinition = "TINYINT", nullable = false)
	private Boolean deathDateBc;

	@Column(name = "DPLACEUNS", length = 1, columnDefinition = "TINYINT", nullable = false)
	private Boolean deathPlaceUnsure;

	@Column(name = "STATUS", length = 15)
	private String status;

	@Column(name = "BIONOTES", columnDefinition = "LONGTEXT")
	private String bioNotes;

	@Column(name = "STAFFNOTES", columnDefinition = "LONGTEXT")
	private String staffNotes;

	@Column(name = "PORTRAIT", length = 1, columnDefinition = "TINYINT", nullable = false)
	private Boolean portrait;

	@Column(name = "portraitImageName", length = 100)
	private String portraitImageName;

	@Column(name = "portraitAuthor", length = 100)
	private String portraitAuthor;

	@Column(name = "portraitSubject", length = 100)
	private String portraitSubject;

	@Column(name = "RESID")
	private String researcher;

	// @Column(name = "\"createdBy\"", nullable = true)
	@Column(name = "createdBy", nullable = true)
	private String createdBy;

	@Column(name = "\"DATECREATED\"")
	private Date dateCreated;

	@Column(name = "LASTUPDATE")
	private Date lastUpdate;

	@Column(name = "lastUpdateBy", nullable = true)
	private String lastUpdateBy;

	@Column(name = "LOGICALDELETE", length = 1, columnDefinition = "tinyint default 0", nullable = false)
	private Boolean logicalDelete;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "child")
	private Set<Parent> parents;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
	private Set<Parent> children;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "person")
	@OrderBy("altName ASC")
	private Set<AltName> altName;

	// Association Biographic
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "person")
	private Set<BioRefLink> bioRefLink;

	// Association Linked Document
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "person")
	private Set<EpLink> epLink;

	// Association titles and occupations
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "person")
	@OrderBy("preferredRole DESC, titleOccList ASC")
	private Set<PoLink> poLink;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "personId")
	private Set<PrcLink> prcLink;

	@OneToMany(mappedBy = "senderPeople", fetch = FetchType.LAZY)
	private Set<Document> senderDocuments;

	@OneToMany(mappedBy = "recipientPeople", fetch = FetchType.LAZY)
	private Set<Document> recipientDocuments;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "husband")
	private Set<Marriage> marriagesAsHusband;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "wife")
	private Set<Marriage> marriagesAsWife;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "producers", targetEntity = MiaDocumentEntity.class)
	private List<MiaDocumentEntity> miaDocumentEntities;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "refToPeople", targetEntity = MiaDocumentEntity.class)
	private List<MiaDocumentEntity> miaDocEntRefToPeople;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "biographicalPeople", cascade = {
			CascadeType.PERSIST, CascadeType.MERGE })
	private List<OrganizationEntity> Organizations;

	@Column(name = "commentsCount")
	private Integer commentsCount;

	@Transient
	private String unsure;

	/**
	 * Default Constructor
	 */
	public BiographicalPeople() {
		super();
	}

	/**
	 * 
	 * @param senderPeopleId
	 */
	public BiographicalPeople(Integer senderPeopleId) {
		super();
		setPersonId(senderPeopleId);
	}

	/**
	 * @return the personId
	 */
	public Integer getPersonId() {
		return personId;
	}

	/**
	 * @param personId
	 *            the personId to set
	 */
	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	/**
	 * @return the mapNameLf
	 */
	public String getMapNameLf() {
		return mapNameLf;
	}

	/**
	 * @param mapNameLf
	 *            the mapNameLf to set
	 */
	public void setMapNameLf(String mapNameLf) {
		this.mapNameLf = mapNameLf;
	}

	/**
	 * @return the gender
	 */
	public Gender getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(Gender gender) {
		this.gender = gender;
	}

	/**
	 * @return the activeStart
	 */
	public String getActiveStart() {
		return activeStart;
	}

	/**
	 * @param activeStart
	 *            the activeStart to set
	 */
	public void setActiveStart(String activeStart) {
		this.activeStart = activeStart;
	}

	/**
	 * @return the activeEnd
	 */
	public String getActiveEnd() {
		return activeEnd;
	}

	/**
	 * @param activeEnd
	 *            the activeEnd to set
	 */
	public void setActiveEnd(String activeEnd) {
		this.activeEnd = activeEnd;
	}

	/**
	 * @return the bornDay
	 */
	public Integer getBornDay() {
		return bornDay;
	}

	/**
	 * @param bDay
	 *            the bornDay to set
	 */
	public void setBornDay(Integer bornDay) {
		this.bornDay = bornDay;
	}

	/**
	 * @return the bornYear
	 */
	public Integer getBornYear() {
		return bornYear;
	}

	/**
	 * @param bornYear
	 *            the bornYear to set
	 */
	public void setBornYear(Integer bornYear) {
		this.bornYear = bornYear;
	}

	/**
	 * @param bornDate
	 *            the bornDate to set
	 */
	public void setBornDate(Integer bornDate) {
		this.bornDate = bornDate;
	}

	/**
	 * @return the bornDate
	 */
	public Integer getBornDate() {
		return bornDate;
	}

	/**
	 * @return the bPlace
	 */
	public String getbPlace() {
		return bPlace;
	}

	/**
	 * @param bPlace
	 *            the bPlace to set
	 */
	public void setbPlace(String bPlace) {
		this.bPlace = bPlace;
	}

	/**
	 * @return the deathDay
	 */
	public Integer getDeathDay() {
		return deathDay;
	}

	/**
	 * @param deathDay
	 *            the deathDay to set
	 */
	public void setDeathDay(Integer deathDay) {
		this.deathDay = deathDay;
	}

	/**
	 * @return the deathYear
	 */
	public Integer getDeathYear() {
		return deathYear;
	}

	/**
	 * @param deathYear
	 *            the deathYear to set
	 */
	public void setDeathYear(Integer deathYear) {
		this.deathYear = deathYear;
	}

	/**
	 * @param deathDate
	 *            the deathDate to set
	 */
	public void setDeathDate(Integer deathDate) {
		this.deathDate = deathDate;
	}

	/**
	 * @return the deathDate
	 */
	public Integer getDeathDate() {
		return deathDate;
	}

	/**
	 * @param dPlace
	 *            the dPlace to set
	 */
	public void setdPlace(String dPlace) {
		this.dPlace = dPlace;
	}

	/**
	 * @return the first
	 */
	public String getFirst() {
		return first;
	}

	/**
	 * @param first
	 *            the first to set
	 */
	public void setFirst(String first) {
		this.first = first;
	}

	/**
	 * @return the sucNum
	 */
	public String getSucNum() {
		return sucNum;
	}

	/**
	 * @param sucNum
	 *            the sucNum to set
	 */
	public void setSucNum(String sucNum) {
		this.sucNum = sucNum;
	}

	/**
	 * @return the middle
	 */
	public String getMiddle() {
		return middle;
	}

	/**
	 * @param middle
	 *            the middle to set
	 */
	public void setMiddle(String middle) {
		this.middle = middle;
	}

	/**
	 * @return the midPrefix
	 */
	public String getMidPrefix() {
		return midPrefix;
	}

	/**
	 * @param midPrefix
	 *            the midPrefix to set
	 */
	public void setMidPrefix(String midPrefix) {
		this.midPrefix = midPrefix;
	}

	/**
	 * @return the last
	 */
	public String getLast() {
		return last;
	}

	/**
	 * @param last
	 *            the last to set
	 */
	public void setLast(String last) {
		this.last = last;
	}

	/**
	 * @return the lastPrefix
	 */
	public String getLastPrefix() {
		return lastPrefix;
	}

	/**
	 * @param lastPrefix
	 *            the lastPrefix to set
	 */
	public void setLastPrefix(String lastPrefix) {
		this.lastPrefix = lastPrefix;
	}

	/**
	 * @return the postLast
	 */
	public String getPostLast() {
		return postLast;
	}

	/**
	 * @param postLast
	 *            the postLast to set
	 */
	public void setPostLast(String postLast) {
		this.postLast = postLast;
	}

	/**
	 * @return the postLastPrefix
	 */
	public String getPostLastPrefix() {
		return postLastPrefix;
	}

	/**
	 * @param postLastPrefix
	 *            the postLastPrefix to set
	 */
	public void setPostLastPrefix(String postLastPrefix) {
		this.postLastPrefix = postLastPrefix;
	}

	/**
	 * @return the bornApprox
	 */
	public Boolean getBornApprox() {
		return bornApprox;
	}

	/**
	 * @param bornApprox
	 *            the bornApprox to set
	 */
	public void setBornApprox(Boolean bornApprox) {
		this.bornApprox = bornApprox;
	}

	/**
	 * @return the bornDateBc
	 */
	public Boolean getBornDateBc() {
		return bornDateBc;
	}

	/**
	 * @param bornDateBc
	 *            the bornDateBc to set
	 */
	public void setBornDateBc(Boolean bornDateBc) {
		this.bornDateBc = bornDateBc;
	}

	/**
	 * @return the bornPlaceUnsure
	 */
	public Boolean getBornPlaceUnsure() {
		return bornPlaceUnsure;
	}

	/**
	 * @param bornPlaceUnsure
	 *            the bornPlaceUnsure to set
	 */
	public void setBornPlaceUnsure(Boolean bornPlaceUnsure) {
		this.bornPlaceUnsure = bornPlaceUnsure;
	}

	/**
	 * @return the deathApprox
	 */
	public Boolean getDeathApprox() {
		return deathApprox;
	}

	/**
	 * @param dApprox
	 *            the deathApprox to set
	 */
	public void setDeathApprox(Boolean deathApprox) {
		this.deathApprox = deathApprox;
	}

	/**
	 * @return the deathDateBc
	 */
	public Boolean getDeathDateBc() {
		return deathDateBc;
	}

	/**
	 * @param deathDateBc
	 *            the deathDateBc to set
	 */
	public void setDeathDateBc(Boolean deathDateBc) {
		this.deathDateBc = deathDateBc;
	}

	/**
	 * @return the deathPlaceUnsure
	 */
	public Boolean getDeathPlaceUnsure() {
		return deathPlaceUnsure;
	}

	/**
	 * @param deathPlaceUnsure
	 *            the deathPlaceUnsure to set
	 */
	public void setDeathPlaceUnsure(Boolean deathPlaceUnsure) {
		this.deathPlaceUnsure = deathPlaceUnsure;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the bioNotes
	 */
	public String getBioNotes() {
		return bioNotes;
	}

	/**
	 * @param bioNotes
	 *            the bioNotes to set
	 */
	public void setBioNotes(String bioNotes) {
		this.bioNotes = bioNotes;
	}

	/**
	 * @return the staffNotes
	 */
	public String getStaffNotes() {
		return staffNotes;
	}

	/**
	 * @param staffNotes
	 *            the staffNotes to set
	 */
	public void setStaffNotes(String staffNotes) {
		this.staffNotes = staffNotes;
	}

	/**
	 * @return the portrait
	 */
	public Boolean getPortrait() {
		return portrait;
	}

	/**
	 * @param portrait
	 *            the portrait to set
	 */
	public void setPortrait(Boolean portrait) {
		this.portrait = portrait;
	}

	public void setPortraitImageName(String portraitImageName) {
		this.portraitImageName = portraitImageName;
	}

	public String getPortraitImageName() {
		return portraitImageName;
	}

	/**
	 * @return the portraitAuthor
	 */
	public String getPortraitAuthor() {
		return portraitAuthor;
	}

	/**
	 * @param portraitAuthor
	 *            the portraitAuthor to set
	 */
	public void setPortraitAuthor(String portraitAuthor) {
		this.portraitAuthor = portraitAuthor;
	}

	/**
	 * @return the portraitSubject
	 */
	public String getPortraitSubject() {
		return portraitSubject;
	}

	/**
	 * @param portraitSubject
	 *            the portraitSubject to set
	 */
	public void setPortraitSubject(String portraitSubject) {
		this.portraitSubject = portraitSubject;
	}

	/**
	 * @return the researcher
	 */
	public String getResearcher() {
		return researcher;
	}

	/**
	 * @param researcher
	 *            the researcher to set
	 */
	public void setResearcher(String researcher) {
		this.researcher = researcher;
	}

	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}

	/**
	 * @param dateCreated
	 *            the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * @return the lastUpdate
	 */
	public Date getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * @param lastUpdate
	 *            the lastUpdate to set
	 */
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	/**
	 * @param altName
	 *            the altName to set
	 */
	public void setAltName(Set<AltName> altName) {
		this.altName = altName;
	}

	/**
	 * @return the altName
	 */
	public Set<AltName> getAltName() {
		return altName;
	}

	/**
	 * @param bioRefLink
	 *            the bioRefLink to set
	 */
	public void setBioRefLink(Set<BioRefLink> bioRefLink) {
		this.bioRefLink = bioRefLink;
	}

	/**
	 * @return the bioRefLink
	 */
	public Set<BioRefLink> getBioRefLink() {
		return bioRefLink;
	}

	/**
	 * Adds an {@link EpLink} to this person.
	 * 
	 * @param link
	 *            the epLink to add
	 */
	public void addEpLink(EpLink link) {
		if (epLink == null) {
			epLink = new HashSet<EpLink>();
		}
		epLink.add(link);
	}

	/**
	 * Removes an epLink from this person.
	 * 
	 * @param link
	 *            the epLink to remove
	 * @return true if the epLink has been removed from the person
	 */
	public boolean removeEplink(EpLink link) {
		if (epLink == null) {
			return false;
		}
		link.setPerson(null);
		return epLink.remove(link);
	}

	/**
	 * Returns an unmodifiable set of epLinks. Note: the returned set considers
	 * the referenced documents that are not deleted.
	 * 
	 * @return a set of epLinks
	 */
	public Set<EpLink> getEpLink() {
		if (epLink == null) {
			epLink = new HashSet<EpLink>();
		}
		Set<EpLink> actives = new HashSet<EpLink>();
		for (EpLink link : epLink) {
			if (link.getDocument() != null
					&& !link.getDocument().getLogicalDelete()) {
				actives.add(link);
			}
		}
		return Collections.unmodifiableSet(actives);
	}

	/**
	 * @param poLink
	 *            the poLink to set
	 */
	public void setPoLink(Set<PoLink> poLink) {
		this.poLink = poLink;
	}

	/**
	 * @return the poLink
	 */
	public Set<PoLink> getPoLink() {
		return poLink;
	}

	/**
	 * @param prcLink
	 *            the prcLink to set
	 */
	public void setPrcLink(Set<PrcLink> prcLink) {
		this.prcLink = prcLink;
	}

	/**
	 * @return the prcLink
	 */
	public Set<PrcLink> getPrcLink() {
		return prcLink;
	}

	/**
	 * 
	 * @param senderDocuments
	 */
	public void setSenderDocuments(Set<Document> senderDocuments) {
		this.senderDocuments = senderDocuments;
	}

	/**
	 * 
	 * @return
	 */
	public Set<Document> getSenderDocuments() {
		return senderDocuments;
	}

	/**
	 * @param recipientDocuments
	 *            the recipientDocuments to set
	 */
	public void setRecipientDocuments(Set<Document> recipientDocuments) {
		this.recipientDocuments = recipientDocuments;
	}

	/**
	 * @return the recipientDocuments
	 */
	public Set<Document> getRecipientDocuments() {
		return recipientDocuments;
	}

	/**
	 * @param marriagesAsHusband
	 *            the marriagesAsHusband to set
	 */
	public void setMarriagesAsHusband(Set<Marriage> marriagesAsHusband) {
		this.marriagesAsHusband = marriagesAsHusband;
	}

	/**
	 * @return the marriagesAsHusband
	 */
	public Set<Marriage> getMarriagesAsHusband() {
		return marriagesAsHusband;
	}

	/**
	 * @param marriagesAsWife
	 *            the marriagesAsWife to set
	 */
	public void setMarriagesAsWife(Set<Marriage> marriagesAsWife) {
		this.marriagesAsWife = marriagesAsWife;
	}

	/**
	 * @return the marriagesAsWife
	 */
	public Set<Marriage> getMarriagesAsWife() {
		return marriagesAsWife;
	}

	/**
	 * @param parents
	 *            the parents to set
	 */
	public void setParents(Set<Parent> parents) {
		this.parents = parents;
	}

	/**
	 * @return the parents
	 */
	public Set<Parent> getParents() {
		return parents;
	}

	/**
	 * @param children
	 *            the children to set
	 */
	public void setChildren(Set<Parent> children) {
		this.children = children;
	}

	/**
	 * @return the children
	 */
	public Set<Parent> getChildren() {
		return children;
	}

	/**
	 * @param logicalDelete
	 *            the logicalDelete to set
	 */
	public void setLogicalDelete(Boolean logicalDelete) {
		this.logicalDelete = logicalDelete;
	}

	/**
	 * @return the logicalDelete
	 */
	public Boolean getLogicalDelete() {
		return logicalDelete;
	}

	public void setEpLink(Set<EpLink> epLink) {
		this.epLink = epLink;
	}

	public List<MiaDocumentEntity> getMiaDocumentEntities() {
		return miaDocumentEntities;
	}

	public void setMiaDocumentEntities(
			List<MiaDocumentEntity> miaDocumentEntities) {
		this.miaDocumentEntities = miaDocumentEntities;
	}

	public Integer getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(Integer commentsCount) {
		this.commentsCount = commentsCount;
	}

	public Integer getBornMonthNum() {
		return bornMonthNum;
	}

	public void setBornMonthNum(Integer bornMonthNum) {
		this.bornMonthNum = bornMonthNum;
	}

	public Integer getBornPlaceId() {
		return bornPlaceId;
	}

	public void setBornPlaceId(Integer bornPlaceId) {
		this.bornPlaceId = bornPlaceId;
	}

	public Integer getDeathMonthNum() {
		return deathMonthNum;
	}

	public void setDeathMonthNum(Integer deathMonthNum) {
		this.deathMonthNum = deathMonthNum;
	}

	public Integer getDeathPlaceId() {
		return deathPlaceId;
	}

	public void setDeathPlaceId(Integer deathPlaceId) {
		this.deathPlaceId = deathPlaceId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

	public List<MiaDocumentEntity> getMiaDocEntRefToPeople() {
		return miaDocEntRefToPeople;
	}

	public void setMiaDocEntRefToPeople(
			List<MiaDocumentEntity> miaDocEntRefToPeople) {
		this.miaDocEntRefToPeople = miaDocEntRefToPeople;
	}

	public String getdPlace() {
		return dPlace;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder(0);
		if (getLast() != null) {
			stringBuilder.append(getLast());
		}
		if ((stringBuilder.length() > 0)
				&& ((getFirst() != null) || (getSucNum() != null))) {
			stringBuilder.append(", ");
		}
		if (getFirst() != null) {
			stringBuilder.append(getFirst());
		}

		if (getSucNum() != null) {
			if (stringBuilder.length() > 0) {
				stringBuilder.append(' ');
			}

			stringBuilder.append(getSucNum());
		}

		return stringBuilder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((getPersonId() == null) ? 0 : getPersonId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj == null) {
			return false;
		}

		if (!(obj instanceof BiographicalPeople)) {
			return false;
		}
		BiographicalPeople other = (BiographicalPeople) obj;
		if (getPersonId() == null) {
			if (other.getPersonId() != null) {
				return false;
			}
		} else if (!getPersonId().equals(other.getPersonId())) {
			return false;
		}

		return true;
	}

	public static enum Gender {
		NULL(null), M("M"), F("F"), X("X");

		private final String gender;

		private Gender(String value) {
			gender = value;
		}

		@Override
		public String toString() {
			return gender;
		}
	}

	public String getUnsure() {
		return unsure;
	}

	public void setUnsure(String unsure) {
		this.unsure = unsure;
	}

	public List<OrganizationEntity> getOrganizations() {
		return Organizations;
	}

	public void setOrganizations(List<OrganizationEntity> organizations) {
		Organizations = organizations;
	}

	
	
	

}
