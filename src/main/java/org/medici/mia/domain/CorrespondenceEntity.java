package org.medici.mia.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 
 * @author utente
 *
 */
@Entity
@Table(name = "\"tblDocCorrespondence\"")
public class CorrespondenceEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "documentEntityId", length = 254, nullable = false)
	private Integer documentEntityId;

	@Column(name = "recipient")
	private String recipient;

	@Column(name = "recipientPlace")
	private String recipientPlace;

	@Transient
	private List<People> recipients;

	@Transient
	private List<Place> recipientPlaces;

	public CorrespondenceEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getDocumentEntityId() {
		return documentEntityId;
	}

	public void setDocumentEntityId(Integer documentEntityId) {
		this.documentEntityId = documentEntityId;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getRecipientPlace() {
		return recipientPlace;
	}

	public void setRecipientPlace(String recipientPlace) {
		this.recipientPlace = recipientPlace;
	}

	public List<People> getRecipients() {
		return recipients;
	}

	public void setRecipients(List<People> recipients) {
		this.recipients = recipients;
	}

	public List<Place> getRecipientPlaces() {
		return recipientPlaces;
	}

	public void setRecipientPlaces(List<Place> recipientPlaces) {
		this.recipientPlaces = recipientPlaces;
	}

}
