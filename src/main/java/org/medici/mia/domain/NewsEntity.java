package org.medici.mia.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Entity
@Table(name = "\"tblDocNews\"")
public class NewsEntity implements Serializable {

	private static final long serialVersionUID = -597855076171094858L;

	@Id
	@Column(name = "documentEntityId", length = 254, nullable = false)
	private Integer documentEntityId;

	@Column(name = "newsFrom")
	private String newsFrom;
	
	@Column(name = "printer")
	private String printer;

	@Column(name = "printerPlace")
	private String printerPlace;
	
	@Transient
	private List<Place> newsFroms;
	
	@Transient
	private List<People> printers;

	@Transient
	private List<Place> printerPlaces;

	public NewsEntity() {
		super();
	}

	public Integer getDocumentEntityId() {
		return documentEntityId;
	}

	public void setDocumentEntityId(Integer documentEntityId) {
		this.documentEntityId = documentEntityId;
	}

	public String getNewsFrom() {
		return newsFrom;
	}

	public void setNewsFrom(String newsFrom) {
		this.newsFrom = newsFrom;
	}

	public String getPrinter() {
		return printer;
	}

	public void setPrinter(String printer) {
		this.printer = printer;
	}

	public String getPrinterPlace() {
		return printerPlace;
	}

	public void setPrinterPlace(String printerPlace) {
		this.printerPlace = printerPlace;
	}

	public List<Place> getNewsFroms() {
		return newsFroms;
	}

	public void setNewsFroms(List<Place> newsFroms) {
		this.newsFroms = newsFroms;
	}

	public List<People> getPrinters() {
		return printers;
	}

	public void setPrinters(List<People> printers) {
		this.printers = printers;
	}

	public List<Place> getPrinterPlaces() {
		return printerPlaces;
	}

	public void setPrinterPlaces(List<Place> printerPlaces) {
		this.printerPlaces = printerPlaces;
	}
	
}
