package org.medici.mia.domain;

public enum EntityType {	
	genericdocument,
	topic,
	transcription;
}
