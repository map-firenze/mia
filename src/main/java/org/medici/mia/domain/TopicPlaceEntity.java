package org.medici.mia.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author utente
 *
 */
@Entity
@Table(name = "\"tblDocumentTopicPlace\"")
public class TopicPlaceEntity implements IGenericEntity, Serializable {

	private static final long serialVersionUID = 4651235729374098215L;

	@Id
	@Column(name = "documentEntityId")
	private Integer documentEntityId;

	@Id
	@Column(name = "placeId")
	private Integer placeId;

	@Id
	@Column(name = "topicListId")
	private Integer topicListId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "documentEntityId", nullable = true)
	private MiaDocumentEntity miaDocumentEntity;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "placeId", nullable = true)
	private Place placeEntity;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "topicListId", nullable = true)
	private TopicList topicListEntity;

	public Integer getDocumentEntityId() {
		return documentEntityId;
	}

	public void setDocumentEntityId(Integer documentEntityId) {
		this.documentEntityId = documentEntityId;
	}

	public Integer getPlaceId() {
		return placeId;
	}

	public void setPlaceId(Integer placeId) {
		this.placeId = placeId;
	}

	public Integer getTopicListId() {
		return topicListId;
	}

	public void setTopicListId(Integer topicListId) {
		this.topicListId = topicListId;
	}

	public MiaDocumentEntity getMiaDocumentEntity() {
		return miaDocumentEntity;
	}

	public void setMiaDocumentEntity(MiaDocumentEntity miaDocumentEntity) {
		this.miaDocumentEntity = miaDocumentEntity;
	}

	public Place getPlaceEntity() {
		return placeEntity;
	}

	public void setPlaceEntity(Place placeEntity) {
		this.placeEntity = placeEntity;
	}

	public TopicList getTopicListEntity() {
		return topicListEntity;
	}

	public void setTopicListEntity(TopicList topicListEntity) {
		this.topicListEntity = topicListEntity;
	}

}
