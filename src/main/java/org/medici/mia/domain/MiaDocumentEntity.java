package org.medici.mia.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Entity
@Table(name = "\"tblDocumentEnts\"")
public class MiaDocumentEntity implements IGenericEntity, Serializable {

	private static final long serialVersionUID = 4651235729374098215L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "documentEntityId", length = 254, nullable = false)
	private Integer documentEntityId;

	@Column(name = "category")
	private String category;

	@Column(name = "typology")
	private String typology;

	@Column(name = "privacy")
	private Integer privacy;

	@Column(name = "docYear")
	private Integer docYear;

	@Column(name = "docMonth")
	private Integer docMonth;

	@Column(name = "docDay")
	private Integer docDay;

	@Column(name = "docModernYear")
	private Integer docModernYear;
	
	@Column(name = "docSortingYear")
	private Integer docSortingYear; 

	@Column(name = "docDateUncertain")
	private Integer docDateUncertain;

	@Column(name = "docUndated")
	private Integer docUndated;

	@Column(name = "docDateNotes")
	private String docDateNotes;

	@Column(name = "flgPrinted", nullable = false, columnDefinition = "TINYINT(1)")
	private Integer flgPrinted;

	@Column(name = "deTitle")
	private String deTitle;

	@Column(name = "incipit")
	private String incipit;

	@Lob
	@Column(name = "generalNotesSynopsis")
	private String generalNotesSynopsis;
	
	@Lob
	@Column(name = "chineseSynopsis")
	private String chineseSynopsis;

	@Lob
	@Column(name = "transcription")
	private String transcription;

	@Lob
	@Column(name = "transSearch")
	private String transSearch;

	@Column(name = "createdBy")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dateCreated")
	private Date dateCreated;

	@Column(name = "flgLogicalDelete", nullable = false, columnDefinition = "TINYINT(1)")
	private Boolean flgLogicalDelete;

	@Column(name = "deletedBy")
	private String deletedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dateDeleted")
	private Date dateDeleted;

	@Column(name = "LastUpdateBy")
	private String LastUpdateBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dateLastUpdate")
	private Date dateLastUpdate;

	@Column(name = "placeOfOrigin")
	private Integer placeOfOrigin;

	@Column(name = "placeOfOriginUnsure")
	private String placeOfOriginUnsure;

	@Column(name = "commentsCount")
	private Integer commentsCount;

	@Transient
	private String errorMsg;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn(name = "documentEntityId", referencedColumnName = "documentEntityId")
	private CorrespondenceEntity correspondence;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn(name = "documentEntityId", referencedColumnName = "documentEntityId")
	private NotarialEntity notarial;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn(name = "documentEntityId", referencedColumnName = "documentEntityId")
	private OfficialEntity official;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn(name = "documentEntityId", referencedColumnName = "documentEntityId")
	private InventoryEntity inventory;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn(name = "documentEntityId", referencedColumnName = "documentEntityId")
	private FiscalEntity fiscal;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn(name = "documentEntityId", referencedColumnName = "documentEntityId")
	private FinancialEntity financial;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn(name = "documentEntityId", referencedColumnName = "documentEntityId")
	private MiscellaneousEntity miscellaneous;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn(name = "documentEntityId", referencedColumnName = "documentEntityId")
	private LiteraryEntity literary;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn(name = "documentEntityId", referencedColumnName = "documentEntityId")
	private NewsEntity news;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "createdBy", nullable = true)
	private User userEntCreatedBy;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "deletedBy", nullable = true)
	private User userEntDeletedBy;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "LastUpdateBy", nullable = true)
	private User userEntLastUpdateBy;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "placeOfOrigin", nullable = true)
	private Place placeOfOriginEntity;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "miaDocumentEntity", cascade = { CascadeType.ALL })
	private List<TopicPlaceEntity> topicPlaces;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "miaDocumentEntity", cascade = { CascadeType.ALL })
	private List<BiblioRefEntity> biblioRefs;

	@ManyToMany(targetEntity = UploadFileEntity.class, cascade = { CascadeType.ALL })
	@JoinTable(name = "tblDocumentEntsUploadedFiles", joinColumns = { @JoinColumn(name = "documentId") }, inverseJoinColumns = { @JoinColumn(name = "fileId") })
	private List<UploadFileEntity> uploadFileEntities;

	@ManyToMany(targetEntity = People.class, cascade = { CascadeType.ALL })
	@JoinTable(name = "tblDocumentsEntsPeople", joinColumns = { @JoinColumn(name = "documentEntityId") }, inverseJoinColumns = { @JoinColumn(name = "producer") })
	private List<People> producers;

	@ManyToMany(targetEntity = People.class, cascade = { CascadeType.ALL })
	@JoinTable(name = "tblDocumentEntsRefToPeople", joinColumns = { @JoinColumn(name = "documentId") }, inverseJoinColumns = { @JoinColumn(name = "peopleId") })
	private List<People> refToPeople;

	@ManyToMany
	@JoinTable(name = "tblDocumentEntsRelatedDocs", joinColumns = { @JoinColumn(name = "documentId") }, inverseJoinColumns = { @JoinColumn(name = "documentRelatedId") })
	private List<MiaDocumentEntity> relatedDocs;

	@ManyToMany(targetEntity = LanguageEntity.class, cascade = { CascadeType.ALL })
	@JoinTable(name = "tblDocumentEntsLanguages", joinColumns = { @JoinColumn(name = "docId") }, inverseJoinColumns = { @JoinColumn(name = "langId") })
	private List<LanguageEntity> languages;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "document", cascade = { CascadeType.ALL })
	private List<DocumentTranscriptionEntity> documentTranscriptions;
	
	public Integer getDocumentEntityId() {
		return documentEntityId;
	}

	public void setDocumentEntityId(Integer documentEntityId) {
		this.documentEntityId = documentEntityId;
	}

	public String getTypology() {
		return typology;
	}

	public void setTypology(String typology) {
		this.typology = typology;
	}

	public Integer getDocYear() {
		return docYear;
	}

	public void setDocYear(Integer docYear) {
		this.docYear = docYear;
	}

	public Integer getDocMonth() {
		return docMonth;
	}

	public void setDocMonth(Integer docMonth) {
		this.docMonth = docMonth;
	}

	public Integer getDocDay() {
		return docDay;
	}

	public void setDocDay(Integer docDay) {
		this.docDay = docDay;
	}

	public Integer getDocModernYear() {
		return docModernYear;
	}

	public void setDocModernYear(Integer docModernYear) {
		this.docModernYear = docModernYear;
		
		if (docModernYear != 0) {
			this.docSortingYear = docModernYear;
		}
		else {
			this.docSortingYear = this.docYear;
		}
	}
	
	public Integer getDocSortingYear() {
		return docSortingYear;
	}

	public void setDocSortingYear(Integer docSortingYear) {
		this.docSortingYear = docSortingYear;
	}

	public Integer getDocDateUncertain() {
		return docDateUncertain;
	}

	public void setDocDateUncertain(Integer docDateUncertain) {
		this.docDateUncertain = docDateUncertain;
	}

	public Integer getDocUndated() {
		return docUndated;
	}

	public void setDocUndated(Integer docUndated) {
		this.docUndated = docUndated;
	}

	public String getDocDateNotes() {
		return docDateNotes;
	}

	public void setDocDateNotes(String docDateNotes) {
		this.docDateNotes = docDateNotes;
	}

	public Integer getFlgPrinted() {
		return flgPrinted;
	}

	public void setFlgPrinted(Integer flgPrinted) {
		this.flgPrinted = flgPrinted;
	}

	public String getDeTitle() {
		return deTitle;
	}

	public void setDeTitle(String deTitle) {
		this.deTitle = deTitle;
	}

	public String getIncipit() {
		return incipit;
	}

	public void setIncipit(String incipit) {
		this.incipit = incipit;
	}

	public String getGeneralNotesSynopsis() {
		return generalNotesSynopsis;
	}

	public void setGeneralNotesSynopsis(String generalNotesSynopsis) {
		this.generalNotesSynopsis = generalNotesSynopsis;
	}

	public String getChineseSynopsis() {
		return chineseSynopsis;
	}

	public void setChineseSynopsis(String chineseSynopsis) {
		this.chineseSynopsis = chineseSynopsis;
	}

	public String getTranscription() {
		return transcription;
	}

	public void setTranscription(String transcription) {
		this.transcription = transcription;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Boolean getFlgLogicalDelete() {
		return flgLogicalDelete;
	}

	public void setFlgLogicalDelete(Boolean flgLogicalDelete) {
		this.flgLogicalDelete = flgLogicalDelete;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDateDeleted() {
		return dateDeleted;
	}

	public void setDateDeleted(Date dateDeleted) {
		this.dateDeleted = dateDeleted;
	}

	public String getLastUpdateBy() {
		return LastUpdateBy;
	}

	public void setLastUpdateBy(String lastUpdateBy) {
		LastUpdateBy = lastUpdateBy;
	}

	public Date getDateLastUpdate() {
		return dateLastUpdate;
	}

	public void setDateLastUpdate(Date dateLastUpdate) {
		this.dateLastUpdate = dateLastUpdate;
	}

	public Integer getPlaceOfOrigin() {
		return placeOfOrigin;
	}

	public void setPlaceOfOrigin(Integer placeOfOrigin) {
		this.placeOfOrigin = placeOfOrigin;
	}

	public User getUserEntCreatedBy() {
		return userEntCreatedBy;
	}

	public void setUserEntCreatedBy(User userEntCreatedBy) {
		this.userEntCreatedBy = userEntCreatedBy;
	}

	public User getUserEntDeletedBy() {
		return userEntDeletedBy;
	}

	public void setUserEntDeletedBy(User userEntDeletedBy) {
		this.userEntDeletedBy = userEntDeletedBy;
	}

	public User getUserEntLastUpdateBy() {
		return userEntLastUpdateBy;
	}

	public void setUserEntLastUpdateBy(User userEntLastUpdateBy) {
		this.userEntLastUpdateBy = userEntLastUpdateBy;
	}

	public Place getPlaceOfOriginEntity() {
		return placeOfOriginEntity;
	}

	public void setPlaceOfOriginEntity(Place placeOfOriginEntity) {
		this.placeOfOriginEntity = placeOfOriginEntity;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getPrivacy() {
		return privacy;
	}

	public void setPrivacy(Integer privacy) {
		this.privacy = privacy;
	}

	public List<TopicPlaceEntity> getTopicPlaces() {
		return topicPlaces;
	}

	public void setTopicPlaces(List<TopicPlaceEntity> topicPlaces) {
		this.topicPlaces = topicPlaces;
	}

	public CorrespondenceEntity getCorrespondence() {
		return correspondence;
	}

	public void setCorrespondence(CorrespondenceEntity correspondence) {
		this.correspondence = correspondence;
	}

	public NotarialEntity getNotarial() {
		return notarial;
	}

	public void setNotarial(NotarialEntity notarial) {
		this.notarial = notarial;
	}

	public List<UploadFileEntity> getUploadFileEntities() {
		return uploadFileEntities;
	}

	public void setUploadFileEntities(List<UploadFileEntity> uploadFileEntities) {
		this.uploadFileEntities = uploadFileEntities;
	}

	public List<People> getProducers() {
		return producers;
	}

	public void setProducers(List<People> producers) {
		this.producers = producers;
	}

	public OfficialEntity getOfficial() {
		return official;
	}

	public void setOfficial(OfficialEntity official) {
		this.official = official;
	}

	public InventoryEntity getInventory() {
		return inventory;
	}

	public void setInventory(InventoryEntity inventory) {
		this.inventory = inventory;
	}

	public FiscalEntity getFiscal() {
		return fiscal;
	}

	public void setFiscal(FiscalEntity fiscal) {
		this.fiscal = fiscal;
	}

	public FinancialEntity getFinancial() {
		return financial;
	}

	public void setFinancial(FinancialEntity financial) {
		this.financial = financial;
	}

	public MiscellaneousEntity getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(MiscellaneousEntity miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public LiteraryEntity getLiterary() {
		return literary;
	}

	public void setLiterary(LiteraryEntity literary) {
		this.literary = literary;
	}

	public NewsEntity getNews() {
		return news;
	}

	public void setNews(NewsEntity news) {
		this.news = news;
	}

	public String getPlaceOfOriginUnsure() {
		return placeOfOriginUnsure;
	}

	public void setPlaceOfOriginUnsure(String placeOfOriginUnsure) {
		this.placeOfOriginUnsure = placeOfOriginUnsure;
	}

	public List<People> getRefToPeople() {
		return refToPeople;
	}

	public void setRefToPeople(List<People> refToPeople) {
		this.refToPeople = refToPeople;
	}

	public List<MiaDocumentEntity> getRelatedDocs() {
		return relatedDocs;
	}

	public void setRelatedDocs(List<MiaDocumentEntity> relatedDocs) {
		this.relatedDocs = relatedDocs;
	}

	public List<BiblioRefEntity> getBiblioRefs() {
		return biblioRefs;
	}

	public void setBiblioRefs(List<BiblioRefEntity> biblioRefs) {
		this.biblioRefs = biblioRefs;
	}

	public List<LanguageEntity> getLanguages() {
		return languages;
	}

	public void setLanguages(List<LanguageEntity> languages) {
		this.languages = languages;
	}

	public List<DocumentTranscriptionEntity> getDocumentTranscriptions() {
		return documentTranscriptions;
	}

	public Integer getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(Integer commentsCount) {
		this.commentsCount = commentsCount;
	}

	public void setDocumentTranscriptions(
			List<DocumentTranscriptionEntity> documentTranscriptions) {
		this.documentTranscriptions = documentTranscriptions;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getTransSearch() {
		return transSearch;
	}

	public void setTransSearch(String transSearch) {
		this.transSearch = transSearch;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((documentEntityId == null) ? 0 : documentEntityId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MiaDocumentEntity other = (MiaDocumentEntity) obj;
		if (documentEntityId == null) {
			if (other.documentEntityId != null)
				return false;
		} else if (!documentEntityId.equals(other.documentEntityId))
			return false;
		return true;
	}
}
