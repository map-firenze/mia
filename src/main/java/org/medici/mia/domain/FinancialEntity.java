package org.medici.mia.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Entity
@Table(name = "\"tblDocFinancialRecords\"")
public class FinancialEntity implements Serializable{

	private static final long serialVersionUID = -6488809452131448632L;

	@Id
	@Column(name = "documentEntityId", length = 254, nullable = false)
	private Integer documentEntityId;

	@Column(name = "commissionerOfAccount")
	private String commissionerOfAccount;
	
	@Transient
	private List<People> commissionersOfAccount;

	public FinancialEntity() {
		super();
	}

	public Integer getDocumentEntityId() {
		return documentEntityId;
	}

	public void setDocumentEntityId(Integer documentEntityId) {
		this.documentEntityId = documentEntityId;
	}

	public String getCommissionerOfAccount() {
		return commissionerOfAccount;
	}

	public void setCommissionerOfAccount(String commissionerOfAccount) {
		this.commissionerOfAccount = commissionerOfAccount;
	}

	public List<People> getCommissionersOfAccount() {
		return commissionersOfAccount;
	}

	public void setCommissionersOfAccount(List<People> commissionersOfAccount) {
		this.commissionersOfAccount = commissionersOfAccount;
	}

}
