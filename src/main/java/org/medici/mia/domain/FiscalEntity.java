package org.medici.mia.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Entity
@Table(name = "\"tblDocFiscalRecords\"")
public class FiscalEntity implements Serializable {

	private static final long serialVersionUID = 5462226377120982934L;

	@Id
	@Column(name = "documentEntityId", length = 254, nullable = false)
	private Integer documentEntityId;

	@Column(name = "gonfaloneDistrict")
	private String gonfaloneDistrict;

	@Column(name = "taxpayersOccupation")
	private String taxpayersOccupation;

	@Column(name = "placeofOccupation")
	private String placeofOccupation;

	@Transient
	private List<Place> gonfaloneDistricts;

	@Transient
	private List<Place> taxpayersOccupations;

	@Transient
	private List<Place> placeofOccupations;

	public FiscalEntity() {
		super();
	}

	public Integer getDocumentEntityId() {
		return documentEntityId;
	}

	public void setDocumentEntityId(Integer documentEntityId) {
		this.documentEntityId = documentEntityId;
	}

	public String getGonfaloneDistrict() {
		return gonfaloneDistrict;
	}

	public void setGonfaloneDistrict(String gonfaloneDistrict) {
		this.gonfaloneDistrict = gonfaloneDistrict;
	}

	public String getTaxpayersOccupation() {
		return taxpayersOccupation;
	}

	public void setTaxpayersOccupation(String taxpayersOccupation) {
		this.taxpayersOccupation = taxpayersOccupation;
	}

	public String getPlaceofOccupation() {
		return placeofOccupation;
	}

	public void setPlaceofOccupation(String placeofOccupation) {
		this.placeofOccupation = placeofOccupation;
	}

	public List<Place> getGonfaloneDistricts() {
		return gonfaloneDistricts;
	}

	public void setGonfaloneDistricts(List<Place> gonfaloneDistricts) {
		this.gonfaloneDistricts = gonfaloneDistricts;
	}

	public List<Place> getTaxpayersOccupations() {
		return taxpayersOccupations;
	}

	public void setTaxpayersOccupations(List<Place> taxpayersOccupations) {
		this.taxpayersOccupations = taxpayersOccupations;
	}

	public List<Place> getPlaceofOccupations() {
		return placeofOccupations;
	}

	public void setPlaceofOccupations(List<Place> placeofOccupations) {
		this.placeofOccupations = placeofOccupations;
	}

}
