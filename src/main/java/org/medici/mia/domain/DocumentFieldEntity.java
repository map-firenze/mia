package org.medici.mia.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author utente
 *
 */
@Entity
@Table(name = "\"tblDocumentFields\"")
public class DocumentFieldEntity implements IGenericEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "documentFieldId", length = 254, nullable = false)
	private Integer documentFieldId;

	@Column(name = "documentCategory")
	private String documentCategory;

	@Column(name = "fieldName")
	private String fieldName;

	@Column(name = "fieldBeName")
	private String fieldBeName;

	@Column(name = "fieldValue")
	private String fieldValue;

	@Column(name = "fieldType")
	private String fieldType;

	@Column(name = "multiple")
	private String allowMultiple;

	public Integer getDocumentFieldId() {
		return documentFieldId;
	}

	public void setDocumentFieldId(Integer documentFieldId) {
		this.documentFieldId = documentFieldId;
	}

	public String getDocumentCategory() {
		return documentCategory;
	}

	public void setDocumentCategory(String documentCategory) {
		this.documentCategory = documentCategory;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public String getAllowMultiple() {
		return allowMultiple;
	}

	public void setAllowMultiple(String allowMultiple) {
		this.allowMultiple = allowMultiple;
	}

	public String getFieldBeName() {
		return fieldBeName;
	}

	public void setFieldBeName(String fieldBeName) {
		this.fieldBeName = fieldBeName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((allowMultiple == null) ? 0 : allowMultiple.hashCode());
		result = prime
				* result
				+ ((documentCategory == null) ? 0 : documentCategory.hashCode());
		result = prime * result
				+ ((documentFieldId == null) ? 0 : documentFieldId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocumentFieldEntity other = (DocumentFieldEntity) obj;
		if (allowMultiple == null) {
			if (other.allowMultiple != null)
				return false;
		} else if (!allowMultiple.equals(other.allowMultiple))
			return false;
		if (documentCategory == null) {
			if (other.documentCategory != null)
				return false;
		} else if (!documentCategory.equals(other.documentCategory))
			return false;
		if (documentFieldId == null) {
			if (other.documentFieldId != null)
				return false;
		} else if (!documentFieldId.equals(other.documentFieldId))
			return false;
		return true;
	}

}
