package org.medici.mia.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author User
 *
 */
@Entity
@Table(name = "\"tblUploadedFiles\"")
public class UploadFileEntity implements Serializable {
	public static final String NO_IMAGE = "no_image";

	private static final long serialVersionUID = 4874555207211041941L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"", length = 254, nullable = false)
	private Integer uploadFileId;

	@Column(name = "idTblUpload")
	private Integer idTblUpload;

	@Column(name = "\"fileName\"")
	private String filename;

	@Column(name = "\"fileOriginalName\"")
	private String fileOriginalName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "\"dateCreated\"", nullable = false)
	private Date dateCreated;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "\"dateModified\"", nullable = false)
	private Date dateModified;

	@Column(name = "\"imageOrder\"", nullable = false)
	private Integer imageOrder;

	@Column(name = "\"logicalDelete\"", nullable = false)
	private Integer logicalDelete;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "\"dateDeleted\"", nullable = false)
	private Date dateDeleted;

	@Column(name = "tiledConversionProcess")
	private Integer tiledConversionProcess;

	@Column(name = "filePrivacy")
	private Integer filePrivacy;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "idTblUpload", nullable = true)
	private UploadInfoEntity uploadInfoEntity;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "uploadFileEntities", targetEntity = MiaDocumentEntity.class)
	private List<MiaDocumentEntity> miaDocumentEntities;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "uploadFileEntity")
	private List<FolioEntity> folioEntities;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "uploadFile", cascade = { CascadeType.ALL })
	private List<Annotation> annotations;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "uploadFileEntity", cascade = { CascadeType.ALL })
	private List<DocumentTranscriptionEntity> documentTranscriptions;
	
	

	public UploadFileEntity() {
		super();
	}

	public UploadFileEntity(String filename, String fileOriginalName,
			Integer idTblUpload, Date dateCreated, Date dateModified,
			Integer imageOrder, Integer logicalDelete, Date dateDeleted,
			Integer tiledConversionProcess, Integer filePrivacy) {
		super();
		this.filename = filename;
		this.fileOriginalName = fileOriginalName;
		this.idTblUpload = idTblUpload;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.imageOrder = imageOrder;
		this.logicalDelete = logicalDelete;
		this.dateDeleted = dateDeleted;
		this.tiledConversionProcess = tiledConversionProcess;
		this.filePrivacy = filePrivacy;
	}

	public Integer getUploadFileId() {
		return uploadFileId;
	}

	public void setUploadFileId(Integer uploadFileId) {
		this.uploadFileId = uploadFileId;
	}

	public Integer getIdTblUpload() {
		return idTblUpload;
	}

	public void setIdTblUpload(Integer idTblUpload) {
		this.idTblUpload = idTblUpload;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFileOriginalName() {
		return fileOriginalName;
	}

	public void setFileOriginalName(String fileOriginalName) {
		this.fileOriginalName = fileOriginalName;
	}

	public UploadInfoEntity getUploadInfoEntity() {
		return uploadInfoEntity;
	}

	public void setUploadInfoEntity(UploadInfoEntity uploadInfoEntity) {
		this.uploadInfoEntity = uploadInfoEntity;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	public Integer getImageOrder() {
		return imageOrder;
	}

	public void setImageOrder(Integer imageOrder) {
		this.imageOrder = imageOrder;
	}

	public Integer getTiledConversionProcess() {
		return tiledConversionProcess;
	}

	public void setTiledConversionProcess(Integer tiledConversionProcess) {
		this.tiledConversionProcess = tiledConversionProcess;
	}

	public Integer getFilePrivacy() {
		return filePrivacy;
	}

	public void setFilePrivacy(Integer filePrivacy) {
		this.filePrivacy = filePrivacy;
	}

	public Integer getLogicalDelete() {
		return logicalDelete;
	}

	public void setLogicalDelete(Integer logicalDelete) {
		this.logicalDelete = logicalDelete;
	}

	public Date getDateDeleted() {
		return dateDeleted;
	}

	public void setDateDeleted(Date dateDeleted) {
		this.dateDeleted = dateDeleted;
	}

	public List<FolioEntity> getFolioEntities() {
		return folioEntities;
	}

	public void setFolioEntities(List<FolioEntity> folioEntities) {
		this.folioEntities = folioEntities;
	}

	public List<MiaDocumentEntity> getMiaDocumentEntities() {
		return miaDocumentEntities;
	}

	public void setMiaDocumentEntities(
			List<MiaDocumentEntity> miaDocumentEntities) {
		this.miaDocumentEntities = miaDocumentEntities;
	}

	public List<Annotation> getAnnotations() {
		return annotations;
	}

	public void setAnnotations(List<Annotation> annotations) {
		this.annotations = annotations;
	}
	
	

	public List<DocumentTranscriptionEntity> getDocumentTranscriptions() {
		return documentTranscriptions;
	}

	public void setDocumentTranscriptions(
			List<DocumentTranscriptionEntity> documentTranscriptions) {
		this.documentTranscriptions = documentTranscriptions;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idTblUpload == null) ? 0 : idTblUpload.hashCode());
		result = prime * result
				+ ((uploadFileId == null) ? 0 : uploadFileId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UploadFileEntity other = (UploadFileEntity) obj;
		if (idTblUpload == null) {
			if (other.idTblUpload != null)
				return false;
		} else if (!idTblUpload.equals(other.idTblUpload))
			return false;
		if (uploadFileId == null) {
			if (other.uploadFileId != null)
				return false;
		} else if (!uploadFileId.equals(other.uploadFileId))
			return false;
		return true;
	}

}
