/*
 * FolioEntity.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Entity
@Table(name = "\"tblUploadFilesFolios\"")
public class FolioEntity implements Serializable {

	private static final long serialVersionUID = 7288740588237174542L;

	@Id
	@Column(name = "folioId")
	private Integer folioId;

	@Id
	@Column(name = "uploadedFileId")
	private Integer uploadedFileId;

	@Column(name = "folioNumber")
	private String folioNumber;

	@Column(name = "rectoverso")
	private String rectoverso;

	@Column(name = "noNumb")
	private Boolean noNumb;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "uploadedFileId", nullable = true)
	private UploadFileEntity uploadFileEntity;

	public Integer getFolioId() {
		return folioId;
	}

	public void setFolioId(Integer folioId) {
		this.folioId = folioId;
	}

	public String getFolioNumber() {
		return folioNumber;
	}

	public void setFolioNumber(String folioNumber) {
		this.folioNumber = folioNumber;
	}

	public String getRectoverso() {
		return rectoverso;
	}

	public void setRectoverso(String rectoverso) {
		this.rectoverso = rectoverso;
	}

	public Integer getUploadedFileId() {
		return uploadedFileId;
	}

	public void setUploadedFileId(Integer uploadedFileId) {
		this.uploadedFileId = uploadedFileId;
	}

	public UploadFileEntity getUploadFileEntity() {
		return uploadFileEntity;
	}

	public void setUploadFileEntity(UploadFileEntity uploadFileEntity) {
		this.uploadFileEntity = uploadFileEntity;
	}

	public Boolean getNoNumb() {
		return noNumb;
	}

	public void setNoNumb(Boolean noNumb) {
		this.noNumb = noNumb;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((folioNumber == null) ? 0 : folioNumber.hashCode());
		result = prime * result + ((noNumb == null) ? 0 : noNumb.hashCode());
		result = prime * result
				+ ((rectoverso == null) ? 0 : rectoverso.hashCode());
		result = prime * result
				+ ((uploadedFileId == null) ? 0 : uploadedFileId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FolioEntity other = (FolioEntity) obj;
		if (folioNumber == null) {
			if (other.folioNumber != null)
				return false;
		} else if (!folioNumber.equals(other.folioNumber))
			return false;
		if (noNumb == null) {
			if (other.noNumb != null)
				return false;
		} else if (!noNumb.equals(other.noNumb))
			return false;
		if (rectoverso == null) {
			if (other.rectoverso != null)
				return false;
		} else if (!rectoverso.equals(other.rectoverso))
			return false;
		if (uploadedFileId == null) {
			if (other.uploadedFileId != null)
				return false;
		} else if (!uploadedFileId.equals(other.uploadedFileId))
			return false;
		return true;
	}
	
	

}
