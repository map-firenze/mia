package org.medici.mia.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "\"tblRepositories\"")
public class RepositoryEntity implements Serializable {

	private static final long serialVersionUID = 4651235729374098215L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"", length = 254, nullable = false)
	private Integer repositoryId;

	@Column(name = "location")
	private String location;

	@Column(name = "\"repositoryName\"")
	private String repositoryName;

	@Column(name = "\"repositoryAbbreviation\"")
	private String repositoryAbbreviation;

	@Column(name = "\"repositoryDescription\"")
	private String repositoryDescription;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "location", insertable = false, updatable = false, nullable = true)
	private GoogleLocation googleLocation;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "repositoryEntity", cascade = {
			CascadeType.PERSIST, CascadeType.MERGE })
	private List<CollectionEntity> collections;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "repositoryEntity", cascade = {
			CascadeType.PERSIST, CascadeType.MERGE })
	private List<UploadInfoEntity> uploadInfoEntities;

	public RepositoryEntity() {
		super();
	}

	public RepositoryEntity(Integer id, String location, String repositoryName,
			String repositoryAbbreviation, String repositoryDescription) {
		this.repositoryId = id;
		this.location = location;
		this.repositoryName = repositoryName;
		this.repositoryAbbreviation = repositoryAbbreviation;
		this.repositoryDescription = repositoryDescription;

	}

	public Integer getRepositoryId() {
		return repositoryId;
	}

	public void setRepositoryId(Integer repositoryId) {
		this.repositoryId = repositoryId;
	}

	public List<CollectionEntity> getCollections() {
		return collections;
	}

	public void setCollections(List<CollectionEntity> collections) {
		this.collections = collections;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getRepositoryName() {
		return repositoryName;
	}

	public void setRepositoryName(String repositoryName) {
		this.repositoryName = repositoryName;
	}

	public String getRepositoryAbbreviation() {
		return repositoryAbbreviation;
	}

	public void setRepositoryAbbreviation(String repositoryAbbreviation) {
		this.repositoryAbbreviation = repositoryAbbreviation;
	}

	public String getRepositoryDescription() {
		return repositoryDescription;
	}

	public void setRepositoryDescription(String repositoryDescription) {
		this.repositoryDescription = repositoryDescription;
	}

	public GoogleLocation getGoogleLocation() {
		return googleLocation;
	}

	public void setGoogleLocation(GoogleLocation googleLocation) {
		this.googleLocation = googleLocation;
	}

	public List<UploadInfoEntity> getUploadInfoEntities() {
		return uploadInfoEntities;
	}

	public void setUploadInfoEntities(List<UploadInfoEntity> uploadInfoEntities) {
		this.uploadInfoEntities = uploadInfoEntities;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((repositoryId == null) ? 0 : repositoryId.hashCode());
		result = prime * result
				+ ((repositoryName == null) ? 0 : repositoryName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RepositoryEntity other = (RepositoryEntity) obj;
		if (repositoryId == null) {
			if (other.repositoryId != null)
				return false;
		} else if (!repositoryId.equals(other.repositoryId))
			return false;
		if (repositoryName == null) {
			if (other.repositoryName != null)
				return false;
		} else if (!repositoryName.equals(other.repositoryName))
			return false;
		return true;
	}

}
