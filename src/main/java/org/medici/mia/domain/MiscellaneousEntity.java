package org.medici.mia.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "\"tblDocMiscellaneous\"")
public class MiscellaneousEntity implements Serializable{


	private static final long serialVersionUID = 2889427099798098975L;
	
	@Id
	@Column(name = "documentEntityId", length = 254, nullable = false)
	private Integer documentEntityId;

	@Column(name = "printer")
	private String printer;

	@Column(name = "printerPlace")
	private String printerPlace;
	
	@Transient
	private List<People> printers;

	@Transient
	private List<Place> printerPlaces;
	
	public MiscellaneousEntity() {
		super();
	}

	public Integer getDocumentEntityId() {
		return documentEntityId;
	}

	public void setDocumentEntityId(Integer documentEntityId) {
		this.documentEntityId = documentEntityId;
	}

	public String getPrinter() {
		return printer;
	}

	public void setPrinter(String printer) {
		this.printer = printer;
	}

	public String getPrinterPlace() {
		return printerPlace;
	}

	public void setPrinterPlace(String printerPlace) {
		this.printerPlace = printerPlace;
	}

	public List<People> getPrinters() {
		return printers;
	}

	public void setPrinters(List<People> printers) {
		this.printers = printers;
	}

	public List<Place> getPrinterPlaces() {
		return printerPlaces;
	}

	public void setPrinterPlaces(List<Place> printerPlaces) {
		this.printerPlaces = printerPlaces;
	}
	
}
