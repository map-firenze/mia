package org.medici.mia.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.MetaValue;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tblCaseStudyItem")
public class CaseStudyItem {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "entity_id")
    private Integer entityId;

    @Enumerated(EnumType.STRING)
    @Column(name = "entity_type")
    private EntityType entityType;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "folder_id")
    private CaseStudyFolder folder;

    @OneToOne
    @JoinColumn(name = "created_by")
    private User createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "order_by")
    private Integer order;

    @Column(name = "path")
    private String path;

    @Column(name = "link")
    private String link;

    public CaseStudyItem() {
    }

    public CaseStudyItem(String title, Integer entityId, EntityType entityType, CaseStudyFolder folder, Date createdAt, Integer order, String link) {
        this.title = title;
        this.entityId = entityId;
        this.entityType = entityType;
        this.folder = folder;
        this.createdAt = createdAt;
        this.order = order;
        this.link = link;
    }

    public static enum EntityType {
        DE("DE"), BIO("BIO"), GEO("GEO"), LINK("LINK"), FILE("FILE");

        private final String type;

        private EntityType(String value) {
            type = value;
        }

        @Override
        public String toString(){
            return type;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getEntityId() {
        return entityId;
    }

    public void setEntityId(Integer entityId) {
        this.entityId = entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public CaseStudyFolder getFolder() {
        return folder;
    }

    public void setFolder(CaseStudyFolder folder) {
        this.folder = folder;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
