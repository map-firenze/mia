/*
 * Collation.java
 *
 * Developed by Medici Archive Project (2010-2012).
 *
 * This file is part of DocSources.
 *
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@Entity(name = "Collation")
@Table(name = "\"tblCollations\"")
public class Collation implements IGenericEntity, Serializable {

    private static final long serialVersionUID = 2728047284410126845L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", length = 10, nullable = false)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "parentDocId")
    private MiaDocumentEntity parentDocument;

    @OneToOne
    @JoinColumn(name = "childDocId")
    private MiaDocumentEntity childDocument;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private CollationType type;

    @Column(name = "created")
    private Date created;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "createdBy")
    private User createdBy;

    @OneToOne
    @JoinColumn(name = "destinationFile")
    private UploadFileEntity file;

    @Column(name = "title")
    private String title;

    @OneToOne
    @JoinColumn(name = "parentVolId")
    private Volume parentVolume;

    @OneToOne
    @JoinColumn(name = "childVolId")
    private Volume childVolume;

    @OneToOne
    @JoinColumn(name = "childInsId")
    private InsertEntity childInsert;

    @OneToOne
    @JoinColumn(name = "parentInsId")
    private InsertEntity parentInsert;

    @OneToOne
    @JoinColumn(name = "uploadInfoId")
    private UploadInfoEntity uploadInfoEntity;

    @Column(name = "folioNumber")
    private String folioNumber;

    @Column(name = "foliorv")
    private String foliorv;

    @Column(name = "unsure")
    private Boolean unsure;

    @Column(name = "description")
    private String description;

    @Column(name = "logicalDelete")
    private Boolean logicalDelete;



    public Collation() {
        super();
    }

    public Collation(MiaDocumentEntity parentDocument, MiaDocumentEntity childDocument) {
        this.parentDocument = parentDocument;
        this.childDocument = childDocument;
        this.type = CollationType.ATTACHMENT;
    }

    public static enum CollationType {
        LACUNA("LACUNA"), ATTACHMENT("ATTACHMENT");

        private final String type;

        private CollationType(String value) {
            type = value;
        }

        @Override
        public String toString(){
            return type;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Collation collation = (Collation) o;

        return new EqualsBuilder()
                .append(id, collation.id)
                .append(type, collation.type)
                .append(title, collation.title)
                .append(created, collation.created)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(title)
                .append(type)
                .append(created)
                .toHashCode();
    }

    public Boolean getLogicalDelete() {
        return logicalDelete;
    }

    public void setLogicalDelete(Boolean logicalDelete) {
        this.logicalDelete = logicalDelete;
    }

    public Boolean getUnsure() {
        return unsure;
    }

    public void setUnsure(Boolean unsure) {
        this.unsure = unsure;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MiaDocumentEntity getChildDocument() {
        return childDocument;
    }

    public void setChildDocument(MiaDocumentEntity childDocument) {
        this.childDocument = childDocument;
    }

    public MiaDocumentEntity getParentDocument() {
        return parentDocument;
    }

    public void setParentDocument(MiaDocumentEntity parentDocument) {
        this.parentDocument = parentDocument;
    }

    public CollationType getType() {
        return type;
    }

    public void setType(CollationType type) {
        this.type = type;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public UploadFileEntity getFile() {
        return file;
    }

    public void setFile(UploadFileEntity file) {
        this.file = file;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public UploadInfoEntity getUploadInfoEntity() {
        return uploadInfoEntity;
    }

    public void setUploadInfoEntity(UploadInfoEntity uploadInfoEntity) {
        this.uploadInfoEntity = uploadInfoEntity;
    }

    public Volume getParentVolume() {
        return parentVolume;
    }

    public void setParentVolume(Volume parentVolume) {
        this.parentVolume = parentVolume;
    }

    public Volume getChildVolume() {
        return childVolume;
    }

    public void setChildVolume(Volume childVolume) {
        this.childVolume = childVolume;
    }

    public InsertEntity getChildInsert() {
        return childInsert;
    }

    public void setChildInsert(InsertEntity childInsert) {
        this.childInsert = childInsert;
    }

    public InsertEntity getParentInsert() {
        return parentInsert;
    }

    public void setParentInsert(InsertEntity parentInsert) {
        this.parentInsert = parentInsert;
    }

    public String getFolioNumber() {
        return folioNumber;
    }

    public void setFolioNumber(String folioNumber) {
        this.folioNumber = folioNumber;
    }

    public String getFoliorv() {
        return foliorv;
    }

    public void setFoliorv(String foliorv) {
        this.foliorv = foliorv;
    }
}