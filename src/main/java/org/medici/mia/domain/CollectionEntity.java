package org.medici.mia.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "\"tblCollections\"")
public class CollectionEntity implements Serializable {

	private static final long serialVersionUID = 4651235729374098215L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"", length = 254, nullable = false)
	private Integer collectionId;

	@Column(name = "repository")
	private Integer repository;

	@Column(name = "\"collectionName\"")
	private String collectionName;

	@Column(name = "\"collectionAbbreviation\"")
	private String collectionAbbreviation;

	@Column(name = "\"collectionDescription\"")
	private String collectionDescription;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "repository", nullable = true)
	private RepositoryEntity repositoryEntity;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "collectionEntity", cascade = {
			CascadeType.PERSIST, CascadeType.MERGE })
	private List<SeriesEntity> seriesEntity;

	// volume list
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "collectionEntity", cascade = {
			CascadeType.PERSIST, CascadeType.MERGE })
	private List<Volume> volumes;	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "collectionEntity", cascade = {
			CascadeType.PERSIST, CascadeType.MERGE })
	private List<UploadInfoEntity> uploadInfoEntities;

	@Column(name = "isStudyCollection")
	private Boolean isStudyCollection;

	public CollectionEntity() {
		super();
	}

	public CollectionEntity(Integer id, Integer repositoryId,
			String collectionName, String collectionAbbreviation) {
		this.collectionId = id;
		this.repository = repositoryId;
		this.collectionName = collectionName;
		this.collectionAbbreviation = collectionAbbreviation;
	}

	public Integer getCollectionId() {
		return collectionId;
	}

	public void setCollectionId(Integer collectionId) {
		this.collectionId = collectionId;
	}

	public Integer getRepositoryId() {
		return repository;
	}

	public void setRepositoryId(Integer repositoryId) {
		this.repository = repositoryId;
	}

	public String getCollectionName() {
		return collectionName;
	}

	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}

	public String getCollectionAbbreviation() {
		return collectionAbbreviation;
	}

	public void setCollectionAbbreviation(String collectionAbbreviation) {
		this.collectionAbbreviation = collectionAbbreviation;
	}

	public String getCollectionDescription() {
		return collectionDescription;
	}

	public void setCollectionDescription(String collectionDescription) {
		this.collectionDescription = collectionDescription;
	}

	public RepositoryEntity getRepositoryEntity() {
		return repositoryEntity;
	}

	public void setRepositoryEntity(RepositoryEntity repositoryEntity) {
		this.repositoryEntity = repositoryEntity;
	}

	public List<Volume> getVolumes() {
		return volumes;
	}

	public void setVolumes(List<Volume> volumes) {
		this.volumes = volumes;
	}

	public List<SeriesEntity> getSeriesEntity() {
		return seriesEntity;
	}

	public void setSeriesEntity(List<SeriesEntity> seriesEntity) {
		this.seriesEntity = seriesEntity;
	}

	public List<UploadInfoEntity> getUploadInfoEntities() {
		return uploadInfoEntities;
	}

	public void setUploadInfoEntities(List<UploadInfoEntity> uploadInfoEntities) {
		this.uploadInfoEntities = uploadInfoEntities;
	}

	public Boolean getStudyCollection() {
		return isStudyCollection;
	}

	public void setStudyCollection(Boolean studyCollection) {
		isStudyCollection = studyCollection;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((collectionDescription == null) ? 0 : collectionDescription
						.hashCode());
		result = prime * result
				+ ((collectionName == null) ? 0 : collectionName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CollectionEntity other = (CollectionEntity) obj;
		if (collectionDescription == null) {
			if (other.collectionDescription != null)
				return false;
		} else if (!collectionDescription.equals(other.collectionDescription))
			return false;
		if (collectionName == null) {
			if (other.collectionName != null)
				return false;
		} else if (!collectionName.equals(other.collectionName))
			return false;
		return true;
	}

}
