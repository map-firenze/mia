package org.medici.mia.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Entity
@Table(name = "\"tblOrganizationHeadquarters\"")
public class OrganizationEntity implements Serializable {

	private static final long serialVersionUID = 4651235729374098215L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"", length = 254, nullable = false)
	private Integer organizationId;

	@Column(name = "year")
	private Integer year;

	@Column(name = "headquarters")
	private Integer placeAllId;

	@Column(name = "personId")
	private Integer personId;

	@ManyToOne
	@JoinColumn(insertable = false, updatable = false, name = "\"personId\"")
	private BiographicalPeople biographicalPeople;

	public OrganizationEntity() {
		super();
	}

	public Integer getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public BiographicalPeople getBiographicalPeople() {
		return biographicalPeople;
	}

	public void setBiographicalPeople(BiographicalPeople biographicalPeople) {
		this.biographicalPeople = biographicalPeople;
	}

	public Integer getPlaceAllId() {
		return placeAllId;
	}

	public void setPlaceAllId(Integer placeAllId) {
		this.placeAllId = placeAllId;
	}

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

}
