package org.medici.mia.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author utente
 *
 */
@Entity
@Table(name = "\"tblDocumentEntsUploadedFiles\"")
public class DocumentFileJoinEntity implements Serializable {

	private static final long serialVersionUID = 4651235729374098215L;
	@Id
	@Column(name = "documentId", length = 254, nullable = false)
	private Integer documentId;

	@Id
	@Column(name = "fileId")
	private Integer fileId;

	public DocumentFileJoinEntity() {
		super();
	}

	public DocumentFileJoinEntity(Integer documentId, Integer fileId) {
		super();
		this.documentId = documentId;
		this.fileId = fileId;
	}

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	public Integer getFileId() {
		return fileId;
	}

	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((documentId == null) ? 0 : documentId.hashCode());
		result = prime * result + ((fileId == null) ? 0 : fileId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocumentFileJoinEntity other = (DocumentFileJoinEntity) obj;
		if (documentId == null) {
			if (other.documentId != null)
				return false;
		} else if (!documentId.equals(other.documentId))
			return false;
		if (fileId == null) {
			if (other.fileId != null)
				return false;
		} else if (!fileId.equals(other.fileId))
			return false;
		return true;
	}

}
