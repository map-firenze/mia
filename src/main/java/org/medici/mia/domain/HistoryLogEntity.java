/*
 * UserHistory.java
 * 
 * Developed by Medici Archive Project (2010-2012).
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Entity
@Table(name = "\"tblLastAccessedRecords\"")
public class HistoryLogEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2001847076255349765L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "\"idLastAccessedItems\"", length = 10, nullable = false)
	private Integer idLastAccessedItems;

	@Column(name = "\"entryId\"", nullable = false)
	private Integer entryId;

	@Column(name = "\"dateAndTime\"", nullable = false)
	private Date dateAndTime;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "\"account\"", nullable = true)
	private User user;

	@Column(name = "\"action\"", length = 20, nullable = true)
	@Enumerated(EnumType.STRING)
	private UserAction action;

	@Column(name = "\"recordType\"", length = 20, nullable = true)
	@Enumerated(EnumType.STRING)
	private RecordType recordType;

	@Column(name = "\"serializedData\"", nullable = true)
	private String serializedData;

	@Column(name = "\"accessedFrom\"", nullable = true)
	private String accessedFrom;

	public Integer getIdLastAccessedItems() {
		return idLastAccessedItems;
	}

	public void setIdLastAccessedItems(Integer idLastAccessedItems) {
		this.idLastAccessedItems = idLastAccessedItems;
	}

	public Date getDateAndTime() {
		return dateAndTime;
	}

	public void setDateAndTime(Date dateAndTime) {
		this.dateAndTime = dateAndTime;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserAction getAction() {
		return action;
	}

	public void setAction(UserAction action) {
		this.action = action;
	}

	public RecordType getRecordType() {
		return recordType;
	}

	public void setRecordType(RecordType recordType) {
		this.recordType = recordType;
	}

	public Integer getEntryId() {
		return entryId;
	}

	public void setEntryId(Integer entryId) {
		this.entryId = entryId;
	}

	public String getSerializedData() {
		return serializedData;
	}

	public void setSerializedData(String serializedData) {
		this.serializedData = serializedData;
	}

	public String getAccessedFrom() {
		return accessedFrom;
	}

	public void setAccessedFrom(String accessedFrom) {
		this.accessedFrom = accessedFrom;
	}

	/**
	 * 
	 * @author Shadab Bigdel (<a
	 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
	 *
	 */
	public static enum UserAction {

		CREATE("Created"), EDIT("Edit"), VIEW("Viewed"), DELETE("Deleted"), UNDELETE("Restored");

		private final String action;

		private UserAction(String value) {
			action = value;
		}

		@Override
		public String toString() {
			return action;
		}
	}

	/**
	 * 
	 * @author Shadab Bigdel (<a
	 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
	 *
	 */
	public static enum RecordType {
		AE("Upload"), DE("Document"), BIO("Person"), GEO("Place"), VOL("Volume"), INS(
				"Insert");

		private final String recordType;

		private RecordType(String value) {
			recordType = value;
		}
		
		public static RecordType of(String value) {
			for (RecordType recordType : values()) {
				if (recordType.toString().equals(value)) {
					return recordType;
				}
			}
			return null;
		}

		@Override
		public String toString() {
			return recordType;
		}
	}
}
