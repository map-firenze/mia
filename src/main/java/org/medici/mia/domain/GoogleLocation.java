package org.medici.mia.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.solr.analysis.ASCIIFoldingFilterFactory;
import org.apache.solr.analysis.LowerCaseFilterFactory;
import org.apache.solr.analysis.MappingCharFilterFactory;
import org.apache.solr.analysis.NGramFilterFactory;
import org.apache.solr.analysis.StandardFilterFactory;
import org.apache.solr.analysis.StandardTokenizerFactory;
import org.hibernate.envers.Audited;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.AnalyzerDefs;
import org.hibernate.search.annotations.CharFilterDef;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Parameter;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

@Entity
@Indexed
@AnalyzerDefs({
		@AnalyzerDef(name = "googleLocationAnalyzer", charFilters = { @CharFilterDef(factory = MappingCharFilterFactory.class, params = { @Parameter(name = "mapping", value = "org/medici/mia/mapping-chars.properties") }) }, tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class), filters = { @TokenFilterDef(factory = ASCIIFoldingFilterFactory.class) }),
		@AnalyzerDef(name = "googleLocationNGram3Analyzer", tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class), filters = {
				@TokenFilterDef(factory = StandardFilterFactory.class),
				@TokenFilterDef(factory = LowerCaseFilterFactory.class),
				@TokenFilterDef(factory = ASCIIFoldingFilterFactory.class),
				@TokenFilterDef(factory = NGramFilterFactory.class, params = {
						@Parameter(name = "minGramSize", value = "3"),
						@Parameter(name = "maxGramSize", value = "3") }) }) })
@Audited
@Table(name = "\"tblGoogleLocations\"")
public class GoogleLocation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4651235729374098215L;
	@Id
	@Column(name = "\"gplace_id\"", length = 254, nullable = false)
	private String gPlaceId;
	@Column(name = "\"city\"", length = 2083, nullable = false)
	@Field(index = Index.TOKENIZED, store = Store.YES, indexNullAs = Field.DEFAULT_NULL_TOKEN)
	private String city;
	@Column(name = "\"country\"", length = 200, nullable = false)
	@Field(index = Index.TOKENIZED, store = Store.YES, indexNullAs = Field.DEFAULT_NULL_TOKEN)
	private String country;	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "googleLocation", cascade={CascadeType.PERSIST, CascadeType.MERGE})
	private List<RepositoryEntity> repositories;
	

	
	
	public GoogleLocation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GoogleLocation(String gPlaceId, String country, String city) {
		super();
		this.gPlaceId = gPlaceId;		
		this.country = country;
		this.city = city;
	}

	
	public String getgPlaceId() {
		return gPlaceId;
	}

	public void setgPlaceId(String gPlaceId) {
		this.gPlaceId = gPlaceId;
	}

	public List<RepositoryEntity> getRepositories() {
		return repositories;
	}

	public void setRepositories(List<RepositoryEntity> repositories) {
		this.repositories = repositories;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result
				+ ((gPlaceId == null) ? 0 : gPlaceId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GoogleLocation other = (GoogleLocation) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (gPlaceId == null) {
			if (other.gPlaceId != null)
				return false;
		} else if (!gPlaceId.equals(other.gPlaceId))
			return false;
		return true;
	}

}
