package org.medici.mia.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "tblDocTranscriptions")
@NamedQuery(name = "DocumentTranscriptionEntity.findAll", query = "SELECT d FROM DocumentTranscriptionEntity d")
public class DocumentTranscriptionEntity implements IGenericEntity, Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "docTranscriptionId", length = 254, nullable = false)
	private Integer docTranscriptionId;

	@Column(name = "transcription", length = 2083, nullable = false)
	private String transcription;

	@Column(name = "documentEntityId", length = 2083, nullable = false)
	private Integer documentEntityId;
	
	@Column(name = "uploadedFileId", length = 2083, nullable = false)
	private Integer uploadedFileId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "documentEntityId", nullable = true)
	private MiaDocumentEntity document;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "uploadedFileId", nullable = true)
	private UploadFileEntity uploadFileEntity;

	public MiaDocumentEntity getDocument() {
		return document;
	}

	public void setDocument(MiaDocumentEntity document) {
		this.document = document;
	}

	public DocumentTranscriptionEntity() {
	}

	public Integer getDocTranscriptionId() {
		return docTranscriptionId;
	}

	public void setDocTranscriptionId(Integer docTranscriptionId) {
		this.docTranscriptionId = docTranscriptionId;
	}

	public String getTranscription() {
		return this.transcription;
	}

	public void setTranscription(String transcription) {
		this.transcription = transcription;
	}

	public Integer getDocumentEntityId() {
		return documentEntityId;
	}

	public void setDocumentEntityId(Integer documentEntityId) {
		this.documentEntityId = documentEntityId;
	}

	public Integer getUploadedFileId() {
		return uploadedFileId;
	}

	public void setUploadedFileId(Integer uploadedFileId) {
		this.uploadedFileId = uploadedFileId;
	}

	public UploadFileEntity getUploadFileEntity() {
		return uploadFileEntity;
	}

	public void setUploadFileEntity(UploadFileEntity uploadFileEntity) {
		this.uploadFileEntity = uploadFileEntity;
	}
	
	
	
	

}