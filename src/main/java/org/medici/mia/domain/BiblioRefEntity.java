package org.medici.mia.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "\"tblBiblioRef\"")
public class BiblioRefEntity implements Serializable {

	private static final long serialVersionUID = 7211063976399118257L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idRef", length = 254, nullable = false)
	private Integer idRef;

	@Column(name = "idDoc", length = 254, nullable = false)
	private Integer idDoc;

	@Column(name = "name")
	private String name;

	@Column(name = "permalink")
	private String permalink;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "idDoc", nullable = true)
	private MiaDocumentEntity miaDocumentEntity;

	public BiblioRefEntity() {
		super();
	}

	public BiblioRefEntity(Integer idRef, Integer idDoc, String name,
			String permalink) {
		super();
		this.idRef = idRef;
		this.idDoc = idDoc;
		this.name = name;
		this.permalink = permalink;
	}

	public Integer getIdRef() {
		return idRef;
	}

	public void setIdRef(Integer idRef) {
		this.idRef = idRef;
	}

	public Integer getIdDoc() {
		return idDoc;
	}

	public void setIdDoc(Integer idDoc) {
		this.idDoc = idDoc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPermalink() {
		return permalink;
	}

	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}

	public MiaDocumentEntity getMiaDocumentEntity() {
		return miaDocumentEntity;
	}

	public void setMiaDocumentEntity(MiaDocumentEntity miaDocumentEntity) {
		this.miaDocumentEntity = miaDocumentEntity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idDoc == null) ? 0 : idDoc.hashCode());
		result = prime * result + ((idRef == null) ? 0 : idRef.hashCode());
		result = prime
				* result
				+ ((miaDocumentEntity == null) ? 0 : miaDocumentEntity
						.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((permalink == null) ? 0 : permalink.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BiblioRefEntity other = (BiblioRefEntity) obj;
		if (idDoc == null) {
			if (other.idDoc != null)
				return false;
		} else if (!idDoc.equals(other.idDoc))
			return false;
		if (idRef == null) {
			if (other.idRef != null)
				return false;
		} else if (!idRef.equals(other.idRef))
			return false;
		if (miaDocumentEntity == null) {
			if (other.miaDocumentEntity != null)
				return false;
		} else if (!miaDocumentEntity.equals(other.miaDocumentEntity))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (permalink == null) {
			if (other.permalink != null)
				return false;
		} else if (!permalink.equals(other.permalink))
			return false;
		return true;
	}
}
