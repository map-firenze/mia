package org.medici.mia.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author User
 *
 */
@Entity
@Table(name = "\"tblDocumentEntsRelatedDocs\"")
public class DocumentRelatedDocsJoinEntity implements Serializable {

	private static final long serialVersionUID = 6702452905865228090L;

	@Id
	@Column(name = "documentId", length = 254, nullable = false)
	private Integer documentId;

	@Id
	@Column(name = "documentRelatedId", length = 254, nullable = false)
	private Integer documentRelatedId;

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	public Integer getDocumentRelatedId() {
		return documentRelatedId;
	}

	public void setDocumentRelatedId(Integer documentRelatedId) {
		this.documentRelatedId = documentRelatedId;
	}

	public DocumentRelatedDocsJoinEntity() {
		super();
	}

	public DocumentRelatedDocsJoinEntity(Integer documentId,
			Integer documentRelatedId) {
		super();
		this.documentId = documentId;
		this.documentRelatedId = documentRelatedId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((documentId == null) ? 0 : documentId.hashCode());
		result = prime
				* result
				+ ((documentRelatedId == null) ? 0 : documentRelatedId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocumentRelatedDocsJoinEntity other = (DocumentRelatedDocsJoinEntity) obj;
		if (documentId == null) {
			if (other.documentId != null)
				return false;
		} else if (!documentId.equals(other.documentId))
			return false;
		if (documentRelatedId == null) {
			if (other.documentRelatedId != null)
				return false;
		} else if (!documentRelatedId.equals(other.documentRelatedId))
			return false;
		return true;
	}
}
