/*
 * DocumentTypologyEntity.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Entity
@Table(name = "\"tblDocumentTypologies\"")
public class DocumentTypologyEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "documentTypologyId", nullable = false)
	private Integer documentTypologyId;
	
	@Column(name = "type")
	private String type;

	@Column(name = "categoryName")
	private String categoryName;

	@Column(name = "typologyName")
	private String typologyName;

	@Column(name = "typologyHelp")
	private String typologyHelp;

	public Integer getDocumentTypologyId() {
		return documentTypologyId;
	}

	public void setDocumentTypologyId(Integer documentTypologyId) {
		this.documentTypologyId = documentTypologyId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getTypologyName() {
		return typologyName;
	}

	public void setTypologyName(String typologyName) {
		this.typologyName = typologyName;
	}

	public String getTypologyHelp() {
		return typologyHelp;
	}

	public void setTypologyHelp(String typologyHelp) {
		this.typologyHelp = typologyHelp;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
