package org.medici.mia.domain;

public enum DiscoveryStatus {
	DRAFT("Draft"),
	SENT_TO_PEER_REVIEW("Sent to peer review"),
	APPROVED("Approved"),
	PUBLISHED("Published"),
	REJECTED("Rejected"),
	REOPENED("Re-opened");

	private String displayName;
	
	private DiscoveryStatus(String displayName) {
		this.displayName = displayName;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
}
