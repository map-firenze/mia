package org.medici.mia.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "\"tblDocumentEntsLanguages\"")
public class DocumentLanguageJoinEntity implements IGenericEntity, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "docId", length = 254, nullable = false)
	private Integer docId;

	@Id
	@Column(name = "langId")
	private Integer langId;

	public DocumentLanguageJoinEntity() {
		super();
	}

	public DocumentLanguageJoinEntity(Integer docId, Integer langId) {
		super();
		this.docId = docId;
		this.langId = langId;
	}

	public Integer getDocId() {
		return docId;
	}

	public void setDocId(Integer docId) {
		this.docId = docId;
	}

	public Integer getLangId() {
		return langId;
	}

	public void setLangId(Integer langId) {
		this.langId = langId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((docId == null) ? 0 : docId.hashCode());
		result = prime * result + ((langId == null) ? 0 : langId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocumentLanguageJoinEntity other = (DocumentLanguageJoinEntity) obj;
		if (docId == null) {
			if (other.docId != null)
				return false;
		} else if (!docId.equals(other.docId))
			return false;
		if (langId == null) {
			if (other.langId != null)
				return false;
		} else if (!langId.equals(other.langId))
			return false;
		return true;
	}

}
