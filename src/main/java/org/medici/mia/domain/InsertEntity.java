/*
 * Insert.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.search.annotations.DateBridge;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FieldBridge;
import org.hibernate.search.annotations.Fields;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.NumericField;
import org.hibernate.search.annotations.NumericFields;
import org.hibernate.search.annotations.Resolution;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.bridge.builtin.BooleanBridge;


@Entity
@Table(name = "\"tblInserts\"")
public class InsertEntity implements Serializable{

	private static final long serialVersionUID = 4247339092562322498L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"id\"", length = 10, nullable = false)
	private Integer insertId;
	
	@Column(name = "\"insertN\"")
	private String insertName;
	
	@Column(name = "volume")
	private Integer volume;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "volume", nullable = true)
	private Volume volumeEntity;	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "insertEntity", cascade = {
			CascadeType.PERSIST, CascadeType.MERGE })
	private List<UploadInfoEntity> uploadInfoEntities;

	//ALAI NUOVI CAMPI
	@Column(name = "\"STARTYEAR\"", length = 5)
	@Fields({
			@Field(index = Index.TOKENIZED, store = Store.YES, indexNullAs = Field.DEFAULT_NULL_TOKEN),
			@Field(name = "startYear_Sort", index = Index.UN_TOKENIZED, indexNullAs = Field.DEFAULT_NULL_TOKEN) })
	@NumericFields({ @NumericField(forField = "startYear"),
			@NumericField(forField = "startYear_Sort") })
	private Integer startYear;

	@Column(name = "\"STARTMONTH\"", length = 2)
	@Fields({
			@Field(index = Index.TOKENIZED, store = Store.YES, indexNullAs = Field.DEFAULT_NULL_TOKEN),
			@Field(name = "startMonth_Sort", index = Index.UN_TOKENIZED, indexNullAs = Field.DEFAULT_NULL_TOKEN) })
	private Integer startMonth;


	@Column(name = "\"STARTDAY\"", length = 3, columnDefinition = "TINYINT")
	@Fields({
			@Field(index = Index.TOKENIZED, store = Store.YES, indexNullAs = Field.DEFAULT_NULL_TOKEN),
			@Field(name = "startDay_Sort", index = Index.UN_TOKENIZED, indexNullAs = Field.DEFAULT_NULL_TOKEN) })
	@NumericFields({ @NumericField(forField = "startDay"),
			@NumericField(forField = "startDay_Sort") })
	private Integer startDay;
	
	
	@Column(name = "\"ENDYEAR\"", length = 5)
	@Fields({
			@Field(index = Index.TOKENIZED, store = Store.YES, indexNullAs = Field.DEFAULT_NULL_TOKEN),
			@Field(name = "endYear_Sort", index = Index.UN_TOKENIZED, indexNullAs = Field.DEFAULT_NULL_TOKEN) })
	@NumericFields({ @NumericField(forField = "endYear"),
			@NumericField(forField = "endYear_Sort") })
	private Integer endYear;

	@Column(name = "\"ENDMONTH\"", length = 2)
	@Fields({
			@Field(index = Index.TOKENIZED, store = Store.YES, indexNullAs = Field.DEFAULT_NULL_TOKEN),
			@Field(name = "endMonth_Sort", index = Index.UN_TOKENIZED, indexNullAs = Field.DEFAULT_NULL_TOKEN) })
	private Integer endMonth;


	@Column(name = "\"ENDDAY\"", length = 3, columnDefinition = "TINYINT")
	@Fields({
			@Field(index = Index.TOKENIZED, store = Store.YES, indexNullAs = Field.DEFAULT_NULL_TOKEN),
			@Field(name = "endDay_Sort", index = Index.UN_TOKENIZED, indexNullAs = Field.DEFAULT_NULL_TOKEN) })
	@NumericFields({ @NumericField(forField = "endDay"),
			@NumericField(forField = "endDay_Sort") })
	private Integer endDay;
	
	@Column(name = "\"TITLE\"")
	@Field(index = Index.TOKENIZED, store = Store.YES, indexNullAs = Field.DEFAULT_NULL_TOKEN)
	private String title;
	
	@Column(name = "\"DATENOTES\"", columnDefinition = "LONGTEXT")
	@Field(index = Index.TOKENIZED, store = Store.YES, indexNullAs = Field.DEFAULT_NULL_TOKEN)
	private String dateNotes;
	

	@Column(name = "\"ORGANIZATIONALCRITERIA\"", columnDefinition = "LONGTEXT")
	@Field(index = Index.TOKENIZED, store = Store.YES, indexNullAs = Field.DEFAULT_NULL_TOKEN)
	private String organizationalCriteria;

	@Column(name = "\"PAGINATION\"", columnDefinition = "LONGTEXT")
	@Field(index = Index.TOKENIZED, store = Store.YES, indexNullAs = Field.DEFAULT_NULL_TOKEN)
	private String pagination;
	
	@Column(name = "\"PAGINATIONNOTES\"", columnDefinition = "LONGTEXT")
	@Field(index = Index.TOKENIZED, store = Store.YES, indexNullAs = Field.DEFAULT_NULL_TOKEN)
	private String paginationNotes;

	@Column(name = "\"FOLIOCOUNT\"", length = 50)
	@Fields({
			@Field(index = Index.TOKENIZED, store = Store.YES, indexNullAs = Field.DEFAULT_NULL_TOKEN),
			@Field(name = "folioCount_Sort", index = Index.UN_TOKENIZED, indexNullAs = Field.DEFAULT_NULL_TOKEN) })
	private String folioCount;

	@Column(name = "\"MISSINGFOLIOS\"", length = 50)
	@Fields({
			@Field(index = Index.TOKENIZED, store = Store.YES, indexNullAs = Field.DEFAULT_NULL_TOKEN),
			@Field(name = "missingFoliosCount_Sort", index = Index.UN_TOKENIZED, indexNullAs = Field.DEFAULT_NULL_TOKEN) })
	private String missingFolios;
	
	@Column(name = "\"CONTEXT\"", columnDefinition = "LONGTEXT")
	@Field(index = Index.TOKENIZED, store = Store.YES, indexNullAs = Field.DEFAULT_NULL_TOKEN)
	private String context;
	
	@Column(name = "\"OTHERNOTES\"")
	@Field(index = Index.TOKENIZED, store = Store.YES, indexNullAs = Field.DEFAULT_NULL_TOKEN)
	private String otherNotes;
	
	@Column(name = "\"GUARDIA\"")
	@Field(index = Index.TOKENIZED, store = Store.YES, indexNullAs = Field.DEFAULT_NULL_TOKEN)
	private Integer GUARDIA;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "\"createdBy\"", nullable = true)
	private User createdBy;
	

	@Column(name = "\"DATECREATED\"")
	@Temporal(TemporalType.TIMESTAMP)
	@Field(index = Index.UN_TOKENIZED, store = Store.NO, indexNullAs = Field.DEFAULT_NULL_TOKEN)
	@DateBridge(resolution = Resolution.DAY)
	private Date dateCreated;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "\"lastUpdateBy\"", nullable = true)
	private User lastUpdateBy;

	@Column(name = "\"DATELASTUPDATE\"")
	@Temporal(TemporalType.TIMESTAMP)
	@Field(index = Index.UN_TOKENIZED, store = Store.NO, indexNullAs = Field.DEFAULT_NULL_TOKEN)
	private Date dateLastUpdate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "\"deletedBy\"", nullable = true)
	private User deletedBy;
	
	@Column(name = "\"DATEDELETED\"")
	@Temporal(TemporalType.TIMESTAMP)
	@Field(index = Index.UN_TOKENIZED, store = Store.NO, indexNullAs = Field.DEFAULT_NULL_TOKEN)
	@DateBridge(resolution = Resolution.DAY)
	private Date dateDeleted;
	
	@Column(name = "\"LOGICALDELETE\"", length = 1, columnDefinition = "tinyint default 0", nullable = false)
	@Field(index = Index.UN_TOKENIZED, store = Store.YES, indexNullAs = Field.DEFAULT_NULL_TOKEN)
	@FieldBridge(impl = BooleanBridge.class)
	private Boolean logicalDelete;

	@Column(name = "ae_count", columnDefinition = "int default 0", nullable = false)
	private Integer aeCount = 0;

	
	public InsertEntity() {
		super();
	}
	
	public InsertEntity(Integer insertId, String insertName, Integer volume) {
		super();
		this.insertId = insertId;
		this.insertName = insertName;
		this.volume = volume;
	}

	public Integer getInsertId() {
		return insertId;
	}

	public void setInsertId(Integer insertId) {
		this.insertId = insertId;
	}

	public String getInsertName() {
		return insertName;
	}

	public void setInsertName(String insertName) {
		this.insertName = insertName;
	}

	public Integer getVolume() {
		return volume;
	}

	public void setVolume(Integer volume) {
		this.volume = volume;
	}

	public Volume getVolumeEntity() {
		return volumeEntity;
	}

	public List<UploadInfoEntity> getUploadInfoEntities() {
		return uploadInfoEntities;
	}

	public void setUploadInfoEntities(List<UploadInfoEntity> uploadInfoEntities) {
		this.uploadInfoEntities = uploadInfoEntities;
	}

	public void setVolumeEntity(Volume volumeEntity) {
		this.volumeEntity = volumeEntity;
	}

	public Integer getStartYear() {
		return startYear;
	}

	public void setStartYear(Integer startYear) {
		this.startYear = startYear;
	}

	public Integer getStartMonth() {
		return startMonth;
	}

	public void setStartMonth(Integer startMonth) {
		this.startMonth = startMonth;
	}

	public Integer getStartDay() {
		return startDay;
	}

	public void setStartDay(Integer startDay) {
		this.startDay = startDay;
	}

	public Integer getEndYear() {
		return endYear;
	}

	public void setEndYear(Integer endYear) {
		this.endYear = endYear;
	}

	public Integer getEndMonth() {
		return endMonth;
	}

	public void setEndMonth(Integer endMonth) {
		this.endMonth = endMonth;
	}

	public Integer getEndDay() {
		return endDay;
	}

	public void setEndDay(Integer endDay) {
		this.endDay = endDay;
	}

	public String getDateNotes() {
		return dateNotes;
	}

	public void setDateNotes(String dateNotes) {
		this.dateNotes = dateNotes;
	}

	public String getOrganizationalCriteria() {
		return organizationalCriteria;
	}

	public void setOrganizationalCriteria(String organizationalCriteria) {
		this.organizationalCriteria = organizationalCriteria;
	}

	public String getPagination() {
		return pagination;
	}

	public void setPagination(String pagination) {
		this.pagination = pagination;
	}

	public String getPaginationNotes() {
		return paginationNotes;
	}

	public void setPaginationNotes(String paginationNotes) {
		this.paginationNotes = paginationNotes;
	}

	public String getFolioCount() {
		return folioCount;
	}

	public void setFolioCount(String folioCount) {
		this.folioCount = folioCount;
	}

	public String getMissingFolios() {
		return missingFolios;
	}

	public void setMissingFolios(String missingFolios) {
		this.missingFolios = missingFolios;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getOtherNotes() {
		return otherNotes;
	}

	public void setOtherNotes(String otherNotes) {
		this.otherNotes = otherNotes;
	}

	public Integer getGUARDIA() {
		return GUARDIA;
	}

	public void setGUARDIA(Integer gUARDIA) {
		GUARDIA = gUARDIA;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public User getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(User lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

	public Date getDateLastUpdate() {
		return dateLastUpdate;
	}

	public void setDateLastUpdate(Date dateLastUpdate) {
		this.dateLastUpdate = dateLastUpdate;
	}

	public User getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(User deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDateDeleted() {
		return dateDeleted;
	}

	public void setDateDeleted(Date dateDeleted) {
		this.dateDeleted = dateDeleted;
	}

	public Boolean getLogicalDelete() {
		return logicalDelete;
	}

	public void setLogicalDelete(Boolean logicalDelete) {
		this.logicalDelete = logicalDelete;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}

	public Integer getAeCount() {
		return aeCount;
	}

	public void setAeCount(Integer aeCount) {
		this.aeCount = aeCount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((insertId == null) ? 0 : insertId.hashCode());
		result = prime * result
				+ ((insertName == null) ? 0 : insertName.hashCode());
		result = prime * result + ((volume == null) ? 0 : volume.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InsertEntity other = (InsertEntity) obj;
		if (insertId == null) {
			if (other.insertId != null)
				return false;
		} else if (!insertId.equals(other.insertId))
			return false;
		if (insertName == null) {
			if (other.insertName != null)
				return false;
		} else if (!insertName.equals(other.insertName))
			return false;
		if (volume == null) {
			if (other.volume != null)
				return false;
		} else if (!volume.equals(other.volume))
			return false;
		return true;
	}

	
	
	
	
}
