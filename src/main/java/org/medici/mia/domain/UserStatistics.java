package org.medici.mia.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author fsolfato
 *
 */
public class UserStatistics implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Date joinDate;
	
	private Date lastUpdateDate;
	
	private Long uploadsCount;
	
	private Long documentsCount;
	
	private Long biographicalRecordsCount;

	private Long geographicalRecordsCount;
	
	private Long commentsCount;

	public Date getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdate) {
		this.lastUpdateDate = lastUpdate;
	}

	public Long getUploadsCount() {
		return uploadsCount;
	}

	public void setUploadsCount(Long uploads) {
		this.uploadsCount = uploads;
	}

	public Long getDocumentsCount() {
		return documentsCount;
	}

	public void setDocumentsCount(Long documentsCreated) {
		this.documentsCount = documentsCreated;
	}

	public Long getBiographicalRecordsCount() {
		return biographicalRecordsCount;
	}

	public void setBiographicalRecordsCount(Long biographicalRecordsCreated) {
		this.biographicalRecordsCount = biographicalRecordsCreated;
	}

	public Long getGeographicalRecordsCount() {
		return geographicalRecordsCount;
	}

	public void setGeographicalRecordsCount(Long geographicalRecordsCreated) {
		this.geographicalRecordsCount = geographicalRecordsCreated;
	}

	public Long getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(Long comments) {
		this.commentsCount = comments;
	}
}
