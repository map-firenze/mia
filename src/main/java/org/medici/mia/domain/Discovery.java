package org.medici.mia.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "\"tblDiscoveries\"")
public class Discovery implements Serializable {

	private static final long serialVersionUID = -5533772201292314195L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column (name="\"id\"", length=10, nullable=false)
	private Integer id;

	@Column (name="\"title\"", length=100, nullable=false)
	private String title;
	
	@Column (name="\"whyHistImp\"", nullable=false)
	@Lob
	private String whyHistImp;
	
	@Column (name="\"shortNotice\"", nullable=false)
	@Lob
	private String shortNotice;
	
	@Column (name="\"bibliography\"")
	@Lob
	private String bibliography;
	
	@Column(name = "logicalDelete", nullable = false, columnDefinition = "TINYINT(1)")
	private Integer logicalDelete;
	
	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.STRING)
	private DiscoveryStatus status;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="idTblDocumentEnts")
	private MiaDocumentEntity document;
	
	@Column(name = "\"submissionDate\"", nullable = false)
	private Date submissionDate;

	@ManyToMany
	@JoinTable(name = "tblDiscoveriesRelLink", joinColumns = { @JoinColumn(name = "idDiscoveries") }, inverseJoinColumns = { @JoinColumn(name = "idTblDocumentEnts") })
	private List<MiaDocumentEntity> links;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinColumn(name="idDiscoveries")
	private List<DiscoveryUpload> discoveriesUpload;
	
	public Discovery() {
		status = DiscoveryStatus.DRAFT;
		logicalDelete = 0;
		links = new LinkedList<MiaDocumentEntity>();
		discoveriesUpload = new LinkedList<DiscoveryUpload>();
	}
	
	public Integer getId() {
		return id;
	}
	
	void setId(Integer id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWhyHistImp() {
		return whyHistImp;
	}

	public void setWhyHistImp(String whyHistImp) {
		this.whyHistImp = whyHistImp;
	}

	public String getShortNotice() {
		return shortNotice;
	}

	public void setShortNotice(String shortNotice) {
		this.shortNotice = shortNotice;
	}

	public String getBibliography() {
		return bibliography;
	}

	public void setBibliography(String bibliography) {
		this.bibliography = bibliography;
	}

	public Integer getLogicalDelete() {
		return logicalDelete;
	}

	public void setLogicalDelete(Integer logicalDelete) {
		this.logicalDelete = logicalDelete;
	}

	public DiscoveryStatus getStatus() {
		return status;
	}
	
	public void setStatus(DiscoveryStatus status) {
		this.status = status;
	}
	
	public MiaDocumentEntity getDocument() {
		return document;
	}
	
	public void setDocument(MiaDocumentEntity document) {
		this.document = document;
	}

	public Date getSubmissionDate() {
		return submissionDate;
	}

	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}
	
	public List<MiaDocumentEntity> getLinks() {
		return links;
	}
	
	public void setLinks(List<MiaDocumentEntity> links) {
		this.links = links;
	}

	public List<DiscoveryUpload> getDiscoveriesUpload() {
		return discoveriesUpload;
	}
	
	public void setDiscoveriesUpload(List<DiscoveryUpload> discoveriesUpload) {
		this.discoveriesUpload = discoveriesUpload;
	}
}
