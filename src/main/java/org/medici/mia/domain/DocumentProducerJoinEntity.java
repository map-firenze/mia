package org.medici.mia.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author utente
 *
 */
@Entity
@Table(name = "\"tblDocumentsEntsPeople\"")
public class DocumentProducerJoinEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "documentEntityId", length = 254, nullable = false)
	private Integer documentEntityId;

	@Id
	@Column(name = "producer")
	private Integer producer;
	
	@Column(name = "producerUnsure")
	private String producerUnsure;

	public DocumentProducerJoinEntity() {
		super();
	}

	public DocumentProducerJoinEntity(Integer documentEntityId, Integer producer, String producerUnsure) {
		super();
		this.documentEntityId = documentEntityId;
		this.producer = producer;
		this.producerUnsure = producerUnsure;
	}

	public Integer getDocumentEntityId() {
		return documentEntityId;
	}

	public void setDocumentEntityId(Integer documentEntityId) {
		this.documentEntityId = documentEntityId;
	}

	public Integer getProducer() {
		return producer;
	}

	public void setProducer(Integer producer) {
		this.producer = producer;
	}

	public String getProducerUnsure() {
		return producerUnsure;
	}

	public void setProducerUnsure(String producerUnsure) {
		this.producerUnsure = producerUnsure;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((documentEntityId == null) ? 0 : documentEntityId.hashCode());
		result = prime * result
				+ ((producer == null) ? 0 : producer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocumentProducerJoinEntity other = (DocumentProducerJoinEntity) obj;
		if (documentEntityId == null) {
			if (other.documentEntityId != null)
				return false;
		} else if (!documentEntityId.equals(other.documentEntityId))
			return false;
		if (producer == null) {
			if (other.producer != null)
				return false;
		} else if (!producer.equals(other.producer))
			return false;
		return true;
	}

}
