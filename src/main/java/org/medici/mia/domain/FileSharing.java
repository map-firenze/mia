/*
 * FileSharing.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "\"tblFileSharing\"")
public class FileSharing {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "\"idTblUpload\"", nullable = true)
	private UploadInfoEntity tblUpload;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "\"idTblUploadedFiles\"", nullable = true)
	private UploadFileEntity tblUploadedFiles;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "\"account\"", nullable = true)
	private User user;
	
	public FileSharing(UploadInfoEntity tblUpload, UploadFileEntity tblUploadedFiles, User user) {
		this.tblUpload = tblUpload;
		this.tblUploadedFiles = tblUploadedFiles;
		this.user = user;
	}
	
	public FileSharing() {
		
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UploadInfoEntity getTblUpload() {
		return tblUpload;
	}

	public void setTblUpload(UploadInfoEntity tblUpload) {
		this.tblUpload = tblUpload;
	}

	public UploadFileEntity getTblUploadedFiles() {
		return tblUploadedFiles;
	}

	public void setTblUploadedFiles(UploadFileEntity tblUploadedFiles) {
		this.tblUploadedFiles = tblUploadedFiles;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
	
}
