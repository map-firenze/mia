package org.medici.mia.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "\"tblSeriesList\"")
public class SeriesEntity implements Serializable {

	private static final long serialVersionUID = 2357363226348351617L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "\"SERIESREFNUM\"", length = 10, nullable = false)
	private Integer seriesId;

	@Column(name = "collection")
	private Integer collection;

	@Column(name = "\"TITLE\"")
	private String title;

	@Column(name = "\"SUBTITLE1\"")
	private String subtitle1;

	@Column(name = "\"SUBTITLE2\"")
	private String subtitle2;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(insertable = false, updatable = false, name = "collection", nullable = true)
	private CollectionEntity collectionEntity;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "seriesEntity", cascade = {
			CascadeType.PERSIST, CascadeType.MERGE })
	private List<UploadInfoEntity> uploadInfoEntities;

	public SeriesEntity() {
		super();
	}

	public SeriesEntity(Integer seriesId, Integer collection, String title,
			String subtitle1, String subtitle2) {
		super();
		this.seriesId = seriesId;
		this.collection = collection;
		this.title = title;
		this.subtitle1 = subtitle1;
		this.subtitle2 = subtitle2;
	}

	public Integer getSeriesId() {
		return seriesId;
	}

	public void setSeriesId(Integer seriesId) {
		this.seriesId = seriesId;
	}

	public Integer getCollection() {
		return collection;
	}

	public void setCollection(Integer collection) {
		this.collection = collection;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle1() {
		return subtitle1;
	}

	public void setSubtitle1(String subtitle1) {
		this.subtitle1 = subtitle1;
	}

	public String getSubtitle2() {
		return subtitle2;
	}

	public void setSubtitle2(String subtitle2) {
		this.subtitle2 = subtitle2;
	}

	public CollectionEntity getCollectionEntity() {
		return collectionEntity;
	}

	public void setCollectionEntity(CollectionEntity collectionEntity) {
		this.collectionEntity = collectionEntity;
	}

	public List<UploadInfoEntity> getUploadInfoEntities() {
		return uploadInfoEntities;
	}

	public void setUploadInfoEntities(List<UploadInfoEntity> uploadInfoEntities) {
		this.uploadInfoEntities = uploadInfoEntities;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((seriesId == null) ? 0 : seriesId.hashCode());
		result = prime * result
				+ ((subtitle1 == null) ? 0 : subtitle1.hashCode());
		result = prime * result
				+ ((subtitle2 == null) ? 0 : subtitle2.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SeriesEntity other = (SeriesEntity) obj;
		if (seriesId == null) {
			if (other.seriesId != null)
				return false;
		} else if (!seriesId.equals(other.seriesId))
			return false;
		if (subtitle1 == null) {
			if (other.subtitle1 != null)
				return false;
		} else if (!subtitle1.equals(other.subtitle1))
			return false;
		if (subtitle2 == null) {
			if (other.subtitle2 != null)
				return false;
		} else if (!subtitle2.equals(other.subtitle2))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}
