package org.medici.mia.controller.archive;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.medici.mia.service.myarchive.MyArchiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Controller
@RequestMapping("/browseGeneralArchive")
public class GeneralArchiveController {

	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private MyArchiveService archiveService;

	// http://localhost:8181/Mia/json/browseGeneralArchive/browseArchivalLocationWithPagination/2/3/1
	@RequestMapping(value = "browseArchivalLocationWithPagination/{numOfAePerPage}/{numOfThumbsPerRow}/{page}", method = RequestMethod.POST)
	public @ResponseBody HashMap<String, List<MyArchiveBean>> getGeneralArchivesWithPaginationn(
			@PathVariable("numOfAePerPage") Integer numOfAePerPage,
			@PathVariable("numOfThumbsPerRow") Integer numOfThumbsPerRow,
			@PathVariable("page") Integer page,
			@RequestBody BrowseArchivalLocationJson locationJson) {

		HashMap<String, List<MyArchiveBean>> archiveHash = new HashMap<String, List<MyArchiveBean>>();

		List<MyArchiveBean> archives = getArchiveService().getGeneralArchives(
				locationJson, numOfAePerPage, numOfThumbsPerRow, page);

		archiveHash.put("generalentities", archives);

		return archiveHash;

	}

	// http://localhost:8181/Mia/json/browseGeneralArchive/browseArchivalLocation
	@RequestMapping(value = "browseArchivalLocation", method = RequestMethod.POST)
	public @ResponseBody HashMap<String, List<MyArchiveBean>> getArchives(
			@RequestBody BrowseArchivalLocationJson locationJson) {

		HashMap<String, List<MyArchiveBean>> archiveHash = new HashMap<String, List<MyArchiveBean>>();

		List<MyArchiveBean> archives = getArchiveService().getGeneralArchives(
				locationJson);

		archiveHash.put("generalentities", archives);

		return archiveHash;

	}

	public MyArchiveService getArchiveService() {
		return archiveService;
	}

	public void setArchiveService(MyArchiveService archiveService) {
		this.archiveService = archiveService;
	}

}