package org.medici.mia.controller.archive;

import java.io.Serializable;

/** 
 * 
 * @author Shadab Bigdel (<a href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class BrowseArchivalLocationJson implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Integer repositoryId;
	private Integer collectionId;
	private Integer seriesId;
	private Integer volumeId;
	private Integer insertId;

	public Integer getRepositoryId() {
		return repositoryId;
	}

	public void setRepositoryId(Integer repositoryId) {
		this.repositoryId = repositoryId;
	}

	public Integer getCollectionId() {
		return collectionId;
	}

	public void setCollectionId(Integer collectionId) {
		this.collectionId = collectionId;
	}

	public Integer getSeriesId() {
		return seriesId;
	}

	public void setSeriesId(Integer seriesId) {
		this.seriesId = seriesId;
	}

	public Integer getVolumeId() {
		return volumeId;
	}

	public void setVolumeId(Integer volumeId) {
		this.volumeId = volumeId;
	}

	public Integer getInsertId() {
		return insertId;
	}

	public void setInsertId(Integer insertId) {
		this.insertId = insertId;
	}

	

}
