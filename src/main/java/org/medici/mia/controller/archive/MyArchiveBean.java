package org.medici.mia.controller.archive;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.json.CollectionJson;
import org.medici.mia.common.json.InsertJson;
import org.medici.mia.common.json.PaginationJson;
import org.medici.mia.common.json.RepositoryJson;
import org.medici.mia.common.json.SeriesJson;
import org.medici.mia.common.json.VolumeJson;
import org.medici.mia.common.util.FileUtils;
import org.medici.mia.domain.IGenericEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.UploadInfoEntity;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class MyArchiveBean implements IAEntityJson {

	private static final long serialVersionUID = 1L;

	private List<MyArchiveFile> thumbsFiles;

	private RepositoryJson repository;
	private CollectionJson collection;
	private SeriesJson series;
	private VolumeJson volume;
	private InsertJson insert;
	private String owner;
	private String aeName;
	private String aeDescription;
	private Integer uploadId;
	private PaginationJson pagination;
	private Integer logicalDelete;
	private Integer filePrivacy;
	private Integer commentsCount;
	private Boolean noImage;

	public List<MyArchiveFile> getThumbsFiles() {
		return thumbsFiles;
	}

	public void setThumbsFiles(List<MyArchiveFile> thumbsFiles) {
		this.thumbsFiles = thumbsFiles;
	}

	public RepositoryJson getRepository() {
		return repository;
	}

	public void setRepository(RepositoryJson repository) {
		this.repository = repository;
	}

	public CollectionJson getCollection() {
		return collection;
	}

	public void setCollection(CollectionJson collection) {
		this.collection = collection;
	}

	public SeriesJson getSeries() {
		return series;
	}

	public void setSeries(SeriesJson series) {
		this.series = series;
	}

	public VolumeJson getVolume() {
		return volume;
	}

	public void setVolume(VolumeJson volume) {
		this.volume = volume;
	}

	public InsertJson getInsert() {
		return insert;
	}

	public void setLogicalDelete(Integer logicalDelete) {
		this.logicalDelete = logicalDelete;
	}
	
	public Integer getFilePrivacy() {
		return filePrivacy;
	}
	
	public void setFilePrivacy(Integer filePrivacy) {
		this.filePrivacy = filePrivacy;
	}

	public void setInsert(InsertJson insert) {
		this.insert = insert;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Integer getUploadId() {
		return uploadId;
	}

	public Integer getLogicalDelete() {
		return logicalDelete;
	}

	public void setUploadId(Integer uploadId) {
		this.uploadId = uploadId;
	}

	public String getAeName() {
		return aeName;
	}

	public void setAeName(String aeName) {
		this.aeName = aeName;
	}

	public PaginationJson getPagination() {
		return pagination;
	}

	public void setPagination(PaginationJson pagination) {
		this.pagination = pagination;
	}

	public String getAeDescription() {
		return aeDescription;
	}

	public void setAeDescription(String aeDescription) {
		this.aeDescription = aeDescription;
	}

	public Integer getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(Integer commentsCount) {
		this.commentsCount = commentsCount;
	}
	
	public Boolean isNoImage() {
		return noImage;
	}

	public void setNoImage(Boolean noImage) {
		this.noImage = noImage;
	}

	@Override
	public void toJson(IGenericEntity entity) {
		if (entity == null) {
			return;
		}
		if (entity instanceof UploadInfoEntity) {
			this.toArchiveEntityJson((UploadInfoEntity) entity, 0, 1);
		}

	}

	@Override
	public void toArchiveEntityJson(UploadInfoEntity upload,
			int numOfThumbsPerRow, int pageForFile) {

		this.setUploadId(upload.getUploadInfoId());
		this.setAeName(upload.getAeName());
		this.setOwner(upload.getOwner());
		this.setFilePrivacy(upload.getFilePrivacy());
		this.setCommentsCount(upload.getCommentsCount());

		RepositoryJson repository = new RepositoryJson();
		repository.setRepositoryId(upload.getRepositoryEntity()
				.getRepositoryId().toString());
		repository.setRepositoryName(upload.getRepositoryEntity()
				.getRepositoryName());
		repository.setRepositoryAbbreviation(upload.getRepositoryEntity()
				.getRepositoryAbbreviation());
		repository.setRepositoryDescription(upload.getRepositoryEntity()
				.getRepositoryDescription());
		repository.setRepositoryLocation(upload.getRepositoryEntity()
				.getGoogleLocation().getgPlaceId());
		repository.setRepositoryCity(upload.getRepositoryEntity()
				.getGoogleLocation().getCity());
		repository.setRepositoryCountry(upload.getRepositoryEntity()
				.getGoogleLocation().getCountry());
		this.setRepository(repository);

		CollectionJson collection = new CollectionJson();
		collection.setCollectionName(upload.getCollectionEntity()
				.getCollectionName());
		collection.setCollectionAbbreviation(upload.getCollectionEntity()
				.getCollectionAbbreviation());
		collection.setCollectionDescription(upload.getCollectionEntity()
				.getCollectionDescription());
		collection.setCollectionId(upload.getCollectionEntity()
				.getCollectionId().toString());
		this.setCollection(collection);

		if (upload.getSeriesEntity() != null) {
			SeriesJson series = new SeriesJson();
			series.setSeriesName(upload.getSeriesEntity().getTitle());
			series.setSeriesSubtitle(upload.getSeriesEntity().getSubtitle1());
			series.setSeriesId(upload.getSeriesEntity().getSeriesId().toString());
			this.setSeries(series);
		}

		if (upload.getVolumeEntity() != null) {
			VolumeJson volume = new VolumeJson();
			volume.setVolumeName(((UploadInfoEntity) upload).getVolumeEntity()
					.getVolume());
			volume.setVolumeId(((UploadInfoEntity) upload).getVolumeEntity()
					.getSummaryId().toString());
			this.setVolume(volume);
		}

		if (upload.getInsertEntity() != null) {

			InsertJson insert = new InsertJson();
			insert.setInsertName(((UploadInfoEntity) upload).getInsertEntity()
					.getInsertName());
			insert.setInsertId(((UploadInfoEntity) upload).getInsertEntity()
					.getInsertId().toString());
			this.setInsert(insert);
		}

		// Build of the real path
		String insertName = "";
		if (upload.getInsertEntity()!=null){
			insertName = upload.getInsertEntity().getInsertName();
		}
		
		String volume = "";
		if (upload.getVolumeEntity()!=null){
			volume = upload.getVolumeEntity().getVolume();
		}
		String realPath = org.medici.mia.common.util.FileUtils
				.getRealPathJSONResponse(((UploadInfoEntity) upload)
						.getRepositoryEntity().getLocation(),
						((UploadInfoEntity) upload).getRepositoryEntity()
								.getRepositoryAbbreviation(),
						((UploadInfoEntity) upload).getCollectionEntity()
								.getCollectionAbbreviation(),
						volume, insertName);

		if (((UploadInfoEntity) upload).getUploadFileEntities() != null
				&& !((UploadInfoEntity) upload).getUploadFileEntities()
						.isEmpty()) {
			List<UploadFileEntity> ufEntities = ((UploadInfoEntity) upload)
					.getUploadFileEntities();

			List<MyArchiveFile> thumbsFiles = new ArrayList<MyArchiveFile>();

			PaginationJson pagination = new PaginationJson();
			pagination.setTotalPagesForSingleAe(calculateBlockNumber(
					numOfThumbsPerRow, ufEntities.size()));

			int start = (pageForFile - 1) * numOfThumbsPerRow;
			if (start > ufEntities.size())
				start = 0;
			int end = start + numOfThumbsPerRow;
			if (end > ufEntities.size() || numOfThumbsPerRow == 0)
				end = ufEntities.size();

			for (int i = start; i < end; i++) {

				UploadFileEntity uf = ufEntities.get(i);

				MyArchiveFile thumbsFile = new MyArchiveFile();
				thumbsFile.setFilePath(realPath + FileUtils.THUMB_FILES_DIR
						+ FileUtils.OS_SLASH + uf.getFilename() + "&WID=250");
				thumbsFile.setImageOrder(uf.getImageOrder());
				thumbsFile.setConverted(uf.getTiledConversionProcess());
				thumbsFile.setFullScreenView("");
				thumbsFile.setUploadFileId(uf.getUploadFileId());
				thumbsFile.setPrivacy(uf.getFilePrivacy());
				thumbsFile.setFileName(uf.getFileOriginalName());
				thumbsFiles.add(thumbsFile);
			}

			// archiveBean.setOriginalFiles(originalFiles);
			this.setThumbsFiles(thumbsFiles);
			this.setPagination(pagination);
			this.setAeDescription(((UploadInfoEntity) upload)
					.getAeDescription());

		}

	}

	@Override
	public IGenericEntity toEntity(IGenericEntity entity) {
		// TODO Auto-generated method stub
		return null;
	}

	private int calculateBlockNumber(int partialNumber, Integer TotalNumber) {
		// check for the integrity of the division
		if (partialNumber != 0) {
			if (TotalNumber % partialNumber == 0)
				return TotalNumber / partialNumber;
			else
				return TotalNumber / partialNumber + 1;
		} else {
			return 0;
		}
	}

}
