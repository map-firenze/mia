/*
 * MyArchiveRepository.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.controller.archive;

/**
 * 
 * @author User
 *
 */
public class MyArchiveRepository {

	private String  repositoryId;
	private String  repositoryName;
	private String  repositoryAbbreviation;
	private String  repositoryDescription;
	private String  repositoryLocation;
	private String  repositoryCity;
	private String  repositoryCountry;
	
	
	public String getRepositoryId() {
		return repositoryId;
	}
	public void setRepositoryId(String repositoryId) {
		this.repositoryId = repositoryId;
	}
	public String getRepositoryName() {
		return repositoryName;
	}
	public void setRepositoryName(String repositoryName) {
		this.repositoryName = repositoryName;
	}
	public String getRepositoryAbbreviation() {
		return repositoryAbbreviation;
	}
	public void setRepositoryAbbreviation(String repositoryAbbreviation) {
		this.repositoryAbbreviation = repositoryAbbreviation;
	}
	public String getRepositoryDescription() {
		return repositoryDescription;
	}
	public void setRepositoryDescription(String repositoryDescription) {
		this.repositoryDescription = repositoryDescription;
	}
	public String getRepositoryLocation() {
		return repositoryLocation;
	}
	public void setRepositoryLocation(String repositoryLocation) {
		this.repositoryLocation = repositoryLocation;
	}
	public String getRepositoryCity() {
		return repositoryCity;
	}
	public void setRepositoryCity(String repositoryCity) {
		this.repositoryCity = repositoryCity;
	}
	public String getRepositoryCountry() {
		return repositoryCountry;
	}
	public void setRepositoryCountry(String repositoryCountry) {
		this.repositoryCountry = repositoryCountry;
	}
}
