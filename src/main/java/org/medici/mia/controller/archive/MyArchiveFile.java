package org.medici.mia.controller.archive;

import org.medici.mia.common.json.folio.FolioJson;
import org.medici.mia.service.folio.UploadCollectionFolioJson;

import java.util.List;

/**
 * 
 * @author Shadab Bigdel (<a href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class MyArchiveFile {

	private String filePath;
	private Integer converted;
	private String fullScreenView;
	private Integer uploadFileId;
	private Integer privacy;
	private Integer imageOrder;
	private String fileName;
	private List<FolioJson> folios;
	private Boolean canView;
	private Boolean canSetPrivate;
	private Integer documentCount;

	public Boolean getCanView() {
		return canView;
	}

	public void setCanView(Boolean canView) {
		this.canView = canView;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Integer getConverted() {
		return converted;
	}

	public void setConverted(Integer converted) {
		this.converted = converted;
	}

	public String getFullScreenView() {
		return fullScreenView;
	}

	public void setFullScreenView(String fullScreenView) {
		this.fullScreenView = fullScreenView;
	}

	public Integer getUploadFileId() {
		return uploadFileId;
	}

	public void setUploadFileId(Integer uploadFileId) {
		this.uploadFileId = uploadFileId;
	}

	public Integer getPrivacy() {
		return privacy;
	}

	public void setPrivacy(Integer privacy) {
		this.privacy = privacy;
	}

	public Integer getImageOrder() {
		return imageOrder;
	}

	public void setImageOrder(Integer imageOrder) {
		this.imageOrder = imageOrder;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<FolioJson> getFolios() {
		return folios;
	}

	public void setFolios(List<FolioJson> folios) {
		this.folios = folios;
	}

	public Boolean getCanSetPrivate() {
		return canSetPrivate;
	}

	public void setCanSetPrivate(Boolean canSetPrivate) {
		this.canSetPrivate = canSetPrivate;
	}

	public Integer getDocumentCount() {
		return documentCount;
	}

	public void setDocumentCount(Integer documentCount) {
		this.documentCount = documentCount;
	}

}
