package org.medici.mia.controller.archive;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Future;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.medici.mia.common.json.DocumentAEJson;
import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.GenericSetPrivacyResponseJson;
import org.medici.mia.common.json.GenericUploadResponseJson;
import org.medici.mia.common.json.RotateImageJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.UploadFileJson;
import org.medici.mia.common.json.administration.AdminUserJson;
import org.medici.mia.controller.upload.FileUploadForm;
import org.medici.mia.controller.upload.UploadBean;
import org.medici.mia.domain.CollectionEntity;
import org.medici.mia.domain.GoogleLocation;
import org.medici.mia.domain.InsertEntity;
import org.medici.mia.domain.RepositoryEntity;
import org.medici.mia.domain.SeriesEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.User;
import org.medici.mia.domain.Volume;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.myarchive.MyArchiveService;
import org.medici.mia.service.upload.UploadService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.underscore.Predicate;
import com.github.underscore.U;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Controller
@RequestMapping("/archive")
public class MyArchiveController {

	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private MiaDocumentService miaDocumentService;
	
	@Autowired
	private MyArchiveService archiveService;

	@Autowired
	private UploadService uploadService;

	@Autowired
	private UserService userService;

	@Autowired
	private HistoryLogService historyLogService;

	@PersistenceContext
	private EntityManager entityManager;

	private boolean validateId(String id){
		try {
			Integer.valueOf(id);
			return true;
		} catch (NumberFormatException ex){
			return false;
		}
	}

	// http://localhost:8181/Mia/json/archive/getArchives/2/3/1
	@RequestMapping(value = "getArchives/{numOfAePerPage}/{numOfThumbsPerRow}/{page}", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, Object> getArchives(
			@PathVariable("numOfAePerPage") Integer numOfAePerPage,
			@PathVariable("numOfThumbsPerRow") Integer numOfThumbsPerRow,
			@PathVariable("page") Integer page, HttpSession session) {

		String owner = "";
		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()).getUsername());
		owner = user.getAccount();

		HashMap<String, Object> archiveHash = new HashMap<String, Object>();

		List<MyArchiveBean> archives = getArchiveService().getArchivesPaginated(owner,
				numOfAePerPage, numOfThumbsPerRow, page);

		archiveHash.put("aentities", archives);
		archiveHash.put("count", getArchiveService().countAEsForOwner(owner));

		return archiveHash;

	}

	// http://localhost:8181/Mia/json/archive/getArchives/aentity/82/16/1
	@RequestMapping(value = "getArchives/aentity/{uploadId}/{numOfThumbsPerPage}/{pageForFile}", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, Object> getSingleArchive(
			@PathVariable("uploadId") Integer uploadId,
			@PathVariable("numOfThumbsPerPage") Integer numOfThumbsPerPage,
			@PathVariable("pageForFile") Integer pageForFile,
			HttpSession session) {

		HashMap<String, Object> archiveHash = new HashMap<String, Object>();
		MyArchiveBean myArchive = getArchiveService().getArchiveByUploadIdPaginated(
				uploadId, numOfThumbsPerPage, pageForFile);
		
		if (!myArchive.isNoImage()) {
			for (MyArchiveFile thumb : myArchive.getThumbsFiles()) {
				thumb.setDocumentCount(miaDocumentService.countDocumentsByUploadFileId(thumb.getUploadFileId()));
			}
		}
		
		archiveHash.put("aentity", myArchive);
		archiveHash.put("count", getArchiveService().countImagesForAE(uploadId));

		return archiveHash;

	}

	// http://localhost:8181/Mia/json/archive/getArchives/aentityByUploadFile/82/16/1
	@RequestMapping(value = "getArchives/aentityByUploadFile/{uploadFileId}/{numOfThumbsPerPage}/{pageForFile}", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, Object> getSingleArchiveByUploadFile(
			@PathVariable("uploadFileId") Integer uploadFileId,
			@PathVariable("numOfThumbsPerPage") Integer numOfThumbsPerPage,
			@PathVariable("pageForFile") Integer pageForFile,
			HttpSession session) {

		HashMap<String, Object> archiveHash = new HashMap<String, Object>();
		MyArchiveBean myArchive = getArchiveService().getArchiveByUploadFileId(
				uploadFileId, numOfThumbsPerPage, pageForFile);

		archiveHash.put("aentity", myArchive);
		archiveHash.put("count", getArchiveService().getArchiveFilesCountByUploadFileId(uploadFileId));

		return archiveHash;

	}

	// method to set privacy to one file
	// http://localhost:8181/Mia/json/archive/setPrivacyOneFile/72/1
	@RequestMapping(value = "setPrivacyOneFile/{uploadFileId}/{privacyValue}", method = RequestMethod.GET)
	public @ResponseBody GenericSetPrivacyResponseJson setPrivacyOneFile(
			@PathVariable("uploadFileId") Integer uploadFileId,
			@PathVariable("privacyValue") Integer privacyValue,
			HttpSession session,
			HttpServletRequest request) {

		return getArchiveService().setUploadFilePrivacy(uploadFileId,
				privacyValue);
	}

	// method to set privacy to all files belongs to an upload
	// http://localhost:8181/Mia/json/archive/setPrivacyUpload/6/1
	@RequestMapping(value = "setPrivacyUpload/{uploadId}/{privacyValue}", method = RequestMethod.GET)
	public @ResponseBody GenericSetPrivacyResponseJson setPrivacyUpload(
			@PathVariable("uploadId") Integer uploadId,
			@PathVariable("privacyValue") Integer privacyValue,
			HttpSession session,
			HttpServletRequest request) {


		return getArchiveService().setUploadPrivacy(uploadId, privacyValue);

	}

	// method to set shared image
	// http://localhost:8181/Mia/json/archive/shareWithUsers/{uploadId}
	@RequestMapping(value = "shareWithUsers/{uploadId}/{fileId}", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson shareWithUsers(
			@RequestBody List<String> accounts,
			@PathVariable("fileId") Integer fileId,
			@PathVariable("uploadId") Integer uploadId, HttpSession session,
			HttpServletRequest request) {
		GenericResponseJson resp = new GenericResponseJson();
		if (uploadId == null) {
			resp.setMessage("UploadId may be null.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		if (fileId == null) {
			resp.setMessage("FileId may be null.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		if (accounts == null) {
			resp.setMessage("Accounts may be null.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		try {
			getUploadService().SaveSharedUsers(accounts, fileId, uploadId, request);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Added shared Users finished. Call getUploadStatus service for complete report");
		} catch (Exception e) {
			logger.error(e.getMessage());
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("Error while saving data");
		}
		return resp;
	}

	// method to set shared image
	// http://localhost:8181/Mia/json/archive/shareWithUsers/{uploadId}
	@RequestMapping(value = "shareWithUsersByArchivial/{uploadId}", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson shareWithUsersByArchivial(
			@RequestBody List<String> accounts,
			@PathVariable("uploadId") Integer uploadId, HttpSession session,
			HttpServletRequest request) {
		GenericResponseJson resp = new GenericResponseJson();
		if (uploadId == null) {
			resp.setMessage("UploadId may be null.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		if (accounts == null) {
			resp.setMessage("Accounts may be null.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		try {
			getUploadService().SaveSharedUsers(accounts, null, uploadId,request);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Added shared Users finished. Call getUploadStatus service for complete report");
		} catch (Exception e) {
			logger.error(e.getMessage());
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("Error while saving data");
		}
		return resp;
	}

	// method to set shared image
	// http://localhost:8181/Mia/json/archive/getSharedImagesByArchivial/{uploadId}
	@RequestMapping(value = "getSharedImagesByArchivial/{uploadId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<Integer>> getsharedImagesByArchivial(
			@PathVariable("uploadId") Integer uploadId, HttpSession session) {
		GenericResponseDataJson<List<Integer>> resp = new GenericResponseDataJson<List<Integer>>();
		if (uploadId == null) {
			resp.setMessage("UploadId may be null.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		try {
			List<Integer> imageIds = getUploadService().findSharedImages(
					uploadId);
			resp.setData(imageIds);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Find " + imageIds.size() + " images in DB.");
		} catch (Exception e) {
			logger.error(e.getMessage());
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("Archivial NOT replaced in DB successfully.");
		}
		return resp;
	}

	// method to set shared image
	// http://localhost:8181/Mia/json/archive/shareWithUsersByDocument/{docId}
	@RequestMapping(value = "shareWithUsersByDocument/{docId}/{replaceSharedUsers}", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson shareWithUsersByDocument(
			@RequestBody List<String> accounts,
			@PathVariable("replaceSharedUsers") Boolean replaceSharedUsers,
			@PathVariable("docId") Integer docId, HttpSession session,
			HttpServletRequest request) {
		GenericResponseJson resp = new GenericResponseJson();
		if (docId == null) {
			resp.setMessage("DocumentId may be null.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		if (accounts == null) {
			resp.setMessage("Accounts may be null.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		try {
			getUploadService().SaveSharedUsersWithDocumentId(accounts, docId,replaceSharedUsers, request);
			resp.setStatus(StatusType.ok.toString());
			resp.setStatus("Added shared Users finished. Call getUploadStatus service for complete report");
		} catch (Exception e) {
			logger.error(e.getMessage());
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("Error while saving data");
		}
		return resp;
	}


	// http://localhost:8181/Mia/json/archive/getSharedUsers/{uploadId}
	@RequestMapping(value = "getSharedUsers/{uploadId}/{fileId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<AdminUserJson>> getSharedusers(
			@PathVariable("uploadId") Integer uploadId,
			@PathVariable("fileId") Integer fileId, HttpSession session) {
		GenericResponseDataJson<List<AdminUserJson>> resp = new GenericResponseDataJson<List<AdminUserJson>>();
		if (uploadId == null) {
			resp.setMessage("UploadId may be null.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		if (fileId == null) {
			resp.setMessage("FileId may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		try {
			List<User> userList = getUploadService().findSharedUsers(uploadId,
					fileId);
			List<AdminUserJson> userJsonList = new ArrayList<AdminUserJson>();
			if (userList != null && userList.size() != 0) {

				for (User user : userList) {
					AdminUserJson userJ = new AdminUserJson();
					userJ.toJson(user);
					userJsonList.add(userJ);
				}
			}
			resp.setData(userJsonList);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Find " + userJsonList.size() + " accounts in DB.");
		} catch (Exception e) {
			logger.error(e.getMessage());
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("Users NOT replaced in DB successfully.");
		}
		return resp;
	}

	// http://localhost:8181/Mia/json/archive/getSharedUsersByDocumentId/{docId}
	@RequestMapping(value = "getSharedUsersByDocumentId/{docId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<AdminUserJson>> getSharedUsersByDocumentId(
			@PathVariable("docId") Integer docId, HttpSession session) {
		GenericResponseDataJson<List<AdminUserJson>> resp = new GenericResponseDataJson<List<AdminUserJson>>();
		if (docId == null) {
			resp.setMessage("UploadId may be null.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		try {
			List<User> userList = getUploadService()
					.findSharedUsersByDocumentId(docId);
			List<AdminUserJson> userJsonList = new ArrayList<AdminUserJson>();
			if (userList != null && userList.size() != 0) {

				for (User user : userList) {
					AdminUserJson userJ = new AdminUserJson();
					userJ.toJson(user);
					userJsonList.add(userJ);
				}
			}
			resp.setData(userJsonList);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Find " + userJsonList.size() + " accounts in DB.");
		} catch (Exception e) {
			logger.error(e.getMessage());
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("Users NOT replaced in DB successfully.");
		}
		return resp;
	}

	// http://localhost:8181/Mia/json/archive/getSharedUsers/{uploadId}
	@RequestMapping(value = "getSharedUsersByArchivial/{uploadId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<AdminUserJson>> getSharedUsersByArchivial(
			@PathVariable("uploadId") Integer uploadId, HttpSession session) {
		GenericResponseDataJson<List<AdminUserJson>> resp = new GenericResponseDataJson<List<AdminUserJson>>();
		if (uploadId == null) {
			resp.setMessage("UploadId may be null.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		try {
			List<User> userList = getUploadService().findSharedUsersByUpdateId(
					uploadId);
			List<AdminUserJson> userJsonList = new ArrayList<AdminUserJson>();
			if (userList != null && userList.size() != 0) {
				for (User user : userList) {
					AdminUserJson userJ = new AdminUserJson();
					userJ.toJson(user);
					userJsonList.add(userJ);
				}
			}
			resp.setData(userJsonList);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Find " + userJsonList.size() + " accounts in DB.");
		} catch (Exception e) {
			logger.error(e.getMessage());
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("Users NOT replaced in DB successfully.");
		}
		return resp;
	}

	@RequestMapping(value = "repairarchive/{uploadFileId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseJson repairArchive(
			@PathVariable("uploadFileId") Integer uploadFileId,
			HttpSession session) {

		List<GenericUploadResponseJson> resList = new ArrayList<GenericUploadResponseJson>();
		session.setAttribute("futureUploadResp", null);
		GenericUploadResponseJson resp = getArchiveService().repairArchive(
				uploadFileId);
		resList.add(resp);
		session.setAttribute("futureUploadResp",
				new AsyncResult<List<GenericUploadResponseJson>>(resList));
		return resp;
	}

	// method to add new files to an upload
	// http://localhost:8181/Mia/json/archive/addfilestoAEMain/26
	@RequestMapping(value = "addfilestoAE/{uploadInfoId}", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson addfilestoAE(
			@ModelAttribute("files") FileUploadForm filesForm,
			@PathVariable("uploadInfoId") Integer uploadInfoId,
			HttpSession session,
			HttpServletRequest request) {
		return addfilestoAE(filesForm, uploadInfoId, session, request, true);
	}


	public GenericResponseJson addfilestoAE(FileUploadForm filesForm, Integer uploadInfoId, HttpSession session,
			HttpServletRequest request, boolean showInHistory) {

		GenericResponseJson resp = new GenericResponseJson();
		if (filesForm == null || filesForm.getFiles() == null
				|| filesForm.getFiles().isEmpty()) {
			resp.setMessage("user portrait image may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		UploadBean uploadBean = getUploadService().addMyArchiveFiles(
				filesForm.getFiles(), Integer.valueOf(uploadInfoId), showInHistory);
		Future<List<GenericUploadResponseJson>> asynR = null;

		if (uploadBean != null) {
			List<UploadFileEntity> uploadFiles = uploadBean
					.getUploadFileEntities();

			session.setAttribute("futureUploadResp", null);
			asynR = getUploadService().convertFiles(uploadFiles,
					uploadBean.getRealPath(), false);

			session.setAttribute("futureUploadResp",
					(Future<List<GenericUploadResponseJson>>) asynR);

			// Check if the privacy in the tblUpload is private, and in case
			// change to public.
			// Because the new file added is by default public when added.
			Integer privacyValue = getArchiveService().getUploadInfoPrivacy(
					uploadInfoId);
			if (privacyValue != null && privacyValue.intValue() == 1) {
				getArchiveService().setUploadPrivacyForAddNewFile(uploadInfoId,
						new Integer(0));
			}
		}

		resp.setStatus(StatusType.ok.toString());
		resp.setStatus("Added files to AE finished. Call getUploadStatus service for complete report");

		return resp;
	}

	@RequestMapping(value = "replaceFileInAE/{uploadInfoId}/{uploadFileId}", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<List<GenericUploadResponseJson>> replaceFileInAE(
			@ModelAttribute("files") FileUploadForm filesForm,
			@PathVariable("uploadInfoId") Integer uploadInfoId,
			@PathVariable("uploadFileId") Integer uploadFileId,
			HttpSession session,
			HttpServletRequest request) {

		GenericResponseDataJson<List<GenericUploadResponseJson>> resp = new GenericResponseDataJson<List<GenericUploadResponseJson>>();
		addfilestoAE(filesForm, uploadInfoId, session, request, false);

		Future<List<GenericUploadResponseJson>> asynR = null;
		while (asynR == null) {
			asynR = (Future<List<GenericUploadResponseJson>>) session
					.getAttribute("futureUploadResp");
		}

		MyArchiveBean oldArchJson =  getArchiveService().getArchiveByUploadId(
				uploadInfoId, 6, 1);

		try {
			List<GenericUploadResponseJson> listResponse = asynR.get();

			if (listResponse != null && !listResponse.isEmpty()) {
				resp.setData(listResponse);
				Integer respInt = getUploadService().replaceFiles(uploadFileId,
						Integer.valueOf(listResponse.get(0).getFileId()));
				if (respInt > 0) {
					listResponse.get(0).setFileId(uploadFileId);
					resp.setMessage("File replaced in DB successfully.");
					resp.setStatus(StatusType.ok.toString());
				} else {
					resp.setMessage("File NOT replaced in DB successfully.");
					resp.setStatus(StatusType.ko.toString());
				}

			}

		} catch (Exception ie) {
			logger.error(ie.getMessage());
		}

		return resp;
	}

	// method to move the order of an uploadFile
	// http://localhost:8181/Mia/json/archive/orderFileinAE/163/15
	@RequestMapping(value = "orderFileinAE/{uploadFileId}/{newImageOrder}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseJson orderFileinAE(
			@PathVariable("uploadFileId") Integer uploadFileId,
			@PathVariable("newImageOrder") int newImageOrder,
			HttpServletRequest request) {

		MyArchiveBean oldArchJson =  getArchiveService().getArchiveByUploadFileId(
				uploadFileId, 6, 1);

		return getArchiveService()
				.updateImageOrder(uploadFileId, newImageOrder);

	}

	// method to download a File from his uploadFileId
	// http://localhost:8181/Mia/json/archive/downloadAFile/163
	@RequestMapping(value = "downloadAFile/{uploadFileId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseJson downloadFile(
			@PathVariable("uploadFileId") Integer uploadFileId,
			HttpServletResponse response) {
		UploadFileEntity fileEntity = entityManager.find(UploadFileEntity.class, uploadFileId);
		GenericResponseJson resp = new GenericResponseJson();
		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()).getUsername());
		if(fileEntity == null){
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("Requested file does not exist");
			return resp;
		}
		if(!userService.isAccountAdministrator(user.getAccount())){
			if(!fileEntity.getUploadInfoEntity().getOwner().equals(user.getAccount())) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("You don't have a permission to download this file");
				return resp;
			}
		}
		resp.setStatus(StatusType.ko.toString());
		String[] pathAndFilename = getArchiveService().downloadFile(
				uploadFileId);
		File file = null;
		FileInputStream fileIn = null;
		ServletOutputStream out = null;
		try {
			if (pathAndFilename[0] != null && pathAndFilename[1] != null) {
				response.setContentType("application/octet-stream");
				response.setHeader("Content-Disposition",
						"attachment;filename=" + pathAndFilename[1]);

				file = new File(pathAndFilename[0] + pathAndFilename[1]);
				fileIn = new FileInputStream(file);
				out = response.getOutputStream();

				byte[] outputByte = new byte[(int) file.length()];
				// copy binary content to output stream
				while (fileIn.read(outputByte, 0, (int) file.length()) != -1) {
					out.write(outputByte, 0, (int) file.length());
				}
				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("File Download successfull complete");
			} else {
				resp.setMessage("Path or Filename of UploadFileId not found on DB");
				return resp;
			}
		} catch (IOException e) {
			logger.error("Unable to download the file");
			e.printStackTrace();
			resp.setMessage("Unable to download the file - IOException");
		} finally {
			try {
				if (fileIn != null) {
					fileIn.close();
				}
				if (out != null) {
					out.flush();
					out.close();
				}
			} catch (IOException e) {
				logger.warn("Problem during closing of the streams");
				e.printStackTrace();
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("Problem during closing of the stream for download file");
			}
		}

		return resp;
	}

	// http://localhost:8181/Mia/json/archive/deleteFileInAE/{uploadFileId}
	@RequestMapping(value = "deleteFileInAE/{uploadFileId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, DocumentAEJson>> deleteFileInAE(
			@PathVariable("uploadFileId") Integer uploadFileId,
			HttpSession session,
			HttpServletRequest request) {

		GenericResponseDataJson<HashMap<String, DocumentAEJson>> resp = new GenericResponseDataJson<HashMap<String, DocumentAEJson>>();
		HashMap<String, DocumentAEJson> docHash = new HashMap<String, DocumentAEJson>();

		DocumentAEJson documentAE = getArchiveService()
				.checkDocForDeleteFileInAE(uploadFileId);
		docHash.put("documentAE", documentAE);
		resp.setData(docHash);
		if (documentAE == null) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No Archive with this uploadInfoId or No file Entity inside this Archive found.");
			return resp;
		}

		if (documentAE.getIsOwner() == null) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No Owner found.");
			return resp;
		}
		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()).getUsername());

		if (!getUserService().isAccountAutorizedForDelete(user.getAccount()) && "NO".equalsIgnoreCase(documentAE.getIsOwner())) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("You are not owner of these files and you are not allowed to delete them.");
			return resp;
		}

		if (documentAE.getDocumentIds() != null
				&& !documentAE.getDocumentIds().isEmpty()) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("There are "
                    + documentAE.getDocumentIds().size()
                    + " document(s) attached to this image(s). You must delete these documents first.");
			return resp;
		}


		GenericResponseJson genericResp = getArchiveService().deleteFileInAE(
				uploadFileId);

		resp.setStatus(genericResp.getStatus());
		resp.setMessage(genericResp.getMessage());

		return resp;

	}

	@RequestMapping(value = "deleteAE/{uploadInfoId}", method = RequestMethod.DELETE)
	public @ResponseBody GenericResponseDataJson<HashMap<String, DocumentAEJson>> deleteAE(
			@PathVariable("uploadInfoId") Integer uploadInfoId,
			HttpSession session,
			HttpServletRequest request) {

		GenericResponseDataJson<HashMap<String, DocumentAEJson>> resp = new GenericResponseDataJson<HashMap<String, DocumentAEJson>>();
		HashMap<String, DocumentAEJson> docHash = new HashMap<String, DocumentAEJson>();

		DocumentAEJson documentAE = getArchiveService().checkDocForDeleteAE(
				uploadInfoId);
		docHash.put("documentAE", documentAE);
		resp.setData(docHash);
		if (documentAE == null) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("Archive Entity with id = "+uploadInfoId+" does not exist");
			return resp;
		}

		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()).getUsername());

		if (!getUserService().isAccountAutorizedForDelete(user.getAccount())) {
			// User is not admin or on site fellow: Control If he is owner of
			// the document
			if (documentAE.getIsOwner() == null
					|| "NO".equalsIgnoreCase(documentAE.getIsOwner())) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("You are not Admin or Onsite_Fellows and No Owner found. you are not allowed to delete them");
				return resp;
			}

		}

		if (documentAE.getDocumentIds() != null
				&& !documentAE.getDocumentIds().isEmpty()) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("You are not allowed to perform this action there are "
					+ documentAE.getDocumentIds().size()
					+ " document entities attached this image/s that should be deleted first.");

			return resp;
		}

		GenericResponseJson genericResp = getArchiveService().deleteAE(
				uploadInfoId);

		resp.setStatus(genericResp.getStatus());
		resp.setMessage(genericResp.getMessage());

		return resp;

	}

	// method to edit metadata of an Upload
	// http://localhost:8181/Mia/json/archive/updateMetaDataAE/{uploadId}
	@RequestMapping(value = "updateMetaDataAE/{uploadInfoId}", method = RequestMethod.PUT)
	public @ResponseBody GenericResponseJson editAEmetadata(
			@PathVariable("uploadInfoId") Integer uploadInfoId,
			@RequestBody final MyArchiveBean archive,
			HttpServletRequest request,
			HttpServletResponse httpServletResponse) {

		GenericResponseJson resp = new GenericResponseJson();

		// valorizzato oggetti per UploadBean con i valori da FE
		RepositoryEntity repo = new RepositoryEntity();
		CollectionEntity coll = new CollectionEntity();
		SeriesEntity ser = new SeriesEntity();
		Volume vol;

		if(archive.getVolume() != null){
			if(archive.getVolume().getVolumeId() != null && archive.getVolume().getVolumeId().length() > 0){
				vol = archiveService.findVolumeById(Integer.valueOf(archive.getVolume().getVolumeId()));
				if(vol == null){
					vol = new Volume();
				} else {
					if(vol.getLogicalDelete() != null && vol.getLogicalDelete()){
						vol = new Volume();
					}
				}
			} else {
				vol = new Volume();
			}
		} else {
			vol = new Volume();
		}

		InsertEntity ins;

		if(archive.getInsert() != null){
			if(archive.getInsert().getInsertId() != null && archive.getInsert().getInsertId().length() > 0){
				ins = archiveService.findInsertById(Integer.valueOf(archive.getInsert().getInsertId()));
				if(ins == null){
					ins = new InsertEntity();
				}
			} else if(archive.getInsert().getInsertName() != null
                    && archive.getInsert().getInsertName().length() > 0
                    && archive.getVolume() != null
                    && archive.getVolume().getVolumeId() != null) {
				List<InsertEntity> inserts = archiveService.findInsertByName(archive.getInsert().getInsertName(), Integer.valueOf(archive.getVolume().getVolumeId()));
				if(inserts == null || inserts.isEmpty()){
				    ins = new InsertEntity();
                } else {
				    ins = inserts.get(0);
                }
			} else {
			    ins = new InsertEntity();
			}
		} else {
			ins = new InsertEntity();
		}

		if(ins.getInsertId() == null && archive.getInsert().getInsertName() == null && vol.getInsertEntities() != null && U.any(vol.getInsertEntities(), new Predicate<InsertEntity>() {
			@Override
			public boolean test(InsertEntity insertEntity) {
				if(insertEntity.getLogicalDelete() == null) return true;
				return !insertEntity.getLogicalDelete();
			}
		})){
			return new GenericResponseJson("error", "Cannot move AE to the volume with id = " +vol.getSummaryId()+ " - volume has inserts");
		}
		GoogleLocation gloc = new GoogleLocation();

		if (archive.getRepository() != null) {
			if (archive.getRepository().getRepositoryId() != null
					&& !archive.getRepository().getRepositoryId().isEmpty()) {
				if(!validateId(archive.getRepository().getRepositoryId())){
					httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					return null;
				}
				repo.setRepositoryId(Integer.valueOf(archive.getRepository()
						.getRepositoryId()));
			}
			if (archive.getRepository().getRepositoryName() != null
					&& !archive.getRepository().getRepositoryName().isEmpty()) {
				repo.setRepositoryName(archive.getRepository()
						.getRepositoryName());
			}
			if (archive.getRepository().getRepositoryAbbreviation() != null
					&& !archive.getRepository().getRepositoryAbbreviation()
							.isEmpty()) {
				repo.setRepositoryAbbreviation(archive.getRepository()
						.getRepositoryAbbreviation());
			}
			if (archive.getRepository().getRepositoryDescription() != null
					&& !archive.getRepository().getRepositoryDescription()
							.isEmpty()) {
				repo.setRepositoryDescription(archive.getRepository()
						.getRepositoryDescription());
			}
			if (archive.getRepository().getRepositoryLocation() != null
					&& !archive.getRepository().getRepositoryLocation()
							.isEmpty()) {
				repo.setLocation(archive.getRepository()
						.getRepositoryLocation());
				gloc.setgPlaceId(archive.getRepository()
						.getRepositoryLocation());
			}
			if (archive.getRepository().getRepositoryCountry() != null
					&& !archive.getRepository().getRepositoryCountry()
							.isEmpty()) {
				gloc.setCountry(archive.getRepository().getRepositoryCountry());
			}
			if (archive.getRepository().getRepositoryCity() != null
					&& !archive.getRepository().getRepositoryCity().isEmpty()) {
				gloc.setCity(archive.getRepository().getRepositoryCity());
			}

		}

		if (archive.getCollection() != null) {
			if (archive.getCollection().getCollectionId() != null
					&& !archive.getCollection().getCollectionId().isEmpty()) {
				if(!validateId(archive.getCollection().getCollectionId())){
					httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					return null;
				}
				CollectionEntity tempColl = archiveService.findCollectionById(Integer.parseInt(archive.getCollection().getCollectionId()));
				if(tempColl == null){
					resp.setStatus("error");
					resp.setMessage("Collection with id = "+archive.getCollection().getCollectionId()+" does not exist or it is deleted");
					return resp;
				}
				coll.setCollectionId(Integer.valueOf(archive.getCollection()
						.getCollectionId()));
			}
			if (archive.getCollection().getCollectionName() != null
					&& !archive.getCollection().getCollectionName().isEmpty()) {
				coll.setCollectionName(archive.getCollection()
						.getCollectionName());
			}
			if (archive.getCollection().getCollectionAbbreviation() != null
					&& !archive.getCollection().getCollectionAbbreviation()
							.isEmpty()) {
				coll.setCollectionAbbreviation(archive.getCollection()
						.getCollectionAbbreviation());
			}
			if (archive.getCollection().getCollectionDescription() != null
					&& !archive.getCollection().getCollectionDescription()
							.isEmpty()) {
				coll.setCollectionDescription(archive.getCollection()
						.getCollectionDescription());
			}

		}

//		if (archive.getSeries() != null) {
//			if (archive.getSeries().getSeriesId() != null
//					&& !archive.getSeries().getSeriesId().isEmpty()) {
//				ser.setSeriesId(Integer.valueOf(archive.getSeries()
//						.getSeriesId()));
//			}
//			if (archive.getSeries().getSeriesName() != null
//					&& !archive.getSeries().getSeriesName().isEmpty()) {
//				ser.setTitle(archive.getSeries().getSeriesName());
//			}
//			if (archive.getSeries().getSeriesSubtitle() != null
//					&& !archive.getSeries().getSeriesSubtitle().isEmpty()) {
//				ser.setSubtitle1(archive.getSeries().getSeriesSubtitle());
//			}
//
//		}

		if (archive.getVolume() != null) {
			if (archive.getVolume().getVolumeId() != null
					&& !archive.getVolume().getVolumeId().isEmpty()) {
				if(!validateId(archive.getVolume().getVolumeId())){
					httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					return null;
				}
				Volume tempVol = archiveService.findVolumeById(Integer.parseInt(archive.getVolume().getVolumeId()));
				if(tempVol == null || tempVol.getLogicalDelete()){
					resp.setStatus("error");
					resp.setMessage("Volume with id = "+archive.getVolume().getVolumeId()+" does not exist or it is deleted");
					return resp;
				}
				vol.setSummaryId(Integer.valueOf(archive.getVolume()
						.getVolumeId()));
			}
			if (archive.getVolume().getVolumeName() != null
					&& !archive.getVolume().getVolumeName().isEmpty()
					&& (archive.getVolume().getVolumeId() == null || archive.getVolume().getVolumeId().isEmpty())) {
				vol.setVolume(archive.getVolume().getVolumeName());
			}
		}
		if (archive.getInsert() != null) {

			if (archive.getInsert().getInsertId() != null
					&& !archive.getInsert().getInsertId().isEmpty()) {
				if(!validateId(archive.getInsert().getInsertId())){
					httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
					return null;
				}
				InsertEntity tempIns = archiveService.findInsertById(Integer.parseInt(archive.getInsert().getInsertId()));
				if(tempIns == null){
					resp.setStatus("error");
					resp.setMessage("Insert with id = "+archive.getInsert().getInsertId()+" does not exist or it is deleted");
					return resp;
				}
				ins.setInsertId(Integer.valueOf(archive.getInsert()
						.getInsertId()));
			}
			if (archive.getInsert().getInsertName() != null
					&& !archive.getInsert().getInsertName().isEmpty()
					&& (archive.getInsert().getInsertId() == null || archive.getInsert().getInsertId().isEmpty())) {
				ins.setInsertName(archive.getInsert().getInsertName());
			}
			if(archive.getInsert().getInsertId().equals("") && archive.getInsert().getInsertName().equals("")){
				archive.setInsert(null);
			}

		}

		// Creazione UploadBean
		UploadBean newUploadBean = new UploadBean(null, gloc, repo, coll, ser,
				vol, ins, null, archive.getAeDescription());
		// aeName valorization
		String aeName = archive.getAeName();
		// Chiama servizio editMetadata passando newUploadBean
		return getArchiveService().editAEMetadata(
				Integer.valueOf(uploadInfoId), newUploadBean, aeName);

	}

	@RequestMapping(value = "rotateFilesInAE", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson rotateFilesInAE(
			@RequestBody RotateImageJson rotateImageJson,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();
		if (rotateImageJson == null
				|| rotateImageJson.getUploadFileIds() == null
				|| rotateImageJson.getUploadFileIds().isEmpty()) {
			resp.setMessage("uploadFiles may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		if (rotateImageJson.getRotateType() == null
				|| rotateImageJson.getRotateType().isEmpty()) {
			resp.setMessage("rotate type may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		} else if (rotateImageJson.getRotateType().equalsIgnoreCase(
				RotateType.FLIP.toString())) {
			return getUploadService().rotateImages(
					rotateImageJson.getUploadFileIds(), RotateType.FLIP);

		} else if (rotateImageJson.getRotateType().equalsIgnoreCase(
				RotateType.RIGHT.toString())) {
			return getUploadService().rotateImages(
					rotateImageJson.getUploadFileIds(), RotateType.RIGHT);

		} else if (rotateImageJson.getRotateType().equalsIgnoreCase(
				RotateType.LEFT.toString())) {
			return getUploadService().rotateImages(
					rotateImageJson.getUploadFileIds(), RotateType.LEFT);

		} else {
			resp.setMessage("rotate type is wrong.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

	}

	@RequestMapping(value = "unDeleteAE/{uploadId}", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson unDeleteAE(
			@PathVariable("uploadId") Integer uploadId, HttpSession session,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();
		if (uploadId == null) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("uploadId is null.");
			return resp;
		}

		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()).getUsername());

		if (!getUserService().isAccountAutorizedForDelete(user.getAccount())) {
			// User is not admin or on site fellow:
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("You are not Admin or Onsite_Fellows . you are not allowed to undelete AE");
			return resp;

		}

		GenericResponseJson genericResp = getArchiveService()
				.unDeleteAE(uploadId);

		resp.setStatus(genericResp.getStatus());
		resp.setMessage(genericResp.getMessage());

		return resp;

	}

	@RequestMapping(value = "findPageForFile", method = RequestMethod.GET)
	public @ResponseBody GenericResponseJson unDeleteAE(
			@RequestParam("uploadId") Integer uploadId,
			@RequestParam("fileId") Integer fileId,
			@RequestParam("perPage") Integer perPage,
			HttpServletResponse httpServletResponse) {
		GenericResponseDataJson<HashMap<String, Object>> resp = new GenericResponseDataJson<HashMap<String, Object>>();
		HashMap<String, Object> data = archiveService.findPageForFile(uploadId, fileId, perPage);
		if(uploadId == null || fileId == null || perPage == null){
			httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return null;
		}
		if(perPage <= 0){
			resp.setStatus("error");
			resp.setMessage("Wrong amount per page specified");
			return resp;
		}
		if(data == null){
			resp.setStatus("error");
			resp.setMessage("Cannot find a proper page for file in this AE");
			return resp;
		}
		resp.setData(data);
		resp.setStatus("success");
		resp.setMessage("Found image with id = `"+uploadId+"` on page = " + resp.getData().get("page"));
		return resp;
	}
	

	/**
	 * Find last uploadedFile for the specified upload
	 * @param uploadId upload id
	 * @param httpServletResponse http response
	 * @return the last uploaded file
	 */
	@RequestMapping(value = "getLastUploadedFile/{uploadId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseJson getLastUploadedFile(
			@PathVariable("uploadId") Integer uploadId,
			HttpServletResponse httpServletResponse) {
		
		if (uploadId == null) {
			httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return null;
		}
		
		GenericResponseDataJson<HashMap<String, UploadFileJson>> response = 
				new GenericResponseDataJson<HashMap<String, UploadFileJson>>();
		HashMap<String, UploadFileJson> data = new HashMap<String, UploadFileJson>();
		
		UploadFileEntity uploadFileEntity = 
				getArchiveService().getLastUploadFileEntity(uploadId);
		
		if (uploadFileEntity == null) {
			response.setData(null);
			response.setStatus(StatusType.ko.toString());
			response.setMessage("Last uploaded file not found");
		}
		else {
			UploadFileJson uploadFileJson = new UploadFileJson();
			uploadFileJson.setUploadFileId(uploadFileEntity.getUploadFileId());    
			uploadFileJson.setImageOrder(uploadFileEntity.getImageOrder());      
			uploadFileJson.setFileName(uploadFileEntity.getFilename());
			
			data.put("lastUpload", uploadFileJson);
			
			response.setData(data);
			response.setStatus(StatusType.ok.toString());
			response.setMessage(String.format("Last uploadedFile: %s", 
					uploadFileEntity.getFilename()));
		}
		
		return response;
	}

	public MyArchiveService getArchiveService() {
		return archiveService;
	}

	public void setArchiveService(MyArchiveService archiveService) {
		this.archiveService = archiveService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public UploadService getUploadService() {
		return uploadService;
	}

	public void setUploadService(UploadService uploadService) {
		this.uploadService = uploadService;
	}

	public HistoryLogService getHistoryLogService() {
		return historyLogService;
	}
}