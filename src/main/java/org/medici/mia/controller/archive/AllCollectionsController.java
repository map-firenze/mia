package org.medici.mia.controller.archive;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.allcollections.AllCollectionsFolioJson;
import org.medici.mia.common.json.allcollections.AllCollectionsThumbsFileJson;
import org.medici.mia.common.json.allcollections.AllCollectionsVolumeJson;
import org.medici.mia.service.allcollections.AllCollectionsService;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.myarchive.MyArchiveService;
import org.medici.mia.service.upload.UploadService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Controller
@RequestMapping("/browseAllCollections")
public class AllCollectionsController {

	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private AllCollectionsService allCollectionsService;

	@Autowired
	private UploadService uploadService;

	@Autowired
	private UserService userService;

    @Autowired
    private MyArchiveService archiveService;
    
    @Autowired
    private MiaDocumentService miaDocumentService;

	// http://localhost:8181/Mia/json/browseAllCollections/findVolumeWithNumberedImages/1316/3/1
	@RequestMapping(value = "findVolumeWithImages/{volumeId}", method = RequestMethod.GET)
	public @ResponseBody AllCollectionsVolumeJson findVolumeWithNumberedImages(
			@PathVariable("volumeId") Integer volumeId,
			@RequestParam("numbered") Boolean numbered,
			@RequestParam(value = "folioNumber", required = false) String folioNumber,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "perPage", required = false) Integer perPage) {
		if (!numbered) {
			return getAllCollectionsService().getAllCollectionsNotNumberedVolume(volumeId, page, perPage);
		}
		AllCollectionsVolumeJson resp;
		if (folioNumber != null) {
			resp = allCollectionsService.getAllCollectionsVolume(volumeId, 0, 0);

			Integer imagePosition = 0;
			List<AllCollectionsThumbsFileJson> thumbFiles = resp.getThumbsFiles();
			for (AllCollectionsThumbsFileJson thumb : thumbFiles) {
				boolean found = false;
				for (AllCollectionsFolioJson folio : thumb.getFolios()) {
					if (!folio.getNoNumb() && StringUtils.equals(folio.getFolioNumber(), folioNumber)) {
						found = true;
					}
				}
				if (found) {
					break;
				}
				imagePosition++;
			}

			if (imagePosition >= thumbFiles.size()) {
				resp = new AllCollectionsVolumeJson();
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("an error occurred");
			} else if (perPage != null && perPage > 0) {
				Integer currentPage = imagePosition / perPage + 1;
				Integer limit = currentPage * perPage;
				Integer offset = limit - perPage;
				if (offset > thumbFiles.size()) {
					resp.setThumbsFiles(thumbFiles.subList(0, 0));
				} else if (limit > thumbFiles.size()) {
					resp.setThumbsFiles(thumbFiles.subList(offset, thumbFiles.size()));
				} else {
					resp.setThumbsFiles(thumbFiles.subList(offset, limit));
				}
				resp.setCurrentPage(currentPage);
			}
		} else {
			resp = getAllCollectionsService().getAllCollectionsVolume(volumeId, page, perPage);
		}
		if (resp.getThumbsFiles() != null) {
			for (AllCollectionsThumbsFileJson thumb : resp.getThumbsFiles()) {
				thumb.setDocumentCount(miaDocumentService.countDocumentsByUploadFileId(thumb.getUploadFileId()));
			}
		}
		return resp;
	}

	// http://localhost:8181/Mia/json/browseAllCollections/findInsertWithNumberedImages/1316/?numbered={true|false}[&page=1&perPage=2]
	@RequestMapping(value = "findInsertWithImages/{insertId}", method = RequestMethod.GET)
	public @ResponseBody AllCollectionsVolumeJson findInsertWithNumberedImages(
			@PathVariable("insertId") Integer insertId,
			@RequestParam("numbered") Boolean numbered,
			@RequestParam(value = "folioNumber", required = false) String folioNumber,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "perPage", required = false) Integer perPage) {
		if (!numbered) {
			return getAllCollectionsService().getAllCollectionsNotNumberedInsert(insertId, page, perPage);
		}
		AllCollectionsVolumeJson resp;
		if (folioNumber != null) {
			resp = allCollectionsService.getAllCollectionsInsert(insertId, page, perPage);

			Integer imagePosition = 0;
			List<AllCollectionsThumbsFileJson> thumbFiles = resp.getThumbsFiles();
			for (AllCollectionsThumbsFileJson thumb : thumbFiles) {
				boolean found = false;
				for (AllCollectionsFolioJson folio : thumb.getFolios()) {
					if (!folio.getNoNumb() && StringUtils.equals(folio.getFolioNumber(), folioNumber)) {
						found = true;
					}
				}
				if (found) {
					break;
				}
				imagePosition++;
			}

			if (imagePosition >= thumbFiles.size()) {
				resp = new AllCollectionsVolumeJson();
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("an error occurred");
			} else if (perPage != null && perPage > 0) {
				Integer currentPage = imagePosition / perPage + 1;
				Integer limit = currentPage * perPage;
				Integer offset = limit - perPage;
				if (offset > thumbFiles.size()) {
					resp.setThumbsFiles(thumbFiles.subList(0, 0));
				} else if (limit > thumbFiles.size()) {
					resp.setThumbsFiles(thumbFiles.subList(offset, thumbFiles.size()));
				} else {
					resp.setThumbsFiles(thumbFiles.subList(offset, limit));
				}
				resp.setCurrentPage(currentPage);
			}
		} else {
			resp = getAllCollectionsService().getAllCollectionsInsert(insertId, page, perPage);
		}
		if (resp.getThumbsFiles() != null) {
			for (AllCollectionsThumbsFileJson thumb : resp.getThumbsFiles()) {
				thumb.setDocumentCount(miaDocumentService.countDocumentsByUploadFileId(thumb.getUploadFileId()));
			}
		}
		return resp;
	}


	// http://localhost:8181/Mia/json/browseAllCollections/findInsertAEsWithNotNumberedImages/1316/[?page=1&perPage=2]
	@RequestMapping(value = "findVolumeAEsWithNotYetNumberedFolios/{insertId}", method = RequestMethod.GET)
	public @ResponseBody HashMap findVolumeAEsWithNotNumberedImages(
			@PathVariable("insertId") Integer volumeId,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "perPage", required = false) Integer perPage,
            @RequestParam(value = "thumbsPerPage", required = false) Integer thumbsPerPage) {
		HashMap<String, List> response = new HashMap<String, List>();
        thumbsPerPage = thumbsPerPage == null ? 6 : thumbsPerPage;
		if(page == null || perPage == null) {
			page = page == null ? 1 : page;
			perPage = perPage == null ? 10000 : perPage;
		}
		return getArchiveService().getVolumeAEsWithNotNumberedImages(volumeId, perPage, thumbsPerPage, page);
	}

	@RequestMapping(value = "findInsertAEsWithNotYetNumberedFolios/{volumeId}", method = RequestMethod.GET)
	public @ResponseBody HashMap findInsertAEsWithNotNumberedImages(
			@PathVariable("volumeId") Integer insertId,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "perPage", required = false) Integer perPage,
            @RequestParam(value = "thumbsPerPage", required = false) Integer thumbsPerPage) {
		HashMap<String, List> response = new HashMap<String, List>();
		thumbsPerPage = thumbsPerPage == null ? 6 : thumbsPerPage;
		if(page == null || perPage == null) {
			page = page == null ? 1 : page;
			perPage = perPage == null ? 10000 : perPage;
		}
		return getArchiveService().getInsertAEsWithNotNumberedImages(insertId, perPage, thumbsPerPage, page);
	}

	@RequestMapping(value = "findFoliosInInsert/{insertId}", method = RequestMethod.GET)
	public @ResponseBody AllCollectionsVolumeJson findFoliosInInsert(
			@PathVariable("insertId") Integer insertId,
			@RequestParam(value = "q", required = false) String q,
			@RequestParam(value = "id", required = false) Integer id,
			@RequestParam(value = "exact", required = false) Boolean exact,
			HttpServletResponse response
	){
		exact = exact == null ? false : exact;
		if(q == null && id == null || q != null && id != null){
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return null;
		}
		return getAllCollectionsService().findFoliosInInsert(insertId, q, id, exact);
	}

	@RequestMapping(value = "findFoliosInVolume/{volumeId}", method = RequestMethod.GET)
	public @ResponseBody AllCollectionsVolumeJson findFoliosInVolume(
			@PathVariable("volumeId") Integer volumeId,
			@RequestParam(value = "q", required = false) String q,
			@RequestParam(value = "id", required = false) Integer id,
			@RequestParam(value = "exact", required = false) Boolean exact,
			HttpServletResponse response
	){
		exact = exact == null ? false : exact;
		if(q == null && id == null || q != null && id != null){
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return null;
		}
		return getAllCollectionsService().findFoliosInVolume(volumeId, q, id, exact);
	}

	public AllCollectionsService getAllCollectionsService() {
		return allCollectionsService;
	}

    public MyArchiveService getArchiveService() {
        return archiveService;
    }

	public void setAllCollectionsService(
			AllCollectionsService allCollectionsService) {
		this.allCollectionsService = allCollectionsService;
	}

}