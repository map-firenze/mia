package org.medici.mia.controller.geographical;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.PlaceJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.TopicPlaceRelDocJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.geographical.*;
import org.medici.mia.domain.CaseStudyItem;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.service.casestudy.CaseStudyService;
import org.medici.mia.service.geografical.GeographicalPlaceService;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.people.BiographicalPeopleService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Controller
@RequestMapping("/geographical")
public class GeographicalPlaceController {

	@Autowired
	private GeographicalPlaceService geographicalPlaceService;

	@Autowired
	private MiaDocumentService miaDocumentService;

	@Autowired
	private BiographicalPeopleService biographicalPeopleService;

	@Autowired
	private UserService userService;

	@Autowired
	private HistoryLogService historyLogService;

	@Autowired
	private CaseStudyService caseStudyService;

	@RequestMapping(value = "modifyGeographicalPlace", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyGeographicalPlace(
			@RequestBody GeographicalPlaceJson modifyJson,
			HttpServletRequest request) {
		GenericResponseJson resp = new GenericResponseJson();
		if (modifyJson != null && modifyJson.getPlaceId() != null) {
			resp = getGeographicalPlaceService().modifyGeographicalPlace(
					modifyJson);
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Data not valid to modify people");
		}

		return resp;
	}

	// http://localhost:8181/Mia/json/geographical/findGeographicalPlace/116
	@RequestMapping(value = "findGeographicalPlace/{placeId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<GeographicalPlaceJson> findGeographicalPlace(
			@PathVariable("placeId") Integer placeId) {
		GenericResponseDataJson<GeographicalPlaceJson> resp = new GenericResponseDataJson<GeographicalPlaceJson>();
		GeographicalPlaceJson geoPlace = getGeographicalPlaceService()
				.findGeographicalPlace(placeId, false);
		if (geoPlace != null && geoPlace.getPlaceId() != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got Geographical place with id: " + placeId);
			resp.setData(geoPlace);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No Geografical place found with id: " + placeId);
			return resp;
		}

	}

	// http://localhost:8181/Mia/json/geographical/findNotDeletedGeographicalPlace/116
	@RequestMapping(value = "findNotDeletedGeographicalPlace/{placeId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<GeographicalPlaceJson> findNotDeletedGeographicalPlace(
			@PathVariable("placeId") Integer placeId) {
		GenericResponseDataJson<GeographicalPlaceJson> resp = new GenericResponseDataJson<GeographicalPlaceJson>();
		GeographicalPlaceJson geoPlace = getGeographicalPlaceService()
				.findGeographicalPlace(placeId, true);
		if (geoPlace != null && geoPlace.getPlaceId() != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got Geographical place with id: " + placeId);
			resp.setData(geoPlace);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No Not Deleted Geografical place found with id: "
					+ placeId);
			return resp;
		}

	}

	// http://localhost:8181/Mia/json/geographical/findGeographicalPlace/Lig
	@RequestMapping(value = "findPlaceNameVariants/{placeName}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<GeographicalPlaceJson>> findPlaceNameVariants(
			@PathVariable("placeName") String placeName) {
		GenericResponseDataJson<List<GeographicalPlaceJson>> resp = new GenericResponseDataJson<List<GeographicalPlaceJson>>();
		List<GeographicalPlaceJson> geoPlaces = getGeographicalPlaceService()
				.findPlaceNameVariants(placeName);
		if (geoPlaces != null && !geoPlaces.isEmpty()) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + geoPlaces.size() + " Geographical places.");
			resp.setData(geoPlaces);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No Geografical places found with placeName: "
					+ placeName);
			return resp;
		}

	}

	// http://localhost:8181/Mia/json/geographical/findPlaceNameVariantsById/Lig
	@RequestMapping(value = "findPlaceNameVariantsById/{placeId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<FindGeoPlaceNameVariantJson> findPlaceNameVariantsById(
			@PathVariable("placeId") Integer placeId) {
		GenericResponseDataJson<FindGeoPlaceNameVariantJson> resp = new GenericResponseDataJson<FindGeoPlaceNameVariantJson>();
		FindGeoPlaceNameVariantJson geoPlaces = getGeographicalPlaceService()
				.findPlaceNameVariantsById(placeId);
		if (geoPlaces != null && geoPlaces.getPlaceNameVariants() != null
				&& !geoPlaces.getPlaceNameVariants().isEmpty()) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + geoPlaces.getPlaceNameVariants().size()
					+ " Geographical places.");
			resp.setData(geoPlaces);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No Geografical places found with placeId: "
					+ placeId);
			return resp;
		}

	}

	@RequestMapping(value = "addPlaceNameVariant", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson addPlaceNameVariant(
			@RequestBody ModifyGeoPlaceNameVariantJson placeToModify,
			HttpServletRequest request) {
		GenericResponseJson resp = new GenericResponseJson();
		if (placeToModify != null && placeToModify.getPlaceAllId() != null
				&& placeToModify.getNewPlaceNameVariant() != null) {
			resp = getGeographicalPlaceService().addGeoPlaceNameVariant(
					placeToModify);
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Data not valid to add place name variant.");
		}
		return resp;
	}

	@RequestMapping(value = "modifyPlaceNameVariant", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyPlaceNameVariant(
			@RequestBody ModifyGeoPlaceNameVariantJson placeToModify,
			HttpServletRequest request) {
		GenericResponseJson resp = new GenericResponseJson();
		if (placeToModify != null
				&& placeToModify.getNewPlaceNameVariant() != null
				&& placeToModify.getNewPlaceNameVariant().getPlaceAllId() != null) {
			resp = getGeographicalPlaceService().modifyGeoPlaceNameVariant(
					placeToModify);
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Data not valid to modify place name variant");
		}
		return resp;
	}

	// http://localhost:8181/Mia/json/geographical/deletePlaceNameVariant/125
	@RequestMapping(value = "deletePlaceNameVariant/{placeId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseJson deletePlaceNameVariant(
			@PathVariable("placeId") Integer placeId,
			HttpServletRequest request) {
		return getGeographicalPlaceService().deletePlaceVariant(placeId);

	}

	// http://localhost:8181/Mia/json/geographical/deletePlace/175546
	@RequestMapping(value = "deletePlace/{placeId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<DeletePlaceJson> deletePlace(
			@PathVariable("placeId") Integer placeId,
			HttpServletRequest request) {

		GenericResponseDataJson<DeletePlaceJson> resp = new GenericResponseDataJson<DeletePlaceJson>();

		if (!getUserService().isAccountAutorizedToDeletePlace()) {

			resp.setStatus(StatusType.w.toString());
			resp.setMessage("Only staff and administrators are authorized to delete place");
			return resp;
		}

		PlaceJson p = getMiaDocumentService().findPrinicipalPlaceByPlaceId(
				placeId);
		FindGeoPlaceNameVariantJson variants = getGeographicalPlaceService().findPlaceNameVariantsById(placeId);
		if (p != null && !p.getId().equals(placeId)) {
			DeletePlaceJson json = new DeletePlaceJson();
			json.setPrincipalPlaceId(p.getId());
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("This is a variant name of a geographical or topographical place and therefore cannot be deleted. Please delete it from the its Principal name record. ");
			resp.setData(json);
			return resp;
		}

		DeletePlaceJson json = new DeletePlaceJson();
		List<String> places = new ArrayList<String>();
		places.add(String.valueOf(placeId));

		List<Integer> docs = getMiaDocumentService().findDocumentIdsByPlaces(
				places);

		List<Integer> bios = getBiographicalPeopleService().findPeopleByPlace(
				placeId);

		boolean isDeleted = true;

		if (docs != null && !docs.isEmpty()) {
			isDeleted = false;
			json.setDocumentEntities(docs);
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("This person is used in documents and biographical records and can't be deleted. You should delete person from all documents. ");

		}

		if (bios != null && !bios.isEmpty()) {
			isDeleted = false;
			json.setPeopleRecords(bios);
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("This person is used in documents and biographical records and can't be deleted. You should delete person from all documents. ");

		}

		resp.setData(json);

		if (isDeleted) {
			caseStudyService.deleteAttachedItems(placeId, CaseStudyItem.EntityType.GEO);
			for(PlaceNameVariantJson variant: variants.getPlaceNameVariants()){
				caseStudyService.deleteAttachedItems(variant.getPlaceAllId(), CaseStudyItem.EntityType.GEO);
			}
			GeographicalPlaceJson oldPlaceJson = getGeographicalPlaceService().findGeographicalPlace(placeId, true);
			getHistoryLogService().registerAction(
					placeId, HistoryLogEntity.UserAction.DELETE,
					HistoryLogEntity.RecordType.GEO, request.getRequestURI(), null);

			Integer result = getGeographicalPlaceService().deletePlace(placeId);

			if (result > 0) {
				resp.setMessage(result
						+ " principal Place and all the variants are deleted.");
				resp.setStatus(StatusType.ok.toString());
			} else {
				resp.setMessage("Place name was already deleted or it was not found in DB.");
				resp.setStatus(StatusType.w.toString());
			}

		}

		return resp;

	}

	// http://localhost:8181/Mia/json/geographical/undeletePlace/175550
	@RequestMapping(value = "undeletePlace/{placeId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<Integer> undeletePlace(
			@PathVariable("placeId") Integer placeId,
			HttpServletRequest request) {

		GeographicalPlaceJson oldPlaceJson = getGeographicalPlaceService().findGeographicalPlace(placeId, true);
		getHistoryLogService().registerAction(
				placeId, HistoryLogEntity.UserAction.UNDELETE,
				HistoryLogEntity.RecordType.GEO, request.getRequestURI(), null);
		return getGeographicalPlaceService().unDeleteGeographicalPlace(placeId);

	}

	// http://localhost:8181/Mia/json/geographical/countRelatedDocumentsWithTopics/53796
	@RequestMapping(value = "countRelatedDocumentsWithTopics/{placeId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<TopicPlaceRelDocJson> countRelatedDocumentsWithTopics(
			@PathVariable("placeId") Integer placeId) {
		GenericResponseDataJson<TopicPlaceRelDocJson> resp = new GenericResponseDataJson<TopicPlaceRelDocJson>();
		TopicPlaceRelDocJson geoTopicPlace = getGeographicalPlaceService()
				.findTopicsByPlace(placeId);
		if (geoTopicPlace != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got RelatedDocumentsWithTopics with placeId: "
					+ placeId);
			resp.setData(geoTopicPlace);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No RelatedDocumentsWithTopics found with placeId: "
					+ placeId);
			return resp;
		}

	}

	// http://localhost:8181/Mia/json/geographical/findRelatedDocumentsWithPlaceOfOrigin/54033
	@RequestMapping(value = "findRelatedDocumentsWithPlaceOfOrigin/{placeId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<DocumentJson>>> findRelatedDocumentsWithPlaceOfOrigin(
			@PathVariable("placeId") Integer placeId) {

		GenericResponseDataJson<HashMap<String, List<DocumentJson>>> resp = new GenericResponseDataJson<HashMap<String, List<DocumentJson>>>();
		HashMap<String, List<DocumentJson>> docHash = new HashMap<String, List<DocumentJson>>();
		List<DocumentJson> list = getMiaDocumentService()
				.findDocumentsByPlaceOforigin(placeId);
		if (list != null && !list.isEmpty()) {
			docHash.put("documentEntities", list);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + list.size()
					+ " Documents for PlaceOfOrigin with ID:" + placeId);
			resp.setData(docHash);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Documents not exists in DB for this PlaceOfOrigin");
			return resp;
		}

	}

	// http://localhost:8181/Mia/json/geographical/findRelatedDocumentsWithTopics/29071/27
	@RequestMapping(value = "findRelatedDocumentsWithTopics/{placeId}/{topicId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<DocumentJson>>> findRelatedDocumentsWithTopics(
			@PathVariable("placeId") Integer placeId,
			@PathVariable("topicId") Integer topicId) {

		GenericResponseDataJson<HashMap<String, List<DocumentJson>>> resp = new GenericResponseDataJson<HashMap<String, List<DocumentJson>>>();
		HashMap<String, List<DocumentJson>> docHash = new HashMap<String, List<DocumentJson>>();
		List<DocumentJson> list = getGeographicalPlaceService()
				.findDccumentsByPlaceAndTopic(placeId, topicId);
		if (list != null && !list.isEmpty()) {
			docHash.put("documentEntities", list);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + list.size()
					+ " Documents for place with ID:" + placeId
					+ " / tipoc with ID:" + topicId);
			resp.setData(docHash);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Documents not exists in DB for this placeId and topicLisId");
			return resp;
		}

	}

	public GeographicalPlaceService getGeographicalPlaceService() {
		return geographicalPlaceService;
	}

	public void setGeographicalPlaceService(
			GeographicalPlaceService geographicalPlaceService) {
		this.geographicalPlaceService = geographicalPlaceService;
	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

	public void setMiaDocumentService(MiaDocumentService miaDocumentService) {
		this.miaDocumentService = miaDocumentService;
	}

	public BiographicalPeopleService getBiographicalPeopleService() {
		return biographicalPeopleService;
	}

	public void setBiographicalPeopleService(
			BiographicalPeopleService biographicalPeopleService) {
		this.biographicalPeopleService = biographicalPeopleService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public HistoryLogService getHistoryLogService() {
		return historyLogService;
	}
}