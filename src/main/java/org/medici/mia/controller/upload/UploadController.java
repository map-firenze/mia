package org.medici.mia.controller.upload;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.medici.mia.common.json.CollectionJson;
import org.medici.mia.common.json.GenericCheckRepoJson;
import org.medici.mia.common.json.GenericFindCollectionJson;
import org.medici.mia.common.json.GenericFindInsertJson;
import org.medici.mia.common.json.GenericFindRepositoryJson;
import org.medici.mia.common.json.GenericFindSeriesJson;
import org.medici.mia.common.json.GenericFindVolumeJson;
import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.GenericUploadResponseJson;
import org.medici.mia.common.json.RepositoryJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.dao.collection.CollectionDAO;
import org.medici.mia.domain.CollectionEntity;
import org.medici.mia.domain.GoogleLocation;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.domain.InsertEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.RepositoryEntity;
import org.medici.mia.domain.SeriesEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.UploadInfoEntity;
import org.medici.mia.domain.User;
import org.medici.mia.domain.UserAuthority;
import org.medici.mia.domain.UserRole;
import org.medici.mia.domain.Volume;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.myarchive.MyArchiveService;
import org.medici.mia.service.upload.UploadService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.github.underscore.Predicate;
import com.github.underscore.U;

/**
 *
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Controller
@RequestMapping("/upload")
public class UploadController {

	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private HistoryLogService historyLogService;

	@Autowired
	private UploadService uploadService;

	@Autowired
	private UserService userService;

	@Autowired
	private MyArchiveService archiveService;

	@RequestMapping(value = "uploadWithMeta", method = RequestMethod.POST)
	public @ResponseBody HashMap<String, GenericUploadResponseJson> upload(
			@ModelAttribute("files") FileUploadForm filesForm,
			@RequestParam("params[repository][repositoryId]") String repositoryId,
			@RequestParam("params[repository][repositoryName]") String repositoryName,
			@RequestParam("params[repository][repositoryAbbreviation]") String repositoryAbbreviation,
			@RequestParam("params[repository][repositoryDescription]") String repositoryDescription,
			@RequestParam("params[repository][repositoryLocation]") String gplace_id,
			@RequestParam("params[repository][repositoryCountry]") String country,
			@RequestParam("params[repository][repositoryCity]") String city,
			@RequestParam("params[collection][collectionId]") String collectionId,
			@RequestParam("params[collection][collectionName]") String collectionName,
			@RequestParam("params[collection][collectionDescription]") String collectionDescription,
			@RequestParam("params[series][seriesId]") Integer seriesId,
			@RequestParam("params[series][seriesName]") String seriesName,
			@RequestParam("params[series][seriesSubtitle]") String seriesSubtitle,
			@RequestParam("params[volume][volumeId]") Integer volumeId,
			@RequestParam("params[volume][volumeName]") String volume,
			@RequestParam("params[insert][insertId]") Integer insertId,
			@RequestParam("params[insert][insertName]") String insertName,
			@RequestParam("params[aeDescription]") String aeDescription,
			HttpSession session) {
		HashMap<String, GenericUploadResponseJson> respHash = new HashMap<String, GenericUploadResponseJson>();
		GenericUploadResponseJson resp = new GenericUploadResponseJson();
		// Recovery User Name
		String owner = "";
		if(aeDescription != null && aeDescription.length() > 100){
			HashMap<String, GenericUploadResponseJson> responseJsonHashMap = new HashMap<String, GenericUploadResponseJson>();
			GenericUploadResponseJson uploadResponseJson = new GenericUploadResponseJson();
			uploadResponseJson.setMessage("Archival Entity Description is too long (max. 250 characters)");
			uploadResponseJson.setStatus("error");
			responseJsonHashMap.put("uploadImages-ko", uploadResponseJson);
			return responseJsonHashMap;
		}
		for(MultipartFile file: filesForm.getFiles()){
		    if(file.getOriginalFilename().length() > 100){
                HashMap<String, GenericUploadResponseJson> responseJsonHashMap = new HashMap<String, GenericUploadResponseJson>();
                GenericUploadResponseJson uploadResponseJson = new GenericUploadResponseJson();
				uploadResponseJson.setMessage("Uploaded file name `"+file.getOriginalFilename()+"` is too long (max. 100 characters)");
				uploadResponseJson.setStatus("error");
				responseJsonHashMap.put("uploadImages-ko", uploadResponseJson);
				return responseJsonHashMap;
			}
        }
		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()).getUsername());

		// GoogleLocation
		GoogleLocation glEntity = new GoogleLocation();
		glEntity.setCity(city);
		glEntity.setCountry(country);
		glEntity.setgPlaceId(gplace_id);

		// Repository
		RepositoryEntity repEntity = new RepositoryEntity();
		if (repositoryId != null && !repositoryId.isEmpty()) {
			repEntity.setRepositoryId(Integer.valueOf(repositoryId));
		}

		repEntity.setRepositoryName(repositoryName);
		repEntity.setLocation(gplace_id);
		repEntity.setRepositoryAbbreviation(repositoryAbbreviation);
		repEntity.setRepositoryDescription(repositoryDescription);

		// Collection
		CollectionEntity collEntity = new CollectionEntity();
		if (collectionId != null && !collectionId.isEmpty()) {
			collEntity.setCollectionId(Integer.valueOf(collectionId));
		}
		collEntity.setCollectionName(collectionName);

		collEntity.setCollectionDescription(collectionDescription);

		// Series
		SeriesEntity serEntity = new SeriesEntity();
		if (seriesId != null) {
			serEntity.setSeriesId(Integer.valueOf(seriesId));
		}
		serEntity.setTitle(seriesName);
		serEntity.setSubtitle1(seriesSubtitle);

		// Volume
		Volume volEntity;
		if (volumeId != null) {
			volEntity = archiveService.findVolumeById(volumeId);
			if(volEntity == null){
				resp.setStatus("error");
				resp.setMessage("Cannot create an upload. Volume with id = "+volumeId+" does not exist");
				respHash.put("uploadImages-ko", resp);
				return respHash;
			}
			if(volEntity.getLogicalDelete() != null && volEntity.getLogicalDelete()) {
				resp.setStatus("error");
				resp.setMessage("Cannot create an upload. Volume with id = "+volumeId+" is deleted");
				respHash.put("uploadImages-ko", resp);
				return respHash;
			}
			boolean hasInserts = U.any(volEntity.getInsertEntities(), new Predicate<InsertEntity>() {
				@Override
				public boolean test(InsertEntity insertEntity) {
					return insertEntity.getLogicalDelete() == null ? true : !insertEntity.getLogicalDelete();
				}
			});
			if(hasInserts && insertId == null && (insertName == null || insertName.isEmpty())){
				logger.error("Cannot create an upload. Volume with id = "+volumeId+" has inserts");
				resp.setStatus("error");
				resp.setMessage("Cannot create an upload. Volume with id = "+volumeId+" has inserts");
				respHash.put("uploadImages-ko", resp);
				return respHash;
			}
		} else {
			List<Volume> volumes = archiveService.findVolumeByName(volume, Integer.valueOf(collectionId));
			volEntity = volumes == null || volumes.isEmpty() ? null : volumes.get(0);
			if(volEntity == null) {
				volEntity = new Volume();
				if (volume != null) {
					volEntity.setCreatedBy(user);
					volEntity.setDateCreated(new Date());
					volEntity.setVolume(volume);
				}
			}
		}

		// Insert
		InsertEntity insEntity;
		if (insertId != null) {
			insEntity = archiveService.findInsertById(insertId);
			if(insEntity == null){
				insEntity = new InsertEntity();
				if (insertName != null && !insertName.isEmpty()) {
					insEntity.setInsertName(insertName);
					insEntity.setDateCreated(new Date());
					insEntity.setCreatedBy(user);

				}
			}
		} else {
			insEntity = new InsertEntity();
			if (insertName != null && !insertName.isEmpty()) {
				for(InsertEntity ins : volEntity.getInsertEntities()){
					if(insertName.equals(ins.getInsertName())){
						insEntity = ins;
						break;
					}
				}
				if(insEntity.getInsertId() == null) {
					insEntity.setInsertName(insertName);
					insEntity.setDateCreated(new Date());
					insEntity.setCreatedBy(user);
				}

			}

		}

		owner = user.getAccount();

		UploadBean uploadBean = new UploadBean(filesForm.getFiles(), glEntity,
				repEntity, collEntity, serEntity, volEntity, insEntity, owner,
				aeDescription);

		UploadBean uploadBeanAfterInsert = getUploadService()
				.insertUploadImage(uploadBean);

		session.setAttribute("futureUploadResp", null);

		// 3. Convert files to tiff
		Future<List<GenericUploadResponseJson>> futureUploadResp = getUploadService()
				.convertFiles(uploadBeanAfterInsert.getUploadFileEntities(),
						uploadBeanAfterInsert.getRealPath(), false);

		session.setAttribute("futureUploadResp",
				(Future<List<GenericUploadResponseJson>>) futureUploadResp);

		// Implementation of the JSON object to return to the FE, as written in
		// the documentation.
		// The test if the upload is OK is made if the RealPath exists, is it
		// OK??
		String ret = uploadBeanAfterInsert.getRealPath();
		if (ret != null && !ret.isEmpty()) {
			if(volEntity.getSummaryId() != null || volEntity.getVolume() != null){
				if(insEntity.getInsertId() != null || insEntity.getInsertName() != null){
					insEntity.setAeCount(insEntity.getAeCount() + 1);
					archiveService.saveInsert(insEntity);
				} else {
					volEntity.setAeCount(volEntity.getAeCount() + 1);
					archiveService.saveVolume(volEntity);
				}
			}
			historyLogService.registerAction(
					uploadBeanAfterInsert.getUploadInfoId(), HistoryLogEntity.UserAction.CREATE,
					HistoryLogEntity.RecordType.AE, null, null);
			resp.setStatus("OK");
			resp.setMessage("The Upload process performed successfully!!");
			resp.setUploadInfoId(uploadBeanAfterInsert.getUploadInfoId());
			respHash.put("uploadImages-ok", resp);
		} else {
			resp.setStatus("KO");
			resp.setMessage("Upload Failed!!");
			respHash.put("uploadImages-ko", resp);
		}
		return respHash;
	}

	// localhost:8181/Mia/json/upload/statusUpload/
	@RequestMapping(value = "statusUpload", method = RequestMethod.GET)
	public @ResponseBody Boolean statusUpload(HttpSession session) {

		Future<List<GenericUploadResponseJson>> statusUpload = (Future<List<GenericUploadResponseJson>>) session
				.getAttribute("futureUploadResp");

		if (statusUpload != null && statusUpload.isDone()) {
			System.out.println("statusUpload Generation Done");
			return true;
		} else {
			System.out.println("Still Working on statusUpload");
			return false;
		}
	}

	// localhost:8181/Mia/json/upload/getUploadReport/
	@RequestMapping(value = "getUploadReport", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<GenericUploadResponseJson>> getUploadReport(
			HttpSession session) {
		GenericResponseDataJson<List<GenericUploadResponseJson>> resp = new GenericResponseDataJson<List<GenericUploadResponseJson>>();
		resp.setMessage("Convert Reposrt is null");
		resp.setStatus(StatusType.w.toString());

		try {
			Future<List<GenericUploadResponseJson>> statusUpload = (Future<List<GenericUploadResponseJson>>) session
					.getAttribute("futureUploadResp");

			if (statusUpload == null) {
				return resp;
			}

			List<GenericUploadResponseJson> listResponse = statusUpload.get();
			resp.setMessage("Complete Convert Report");
			resp.setStatus(StatusType.ok.toString());
			resp.setData(listResponse);

		} catch (InterruptedException ie) {
			logger.error(ie.getMessage());
		} catch (ExecutionException ee) {
			logger.error(ee.getMessage());
		}
		return resp;
	}

	// localhost:8181/Mia/json/upload/findRepository/ABC
	@RequestMapping(value = "findRepository/{repositoryName}", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, List<GenericFindRepositoryJson>> findRepository(
			@PathVariable("repositoryName") String repositoryName) {

		HashMap<String, List<GenericFindRepositoryJson>> repHash = new HashMap<String, List<GenericFindRepositoryJson>>();
		List<RepositoryEntity> repositories = getUploadService()
				.findRepositoryByName(repositoryName);
		List<GenericFindRepositoryJson> repsJson = new ArrayList<GenericFindRepositoryJson>();
		for (RepositoryEntity rep : repositories) {
			GenericFindRepositoryJson repJson = new GenericFindRepositoryJson();
			repJson.setRepositoryName(rep.getRepositoryName());
			repJson.setRepositoryId(String.valueOf(rep.getRepositoryId()));
			repJson.setRepositoryAbbreviation(rep.getRepositoryAbbreviation());
			repJson.setRepositoryCity(rep.getGoogleLocation().getCity());
			repJson.setRepositoryCountry(rep.getGoogleLocation().getCountry());
			repJson.setRepositoryLocation(rep.getGoogleLocation().getgPlaceId());
			repJson.setRepositoryDescription(rep.getRepositoryDescription());
			repsJson.add(repJson);
			logger.info(repJson.toString());
		}
		repHash.put("repository", repsJson);

		return repHash;

	}

	// localhost:8181/Mia/json/upload/findAllRepositories
	@RequestMapping(value = "findAllRepositories", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, List<RepositoryJson>> findAllRepositories() {

		HashMap<String, List<RepositoryJson>> repHash = new HashMap<String, List<RepositoryJson>>();
		List<RepositoryJson> repositories = getUploadService()
				.findAllRepositories();

		repHash.put("repositories", repositories);

		return repHash;

	}

	// localhost:8181/Mia/json/upload/checkRepository/{repoName}/{repoLocation}/{repoAbbreviation}
	@RequestMapping(value = "checkRepository/{repositoryName}/{repositoryLocation}/{repositoryAbbreviation}", method = RequestMethod.GET)
	public @ResponseBody GenericCheckRepoJson checkRepository(
			@PathVariable("repositoryName") String repositoryName,
			@PathVariable("repositoryLocation") String repositoryLocation,
			@PathVariable("repositoryAbbreviation") String repositoryAbbreviation) {

		GenericCheckRepoJson resp = new GenericCheckRepoJson();
		List<RepositoryEntity> repositories = getUploadService()
				.checkRepositoryByNameLocation(repositoryName,
						repositoryLocation);
		List<RepositoryEntity> repositories2Check = getUploadService()
				.checkRepositoryByLocationAbbreviation(repositoryLocation,
						repositoryAbbreviation);
		if ((repositories != null && !repositories.isEmpty())) {
			resp.setStatus("ko");
			resp.setMessage("wrongName");
		} else if ((repositories2Check != null && !repositories2Check.isEmpty())) {
			resp.setStatus("ko");
			resp.setMessage("wrongAbbreviation");
		} else {
			resp.setStatus("ok");
		}
		return resp;
	}

	// localhost:8181/Mia/json/upload/findCollection/collectionName//123
	@RequestMapping(value = "findCollection/{collectionName}/{repositoryId}", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, List<GenericFindCollectionJson>> findCollection(
			@PathVariable("collectionName") String collectionName,
			@PathVariable("repositoryId") String repositoryId) {
		HashMap<String, List<GenericFindCollectionJson>> collHash = new HashMap<String, List<GenericFindCollectionJson>>();
		List<CollectionEntity> collections = getUploadService()
				.findCollectionsByName(collectionName,
						Integer.valueOf(repositoryId));
		List<GenericFindCollectionJson> collJson = new ArrayList<GenericFindCollectionJson>();
		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()).getUsername());
		boolean canCreate = false;
		for (UserRole role : user.getUserRoles()) {
			canCreate = role.containsAuthority(UserAuthority.Authority.ADMINISTRATORS)
					|| role.containsAuthority(UserAuthority.Authority.ONSITE_FELLOWS);
			if(canCreate) break;
		}
		for (CollectionEntity coll : collections) {
			GenericFindCollectionJson repJson = new GenericFindCollectionJson();
			repJson.setCollectionName(coll.getCollectionName());
			repJson.setCollectionId(String.valueOf(coll.getCollectionId()));
			repJson.setCollectionAbbreviation(coll.getCollectionAbbreviation());
			repJson.setStudyCollection(coll.getStudyCollection());
			collJson.add(repJson);
		}
		collHash.put("collection", collJson);

		return collHash;

	}

	// localhost:8181/Mia/json/upload/findAllCollections/123
	@RequestMapping(value = "findAllCollections/{repositoryId}", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, List<GenericFindCollectionJson>> findAllCollections(
			@PathVariable("repositoryId") String repositoryId) {
		HashMap<String, List<GenericFindCollectionJson>> collHash = new HashMap<String, List<GenericFindCollectionJson>>();
		List<CollectionEntity> collections = getUploadService()
				.findAllCollections(Integer.valueOf(repositoryId));
		List<GenericFindCollectionJson> collJson = new ArrayList<GenericFindCollectionJson>();
		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()).getUsername());
		boolean canCreate = false;
		for (UserRole role : user.getUserRoles()) {
			canCreate = role.containsAuthority(UserAuthority.Authority.ADMINISTRATORS)
					|| role.containsAuthority(UserAuthority.Authority.ONSITE_FELLOWS);
			if(canCreate) break;
		}
		for (CollectionEntity coll : collections) {
			GenericFindCollectionJson repJson = new GenericFindCollectionJson();
			repJson.setCollectionName(coll.getCollectionName());
			repJson.setCollectionId(String.valueOf(coll.getCollectionId()));
			repJson.setCollectionAbbreviation(coll.getCollectionAbbreviation());
			repJson.setStudyCollection(coll.getStudyCollection());
			collJson.add(repJson);
		}
		collHash.put("collection", collJson);

		return collHash;

	}

	// localhost:8181/Mia/json/upload/checkCollection/{collName}/{repositoryId}
	@RequestMapping(value = "checkCollection/{collName}/{repositoryId}", method = RequestMethod.GET)
	public @ResponseBody GenericCheckRepoJson checkCollection(
			@PathVariable("collName") String collName,
			@PathVariable("repositoryId") Integer repositoryId) {

		GenericCheckRepoJson resp = new GenericCheckRepoJson();
		List<CollectionEntity> collections = getUploadService()
				.checkCollectionsByName(collName, repositoryId);
		if ((collections != null && !collections.isEmpty())) {
			resp.setStatus("ko");
			resp.setMessage("wrongNameAndRepoId");
		} else {
			resp.setStatus("ok");
		}
		return resp;
	}

	// localhost:8181/Mia/json/upload/findSeries/seriesName//123
	@RequestMapping(value = "findSeries/{seriesName}/{collectionId}", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, List<GenericFindSeriesJson>> findSeries(
			@PathVariable("seriesName") String seriesName,
			@PathVariable("collectionId") String collectionId) {
		HashMap<String, List<GenericFindSeriesJson>> serHash = new HashMap<String, List<GenericFindSeriesJson>>();
		List<SeriesEntity> series = getUploadService().findSeriesByTitle(
				seriesName, Integer.valueOf(collectionId));
		List<GenericFindSeriesJson> serJson = new ArrayList<GenericFindSeriesJson>();
		for (SeriesEntity ser : series) {
			GenericFindSeriesJson collJson = new GenericFindSeriesJson();
			collJson.setSeriesName(ser.getTitle());
			collJson.setSeriesSubtitle(String.valueOf(ser.getSubtitle1()));
			collJson.setSeriesId(String.valueOf(ser.getSeriesId()));

			serJson.add(collJson);

		}
		serHash.put("series", serJson);

		return serHash;

	}

	// localhost:8181/Mia/json/upload/findAllSeries/123
	@RequestMapping(value = "findAllSeries/{collectionId}", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, List<GenericFindSeriesJson>> findAllSeries(

			@PathVariable("collectionId") String collectionId) {
		HashMap<String, List<GenericFindSeriesJson>> serHash = new HashMap<String, List<GenericFindSeriesJson>>();
		List<SeriesEntity> series = getUploadService().findAllSeries(
				Integer.valueOf(collectionId));
		List<GenericFindSeriesJson> serJson = new ArrayList<GenericFindSeriesJson>();
		for (SeriesEntity ser : series) {
			GenericFindSeriesJson collJson = new GenericFindSeriesJson();
			collJson.setSeriesName(ser.getTitle());
			collJson.setSeriesSubtitle(String.valueOf(ser.getSubtitle1()));
			collJson.setSeriesId(String.valueOf(ser.getSeriesId()));

			serJson.add(collJson);

		}
		serHash.put("series", serJson);

		return serHash;

	}

	// localhost:8181/Mia/json/upload/findVolume/volumeName//123
	@RequestMapping(value = "findVolume/{volumeName}/{collectionId}", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, List<GenericFindVolumeJson>> findVolume(
			@PathVariable("volumeName") String volumeName,
			@PathVariable("collectionId") Integer collectionId,
			@RequestParam(value = "withAE", required = false) Boolean withAE,
			@RequestParam(value = "withDE", required = false) Boolean withDE) {
		withDE = withDE == null ? false : withDE;
		withAE = withAE == null ? false : withAE;
		HashMap<String, List<GenericFindVolumeJson>> volHash = new HashMap<String, List<GenericFindVolumeJson>>();
		List<Volume> volumes = getUploadService().findVolumeByName(volumeName,
				Integer.valueOf(collectionId));
		List<GenericFindVolumeJson> volJson = new ArrayList<GenericFindVolumeJson>();
		for (Volume vol : volumes) {
			GenericFindVolumeJson collJson = new GenericFindVolumeJson();
			collJson.setVolumeName(String.valueOf(vol.getVolume()));
			collJson.setVolumeId(String.valueOf(vol.getSummaryId()));

			if(withAE) {
				boolean hasAE = false;
				for(UploadInfoEntity u: vol.getUploadInfoEntities()){
					if(hasAE) break;
					if(u.getLogicalDelete() != null) {
						if (u.getLogicalDelete() == 0) {
							hasAE = true;
						}
					}
				}
				if(!hasAE) continue;
			}

			if(withDE) {
				boolean hasDE = false;
				for(UploadInfoEntity u: vol.getUploadInfoEntities()){
					if(u.getLogicalDelete() != null && u.getLogicalDelete() > 0){
						continue;
					}
					for(UploadFileEntity f: u.getUploadFileEntities()){
						for(MiaDocumentEntity d : f.getMiaDocumentEntities()) {
							if(d.getFlgLogicalDelete() == null){
								hasDE = true;
								break;
							} else if (!d.getFlgLogicalDelete()) {
								hasDE = true;
								break;
							}
						}
						if(hasDE){
							break;
						}
					}
					if(hasDE){
						break;
					}
				}
				if(!hasDE){
					continue;
				}
			}
			collJson.setAeCount(vol.getAeCount());
			volJson.add(collJson);

		}
		volHash.put("volume", volJson);

		return volHash;

	}

	private List paginateResult(List res, Integer page, Integer perPage){
		List paginated = new ArrayList();
		int limit = page * perPage;
		int offset = limit - perPage;
		if(limit > 0 && offset >= 0 && offset < limit && offset < res.size()){
			if(limit < res.size()) {
				paginated = res.subList(offset, limit);
			}
			else if(limit > res.size()){
				paginated = res.subList(offset, res.size());
			}
		}
		return paginated;
	}

	// localhost:8181/Mia/json/upload/findVolume/2?q=123
	@RequestMapping(value = "findVolume/{collectionId}", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, List<GenericFindVolumeJson>> findVolumeParam(
			@RequestParam(value = "withDE", required = false) Boolean withDE,
			@RequestParam(value = "withAE", required = false) Boolean withAE,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "perPage", required = false) Integer perPage,
			@RequestParam("q") String volumeName,
			@PathVariable("collectionId") Integer collectionId) {
		withDE = withDE == null ? false : withDE;
		withAE = withAE == null ? false : withAE;
		HashMap<String, List<GenericFindVolumeJson>> volHash = new HashMap<String, List<GenericFindVolumeJson>>();
		List<Volume> volumes = getUploadService().findVolumeByName(volumeName,
				Integer.valueOf(collectionId));
		List<GenericFindVolumeJson> volJson = new ArrayList<GenericFindVolumeJson>();
		for (Volume vol : volumes) {
			GenericFindVolumeJson collJson = new GenericFindVolumeJson();
			collJson.setVolumeName(String.valueOf(vol.getVolume()));
			collJson.setVolumeId(String.valueOf(vol.getSummaryId()));

			if(withAE) {
				boolean hasAE = false;
				for(UploadInfoEntity u: vol.getUploadInfoEntities()){
					if(hasAE) break;
					if(u.getLogicalDelete() != null) {
						if (u.getLogicalDelete() == 0) {
							hasAE = true;
						}
					}
				}
				if(!hasAE) continue;
			}

			if(withDE) {
				boolean hasDE = false;
				for(UploadInfoEntity u: vol.getUploadInfoEntities()){
					for(UploadFileEntity f: u.getUploadFileEntities()){
						if(u.getLogicalDelete() != null && u.getLogicalDelete() > 0){
							continue;
						}
						for(MiaDocumentEntity d : f.getMiaDocumentEntities()) {
							if(d.getFlgLogicalDelete() == null){
								hasDE = true;
								break;
							} else if (!d.getFlgLogicalDelete()) {
								hasDE = true;
								break;
							}
						}
						if(hasDE){
							break;
						}
					}
					if(hasDE){
						break;
					}
				}
				if(!hasDE){
					continue;
				}
			}
			collJson.setAeCount(vol.getAeCount());
			volJson.add(collJson);

		}
		if(page != null && perPage != null){
			volJson = paginateResult(volJson, page, perPage);
		}
		volHash.put("volume", volJson);

		return volHash;

	}

	// localhost:8181/Mia/json/upload/findAllVolumes/123
	@RequestMapping(value = "findAllVolumes/{collectionId}", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, List<GenericFindVolumeJson>> findAllVolumes(
			@PathVariable("collectionId") String collectionId,
			@RequestParam(value = "withAe", required = false) Boolean withAe,
			HttpServletResponse httpServletResponse) {
		withAe = withAe == null ? false : withAe;
		HashMap<String, List<GenericFindVolumeJson>> volHash = new HashMap<String, List<GenericFindVolumeJson>>();
		try {
			Integer.valueOf(collectionId);
		} catch (NumberFormatException ex){
			httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return null;
		}
//		List<Volume> volumes = getUploadService().findAllVolumes(
//				Integer.valueOf(collectionId));
		List<Volume> volumes = getUploadService().findVolumeByName("",
				Integer.valueOf(collectionId));
		List<GenericFindVolumeJson> volJson = new ArrayList<GenericFindVolumeJson>();
		if(volumes == null || volumes.isEmpty()){
			volHash.put("volume", volJson);
		}
		for (Volume vol : volumes) {
			if(vol.getLogicalDelete()) continue;
			GenericFindVolumeJson collJson = new GenericFindVolumeJson();
			collJson.setVolumeName(String.valueOf(vol.getVolume()));
			collJson.setVolumeId(String.valueOf(vol.getSummaryId()));
			collJson.setAeCount(vol.getAeCount());
			if(withAe && collJson.getAeCount() != 0){
				volJson.add(collJson);
			} else if(U.any(vol.getInsertEntities(), new Predicate<InsertEntity>() {
				@Override
				public boolean test(InsertEntity insertEntity) {
					return insertEntity.getAeCount() > 0 && insertEntity.getLogicalDelete() != null && !insertEntity.getLogicalDelete();
				}
			})){
				volJson.add(collJson);
			}
		}
		volHash.put("volume", volJson);

		return volHash;

	}

	// localhost:8181/Mia/json/upload/findInsert/insertName//123
	@RequestMapping(value = "findInsert/{insertName}/{volumeId}", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, List<GenericFindInsertJson>> findInsert(
			@PathVariable("insertName") String insertName,
			@PathVariable("volumeId") Integer volumeId,
			@RequestParam(value = "withDE", required = false) Boolean withDE,
			@RequestParam(value = "withAE", required = false) Boolean withAE){
		withDE = withDE == null ? false : withDE;
		withAE = withAE == null ? false : withAE;
		HashMap<String, List<GenericFindInsertJson>> insHash = new HashMap<String, List<GenericFindInsertJson>>();
		List<InsertEntity> inserts = getUploadService().findInsertByName(
				insertName, Integer.valueOf(volumeId));
		List<GenericFindInsertJson> insJson = new ArrayList<GenericFindInsertJson>();
		for (InsertEntity ins : inserts) {
			if(ins.getInsertName().equals("Restored") && ins.getAeCount() == 0) continue;
			GenericFindInsertJson volJson = new GenericFindInsertJson();
			volJson.setInsertName(String.valueOf(ins.getInsertName()));
			volJson.setInsertId(String.valueOf(ins.getInsertId()));

			if(withAE) {
				boolean hasAE = false;
				for(UploadInfoEntity u: ins.getUploadInfoEntities()){
					if(hasAE) break;
					if(u.getLogicalDelete() != null) {
						if (u.getLogicalDelete() == 0) {
							hasAE = true;
						}
					}
				}
				if(!hasAE) continue;
			}

			if(withDE) {
				boolean hasDE = false;
				for(UploadInfoEntity u: ins.getUploadInfoEntities()){
					if(u.getLogicalDelete() != null){
						if(u.getLogicalDelete() > 0) {
							continue;
						}
					}
					for(UploadFileEntity f: u.getUploadFileEntities()){
						for(MiaDocumentEntity d : f.getMiaDocumentEntities()) {
							if(d.getFlgLogicalDelete() == null){
								hasDE = true;
								break;
							} else if (!d.getFlgLogicalDelete()) {
								hasDE = true;
								break;
							}
						}
						if(hasDE){
							break;
						}
					}
					if(hasDE){
						break;
					}
				}
				if(!hasDE){
					continue;
				}
			}

			volJson.setAeCount(ins.getAeCount());
			insJson.add(volJson);

		}
		insHash.put("insert", insJson);

		return insHash;

	}

	// localhost:8181/Mia/json/upload/findInsert/insertName?q=123
	@RequestMapping(value = "findInsert/{volumeId}", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, List<GenericFindInsertJson>> findInsertParam(
			@RequestParam(value = "withDE", required = false) Boolean withDE,
			@RequestParam(value = "withAE", required = false) Boolean withAE,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "perPage", required = false) Integer perPage,
			@RequestParam("q") String insertName,
			@PathVariable("volumeId") Integer volumeId) {
		withDE = withDE == null ? false : withDE;
		withAE = withAE == null ? false : withAE;
		HashMap<String, List<GenericFindInsertJson>> insHash = new HashMap<String, List<GenericFindInsertJson>>();
		List<InsertEntity> inserts = getUploadService().findInsertByName(
				insertName, Integer.valueOf(volumeId));
		List<GenericFindInsertJson> insJson = new ArrayList<GenericFindInsertJson>();
		for (InsertEntity ins : inserts) {
			if(ins.getInsertName().equals("Restored") && ins.getAeCount() == 0) continue;
			GenericFindInsertJson volJson = new GenericFindInsertJson();
			volJson.setInsertName(String.valueOf(ins.getInsertName()));
			volJson.setInsertId(String.valueOf(ins.getInsertId()));

			if(withAE) {
				boolean hasAE = false;
				for(UploadInfoEntity u: ins.getUploadInfoEntities()){
					if(hasAE) break;
					if(u.getLogicalDelete() != null) {
						if (u.getLogicalDelete() == 0) {
							hasAE = true;
						}
					}
				}
				if(!hasAE) continue;
			}

			if(withDE) {
				boolean hasDE = false;
				for(UploadInfoEntity u: ins.getUploadInfoEntities()){
					if(u.getLogicalDelete() != null && u.getLogicalDelete() > 0){
						continue;
					}
					for(UploadFileEntity f: u.getUploadFileEntities()){
						for(MiaDocumentEntity d : f.getMiaDocumentEntities()) {
							if(d.getFlgLogicalDelete() == null){
								hasDE = true;
								break;
							} else if (!d.getFlgLogicalDelete()) {
								hasDE = true;
								break;
							}
						}
						if(hasDE){
							break;
						}
					}
					if(hasDE){
						break;
					}
				}
				if(!hasDE){
					continue;
				}
			}
			volJson.setAeCount(ins.getAeCount());
			insJson.add(volJson);

		}
		if(page != null && perPage != null){
			paginateResult(insJson, page, perPage);
		}
		insHash.put("insert", insJson);

		return insHash;

	}

	// localhost:8181/Mia/json/upload/findAllInserts/123
	@RequestMapping(value = "findAllInserts/{volumeId}", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, List<GenericFindInsertJson>> findAllInserts(
			@PathVariable("volumeId") String volumeId,
			@RequestParam(value = "withAe", required = false) Boolean withAe,
			HttpServletResponse httpServletResponse) {
		withAe = withAe == null ? false : withAe;
		HashMap<String, List<GenericFindInsertJson>> insHash = new HashMap<String, List<GenericFindInsertJson>>();
		try {
			Integer.valueOf(volumeId);
		} catch (NumberFormatException ex){
			httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return null;
		}
//		List<InsertEntity> inserts = getUploadService().findAllInserts(
//				Integer.valueOf(volumeId));
		List<InsertEntity> inserts = getUploadService().findInsertByName(
				"", Integer.valueOf(volumeId));
		List<GenericFindInsertJson> insJson = new ArrayList<GenericFindInsertJson>();
		if(inserts == null || inserts.isEmpty()){
			insHash.put("insert", insJson);
			return insHash;
		}
		for (InsertEntity ins : inserts) {
			if(ins.getInsertName().equals("Restored") && ins.getAeCount() == 0) continue;
			GenericFindInsertJson volJson = new GenericFindInsertJson();
			volJson.setInsertName(String.valueOf(ins.getInsertName()));
			volJson.setInsertId(String.valueOf(ins.getInsertId()));
			volJson.setAeCount(ins.getAeCount());
			if(withAe && volJson.getAeCount() == 0){
				continue;
			}
			insJson.add(volJson);

		}
		insHash.put("insert", insJson);

		return insHash;

	}
	
	@RequestMapping(value="addCollection", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<CollectionJson> addCollection(
			@RequestBody CollectionJson collectionJson) {
		
		GenericResponseDataJson<CollectionJson> response = 
				new GenericResponseDataJson<CollectionJson>();
		
		CollectionEntity collectionEntity = uploadService.
				addCollection(collectionJson);
		
		if (collectionEntity == null) {
			response.setStatus(StatusType.ko.toString());
			response.setMessage("Error during collection creation");
			response.setData(null);
		}
		
		collectionJson.setCollectionId(
				collectionEntity.getCollectionId().toString());
		
		response.setStatus(StatusType.ok.toString());
		response.setMessage(String.format(
				"Created collection with id: %s", 
				collectionEntity.getCollectionId()));
		response.setData(collectionJson);
		
		return response;
	}
	
	public UploadService getUploadService() {
		return uploadService;
	}

	public void setUploadService(UploadService uploadService) {
		this.uploadService = uploadService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}