/*
 * FileUploadForm.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.controller.upload;

import java.io.File;
import java.util.List;

import org.medici.mia.domain.CollectionEntity;
import org.medici.mia.domain.GoogleLocation;
import org.medici.mia.domain.InsertEntity;
import org.medici.mia.domain.RepositoryEntity;
import org.medici.mia.domain.SeriesEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.Volume;
import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class UploadBean {

	private List<MultipartFile> files;
	private RepositoryEntity repEntity;
	private CollectionEntity collEntity;
	private GoogleLocation glEntity;
	private SeriesEntity series;
	private Volume volume;
	private InsertEntity insert;
	private String owner;
	private Integer uploadInfoId;
	private String realPath;
	private String aeDescription;
	private List<UploadFileEntity> uploadFileEntities;
	private List<org.medici.mia.domain.FolioEntity>  folioEntities;
	
	private String optionalImage;

	private List<File> uploadfiles;
	
	public UploadBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UploadBean(List<MultipartFile> files, GoogleLocation glEntity,
			RepositoryEntity repEntity, CollectionEntity collEntity,
			SeriesEntity series, Volume volume, InsertEntity insert,
			String owner, String aeDescription) {
		this.files = files;
		this.glEntity = glEntity;
		this.repEntity = repEntity;
		this.collEntity = collEntity;
		this.series = series;
		this.volume = volume;
		this.insert = insert;
		this.owner = owner;
		this.aeDescription = aeDescription;

	}
	
	public UploadBean(List<MultipartFile> files, GoogleLocation glEntity,
			RepositoryEntity repEntity, CollectionEntity collEntity,
			SeriesEntity series, Volume volume, InsertEntity insert,
			String owner, String aeDescription, String optionalImage) {
		this.files = files;
		this.glEntity = glEntity;
		this.repEntity = repEntity;
		this.collEntity = collEntity;
		this.series = series;
		this.volume = volume;
		this.insert = insert;
		this.owner = owner;
		this.aeDescription = aeDescription;
		this.optionalImage = optionalImage;

	}
	
	public void setUploadfiles(List<File> uploadfiles) {
		this.uploadfiles = uploadfiles;
	}
	
	public List<File> getUploadfiles() {
		return uploadfiles;
	}
	
	public String getOptionalImage() {
		return optionalImage;
	}
	
	public void setOptionalImage(String optionalImage) {
		this.optionalImage = optionalImage;
	}

	public List<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}

	public RepositoryEntity getRepEntity() {
		return repEntity;
	}

	public void setRepEntity(RepositoryEntity repEntity) {
		this.repEntity = repEntity;
	}

	public CollectionEntity getCollEntity() {
		return collEntity;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public void setCollEntity(CollectionEntity collEntity) {
		this.collEntity = collEntity;
	}

	public GoogleLocation getGlEntity() {
		return glEntity;
	}

	public void setGlEntity(GoogleLocation glEntity) {
		this.glEntity = glEntity;
	}

	public Volume getVolume() {
		return volume;
	}

	public void setVolume(Volume volume) {
		this.volume = volume;
	}

	public InsertEntity getInsert() {
		return insert;
	}

	public void setInsert(InsertEntity insert) {
		this.insert = insert;
	}

	public SeriesEntity getSeries() {
		return series;
	}

	public void setSeries(SeriesEntity series) {
		this.series = series;
	}

	public Integer getUploadInfoId() {
		return uploadInfoId;
	}

	public void setUploadInfoId(Integer uploadInfoId) {
		this.uploadInfoId = uploadInfoId;
	}

	public String getRealPath() {
		return realPath;
	}

	public void setRealPath(String realPath) {
		this.realPath = realPath;
	}

	public List<UploadFileEntity> getUploadFileEntities() {
		return uploadFileEntities;
	}

	public void setUploadFileEntities(List<UploadFileEntity> uploadFileEntities) {
		this.uploadFileEntities = uploadFileEntities;
	}

	public String getAeDescription() {
		return aeDescription;
	}

	public void setAeDescription(String aeDescription) {
		this.aeDescription = aeDescription;
	}
	
	public void setFolioEntities(
			List<org.medici.mia.domain.FolioEntity> folioEntities) {
		this.folioEntities = folioEntities;
	}
	
	public List<org.medici.mia.domain.FolioEntity> getFolioEntities() {
		return folioEntities;
	}
}
