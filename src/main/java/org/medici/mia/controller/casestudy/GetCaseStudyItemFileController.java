package org.medici.mia.controller.casestudy;

import org.apache.log4j.Logger;
import org.medici.mia.command.peoplebase.ShowPortraitPersonCommand;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.dao.casestudy.CaseStudyDAO;
import org.medici.mia.domain.CaseStudyItem;
import org.medici.mia.domain.People;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.casestudy.CaseStudyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@Controller
@RequestMapping("/src/case_study/getItemFile")
public class GetCaseStudyItemFileController {

    @Autowired
    private CaseStudyDAO caseStudyDAO;

    @Autowired
    private CaseStudyService caseStudyService;

    private final Logger logger = Logger.getLogger(this.getClass());

    @RequestMapping(method = RequestMethod.GET)
    public void setupForm(
            @RequestParam("id") Integer id,
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse
    ) {
        CaseStudyItem item = caseStudyDAO.findItem(id);
        GenericResponseJson resp = new GenericResponseJson();
        FileInputStream fileIn = null;
        ServletOutputStream out = null;
        resp.setStatus("error");
        if (item == null) {
            httpServletResponse.setStatus(404);
        } else if (!caseStudyService.validateUser(item.getFolder().getCaseStudy())) {
            httpServletResponse.setStatus(403);
        } else {
            try {
                if (!(!item.getEntityType().equals(CaseStudyItem.EntityType.FILE) || item.getPath() == null)) {
                    File file = new File(item.getPath());
                    if (file.exists()) {
                        httpServletResponse.setContentType("application/octet-stream");
                        httpServletResponse.setHeader("Content-Disposition",
                                "attachment;filename=" + file.getName());
                        fileIn = new FileInputStream(file);
                        out = httpServletResponse.getOutputStream();
                        byte[] outputByte = new byte[(int) file.length()];
                        while (fileIn.read(outputByte, 0, (int) file.length()) != -1) {
                            out.write(outputByte, 0, (int) file.length());
                        }
                        httpServletResponse.getOutputStream().flush();
                    } else {
                        httpServletResponse.setStatus(404);
                    }
                } else {
                    httpServletResponse.setStatus(404);
                }
            } catch (IOException e) {
                logger.error("Unable to download the file");
                e.printStackTrace();
            } finally {
                try {
                    if (fileIn != null) {
                        fileIn.close();
                    }
                    if (out != null) {
                        out.flush();
                        out.close();
                    }
                } catch (IOException e) {
                    logger.warn("Problem during closing of the streams");
                    e.printStackTrace();
                }
            }
        }
    }
}