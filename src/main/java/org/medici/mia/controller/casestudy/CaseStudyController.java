package org.medici.mia.controller.casestudy;

import com.github.underscore.Predicate;
import com.github.underscore.U;
import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.UserProfileJson;
import org.medici.mia.common.json.casestudy.*;
import org.medici.mia.domain.*;
import org.medici.mia.service.casestudy.CaseStudyService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
@RequestMapping("/case_study")
public class CaseStudyController {

    @Autowired
    private UserService userService;

    @Autowired
    private CaseStudyService caseStudyService;

    @RequestMapping(value = "/authorize/{id}", method = RequestMethod.GET)
    public @ResponseBody
    GenericResponseDataJson authorizeNotebook(
            @PathVariable(value = "id") Integer id,
            HttpServletResponse httpServletResponse
    ) {
        GenericResponseDataJson<HashMap<String, Object>> response = new GenericResponseDataJson<HashMap<String, Object>>();

        User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        HashMap<String, Object> json = new HashMap<String, Object>();

        CaseStudy caseStudy = caseStudyService.findCaseStudy(id);
        if(caseStudy==null) {
            response.setStatus("error");
            response.setMessage("Case study with id = " + id + " does not exist or it has been deleted");
            return response;
        }
        if(!caseStudyService.validateUser(caseStudy)){
            httpServletResponse.setStatus(httpServletResponse.SC_UNAUTHORIZED);
            return null;
        }

        json.put("userName", user.getAccount());
        json.put("firstName", user.getFirstName());
        json.put("lastName", user.getLastName());
        json.put("title", caseStudy.getTitle());

        response.setStatus("success");
        response.setData(json);
        response.setMessage("Successfully authorized user");
        return response;
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody GenericResponseDataJson getCaseStudy(
            @PathVariable(value = "id") Integer id,
            HttpServletResponse httpServletResponse
    ) {
        GenericResponseDataJson<CaseStudyJson> response = new GenericResponseDataJson<CaseStudyJson>();
        CaseStudy cs = caseStudyService.findCaseStudy(id);
        if(cs == null){
            response.setMessage("Case study with id = "+id+" was not found");
            response.setStatus("error");
            return response;
        }
        if(!caseStudyService.validateUser(cs)){
            response.setMessage("You do not have an access to view this Case Study");
            response.setStatus("error");
            return response;
        }
        CaseStudyJson json = caseStudyService.getCaseStudyById(id);

        response.setData(json);
        response.setMessage("Found 1 case study with id = "+id);
        response.setStatus("Success");
        return response;
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody GenericResponseDataJson getCaseStudiesByUser(
            HttpServletResponse httpServletResponse
    ) {
        User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        GenericResponseDataJson<List<CaseStudyJson>> response = new GenericResponseDataJson<List<CaseStudyJson>>();
        List<CaseStudyJson> jsons = caseStudyService.getCaseStudiesByUser(user);
        response.setData(jsons);
        response.setStatus("success");
        response.setMessage("Found "+jsons.size()+" records for user `"+user.getAccount()+"`");
        return response;
    }

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    GenericResponseDataJson createCaseStudy(
        @RequestBody CreateCaseStudyJson json,
        HttpServletResponse httpServletResponse
    ) {
        GenericResponseDataJson<CaseStudyJson> response = new GenericResponseDataJson<CaseStudyJson>();
        response.setStatus("error");
        if(json.getTitle() == null || json.getTitle().isEmpty()){
            response.setMessage("Title can't be empty");
            return response;
        }
        if(json.getTitle().length() > 100){
            response.setMessage("Title can't be longer than 100 characters");
            return response;
        }
        if(json.getDescription() == null || json.getDescription().isEmpty()){
            response.setMessage("Description can't be empty");
            return response;
        }
        if(json.getDescription().length() > 500){
            response.setMessage("Description can't be longer than 500 characters");
            return response;
        }
        if(json.getNotes() != null && json.getNotes().length() > 500){
            response.setMessage("Notes can't be longer than 500 characters");
            return response;
        }
        response.setData(caseStudyService.createCaseStudy(json));
        response.setStatus("success");
        response.setMessage("Project with id = "+response.getData().getId()+" was successfully created");
        return response;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody
    GenericResponseJson deleteCaseStudy(
            @PathVariable("id") Integer id,
            HttpServletResponse httpServletResponse
    ) {
        GenericResponseJson response = new GenericResponseJson();
        CaseStudy caseStudy = caseStudyService.findCaseStudy(id);
        if(caseStudy == null){
            response.setMessage("Project with id = "+id+" was not found");
            response.setStatus("error");
            return response;
        }
        if(!caseStudyService.validateUserForDelete(caseStudy)){
            response.setMessage("You don't have the user rights to delete this project");
            response.setStatus("error");
            return response;
        }
        if(!caseStudyService.deleteCaseStudy(caseStudy)){
            response.setMessage("Project with id = "+id+" was not deleted");
            response.setStatus("error");
            return response;
        } else {
            httpServletResponse.setStatus(HttpServletResponse.SC_NO_CONTENT);
            return null;
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public @ResponseBody
    GenericResponseJson modifyCaseStudy(
            @PathVariable("id") Integer id,
            @RequestBody EditCaseStudyJson modifyJson,
            HttpServletResponse httpServletResponse
    ) {
        GenericResponseDataJson<CaseStudyJson> response = new GenericResponseDataJson<CaseStudyJson>();
        User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        CaseStudy caseStudy = caseStudyService.findCaseStudy(id);
        if(caseStudy == null){
            response.setMessage("Case Study with id = "+id+" was not found");
            response.setStatus("error");
            return response;
        }
        if(!caseStudyService.validateUserForDelete(caseStudy)){
            response.setMessage("You don't have the user rights to edit this project");
            response.setStatus("error");
            return response;
        }
        if(modifyJson.getTitle() == null || modifyJson.getDescription() == null){
            httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        if(modifyJson.getTitle().isEmpty() || modifyJson.getDescription().isEmpty()){
            httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        if(modifyJson.getTitle().length() > 100){
            response.setMessage("Title can't be longer than 100 characters");
            return response;
        }
        if(modifyJson.getDescription().length() > 500){
            response.setMessage("Description can't be longer than 500 characters");
            return response;
        }
        if(modifyJson.getNotes() != null && modifyJson.getNotes().length() > 500){
            response.setMessage("Notes can't be longer than 500 characters");
            return response;
        }
        CaseStudyJson json = caseStudyService.modifyCaseStudy(caseStudy, modifyJson);
        if(json == null){
            response.setMessage("Error when editing project with id = "+id);
            response.setStatus("error");
            return response;
        }
        response.setMessage("Successfully edited project with id = "+id);
        response.setStatus("success");
        response.setData(json);
        return response;
    }

    @RequestMapping(value = "/{id}/user_group", method = RequestMethod.GET)
    public @ResponseBody
    GenericResponseJson getUserGroup(
            @PathVariable("id") Integer id,
            HttpServletResponse httpServletResponse
    ) {
        GenericResponseDataJson<List<UserProfileJson>> response = new GenericResponseDataJson<List<UserProfileJson>>();
        CaseStudy caseStudy = caseStudyService.findCaseStudy(id);
        if(caseStudy == null){
            response.setMessage("project with id = "+id+" was not found");
            response.setStatus("error");
            return response;
        }
        if(!caseStudyService.validateUser(caseStudy)){
            response.setMessage("You don't have the user rights view user group in this project");
            response.setStatus("error");
            return response;
        }

        List<UserProfileJson> jsons = caseStudyService.getUserGroup(caseStudy);
        response.setMessage("Found "+jsons.size()+" users in project with id = "+id);
        response.setStatus("success");
        response.setData(jsons);
        return response;
    }

//    @RequestMapping(value = "/{id}/user_group", method = RequestMethod.POST)
//    public @ResponseBody
//    GenericResponseJson addUserToGroup(
//            @PathVariable("id") Integer id,
//            @RequestBody HashMap json,
//            HttpServletResponse httpServletResponse
//    ) {
//        GenericResponseDataJson response = new GenericResponseDataJson();
//        CaseStudy caseStudy = caseStudyService.findCaseStudy(id);
//        if(caseStudy == null){
//            response.setMessage("Case Study with id = "+id+" was not found");
//            response.setStatus("error");
//            return response;
//        }
//        User currentUser = userService.findUser(
//                ((UserDetails) SecurityContextHolder.getContext()
//                        .getAuthentication().getPrincipal()).getUsername());
//        if(!userService.isAccountAutorizedForDelete(currentUser.getAccount()) || !caseStudy.getCreatedBy().getAccount().equals(currentUser.getAccount())){
//            response.setMessage("User does not have permission for adding users to case study user group");
//            response.setStatus("error");
//            return response;
//        }
//        if(json != null && json.containsKey("account")){
//            final String account = (String) json.get("account");
//            User user = userService.findUser(account);
//            if(user == null){
//                response.setMessage("User with account = " + account + " was not found");
//                response.setStatus("error");
//                return response;
//            }
//            if(U.any(caseStudy.getUserGroup(), new Predicate<User>() {
//                @Override
//                public boolean test(User user) {
//                    return user.getAccount().equals(account);
//                }
//            })){
//                response.setMessage("This user is already exists in user group");
//                response.setStatus("error");
//                return response;
//            }
//            List<UserProfileJson> userJsons = caseStudyService.addUserToGroup(caseStudy, user);
//            if(userJsons == null || userJsons.isEmpty()){
//                response.setMessage("Error happened when tried to add user with account = " + account);
//                response.setStatus("error");
//                return response;
//            }
//            response.setMessage("Succesfully added user with account = "+account+" to usergroup");
//            response.setStatus("success");
//            response.setData(userJsons);
//            return response;
//        } else {
//            httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//            return null;
//        }
//    }

    @RequestMapping(value = "/{id}/user_group", method = RequestMethod.POST)
    public @ResponseBody
    GenericResponseJson updateUserGroup(
            @PathVariable("id") Integer id,
            @RequestBody List<String> accounts,
            HttpServletResponse httpServletResponse
    ) {
        GenericResponseDataJson<List<UserProfileJson>> response = new GenericResponseDataJson<List<UserProfileJson>>();
        response.setData(new ArrayList<UserProfileJson>());
        CaseStudy caseStudy = caseStudyService.findCaseStudy(id);
        if(caseStudy == null){
            response.setMessage("Case Study with id = "+id+" was not found");
            response.setStatus("error");
            return response;
        }
        if(!caseStudyService.validateUserForDelete(caseStudy)){
            response.setMessage("You don't have the user rights to update the user group in this project");
            response.setStatus("error");
            return response;
        }
        if(accounts == null || accounts.isEmpty() || accounts.contains(null)){
            httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        String notFoundUser = null;
        for(String account : accounts){
            if(userService.findUser(account) == null){
                notFoundUser = account;
                break;
            }
        }
        if(notFoundUser != null){
            response.setMessage("User with account = "+notFoundUser+" is not found");
            response.setStatus("error");
            return response;
        }
        List<UserProfileJson> users = caseStudyService.updateUserGroup(caseStudy, accounts);
        response.setData(users);
        response.setMessage("Successfully updated user group");
        response.setStatus("success");
        return response;
    }

//    @RequestMapping(value = "/{id}/user_group", method = RequestMethod.DELETE)
//    public @ResponseBody
//    GenericResponseJson removeUserFromGroup(
//            @PathVariable("id") Integer id,
//            @RequestBody HashMap json,
//            HttpServletResponse httpServletResponse
//    ) {
//        GenericResponseDataJson response = new GenericResponseDataJson();
//        CaseStudy caseStudy = caseStudyService.findCaseStudy(id);
//        if(caseStudy == null){
//            response.setMessage("Case Study with id = "+id+" was not found");
//            response.setStatus("error");
//            return response;
//        }
//        if(json != null && json.containsKey("account")){
//            final String account = (String) json.get("account");
//            User user = userService.findUser(account);
//            if(user == null){
//                response.setMessage("User with account = " + account + " was not found");
//                response.setStatus("error");
//                return response;
//            }
//            if(!caseStudyService.validateUserForDelete(caseStudy)){
//                response.setMessage("User does not have permission for deleting users from case study user group");
//                response.setStatus("error");
//                return response;
//            }
//            if(!U.any(caseStudy.getUserGroup(), new Predicate<User>() {
//                @Override
//                public boolean test(User user) {
//                    return user.getAccount().equals(account);
//                }
//            })){
//                response.setMessage("This user does not exist in user group");
//                response.setStatus("error");
//                return response;
//            }
//            List<UserProfileJson> userJsons = caseStudyService.removeUserFromGroup(caseStudy, user);
//            if(userJsons == null || userJsons.isEmpty()){
//                response.setMessage("Error happened when tried to remove user with account = " + account);
//                response.setStatus("error");
//                return response;
//            }
//            httpServletResponse.setStatus(HttpServletResponse.SC_NO_CONTENT);
//            return null;
//        } else {
//            httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//            return null;
//        }
//    }

    @RequestMapping(value = "/{id}/folders", method = RequestMethod.GET)
    public @ResponseBody
    HashMap<String, Object> findFoldersInCS(
            @PathVariable("id") Integer id,
            HttpServletResponse httpServletResponse,
            HttpServletRequest httpServletRequest
    ) {
        HashMap<String, Object> response = new HashMap<String, Object>();
        CaseStudy caseStudy = caseStudyService.findCaseStudy(id);
        if(caseStudy == null){
            response.put("message", "Project with id = "+id+" was not found");
            response.put("status","error");
            response.put("data", new ArrayList<CaseStudyFolderJson>());
            return response;
        }
        if(!caseStudyService.validateUser(caseStudy)){
            response.put("message","You don't have the user rights to view folders in this project");
            response.put("status","error");
            response.put("data", new ArrayList<CaseStudyFolderJson>());
            return response;
        }
        List<CaseStudyFolderJson> jsons = caseStudyService.findFoldersByCaseStudyId(id);
        response.put("message","Found "+jsons.size()+" folders in project with id = "+id);
        response.put("status","success");
        response.put("data",jsons);
        response.put("token", httpServletRequest.getCookies().length == 0 ? null : httpServletRequest.getCookies()[0].getValue());
        return response;
    }

    @RequestMapping(value = "/{id}/folders", method = RequestMethod.POST)
    public @ResponseBody
    GenericResponseJson addFolderToCS(
            @PathVariable("id") Integer id,
            @RequestBody CreateCaseStudyFolderJson folderJson,
            HttpServletResponse httpServletResponse
    ) {
        GenericResponseDataJson<CaseStudyFolderJson> response = new GenericResponseDataJson<CaseStudyFolderJson>();
        CaseStudy caseStudy = caseStudyService.findCaseStudy(id);
        if(caseStudy == null){
            response.setMessage("project with id = "+id+" was not found");
            response.setStatus("error");
            return response;
        }
        if(!caseStudyService.validateUser(caseStudy)){
            response.setMessage("You don't have the user rights to add folders in this project");
            response.setStatus("error");
            return response;
        }
        if(folderJson.getTitle() == null || folderJson.getTitle().isEmpty()){
            response.setMessage("Title cannot be empty");
            response.setStatus("error");
            return response;
        }
        final String title = folderJson.getTitle();
        if(U.any(caseStudy.getFolders(), new Predicate<CaseStudyFolder>() {
            @Override
            public boolean test(CaseStudyFolder f) {
                return f.getTitle().equals(title);
            }
        })){
            response.setMessage("Folder with title `"+title+"` is already exist in project with id = "+id);
            response.setStatus("error");
            return response;
        }
        if(folderJson.getTitle().length() > 100){
            response.setMessage("Title can't be longer than 100 characters");
            response.setStatus("error");
            return response;
        }
        CaseStudyFolderJson json = caseStudyService.addFolder(caseStudy, folderJson);
        response.setMessage("Successfully added folder to project with id = "+id);
        response.setStatus("success");
        response.setData(json);
        return response;
    }

    @RequestMapping(value = "/{id}/folders/{folderId}", method = RequestMethod.DELETE)
    public @ResponseBody
    GenericResponseJson deleteFolderFromCS(
            @PathVariable("id") Integer id,
            @PathVariable("folderId") final Integer folderId,
            HttpServletResponse httpServletResponse
    ) {
        GenericResponseJson response = new GenericResponseJson();
        CaseStudy caseStudy = caseStudyService.findCaseStudy(id);
        if(caseStudy == null){
            response.setMessage("Project with id = "+id+" was not found");
            response.setStatus("error");
            return response;
        }
        CaseStudyFolder folder = null;
        for(CaseStudyFolder f: caseStudy.getFolders()){
            if(f.getId().equals(folderId)){
                folder = f;
                break;
            }
        }
        if(folder == null){
            response.setMessage("This folder does not exist in Project with id = "+id);
            response.setStatus("error");
            return response;
        }
        if(!caseStudyService.validateUserForDelete(folder)){
            response.setMessage("You don't have the user rights to delete this folder");
            response.setStatus("error");
            return response;
        }
        if(caseStudyService.removeFolder(caseStudy, folder)) {
            httpServletResponse.setStatus(httpServletResponse.SC_NO_CONTENT);
            return null;
        } else {
            response.setMessage("Error occured when tried to delete a folder with id = "+folderId+" from project with id = "+id);
            response.setStatus("error");
            return response;
        }
    }

    @RequestMapping(value = "/{id}/folders/{folderId}", method = RequestMethod.PUT)
    public @ResponseBody
    GenericResponseJson modifyFolderInCS(
            @PathVariable("id") Integer id,
            @PathVariable("folderId") final Integer folderId,
            @RequestBody EditCaseStudyJson modifyJson,
            HttpServletResponse httpServletResponse
    ) {
        GenericResponseDataJson<CaseStudyFolderJson> response = new GenericResponseDataJson<CaseStudyFolderJson>();
        CaseStudy caseStudy = caseStudyService.findCaseStudy(id);
        if(caseStudy == null){
            response.setMessage("Project with id = "+id+" was not found");
            response.setStatus("error");
            return response;
        }
        CaseStudyFolder folder = null;
        for(CaseStudyFolder f: caseStudy.getFolders()){
            if(f.getId().equals(folderId)){
                folder = f;
                break;
            }
        }
        if(folder == null){
            response.setMessage("This folder does not exist in project with id = "+id);
            response.setStatus("error");
            return response;
        }
        if(!caseStudyService.validateUser(caseStudy)){
            response.setMessage("You do not have an access to edit this folder");
            response.setStatus("error");
            return response;
        }
        if(modifyJson.getTitle() == null || modifyJson.getNotes() != null || modifyJson.getDescription() != null){
            httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        if(modifyJson.getTitle().isEmpty()){
            response.setMessage("Title cannot be empty");
            response.setStatus("error");
            return response;
        }
        final String title = modifyJson.getTitle();
        if(title.isEmpty()){
            response.setMessage("Title cannot be empty");
            response.setStatus("error");
            return response;
        }
        if(U.any(caseStudy.getFolders(), new Predicate<CaseStudyFolder>() {
            @Override
            public boolean test(CaseStudyFolder f) {
                return f.getTitle().equals(title);
            }
        })){
            response.setMessage("Folder with title `"+title+"` is already exist in project with id = "+id);
            response.setStatus("error");
            return response;
        }
        if(modifyJson.getTitle().length() > 100){
            response.setMessage("Title can't be longer than 100 characters");
            response.setStatus("error");
            return response;
        }
        CaseStudyFolderJson json = caseStudyService.editFolder(folder, modifyJson);
        response.setMessage("Successfully edited folder in project with id = "+id);
        response.setStatus("success");
        response.setData(json);
        return response;
    }

    @RequestMapping(value = "/{id}/folders/{folderId}/items", method = RequestMethod.GET)
    public @ResponseBody
    GenericResponseJson getItemsFromFolder(
            @PathVariable("id") Integer id,
            @PathVariable("folderId") final Integer folderId,
            HttpServletResponse httpServletResponse
    ) {
        GenericResponseDataJson<List<CaseStudyItemJson>> response = new GenericResponseDataJson<List<CaseStudyItemJson>>();
        response.setData(new ArrayList<CaseStudyItemJson>());
        CaseStudy caseStudy = caseStudyService.findCaseStudy(id);
        if(caseStudy == null){
            response.setMessage("Project with id = "+id+" was not found");
            response.setStatus("error");
            return response;
        }
        if(!caseStudyService.validateUser(caseStudy)){
            response.setMessage("You don't have the user rights to view items in this project");
            response.setStatus("error");
            return response;
        }
        CaseStudyFolder folder = null;
        for(CaseStudyFolder f: caseStudy.getFolders()){
            if(f.getId().equals(folderId)){
                folder = f;
                break;
            }
        }
        if(folder == null){
            response.setMessage("This folder does not exist in project with id = "+id);
            response.setStatus("error");
            return response;
        }
        List<CaseStudyItemJson> jsons = caseStudyService.findItemsByFolderId(folderId);
        response.setMessage("Found "+jsons.size()+" items in folder with id = "+folderId+" from project with id = "+id);
        response.setStatus("success");
        response.setData(jsons);
        return response;
    }

    @RequestMapping(value = "/{id}/folders/{folderId}/items/upload",
            consumes = "multipart/form-data",
            method = RequestMethod.POST
    )
    public @ResponseBody
    GenericResponseJson addItemToFolder(
            @PathVariable("id") Integer id,
            @PathVariable("folderId") Integer folderId,
            @RequestParam(value = "title", required = false) String title,
            @RequestParam("file") MultipartFile file,
            HttpServletResponse httpServletResponse,
            HttpServletRequest httpServletRequest
    ) {
        GenericResponseDataJson<CaseStudyItemJson> response = new GenericResponseDataJson<CaseStudyItemJson>();
        response.setStatus("error");
        CaseStudy caseStudy = caseStudyService.findCaseStudy(id);
        if(caseStudy == null){
            response.setMessage("Project with id = "+id+" does not exist");
            return response;
        }
        CaseStudyFolder folder = null;
        for(CaseStudyFolder f: caseStudy.getFolders()){
            if(f.getId().equals(folderId)){
                folder = f;
                break;
            }
        }
        if(!caseStudyService.validateUser(caseStudy)){
            response.setMessage("You don't have the user rights to add items in this project");
            response.setStatus("error");
            return response;
        }
        if(folder == null){
            response.setMessage("This folder does not exist in project with id = "+id);
            response.setStatus("error");
            return response;
        }
        if(file == null || file.isEmpty()){
            response.setMessage("Cannot upload file: file not found");
            return response;
        }
        if(title == null || title.isEmpty()){
            response.setMessage("Title cannot be empty");
            return response;
        }
        if(file.getOriginalFilename().length() > 100){
            response.setMessage("File name is too long (max. 100)");
            return response;
        }
        if(file.getSize() / 1024 / 1024 > 100L){
            response.setMessage("This file is too big. Max allowed size is 10 MB");
            return response;
        }
        Pattern p = Pattern.compile("(?i)^.*\\.(jpg|tif|tiff|pdf|jpeg|png)$");
        Matcher m = p.matcher(file.getOriginalFilename());
        if(!m.find()){
            response.setMessage("File extension not supported (accepted: jpg, jpeg, tif, tiff, pdf, png)");
            return response;
        }
        CaseStudyItemJson json = caseStudyService.uploadFile(title, file, folder);
        response.setMessage("Successfully uploaded file to the folder with id = "+folderId+" in project with id = "+id);
        response.setStatus("success");
        response.setData(json);
        return response;
    }

    @RequestMapping(value = "/{id}/folders/{folderId}/items", method = RequestMethod.POST)
    public @ResponseBody
    GenericResponseJson addItemToFolder(
            @PathVariable("id") Integer id,
            @PathVariable("folderId") Integer folderId,
            @RequestBody final CreateCaseStudyItemJson itemJson,
            HttpServletResponse httpServletResponse
    ) {
        GenericResponseDataJson<CaseStudyItemJson> response = new GenericResponseDataJson<CaseStudyItemJson>();
        CaseStudy caseStudy = caseStudyService.findCaseStudy(id);
        if(caseStudy == null){
            response.setMessage("Project with id = "+id+" was not found");
            response.setStatus("error");
            return response;
        }
        if(!caseStudyService.validateUser(caseStudy)){
            response.setMessage("You don't have the user rights to add items in this project");
            response.setStatus("error");
            return response;
        }
        if(itemJson.getEntityType() == null){
            httpServletResponse.setStatus(httpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        try{
            CaseStudyItem.EntityType.valueOf(itemJson.getEntityType());
        } catch (IllegalArgumentException e) {
            httpServletResponse.setStatus(httpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        if(!CaseStudyItem.EntityType.valueOf(itemJson.getEntityType()).equals(CaseStudyItem.EntityType.LINK) && itemJson.getEntityId() == null){
            httpServletResponse.setStatus(httpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        if(CaseStudyItem.EntityType.valueOf(itemJson.getEntityType()).equals(CaseStudyItem.EntityType.LINK)
                && (itemJson.getLink() == null || itemJson.getLink().isEmpty())
                && (itemJson.getTitle() == null || itemJson.getTitle().isEmpty())){
            httpServletResponse.setStatus(httpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        if(itemJson.getTitle() != null && itemJson.getTitle().length() > 100){
            response.setMessage("Title can't be longer than 100 characters");
            response.setStatus("error");
            return response;
        }
        if(CaseStudyItem.EntityType.valueOf(itemJson.getEntityType()).equals(CaseStudyItem.EntityType.LINK) && (itemJson.getLink() == null || itemJson.getLink().isEmpty())){
            response.setMessage("Link cannot be empty");
            response.setStatus("error");
            return response;
        }
        if(!CaseStudyItem.EntityType.valueOf(itemJson.getEntityType()).equals(CaseStudyItem.EntityType.LINK) && (itemJson.getLink() != null)){
            response.setMessage("Cannot add link to the "+itemJson.getEntityType()+" type entity");
            response.setStatus("error");
            return response;
        }
        if(CaseStudyItem.EntityType.valueOf(itemJson.getEntityType()).equals(CaseStudyItem.EntityType.LINK) && itemJson.getLink() != null){
            if(itemJson.getLink().length() > 500) {
                response.setMessage("Link can't be longer than 500 characters");
                response.setStatus("error");
                return response;
            }
            Pattern p = Pattern.compile("^(http[s]?:\\/\\/){0,1}(www\\.){0,1}[a-zA-Z0-9\\.\\-]+\\.[a-zA-Z]{2,5}[\\.]{0,1}");
            Matcher m = p.matcher(itemJson.getLink());
            if(!m.find()){
                response.setMessage("Supplied link does not have a pattern of a URL");
                response.setStatus("error");
                return response;
            }
        }
        CaseStudyFolder folder = null;
        for(CaseStudyFolder f: caseStudy.getFolders()){
            if(f.getId().equals(folderId)){
                folder = f;
                break;
            }
        }
        if(folder == null){
            response.setMessage("This folder does not exist in project with id = "+id);
            response.setStatus("error");
            return response;
        }
        if(U.any(folder.getItems(), new Predicate<CaseStudyItem>() {
            @Override
            public boolean test(CaseStudyItem item) {
                if(item.getEntityId() == null) return false;
                return item.getEntityId().equals(itemJson.getEntityId())
                        && item.getEntityType().equals(CaseStudyItem.EntityType.valueOf(itemJson.getEntityType()));
            }
        })){
            response.setStatus("error");
//            response.setMessage(itemJson.getEntityType()+" entity with id = "+itemJson.getEntityId()+" already exists in folder with id = "+folderId);
            response.setMessage("This item is already in your project folder");
            return response;
        }
        CaseStudyItemJson json = caseStudyService.addItem(folder, itemJson);
        if(json == null){
            response.setMessage("Cannot find "+itemJson.getEntityType()+" entity with id = "+itemJson.getEntityId());
            response.setStatus("error");
            return response;
        }
        response.setMessage("Successfully added item to folder with id = "+folderId+" from project with id = "+id);
        response.setStatus("success");
        response.setData(json);
        return response;
    }

    @RequestMapping(value = "/{id}/folders/{folderId}/items/{itemId}", method = RequestMethod.PUT)
    public @ResponseBody
    GenericResponseJson editItemInFolder(
            @PathVariable("id") Integer id,
            @PathVariable("folderId") Integer folderId,
            @PathVariable("itemId") Integer itemId,
            @RequestBody EditCaseStudyItemJson modifyJson,
            HttpServletResponse httpServletResponse
    ) {
        GenericResponseDataJson<CaseStudyItemJson> response = new GenericResponseDataJson<CaseStudyItemJson>();
        CaseStudy caseStudy = caseStudyService.findCaseStudy(id);
        if(caseStudy == null){
            response.setMessage("Project with id = "+id+" was not found");
            response.setStatus("error");
            return response;
        }
        CaseStudyFolder folder = null;
        for(CaseStudyFolder f: caseStudy.getFolders()){
            if(f.getId().equals(folderId)){
                folder = f;
                break;
            }
        }
        if(folder == null){
            response.setMessage("This folder does not exist in project with id = "+id);
            response.setStatus("error");
            return response;
        }
        CaseStudyItem item = null;
        for(CaseStudyItem i: folder.getItems()){
            if(i.getId().equals(itemId)){
                item = i;
                break;
            }
        }
        if(item == null){
            response.setMessage("This item does not exist in folder with id = "+folderId);
            response.setStatus("error");
            return response;
        }
        if(!caseStudyService.validateUser(caseStudy)){
            response.setMessage("You do not have an access to edit this item");
            response.setStatus("error");
            return response;
        }
        if(modifyJson.getLink() == null && modifyJson.getTitle() == null){
            httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        if(!item.getEntityType().equals(CaseStudyItem.EntityType.LINK) && modifyJson.getLink() != null && !modifyJson.getLink().isEmpty()){
            response.setMessage("Cannot edit link field in the "+item.getEntityType().toString()+" type entity");
            response.setStatus("error");
            return response;
        }
        if(modifyJson.getTitle() != null){
            if(modifyJson.getTitle().isEmpty()) {
                response.setMessage("Title cannot be empty");
                response.setStatus("error");
                return response;
            }
            if(modifyJson.getTitle().length() > 100){
                response.setMessage("Title can't be longer than 100 characters");
                response.setStatus("error");
                return response;
            }
        }
        if(modifyJson.getLink() != null){
            if(modifyJson.getLink().isEmpty()) {
                response.setMessage("Link cannot be empty");
                response.setStatus("error");
                return response;
            }
            if(modifyJson.getLink().length() > 500){
                    response.setMessage("Link can't be longer than 500 characters");
                    response.setStatus("error");
                    return response;
            }
            Pattern p = Pattern.compile("^(http[s]?:\\/\\/){0,1}(www\\.){0,1}[a-zA-Z0-9\\.\\-]+\\.[a-zA-Z]{2,5}[\\.]{0,1}");
            Matcher m = p.matcher(modifyJson.getLink());
            if(!m.find()){
                response.setMessage("Supplied link does not have a pattern of a URL");
                response.setStatus("error");
                return response;
            }
        }


        CaseStudyItemJson json = caseStudyService.editItem(item, modifyJson);
        response.setMessage("Successfully edited item to folder with id = "+folderId+" from project with id = "+id);
        response.setStatus("success");
        response.setData(json);
        return response;
    }

    @RequestMapping(value = "/{id}/folders/{folderId}/items/{itemId}", method = RequestMethod.DELETE)
    public @ResponseBody
    GenericResponseJson deleteItemFromFolder(
            @PathVariable("id") Integer id,
            @PathVariable("folderId") final Integer folderId,
            @PathVariable("itemId") Integer itemId,
            HttpServletResponse httpServletResponse
    ) {
        GenericResponseJson response = new GenericResponseJson();
        CaseStudy caseStudy = caseStudyService.findCaseStudy(id);
        if(caseStudy == null){
            response.setMessage("Project with id = "+id+" was not found");
            response.setStatus("error");
            return response;
        }
        CaseStudyFolder folder = null;
        for(CaseStudyFolder f: caseStudy.getFolders()){
            if(f.getId().equals(folderId)){
                folder = f;
                break;
            }
        }
        if(folder == null){
            response.setMessage("This folder does not exist in project with id = "+id);
            response.setStatus("error");
            return response;
        }
        CaseStudyItem item = null;
        for(CaseStudyItem i: folder.getItems()){
            if(i.getId().equals(itemId)){
                item = i;
                break;
            }
        }
        if(item == null){
            response.setMessage("This item does not exist in folder with id = "+folderId);
            response.setStatus("error");
            return response;
        }
        if(!caseStudyService.validateUserForDelete(item)){
            response.setMessage("You do not have an access to delete this item");
            response.setStatus("error");
            return response;
        }
        if(caseStudyService.removeItem(folder, item)) {
            httpServletResponse.setStatus(httpServletResponse.SC_NO_CONTENT);
            return null;
        } else {
            response.setMessage("Error occured when tried to delete a project item with id = "+itemId+" from project with id = "+id);
            response.setStatus("error");
            return response;
        }
    }

    @RequestMapping(value = "/{id}/reorder", method = RequestMethod.POST)
    public @ResponseBody
    GenericResponseJson reorderEntities(
            @PathVariable("id") Integer id,
            @RequestBody ReorderCSJson reorderJson,
            HttpServletResponse httpServletResponse
    ) {
        GenericResponseDataJson<List<CaseStudyFolderJson>> resp = new GenericResponseDataJson<List<CaseStudyFolderJson>>();
        resp.setData(new ArrayList<CaseStudyFolderJson>());
        resp.setStatus("error");
        CaseStudy caseStudy = caseStudyService.findCaseStudy(id);
        if(caseStudy == null){
            resp.setMessage("Project with id = "+id+" was not found");
            return resp;
        }
        if(!caseStudyService.validateUser(caseStudy)){
            resp.setMessage("You do not have an access to reorder these entities");
            return resp;
        }
        if(reorderJson.getType() == null){
            resp.setMessage("Type of reorder is not specified");
            return resp;
        }
        if(!(reorderJson.getType().equals("FOLDER") || reorderJson.getType().equals("ITEM"))){
            resp.setMessage("Wrong type of reorder is specified");
            return resp;
        }
        if(reorderJson.getType().equals("ITEM") && reorderJson.getFolderId() == null){
            resp.setMessage("Field `folderId` is required for `ITEM` type reorder");
            return resp;
        }
        if(reorderJson.getType().equals("ITEM")){
            CaseStudyFolder folder = null;
            for(CaseStudyFolder f: caseStudy.getFolders()){
                if(f.getId().equals(reorderJson.getFolderId())){
                    folder = f;
                    break;
                }
            }
            if(folder == null) {
                resp.setMessage("Folder with id = " + reorderJson.getFolderId() + " was not found in this project");
                return resp;
            }
        }
        return caseStudyService.reorderEntities(caseStudy, reorderJson);
    }

    @RequestMapping(value = "/by_entity", method = RequestMethod.GET)
    public @ResponseBody
    GenericResponseJson checkEntityForDelete(
            @RequestParam("id") Integer id,
            @RequestParam("type") String type,
            HttpServletResponse httpServletResponse
    ) {
        GenericResponseDataJson<List<CaseStudyJson>> response = new GenericResponseDataJson<List<CaseStudyJson>>();
        response.setData(new ArrayList<CaseStudyJson>());
        if(type == null || type.isEmpty()){
            httpServletResponse.setStatus(httpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        try{
            CaseStudyItem.EntityType.valueOf(type.toUpperCase());
        } catch (IllegalArgumentException e) {
            httpServletResponse.setStatus(httpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        List<CaseStudyJson> jsons = caseStudyService.findAttachedCS(id, CaseStudyItem.EntityType.valueOf(type.toUpperCase()));
        if(jsons != null){
            response.setData(jsons);
        }
        response.setStatus("success");
        response.setMessage("Found "+response.getData().size()+" projects attached to entity");
        return response;
    }
}