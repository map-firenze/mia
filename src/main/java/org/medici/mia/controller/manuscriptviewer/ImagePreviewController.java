/*
 * ImagePreviewController.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.controller.manuscriptviewer;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.math.NumberUtils;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.domain.Image;
import org.medici.mia.domain.ImagePreview;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.manuscriptviewer.ImagePreviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * @author User
 *
 */
@Controller
@RequestMapping("/imagePreview")
public class ImagePreviewController {

	@Autowired
	private ImagePreviewService imagePreviewService;

	// url: http://localhost:8181/Mia/json/imagePreview/image/$uploadFileId
	@RequestMapping(value = "image/{uploadFileId}", method = RequestMethod.GET)
	public ModelAndView imagePreview(
			@PathVariable("uploadFileId") Integer uploadFileId,
			HttpSession httpSession) {
		Map<String, Object> model = new HashMap<String, Object>(0);
		Image image = new Image();
		try {

			ImagePreview imageP = getImagePreviewService().getImagePreview(
					uploadFileId);

			if (imageP != null) {
				httpSession.setAttribute("imagePreview", imageP);
				// this settings are if FE wants to user ModelAndView instead of
				// HttpSession
				image.setVolNum(imageP.getVolNum());
				image.setImageOrder(imageP.getImageOrder());
				image.setImageName(imageP.getRealPathTIF());
				image.setImageId(uploadFileId);
				
			}

			model.put("image", image);
			
			model.put("maxAnnotationQuestionNumber", NumberUtils.createInteger(
					ApplicationPropertyManager.getApplicationProperty("annotation.question.max.number")));
		} catch (ApplicationThrowable applicationThrowable) {
			model.put("applicationThrowable", applicationThrowable);
		}
		return new ModelAndView("mview/PreviewImageManuscriptViewerHtml", model);
	}

	public ImagePreviewService getImagePreviewService() {
		return imagePreviewService;
	}

	public void setImagePreviewService(ImagePreviewService imagePreviewService) {
		this.imagePreviewService = imagePreviewService;
	}

}
