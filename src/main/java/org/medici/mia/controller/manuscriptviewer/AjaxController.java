/*
 * AjaxController.java
 * 
 * Developed by Medici Archive Project (2010-2012).
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.controller.manuscriptviewer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.medici.mia.common.pagination.DocumentEntityExplorer;
import org.medici.mia.common.util.HtmlUtils;
import org.medici.mia.domain.Annotation;
import org.medici.mia.domain.AnnotationQuestion;
import org.medici.mia.domain.Document;
import org.medici.mia.domain.Image;
import org.medici.mia.domain.Image.ImageType;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.community.CommunityService;
import org.medici.mia.service.manuscriptviewer.ManuscriptViewerService;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * AJAX Controller for ManuscriptViewer.
 * 
 * @author Lorenzo Pasquinelli (<a
 *         href=mailto:l.pasquinelli@gmail.com>l.pasquinelli@gmail.com</a>)
 * @author Matteo Doni (<a
 *         href=mailto:donimatteo@gmail.com>donimatteo@gmail.com</a>)
 * @author Ronny Rianldi (<a
 *         href=mailto:rinaldi.ronny@gmail.com>rinaldi.ronny@gmail.com</a>)
 */
@Controller("ManuscriptViewerAjaxController")
public class AjaxController {
	@Autowired
	private CommunityService communityService;
	@Autowired
	private ManuscriptViewerService manuscriptViewerService;
	@Autowired
	private UserService userService;
	@Autowired
	private MiaDocumentService miaDocumentService;

	/**
	 * @return the communityService
	 */
	public CommunityService getCommunityService() {
		return communityService;
	}

	/**
	 * @param communityService
	 *            the communityService to set
	 */
	public void setCommunityService(CommunityService communityService) {
		this.communityService = communityService;
	}

	/**
	 * @return the manuscriptViewerService
	 */
	public ManuscriptViewerService getManuscriptViewerService() {
		return manuscriptViewerService;
	}

	/**
	 * @param manuscriptViewerService
	 *            the manuscriptViewerService to set
	 */
	public void setManuscriptViewerService(
			ManuscriptViewerService manuscriptViewerService) {
		this.manuscriptViewerService = manuscriptViewerService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService() {
		return userService;
	}

	/**
	 * @param userService
	 *            the userService to set
	 */
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/**
	 * This method is called from the client when the user try to jump to a
	 * folio. It checks if the folio details are correct.
	 * 
	 * @param volNum
	 *            the volume number
	 * @param volLetExt
	 *            the volume letter extension
	 * @param insertNum
	 *            the insert number
	 * @param insertLet
	 *            the insert extension
	 * @param folioNum
	 *            the folio number
	 * @param folioMod
	 *            the folio extension
	 * @return the data of the checking process
	 */
	@RequestMapping(value = { "/src/mview/CheckFolio.json" }, method = RequestMethod.GET)
	public ModelAndView checkFolio(
			@RequestParam(value = "volNum", required = false) Integer volNum,
			@RequestParam(value = "volLetExt", required = false) String volLetExt,
			@RequestParam(value = "insertNum", required = false) String insertNum,
			@RequestParam(value = "insertLet", required = false) String insertLet,
			@RequestParam(value = "folioNum", required = false) Integer folioNum,
			@RequestParam(value = "folioMod", required = false) String folioMod) {

		Map<String, Object> model = new HashMap<String, Object>(0);

		try {
			if (folioNum != null) {
				Image image = getManuscriptViewerService().findImage(
						volNum,
						volLetExt,
						ImageType.C,
						org.medici.mia.common.util.StringUtils
								.nullTrim(insertNum),
						org.medici.mia.common.util.StringUtils
								.nullTrim(insertLet),
						folioNum,
						org.medici.mia.common.util.StringUtils
								.nullTrim(folioMod));
				model.put("folioOK", image != null);
			} else {
				model.put("error", "error.manuscriptviewer.incorrectfolio");
			}
		} catch (ApplicationThrowable e) {
			model.put("error", e.getApplicationError().toString());
		}
		return new ModelAndView("responseOK", model);
	}

	/**
	 * This method is called from the client when the user try to jump to a
	 * folio. It checks if the insert details are correct.
	 * 
	 * @param volNum
	 *            the volume number
	 * @param volLetExt
	 *            the volume letter extension
	 * @param insertNum
	 *            the insert number
	 * @param insertLet
	 *            the insert extension
	 * @return the data of the checking process
	 */
	@RequestMapping(value = { "/src/mview/CheckInsert.json" }, method = RequestMethod.GET)
	public ModelAndView checkInsert(
			@RequestParam(value = "volNum", required = false) Integer volNum,
			@RequestParam(value = "volLetExt", required = false) String volLetExt,
			@RequestParam(value = "insertNum", required = false) String insertNum,
			@RequestParam(value = "insertLet", required = false) String insertLet) {

		Map<String, Object> model = new HashMap<String, Object>(0);

		try {
			if (volNum != null) {
				Boolean insertOK = getManuscriptViewerService().checkInsert(
						volNum,
						volLetExt,
						org.medici.mia.common.util.StringUtils
								.nullTrim(insertNum),
						org.medici.mia.common.util.StringUtils
								.nullTrim(insertLet));
				model.put("insertOK", insertOK);
			} else {
				model.put("error", "error.manuscriptviewer.incorrectvolume");
			}
		} catch (ApplicationThrowable e) {
			model.put("error", e.getApplicationError().toString());
		}
		return new ModelAndView("responseOK", model);
	}

	/**
	 * @param entryId
	 * @param volNum
	 * @param volLetExt
	 * @param imageType
	 * @param imageProgTypeNum
	 * @param imageOrder
	 * @param imageName
	 * @param total
	 * @param totalRubricario
	 * @param totalCarta
	 * @param totalAppendix
	 * @param totalOther
	 * @param totalGuardia
	 * @param modeEdit
	 * @param request
	 * @return
	 */
	@RequestMapping(value = { "/src/mview/GetLinkedDocument.json",
			"/de/mview/GetLinkedDocument.json" }, method = RequestMethod.GET)
	public ModelAndView findLinkedDocument(
			@RequestParam(value = "entryId", required = false) Integer entryId,
			@RequestParam(value = "volNum", required = false) Integer volNum,
			@RequestParam(value = "volLetExt", required = false) String volLetExt,
			@RequestParam(value = "imageType", required = false) String imageType,
			@RequestParam(value = "imageProgTypeNum", required = false) Integer imageProgTypeNum,
			@RequestParam(value = "imageOrder", required = false) Integer imageOrder,
			@RequestParam(value = "imageName", required = false) String imageName,
			@RequestParam(value = "total", required = false) Long total,
			@RequestParam(value = "totalRubricario", required = false) Long totalRubricario,
			@RequestParam(value = "totalCarta", required = false) Long totalCarta,
			@RequestParam(value = "totalAppendix", required = false) Long totalAppendix,
			@RequestParam(value = "totalOther", required = false) Long totalOther,
			@RequestParam(value = "totalGuardia", required = false) Long totalGuardia,
			@RequestParam(value = "modeEdit", required = false) Boolean modeEdit,
			HttpServletRequest request) {
		Map<String, Object> model = new HashMap<String, Object>(0);

		try {
			Integer documentId = null;
			List<Document> linkedDocumentOnStartFolio = null;
			List<Document> linkedDocumentOnTranscribeFolio = null;
			boolean isExtract = false;
			Image image = new Image();

			int intersectionSize = 0;
			int unionSize = 0;

			// if (entryId != null) {
			// documentId = entryId;
			// } else {
			if (org.medici.mia.common.util.StringUtils.safeTrim(imageType) != null
					&& !"C".equals(imageType.trim())) {
				model.put("error", "wrongType");
			} else {
				// We extract image
				image = getManuscriptViewerService().findVolumeImage(
						null,
						volNum,
						volLetExt,
						(imageType != null) ? ImageType.valueOf(imageType)
								: null, imageProgTypeNum, imageOrder);

				if (image != null) {
					model.put("imageName", image.getImageName());
					model.put("imageId", image.getImageId());
					model.put("imageType", image.getImageType());
					model.put("imageOrder", image.getImageOrder());

					if (!ImageType.C.equals(image.getImageType())) {
						model.put("error", "wrongType");
					} else {
						// We check if this image has linked document on start
						// folio...
						linkedDocumentOnStartFolio = getManuscriptViewerService()
								.findLinkedDocumentOnStartFolioWithOrWithoutRectoVerso(
										volNum,
										volLetExt,
										image.getInsertNum(),
										image.getInsertLet(),
										image.getImageProgTypeNum(),
										image.getMissedNumbering(),
										image.getImageRectoVerso() != null ? image
												.getImageRectoVerso()
												.toString() : null);

						// ..and on transcribe folio
						linkedDocumentOnTranscribeFolio = getManuscriptViewerService()
								.findLinkedDocumentOnTranscriptionWithOrWithoutRectoVerso(
										volNum,
										volLetExt,
										image.getInsertNum(),
										image.getInsertLet(),
										image.getImageProgTypeNum(),
										image.getMissedNumbering(),
										image.getImageRectoVerso() != null ? image
												.getImageRectoVerso()
												.toString() : null);

						if (!linkedDocumentOnStartFolio.isEmpty()
								&& !linkedDocumentOnTranscribeFolio.isEmpty()) {
							intersectionSize = CollectionUtils.intersection(
									linkedDocumentOnStartFolio,
									linkedDocumentOnTranscribeFolio).size();
							unionSize = CollectionUtils.union(
									linkedDocumentOnStartFolio,
									linkedDocumentOnTranscribeFolio).size();
						} else {
							unionSize = Math.max(
									linkedDocumentOnStartFolio.size(),
									linkedDocumentOnTranscribeFolio.size());
						}

						if (unionSize == 1) {
							documentId = linkedDocumentOnStartFolio.isEmpty() ? linkedDocumentOnTranscribeFolio
									.iterator().next().getEntryId()
									: linkedDocumentOnStartFolio.iterator()
											.next().getEntryId();
						} else if (unionSize > 1) {
							isExtract = true;
						}
					}
				}
			}
			// }
			if (documentId != null) {
				isExtract = getManuscriptViewerService().isDocumentExtract(
						documentId);
			}

			// old one
			// model.put("linkedDocument", (linkedDocumentOnStartFolio != null
			// && linkedDocumentOnStartFolio.size() > 0) ? true : false);
			// new ones
			model.put(
					"linkedDocumentOnStartFolio",
					(linkedDocumentOnStartFolio != null && linkedDocumentOnStartFolio
							.size() > 0) ? true : false);
			model.put(
					"linkedDocumentOnTranscribeFolio",
					(linkedDocumentOnTranscribeFolio != null && linkedDocumentOnTranscribeFolio
							.size() > 0) ? true : false);
			model.put("countAlreadyEntered", unionSize);
			model.put("countStartAndTranscribeHere", intersectionSize);
			model.put("entryId", documentId);
			if (unionSize == 1) {
				model.put("showLinkedDocument",
						HtmlUtils.showDocument(documentId));
			} else if (unionSize > 1) {
				model.put("showLinkedDocument", HtmlUtils
						.showSameFolioDocuments(volNum, volLetExt, image
								.getInsertNum(), image.getInsertLet(), image
								.getImageProgTypeNum(), image
								.getMissedNumbering(), image
								.getImageRectoVerso().toString()));
			}
			model.put("isExtract", isExtract);
		} catch (ApplicationThrowable applicationThrowable) {
			model.put("entryId", null);
			// model.put("linkedDocument", false);
			model.put("linkedDocumentOnStartFolio", false);
			model.put("linkedDocumentOnTranscribeFolio", false);
			model.put("showLinkedDocument", "");
			model.put("imageName", "");
			model.put("imageId", "");
			model.put("imageOrder", "");
			model.put("isExtract", false);
		}

		return new ModelAndView("responseOK", model);
	}

	/**
	 * 
	 * @param imageName
	 *            the image name
	 * @param annotationId
	 *            the annotation identifier to show (if none all image
	 *            annotations are retrieved)
	 * @return
	 */
	@RequestMapping(value = { "/src/mview/GetImageAnnotation.json" }, method = RequestMethod.GET)
	public ModelAndView getImageAnnotation(
			@RequestParam(value = "imageId", required = false) Integer imageId,
			@RequestParam(value = "imageName", required = false) String imageName,
			@RequestParam(value = "annotationId", required = false) Integer annotationId) {
		Map<String, Object> model = new HashMap<String, Object>(0);

		try {
			String account = ((UserDetails) SecurityContextHolder.getContext()
					.getAuthentication().getPrincipal()).getUsername();
			Boolean administrator = getUserService().isAccountAdministrator(
					account);

			List<Annotation> annotations;
			if (annotationId != null) {
				annotations = new ArrayList<Annotation>();
				Annotation annotation = getManuscriptViewerService()
						.getImageAnnotation(imageName, annotationId);
				if (annotation != null) {
					annotations.add(annotation);
				}
			} else {
				annotations = getManuscriptViewerService().getImageAnnotations(
						imageId, null);
			}
			List<Object> resultList = getAnnotationsForView(annotationId,
					annotations);
			model.put("annotations", resultList);
			model.put("adminPrivileges", administrator);
		} catch (ApplicationThrowable ath) {
			return new ModelAndView("responseKO", model);
		}

		return new ModelAndView("responseOK", model);
	}

	/**
	 * 
	 * @param entryId
	 *            Document identifier
	 * @param volNum
	 *            Volume Number
	 * @param volLetExt
	 *            Volume Letter Extension
	 * @param imageType
	 * @param imageProgTypeNum
	 * @param firstRecord
	 *            This is input parameter for Carta Form
	 * @param secondRecord
	 *            This is input parameter for Rubricario Form
	 * @param imageOrder
	 *            Unique id identifier inside volume.
	 * @param total
	 *            Global total of volume.
	 * @param totalRubricario
	 *            Total count page in rubricario section.
	 * @param totalCarta
	 *            Total count page in carta section.
	 * @param totalAppendix
	 *            Total count page in appendix section.
	 * @param totalOther
	 * @param totalGuardia
	 * @param modeEdit
	 * @return
	 */
	@RequestMapping(value = { "/src/mview/SearchCarta.json",
			"/de/mview/SearchCarta.json" }, method = RequestMethod.GET)
	public ModelAndView searchCarta(
			@RequestParam(value = "documentId", required = false) Integer documentId,
			@RequestParam(value = "volNum", required = false) Integer volNum,
			@RequestParam(value = "volLetExt", required = false) String volLetExt,
			@RequestParam(value = "insertNum", required = false) String insertNum,
			@RequestParam(value = "insertLet", required = false) String insertLet,
			@RequestParam(value = "imageType", required = false) String imageType,
			@RequestParam(value = "imageProgTypeNum", required = false) Integer imageProgTypeNum,
			@RequestParam(value = "missedNumbering", required = false) String missedNumbering,
			@RequestParam(value = "imageOrder", required = false) Integer imageOrder,
			@RequestParam(value = "imagePosition", required = false) Integer imagePosition,
			@RequestParam(value = "total", required = false) Long total,
			@RequestParam(value = "totalRubricario", required = false) Long totalRubricario,
			@RequestParam(value = "totalCarta", required = false) Long totalCarta,
			@RequestParam(value = "totalAppendix", required = false) Long totalAppendix,
			@RequestParam(value = "totalOther", required = false) Long totalOther,
			@RequestParam(value = "totalGuardia", required = false) Long totalGuardia,
			@RequestParam(value = "formSubmitting", required = false) Boolean formSubmitting,
			@RequestParam(value = "modeEdit", required = false) Boolean modeEdit,
			HttpServletRequest request) {

		Map<String, Object> model = new HashMap<String, Object>(0);
		DocumentEntityExplorer documentExplorer = new DocumentEntityExplorer(
				documentId, volNum);

		MiaDocumentEntity document = getMiaDocumentService()
				.findDocumentEntityById(documentId);
		// TODO da rivedere dopo che è stato chiarito se l'imageOrder è relativo
		// alla singola documentEnt e con numerazione partente da 1 e
		// consecutiva
		int maxImageOrder = 0;
		UploadFileEntity mydocent = null;
		if (imagePosition < document.getUploadFileEntities().size()) {
			mydocent = document.getUploadFileEntities().get(imagePosition);
		}

		if (mydocent == null) {
			model.put("error", "image not found");
		} else {
			String filename = mydocent.getFilename();
			String filenameNoFormat = filename.substring(0,
					filename.lastIndexOf('.'));
			String filenameTif = filenameNoFormat + ".tif";

			// Issue 618 If the inserEntity has no value, insertName is null
			String insName = null;
			if (mydocent.getUploadInfoEntity().getInsertEntity() != null) {
				insName = mydocent.getUploadInfoEntity().getInsertEntity().getInsertName();
			}
			
			String realPath = org.medici.mia.common.util.FileUtils
					.getPathIIPImageServer((mydocent.getUploadInfoEntity())
							.getRepositoryEntity().getLocation(), (mydocent
							.getUploadInfoEntity()).getRepositoryEntity()
							.getRepositoryAbbreviation(), (mydocent
							.getUploadInfoEntity()).getCollectionEntity()
							.getCollectionAbbreviation(), (mydocent
							.getUploadInfoEntity()).getVolumeEntity()
							.getVolume(), (insName));

			String pathToImage = realPath + filenameTif;
			documentExplorer.setImage(new Image());
			documentExplorer.getImage().setImageOrder(imageOrder);
			documentExplorer.setImagePosition(imagePosition);
			documentExplorer.setTotal(total);
			documentExplorer.setVolNum(volNum);
			// verifica giranto pagina ed aprendo il transcription
			documentExplorer.setRepository(mydocent.getUploadInfoEntity()
					.getRepositoryEntity().getRepositoryName());
			documentExplorer.setCollection(mydocent.getUploadInfoEntity()
					.getCollectionEntity().getCollectionName());
			documentExplorer.setVolume(mydocent.getUploadInfoEntity()
					.getVolumeEntity().getVolume());
			documentExplorer
					.setFolio((mydocent.getFolioEntities() != null && mydocent
							.getFolioEntities().size() > 0) ? mydocent
							.getFolioEntities().get(0).getFolioNumber()
							+ " "
							+ mydocent.getFolioEntities().get(0)
									.getRectoverso() : "");

			model.put("repository", documentExplorer.getRepository());
			model.put("collection", documentExplorer.getCollection());
			model.put("volume", documentExplorer.getVolume());
			model.put("folio", documentExplorer.getFolio());
			model.put(
					"series",
					(mydocent.getUploadInfoEntity().getSeries() != null) ? mydocent
							.getUploadInfoEntity().getSeries().toString()
							: "");
			model.put(
					"insert",
					(mydocent.getUploadInfoEntity().getInsert() != null) ? mydocent
							.getUploadInfoEntity().getInsert().toString()
							: "");

			model.put("folio", documentExplorer.getFolio());
			
			// model.put("size", mydocent.getFolioEntities().size());
			// Getting Folios' Info
			if(mydocent.getFolioEntities() != null || mydocent.getFolioEntities().size() > 0) {
				model.put(
						"firstFolio",
						(mydocent.getFolioEntities().size() > 0 && mydocent.getFolioEntities().get(0).getFolioNumber() != null) ? mydocent.getFolioEntities().get(0).getFolioNumber()
								: "");
				model.put(
						"firstFolioRectoVerso",
						(mydocent.getFolioEntities().size() > 0 && mydocent.getFolioEntities().get(0).getRectoverso() != null) ? mydocent.getFolioEntities().get(0).getRectoverso()
								: "");
				model.put(
						"secondFolio",
						(mydocent.getFolioEntities().size() > 1) ? mydocent.getFolioEntities().get(1).getFolioNumber()
								: "");
				model.put(
						"secondFolioRectoVerso",
						(mydocent.getFolioEntities().size() > 1 && mydocent.getFolioEntities().get(1).getRectoverso() != null) ? mydocent.getFolioEntities().get(1).getRectoverso()
								: "");
			}
			
			model.put("documentId", documentId);
			model.put("volNum", volNum + 10);
			model.put("volLetExt", volLetExt);
			model.put("insertNum",
					mydocent.getUploadInfoEntity().getInsertEntity() == null ? null : mydocent.getUploadInfoEntity().getInsertEntity().getInsertName());
			// model.put("insertExt",);
			model.put("imageId", mydocent.getUploadFileId());
			model.put("imageType", "C");
			model.put("imageName", mydocent.getFilename());
			// model.put("missedNumbering",
			// documentExplorer.getImage().getMissedNumbering());
			model.put("imageCompleteName", pathToImage);
			// model.put("imageProgTypeNum",
			// documentExplorer.getImage().getImageProgTypeNum());
			// model.put("imageRectoVerso",
			// documentExplorer.getImage().getImageRectoVerso());
			model.put("imageOrder", imageOrder);
			model.put("imagePosition", imagePosition);
			model.put("total", document.getUploadFileEntities().size());
			model.put("totalRubricario", 0L);
			model.put("totalCarta", 0L);
			model.put("totalAppendix", 0L);
			model.put("totalOther", 0L);
			model.put("totalGuardia", 0L);
			model.put("previousPage", HtmlUtils
					.getDocumentExplorerPreviousPageUrl(documentExplorer));
			model.put("nextPage",
					HtmlUtils.getDocumentExplorerNextPageUrl(documentExplorer));
			if (modeEdit != null && modeEdit.equals(Boolean.TRUE)) {
				model.put("redirectUri",
						"/de/mview/EditDocumentInManuscriptViewer.do");
			} else {
				model.put("redirectUri",
						"/src/mview/ShowDocumentInManuscriptViewer.do");
			}

		}

		return new ModelAndView("responseOK", model);
	}

	/**
	 * This method update the annotations of the folio presented in the
	 * manuscript viewer.
	 * 
	 * @param httpServletRequest
	 *            the request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/src/mview/UpdateAnnotations.json",
			"/de/mview/UpdateAnnotations.json" }, method = RequestMethod.POST)
	public Map<String, Object> updateAnnotations(
			HttpServletRequest httpServletRequest) {
		Map<String, Object> model = new HashMap<String, Object>(0);

		String account = ((UserDetails) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal()).getUsername();

		try {
			Boolean administrator = getUserService().isAccountAdministrator(account);

			Integer imageId = NumberUtils.toInt(httpServletRequest
					.getParameter("imageId"));

			String requestBodyAsString = httpServletRequest.getReader().lines()
					.collect(Collectors.joining(System.lineSeparator()));
			List<Map<String, Object>> annotationsFormView = new Gson().fromJson(requestBodyAsString,
					new TypeToken<List<Map<String, Object>>>() {
					}.getType());

			List<Annotation> annotationsList = new ArrayList<>();
			for (Map<String, Object> annotationString : annotationsFormView) {
				Annotation annotation = new Annotation();
				if (annotationString.get("annotationId") != null) {
					annotation.setAnnotationId(((Double) annotationString.get("annotationId")).intValue());
				}
				annotation.setX((Double) annotationString.get("x"));
				annotation.setY((Double) annotationString.get("y"));
				annotation.setWidth((Double) annotationString.get("w"));
				annotation.setHeight((Double) annotationString.get("h"));
				annotation.setType(Annotation.Type.valueOf(((String) annotationString.get("type")).toUpperCase()));
				annotation.setTitle((String) annotationString.get("title"));
				annotation.setText((String) annotationString.get("text"));
				annotation.setVisible((Boolean) annotationString.get("visibility"));
				annotation.setRgbColor("none".equals((String) annotationString.get("color")) ? null
						: (String) annotationString.get("color"));
				List<Map<String, Object>> questionsFormView = (List<Map<String, Object>>) annotationString
						.get("questions");
				if (questionsFormView != null) {
					List<AnnotationQuestion> questions = new ArrayList<>();
					for (Map<String, Object> questionString : questionsFormView) {
						AnnotationQuestion question = new AnnotationQuestion();
						if (questionString.get("id") != null) {
							question.setId(((Double) questionString.get("id")).intValue());
						}
						question.setText((String) questionString.get("text"));
						questions.add(question);
					}
					annotation.setQuestions(questions);
				}
				annotationsList.add(annotation);
			}
			
			Map<Annotation, Integer> imageAnnotationsMap = getManuscriptViewerService()
					.updateAnnotations(imageId, annotationsList,
							httpServletRequest.getRemoteAddr(), administrator);

			List<Object> resultList = new ArrayList<Object>();
			for (Annotation currentAnnotation : imageAnnotationsMap.keySet()) {
				Map<String, Object> singleRow = new HashMap<String, Object>(0);
				if (imageAnnotationsMap.get(currentAnnotation) > -1) {
					singleRow
							.put("forum",
									((ServletRequestAttributes) RequestContextHolder
											.currentRequestAttributes())
											.getRequest().getContextPath()
											+ "/community/EditForumPostAnnotation.do?topicId="
											+ imageAnnotationsMap
													.get(currentAnnotation));
					resultList.add(singleRow);
				}
			}
			// links -> only new annotations associated to a forum
			model.put("links", resultList);
			// annotation -> all of the annotations associated to the current
			// image
			model.put("annotations",
					getAnnotationsForView(null, imageAnnotationsMap.keySet()));
			model.put("adminPrivileges", administrator);
		} catch (ApplicationThrowable | IOException applicationThrowable) {
			model.put("operation", "KO");
			model.put(
					"error",
					applicationThrowable.getMessage() != null ? applicationThrowable
							.toString() : applicationThrowable.getCause()
							.toString());
			return model;
		}
		model.put("operation", "OK");
		return model;
	}

	/**
	 * This method generates a view of annotations to be sent to the view level.
	 * 
	 * @param annotationId
	 *            the identifier of the annotation to show (if null all
	 *            annotations of the list are showed)
	 * @param annotations
	 *            a list of annotations
	 * @return a view of annotations
	 * @throws ApplicationThrowable
	 */
	private List<Object> getAnnotationsForView(Integer annotationId,
			Collection<Annotation> annotations) throws ApplicationThrowable {
		String account = ((UserDetails) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal()).getUsername();
		Boolean isAdmin = getUserService().isAccountAdministrator(account);
		Boolean isAdminOrCommunityCoordinator = getUserService().isAccountAdministratorOrCommunityCoordinator(account);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		List<Object> resultList = new ArrayList<Object>();
		for (Annotation currentAnnotation : annotations) {
			Map<String, Object> singleRow = new HashMap<String, Object>(0);
			if (annotationId == null
					|| (annotationId != null && annotationId
							.equals(currentAnnotation.getAnnotationId()))) {
				boolean isAnnotationOwner = account.equals(currentAnnotation.getUser()
						.getAccount());
				singleRow.put("annotationId",
						currentAnnotation.getAnnotationId());
				singleRow.put("account", currentAnnotation.getUser().getAccount());
				singleRow.put("x", currentAnnotation.getX());
				singleRow.put("y", currentAnnotation.getY());
				singleRow.put("w", currentAnnotation.getWidth());
				singleRow.put("h", currentAnnotation.getHeight());
				singleRow.put("type", currentAnnotation.getType());
				singleRow.put("title", currentAnnotation.getTitle());
				// RR: we do not show annotation text if it is transcribed
				singleRow.put("text",
						currentAnnotation.getTranscribed() != null
								&& currentAnnotation.getTranscribed() ? ""
								: currentAnnotation.getText());
				singleRow.put("deletable", isAnnotationOwner || isAdminOrCommunityCoordinator);
				singleRow.put("updatable", isAnnotationOwner);
				if (annotationId != null || isAdmin || Boolean.TRUE.equals(currentAnnotation.getVisible())) {
					singleRow.put(
							"visibility",
							annotationId == null ? currentAnnotation
									.getVisible() : true);
				}
				if (currentAnnotation.getForumTopic() != null) {
					singleRow.put(
							"forumTopicURL",
							HtmlUtils
									.getShowTopicForumHrefUrl(currentAnnotation
											.getForumTopic())
									+ "&completeDOM=true");
				}
				if (currentAnnotation.getRgbColor() != null) {
					singleRow.put("color", currentAnnotation.getRgbColor());
				}
				List<AnnotationQuestion> questions = currentAnnotation.getQuestions();
				if (questions != null && !questions.isEmpty()) {
					List<Map<String, Object>> questionList = new ArrayList<>();
					Collections.sort(questions, new Comparator<AnnotationQuestion>() {
					    @Override
					    public int compare(AnnotationQuestion o1, AnnotationQuestion o2) {
					        return o1.getDateCreated().compareTo(o2.getDateCreated());
					    }
					});
					for (AnnotationQuestion question : questions) {
						if (!Boolean.TRUE.equals(question.getLogicalDelete())) {
							boolean isQuestionOwner = account.equals(question.getUser()
									.getAccount());
							Map<String, Object> questionMap = new HashMap<String, Object>(0);
							questionMap.put("id", question.getId());
							questionMap.put("account", question.getUser().getAccount());
							questionMap.put("text", question.getText());
							questionMap.put("dateCreated", simpleDateFormat.format(question.getDateCreated()));
							questionMap.put("deletable", isQuestionOwner || isAdminOrCommunityCoordinator);
							questionMap.put("updatable", isQuestionOwner);
							questionList.add(questionMap);
						}
					}
					singleRow.put("questions", questionList);
				}
				resultList.add(singleRow);
			}
		}
		return resultList;
	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

	public void setMiaDocumentService(MiaDocumentService miaDocumentService) {
		this.miaDocumentService = miaDocumentService;
	}

}