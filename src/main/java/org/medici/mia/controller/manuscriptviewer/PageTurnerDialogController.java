/*
 * PageTurnerDialogController.java
 * 
 * Developed by Medici Archive Project (2010-2012).
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.controller.manuscriptviewer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;
import org.medici.mia.command.manuscriptviewer.PageTurnerCommand;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.domain.Document;
import org.medici.mia.domain.Image;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.manuscriptviewer.ManuscriptViewerService;
import org.medici.mia.service.volbase.VolBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller for action "Page Turner Document Dialog".
 * 
 * @author Lorenzo Pasquinelli (<a href=mailto:l.pasquinelli@gmail.com>l.pasquinelli@gmail.com</a>)
 * @author Matteo Doni (<a href=mailto:donimatteo@gmail.com>donimatteo@gmail.com</a>)
 * @author Ronny Rinaldi (<a href=mailto:rinaldi.ronny@gmail.com>rinaldi.ronny@gmail.com</a>)
 */
@Controller
@RequestMapping(value={"/src/mview/PageTurnerDialog", "/de/mview/PageTurnerDialog"})
public class PageTurnerDialogController {
	
	@Autowired
	private ManuscriptViewerService manuscriptViewerService;
	@Autowired
	private VolBaseService volBaseService;
	
	/**
	 * @param manuscriptViewerService the manuscriptViewerService to set
	 */
	public void setManuscriptViewerService(ManuscriptViewerService manuscriptViewerService) {
		this.manuscriptViewerService = manuscriptViewerService;
	}

	/**
	 * @return the manuscriptViewerService
	 */
	public ManuscriptViewerService getManuscriptViewerService() {
		return manuscriptViewerService;
	}
	
	/**
	 * @param volBaseService the volBaseService to set
	 */
	public void setVolBaseService(VolBaseService volBaseService) {
		this.volBaseService = volBaseService;
	}
	
	/**
	 * @return the volBaseService
	 */
	public VolBaseService getVolBaseService() {
		return volBaseService;
	}

	/**
	 * 
	 * @param command
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView setupForm(@ModelAttribute("command") PageTurnerCommand command, BindingResult result) {
		Map<String, Object> model = new HashMap<String, Object>(0);
		Image image = null;

		try {
			// we set default image as empty string, so we need only to update the record.
			model.put("documentId", command.getDocumentId());
			// If the request is made with entryId, we are asking a document
			
			image =new Image();
			image.setVolNum(command.getVolNum());
			image.setVolLetExt(command.getVolLetExt());
			image.setImageType(command.getImageType());
			image.setImageProgTypeNum(command.getImageProgTypeNum());
			image.setImageOrder(command.getImageOrder());
			model.put("image", image);
			
			model.put("maxAnnotationQuestionNumber", NumberUtils.createInteger(
					ApplicationPropertyManager.getApplicationProperty("annotation.question.max.number")));
		} catch (ApplicationThrowable applicationThrowable) {
			model.put("applicationThrowable", applicationThrowable);
		}
		
		try {
			// We check if this image has a document linked...
			List<Document> documents = new ArrayList<Document>();
			model.put("entryId", documents != null && documents.size() > 0 ? documents.get(0).getEntryId() : null);
			Boolean hasInserts = getVolBaseService().hasInserts(command.getVolNum(), command.getVolLetExt());
			model.put("hasInsert", hasInserts);
		} catch (ApplicationThrowable applicationThrowable) {
			model.put("applicationThrowable", applicationThrowable);
			model.put("entryId", null);
		}

		if (command.getModeEdit()) {
			model.put("caller", "/de/mview/EditDocumentInManuscriptViewer.do");
		} else {
			model.put("caller", "/src/mview/ShowDocumentInManuscriptViewer.do");
		}
		
		
		return new ModelAndView("mview/PageTurnerDialog", model);
	}
}