/*
 * ShowDocumentInManuscriptViewerController.java
 * 
 * Developed by Medici Archive Project (2010-2012).
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 * 
 */
package org.medici.mia.controller.manuscriptviewer;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.medici.mia.command.manuscriptviewer.ShowArchivalLocationInManuscriptViewerCommand;
import org.medici.mia.common.json.allcollections.AllCollectionsFolioJson;
import org.medici.mia.common.json.allcollections.AllCollectionsThumbsFileJson;
import org.medici.mia.common.json.allcollections.AllCollectionsVolumeJson;
import org.medici.mia.common.pagination.ArchivalEntityExplorer;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.dao.uploadfile.UploadFileDAO;
import org.medici.mia.domain.Image;
import org.medici.mia.domain.Image.ImageType;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.UploadInfoEntity;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.allcollections.AllCollectionsService;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller for action "Show Archival Entity In Manuscript Viewer".
 * 
 */
@Controller
@RequestMapping("/src/ShowArchivalLocationInManuscriptViewer")
public class ShowArchivalLocationInManuscriptViewerController {

	@Autowired
	private AllCollectionsService allCollectionsService;
	
    @Autowired
    private MiaDocumentService miaDocumentService;

	@Autowired
	private UploadFileDAO uploadFileDao;
	
	/**
	 * 
	 * @param volumeId
	 * @return
	 */
	@RequestMapping(value = "show", method = RequestMethod.GET)
	public ModelAndView setupPage(@ModelAttribute("requestCommand") ShowArchivalLocationInManuscriptViewerCommand command,
			BindingResult result, HttpServletRequest request) {

		Map<String, Object> model = new HashMap<String, Object>(0);

		try {
			model.put("archivalEntityExplorer", buildArchivalEntityExplorer(command, 0));
			model.put("maxAnnotationQuestionNumber", NumberUtils.createInteger(
					ApplicationPropertyManager.getApplicationProperty("annotation.question.max.number")));
		} catch (ApplicationThrowable applicationThrowable) {
			model.put("applicationThrowable", applicationThrowable);
		}

		return new ModelAndView("mview/ShowArchivalEntityInManuscriptViewerHtml", model);
	}

	@RequestMapping(value = "next", method = RequestMethod.GET)
	public @ResponseBody ArchivalEntityExplorer setupNextPage(
			@ModelAttribute("requestCommand") ShowArchivalLocationInManuscriptViewerCommand command, BindingResult result,
			HttpServletRequest request) {

		try {
			return buildArchivalEntityExplorer(command, 1);
			
		} catch (ApplicationThrowable applicationThrowable) {
			throw new RuntimeException(applicationThrowable);
			
		}
	}

	@RequestMapping(value = "previous", method = RequestMethod.GET)
	public @ResponseBody ArchivalEntityExplorer setupPreviousPage(
			@ModelAttribute("requestCommand") ShowArchivalLocationInManuscriptViewerCommand command, BindingResult result,
			HttpServletRequest request) {

		try {
			return buildArchivalEntityExplorer(command, -1);
			
		} catch (ApplicationThrowable applicationThrowable) {
			throw new RuntimeException(applicationThrowable);
			
		}
	}
	
	@RequestMapping(value = "goTo", method = RequestMethod.GET)
	public @ResponseBody ArchivalEntityExplorer goToPage(
			@ModelAttribute("requestCommand") ShowArchivalLocationInManuscriptViewerCommand command,
			@RequestParam(value = "imageProgTypeNum") String imageProgTypeNum,
			BindingResult result,
			HttpServletRequest request) {

		try {
			ArchivalEntityExplorer archivalEntityExplorer = null;
			
			if (command.getVolumeId() != null) {
				AllCollectionsVolumeJson volume = allCollectionsService.getAllCollectionsVolume(command.getVolumeId(), 0, 0);
				
				Integer imagePosition = 0;
				for(AllCollectionsThumbsFileJson thumb : volume.getThumbsFiles()) {
					boolean found = false;
					for(AllCollectionsFolioJson folio : thumb.getFolios()) {
						if(!folio.getNoNumb() && StringUtils.equals(folio.getFolioNumber(), imageProgTypeNum)) {
							found = true;
						}
					}
					if(found) {
						break;
					}
					imagePosition++;
				}
				
				if(imagePosition >= volume.getThumbsFiles().size()) {
					archivalEntityExplorer = new ArchivalEntityExplorer();
					archivalEntityExplorer.setError("an error occurred");
					
					return archivalEntityExplorer;
				}
				
				archivalEntityExplorer = buildArchivalEntityExplorerImpl(volume, imagePosition);
			}
			
			return archivalEntityExplorer;
			
			
		} catch (ApplicationThrowable applicationThrowable) {
			throw new RuntimeException(applicationThrowable);
			
		}
	}
	
	private ArchivalEntityExplorer buildArchivalEntityExplorer(ShowArchivalLocationInManuscriptViewerCommand command, int adjust) {
		ArchivalEntityExplorer archivalEntityExplorer = null;
		if (command.getVolumeId() != null) {
			AllCollectionsVolumeJson volume = allCollectionsService.getAllCollectionsVolume(command.getVolumeId(), 0, 0);
			
			Integer imagePosition = 0;
			for(AllCollectionsThumbsFileJson thumb : volume.getThumbsFiles()) {
				if (thumb.getUploadFileId().equals(command.getFileId())) {
					break;
				}
				imagePosition++;
			}
			
			imagePosition += adjust;
			
			if(imagePosition < 0 || imagePosition >=  volume.getThumbsFiles().size()) {
				throw new ApplicationThrowable();
			}
			
			archivalEntityExplorer = buildArchivalEntityExplorerImpl(volume, imagePosition);
		}
		
		return archivalEntityExplorer;
	}
	
	private ArchivalEntityExplorer buildArchivalEntityExplorerImpl(AllCollectionsVolumeJson volume, int imagePosition) {
		AllCollectionsThumbsFileJson selectedThumb = volume.getThumbsFiles().get(imagePosition);

		UploadFileEntity uploadFile = uploadFileDao.find(selectedThumb.getUploadFileId());
		UploadInfoEntity uploadInfo = uploadFile.getUploadInfoEntity();
		
		String pathToImage = getRealPathToImage(uploadFile);

		ArchivalEntityExplorer archivalEntityExplorer =  new ArchivalEntityExplorer();
		
		archivalEntityExplorer.setPathToImage(pathToImage);
		archivalEntityExplorer.setImageCompleteName(archivalEntityExplorer.getPathToImage());
		
		archivalEntityExplorer.setPreviousPage("/Mia/json/src/ShowArchivalLocationInManuscriptViewer/previous?volumeId="+volume.getVolumeId()+"&fileId="+uploadFile.getUploadFileId());
		archivalEntityExplorer.setNextPage("/Mia/json/src/ShowArchivalLocationInManuscriptViewer/next?volumeId="+volume.getVolumeId()+"&fileId="+uploadFile.getUploadFileId());
		// this one doesn't want the Mia prefix
		archivalEntityExplorer.setGoToPage("/json/src/ShowArchivalLocationInManuscriptViewer/goTo?volumeId="+volume.getVolumeId()+"&fileId="+uploadFile.getUploadFileId());
		
		archivalEntityExplorer.setRepository(uploadInfo.getRepositoryEntity().getRepositoryName());
		archivalEntityExplorer.setCollection(uploadInfo.getCollectionEntity().getCollectionName());
		archivalEntityExplorer
				.setSeries((uploadInfo.getSeries() != null) ? uploadInfo.getSeries().toString() : "");
		archivalEntityExplorer
				.setInsert((uploadInfo.getInsert() != null) ? uploadInfo.getInsert().toString() : "");
		archivalEntityExplorer.setVolume(uploadInfo.getVolumeEntity().getVolume());
		archivalEntityExplorer
				.setFolio((uploadFile.getFolioEntities() != null && uploadFile.getFolioEntities().size() > 0)
						? uploadFile.getFolioEntities().get(0).getFolioNumber() + " "
								+ uploadFile.getFolioEntities().get(0).getRectoverso()
						: "");
		archivalEntityExplorer.setTotal(Long.parseLong(String.valueOf(volume.getThumbsFiles().size())));
		archivalEntityExplorer.setImage(new Image());
		archivalEntityExplorer.getImage().setImageId(uploadFile.getUploadFileId());
		archivalEntityExplorer.getImage().setImageName(pathToImage);
		archivalEntityExplorer.getImage().setImageOrder(uploadFile.getImageOrder());
		archivalEntityExplorer.getImage().setImageType(ImageType.C);
		archivalEntityExplorer.setTotalAppendix(0L);
		archivalEntityExplorer
				.setTotalCarta(Long.parseLong(String.valueOf(volume.getThumbsFiles().size())));
		archivalEntityExplorer.setTotalGuardia(0L);
		archivalEntityExplorer.setTotalOther(0L);
		archivalEntityExplorer.setTotalRubricario(0L);
		
		// used for volume info
		archivalEntityExplorer.setVolNum(uploadInfo.getVolumeEntity().getVolNum());
		archivalEntityExplorer.setVolLetExt(uploadInfo.getVolumeEntity().getVolLetExt());
		
		archivalEntityExplorer.setImagePosition(imagePosition);
		
		archivalEntityExplorer.setArchivalEntityId(selectedThumb.getUploadId());
		archivalEntityExplorer.setImageId(uploadFile.getUploadFileId());
		
		archivalEntityExplorer.setDocumentCount(miaDocumentService.countDocumentsByUploadFileId(uploadFile.getUploadFileId()));
		archivalEntityExplorer.setSingleUploadPage("/Mia/index.html#/mia/archival-entity/" + selectedThumb.getUploadId()
				+ "?fileId=" + uploadFile.getUploadFileId());
		
		return archivalEntityExplorer;
	}

	private String getRealPathToImage(UploadFileEntity uploadFile) {
		UploadInfoEntity uploadInfo = uploadFile.getUploadInfoEntity();

		String insName = null;
		if (uploadInfo.getInsertEntity() != null) {
			insName = uploadInfo.getInsertEntity().getInsertName();
		}

		String realPath = org.medici.mia.common.util.FileUtils.getPathIIPImageServer(
				((UploadInfoEntity) uploadInfo).getRepositoryEntity().getLocation(),
				((UploadInfoEntity) uploadInfo).getRepositoryEntity().getRepositoryAbbreviation(),
				((UploadInfoEntity) uploadInfo).getCollectionEntity().getCollectionAbbreviation(),
				((UploadInfoEntity) uploadInfo).getVolumeEntity().getVolume(), (insName));

		String filename = uploadFile.getFilename();
		String filenameNoFormat = filename.substring(0, filename.lastIndexOf('.'));
		String filenameTif = filenameNoFormat + ".tif";

		return realPath + filenameTif;
	}

}