/*
 * EditExtractDocumentDialogController.java
 * 
 * Developed by Medici Archive Project (2010-2012).
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.controller.manuscriptviewer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.medici.mia.command.manuscriptviewer.EditExtractDocumentDialogCommand;
import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.document.DocumentTranscriptionJson;
import org.medici.mia.domain.DocumentTranscriptionEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.docbase.DocBaseService;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller for action "Edit Extract Document Dialog".
 * 
 * @author Lorenzo Pasquinelli (<a
 *         href=mailto:l.pasquinelli@gmail.com>l.pasquinelli@gmail.com</a>)
 */
@Controller
@RequestMapping("/src/mview/EditExtractDocumentDialog")
public class EditExtractDocumentDialogController {
	@Autowired
	private DocBaseService docBaseService;
	@Autowired(required = false)
	@Qualifier("editExtractDocumentDialogValidator")
	private Validator validator;
	@Autowired
	private MiaDocumentService miaDocumentService;
	
	@Autowired
	private HistoryLogService historyLogService;

	/**
	 * @return the docBaseService
	 */
	public DocBaseService getDocBaseService() {
		return docBaseService;
	}

	/**
	 * This method returns the Validator class used by Controller to make
	 * business validation.
	 * 
	 * @return
	 */
	public Validator getValidator() {
		return validator;
	}

	/**
	 * 
	 * @param command
	 * @param result
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView processSubmit(
			@Valid @ModelAttribute("command") EditExtractDocumentDialogCommand command,
			BindingResult result) {
		// getValidator().validate(command, result);

		if (result.hasErrors()) {
			return setupForm(command);
		} else {
			Map<String, Object> model = new HashMap<String, Object>(0);

			try {
				List<DocumentTranscriptionJson> transcriptions = new ArrayList<DocumentTranscriptionJson>();
				DocumentTranscriptionJson transcription = new DocumentTranscriptionJson();
				String transid = (command.getDocTranscriptionId() == null) ? null
						: command.getDocTranscriptionId().toString();
				String docentid = (command.getDocumentEntityId() == null) ? null
						: command.getDocumentEntityId().toString();
				// modificatio Issue 360
				String uploadedFileId = (command.getUploadedFileId() == null) ? null
						: command.getUploadedFileId().toString();
				transcription.setDocTranscriptionId(transid);
				transcription.setDocumentId(docentid);
				transcription.setUploadedFileId(uploadedFileId);
				transcription.setTranscription(command.getDocExtract());

				transcriptions.add(transcription);
				
				MiaDocumentEntity documentEntity = miaDocumentService
						.findDocumentEntity(Integer.parseInt(docentid));
				
				historyLogService.registerTranscriptionModification(
						documentEntity, 
						transcription);
			
				GenericResponseDataJson<List<DocumentTranscriptionJson>> resp = (GenericResponseDataJson<List<DocumentTranscriptionJson>>) getMiaDocumentService().saveOrModifyTranscriptions(
						transcriptions);
				if (StatusType.w.toString().equals(resp.getStatus()) && resp.getData() != null && !resp.getData().isEmpty()) {
					command.setConfirmOverwrite(true);
					command.setDocTranscriptionId(Integer.valueOf(resp.getData().get(0).getDocTranscriptionId()));
				}
				
				//Update transSearch column
				getMiaDocumentService().updateTransSearch(command.getDocumentEntityId());
				

				model.put("docExtract", command.getDocExtract());
				model.put("docTranscriptionId", transcriptions.get(0)
						.getDocTranscriptionId());
				model.put("uploadedFileId", uploadedFileId);
				
				if (transcriptions.get(0)
						.getDocTranscriptionId() != null ) {
					command.setDocTranscriptionId(Integer.valueOf(transcriptions.get(0)
						.getDocTranscriptionId()));
				}

				return new ModelAndView("mview/EditExtractDocumentDialog",
						model);
			} catch (ApplicationThrowable applicationThrowable) {
				model.put("applicationThrowable", applicationThrowable);
				return new ModelAndView("error/EditExtractOrSynopsisDocument",
						model);
			}
		}
	}

	/**
	 * @param docBaseService
	 *            the docBaseService to set
	 */
	public void setDocBaseService(DocBaseService docBaseService) {
		this.docBaseService = docBaseService;
	}

	/**
	 * 
	 * @param command
	 * @return
	 */
	/**
	 * @param command
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView setupForm(
			@ModelAttribute("command") EditExtractDocumentDialogCommand command) {
		Map<String, Object> model = new HashMap<String, Object>(0);
		if ((command != null) && (command.getDocumentEntityId() > 0)) {

			try {


				MiaDocumentEntity document = getMiaDocumentService()
						.findDocumentEntityById(command.getDocumentEntityId());
				List<DocumentTranscriptionEntity> transcriptions = getMiaDocumentService().getDocumentTranscriptions(command.getDocumentEntityId());
				String trans2send = null;
				Integer id = null;
				Integer uploadedFileId =  null;
				int imagePosition = command.getImagePosition() == null ? 0 :command.getImagePosition() ;
				UploadFileEntity mydocent = null;
				if (imagePosition < document.getUploadFileEntities().size()) {
					mydocent = document.getUploadFileEntities().get(imagePosition);
					uploadedFileId =  mydocent.getUploadFileId() ;
				}

				if (transcriptions.size() > 0 && mydocent != null ) {
					for (DocumentTranscriptionEntity documentTranscriptionEntity : transcriptions) {
						if (documentTranscriptionEntity.getUploadedFileId() != null && mydocent.getUploadFileId()  != null &&
								mydocent.getUploadFileId().equals(documentTranscriptionEntity.getUploadedFileId()) ) {
							trans2send = documentTranscriptionEntity.getTranscription();
							id = documentTranscriptionEntity.getDocTranscriptionId();
							
						}
					}
				}
				model.put("docExtract", trans2send);
				model.put("docTranscriptionId", id);
				model.put("uploadedFileId", uploadedFileId);

				command.setDocExtract(trans2send);
				command.setDocTranscriptionId(id);
				command.setUploadedFileId(uploadedFileId);

			} catch (ApplicationThrowable ath) {
				return new ModelAndView("error/EditExtractDocumentDialog",
						model);
			}
		} else {
			// On Document creation, the research is always the current user.
			command.setSynExtrId(0);
			command.setDocExtract(null);
		}

		return new ModelAndView("mview/EditExtractDocumentDialog", model);
	}
	
	

	/**
	 * 
	 * @param validator
	 */
	public void setValidator(Validator validator) {
		this.validator = validator;
	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

	public void setMiaDocumentService(MiaDocumentService miaDocumentService) {
		this.miaDocumentService = miaDocumentService;
	}
}