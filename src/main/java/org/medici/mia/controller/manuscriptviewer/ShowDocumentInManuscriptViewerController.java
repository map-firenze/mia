/*
 * ShowDocumentInManuscriptViewerController.java
 * 
 * Developed by Medici Archive Project (2010-2012).
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 * 
 */
package org.medici.mia.controller.manuscriptviewer;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.math.NumberUtils;
import org.medici.mia.command.manuscriptviewer.ShowDocumentInManuscriptViewerCommand;
import org.medici.mia.common.pagination.DocumentEntityExplorer;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.domain.Image;
import org.medici.mia.domain.Image.ImageType;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.UploadInfoEntity;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.docbase.DocBaseService;
import org.medici.mia.service.manuscriptviewer.ManuscriptViewerService;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller for action "Show Document In Manuscript Viewer".
 * 
 * @author Lorenzo Pasquinelli (<a
 *         href=mailto:l.pasquinelli@gmail.com>l.pasquinelli@gmail.com</a>)
 * @author Matteo Doni (<a
 *         href=mailto:donimatteo@gmail.com>donimatteo@gmail.com</a>)
 */
@Controller
@RequestMapping("/src/ShowDocumentInManuscriptViewer")
public class ShowDocumentInManuscriptViewerController {
	@Autowired
	private DocBaseService docBaseService;
	@Autowired
	private ManuscriptViewerService manuscriptViewerService;
	@Autowired
	private MiaDocumentService miaDocumentService;

	// /**
	// *
	// * @param volumeId
	// * @return
	// */
	// @RequestMapping(value = "show",method = RequestMethod.GET)
	// public ModelAndView setupPage(@ModelAttribute("requestCommand")
	// ShowDocumentInManuscriptViewerCommand command, BindingResult result){
	// Map<String, Object> model = new HashMap<String, Object>(0);
	// DocumentExplorer documentExplorer = null;
	//
	// //TODO mettere un controllo su document ID
	//
	// if (!Boolean.TRUE.equals(command.getShowTranscription())) {
	// documentExplorer = new DocumentExplorer(command.getDocumentId(),
	// command.getVolume());
	//
	// MiaDocumentEntity document =
	// getMiaDocumentService().findDocumentEntityById(command.getDocumentId());
	//
	// cercare i campi usati nel bean doucment
	//
	// documentExplorer.setImage(new Image());
	// documentExplorer.getImage().setImageProgTypeNum(command.getImageProgTypeNum());
	// documentExplorer.getImage().setImageOrder(command.getImageOrder());
	// if (command.getImageOrder() == null) {
	// // FIXME do extend command
	// documentExplorer.getImage().setInsertNum("");
	// documentExplorer.getImage().setInsertLet("");
	// documentExplorer.getImage().setMissedNumbering("");
	// documentExplorer.getImage().setImageRectoVerso(null);
	// }
	// documentExplorer.getImage().setImageType(command.getImageType());
	// documentExplorer.setTotal(command.getTotal());
	// documentExplorer.setTotalRubricario(command.getTotalRubricario());
	// documentExplorer.setTotalCarta(command.getTotalCarta());
	// documentExplorer.setTotalAppendix(command.getTotalAppendix());
	// documentExplorer.setTotalOther(command.getTotalOther());
	// documentExplorer.setTotalGuardia(command.getTotalGuardia());
	// }
	//
	// try {
	// if (!Boolean.TRUE.equals(command.getShowTranscription())) {
	// documentExplorer =
	// getManuscriptViewerService().getDocumentExplorer(documentExplorer);
	// } else {
	// //TODO dopo aver verificato che non esiste più il caso in cui non si
	// mostra la
	// //transcription eliminare gli altri casi
	//
	// documentExplorer =
	// getManuscriptViewerService().getDocumentExplorer(command.getEntryId(),
	// true);
	// }
	//
	// if (documentExplorer.getEntryId() == null) {
	// Image image = documentExplorer.getImage();
	// List<Document> documents =
	// getManuscriptViewerService().findLinkedDocument(
	// command.getVolume(),
	// command.getVolLetExt(),
	// image.getInsertNum(),
	// image.getInsertLet(),
	// image.getImageProgTypeNum(),
	// image.getMissedNumbering(),image.getImageRectoVerso().toString());
	//
	// if (documents.size() > 0) {
	// documentExplorer.setEntryId(documents.get(0).getEntryId());
	// }
	// }
	//
	// model.put("documentExplorer", documentExplorer);
	// model.put("showTranscription",
	// !Boolean.TRUE.equals(command.getShowTranscription()) ? false : true);
	// } catch (ApplicationThrowable applicationThrowable) {
	// model.put("applicationThrowable", applicationThrowable);
	// }
	//
	// return new ModelAndView("mview/ShowDocumentInManuscriptViewerHtml",
	// model);
	// }

	/**
	 * 
	 * @param volumeId
	 * @return
	 */
	@RequestMapping(value = "show", method = RequestMethod.GET)
	public ModelAndView setupPage(
			@ModelAttribute("requestCommand") ShowDocumentInManuscriptViewerCommand command,
			BindingResult result, HttpServletRequest request) {
		Map<String, Object> model = new HashMap<String, Object>(0);
		DocumentEntityExplorer documentExplorer = null;

		try {
			if (command.getDocumentEntityId() != null) {
				documentExplorer = new DocumentEntityExplorer(
						command.getDocumentEntityId(), command.getVolume());

				MiaDocumentEntity document = getMiaDocumentService()
						.findDocumentEntityById(command.getDocumentEntityId());
				// TODO da rivedere dopo che è stato chiarito se l'imageOrder è relativo
				// alla singola documentEnt e con numerazione partente da 1 e
				// consecutiva
				int maxImageOrder = 0;
				Integer imagePosition = command.getImagePosition() == null ? 0 : command.getImagePosition();
				UploadFileEntity upload = null;

				if(command.getFileId() != null){
					for(UploadFileEntity uploadFileEntity: document.getUploadFileEntities()){
						if(uploadFileEntity.getUploadFileId().equals(command.getFileId())){
							upload = uploadFileEntity;
						}
					}
				} else if (imagePosition < document.getUploadFileEntities().size()) {
					upload = document.getUploadFileEntities().get(imagePosition);
				}

				if (upload == null) {
					model.put("error", "image not found");
				} else {
					UploadInfoEntity uploadInfo = upload.getUploadInfoEntity();
					imagePosition = document.getUploadFileEntities().indexOf(upload);

					// Issue 618 If the inserEntity has no value, insertName is null
					String insName = null;
					if (uploadInfo.getInsertEntity() != null) {
						insName = uploadInfo.getInsertEntity().getInsertName();
					}

					String realPath = org.medici.mia.common.util.FileUtils
							.getPathIIPImageServer(((UploadInfoEntity) uploadInfo)
											.getRepositoryEntity().getLocation(),
									((UploadInfoEntity) uploadInfo)
											.getRepositoryEntity()
											.getRepositoryAbbreviation(),
									((UploadInfoEntity) uploadInfo)
											.getCollectionEntity()
											.getCollectionAbbreviation(),
									((UploadInfoEntity) uploadInfo)
											.getVolumeEntity().getVolume(),
									(insName));

					String filename = upload.getFilename();
					String filenameNoFormat = filename.substring(0,
							filename.lastIndexOf('.'));
					String filenameTif = filenameNoFormat + ".tif";

					String pathToImage = realPath + filenameTif;
					documentExplorer.setPathToImage(pathToImage);
					documentExplorer.setRepository(uploadInfo.getRepositoryEntity()
							.getRepositoryName());
					documentExplorer.setCollection(uploadInfo.getCollectionEntity()
							.getCollectionName());
					documentExplorer
							.setSeries((uploadInfo.getSeries() != null) ? uploadInfo
									.getSeries().toString() : "");
					documentExplorer
							.setInsert((uploadInfo.getInsert() != null) ? uploadInfo
									.getInsert().toString() : "");
					documentExplorer.setVolume(uploadInfo.getVolumeEntity()
							.getVolume());
					documentExplorer
							.setFolio((upload.getFolioEntities() != null && upload
									.getFolioEntities().size() > 0) ? upload
									.getFolioEntities().get(0).getFolioNumber()
									+ " "
									+ upload.getFolioEntities().get(0)
									.getRectoverso() : "");
					documentExplorer.setTotal(Long.parseLong(String
							.valueOf(document.getUploadFileEntities().size())));
					documentExplorer.setImage(new Image());
					documentExplorer.getImage().setImageOrder(
							upload.getImageOrder());
					// TODO destinato a sparire
					documentExplorer.getImage().setImageType(ImageType.C);
					documentExplorer.getImage().setImageId(upload.getUploadFileId());
					documentExplorer.getImage().setImageName(pathToImage);
					documentExplorer.setTotalAppendix(0L);
					documentExplorer.setTotalCarta(Long.parseLong(String
							.valueOf(document.getUploadFileEntities().size())));
					documentExplorer.setTotalGuardia(0L);
					documentExplorer.setTotalOther(0L);
					documentExplorer.setTotalRubricario(0L);
					documentExplorer.setVolNum(0);
					documentExplorer.setImagePosition(imagePosition);

					document.getTranscription();

					model.put("documentExplorer", documentExplorer);
					model.put("showTranscription", !Boolean.TRUE.equals(command
							.getShowTranscription()) ? false : true);
					model.put("imagePosition", imagePosition);
					model.put("maxAnnotationQuestionNumber", NumberUtils.createInteger(
							ApplicationPropertyManager.getApplicationProperty("annotation.question.max.number")));
				}
	

			}
		} catch (ApplicationThrowable applicationThrowable) {
			model.put("applicationThrowable", applicationThrowable);
		}

		return new ModelAndView("mview/ShowDocumentInManuscriptViewerHtml",
				model);
	}

	/**
	 * @param docBaseService
	 *            the docBaseService to set
	 */
	public void setDocBaseService(DocBaseService docBaseService) {
		this.docBaseService = docBaseService;
	}

	/**
	 * @return the docBaseService
	 */
	public DocBaseService getDocBaseService() {
		return docBaseService;
	}

	/**
	 * @param manuscriptViewerService
	 *            the manuscriptViewerService to set
	 */
	public void setManuscriptViewerService(
			ManuscriptViewerService manuscriptViewerService) {
		this.manuscriptViewerService = manuscriptViewerService;
	}

	/**
	 * @return the manuscriptViewerService
	 */
	public ManuscriptViewerService getManuscriptViewerService() {
		return manuscriptViewerService;
	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

	public void setMiaDocumentService(MiaDocumentService miaDocumentService) {
		this.miaDocumentService = miaDocumentService;
	}

}