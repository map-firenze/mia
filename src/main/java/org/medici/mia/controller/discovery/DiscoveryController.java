package org.medici.mia.controller.discovery;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.medici.mia.common.json.DiscoveryJson;
import org.medici.mia.common.json.DiscoveryUploadJson;
import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.controller.upload.FileUploadForm;
import org.medici.mia.domain.DiscoveryStatus;
import org.medici.mia.service.discovery.DiscoveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/discovery")
public class DiscoveryController {

	@Autowired
	private DiscoveryService service;
	
	@RequestMapping(value = "find/{id}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, DiscoveryJson>> findById(
			@PathVariable("id") Integer id) {
		
		GenericResponseDataJson<HashMap<String, DiscoveryJson>> resp = new GenericResponseDataJson<HashMap<String, DiscoveryJson>>();
		HashMap<String, DiscoveryJson> docHash = new HashMap<String, DiscoveryJson>();
		
		DiscoveryJson rez = service.findById(id);
		
		if(rez != null) {
			docHash.put("discovery", rez);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Discovery find OK");
			resp.setData(docHash);
			return resp;
			
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Unable to add link. This document doesn't exist in the database.");
			return resp;
			
		}
	}
	
	@RequestMapping(value = "default", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, DiscoveryJson>> getDefaultDiscovery(@RequestParam("documentId") Integer documentId) {
		
		GenericResponseDataJson<HashMap<String, DiscoveryJson>> resp = new GenericResponseDataJson<HashMap<String, DiscoveryJson>>();
		HashMap<String, DiscoveryJson> docHash = new HashMap<String, DiscoveryJson>();
		
		DiscoveryJson rez = service.getDefault(documentId);
		
		if(rez != null) {
			docHash.put("discovery", rez);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Publication found. OK");
			resp.setData(docHash);
			return resp;
			
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("This Publication does not exist in the Database");
			return resp;
			
		}
	}
	
	@RequestMapping(value = "findDiscoveries", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, Object>> findDiscoveries(
			@RequestParam("searchText") String searchText, @RequestParam("limit") Integer limit, @RequestParam("offset") Integer offset) {
		
		GenericResponseDataJson<HashMap<String, Object>> resp = new GenericResponseDataJson<HashMap<String, Object>>();
		HashMap<String, Object> docHash = new HashMap<String, Object>();
		
		List<DiscoveryJson> rez = service.findDiscoveries(searchText, limit, offset, null);
		Integer rezCount = service.getDiscoveriesCount(null);
		
		if(rez != null) {
			docHash.put("discoveries", rez);
			docHash.put("count", rezCount);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Discovery find OK");
			resp.setData(docHash);
			return resp;
			
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("This document doesn't exist in the database.");
			return resp;
			
		}
	}
	
	@RequestMapping(value = "findDiscoveriesByStatus", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, Object>> findDiscoveriesByStatus(@RequestParam("limit") Integer limit, @RequestParam("offset") Integer offset, @RequestParam("status") DiscoveryStatus status) {
		
		GenericResponseDataJson<HashMap<String, Object>> resp = new GenericResponseDataJson<HashMap<String, Object>>();
		HashMap<String, Object> docHash = new HashMap<String, Object>();
		
		List<DiscoveryJson> rez = service.findDiscoveries(null, limit, offset, status);
		Integer rezCount = service.getDiscoveriesCount(status);
		
		if(rez != null) {
			docHash.put("discoveries", rez);
			docHash.put("count", rezCount);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Discovery find OK");
			resp.setData(docHash);
			return resp;
			
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("This document doesn't exist in the database.");
			return resp;
			
		}
	}
	
	@RequestMapping(value = "add", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<HashMap<String, DiscoveryJson>> add(
			@RequestBody DiscoveryJson discovery) {
		
		GenericResponseDataJson<HashMap<String, DiscoveryJson>> resp = new GenericResponseDataJson<HashMap<String, DiscoveryJson>>();
		HashMap<String, DiscoveryJson> docHash = new HashMap<String, DiscoveryJson>();
		
		DiscoveryJson rez = service.add(discovery);
		
		docHash.put("discovery", rez);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage("Discovery add OK");
		resp.setData(docHash);
		return resp;
	}
	
	@RequestMapping(value = "{id}/approve", method = RequestMethod.PUT)
	public @ResponseBody GenericResponseDataJson<HashMap<String, DiscoveryJson>> approve(@PathVariable Integer id) {
		
		GenericResponseDataJson<HashMap<String, DiscoveryJson>> resp = new GenericResponseDataJson<HashMap<String, DiscoveryJson>>();
		HashMap<String, DiscoveryJson> docHash = new HashMap<String, DiscoveryJson>();
		
		DiscoveryJson rez = service.approve(id);
		
		docHash.put("discovery", rez);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage("Discovery approved");
		resp.setData(docHash);
		return resp;
	}
	
	
	@RequestMapping(value = "{id}/reject", method = RequestMethod.PUT)
	public @ResponseBody GenericResponseDataJson<HashMap<String, DiscoveryJson>> reject(@PathVariable Integer id, @RequestBody HashMap<String, String> motivation) {
		
		GenericResponseDataJson<HashMap<String, DiscoveryJson>> resp = new GenericResponseDataJson<HashMap<String, DiscoveryJson>>();
		HashMap<String, DiscoveryJson> docHash = new HashMap<String, DiscoveryJson>();
		
		DiscoveryJson rez = service.reject(id, motivation.get("motivation"));
		
		docHash.put("discovery", rez);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage("Discovery rejected");
		resp.setData(docHash);
		return resp;
	}
	
	@RequestMapping(value = "{id}/publish", method = RequestMethod.PUT)
	public @ResponseBody GenericResponseDataJson<HashMap<String, DiscoveryJson>> publish(@PathVariable Integer id) {
		
		GenericResponseDataJson<HashMap<String, DiscoveryJson>> resp = new GenericResponseDataJson<HashMap<String, DiscoveryJson>>();
		HashMap<String, DiscoveryJson> docHash = new HashMap<String, DiscoveryJson>();
		
		DiscoveryJson rez = service.publish(id);
		
		docHash.put("discovery", rez);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage("Discovery published");
		resp.setData(docHash);
		return resp;
	}
	
	@RequestMapping(value = "{id}/editRequest", method = RequestMethod.PUT)
	public @ResponseBody GenericResponseDataJson<HashMap<String, DiscoveryJson>> editRequest(@PathVariable Integer id, @RequestBody HashMap<String, String> motivation) {
		
		GenericResponseDataJson<HashMap<String, DiscoveryJson>> resp = new GenericResponseDataJson<HashMap<String, DiscoveryJson>>();
		HashMap<String, DiscoveryJson> docHash = new HashMap<String, DiscoveryJson>();
		
		DiscoveryJson rez = service.editRequest(id, motivation.get("motivation"));
		
		docHash.put("discovery", rez);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage("Discovery edit request");
		resp.setData(docHash);
		return resp;
	}
	
	@RequestMapping(value = "{id}/edit", method = RequestMethod.PUT)
	public @ResponseBody GenericResponseDataJson<HashMap<String, DiscoveryJson>> edit(@PathVariable Integer id, @RequestBody DiscoveryJson discovery) {
		
		GenericResponseDataJson<HashMap<String, DiscoveryJson>> resp = new GenericResponseDataJson<HashMap<String, DiscoveryJson>>();
		HashMap<String, DiscoveryJson> docHash = new HashMap<String, DiscoveryJson>();
		
		DiscoveryJson rez = service.edit(discovery);
		
		docHash.put("discovery", rez);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage("Discovery edit request");
		resp.setData(docHash);
		return resp;
	}
	
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody GenericResponseDataJson<HashMap<String, DiscoveryJson>> delete(@PathVariable Integer id){
    	GenericResponseDataJson<HashMap<String, DiscoveryJson>> resp = new GenericResponseDataJson<HashMap<String, DiscoveryJson>>();
		HashMap<String, DiscoveryJson> docHash = new HashMap<String, DiscoveryJson>();
    	
    	if( id == null ) {
			throw new IllegalArgumentException("Discovery is new");
		}
    	
    	DiscoveryJson rez = service.remove(id);
    	
    	docHash.put("discovery", rez);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage("Discovery remove OK");
		resp.setData(docHash);
		
		return resp;
    }
    
    
	@RequestMapping(value = "findUserDiscoveries", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, Object>> findUserDiscoveries(@RequestParam("account") String account) {
		GenericResponseDataJson<HashMap<String, Object>> resp = new GenericResponseDataJson<HashMap<String, Object>>();
		HashMap<String, Object> docHash = new HashMap<String, Object>();
		
		List<DiscoveryJson> rezDiscoveryList = service.findUserDiscoveries(account);
		
		if(rezDiscoveryList != null) {
			docHash.put("discoveries", rezDiscoveryList);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Discovery find OK");
			resp.setData(docHash);
			return resp;
			
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Document not exists in DB");
			return resp;
			
		}
	}
	
	@RequestMapping(value = "newSpotlights", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, Object>> newSpotlights() {
		
		GenericResponseDataJson<HashMap<String, Object>> resp = new GenericResponseDataJson<HashMap<String, Object>>();
		HashMap<String, Object> docHash = new HashMap<String, Object>();
		
		Integer rezCount = service.getDiscoveriesCount(null);
		
		if(rezCount != null) {
			docHash.put("count", rezCount);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("New spotlights find OK");
			resp.setData(docHash);
			return resp;
			
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("This document doesn't exist in the database.");
			return resp;
			
		}
	}
	
	@RequestMapping(value = "upload", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<HashMap<String, Object>> uploadDiscoveryUpload(@ModelAttribute("files") FileUploadForm filesForm) {
		ByteArrayInputStream in = null;
		FileOutputStream out = null;
		
		GenericResponseDataJson<HashMap<String, Object>> resp = new GenericResponseDataJson<HashMap<String, Object>>();

		try {
			File parentFolder = new File(ApplicationPropertyManager.getApplicationProperty("discoveries.upload.path"));
			if( !parentFolder.exists() ) {
				parentFolder.mkdirs();
			}
			
			MultipartFile inputFile = filesForm.getFiles().get(0);
			
			String extension = getExtension(inputFile.getOriginalFilename());
			
			String internalFileName = UUID.randomUUID().toString();
			File newFile = new File(parentFolder, internalFileName + extension);

			in = new ByteArrayInputStream(inputFile.getBytes());
			out = new FileOutputStream(newFile);
			IOUtils.copy(in, out);
			
			DiscoveryUploadJson upload = new DiscoveryUploadJson();
			upload.setFileName(inputFile.getOriginalFilename());
			upload.setInternalFileName(internalFileName);
			upload.setDeleted(false);
			
			HashMap<String, Object> docHash = new HashMap<String, Object>();
			docHash.put("upload", upload);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("File upload OK");
			resp.setData(docHash);
			return resp;
			
		} catch( Exception e) {
			e.printStackTrace();
			
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Exception during upload");
			return resp;
		} finally {
			try {
				if( in != null ) {
					in.close();
				}
				if( out != null ) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Exception during method cleanup");
				return resp;
			}
		}
	}
	
	
	@RequestMapping(value = "{id}/remove/{discoveryUploadId}", method = RequestMethod.DELETE)
	public @ResponseBody GenericResponseDataJson<HashMap<String, Object>> removeDiscoveryUpload(@PathVariable("id") Integer id, @PathVariable("discoveryUploadId") Integer discoveryUploadId,
			HttpServletResponse response) {
		
		GenericResponseDataJson<HashMap<String, Object>> resp = new GenericResponseDataJson<HashMap<String, Object>>();
		
		DiscoveryUploadJson deletedUpload = service.removeDiscoveryUpload(id, discoveryUploadId);
		
		try {
			HashMap<String, Object> docHash = new HashMap<String, Object>();
			docHash.put("uplaad", deletedUpload);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("File upload OK");
			resp.setData(docHash);
			return resp;
		} catch( Exception e ) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Error deleting attachment");
			return resp;
		}
	}
	
	
	@RequestMapping(value = "{id}/download/{discoveryUploadId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseJson downloadDiscoveryUpload(
			@PathVariable("id") Integer id, @PathVariable("discoveryUploadId") Integer discoveryUploadId,
			HttpServletResponse response) {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ko.toString());
		
		if( id == null ) {
			throw new IllegalArgumentException("Discovery is new");
		}
		
		if( discoveryUploadId == null ) {
			throw new IllegalArgumentException("DiscoveryUpload is new");
		}
		
		DiscoveryUploadJson discoveryUpload = service.findDiscoveryUploadById(discoveryUploadId);
		
		File file = null;
		FileInputStream fileIn = null;
		ServletOutputStream out = null;
		try {
			if ( discoveryUpload != null ) {
				response.setContentType("application/octet-stream");
				response.setHeader("Content-Disposition",
						"attachment;filename=" + discoveryUpload.getFileName());

				file = new File(discoveryUpload.getFilePath() + discoveryUpload.getInternalFileName() + getExtension(discoveryUpload.getFileName()));
				fileIn = new FileInputStream(file);
				out = response.getOutputStream();

				byte[] outputByte = new byte[(int) file.length()];
				
				// copy binary content to output stream
				while (fileIn.read(outputByte, 0, (int) file.length()) != -1) {
					out.write(outputByte, 0, (int) file.length());
				}
				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("File Download successfull complete");
			} else {
				resp.setMessage("Path or Filename of DiscoveryUpload not found on DB");
				return resp;
			}
		} catch (IOException e) {
			e.printStackTrace();
			resp.setMessage("Unable to download the file - IOException");
		} finally {
			try {
				if (fileIn != null) {
					fileIn.close();
				}
				if (out != null) {
					out.flush();
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("Problem during closing of the stream for download file");
			}
		}

		return resp;
	}
	
	private static String getExtension(String originalFilename) {
		int idx = originalFilename.lastIndexOf(".");
		return originalFilename.substring(idx);
		
	}
	
}
