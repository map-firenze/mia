package org.medici.mia.controller.news;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.news.MyNewsfeedItemJson;
import org.medici.mia.common.json.news.NewsAEJson;
import org.medici.mia.common.json.news.NewsDocumentJson;
import org.medici.mia.common.json.news.NewsPeopleJson;
import org.medici.mia.common.json.news.NewsPlaceJson;
import org.medici.mia.common.json.news.NewsUserJson;
import org.medici.mia.domain.User;
import org.medici.mia.service.user.UserService;
import org.medici.mia.service.useractivity.INewsUserJson;
import org.medici.mia.service.useractivity.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Controller
@RequestMapping("/news")
public class NewsController {

	private static DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Autowired
	private UserService userService;
	
	@Autowired
	private NewsService newsService;

	// http://localhost:8181/Mia/json/news/getMonthlyUpdates
	@RequestMapping(value = "/getMonthlyUpdates", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, NewsUserJson>> getMonthlyUpdates() {

		GenericResponseDataJson<HashMap<String, NewsUserJson>> resp = new GenericResponseDataJson<HashMap<String, NewsUserJson>>();
		HashMap<String, NewsUserJson> docHash = new HashMap<String, NewsUserJson>();

		java.util.Date newDate = DateUtils.addMonths(new Date(), -1);
		// NewsUserJson news = getNewsService().findNews(sdf.format(newDate));
		NewsUserJson news = getNewsService().findNews(newDate, 50);

		if (news == null) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No news with found.");
			return resp;
		} else {
			docHash.put("news", news);
			resp.setData(docHash);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("News found.");
		}

		return resp;

	}

	// http://localhost:8181/Mia/json/news/getDailyUpdates
	@RequestMapping(value = "getDailyUpdates", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, NewsUserJson>> getDailyUpdates() {

		GenericResponseDataJson<HashMap<String, NewsUserJson>> resp = new GenericResponseDataJson<HashMap<String, NewsUserJson>>();
		HashMap<String, NewsUserJson> docHash = new HashMap<String, NewsUserJson>();
		java.util.Date newDate = DateUtils.addDays(new Date(), -1);
		NewsUserJson news = getNewsService().findNews(newDate, 50);

		if (news == null) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No news with found.");
			return resp;
		} else {
			docHash.put("news", news);
			resp.setData(docHash);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("News found.");
		}

		return resp;

	}

	// http://localhost:8181/Mia/json/news/getWeeklyUpdates
	@RequestMapping(value = "getWeeklyUpdates", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, NewsUserJson>> getWeeklyUpdates() {

		GenericResponseDataJson<HashMap<String, NewsUserJson>> resp = new GenericResponseDataJson<HashMap<String, NewsUserJson>>();
		HashMap<String, NewsUserJson> docHash = new HashMap<String, NewsUserJson>();

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -7);

		java.util.Date newDate = DateUtils.addWeeks(new Date(), -1);

		NewsUserJson news = getNewsService().findNews(newDate, 50);

		if (news == null) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No news with found.");
			return resp;
		} else {
			docHash.put("news", news);
			resp.setData(docHash);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("News found.");
		}

		return resp;

	}

	// http://localhost:8181/Mia/json/news/getUpdates/8
	@RequestMapping(value = "/getUpdates/{numberOfUpdates}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<INewsUserJson>>> getOrderedMonthlyUpdates(
			@PathVariable("numberOfUpdates") Integer numberOfUpdates) {

		GenericResponseDataJson<HashMap<String, List<INewsUserJson>>> resp = new GenericResponseDataJson<HashMap<String, List<INewsUserJson>>>();
		HashMap<String, List<INewsUserJson>> docHash = new HashMap<String, List<INewsUserJson>>();

		java.util.Date newDate = DateUtils.addMonths(new Date(), -1);
		NewsUserJson news = getNewsService().findNews(newDate, numberOfUpdates);

		if (news == null) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No news with found.");
			return resp;
		}

		List<INewsUserJson> iNews = new ArrayList<INewsUserJson>();
		List<NewsAEJson> aes = news.getAes();
		if (aes != null && !aes.isEmpty()) {
			for (NewsAEJson newsAe : aes) {
				iNews.add((NewsAEJson) newsAe);
			}
		}

		List<NewsDocumentJson> documents = news.getDocuments();
		if (documents != null && !documents.isEmpty()) {
			for (NewsDocumentJson newsDoc : documents) {
				iNews.add((NewsDocumentJson) newsDoc);
			}
		}
		List<NewsPeopleJson> people = news.getPeople();
		if (people != null && !people.isEmpty()) {
			for (NewsPeopleJson person : people) {
				iNews.add((NewsPeopleJson) person);
			}
		}

		List<NewsPlaceJson> places = news.getPlaces();
		if (places != null && !places.isEmpty()) {
			for (NewsPlaceJson place : places) {
				iNews.add((NewsPlaceJson) place);
			}
		}

		if (iNews.isEmpty()) {
			resp.setData(docHash);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("No News found.");
			return resp;
		}

		Collections.sort(iNews, INewsUserJson.COMPARE_BY_DATETIME);

		if (numberOfUpdates == null || iNews.size() < numberOfUpdates) {
			docHash.put("news", iNews.subList(0, iNews.size()));
		} else {
			docHash.put("news", iNews.subList(0, numberOfUpdates));
		}

		resp.setData(docHash);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage("News found.");
		return resp;

	}
	
	// http://localhost:8181/Mia/json/news/getMyNewsfeed/testuser1
	@RequestMapping(value = "getMyNewsfeed/{account}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<MyNewsfeedItemJson>> getMyResearchHistory(
			@PathVariable("account") String account) {

		GenericResponseDataJson<List<MyNewsfeedItemJson>> resp = new GenericResponseDataJson<List<MyNewsfeedItemJson>>();

		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
		if (!account.equals(user.getAccount()) && !getUserService().isAccountAdministrator(user.getAccount())) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("You do not have an access to view newsfeed for this account");
		} else {
			List<MyNewsfeedItemJson> data = getNewsService().getMyNewsfeed(user);
			resp.setData(data);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("got " + data.size() + " items");
		}
		return resp;
	}

	// http://localhost:8181/Mia/json/news/createMyNewsfeedItem/testuser1
	@RequestMapping(value = "createMyNewsfeedItem/{account}", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<MyNewsfeedItemJson> createMyNewsfeedItem(
			@PathVariable("account") String account, @RequestBody MyNewsfeedItemJson myNewsfeedItemJson) {

		GenericResponseDataJson<MyNewsfeedItemJson> resp = new GenericResponseDataJson<MyNewsfeedItemJson>();

		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
		if (!account.equals(user.getAccount())) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("You do not have an access to create newsfeed item for this account");
		} else {
			resp = getNewsService().createMyNewsfeedItem(user, myNewsfeedItemJson);
		}
		return resp;
	}

	// http://localhost:8181/Mia/json/news/deleteMyNewsfeedItem/testuser1/1
	@RequestMapping(value = "deleteMyNewsfeedItem/{account}/{id}", method = RequestMethod.DELETE)
	public @ResponseBody GenericResponseJson deleteMyNewsfeedItem(@PathVariable("account") String account,
			@PathVariable("id") Integer id) {

		GenericResponseJson resp = new GenericResponseJson();

		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
		if (!account.equals(user.getAccount())) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("You do not have an access to delete newsfeed item for this account");
		} else {
			getNewsService().deleteMyNewsfeedItem(user, id);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("item successfully deleted");
		}
		return resp;
	}

	public NewsService getNewsService() {
		return newsService;
	}

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}
	
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}