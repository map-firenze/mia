package org.medici.mia.controller.advancedsearch;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.advancedsearch.AdvancedSearchFactoryJson;
import org.medici.mia.common.json.advancedsearch.GenericAdvancedSearchJson;
import org.medici.mia.common.json.advancedsearch.IAdvancedSearchJson;
import org.medici.mia.common.json.advancedsearch.PeopleAdvancedSearchJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.search.PeopleWordSearchJson;
import org.medici.mia.common.json.search.PeopleWordSearchType;
import org.medici.mia.domain.User;
import org.medici.mia.service.advancedsearch.AdvancedSearchService;
import org.medici.mia.service.genericsearch.PeopleSearchService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Controller
@RequestMapping("/de/advancedsearch")
public class DEAdvancedSearchController {

	@Autowired
	private AdvancedSearchService advancedSearchService;

	@Autowired
	private PeopleSearchService searchService;

	@Autowired
	private UserService userService;

	// localhost:8181/Mia/json/de/advancedsearch/partialRecordCount/
	@RequestMapping(value = "partialRecordCount", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<Integer> countWordAdvacedSearch(
			@RequestParam(value = "isNewsFeedSearch", required = false) Boolean isNewsFeedSearch,
			@RequestBody List<GenericAdvancedSearchJson> adSearches) {

		GenericResponseDataJson<Integer> resp = new GenericResponseDataJson<Integer>();
		if (adSearches == null || adSearches.isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request list is null or empty");
			return resp;
		}

		List<String> queryList = new ArrayList<String>();

		for (GenericAdvancedSearchJson advancedSearchJson : adSearches) {

			if (Boolean.TRUE.equals(isNewsFeedSearch)) {
				User user = getUserService()
						.findUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
								.getUsername());
				advancedSearchJson.setNewsfeedUser(user);
			}
			IAdvancedSearchJson search = AdvancedSearchFactoryJson
					.getAdvancedSearchJson(advancedSearchJson);
			if (search == null) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("searchType not found.");
				return resp;
			}
			if (advancedSearchJson.isActiveFilter()) {
				List<String> queries = (search.getQueries());
				if (queries != null && !queries.isEmpty()) {
					for (String query : queries) {
						queryList.add(query);

					}
				}

			}

		}

		return getAdvancedSearchService().countDocumentsAdvancedSearch(
				queryList);

	}

	// localhost:8181/Mia/json/de/advancedsearch/advancedSearchResults/10/15/documentEntityId
	@RequestMapping(value = "advancedSearchResults/{firstResult}/{maxResult}/{orderColumn}/{ascOrDesc}", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<List<DocumentJson>> advancedSearchResults(
			@RequestBody List<GenericAdvancedSearchJson> adSearches,
			@PathVariable("firstResult") Integer firstResult,
			@PathVariable("maxResult") Integer maxResult,
			@PathVariable("orderColumn") String orderColumn,
			@PathVariable("ascOrDesc") String ascOrDesc,
			@RequestParam(value = "isNewsFeedSearch", required = false) Boolean isNewsFeedSearch) {

		GenericResponseDataJson<List<DocumentJson>> resp = new GenericResponseDataJson<List<DocumentJson>>();
		if (adSearches == null || adSearches.isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request list is null or empty");
			return resp;
		}

		List<String> queryList = new ArrayList<String>();

		for (GenericAdvancedSearchJson advancedSearchJson : adSearches) {

			if (Boolean.TRUE.equals(isNewsFeedSearch)) {
				User user = getUserService()
						.findUser(((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
								.getUsername());
				advancedSearchJson.setNewsfeedUser(user);
			}
			IAdvancedSearchJson search = AdvancedSearchFactoryJson
					.getAdvancedSearchJson(advancedSearchJson);
			if (search == null) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("searchType not found.");
				return resp;
			}
			if (advancedSearchJson.isActiveFilter()) {
				List<String> queries = search.getQueries();
				if (queries != null && !queries.isEmpty()) {
					for (String query : queries) {
						queryList.add(query);

					}
				}

			}

		}

		return getAdvancedSearchService().getDocumentsAdvancedSearch(queryList,
				firstResult, maxResult, orderColumn, ascOrDesc);

	}

	// localhost:8181/Mia/json/de/advancedsearch/advancedSearchPeopleResults/
	@RequestMapping(value = "advancedSearchPeopleResults/{firstResult}/{maxResult}/{orderColumn}/{ascOrDesc}", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<List<DocumentJson>> advancedSearchResults(
			@RequestBody PeopleWordSearchJson searchReq,
			@PathVariable("firstResult") Integer firstResult,
			@PathVariable("maxResult") Integer maxResult,
			@PathVariable("orderColumn") String orderColumn,
			@PathVariable("ascOrDesc") String ascOrDesc) {	
		

		GenericResponseDataJson<List<DocumentJson>> resp = new GenericResponseDataJson<List<DocumentJson>>();

		if (isSearchable(searchReq, resp)) {
			List<String> peopleList = searchService
					.getPeopleIdBySearchWords(searchReq);
			PeopleAdvancedSearchJson peopleSearch = new PeopleAdvancedSearchJson();
			peopleSearch.setActiveFilter(true);
			peopleSearch.setPeople(peopleList);

			List<String> queryList = new ArrayList<String>();
			List<String> queries = peopleSearch.getQueries();
			if (queries != null && !queries.isEmpty()) {
				for (String query : queries) {
					queryList.add(query);

				}
			}

			return getAdvancedSearchService().getDocumentsAdvancedSearch(
					queryList, firstResult, maxResult, orderColumn, ascOrDesc);

		}
		resp.setMessage("The input data is not correct.");
		resp.setStatus(StatusType.w.toString());

		return resp;

	}

	private boolean isSearchable(PeopleWordSearchJson searchReq,
			GenericResponseDataJson<List<DocumentJson>> resp) {

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return false;
		}
		if (searchReq.getSearchType() == null
				|| searchReq.getSearchType().isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("SearchType is empty");
			return false;
		}
		int idx = 0;
		try {
			for (String type : searchReq.getSearchType()) {
				PeopleWordSearchType searchType = PeopleWordSearchType
						.valueOf(type);
				idx++;
			}

		} catch (IllegalArgumentException ex) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("SearchType " + searchReq.getSearchType().get(idx)
					+ " not permitted");
			return false;
		}

		if (searchReq.getSearchString() == null
				|| searchReq.getSearchString().trim().isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Search String is empty");
			return false;
		}

		return true;

	}

	public AdvancedSearchService getAdvancedSearchService() {
		return advancedSearchService;
	}

	public void setAdvancedSearchService(
			AdvancedSearchService advancedSearchService) {
		this.advancedSearchService = advancedSearchService;
	}
	
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
