package org.medici.mia.controller.advancedsearch;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.advancedsearch.AdvancedSearchPeopleFactoryJson;
import org.medici.mia.common.json.advancedsearch.GenericAdvancedSearchJson;
import org.medici.mia.common.json.advancedsearch.IAdvancedSearchJson;
import org.medici.mia.common.json.biographical.BiographicalPeopleJson;
import org.medici.mia.service.advancedsearch.AdvancedSearchService;
import org.medici.mia.service.genericsearch.PeopleSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Controller
@RequestMapping("/biographical/advancedsearch")
public class PeopleAdvancedSearchController {

	@Autowired
	private AdvancedSearchService advancedSearchService;

	@Autowired
	private PeopleSearchService searchService;

	// localhost:8181/Mia/json/biographical/advancedsearch/partialRecordCount/
	@RequestMapping(value = "partialRecordCount", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<Integer> countWordAdvacedSearch(
			@RequestBody List<GenericAdvancedSearchJson> adSearches) {

		GenericResponseDataJson<Integer> resp = new GenericResponseDataJson<Integer>();
		if (adSearches == null || adSearches.isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request list is null or empty");
			return resp;
		}

		List<String> queryList = new ArrayList<String>();

		for (GenericAdvancedSearchJson advancedSearchJson : adSearches) {

			IAdvancedSearchJson search = AdvancedSearchPeopleFactoryJson
					.getAdvancedSearchJson(advancedSearchJson);
			if (search == null) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("searchType not found.");
				return resp;
			}
			if (advancedSearchJson.isActiveFilter()) {
				List<String> queries = (search.getQueries());
				if (queries != null && !queries.isEmpty()) {
					for (String query : queries) {
						queryList.add(query);

					}
				}

			}

		}

		return getAdvancedSearchService().countPeopleAdvancedSearch(queryList);

	}

	// localhost:8181/Mia/json/biographical/advancedsearch/advancedSearchResults/
	@RequestMapping(value = "advancedSearchResults/{firstResult}/{maxResult}/{orderColumn}/{ascOrDesc}", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<List<BiographicalPeopleJson>> advancedSearchResults(
			@RequestBody List<GenericAdvancedSearchJson> adSearches,
			@PathVariable("firstResult") Integer firstResult,
			@PathVariable("maxResult") Integer maxResult,
			@PathVariable("orderColumn") String orderColumn,
			@PathVariable("ascOrDesc") String ascOrDesc) {

		GenericResponseDataJson<List<BiographicalPeopleJson>> resp = new GenericResponseDataJson<List<BiographicalPeopleJson>>();
		if (adSearches == null || adSearches.isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request list is null or empty");
			return resp;
		}

		List<String> queryList = new ArrayList<String>();

		for (GenericAdvancedSearchJson advancedSearchJson : adSearches) {

			IAdvancedSearchJson search = AdvancedSearchPeopleFactoryJson
					.getAdvancedSearchJson(advancedSearchJson);
			if (search == null) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("searchType not found.");
				return resp;
			}
			if (advancedSearchJson.isActiveFilter()) {
				List<String> queries = search.getQueries();
				if (queries != null && !queries.isEmpty()) {
					for (String query : queries) {
						queryList.add(query);

					}
				}

			}

		}

		return getAdvancedSearchService().getPeopleAdvancedSearch(queryList,
				firstResult, maxResult, orderColumn, ascOrDesc);

	}

	public AdvancedSearchService getAdvancedSearchService() {
		return advancedSearchService;
	}

	public void setAdvancedSearchService(
			AdvancedSearchService advancedSearchService) {
		this.advancedSearchService = advancedSearchService;
	}

}
