package org.medici.mia.controller.message;

import javax.servlet.http.HttpServletRequest;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.message.CountMessageJson;
import org.medici.mia.common.json.message.FindInboxMessageJson;
import org.medici.mia.common.json.message.FindOutboxMessageJson;
import org.medici.mia.common.json.message.MessageDeleteJson;
import org.medici.mia.common.json.message.MessageSelectedJson;
import org.medici.mia.common.json.message.MessageSendJson;
import org.medici.mia.domain.EmailMessageUser;
import org.medici.mia.domain.User;
import org.medici.mia.service.community.CommunityService;
import org.medici.mia.service.message.MessageService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Controller
@RequestMapping("/messaging")
public class MessageController {
	
	@Autowired
	private CommunityService communityService;

	@Autowired
	private MessageService messageService;

	@Autowired
	private UserService userService;

	// http://localhost:8181/Mia/json/messaging/getInboxMessages/user/?page=1&perPage=10
	@RequestMapping(value = "getInboxMessages/{account}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<FindInboxMessageJson> getInboxMessages(
			@PathVariable("account") String account,
//			@PathVariable("startMesNum") Integer startMesNum,
//			@PathVariable("endMesNum") Integer endMesNum),
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "perPage", required = false) Integer perPage) {

		if (page == null && perPage == null) {
			page = 1;
			perPage = 10;
		}
		GenericResponseDataJson<FindInboxMessageJson> resp = new GenericResponseDataJson<FindInboxMessageJson>();
		if (!getUserService().isAccountAutorizedToSeeMessageList(account)) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("User is not authorized to see the message list.");
			return resp;
		}

		FindInboxMessageJson messages = getMessageService().findInboxMessages(
				account, page, perPage, false);

		if (messages != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got   " + messages.getMessages().size()
					+ " inbox messages");

			resp.setData(messages);
			return resp;
		} else {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("No inbox messages for this account found");
			return resp;
		}

	}

	// http://localhost:8181/Mia/json/messaging/getNewInboxMessages/user/?page=1&perPage=10
	@RequestMapping(value = "getNewInboxMessages/{account}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<CountMessageJson> getNewInboxMessages(
			@PathVariable("account") String account,
//			@PathVariable("startMesNum") Integer startMesNum,
//			@PathVariable("endMesNum") Integer endMesNum,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "perPage", required = false) Integer perPage
	) {
		if(page == null && perPage == null){
			page = 1;
			perPage = 10;
		}
		GenericResponseDataJson<CountMessageJson> resp = new GenericResponseDataJson<CountMessageJson>();
		if (!getUserService().isAccountAutorizedToSeeMessageList(account)) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("User is not authorized to see the message list.");
			return resp;
		}

		FindInboxMessageJson messages = getMessageService().findInboxMessages(
				account, page, perPage, true);

		CountMessageJson countMessage = new CountMessageJson();

		if (messages != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got   " + messages.getMessages().size()
					+ " new inbox messages");
			countMessage.setMessTotalCount(messages.getMessTotalCount());

			resp.setData(countMessage);
			return resp;
		} else {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("No new inbox messages for this account found.");
			countMessage.setMessTotalCount(0);
			resp.setData(countMessage);
			return resp;
		}

	}

	// http://localhost:8181/Mia/json/messaging/getOutboxMessages/user/?page=1&perPage=10
	@RequestMapping(value = "getOutboxMessages/{account}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<FindOutboxMessageJson> getOutboxMessages(
			@PathVariable("account") String account,
//			@PathVariable("startMesNum") Integer startMesNum,
//			@PathVariable("endMesNum") Integer endMesNum,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "perPage", required = false) Integer perPage) {

		if(page == null && perPage == null) {
			page = 1;
			perPage = 10;
		}

		GenericResponseDataJson<FindOutboxMessageJson> resp = new GenericResponseDataJson<FindOutboxMessageJson>();
		if (!getUserService().isAccountAutorizedToSeeMessageList(account)) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("User is not authorized to see the message list.");
			return resp;
		}

		FindOutboxMessageJson messages = getMessageService()
				.findOutboxMessages(account, page, perPage);

		if (messages != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got   " + messages.getMessages().size()
					+ " outbox messages");

			resp.setData(messages);
			return resp;
		} else {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("No inbox messages for this account found");
			return resp;
		}

	}

	// http://localhost:8181/Mia/json/messaging/send/
	@RequestMapping(value = "send", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson send(
			@RequestBody MessageSendJson message,
			HttpServletRequest request) {
		GenericResponseJson resp = new GenericResponseJson();
		if (message != null) {
			if (!getUserService().isAccountAutorizedToSeeMessageList(
					request.getRemoteUser()) || message.getAccount().compareToIgnoreCase(request.getRemoteUser()) != 0) {
				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("User is not authorized to modify the message list.");
				return resp;
			}
			resp = getMessageService().send(message);
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Data not valid to sens");
		}
		return resp;
	}
	
	// http://localhost:8181/Mia/json/messaging/sendToAll/
	@RequestMapping(value = "sendToAll", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson sendToAll(@RequestBody MessageSendJson message,
			HttpServletRequest request) {
		GenericResponseJson resp = new GenericResponseJson();
		if (message != null) {
			if (!getUserService().isAccountAdministrator(request.getRemoteUser())
					|| message.getAccount().compareToIgnoreCase(request.getRemoteUser()) != 0) {
				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("User is not authorized to send the message to all users.");
				return resp;
			}
			EmailMessageUser emailMessageUser = new EmailMessageUser();
			emailMessageUser.setSubject(message.getMessageSubject());
			emailMessageUser.setBody(message.getMessageText());
			getCommunityService().createNewEmailMessageUserForAll(emailMessageUser);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Message added in DB.");
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Data not valid to send");
		}
		return resp;
	}

	// http://localhost:8181/Mia/json/messaging/delete/5
	@RequestMapping(value = "delete/{messageId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseJson delete(
			@PathVariable("messageId") Integer messageId) {

		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()).getUsername());

		if (!getUserService().isAccountAutorizedToSeeMessageList(
				user.getAccount())) {
			GenericResponseJson resp = new GenericResponseJson();
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("User is not authorized to delete the message.");
			return resp;
		}

		return getMessageService().deleteMessageById(messageId);
	}

	@RequestMapping(value = "deleteMultipleMessages", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson deleteMultipleMessages(
			@RequestBody MessageDeleteJson messageDeleteJson) {
		GenericResponseJson resp = new GenericResponseJson();
		if (messageDeleteJson != null
				&& messageDeleteJson.getMessagesId() != null
				&& !messageDeleteJson.getMessagesId().isEmpty()) {
			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());
			if (!getUserService().isAccountAutorizedToSeeMessageList(
					user.getAccount())) {
				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("User is not authorized to modify the message list.");
				return resp;
			}
			return getMessageService().deleteMultipleMessages(messageDeleteJson.getMessagesId());
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Data not valid to sens");
		}
		return resp;
	}

	@RequestMapping(value = "getMessage/{messageId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseJson getMessage(
			@PathVariable("messageId") Integer messageId) {

		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()).getUsername());
		GenericResponseDataJson<MessageSelectedJson> resp = new GenericResponseDataJson<MessageSelectedJson>();

		if (!getUserService().isAccountAutorizedToSeeMessageList(
				user.getAccount())) {

			resp.setStatus(StatusType.w.toString());
			resp.setMessage("User is not authorized to delete the message.");
			return resp;
		}

		MessageSelectedJson message = getMessageService().getMessage(messageId);

		if (message != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Message by messaheId  " + messageId + " found");

			resp.setData(message);
			return resp;
		} else {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("No message by messageId  " + messageId + " found");
			return resp;
		}

	}
	
	public CommunityService getCommunityService() {
		return communityService;
	}

	public void setCommunityService(CommunityService communityService) {
		this.communityService = communityService;
	}

	public MessageService getMessageService() {
		return messageService;
	}

	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}