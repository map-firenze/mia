/*
 * ApplicationPropertiesController.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.controller.applicationproperties;

import java.util.HashMap;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mysql.jdbc.StringUtils;

/**
 * 
 * @author fsolfato
 *
 */

@Controller
@RequestMapping("applicationProperties")
public class ApplicationPropertiesController {
	
	private String[] privateVariables = {
		"mail.server.host",
		"mail.server.port",
		"mail.server.password",
		"mail.server.username",
		"recaptcha.domainName",
		"recaptcha.privateKey",
		"recaptcha.server"};
	
	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, String>> 
	getProperty(
			@RequestParam("propertyName") String propertyName) {
		
		GenericResponseDataJson<HashMap<String, String>> response = 
				new GenericResponseDataJson<HashMap<String, String>>();
		
		if(StringUtils.isNullOrEmpty(propertyName)){
            response.setMessage("Property name cannot be empty");
            response.setStatus(StatusType.ko.toString());
            
            return response;
        }
		
		if(isPrivate(propertyName)){
            response.setMessage("Property is private");
            response.setStatus(StatusType.ko.toString());
            
            return response;
        }
        
		HashMap<String, String> data = new HashMap<String, String>();
		
		String propertyValue = 
				ApplicationPropertyManager.getApplicationProperty(propertyName);
		
		data.put(propertyName, propertyValue);
		
		response.setData(data);
		response.setStatus(StatusType.ok.toString());
		response.setMessage(String.format("Property '%s value: %s", 
				propertyName, propertyValue));
		
		return response;
	}
	
	private boolean isPrivate(String propertyName) {
		for (String variable : privateVariables) {
			if (variable.equalsIgnoreCase(propertyName.trim().toLowerCase())) {
				return true;
			}
		}
		return false;
	}
}
