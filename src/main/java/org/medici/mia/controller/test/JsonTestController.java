package org.medici.mia.controller.test;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.medici.mia.command.manuscriptviewer.ShowManuscriptViewerRequestCommand;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.UploadTestJson;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.domain.CollectionEntity;
import org.medici.mia.domain.GoogleLocation;
import org.medici.mia.domain.Image;
import org.medici.mia.domain.RepositoryEntity;
import org.medici.mia.domain.SeriesEntity;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.test.JsonTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/test/JsonTest")
public class JsonTestController {

	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private JsonTestService jsonTestService;

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody JsonTestOutput getJson(HttpSession session) {

		// test get Json
		JsonTestOutput jsonTestOutput = new JsonTestOutput();
		jsonTestOutput.setId(2);
		jsonTestOutput.setName("Mia");

		// test get session attribute
		JsonTest testSession = (JsonTest) session.getAttribute("success");
		logger.info("testSession: " + testSession);

		return jsonTestOutput;
	}

	@RequestMapping(value = "jsonTestInput", method = RequestMethod.POST)
	public @ResponseBody JsonTest postJson(
			@RequestBody final JsonTest jsonTest, HttpSession session) {

		// test post Json
		logger.info("SUCCESS: Inside JsonTestController - Json Values on Controller are: "
				+ jsonTest.getName() + " " + jsonTest.getId());

		// Decomment the operation you want to do from 'Testa la post' FE button
		getJsonTestService().save(jsonTest);
		// getJsonTestService().delete(jsonTest); //delete by PK id
		// getJsonTestService().update(jsonTest); //update name field by PK id
		// getJsonTestService().retrieve(jsonTest); //retrieve by PK id

		// test set session attribute
		session.setAttribute("success", jsonTest);

		return jsonTest;
	}

	// url: http://localhost:8181/Mia/json/test/JsonTest/Hello
	@RequestMapping(value = "/{st}", method = RequestMethod.GET)
	public @ResponseBody String findString(@PathVariable("st") String st) {

		RepositoryEntity repEntity = new RepositoryEntity();
		String repositoryId = "";
		if (repositoryId != null && !repositoryId.isEmpty()) {
			repEntity.setRepositoryId(Integer.valueOf(repositoryId));
		}

		repEntity.setRepositoryName(st);
		repEntity.setLocation("gPlaceId");
		repEntity.setRepositoryAbbreviation("repositoryAbbreviation");
		repEntity.setRepositoryDescription("repositoryDescription");

		GoogleLocation gl = new GoogleLocation();
		gl.setCity("city");
		gl.setCountry("country");
		gl.setgPlaceId("gPlaceId");

		String collectionId = "";

		CollectionEntity coll = new CollectionEntity();
		if (collectionId != null && !collectionId.isEmpty()) {
			coll.setCollectionId(Integer.valueOf(collectionId));
		}

		coll.setCollectionName("coll Test");
		coll.setCollectionDescription("collectionDescription");
		coll.setCollectionAbbreviation("collectionAbbreviation");

		String seriesId = "";

		SeriesEntity ser = new SeriesEntity();
		if (seriesId != null && !seriesId.isEmpty()) {
			ser.setSeriesId(Integer.valueOf(seriesId));
		}

		ser.setTitle("Titolo");
		ser.setSubtitle1("Sottotitolo1");
		ser.setSubtitle2("Sottotitolo2");

		// Integer repId = getJsonTestService().insertRepository(repEntity,
		// coll);
		// Integer collId = getJsonTestService().insertCollection(coll);

		// return String.valueOf(collId);
		String ret = getJsonTestService().insertUploadFiles(gl, repEntity,
				coll, ser);
		getJsonTestService().testAsync();

		return ret;

	}

	// return st;

	// url: http://localhost:8181/Mia/json/test/JsonTest/findCountries/IT
	@RequestMapping(value = "findCountries/{country}", method = RequestMethod.GET)
	public @ResponseBody String findCountries(
			@PathVariable("country") String country) {
		JsonTest jsonTest = new JsonTest();
		jsonTest.setId(10);
		jsonTest.setName("shadi");

		getJsonTestService().save(jsonTest);

		return country;

	}

	public JsonTestService getJsonTestService() {
		return jsonTestService;
	}

	public void setJsonTestService(JsonTestService jsonTestService) {
		this.jsonTestService = jsonTestService;
	}

	// url: http://localhost:8181/Mia/json/test/JsonTest/provaImmagine
	// TODO: request: uploadedFilesId - response: PreviewImage
	@RequestMapping(value = "provaImmagine", method = RequestMethod.GET)
	public ModelAndView setupForm(
			@ModelAttribute("requestCommand") ShowManuscriptViewerRequestCommand command,
			BindingResult result) {
		Map<String, Object> model = new HashMap<String, Object>(0);
		Image image = null;

		try {
			// we set default image as empty string, so we need only to update
			// the record.
			model.put("image", "");

			// If the request is made with entryId, we are asking a document
			// if (!ObjectUtils.toString(command.getEntryId()).equals("")) {
			// image =
			// getManuscriptViewerService().findDocumentImage(command.getEntryId(),
			// null, null, command.getImageType(),
			// command.getImageProgTypeNum(), command.getImageOrder());
			// } else {
			// image =
			// getManuscriptViewerService().findVolumeImage(command.getSummaryId(),
			// command.getVolNum(), command.getVolLetExt(),
			// command.getImageType(), command.getImageProgTypeNum(),
			// command.getImageOrder());
			// }

			// TODO: viene salvato in sessione oggetto previewImage già
			// valorizzato
			model.put("image", image);

			if (command.getAnnotationId() != null) {
				model.put("annotationId", command.getAnnotationId());
			}
			model.put("maxAnnotationQuestionNumber", NumberUtils.createInteger(
					ApplicationPropertyManager.getApplicationProperty("annotation.question.max.number")));
		} catch (ApplicationThrowable applicationThrowable) {
			model.put("applicationThrowable", applicationThrowable);
		}

		return new ModelAndView("mview/TestShowManuscriptViewerHtml", model);
	}

	// url: http://localhost:8181/Mia/json/test/JsonTest/jsonTestProva
	@RequestMapping(value = "jsonTestProva", method = RequestMethod.POST)
	public @ResponseBody JsonTest jsonTestProva(@RequestBody JsonTest jsonTest) {

		String str = jsonTest.toString();
		// test post Json
		logger.info("SUCCESS! " + str);
		return jsonTest;
	}

//	http://localhost:8181/Mia/json/test/JsonTest/uploadPortraitUser
	@RequestMapping(value = "uploadPortraitUser", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson uploadPortraitUser(
			@ModelAttribute("portrait") UploadTestJson portrait,
			HttpSession session) {

		GenericResponseJson resp = new GenericResponseJson();
		if (portrait == null) {
			resp.setMessage("UploadTestJson null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		if (portrait.getImageMultipart() != null
				&& !portrait.getImageMultipart().isEmpty()) {
			resp.setMessage("multipartfile OK.");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		} else if (portrait.getBufferedImage() != null) {
			resp.setMessage("Buffered image OK.");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		} else if (portrait.getImageBlob() != null) {
			resp.setMessage("Blob image OK.");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		} else if (portrait.getImageByteArray() != null) {
			resp.setMessage("Byte Array image OK.");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}

		return resp;
	}

//	http://localhost:8181/Mia/json/test/JsonTest/uploadPortraitUserReq
	@RequestMapping(value = "uploadPortraitUserReq", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson uploadPortraitUserReq(
			@RequestBody UploadTestJson portrait) {

		GenericResponseJson resp = new GenericResponseJson();
		if (portrait == null) {
			resp.setMessage("UploadTestJson null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		if (portrait.getImageMultipart() != null
				&& !portrait.getImageMultipart().isEmpty()) {
			resp.setMessage("multipartfile OK.");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		} else if (portrait.getBufferedImage() != null) {
			resp.setMessage("Buffered image OK.");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		} else if (portrait.getImageBlob() != null) {
			resp.setMessage("Blob image OK.");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		} else if (portrait.getImageByteArray() != null) {
			resp.setMessage("Byte Array image OK.");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}

		return resp;
	}
	
	@RequestMapping(value = "uploadTest", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson uploadTest(
			@RequestBody UploadTestJson portrait) {
		
		GenericResponseJson resp = new GenericResponseJson();
		if (portrait == null) {
			resp.setMessage("UploadTestJson null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		if (portrait.getImageMultipart() != null
				&& !portrait.getImageMultipart().isEmpty()) {
			resp.setMessage("multipartfile OK.");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		} else if (portrait.getBufferedImage() != null) {
			resp.setMessage("Buffered image OK.");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		} else if (portrait.getImageBlob() != null) {
			resp.setMessage("Blob image OK.");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		} else if (portrait.getImageByteArray() != null) {
			resp.setMessage("Byte Array image OK.");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}else if (portrait.getImageString() != null) {
			resp.setMessage("Strin image OK.");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}
		
		resp.setMessage("All attrs in portrait is null.");
		resp.setStatus(StatusType.ko.toString());

		return resp;
	}

}
