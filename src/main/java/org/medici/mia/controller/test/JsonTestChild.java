package org.medici.mia.controller.test;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author User
 *
 */
@JsonTypeName("child")
public class JsonTestChild extends JsonTest implements Serializable{

	private static final long serialVersionUID = 1L;
	
//	@JsonProperty("surname")
	private String surname;

	public JsonTestChild() {
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	
}
