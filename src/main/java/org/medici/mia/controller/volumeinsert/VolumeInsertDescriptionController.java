/*
 * VolumeInsertController.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.controller.volumeinsert;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.github.underscore.*;
import org.apache.log4j.Logger;
import org.medici.mia.common.json.*;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.search.AEArchivalSearchJson;
import org.medici.mia.controller.archive.CountAESandDESJson;
import org.medici.mia.controller.archive.MyArchiveBean;
import org.medici.mia.controller.upload.FileUploadForm;
import org.medici.mia.controller.upload.UploadBean;
import org.medici.mia.dao.collection.CollectionDAO;
import org.medici.mia.dao.insert.InsertDAO;
import org.medici.mia.dao.repository.RepositoryDAO;
import org.medici.mia.dao.schedone.SchedoneDAO;
import org.medici.mia.dao.volume.VolumeDAO;
import org.medici.mia.domain.*;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.genericsearch.AESearchService;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.myarchive.MyArchiveService;
import org.medici.mia.service.upload.UploadService;
import org.medici.mia.service.user.UserService;
import org.medici.mia.service.volbase.VolBaseService;
import org.medici.mia.service.volumeinsert.VolumeInsertDescriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author alai
 *
 */
@Controller
@RequestMapping("/volumeInsertDescription")
public class VolumeInsertDescriptionController {

	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private VolBaseService volBaseService;

	@Autowired
	private UploadService uploadService;

	@Autowired
	private VolumeInsertDescriptionService volumeInsertDescriptionService;

	@Autowired
	private AESearchService searchService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RepositoryDAO repositoryDAO;
	
	@Autowired
	private SchedoneDAO schedoneDAO;

	@Autowired
	private HistoryLogService historyLogService;

	@Autowired
	private MyArchiveService archiveService;

	/*
	 * Find Volume Description Find Insert Description Add Volume Modify Volume
	 * basic metadata Modify Volume advanced metadata Add Insert Modify Insert basic
	 * metadata Modify Insert advanced metadata Delete Volume Record Delete Insert
	 * Record
	 */
	// json/volumeInsertDescription/findVolume/{volumeId}
	// localhost:8181/Mia/json/volumeInsertDescription/findVolume/123
	@RequestMapping(value = "findVolume/{volumeId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, VolumeDescriptionJson>> findVolume(
			@PathVariable("volumeId") String volumeId) {
		GenericResponseDataJson<HashMap<String, VolumeDescriptionJson>> resp = new GenericResponseDataJson<HashMap<String, VolumeDescriptionJson>>();

		HashMap<String, VolumeDescriptionJson> docHash = new HashMap<String, VolumeDescriptionJson>();
		Integer volumeIdNum = null;
		try {
			volumeIdNum = Integer.valueOf(volumeId);
		} catch (Exception e) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("volumeId may be null or not a number.");
			return resp;
		}
		Volume volume = volBaseService.findVolume(volumeIdNum);

		if (volume != null) {
			// Search
			Schedone schedone = getSchedoneDAO().findBySummaryId(volume.getSummaryId());

			VolumeDescriptionJson dataVolume = new VolumeDescriptionJson();
			dataVolume.toJson(volume, schedone);

			docHash.put("data", dataVolume);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got 1 volume in total.");
			resp.setData(docHash);

		} else {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No volume exists in DB. ");

		}

		return resp;

	}
		
	// Count Archival and Document Entities related to Volumes 
	@RequestMapping(value = "countVolumeAESandDES/{volumeId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<CountAESandDESJson> countVolumeAESandDES(
			@PathVariable String volumeId) {
		GenericResponseDataJson<CountAESandDESJson> resp = new GenericResponseDataJson<CountAESandDESJson>();

		Integer volumeIdNum = null;
		try {
			volumeIdNum = Integer.valueOf(volumeId);
		} catch (Exception e) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("volumeId may be null or not a number.");
			return resp;
		}
		
		resp = searchService.countVolumeAESandDES(volumeIdNum);

		return resp;
		
	}
	
	// Count Archival and Document Entities related to Insert 
	@RequestMapping(value = "countInsertAESandDES/{insertId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<CountAESandDESJson> countInsertAESandDES(
			@PathVariable String insertId) {
		GenericResponseDataJson<CountAESandDESJson> resp = new GenericResponseDataJson<CountAESandDESJson>();
		
		Integer insertIdNum = null;
		try {
			insertIdNum = Integer.valueOf(insertId);
		} catch (Exception e) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("volumeId may be null or not a number.");
			return resp;
		}
		
		resp = searchService.countInsertAESandDES(insertIdNum);

		return resp;

		
	}

	// Find Archival  Entities related to Insert
	@RequestMapping(value = "FindInsertAES/{insertId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> findInsertAES(
			@PathVariable String insertId) {
		GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> resp = new GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>>();
		Integer InsertIdNum = null;
		try {
			InsertIdNum = Integer.valueOf(insertId);
		} catch (Exception e) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("volumeId may be null or not a number.");
			return resp;
		}
		
		resp = searchService.findArchivalEntitiesRelatedToInsert(InsertIdNum);

		return resp;
	}

	// Find Document Entities related to Insert
	@RequestMapping(value = "FindInsertDES/{insertId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<DocumentJson>>> findInsertDES(
			@PathVariable String insertId) {

		GenericResponseDataJson<HashMap<String, List<DocumentJson>>> resp = new GenericResponseDataJson<HashMap<String, List<DocumentJson>>>();
		Integer InsertIdNum = null;
		try {
			InsertIdNum = Integer.valueOf(insertId);
		} catch (Exception e) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("volumeId may be null or not a number.");
			return resp;
		}

		resp = searchService.findDocumentEntitiesRelatedToInsert(InsertIdNum);

		return resp;
	}

	// Find Document Entities related to Volume
	@RequestMapping(value = "FindVolumeDES/{volumeId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<DocumentJson>>> findVolumeDES(
			@PathVariable String volumeId) {
		GenericResponseDataJson<HashMap<String, List<DocumentJson>>> resp = new GenericResponseDataJson<HashMap<String, List<DocumentJson>>>();

		Integer volumeIdNum = null;
		try {
			volumeIdNum = Integer.valueOf(volumeId);
		} catch (Exception e) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("volumeId may be null or not a number.");
			return resp;
		}

		resp = searchService.findDocumentEntitiesRelatedToVolume(volumeIdNum);

		return resp;
	}

	// Find Archival Entities related to Volume
	@RequestMapping(value = "FindVolumeAES/{volumeId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> findVolumeAES(
			@PathVariable String volumeId) {
		GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> resp = new GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>>();

		Integer volumeIdNum = null;
		try {
			volumeIdNum = Integer.valueOf(volumeId);
		} catch (Exception e) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("volumeId may be null or not a number.");
			return resp;
		}

		resp = searchService.findArchivalEntitiesRelatedToVolume(volumeIdNum);

		return resp;
	}

	// GET Spine by volume
	@RequestMapping(value = "findVolumeSpine/{volumeId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<VolumeSpineJson> findVolumeSpine(
			@PathVariable("volumeId") String volumeId) {

		GenericResponseDataJson<VolumeSpineJson> resp = new GenericResponseDataJson<VolumeSpineJson>();
		Integer volumeIdNum = null;
		try {
			volumeIdNum = Integer.valueOf(volumeId);
		} catch (Exception e) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("volumeId may be null or not a number.");
			return resp;
		} 
		
		try {
			VolumeSpineJson spine = volBaseService.findVolumeSpine(volumeIdNum);
			if(spine != null) {
				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("Got volume spine");
				resp.setData(spine);
			} else {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("No volume exists in DB. ");
				return resp;
			}
		} catch (Exception e) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No volume exists in DB. ");
		}

		return resp;
	}

	// GET Guardia by insert
	@RequestMapping(value = "findInsertGuardia/{insertId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<InsertGuardiaJson> findInsertGuardia(
			@PathVariable("insertId") String volumeId) {

		GenericResponseDataJson<InsertGuardiaJson> resp = new GenericResponseDataJson<InsertGuardiaJson>();
		Integer insertIdNum = null;
		try {
			insertIdNum = Integer.valueOf(volumeId);
		} catch (Exception e) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("insertId may be null or not a number.");
			return resp;
		}
		
		try {
			InsertGuardiaJson guardia = volBaseService.findInsertGuardia(insertIdNum);
			if(guardia != null) {
				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("Got insert guardia");
				resp.setData(guardia);
			} else {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("No insert exists in DB. ");
				return resp;
			}
		} catch (Exception e) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No insert exists in DB. ");
		}
		return resp;
	}

	// localhost:8181/Mia/json/volumeInsertDescription/findInsert/123
	@RequestMapping(value = "findInsert/{insertId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, InsertDescriptionJson>> findInsert(
			@PathVariable("insertId") String insertId) {
		GenericResponseDataJson<HashMap<String, InsertDescriptionJson>> resp = new GenericResponseDataJson<HashMap<String, InsertDescriptionJson>>();

		HashMap<String, InsertDescriptionJson> docHash = new HashMap<String, InsertDescriptionJson>();
		Integer insertIdNum = null;
		try {
			insertIdNum = Integer.valueOf(insertId);
		} catch (Exception e) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("insertId may be null or not a number.");
			return resp;
		}
		InsertEntity insert = uploadService.findInsertById(insertIdNum);

		if (insert != null) {

			InsertDescriptionJson data = new InsertDescriptionJson();
			data.toJson(insert);

			docHash.put("data", data);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got 1 insert in total.");
			resp.setData(docHash);

		} else {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No insert exists in DB. ");

		}
		return resp;
	}

	// localhost:8181/Mia/json/volumeInsertDescription/addVolume
	@RequestMapping(value = "addVolume", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson addVolume(
			@RequestBody VolumeDescriptionJson volumejson,
			HttpServletRequest request) {

		GenericResponseDataJson resp = new GenericResponseDataJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {
			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());
			boolean canCreate = false;
			for (UserRole role : user.getUserRoles()) {
				canCreate = role.containsAuthority(UserAuthority.Authority.ADMINISTRATORS)
						|| role.containsAuthority(UserAuthority.Authority.ONSITE_FELLOWS);
				if(canCreate) break;
			}
			if(!canCreate){
				resp.setMessage("Creation of the volume is not permitted.");
				resp.setStatus(StatusType.ko.toString());
				return resp;
			}
			if (volumejson == null) {
				resp.setMessage("volume may be null or empty.");
				resp.setStatus(StatusType.ko.toString());
				return resp;
			}

			if (volumejson.getCollectionId() == null) {
				resp.setMessage("The collection id may be null or empty.");
				resp.setStatus(StatusType.ko.toString());
				return resp;
			}

			if (volumejson.getVolumeNo() == null) {
				resp.setMessage("The Volume no object may be null or empty.");
				resp.setStatus(StatusType.ko.toString());
				return resp;
			}

			Volume volume = new Volume();
			volume.setVolume(volumejson.getVolumeNo());
			volume.setCollection(Integer.valueOf(volumejson.getCollectionId()));

			boolean existVolume = volBaseService.getVolumeByVolumeNoAndCollectionId(volume) != null;
			if (existVolume) {
				resp.setMessage("The Volume already exist.");
				resp.setStatus(StatusType.ko.toString());
				return resp;
			}
			volume = volBaseService.addNewVolume(volume);
			Schedone schedone = getSchedoneDAO().findBySummaryId(volume.getSummaryId());
			VolumeDescriptionJson volJson = new VolumeDescriptionJson();
			volJson = volJson.toJson(volume, schedone);
			getHistoryLogService().registerAction(
					volume.getSummaryId(), HistoryLogEntity.UserAction.CREATE,
					HistoryLogEntity.RecordType.VOL, null, null);
			resp.setMessage("Volume Name added.");
			resp.setStatus(StatusType.ok.toString());
			resp.setData(volJson);
			return resp;

		} catch (ApplicationThrowable applicationThrowable) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Error saving volume");
			return resp;
		}
	}

	@RequestMapping(value = "modifyVolumeBasicMetadata", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyVolumeBasicMetadata(
			@RequestBody VolumeDescriptionJson volumejson,
			HttpServletRequest request) {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");

		try {
			// leggi il Volume
			Volume volume = volumejson.toVolumeEntity();

			// Calcolare il mese!
			if (volumejson.getStartMonth() != null && !volumejson.getStartMonth().equalsIgnoreCase("")) {
				Month month = volBaseService.decodeMonth(Integer.valueOf(volumejson.getStartMonth()));
				if (month == null) {
					resp.setMessage("Invalid Start Month");
					resp.setStatus(StatusType.ko.toString());
					return resp;
				}
				volume.setStartMonth(month.getMonthName());
				volume.setStartMonthNum(month);
			}
			if (volumejson.getEndMonth() != null && !volumejson.getEndMonth().equalsIgnoreCase("")) {
				Month month = volBaseService.decodeMonth(Integer.valueOf(volumejson.getEndMonth()));
				if (month == null) {
					resp.setMessage("Invalid End Month");
					resp.setStatus(StatusType.ko.toString());
					return resp;
				}
				volume.setEndMonth(month.getMonthName());
				volume.setEndMonthNum(month);
			}
			VolumeJson volJson = new VolumeJson();

			Volume volumeToUpdate = volBaseService.modifyVolumeBasicMetadata(volume);
			if (volumeToUpdate == null) {
				resp.setMessage("Volume not found");
				resp.setStatus(StatusType.ko.toString());
				return resp;
			}
		} catch (ApplicationThrowable applicationThrowable) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Error saving volume");
			return resp;
		}
		resp.setMessage("Volume saved");
		resp.setStatus(StatusType.ok.toString());

		return resp;
	}

	@RequestMapping(value = "modifyVolumeAdvancedMetadata", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyVolumeAdvancedMetadata(
			@RequestBody VolumeDescriptionJson volumejson,
			HttpServletRequest request) {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");

		try {
			// leggi il Volume
			Volume volume = volumejson.toVolumeEntity();
			Schedone schedone = volumejson.toSchedoneEntity();

			VolumeJson volJson = new VolumeJson();

			Volume volumeToUpdate = volBaseService.modifyVolumeAdvancedMetadata(volume, schedone);
			if (volumeToUpdate == null) {
				resp.setMessage("Volume not found");
				resp.setStatus(StatusType.ko.toString());
				return resp;
			}

		} catch (ApplicationThrowable applicationThrowable) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Error saving volume");
			return resp;
		}
		resp.setMessage("Volume saved");
		resp.setStatus(StatusType.ok.toString());

		return resp;
	}

	@RequestMapping(value = "addInsert", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson addInsert(
			@RequestBody InsertDescriptionJson insertJson,
			HttpServletRequest request) {

		GenericResponseDataJson resp = new GenericResponseDataJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		try {
			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());
			boolean canCreate = false;
			CollectionEntity coll = archiveService.findCollectionById(Integer.valueOf(insertJson.getCollectionId()));
			for (UserRole role : user.getUserRoles()) {
				canCreate = role.containsAuthority(UserAuthority.Authority.ADMINISTRATORS)
						|| role.containsAuthority(UserAuthority.Authority.ONSITE_FELLOWS);
				if(canCreate) break;
			}
			if(!canCreate && coll.getCollectionAbbreviation().toUpperCase().equals("MDP")){
				canCreate = true;
			}
			if(!canCreate){
				resp.setMessage("Creation of the insert is not permitted. Regular users can create only inserts in MDP collection");
				resp.setStatus(StatusType.ko.toString());
				return resp;
			}
			if (insertJson == null) {
				resp.setMessage("volume may be null or empty.");
				resp.setStatus(StatusType.ko.toString());
				return resp;
			}

			if (insertJson.getCollectionId() == null) {
				resp.setMessage("The collection object may be null or empty.");
				resp.setStatus(StatusType.ko.toString());
				return resp;
			}

			if (insertJson.getInsertNo() == null) {
				resp.setMessage("The Insert no object may be null or empty.");
				resp.setStatus(StatusType.ko.toString());
				return resp;
			}

			if (insertJson.getVolumeNo() == null) {
				resp.setMessage("The Volume no object may be null or empty.");
				resp.setStatus(StatusType.ko.toString());
				return resp;
			}
			InsertEntity insert = new InsertEntity();

			Volume volume = new Volume();
			volume.setVolume(insertJson.getVolumeNo());
			volume.setCollection(Integer.valueOf(insertJson.getCollectionId()));

			volume = volBaseService.getVolumeByVolumeNoAndCollectionId(volume);
			if (volume == null) {
				resp.setMessage("The Volume not found.");
				resp.setStatus(StatusType.ko.toString());
				return resp;
			}
			if(U.any(volume.getUploadInfoEntities(), new Predicate<UploadInfoEntity>() {
				@Override
				public boolean test(UploadInfoEntity uploadInfoEntity) {
					if(uploadInfoEntity.getLogicalDelete() == null && uploadInfoEntity.getInsertEntity() == null) return true;
					else return uploadInfoEntity.getLogicalDelete() == 0 && uploadInfoEntity.getInsertEntity() == null;
				}
			})){
				resp.setMessage("Cannot create insert. Volume has active Archival Entities.");
				logger.error("Cannot create an upload. Volume has active Archival Entities.");
				resp.setStatus(StatusType.ko.toString());
				return resp;
			}
			insert.setInsertName(insertJson.getInsertNo());
			insert.setVolume(volume.getSummaryId());
			insert.setVolumeEntity(volume);

			boolean existVolume = volumeInsertDescriptionService.getInsertByVolumeAndInsertName(insert) != null;
			if (existVolume) {
				resp.setMessage("The Volume already exist.");
				resp.setStatus(StatusType.ko.toString());
				return resp;
			}
			InsertEntity ins = volumeInsertDescriptionService.addNewInsert(insert);
			InsertDescriptionJson data = new InsertDescriptionJson();
			data = data.toJson(ins);
			getHistoryLogService().registerAction(
					insert.getInsertId(), HistoryLogEntity.UserAction.CREATE,
					HistoryLogEntity.RecordType.INS, null, null);
			resp.setData(data);
			resp.setMessage("Insert added.");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		} catch (ApplicationThrowable applicationThrowable) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Error saving insert");
			return resp;
		}
	}

	@RequestMapping(value = "modifyInsertBasicMetadata", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyInsertBasicMetadata(
			@RequestBody InsertDescriptionJson insertJson,
			HttpServletRequest request) {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");

		try {
			// leggi il Volume
			InsertEntity insert = insertJson.toEntity();
			InsertDescriptionJson data = new InsertDescriptionJson();
			data = data.toJson(insert);

			InsertEntity insertUdated = volBaseService.modifyInsertBasicMetadata(insert);
			if (insertUdated == null) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("No insert exist in DB");
				return resp;
			}
		} catch (ApplicationThrowable applicationThrowable) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Error saving insert");
			return resp;
		}
		resp.setMessage("Insert saved");
		resp.setStatus(StatusType.ok.toString());

		return resp;
	}

	@RequestMapping(value = "modifyInsertAdvancedMetadata", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyInsertAdvancedMetadata(
			@RequestBody InsertDescriptionJson insertJson,
			HttpServletRequest request) {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");

		try {
			// leggi il Volume
			InsertEntity insert = insertJson.toEntity();
			InsertDescriptionJson data = new InsertDescriptionJson();
			data = data.toJson(insert);

			InsertEntity insertUdated = volBaseService.modifyInsertAdvancedMetadata(insert);
			if (insertUdated == null) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("No insert exist in DB");
				return resp;
			}
		} catch (ApplicationThrowable applicationThrowable) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Error saving insert");
			return resp;
		}
		resp.setMessage("Insert saved");
		resp.setStatus(StatusType.ok.toString());

		return resp;
	}

	@RequestMapping(value = "deleteVolume/{volumeId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseJson deleteVolume(
			@PathVariable("volumeId") Integer volumeId,
			HttpServletRequest request) {

		GenericResponseDataJson<HashMap<String, List<Integer>>> resp = new GenericResponseDataJson<HashMap<String, List<Integer>>>();

		HashMap<String, List<Integer>> checks = volumeInsertDescriptionService
				.checkDataBeforeDeleteVolumeById(volumeId);
		if (checks == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Volume not exist in DB");
			resp.setData(checks);
			return resp;
		}
		if (checks != null && !checks.isEmpty()) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage(
					"This volume is connected to some records and can't be deleted. You should delete those records first.");
			resp.setData(checks);
			return resp;
		} else {
			getHistoryLogService().registerAction(
					volumeId, HistoryLogEntity.UserAction.DELETE,
					HistoryLogEntity.RecordType.VOL, null, null);
			GenericResponseJson deleteRes = volumeInsertDescriptionService.deleteVolumeById(volumeId);
			resp.setStatus(deleteRes.getStatus());
			resp.setMessage(deleteRes.getMessage());
			return resp;
		}
	}

	@RequestMapping(value = "deleteInsert/{insertId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseJson deleteInsert(
			@PathVariable("insertId") Integer insertId,
			HttpServletRequest request) {
		GenericResponseDataJson<HashMap<String, List<Integer>>> resp = new GenericResponseDataJson<HashMap<String, List<Integer>>>();

		HashMap<String, List<Integer>> checks = volumeInsertDescriptionService
				.checkDataBeforeDeleteInsertById(insertId);
		if (checks == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Insert not exist in DB");
			resp.setData(checks);
			return resp;
		}

		if (checks != null && !checks.isEmpty()) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage(
					"This inserts is connected to some records and can't be deleted. You should delete those records first.");
			resp.setData(checks);
			return resp;
		} else {
			InsertEntity insert = archiveService.findInsertById(insertId);
			getHistoryLogService().registerAction(
					insert.getInsertId(), HistoryLogEntity.UserAction.DELETE,
					HistoryLogEntity.RecordType.INS, null, null);
			GenericResponseJson deleteRes = volumeInsertDescriptionService.deleteInsertById(insertId);
			resp.setStatus(deleteRes.getStatus());
			resp.setMessage(deleteRes.getMessage());
			return resp;
		}
	}

	@RequestMapping(value = "uploadVolumeSpine", method = RequestMethod.POST)
	public @ResponseBody HashMap<String, GenericUploadResponseJson> uploadVolumeSpine(
			@RequestParam("filesForm") MultipartFile filesForm,
			@RequestParam("repositoryId") String repositoryId,
			@RequestParam("volumeId") String volumeId,
			HttpSession session,
			HttpServletRequest request) {

		HashMap<String, GenericUploadResponseJson> respHash = new HashMap<String, GenericUploadResponseJson>();
		GenericUploadResponseJson resp = new GenericUploadResponseJson();
		// Recovery User Name
		String owner = "";
		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()).getUsername());


		// Repository
		RepositoryEntity repEntity = new RepositoryEntity();
		try {
			repEntity.setRepositoryId(Integer.valueOf(repositoryId));
		} catch (Exception e) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("repositoryId may be null or not a number.");;
			respHash.put("uploadImages-ko", resp);
			return respHash;
		}

		// Volume
		Volume volEntity = new Volume();
		try {	
			volEntity.setSummaryId(Integer.valueOf(volumeId));
		} catch(Exception e) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("volumeId may be null or not a number.");;
			respHash.put("uploadImages-ko", resp);
			return respHash;
		}

		List<MultipartFile> listFile = new ArrayList<MultipartFile>();
		if(filesForm != null && !filesForm.isEmpty()) {
			listFile.add(filesForm);
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("filesForm is null.");
			respHash.put("uploadImages-ko", resp);
			return respHash;
		}
		owner = user.getAccount();

		UploadBean uploadBean = new UploadBean(listFile, null,
				repEntity, null, null, volEntity, null, owner,
				"", "spine");
		UploadBean uploadBeanAfterInsert = null;

		try {
			uploadBeanAfterInsert = uploadService
					.insertUploadOptionalFile(uploadBean);
		} catch (Exception e) {
			resp.setStatus("KO");
			resp.setMessage("Upload Failed!!");
			respHash.put("uploadImages-ko", resp);	
			return respHash;	
		}


		session.setAttribute("futureUploadResp", null);

		// 3. Convert files to tiff
		Future<List<GenericUploadResponseJson>> futureUploadResp = uploadService
				.convertFiles(uploadBeanAfterInsert.getUploadFileEntities(),
						uploadBeanAfterInsert.getRealPath(), true);

		session.setAttribute("futureUploadResp",
				(Future<List<GenericUploadResponseJson>>) futureUploadResp);

		// Implementation of the JSON object to return to the FE, as written in
		// the documentation.
		// The test if the upload is OK is made if the RealPath exists, is it
		// OK??
		String ret = uploadBeanAfterInsert.getRealPath();
		if (ret != null && !ret.isEmpty()) {
			HashMap<String, Object> changes = new HashMap<String, Object>();
			HashMap<String, HashMap> category = new HashMap<String, HashMap>();
			HashMap<String, HashMap> action = new HashMap<String, HashMap>();
			changes.put("spine", "Uploaded new spine");
			action.put("edit", changes);
			category.put("spine", action);
			getHistoryLogService().registerAction(
					volEntity.getSummaryId(), HistoryLogEntity.UserAction.EDIT,
					HistoryLogEntity.RecordType.VOL, null,category);
			resp.setStatus("OK");
			resp.setMessage("The Upload process performed successfully!!");
			resp.setUploadInfoId(uploadBeanAfterInsert.getUploadInfoId());
			resp.setFileName(uploadBeanAfterInsert.getUploadFileEntities().get(0).getFilename());
			resp.setOriginalFileName(uploadBeanAfterInsert.getUploadFileEntities().get(0).getFileOriginalName());
			resp.setFileId(uploadBeanAfterInsert.getUploadFileEntities().get(0).getUploadFileId());
			respHash.put("uploadImages-ok", resp);
		} else {
			resp.setStatus("KO");
			resp.setMessage("Upload Failed!!");
			respHash.put("uploadImages-ko", resp);
		}
		return respHash;
	}
	

	@RequestMapping(value = "uploadInsertGuardia", method = RequestMethod.POST)
	public @ResponseBody HashMap<String, GenericUploadResponseJson> uploadInsertGuardia(
			@RequestParam("filesForm") MultipartFile filesForm,
			@RequestParam("repositoryId") String repositoryId,
			@RequestParam("insertId") String insertId,
			HttpSession session,
			HttpServletRequest request) {

		HashMap<String, GenericUploadResponseJson> respHash = new HashMap<String, GenericUploadResponseJson>();
		GenericUploadResponseJson resp = new GenericUploadResponseJson();
		// Recovery User Name
		String owner = "";
		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()).getUsername());


		// Repository
		RepositoryEntity repEntity = new RepositoryEntity();
		try {
			repEntity.setRepositoryId(Integer.valueOf(repositoryId));
		} catch (Exception e) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("repositoryId may be null or not a number.");;
			respHash.put("uploadImages-ko", resp);
			return respHash;
		}

		// Volume
		InsertEntity insEntity = new InsertEntity();
		try {	
			insEntity.setInsertId(Integer.valueOf(insertId));
		} catch(Exception e) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("insertId may be null or not a number.");;
			respHash.put("uploadImages-ko", resp);
			return respHash;
		}

		InsertEntity insert = archiveService.findInsertById(Integer.valueOf(insertId));

		List<MultipartFile> listFile = new ArrayList<MultipartFile>();
		if(filesForm != null && !filesForm.isEmpty()) {
			listFile.add(filesForm);
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("filesForm is null.");
			respHash.put("uploadImages-ko", resp);
			return respHash;
		}
		owner = user.getAccount();

		UploadBean uploadBean = new UploadBean(listFile, null,
				repEntity, null, null, null, insEntity, owner,
				"", "guardia");
		UploadBean uploadBeanAfterInsert = null;
		try {
			uploadBeanAfterInsert = uploadService
					.insertUploadOptionalFile(uploadBean);
		} catch (Exception e) {
			resp.setStatus("KO");
			resp.setMessage("Upload Failed!!");
			respHash.put("uploadImages-ko", resp);	
			return respHash;
		}


		session.setAttribute("futureUploadResp", null);

		// 3. Convert files to tiff
		Future<List<GenericUploadResponseJson>> futureUploadResp = uploadService
				.convertFiles(uploadBeanAfterInsert.getUploadFileEntities(),
						uploadBeanAfterInsert.getRealPath(), true);

		session.setAttribute("futureUploadResp",
				(Future<List<GenericUploadResponseJson>>) futureUploadResp);

		// Implementation of the JSON object to return to the FE, as written in
		// the documentation.
		// The test if the upload is OK is made if the RealPath exists, is it
		// OK??
		String ret = uploadBeanAfterInsert.getRealPath();
		if (ret != null && !ret.isEmpty()) {

			HashMap<String, Object> changes = new HashMap<String, Object>();
			HashMap<String, HashMap> category = new HashMap<String, HashMap>();
			HashMap<String, HashMap> action = new HashMap<String, HashMap>();
			changes.put("guardia", "Uploaded new guardia");
			action.put("edit", changes);
			category.put("guardia", action);
			getHistoryLogService().registerAction(
					insert.getInsertId(), HistoryLogEntity.UserAction.EDIT,
					HistoryLogEntity.RecordType.INS, null,category);
			resp.setStatus("OK");
			resp.setMessage("The Upload process performed successfully!!");
			resp.setUploadInfoId(uploadBeanAfterInsert.getUploadInfoId());
			resp.setFileName(uploadBeanAfterInsert.getUploadFileEntities().get(0).getFilename());
			resp.setOriginalFileName(uploadBeanAfterInsert.getUploadFileEntities().get(0).getFileOriginalName());
			resp.setFileId(uploadBeanAfterInsert.getUploadFileEntities().get(0).getUploadFileId());
			respHash.put("uploadImages-ok", resp);
		} else {
			resp.setStatus("KO");
			resp.setMessage("Upload Failed!!");
			respHash.put("uploadImages-ko", resp);
		}
		return respHash;
	}
	
	public UserService getUserService() {
		return userService;
	}
	
	public RepositoryDAO getRepositoryDAO() {
		return repositoryDAO;
	}
	
	public SchedoneDAO getSchedoneDAO() {
		return schedoneDAO;
	}

	public HistoryLogService getHistoryLogService() {
		return historyLogService;
	}
}
