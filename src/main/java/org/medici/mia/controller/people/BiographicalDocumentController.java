package org.medici.mia.controller.people;

import java.util.HashMap;
import java.util.Map;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.document.DocumentPeopleJson;
import org.medici.mia.service.people.BiographicalDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Controller
@RequestMapping("/biodoc")
public class BiographicalDocumentController {

	@Autowired
	private BiographicalDocumentService biographicalDocumentService;

	// http://localhost:8181/Mia/json/biodoc/countAllDocumentsPeople/131
	@RequestMapping(value = "countAllDocumentsPeople/{personId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, Integer>> countAllDocumentsPeople(
			@PathVariable("personId") Integer personId) {

		Integer count = getBiographicalDocumentService()
				.getCountAllDocumentsPeople(personId);

		GenericResponseDataJson<HashMap<String, Integer>> resp = new GenericResponseDataJson<HashMap<String, Integer>>();
		HashMap<String, Integer> docHash = new HashMap<String, Integer>();

		docHash.put("countDocuments", count);
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage("Got " + count + " documents related to this personId.");
		resp.setData(docHash);
		return resp;

	}

	// http://localhost:8181/Mia/json/biodoc/findDocumentsPeople/470
	@RequestMapping(value = "findDocumentsPeople/{personId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, DocumentPeopleJson>> findDocumentsPeople(
			@PathVariable("personId") Integer personId) {

		DocumentPeopleJson documentsPeople = getBiographicalDocumentService()
				.getDocumentsPeople(personId);

		GenericResponseDataJson<HashMap<String, DocumentPeopleJson>> resp = new GenericResponseDataJson<HashMap<String, DocumentPeopleJson>>();
		HashMap<String, DocumentPeopleJson> docHash = new HashMap<String, DocumentPeopleJson>();

		if (documentsPeople != null) {
			docHash.put("documentsPeople", documentsPeople);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got documents related to this personId.");
			resp.setData(docHash);
			return resp;
		} else {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No Document related to this personId exist in DB. ");
			return resp;
		}

	}

	// http://localhost:8181/Mia/json/document/countCategoryAndFieldsDocumentsPeople/131
	@RequestMapping(value = "countCategoryAndFieldsDocumentsPeople/{personId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, Map<String, Map<String, Integer>>>> countCategoryAndFieldsDocumentsPeople(
			@PathVariable("personId") Integer personId) {

		Map<String, Map<String, Integer>> documentsPeople = getBiographicalDocumentService()
				.getDocumentsCategoryAndFieldCountPeople(personId);

		GenericResponseDataJson<HashMap<String, Map<String, Map<String, Integer>>>> resp = new GenericResponseDataJson<HashMap<String, Map<String, Map<String, Integer>>>>();
		HashMap<String, Map<String, Map<String, Integer>>> docHash = new HashMap<String, Map<String, Map<String, Integer>>>();

		if (documentsPeople != null && !documentsPeople.isEmpty()) {
			docHash.put("countDocumentFields", documentsPeople);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + documentsPeople.size()
					+ " documents category related to this personId.");
			resp.setData(docHash);
			return resp;
		} else {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No Document related to this personId exist in DB. ");
			return resp;
		}

	}

	// http://localhost:8181/Mia/json/document/countCategoryDocumentsPeople/131
	@RequestMapping(value = "countCategoryDocumentsPeople/{personId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, Map<String, Integer>>> countCategoryDocumentsPeople(
			@PathVariable("personId") Integer personId) {

		Map<String, Integer> documentsPeople = getBiographicalDocumentService()
				.getDocumentsCategoryCountPeople(personId);

		GenericResponseDataJson<HashMap<String, Map<String, Integer>>> resp = new GenericResponseDataJson<HashMap<String, Map<String, Integer>>>();
		HashMap<String, Map<String, Integer>> docHash = new HashMap<String, Map<String, Integer>>();

		if (documentsPeople != null && !documentsPeople.isEmpty()) {
			docHash.put("countDocumentCategories", documentsPeople);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + documentsPeople.size()
					+ " documents category related to this personId.");
			resp.setData(docHash);
			return resp;
		} else {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No Document related to this personId exist in DB. ");
			return resp;
		}

	}

	public BiographicalDocumentService getBiographicalDocumentService() {
		return biographicalDocumentService;
	}

	public void setBiographicalDocumentService(
			BiographicalDocumentService biographicalDocumentService) {
		this.biographicalDocumentService = biographicalDocumentService;
	}

}