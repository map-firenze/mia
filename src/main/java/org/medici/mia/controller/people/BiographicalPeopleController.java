package org.medici.mia.controller.people;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.ModifyPersonJson;
import org.medici.mia.common.json.PersonJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.biographical.AddHeadquarterJson;
import org.medici.mia.common.json.biographical.BiographicalPeopleJson;
import org.medici.mia.common.json.biographical.FindPersonChildrenJson;
import org.medici.mia.common.json.biographical.FindPersonParentsJson;
import org.medici.mia.common.json.biographical.FindPersonSpousesJson;
import org.medici.mia.common.json.biographical.FindPersonTitlesOccsJson;
import org.medici.mia.common.json.biographical.HeadquarterJson;
import org.medici.mia.common.json.biographical.ModifyPersonChildJson;
import org.medici.mia.common.json.biographical.ModifyPersonParentsJson;
import org.medici.mia.common.json.biographical.ModifyPersonSpouseJson;
import org.medici.mia.common.json.biographical.ModifyPersonTitlesOccsJson;
import org.medici.mia.common.json.biographical.OrganizationJson;
import org.medici.mia.common.json.biographical.PersonPortraitJson;
import org.medici.mia.common.json.biographical.TitleOccJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.controller.upload.FileUploadForm;
import org.medici.mia.domain.CaseStudyItem;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.service.casestudy.CaseStudyService;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.people.BiographicalPeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Controller
@RequestMapping("/biographical")
public class BiographicalPeopleController {

	@Autowired
	private BiographicalPeopleService biographicalPeopleService;

	@Autowired
	private MiaDocumentService miaDocumentService;

	@Autowired
	private HistoryLogService historyLogService;

	@Autowired
	private CaseStudyService caseStudyService;

	@RequestMapping(value = "modifyBiographicalPeople", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyBiographicalPeople(
			@RequestBody BiographicalPeopleJson peopleToModify,
			HttpServletRequest request) {
		GenericResponseJson resp = new GenericResponseJson();
		if (peopleToModify != null && peopleToModify.getPeopleId() != null) {
			resp = getBiographicalPeopleService().modifyBiographicalPeople(
					peopleToModify);
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Data not valid to modify people");
		}
		return resp;
	}

	// http://localhost:8181/Mia/json/biographical/findBiographicalPeople/116
	@RequestMapping(value = "findBiographicalPeople/{peopleId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<BiographicalPeopleJson> findBiographicalPeople(
			@PathVariable("peopleId") Integer peopleId) {
		GenericResponseDataJson<BiographicalPeopleJson> resp = new GenericResponseDataJson<BiographicalPeopleJson>();
		BiographicalPeopleJson ppl = getBiographicalPeopleService()
				.findBiographicalPeople(peopleId);
		if (ppl.getPeopleId() != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got Biographical people with id: " + peopleId);
			resp.setData(ppl);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No Biografical people found with id: " + peopleId);
			return resp;
		}

	}

	// http://localhost:8181/Mia/json/biographical/findPersonNames/116
	@RequestMapping(value = "findPersonNames/{peopleId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<PersonJson> findPersonNames(
			@PathVariable("peopleId") Integer peopleId) {
		GenericResponseDataJson<PersonJson> resp = new GenericResponseDataJson<PersonJson>();
		PersonJson ppl = getBiographicalPeopleService().findPersonNames(
				peopleId);
		if (ppl.getPersonId() != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got Biographical people with id: " + peopleId);
			resp.setData(ppl);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No Biographical people found with id: " + peopleId);
			return resp;
		}

	}

	@RequestMapping(value = "modifyPersonName", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyPersonName(
			@RequestBody ModifyPersonJson personToModify,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();
		if (personToModify == null || personToModify.getPersonId() == null
				|| personToModify.getPersonId().equals("")) {
			resp.setMessage("personId may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		if (personToModify.getNewPersonName() == null
				|| personToModify.getOldPersonName() == null) {
			resp.setMessage("The old person Name or the new person name object may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		if (personToModify.getNewPersonName().getAltNameId() == null
				|| personToModify.getOldPersonName().getAltNameId() == null) {
			resp.setMessage("The old altNameId or the newAltnameId may be null.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		if (personToModify.getNewPersonName().getAltName() == null
				|| personToModify.getOldPersonName().getAltName() == null) {
			resp.setMessage("The old altName or the new altName may be null.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		if (personToModify.getNewPersonName().getNameType() == null
				|| personToModify.getOldPersonName().getNameType() == null) {
			resp.setMessage("The old nameType or the new nameType may be null.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		if (!personToModify.getOldPersonName().getAltNameId()
				.equals(personToModify.getNewPersonName().getAltNameId())) {
			resp.setMessage("The oldAltNameId is not the same as newAltnameId.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		return getBiographicalPeopleService().modifyPersonName(personToModify);
	}

	@RequestMapping(value = "addPersonName", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson addPersonName(
			@RequestBody ModifyPersonJson personToModify,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();
		if (personToModify == null) {
			resp.setMessage("personId may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		if (personToModify.getNewPersonName() == null) {
			resp.setMessage("The new person Name object may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		return getBiographicalPeopleService().addPersonName(personToModify);
	}

	@RequestMapping(value = "deletePersonName", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson deletePersonName(
			@RequestBody ModifyPersonJson personToModify,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();
		if (personToModify == null) {
			resp.setMessage("personId may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		if (personToModify.getPersonId() == null
				|| personToModify.getAltNameToBeDeleted() == null) {
			resp.setMessage("PersonId or nameToBeDeletdId may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		return getBiographicalPeopleService().deletePersonName(personToModify);
	}

	// Titles & Occupations

	// localhost:8181/Mia/json/biographical/findTitlesAndOccs/ABC
	@RequestMapping(value = "findTitlesAndOccs/{titleOccName}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<TitleOccJson>>> findTitlesAndOccs(
			@PathVariable("titleOccName") String titleOccName) {

		List<TitleOccJson> titleOccsJson = getBiographicalPeopleService()
				.findTitlesAndOccs(titleOccName);
		GenericResponseDataJson<HashMap<String, List<TitleOccJson>>> resp = new GenericResponseDataJson<HashMap<String, List<TitleOccJson>>>();
		HashMap<String, List<TitleOccJson>> docHash = new HashMap<String, List<TitleOccJson>>();
		if (titleOccsJson != null && !titleOccsJson.isEmpty()) {
			docHash.put("arrayTitleAndOccs", titleOccsJson);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + titleOccsJson.size()
					+ " Titles and Occupations in total.");
			resp.setData(docHash);

		} else {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No Tittle and Ocuupation exists in DB. ");

		}

		return resp;

	}

	// http://localhost:8181/Mia/json/biographical/findPersonTitlesAndOccupations/822
	@RequestMapping(value = "findPersonTitlesAndOccupations/{personId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<FindPersonTitlesOccsJson> findPersonTitlesAndOccupations(
			@PathVariable("personId") Integer personId) {
		GenericResponseDataJson<FindPersonTitlesOccsJson> resp = new GenericResponseDataJson<FindPersonTitlesOccsJson>();
		FindPersonTitlesOccsJson ppl = getBiographicalPeopleService()
				.findPersonTitlesAndOccupations(personId);
		if (ppl.getPersonId() != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got person titles and occupations with id:"
					+ personId);
			resp.setData(ppl);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No Person titles and occupations found with id: "
					+ personId);
			return resp;
		}

	}

	@RequestMapping(value = "addPersonTitleAndOccupation", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson addPersonTitleAndOccupation(
			@RequestBody ModifyPersonTitlesOccsJson personTitleOccToAdd,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();
		if (personTitleOccToAdd == null
				|| personTitleOccToAdd.getNewTitleAndOccupation() == null
				|| personTitleOccToAdd.getPersonId() == null) {
			resp.setMessage("personId or newTitleAndOccupation may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		return getBiographicalPeopleService().addPersonTitleAndOccupation(
				personTitleOccToAdd);
	}

	@RequestMapping(value = "deletePersonTitleAndOccupation", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson deletePersonTitleAndOccupation(
			@RequestBody ModifyPersonTitlesOccsJson personTitleOccToAdd,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();
		if (personTitleOccToAdd == null
				|| personTitleOccToAdd.getTitleAndOccupationToBeDeleted() == null
				|| personTitleOccToAdd.getPersonId() == null) {
			resp.setMessage("personId or titleAndOccupationToBeDeleted may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		return getBiographicalPeopleService().deletePersonTitleAndOccupation(
				personTitleOccToAdd);
	}

	@RequestMapping(value = "modifyPersonTitleAndOccupation", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyPersonTitleAndOccupation(
			@RequestBody ModifyPersonTitlesOccsJson personTitleOccToAdd,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();

		if (personTitleOccToAdd == null
				|| personTitleOccToAdd.getPersonId() == null) {
			resp.setMessage("personTitleOccToAdd or personId may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		if (personTitleOccToAdd.getNewTitleAndOccupation() == null) {
			resp.setMessage("newTitleAndOccupation may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		if (personTitleOccToAdd.getOldTitleAndOccupation() == null) {
			resp.setMessage("personId or oldTitleAndOccupation may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		return getBiographicalPeopleService().modifyPersonTitleAndOccupation(
				personTitleOccToAdd);
	}

	// Person Parents

	// http://localhost:8181/Mia/json/biographical/findPersonParents/822
	@RequestMapping(value = "findPersonParents/{personId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<FindPersonParentsJson> findPersonParents(
			@PathVariable("personId") Integer personId) {
		GenericResponseDataJson<FindPersonParentsJson> resp = new GenericResponseDataJson<FindPersonParentsJson>();
		FindPersonParentsJson ppl = getBiographicalPeopleService()
				.findBiographicalPersonParents(personId);
		if (ppl != null && ppl.getPersonId() != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got person parents with id:" + personId);
			resp.setData(ppl);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No Person parents found with id: " + personId);
			return resp;
		}

	}

	@RequestMapping(value = "modifyPersonParents", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyPersonParents(
			@RequestBody ModifyPersonParentsJson modifyJson,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();

		if (modifyJson == null || modifyJson.getPersonId() == null) {
			resp.setMessage("personId may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		return getBiographicalPeopleService().modifyBiographicalPersonParents(
				modifyJson);
	}

	@RequestMapping(value = "addPersonParents", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson addPersonParents(
			@RequestBody ModifyPersonParentsJson addJson,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();

		if (addJson == null || addJson.getPersonId() == null) {
			resp.setMessage("personId may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		return getBiographicalPeopleService().addBiographicalPersonParents(
				addJson);
	}

	@RequestMapping(value = "deletePersonParents", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson deletePersonParents(
			@RequestBody ModifyPersonParentsJson deleteJson,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();

		if (deleteJson == null || deleteJson.getPersonId() == null
				|| deleteJson.getParentToBeDeleted() == null) {
			resp.setMessage("personId or parentToBeDeleted may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		return getBiographicalPeopleService().deleteBiographicalPersonParents(
				deleteJson);
	}

	// person childs

	// http://localhost:8181/Mia/json/biographical/findPersonChildren/822
	@RequestMapping(value = "findPersonChildren/{personId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<FindPersonChildrenJson> findPersonChildren(
			@PathVariable("personId") Integer personId) {
		GenericResponseDataJson<FindPersonChildrenJson> resp = new GenericResponseDataJson<FindPersonChildrenJson>();
		FindPersonChildrenJson children = getBiographicalPeopleService()
				.findBiographicalPersonChildren(personId);
		if (children != null && children.getPersonId() != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got person Children with id:" + personId);
			resp.setData(children);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No Person Children found with id: " + personId);
			return resp;
		}

	}

	@RequestMapping(value = "modifyPersonChild", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyPersonChild(
			@RequestBody ModifyPersonChildJson modifyJson,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();

		if (modifyJson == null || modifyJson.getPersonId() == null
				|| modifyJson.getOldChildId() == null
				|| modifyJson.getNewChildId() == null) {
			resp.setMessage("personId or newChildId or oldChildID may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		return getBiographicalPeopleService().modifyBiographicalPersonChild(
				modifyJson);
	}

	@RequestMapping(value = "addPersonChild", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson addPersonChild(
			@RequestBody ModifyPersonChildJson modifyJson,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();

		if (modifyJson == null || modifyJson.getPersonId() == null
				|| modifyJson.getNewChildId() == null) {
			resp.setMessage("personId or newChildId may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		return getBiographicalPeopleService().addBiographicalPersonChild(
				modifyJson);
	}

	@RequestMapping(value = "deletePersonChild", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson deletePersonChild(
			@RequestBody ModifyPersonChildJson modifyJson,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();

		if (modifyJson == null || modifyJson.getPersonId() == null
				|| modifyJson.getChildToBeDeleted() == null) {
			resp.setMessage("personId or childTiBeDeleted may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		return getBiographicalPeopleService().deleteBiographicalPersonChild(
				modifyJson);
	}

	// http://localhost:8181/Mia/json/biographical/findPersonSpouses/822
	@RequestMapping(value = "findPersonSpouses/{personId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<FindPersonSpousesJson> findPersonSpouses(
			@PathVariable("personId") Integer personId) {
		GenericResponseDataJson<FindPersonSpousesJson> resp = new GenericResponseDataJson<FindPersonSpousesJson>();
		FindPersonSpousesJson ppl = getBiographicalPeopleService()
				.findBiographicalPersonSpouses(personId);
		if (ppl != null && ppl.getPersonId() != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got person spouses with id: " + personId);
			resp.setData(ppl);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No Person Spouses found with id: " + personId);
			return resp;
		}

	}

	@RequestMapping(value = "modifyPersonSpouse", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyPersonSpouse(
			@RequestBody ModifyPersonSpouseJson modifyJson,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();

		if (modifyJson == null || modifyJson.getPersonId() == null
				|| modifyJson.getOldSpouse() == null
				|| modifyJson.getNewSpouse() == null
				|| modifyJson.getOldSpouse().getPersonId() == null
				|| modifyJson.getNewSpouse().getPersonId() == null) {
			resp.setMessage("Data in Input may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		return getBiographicalPeopleService().modifyBiographicalPersonSpouse(
				modifyJson);
	}

	@RequestMapping(value = "addPersonSpouse", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson addPersonSpouse(
			@RequestBody ModifyPersonSpouseJson modifyJson,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();

		if (modifyJson == null || modifyJson.getPersonId() == null
				|| modifyJson.getNewSpouse() == null
				|| modifyJson.getNewSpouse().getPersonId() == null) {
			resp.setMessage("personId or newSpouse or spouseId may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		return getBiographicalPeopleService().addBiographicalPersonSpouse(
				modifyJson);
	}

	@RequestMapping(value = "deletePersonSpouse", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson deletePersonSpouse(
			@RequestBody ModifyPersonSpouseJson modifyJson,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();

		if (modifyJson == null || modifyJson.getPersonId() == null
				|| modifyJson.getSpouseToBeDeleted() == null) {
			resp.setMessage("personId or childTiBeDeleted may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		return getBiographicalPeopleService().deleteBiographicalPersonSpouse(
				modifyJson);
	}

	// http://localhost:8181/Mia/json/biographical/deletePerson/822
	@RequestMapping(value = "deletePerson/{personId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<Integer>> deletePerson(
			@PathVariable("personId") Integer personId,
			HttpServletRequest request) {

		GenericResponseDataJson<List<Integer>> resp = new GenericResponseDataJson<List<Integer>>();
		List<String> people = new ArrayList<String>();
		people.add(String.valueOf(personId));
		List<Integer> docIds = getMiaDocumentService().findDocumentIdsByPeople(
				people);

		if (docIds != null && !docIds.isEmpty()) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("This person is used in documents and can't be deleted. You should delete person from all documents.");
			resp.setData(docIds);
			return resp;
		} else {

			// control for refToPeople
			List<Integer> docs = getBiographicalPeopleService()
					.findDocsRefToPeople(personId);

			if (docs != null && !docs.isEmpty()) {
				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("This person is reffered in documents and can't be deleted. You should delete person from all documents.");
				resp.setData(docs);
				return resp;
			} else {
				caseStudyService.deleteAttachedItems(personId, CaseStudyItem.EntityType.BIO);
				GenericResponseJson deleteRes = getBiographicalPeopleService()
						.deleteBiographicalPerson(personId);
				resp.setStatus(deleteRes.getStatus());
				resp.setMessage(deleteRes.getMessage());
				return resp;
			}

		}

	}

	// http://localhost:8181/Mia/json/biographical/undeletePerson/19573
	@RequestMapping(value = "undeletePerson/{personId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<Integer>> undeletePerson(
			@PathVariable("personId") Integer personId,
			HttpServletRequest request) {

		GenericResponseDataJson<List<Integer>> resp = new GenericResponseDataJson<List<Integer>>();

		GenericResponseJson deleteRes = getBiographicalPeopleService()
				.unDeleteBiographicalPerson(personId);
		resp.setStatus(deleteRes.getStatus());
		resp.setMessage(deleteRes.getMessage());
		return resp;

	}

	// http://mia.medici.org:8080/Mia/json/biographical/findPersonPotraitDetails/19573
	@RequestMapping(value = "findPersonPotraitDetails/{personId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<PersonPortraitJson> findPersonPotraitDetails(
			@PathVariable("personId") Integer personId) {
		GenericResponseDataJson<PersonPortraitJson> resp = new GenericResponseDataJson<PersonPortraitJson>();
		PersonPortraitJson ppl = getBiographicalPeopleService()
				.findPersonPotraitDetails(personId);
		if (ppl.getPersonId() != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got person portrait with peson id: " + personId);
			resp.setData(ppl);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No portrait found with person id: " + personId);
			return resp;
		}

	}

	@RequestMapping(value = "modifyPersonPortraitDetails", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyPersonPortraitDetails(
			@RequestBody PersonPortraitJson modifyPortrait,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();

		if (modifyPortrait == null || modifyPortrait.getPersonId() == null) {
			resp.setMessage("personId or modifyPortrait may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		return getBiographicalPeopleService().modifyPersonPortraitDetails(
				modifyPortrait);
	}

	@RequestMapping(value = "uploadPortraitUser", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson uploadPortraitUser(
			@ModelAttribute("files") FileUploadForm filesForm,
			@ModelAttribute("personId") Integer personId, HttpSession session,
			HttpServletRequest request) {
		GenericResponseJson resp = new GenericResponseJson();
		if (filesForm == null || filesForm.getFiles() == null
				|| filesForm.getFiles().isEmpty()) {
			resp.setMessage("User portrait image may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		return getBiographicalPeopleService().uploadPersonPortrait(
				filesForm.getFiles().get(0), personId);

	}

	// http://localhost:8181/Mia/json/biographical/findHeadquarters/116
	@RequestMapping(value = "findHeadquarters/{personId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<OrganizationJson> findHeadquarters(
			@PathVariable("personId") Integer personId) {
		GenericResponseDataJson<OrganizationJson> resp = new GenericResponseDataJson<OrganizationJson>();
		OrganizationJson ppl = getBiographicalPeopleService().findHeadquarters(
				personId);
		if (ppl != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got headquarters with id: " + personId);
			resp.setData(ppl);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No Headquarters found with id: " + personId);
			return resp;
		}

	}

	@RequestMapping(value = "addHeadquarter", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson addHeadquarter(
			@RequestBody AddHeadquarterJson head, HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();

		if (head == null || head.getPersonId() == null) {
			resp.setMessage("personId  may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;

		}

		if (head.getPlaceId() != null) {
			Integer pId = getMiaDocumentService()
					.findPrinicipalPlaceIdByVariantPlaceId(head.getPlaceId());
			head.setPlaceId(pId);

		}
		return getBiographicalPeopleService().addHeadquarter(head);

	}

	@RequestMapping(value = "deleteHeadquarter", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson deleteHeadquarter(
			@RequestBody HeadquarterJson head) {

		GenericResponseJson resp = new GenericResponseJson();

		if (head == null || head.getHeadquarterId() == null) {
			resp.setMessage("headquarterId  may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;

		}

		return getBiographicalPeopleService().deleteOrganization(head);

	}

	// http://localhost:8181/Mia/json/biographical/findBiographicalPeople/116
	@RequestMapping(value = "setPeopleDeathDate", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<Integer> setPeopleDeathDate() {
		GenericResponseDataJson<Integer> resp = new GenericResponseDataJson<Integer>();
		Integer ppl = getBiographicalPeopleService().setPeopleDeathDate();

		resp.setStatus(StatusType.ok.toString());
		resp.setMessage(" Biographical people death date modified " + ppl);
		resp.setData(ppl);
		return resp;

	}

	public BiographicalPeopleService getBiographicalPeopleService() {
		return biographicalPeopleService;
	}

	public void setBiographicalPeopleService(
			BiographicalPeopleService biographicalPeopleService) {
		this.biographicalPeopleService = biographicalPeopleService;
	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

	public void setMiaDocumentService(MiaDocumentService miaDocumentService) {
		this.miaDocumentService = miaDocumentService;
	}

	public HistoryLogService getHistoryLogService() {
		return historyLogService;
	}
}