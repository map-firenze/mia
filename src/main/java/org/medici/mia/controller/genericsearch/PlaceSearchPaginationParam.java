package org.medici.mia.controller.genericsearch;

public class PlaceSearchPaginationParam extends SearchPaginationParam {

	private static final long serialVersionUID = 1L;

	public PlaceSearchPaginationParam(Integer firstResult, Integer maxResult,
			String ascOrdDesc, String orderColumn) {
		super(firstResult, maxResult, ascOrdDesc);
		this.orderColumn = orderColumn;
	}

	private String orderColumn;

	public String getOrderColumn() {
		if (orderColumn == null || orderColumn.isEmpty()) {
			this.orderColumn = "PLACEALLID";
		}
		return orderColumn;
	}

	public void setOrderColumn(String orderColumn) {
		this.orderColumn = orderColumn;
	}

}