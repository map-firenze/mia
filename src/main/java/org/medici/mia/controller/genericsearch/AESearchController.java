package org.medici.mia.controller.genericsearch;

import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.complexsearch.AEComplexSearchJson;
import org.medici.mia.common.json.complexsearch.AEComplexSearchResponseJson;
import org.medici.mia.common.json.search.AEArchivalSearchJson;
import org.medici.mia.common.json.search.AEWordSearchJson;
import org.medici.mia.common.json.search.AEWordSearchType;
import org.medici.mia.controller.archive.MyArchiveBean;
import org.medici.mia.service.genericsearch.AESearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mysql.jdbc.StringUtils;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Controller
@RequestMapping("/search/ae")
public class AESearchController {

	@Autowired
	private AESearchService searchService;

	// localhost:8181/Mia/json/search/ae/wordSearch/{}
	@RequestMapping(value = "aeWordSearchWithOnlyMatch", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> aeWordSearchWithOnlyMatch(
			@RequestBody AEWordSearchJson searchReq) {

		GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> resp = new GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>>();

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return resp;
		}

		if (isSearchable(searchReq, resp)) {
			resp = searchService.findAEntitiesBySearchWords(searchReq);

		}

		return resp;

	}

	@RequestMapping(value = "wordSearch/{firstResult}/{maxResult}/{orderColumn}/{ascOrDesc}", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<AEComplexSearchResponseJson> aeWordSearch(
			@RequestBody AEComplexSearchJson searchReq,
			@PathVariable("firstResult") Integer firstResult,
			@PathVariable("maxResult") Integer maxResult,
			@PathVariable("orderColumn") String orderColumn,
			@PathVariable("ascOrDesc") String ascOrDesc) {

		GenericResponseDataJson<AEComplexSearchResponseJson> resp = 
				new GenericResponseDataJson<AEComplexSearchResponseJson>();

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return resp;
		}
		if (isSearchByOwner(searchReq)) {
			
			AESearchPaginationParam pagParam = new AESearchPaginationParam(
					firstResult, maxResult, ascOrDesc, orderColumn);
			
			resp = searchService.findByOwner(searchReq, pagParam);
		}
		else if (isSearchable(searchReq, resp)) {
			
			AESearchPaginationParam pagParam = new AESearchPaginationParam(
					firstResult, maxResult, ascOrDesc, orderColumn);
			
			resp = searchService.findAEntitiesByComplexSearch(searchReq, 
					pagParam);
		}

		return resp;
	}

	// localhost:8181/Mia/json/search/ae/archivalSearch/{}
	@RequestMapping(value = "archivalSearch", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> aeArchivalSearch(
			@RequestBody AEArchivalSearchJson searchReq) {

		GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> resp = new GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>>();

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return resp;
		}

		if (isSearchable(searchReq, resp)) {
			resp = searchService.findAEntitiesByArchivalInfo(searchReq);

		}

		return resp;

	}

	// json/search/ae/archivalSearchPartialCount
	@RequestMapping(value = "archivalSearchPartialCount", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<HashMap<String, Integer>> archivalSearchPartialCount(
			@RequestBody AEArchivalSearchJson searchReq) {

		GenericResponseDataJson<HashMap<String, Integer>> resp = new GenericResponseDataJson<HashMap<String, Integer>>();

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return resp;
		}

		if (isSearchable(searchReq, resp)) {
			resp = searchService.countAEntitiesByArchivalInfo(searchReq);
		}

		return resp;
	}

	private boolean isSearchable(AEArchivalSearchJson searchReq,
			GenericResponseDataJson<?> resp) {
		if (searchReq.getOwner() == null
				|| searchReq.getOwner().trim().equals("")) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Owner is empty or null");
			return false;
		}
		if (!((searchReq.isAllArchivalEntities()
				^ searchReq.isEveryoneElsesArchivalEntities() ^ searchReq
					.isOnlyMyArchivalEnties()) && (!searchReq
				.isAllArchivalEntities()
				|| !searchReq.isEveryoneElsesArchivalEntities() || !searchReq
					.isOnlyMyArchivalEnties()))) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("One and only one in onlyMyArchivalEnties, everyoneElsesArchivalEntities and allArchivalEntities has to be true");
			return false;
		}
		if ((searchReq.getInsertId() == null || searchReq.getInsertId()
				.isEmpty())
				&& (searchReq.getCollectionId() == null || searchReq
						.getCollectionId().isEmpty())
				&& (searchReq.getRepositoryId() == null || searchReq
						.getRepositoryId().isEmpty())
				&& (searchReq.getSeriesId() == null || searchReq.getSeriesId()
						.isEmpty())
				&& (searchReq.getVolumeId() == null || searchReq.getVolumeId()
						.isEmpty())) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Set atleast one of insertId,collectionId,repositoryId,seriesId or volumeId");
			return false;
		}
		return true;
	}

	private boolean isSearchable(AEWordSearchJson searchReq,
			GenericResponseDataJson<HashMap<String, List<MyArchiveBean>>> resp) {

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return false;
		}
		if (!((searchReq.isAllArchivalEntities()
				^ searchReq.isEveryoneElsesArchivalEntities() ^ searchReq
					.isOnlyMyArchivalEnties()) && (!searchReq
				.isAllArchivalEntities()
				|| !searchReq.isEveryoneElsesArchivalEntities() || !searchReq
					.isOnlyMyArchivalEnties()))) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("One and only one in onlyMyArchivalEnties, everyoneElsesArchivalEntities and allArchivalEntities has to be true");
			return false;
		}
		if (searchReq.getSearchString() == null
				|| searchReq.getSearchString().trim().isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("SearchString is empty");
			return false;
		}
		if (searchReq.getOwner() == null
				|| searchReq.getOwner().trim().equals("")) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Owner is empty or null");
			return false;
		}

		if (searchReq.getSearchType() == null
				|| searchReq.getSearchType().isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("SearchType is empty");
			return false;
		}
		try {
			AEWordSearchType searchType = AEWordSearchType.valueOf(searchReq
					.getSearchType());
		} catch (IllegalArgumentException ex) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("SearchType " + searchReq.getSearchType()
					+ " not permitted");
			return false;
		}

		return true;

	}

	private boolean isSearchable(AEComplexSearchJson searchReq,
			GenericResponseDataJson<AEComplexSearchResponseJson> resp) {

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return false;
		}
		if (!((searchReq.isAllArchivalEntities()
				^ searchReq.isEveryoneElsesArchivalEntities() ^ searchReq
					.isOnlyMyArchivalEnties()) && (!searchReq
				.isAllArchivalEntities()
				|| !searchReq.isEveryoneElsesArchivalEntities() || !searchReq
					.isOnlyMyArchivalEnties()))) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("One and only one in onlyMyArchivalEnties, everyoneElsesArchivalEntities and allArchivalEntities has to be true");
			return false;
		}
		if ((searchReq.getPartialSearchWords() == null || searchReq
				.getPartialSearchWords().trim().isEmpty())
				&& (searchReq.getMatchSearchWords() == null || searchReq
						.getMatchSearchWords().trim().isEmpty())) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("SearchString is empty");
			return false;
		}
		if (searchReq.getOwner() == null
				|| searchReq.getOwner().trim().equals("")) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Owner is empty or null");
			return false;
		}

		if (searchReq.getSearchType() == null
				|| searchReq.getSearchType().isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("SearchType is empty");
			return false;
		}
		try {
			AEWordSearchType searchType = AEWordSearchType.valueOf(searchReq
					.getSearchType());
		} catch (IllegalArgumentException ex) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("SearchType " + searchReq.getSearchType()
					+ " not permitted");
			return false;
		}

		return true;

	}
	
	private boolean isSearchByOwner(AEComplexSearchJson searchRequest) {
		return !StringUtils.isNullOrEmpty(searchRequest.getOwner()) &&
				searchRequest.getSearchType().equals(
						AEWordSearchType.AE_OWNER.toString()) &&
			   StringUtils.isNullOrEmpty(searchRequest.getMatchSearchWords()) &&
			   StringUtils.isNullOrEmpty(searchRequest.getPartialSearchWords());
	}
}
