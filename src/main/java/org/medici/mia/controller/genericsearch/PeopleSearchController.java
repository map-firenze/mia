package org.medici.mia.controller.genericsearch;

import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.PeopleJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.complexsearch.PeopleComplexSearchJson;
import org.medici.mia.common.json.search.PeopleWordSearchJson;
import org.medici.mia.common.json.search.PeopleWordSearchType;
import org.medici.mia.common.util.StringUtils;
import org.medici.mia.service.genericsearch.PeopleSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Controller
@RequestMapping("/search/people")
public class PeopleSearchController {

	@Autowired
	private PeopleSearchService searchService;

	// localhost:8181/Mia/json/search/people/wordSearchOnlyLike
	@RequestMapping(value = "wordSearchOnlyLike", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<PeopleJson>>> wordSearchOnlyLike(
			@RequestBody PeopleWordSearchJson searchReq) {

		GenericResponseDataJson<HashMap<String, List<PeopleJson>>> resp = new GenericResponseDataJson<HashMap<String, List<PeopleJson>>>();

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return resp;
		}

		if (isSearchable(searchReq, resp)) {
			resp = searchService.findPeopleBySearchWords(searchReq);

		}

		return resp;

	}
	
	// localhost:8181/Mia/json/search/people/wordSearch/{}
	@RequestMapping(
			value = 
			"wordSearch/{firstResult}/{maxResult}/{orderColumn}/{ascOrDesc}", 
			method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<HashMap
		<String, List<PeopleJson>>> wordSearch(
			@RequestBody PeopleComplexSearchJson searchReq,
			@PathVariable("firstResult") Integer firstResult,
			@PathVariable("maxResult") Integer maxResult,
			@PathVariable("orderColumn") String orderColumn,
			@PathVariable("ascOrDesc") String ascOrDesc) {

		GenericResponseDataJson<HashMap<String, List<PeopleJson>>> resp = 
				new GenericResponseDataJson<HashMap<String, List<PeopleJson>>>();

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return resp;
		}
		
		if (isSearchByOwner(searchReq)) {
			PeopleSearchPaginationParam pagParam = 
					new PeopleSearchPaginationParam(
					firstResult, maxResult, ascOrDesc, orderColumn);
			resp = searchService.findPeopleByOwner(searchReq, pagParam);
		}

		else if (isSearchable(searchReq, resp)) {
			PeopleSearchPaginationParam pagParam = 
					new PeopleSearchPaginationParam(
					firstResult, maxResult, ascOrDesc, orderColumn);
			resp = searchService.findPeopleByComplexSearchWords(searchReq, 
					pagParam);
		}

		return resp;
	}

	private boolean isSearchable(PeopleWordSearchJson searchReq,
			GenericResponseDataJson<HashMap<String, List<PeopleJson>>> resp) {

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return false;
		}
		if (searchReq.getSearchType() == null
				|| searchReq.getSearchType().isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("SearchType is empty");
			return false;
		}
		int idx = 0;
		try {
			for (String type : searchReq.getSearchType()) {
				PeopleWordSearchType searchType = PeopleWordSearchType
						.valueOf(type);
				idx++;
			}

		} catch (IllegalArgumentException ex) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("SearchType " + searchReq.getSearchType().get(idx)
					+ " not permitted");
			return false;
		}

		if (searchReq.getSearchString() == null
				|| searchReq.getSearchString().trim().isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Search String is empty");
			return false;
		}

		return true;

	}
	
	private boolean isSearchable(PeopleComplexSearchJson searchReq,
			GenericResponseDataJson<HashMap<String, List<PeopleJson>>> resp) {

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return false;
		}
		if (searchReq.getSearchType() == null
				|| searchReq.getSearchType().isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("SearchType is empty");
			return false;
		}
		int idx = 0;
		try {
			for (String type : searchReq.getSearchType()) {
				PeopleWordSearchType searchType = PeopleWordSearchType
						.valueOf(type);
				idx++;
			}

		} catch (IllegalArgumentException ex) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("SearchType " + searchReq.getSearchType().get(idx)
					+ " not permitted");
			return false;
		}

		if ((searchReq.getMatchSearchWords() == null
				|| searchReq.getMatchSearchWords().trim().isEmpty()) && (searchReq.getPartialSearchWords() == null
						|| searchReq.getPartialSearchWords().trim().isEmpty())) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Search String is empty");
			return false;
		}
		
		

		return true;

	}
	
	private boolean isSearchByOwner(PeopleComplexSearchJson searchRequest) {
		return !StringUtils.isNullableString(searchRequest.getOwner()) && 
			StringUtils.isNullableString(searchRequest.getMatchSearchWords()) &&
			StringUtils.isNullableString(searchRequest.getPartialSearchWords());	
	}
}
