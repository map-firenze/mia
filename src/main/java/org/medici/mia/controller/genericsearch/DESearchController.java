package org.medici.mia.controller.genericsearch;

import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.complexsearch.DEComplexSearchJson;
import org.medici.mia.common.json.complexsearch.DEComplexSearchRespJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.search.DEWordSearchJson;
import org.medici.mia.common.json.search.DEWordSearchType;
import org.medici.mia.service.genericsearch.GenericSearchService;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Controller
@RequestMapping("/search/de")
public class DESearchController {

	@Autowired
	private GenericSearchService deSearchService;

	@Autowired
	private MiaDocumentService miaDocumentService;

	// localhost:8181/Mia/json/search/de/wordSearchWithOnlyMatch/{}
	@RequestMapping(value = "wordSearchWithOnlyMatch", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<DocumentJson>>> wordSearchWithOnlyMatch(
			@RequestBody DEWordSearchJson searchReq) {

		GenericResponseDataJson<HashMap<String, List<DocumentJson>>> resp = new GenericResponseDataJson<HashMap<String, List<DocumentJson>>>();

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return resp;
		}

		if (isSearchable(searchReq, resp)) {
			resp = getDeSearchService().findDEntitiesBySearchWords(searchReq);

		}

		return resp;

	}

	// http://mia.medici.org:8080/Mia/json/search/de/wordSearch/{}
	@RequestMapping(value = "wordSearch/{firstResult}/{maxResult}/{orderColumn}/{ascOrDesc}", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<DEComplexSearchRespJson> deWordSearch(
			@RequestBody DEComplexSearchJson searchReq,
			@PathVariable("firstResult") Integer firstResult,
			@PathVariable("maxResult") Integer maxResult,
			@PathVariable("orderColumn") String orderColumn,
			@PathVariable("ascOrDesc") String ascOrDesc) {

		GenericResponseDataJson<DEComplexSearchRespJson> resp = new GenericResponseDataJson<DEComplexSearchRespJson>();

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return resp;
		}

		if (isSearchable(searchReq, resp)) {
			DESearchPaginationParam pagParam = new DESearchPaginationParam(
					firstResult, maxResult, ascOrDesc, orderColumn);
			return getDeSearchService().findDEntitiesByComplexSearch(searchReq,
					pagParam);
		}

		return resp;

	}

	private boolean isSearchable(DEWordSearchJson searchReq,
			GenericResponseDataJson<HashMap<String, List<DocumentJson>>> resp) {

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return false;
		}

		if (!((searchReq.isAllEntities() ^ searchReq.isEveryoneElsesEntities() ^ searchReq
				.isOnlyMyEntities()) && (!searchReq.isAllEntities()
				|| !searchReq.isEveryoneElsesEntities() || !searchReq
					.isOnlyMyEntities()))) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("One and only one in onlyMyEnties, everyoneElsesEntities and allEntities has to be true");
			return false;
		}
		if (searchReq.getSearchString() == null
				|| searchReq.getSearchString().trim().isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("SearchString is empty");
			return false;
		}
		if (searchReq.getOwner() == null
				|| searchReq.getOwner().trim().equals("")) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Owner is empty or null");
			return false;
		}

		if (searchReq.getSearchType() == null
				|| searchReq.getSearchType().isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("SearchType is empty");
			return false;
		}

		Integer type = Integer.valueOf(searchReq.getSearchType());
		if (type < 0 || type > 3) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("SearchType " + searchReq.getSearchType()
					+ " not permitted");
			return false;
		}

		return true;

	}

	@RequestMapping(value = "updateAllDocTransSearch", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<Integer> updateAllDocTransSearch() {

		GenericResponseDataJson<Integer> resp = new GenericResponseDataJson<Integer>();

		resp.setData(getMiaDocumentService().updateAllTransSearch());
		resp.setStatus(StatusType.ok.toString());

		return resp;

	}

	private boolean isSearchable(DEComplexSearchJson searchReq,
			GenericResponseDataJson<DEComplexSearchRespJson> resp) {

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return false;
		}

		if (!((searchReq.isAllEntities() ^ searchReq.isEveryoneElsesEntities() ^ searchReq
				.isOnlyMyEntities()) && (!searchReq.isAllEntities()
				|| !searchReq.isEveryoneElsesEntities() || !searchReq
					.isOnlyMyEntities()))) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("One and only one in onlyMyEnties, everyoneElsesEntities and allEntities has to be true");
			return false;
		}
		if ((searchReq.getPartialSearchWords() == null || searchReq
				.getPartialSearchWords().trim().isEmpty())
				&& (searchReq.getMatchSearchWords() == null || searchReq
						.getMatchSearchWords().trim().isEmpty())) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Both partialSearchWords and matchSearchWords are empties");
			return false;
		}
		if (searchReq.getOwner() == null
				|| searchReq.getOwner().trim().equals("")) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Owner is empty or null");
			return false;
		}

		if (searchReq.getSearchType() == null
				|| searchReq.getSearchType().isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("SearchType is empty");
			return false;
		}

		if (!((DEWordSearchType.DE_TRANSCRIPTION_AND_SYNOPSIS.toString()
				.equalsIgnoreCase(searchReq.getSearchType()))
				|| (DEWordSearchType.DE_TRANSCRIPTION.toString()
						.equalsIgnoreCase(searchReq.getSearchType())) || (DEWordSearchType.DE_SYNOPSIS
					.toString().equalsIgnoreCase(searchReq.getSearchType())))) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("SearchType " + searchReq.getSearchType()
					+ " not permitted");
			return false;

		}

		return true;

	}

	public GenericSearchService getDeSearchService() {
		return deSearchService;
	}

	public void setDeSearchService(GenericSearchService deSearchService) {
		this.deSearchService = deSearchService;
	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

	public void setMiaDocumentService(MiaDocumentService miaDocumentService) {
		this.miaDocumentService = miaDocumentService;
	}

}
