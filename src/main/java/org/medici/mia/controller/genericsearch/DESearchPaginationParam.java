package org.medici.mia.controller.genericsearch;

public class DESearchPaginationParam extends SearchPaginationParam {

	private static final long serialVersionUID = 1L;

	private String orderColumn;

	public DESearchPaginationParam(Integer firstResult, Integer maxResult,
			String ascOrdDesc, String orderColumn) {
		super(firstResult, maxResult, ascOrdDesc);
		this.orderColumn = orderColumn;
	}

	public String getOrderColumn() {
		if (orderColumn == null || orderColumn.isEmpty()) {
			this.orderColumn = "documentEntityId";
		}
		return orderColumn;
	}

	public void setOrderColumn(String orderColumn) {
		this.orderColumn = orderColumn;
	}

	public boolean isOrderByProducer() {
		if (this.orderColumn != null
				&& this.orderColumn.equalsIgnoreCase("producers"))
			return true;
		return false;

	}

	public boolean isOrderByArchivalLocation() {
		if (this.orderColumn != null && this.orderColumn.equalsIgnoreCase("archivalLocation")) {
			return true;
		}
		return false;
	}

}