package org.medici.mia.controller.genericsearch;

import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.complexsearch.PlaceComplexSearchJson;
import org.medici.mia.common.json.search.PlaceSearchJson;
import org.medici.mia.common.json.search.PlaceTypeJson;
import org.medici.mia.common.json.search.PlaceWordSearchJson;
import org.medici.mia.service.genericsearch.PlaceSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mysql.jdbc.StringUtils;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Controller
@RequestMapping("/search/places")
public class PlaceSearchController {

	@Autowired
	private PlaceSearchService searchService;

	// localhost:8181/Mia/json/search/people/wordSearch/{}
	@RequestMapping(value = "wordSearchOnlyLike", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>> wordSearchOnlyLike(
			@RequestBody PlaceWordSearchJson searchReq) {

		GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>> resp = new GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>>();

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return resp;
		}

		if (isSearchable(searchReq, resp)) {
			resp = searchService.findPlaceByNameAndType(searchReq);

		}

		return resp;

	}

	// localhost:8181/Mia/json/search/places/wordSearch/{}
	@RequestMapping(value = "wordSearch/{firstResult}/{maxResult}/{orderColumn}/{ascOrDesc}", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>> wordSearch(
			@RequestBody PlaceComplexSearchJson searchReq,
			@PathVariable("firstResult") Integer firstResult,
			@PathVariable("maxResult") Integer maxResult,
			@PathVariable("orderColumn") String orderColumn,
			@PathVariable("ascOrDesc") String ascOrDesc) {

		GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>> resp = new GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>>();

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return resp;
		}
		if (isSearchByOwner(searchReq)) {
			AESearchPaginationParam pagParam = new AESearchPaginationParam(
					firstResult, maxResult, ascOrDesc, orderColumn);
			resp = searchService.findByOwner(searchReq, pagParam);
		}
		else if (isSearchable(searchReq, resp)) {
			AESearchPaginationParam pagParam = new AESearchPaginationParam(
					firstResult, maxResult, ascOrDesc, orderColumn);
			resp = searchService.findPlaceByComplexSearchNameAndType(searchReq, pagParam);
		}

		return resp;

	}

	// localhost:8181/Mia/json/search/places/getPlaceTypes/
	@RequestMapping(value = "getPlaceTypes", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<PlaceTypeJson>>> getPlaceTypes() {

		return searchService.findPlaceTypes();

	}

	private boolean isSearchable(PlaceWordSearchJson searchReq,
			GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>> resp) {

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return false;
		}

		if (searchReq.getSearchString() == null
				|| searchReq.getSearchString().trim().isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Search String is empty");
			return false;
		}

		return true;

	}

	private boolean isSearchable(PlaceComplexSearchJson searchReq,
			GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>> resp) {

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return false;
		}

		if ((searchReq.getMatchSearchWords() == null || searchReq
				.getMatchSearchWords().trim().isEmpty())
				&& (searchReq.getPartialSearchWords() == null || searchReq
						.getPartialSearchWords().trim().isEmpty())) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Both match and partial search word are empty.");
			return false;
		}

		return true;

	}
	
	private boolean isSearchByOwner(PlaceComplexSearchJson searchRequest) {
		return !StringUtils.isNullOrEmpty(searchRequest.getOwner()) &&
			StringUtils.isNullOrEmpty(searchRequest.getMatchSearchWords()) &&
			StringUtils.isNullOrEmpty(searchRequest.getPartialSearchWords());
	}
}
