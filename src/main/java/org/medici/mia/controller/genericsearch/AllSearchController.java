package org.medici.mia.controller.genericsearch;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.search.AllCountWordSearchJson;
import org.medici.mia.common.json.search.AllCountWordSearchResponseJson;
import org.medici.mia.common.json.search.AllCountWordSearchType;
import org.medici.mia.service.genericsearch.GenericSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Controller
@RequestMapping("/search/all")
public class AllSearchController {

	@Autowired
	private GenericSearchService genericSearchService;

	// localhost:8181/Mia/json/search/all/countWordSearch/
	@RequestMapping(value = "countWordSearch", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<AllCountWordSearchResponseJson> countWordSearch(
			@RequestBody AllCountWordSearchJson searchReq) {

		GenericResponseDataJson<AllCountWordSearchResponseJson> resp = new GenericResponseDataJson<AllCountWordSearchResponseJson>();

		if (!isSearchable(searchReq, resp)) {
			return resp;
		}

		resp.setData(getGenericSearchService().findAllCountBySearchWords(
				searchReq));
		resp.setStatus(StatusType.ok.toString());

		return resp;

	}

	// localhost:8181/Mia/json/search/all/countWordSearchAll/
	@RequestMapping(value = "countWordSearchAll", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<AllCountWordSearchResponseJson> countWordSearchAll() {

		GenericResponseDataJson<AllCountWordSearchResponseJson> resp = new GenericResponseDataJson<AllCountWordSearchResponseJson>();

		resp.setData(getGenericSearchService().findAllCount());
		resp.setStatus(StatusType.ok.toString());

		return resp;

	}

	private boolean isSearchable(AllCountWordSearchJson searchReq,
			GenericResponseDataJson<AllCountWordSearchResponseJson> resp) {

		if (searchReq == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Request is null");
			return false;
		}

		if ((searchReq.getPartialSearchWords() == null || searchReq
				.getPartialSearchWords().trim().isEmpty())
				&& (searchReq.getMatchSearchWords() == null || searchReq
						.getMatchSearchWords().trim().isEmpty())) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Both fields matchSearchWords and partialSearchWords are empties");
			return false;
		}

		if (searchReq.getSearchType() == null
				|| searchReq.getSearchType().isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("SearchType is empty");
			return false;
		}

		if (!(AllCountWordSearchType.ALL.toString().equalsIgnoreCase(
				searchReq.getSearchType())
				|| AllCountWordSearchType.PEOPLE.toString().equalsIgnoreCase(
						searchReq.getSearchType())
				|| AllCountWordSearchType.ARCHIVAL_ENTITIES.toString()
						.equalsIgnoreCase(searchReq.getSearchType())
				|| AllCountWordSearchType.PLACES.toString().equalsIgnoreCase(
						searchReq.getSearchType())
				|| AllCountWordSearchType.DE_TRANSCRIPTION_AND_SYNOPSIS
						.toString().equalsIgnoreCase(searchReq.getSearchType())
				|| AllCountWordSearchType.TRANSCRIPTIONS.toString()
						.equalsIgnoreCase(searchReq.getSearchType()) || AllCountWordSearchType.SYNOPSES
				.toString().equalsIgnoreCase(searchReq.getSearchType()))) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("SearchType " + searchReq.getSearchType()
					+ " not permitted");
			return false;
		}

		return true;

	}

	public GenericSearchService getGenericSearchService() {
		return genericSearchService;
	}

	public void setGenericSearchService(
			GenericSearchService genericSearchService) {
		this.genericSearchService = genericSearchService;
	}

}
