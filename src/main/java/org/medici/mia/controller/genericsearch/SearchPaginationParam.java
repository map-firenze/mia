package org.medici.mia.controller.genericsearch;

import java.io.Serializable;

public class SearchPaginationParam implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer firstResult;
	private Integer maxResult;	
	private String ascOrdDesc;
	
	
	public SearchPaginationParam(Integer firstResult, Integer maxResult,
			String ascOrdDesc) {
		super();
		this.firstResult = firstResult;
		this.maxResult = maxResult;
		this.ascOrdDesc = ascOrdDesc;
	}
	public Integer getFirstResult() {
		return firstResult;
	}
	public void setFirstResult(Integer firstResult) {
		if(firstResult == null || firstResult.equals("")){
			this.firstResult = 0;
		}
		this.firstResult = firstResult;
	}
	public Integer getMaxResult() {
		if(maxResult == null || maxResult.equals("")){
			this.maxResult = 20;
		}
		return maxResult;
	}
	public void setMaxResult(Integer maxResult) {
		this.maxResult = maxResult;
	}
	
	public String getAscOrdDesc() {
		if(ascOrdDesc == null || ascOrdDesc.isEmpty()){
			this.ascOrdDesc = "asc";
		}
		return ascOrdDesc;
	}
	public void setAscOrdDesc(String ascOrdDesc) {		
		this.ascOrdDesc = ascOrdDesc;
	}

	

}