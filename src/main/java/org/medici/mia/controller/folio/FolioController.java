package org.medici.mia.controller.folio;

import java.util.List;

import org.apache.log4j.Logger;
import org.medici.mia.common.json.folio.FolioJson;
import org.medici.mia.service.folio.FolioService;
import org.medici.mia.service.folio.UploadCollectionJson;
import org.medici.mia.service.folio.UploadCollectionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Controller
@RequestMapping("/folios")
public class FolioController {

	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private FolioService folioService;

	

	// http://localhost:8181/Mia/json/browseAllCollections/findVolumeWithNumberedImages/3094
	@RequestMapping(value = "findVolumeWithNumberedImages/{volumeId}", method = RequestMethod.GET)
	public @ResponseBody UploadCollectionJson findVolumeWithNumberedImages(
			@PathVariable("volumeId") Integer volumeId) {

		return getFolioService().getUploads(volumeId,
				UploadCollectionType.VOLUME);

	}

	// http://localhost:8181/Mia/json/browseAllCollections/findVolumeWithNumberedImages/1316
	@RequestMapping(value = "findInsertWithNumberedImages/{insertId}", method = RequestMethod.GET)
	public @ResponseBody UploadCollectionJson findInsertWithNumberedImages(
			@PathVariable("insertId") Integer insertId) {

		return getFolioService().getUploads(insertId,
				UploadCollectionType.INSERT);

	}
	
	// http://localhost:8181/Mia/json/folios/findFoliosByDocument/1316
	@RequestMapping(
			value = "findFoliosByDocument/{documentEntityId}", 
			method = RequestMethod.GET)
	public @ResponseBody List<FolioJson> findFoliosByDocument(
			@PathVariable("documentEntityId") Integer documentEntityId) {

		return getFolioService().findFoliosByDocument(documentEntityId);
	}
	
	// http://localhost:8181/Mia/json/folios/addFolioToNoImageDocument/1316
	@RequestMapping(
			value = "addFolioToNoImageDocument/{documentEntityId}", 
			method = RequestMethod.POST)
	public @ResponseBody FolioJson addFolioToNoImageDocument(
			@PathVariable("documentEntityId") Integer documentEntityId,
			@RequestBody FolioJson folioJson) {
		
		logger.info(String.format("Add folio to document: %s", documentEntityId));
		
		return getFolioService().addFolioToNoImageDocument(
				folioJson, 
				documentEntityId);
	}
	
	@RequestMapping(
			value = "removeFolioFromNoImageDocument/{documentEntityId}/{folioId}",
			method = RequestMethod.DELETE)
	public @ResponseBody Integer removeFolioFromNoImageDocument(
			@PathVariable("documentEntityId") Integer documentEntityId,
			@PathVariable("folioId") Integer folioId) {
		
		logger.info(String.format("Remove folio %s from document %s", 
				folioId, 
				documentEntityId));
		
		return getFolioService().removeFolioFromNoImageDocument(
				folioId, 
				documentEntityId);
	}

	public FolioService getFolioService() {
		return folioService;
	}

	public void setFolioService(FolioService folioService) {
		this.folioService = folioService;
	}

}