package org.medici.mia.controller.admin;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.UserGroupJson;
import org.medici.mia.common.json.UserJson;
import org.medici.mia.common.json.administration.AdminUserJson;
import org.medici.mia.common.json.administration.CreateAdministrationUserJson;
import org.medici.mia.common.json.administration.FindAdministrationUserJson;
import org.medici.mia.common.json.administration.ModifyAdministrationUserJson;
import org.medici.mia.common.util.DateUtils;
import org.medici.mia.domain.User;
import org.medici.mia.domain.UserRole;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.admin.AdminService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Controller
@RequestMapping("/user")
public class AdministrationController {

	@Autowired
	private AdminService adminService;

	@Autowired
	private UserService userService;

	// http://localhost:8181/Mia/json/user/findMiaUser/sbigdeltest
	@RequestMapping(value = "findMiaUser/{account}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<FindAdministrationUserJson> findMiaUser(
			@PathVariable("account") String account) {

		GenericResponseDataJson<FindAdministrationUserJson> resp = new GenericResponseDataJson<FindAdministrationUserJson>();
		User user = null;

		try {
			user = getUserService().findUser(account);
		} catch (ApplicationThrowable applicationThrowable) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Error retrieving the User logged in");
			return resp;
		}

		if (user == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Account is null");
			return resp;
		}

		FindAdministrationUserJson userJson = new FindAdministrationUserJson();
		userJson.toJson(user);

		resp.setStatus(StatusType.ok.toString());
		resp.setMessage("Users find in DB with this account");
		resp.setData(userJson);
		return resp;

	}

	// create new user
	// http://localhost:8181/Mia/json/user/createMiaUser/
	@RequestMapping(value = "createMiaUser", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<String> createMiaUser(
			@RequestBody final CreateAdministrationUserJson userJson)
			throws ParseException {

		GenericResponseDataJson<String> resp = new GenericResponseDataJson<String>();
		if (userJson == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Tried to create an empty user");
			return resp;
		}

		User newUser = new User();
		userJson.toEntity(newUser);

		getUserService().registerAdministrationUser(newUser);
		if (getUserService().findUser(newUser.getAccount()) != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("User persisted in DB");
			resp.setData(newUser.getAccount());
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("User not persisted in DB");
			return resp;
		}

	}

	// modify existing user
	// http://localhost:8181/Mia/json/user/modifyMiaUser/
	@RequestMapping(value = "modifyMiaUser", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyMiaUser(
			@RequestBody final ModifyAdministrationUserJson userJson)
			throws ParseException {

		GenericResponseJson resp = new GenericResponseJson();
		User userToBeModified = null;
		if (userJson == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Tried to modify user with an empty user request");
			return resp;
		}

		userToBeModified = getUserService().findUser(userJson.getAccount());

		userJson.toEntity(userToBeModified);
		User modifiedUser = getUserService().updateAdministrationUser(
				userToBeModified);
		if (modifiedUser != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("User modified in DB");
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("User not modified in DB");
			return resp;
		}

	}

	// search user by account
	// http://localhost:8181/Mia/json/user/searchUserByAccount/{account}
	@RequestMapping(value = "searchUserByAccount/{account}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<UserJson>> searchUserByAccount(
			@PathVariable("account") String account) {

		GenericResponseDataJson<List<UserJson>> resp = new GenericResponseDataJson<List<UserJson>>();
		List<UserJson> userJsonList = new ArrayList<UserJson>();

		if (account != null && !account.equals("")) {
			User user = new User();
			user.setAccount(account);
			List<User> userList = getUserService().findUsers(user);
			if (userList != null && userList.size() != 0) {

				for (User userL : userList) {
					UserJson userJ = new UserJson();
					userJ.setAccount(userL.getAccount());
					userJ.setFirstName(userL.getFirstName());
					userJ.setLastName(userL.getLastName());
					userJ.setOrganization(userL.getOrganization());
					userJ.setEmail(userL.getMail());
					userJ.setCity(userL.getCity());
					userJ.setCountry(userL.getCountry());

					if (userL.getUserRoles() != null
							&& userL.getUserRoles().size() > 0) {

						List<UserGroupJson> userGroups = new ArrayList<UserGroupJson>();

						for (UserRole userRole : userL.getUserRoles()) {
							UserGroupJson userG = new UserGroupJson();
							userG.setGroup(userRole.getUserAuthority()
									.getAuthority().toString());
							userG.setPriority(userRole.getUserAuthority()
									.getPriority());
							userGroups.add(userG);
						}

						Collections.sort(userGroups,
								UserGroupJson.USER_GROUP_PRIORITY);
						for (UserGroupJson userGroup : userGroups) {
							userJ.getUserGroups().add(userGroup.getGroup());
						}
					}

					userJ.setExpirationDate(DateUtils.getStringDate(userL
							.getExpirationDate()));
					userJsonList.add(userJ);
				}

				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("Users find in DB with this account");
				resp.setData(userJsonList);
				return resp;
			} else {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("No user found in DB with this account");
				return resp;
			}
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Account is null");
			return resp;
		}

	}

	@RequestMapping(value = "me", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<UserJson>> searchUserByAccount() {

		GenericResponseDataJson<List<UserJson>> resp = new GenericResponseDataJson<List<UserJson>>();
		List<UserJson> userJsonList = new ArrayList<UserJson>();

		// Recovery User Name

		User userFromSession = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()).getUsername());
		String account = userFromSession.getAccount();

		if (account != null && !account.equals("")) {
			User user = new User();
			user.setAccount(account);
			List<User> userList = getUserService().findUsers(user);
			if (userList != null && userList.size() != 0) {

				for (User userL : userList) {
					UserJson userJ = new UserJson();
					userJ.setAccount(userL.getAccount());
					userJ.setFirstName(userL.getFirstName());
					userJ.setLastName(userL.getLastName());
					userJ.setOrganization(userL.getOrganization());
					userJ.setEmail(userL.getMail());
					userJ.setCity(userL.getCity());
					userJ.setCountry(userL.getCountry());
					if (userL.getUserRoles() != null
							&& userL.getUserRoles().size() > 0) {

						List<UserGroupJson> userGroups = new ArrayList<UserGroupJson>();

						for (UserRole userRole : userL.getUserRoles()) {
							UserGroupJson userG = new UserGroupJson();
							userG.setGroup(userRole.getUserAuthority()
									.getAuthority().toString());
							userG.setPriority(userRole.getUserAuthority()
									.getPriority());
							userGroups.add(userG);
						}

						Collections.sort(userGroups,
								UserGroupJson.USER_GROUP_PRIORITY);
						for (UserGroupJson userGroup : userGroups) {
							userJ.getUserGroups().add(userGroup.getGroup());
						}
					}
					userJ.setExpirationDate(DateUtils.getStringDate(userL
							.getExpirationDate()));
					boolean isImage = userService.isUserPortrait(
							userL.getPortraitImageName());
					if (isImage) {
						userJ.setPhoto("/Mia/user/ShowPortraitUser.do?account="
								+ userL.getAccount());
					} else {
						userJ.setPhoto("/Mia/images/1024/img_user.png");
					}
					userJsonList.add(userJ);
				}

				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("Users find in DB with this account");
				resp.setData(userJsonList);
				return resp;
			} else {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("No user found in DB with this account");
				return resp;
			}
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Account is null");
			return resp;
		}

	}

	// search user by name
	// http://localhost:8181/Mia/json/user/searchUserByName/{name}
	@RequestMapping(value = "searchUserByName/{name}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<UserJson>> searchUserByName(
			@PathVariable("name") String name) {

		GenericResponseDataJson<List<UserJson>> resp = new GenericResponseDataJson<List<UserJson>>();
		List<UserJson> userJsonList = new ArrayList<UserJson>();

		if (name != null && !name.equals("")) {
			User user = new User();
			user.setFirstName(name);
			user.setLastName(name);
			List<User> userList = getUserService().findUsersByName(user);
			if (userList != null && userList.size() != 0) {

				for (User userL : userList) {
					UserJson userJ = new UserJson();
					userJ.setAccount(userL.getAccount());
					userJ.setFirstName(userL.getFirstName());
					userJ.setLastName(userL.getLastName());
					userJ.setOrganization(userL.getOrganization());
					userJ.setEmail(userL.getMail());
					userJ.setCity(userL.getCity());
					userJ.setCountry(userL.getCountry());
					if (userL.getUserRoles() != null
							&& userL.getUserRoles().size() > 0) {

						List<UserGroupJson> userGroups = new ArrayList<UserGroupJson>();

						for (UserRole userRole : userL.getUserRoles()) {
							UserGroupJson userG = new UserGroupJson();
							userG.setGroup(userRole.getUserAuthority()
									.getAuthority().toString());
							userG.setPriority(userRole.getUserAuthority()
									.getPriority());
							userGroups.add(userG);
						}

						Collections.sort(userGroups,
								UserGroupJson.USER_GROUP_PRIORITY);
						for (UserGroupJson userGroup : userGroups) {
							userJ.getUserGroups().add(userGroup.getGroup());
						}
					}
					userJ.setExpirationDate(DateUtils.getStringDate(userL
							.getExpirationDate()));
					userJsonList.add(userJ);
				}

				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("Users find in DB with this account");
				resp.setData(userJsonList);
				return resp;
			} else {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("No user found in DB with this account");
				return resp;
			}
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Account is null");
			return resp;
		}
	}

	// list all the available user Groups
	// http://localhost:8181/Mia/json/user/getUsersAccount/al
	@RequestMapping(value = "findUserGroups/", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<String>> findUserGroups() {

		GenericResponseDataJson<List<String>> resp = new GenericResponseDataJson<List<String>>();
		List<String> userGroups = getUserService().findUserGroups();

		if (userGroups != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("User Groups Available");
			resp.setData(userGroups);
			return resp;
		} else {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No user groups found in DB");
			return resp;
		}
	}

	// Autocompleter
	@RequestMapping(value = "getUsersAccount/{account}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<AdminUserJson>> getUsersAccount(
			@PathVariable("account") String account) {

		GenericResponseDataJson<List<AdminUserJson>> resp = new GenericResponseDataJson<List<AdminUserJson>>();
		List<AdminUserJson> userJsonList = new ArrayList<AdminUserJson>();

		if (account != null && !account.equals("")) {

			List<User> userList = getUserService().getUsersAccount(account);
			if (userList != null && userList.size() != 0) {

				for (User user : userList) {
					AdminUserJson userJ = new AdminUserJson();
					userJ.toJson(user);
					userJsonList.add(userJ);
				}

				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("Users find in DB with this account");
				resp.setData(userJsonList);
				return resp;
			} else {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("No user found in DB with this account");
				return resp;
			}
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Account is null");
			return resp;
		}

	}

	// Autocompleter
	// Mia/json/user/getUsersByName/{account}
	@RequestMapping(value = "getUsersByName/{account}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<AdminUserJson>> getUsersByName(
			@PathVariable("account") String account) {

		GenericResponseDataJson<List<AdminUserJson>> resp = new GenericResponseDataJson<List<AdminUserJson>>();
		List<AdminUserJson> userJsonList = new ArrayList<AdminUserJson>();

		if (account != null && !account.equals("")) {

			List<User> userList = getUserService().getUsersByName(account);
			if (userList != null && userList.size() != 0) {

				for (User user : userList) {
					AdminUserJson userJ = new AdminUserJson();
					userJ.toJson(user);
					userJsonList.add(userJ);
				}

				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("Users find in DB with this name or surname");
				resp.setData(userJsonList);
				return resp;
			} else {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("No user found in DB with this name or surname");
				return resp;
			}
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Account is null");
			return resp;
		}

	}

	public AdminService getAdminService() {
		return adminService;
	}

	public void setAdminService(AdminService adminService) {
		this.adminService = adminService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
