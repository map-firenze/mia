package org.medici.mia.controller.historylog;

import java.util.HashMap;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.historylog.LastAccessedRecordJson;
import org.medici.mia.common.json.historylog.MyResearchHistoryJson;
import org.medici.mia.common.json.historylog.RegisterLogActionJson;
import org.medici.mia.domain.User;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Controller
@RequestMapping("/historyLog")
public class HistoryLogController {
	
	@Autowired
	private UserService userService;

	@Autowired
	private HistoryLogService historyLogService;

	// http://localhost:8181/Mia/json/historyLog/registerLogAction/
	@RequestMapping(value = "registerLogAction", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson logAction(
			@RequestBody RegisterLogActionJson registerLogActionJson) {
		GenericResponseJson resp = new GenericResponseJson();
		if (registerLogActionJson != null
				&& registerLogActionJson.getAction() != null
				&& registerLogActionJson.getRecordType() != null) {
			resp = getHistoryLogService().registerLoaAction(
					registerLogActionJson);
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Data not valid to register log Action");
		}
		return resp;
	}

	// http://localhost:8181/Mia/json/historyLog/getLastAccessedRecords/testuser1
	@RequestMapping(value = "getLastAccessedRecords/{account}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<LastAccessedRecordJson> getLastAccessedRecords(
			@PathVariable("account") String account) {
		GenericResponseDataJson<LastAccessedRecordJson> resp = new GenericResponseDataJson<LastAccessedRecordJson>();

		resp.setData(getHistoryLogService().getLastAccessedRecords(account));
		resp.setStatus(StatusType.ok.toString());
		resp.setMessage("got User last Accessed Records");
		return resp;

	}

	@RequestMapping(value = "getActionsHistory", method = RequestMethod.GET)
	public @ResponseBody HashMap getActionsHistory(
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "perPage", required = false) Integer perPage,
			@RequestParam(value = "recordType", required = false) String recordType,
			@RequestParam(value = "recordId", required = false) Integer recordId){
		if(recordType == null || recordId == null){
			return getHistoryLogService().getActionsHistory(page, perPage);
		} else {
			return getHistoryLogService().getActionsHistory(recordId, recordType, page, perPage);
		}
	}

	// http://localhost:8181/Mia/json/historyLog/getMyResearchHistory/testuser1/10/15/activityDate/desc?type=DE
	@RequestMapping(value = "getMyResearchHistory/{account}/{firstResult}/{maxResult}/{orderColumn}/{ascOrDesc}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<MyResearchHistoryJson> getMyResearchHistory(
			@PathVariable("account") String account, @PathVariable("firstResult") Integer firstResult,
			@PathVariable("maxResult") Integer maxResult, @PathVariable("orderColumn") String orderColumn,
			@PathVariable("ascOrDesc") String ascOrDesc, @RequestParam(value = "type", required = false) String type) {
		
		GenericResponseDataJson<MyResearchHistoryJson> resp = new GenericResponseDataJson<MyResearchHistoryJson>();

		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
		if (!account.equals(user.getAccount()) && !getUserService().isAccountAdministrator(user.getAccount())) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("You do not have an access to view research history for this account");
		} else {
			MyResearchHistoryJson data = getHistoryLogService().getMyResearchHistory(account, firstResult, maxResult,
					orderColumn, ascOrDesc, type);
			resp.setData(data);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("got " + data.getHistoryItems().size() + " items");
		}
		return resp;
	}
	
	// http://localhost:8181/Mia/json/historyLog/deleteMyResearchHistory/testuser1
//	@RequestMapping(value = "deleteMyResearchHistory/{account}", method = RequestMethod.POST)
//	public @ResponseBody GenericResponseDataJson<MyResearchHistoryJson> deleteMyResearchHistory(
//			@PathVariable("account") String account) {
//
//		GenericResponseDataJson<MyResearchHistoryJson> resp = new GenericResponseDataJson<MyResearchHistoryJson>();
//
//		User user = getUserService().findUser(
//				((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
//		if (!account.equals(user.getAccount())) {
//			resp.setStatus(StatusType.w.toString());
//			resp.setMessage("You do not have an access to delete research history for this account");
//		} else {
//			Integer deleted = getHistoryLogService().deleteMyResearchHistory(account);
//			resp.setStatus(StatusType.ok.toString());
//			resp.setMessage("deleted " + deleted + " items");
//		}
//		return resp;
//	}

	public HistoryLogService getHistoryLogService() {
		return historyLogService;
	}

	public void setHistoryLogService(HistoryLogService historyLogService) {
		this.historyLogService = historyLogService;
	}
	
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}