package org.medici.mia.controller.iiif;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.medici.mia.common.util.DateUtils;
import org.medici.mia.iiif.exception.ResourceNotFoundException;
import org.medici.mia.iiif.exception.UnsupportedFormatException;
import org.medici.mia.iiif.model.IIIFImage;
import org.medici.mia.iiif.model.IIIFImageBitDepth;
import org.medici.mia.iiif.model.IIIFImageFormat;
import org.medici.mia.iiif.model.RegionParameters;
import org.medici.mia.iiif.model.ResizeParameters;
import org.medici.mia.iiif.model.RotationParameters;
import org.medici.mia.service.iiif.IIIFImageService;
import org.medici.mia.service.iiif.IIIFImageServiceImpl;
import org.medici.mia.service.iiif.IIIFParameterParserService;
import org.medici.mia.service.iiif.IIIFParameterParserServiceImpl;
import org.medici.mia.service.iiif.MiaIIIFImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Controller
@RequestMapping("/iiif/getIIIFImage/")
public class IIIFImageApiController {

	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private MiaIIIFImageService MiaIIIFImageService;

	// http://localhost:8181/Mia/json/iiif/getIIIFImage/155/full/full/0/default.jpg
	@RequestMapping(value = "{identifier}/{region}/{size}/{rotation}/{quality}.{format}")
	public ResponseEntity<byte[]> getIIIFImage(@PathVariable String identifier,
			@PathVariable String region, @PathVariable String size,
			@PathVariable String rotation, @PathVariable String quality,
			@PathVariable String format, HttpServletRequest request) {

		final HttpHeaders headers = new HttpHeaders();
		ResponseEntity<byte[]> responseEntity = null;
		long start = System.currentTimeMillis();
		logger.info("WHOISONLINEJOB starts at "
				+ DateUtils.getMYSQLDateTime(new DateTime(start)));

		try {

			if (identifier == null || identifier.isEmpty()) {
				return null;
			}

			IIIFParameterParserService iiifParameterParserService = new IIIFParameterParserServiceImpl();
			RegionParameters regionParameters = iiifParameterParserService
					.parseIiifRegion(region);
			ResizeParameters sizeParameters = iiifParameterParserService
					.parseIiifSize(size);
			RotationParameters rotationParameters = iiifParameterParserService
					.parseIiifRotation(rotation);
			IIIFImageBitDepth bitDepthParameter = iiifParameterParserService
					.parseIiifQuality(quality);
			IIIFImageFormat formatParameter = iiifParameterParserService
					.parseIiifFormat(format);

			String filePath = getMiaIIIFImageService().getIIIFImageFileName(
					Integer.valueOf(identifier));

			byte[] file = readContentIntoByteArray(filePath);

			IIIFImageService imageService = new IIIFImageServiceImpl();

			IIIFImage image = imageService.processJAIImage(file,
					regionParameters, sizeParameters, rotationParameters,
					bitDepthParameter, formatParameter);
			final IIIFImageFormat imageFormat = image.getFormat();
			final String mimeType = imageFormat.getMimeType();
			headers.setContentType(MediaType.parseMediaType(mimeType));
			byte[] data = image.toByteArray();

			responseEntity = new ResponseEntity<byte[]>(data, headers,
					HttpStatus.CREATED);

		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}

		long end = System.currentTimeMillis();
		logger.info("WHOISONLINEJOB ends at "
				+ DateUtils.getMYSQLDateTime(new DateTime(end)));
		logger.info("WHOISONLINEJOB work time: "
				+ (new Float(end - start) / 1000));

		return responseEntity;

	}

	private byte[] readContentIntoByteArray(String filePath) {

		if (filePath == null)
			return null;

		// File file = new File("C:/Users/user/Desktop/IIIFImages/iiif.jpg");
		File file = new File(filePath);
		FileInputStream fileInputStream = null;
		byte[] bFile = new byte[(int) file.length()];
		try {
			// convert file into array of bytes
			fileInputStream = new FileInputStream(file);
			fileInputStream.read(bFile);
			fileInputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return bFile;
	}

	private BufferedImage readContentIntoBI(String filePath) {

		if (filePath == null)
			return null;

		// File file = new File("C:/Users/user/Desktop/IIIFImages/iiif.jpg");
		File file = new File(filePath);
		BufferedImage bimg = null;
		try {
			bimg = ImageIO.read(file);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		return bimg;
	}

	// @SuppressWarnings("unchecked")
	// @CrossOrigin(allowedHeaders = {"*"}, origins = {"*"})
	// http://localhost:8181/Mia/json/iiif/getIIIFImage/155/info.json
	@RequestMapping(value = "{identifier}/info.json", method = RequestMethod.GET)
	public ResponseEntity<String> getInfo(@PathVariable String identifier,
			HttpServletRequest request) throws Exception,
			UnsupportedFormatException, UnsupportedOperationException,
			IOException {
		try {
			identifier = URLDecoder.decode(identifier, "UTF-8");
			String filePath = getMiaIIIFImageService().getIIIFImageFileName(
					Integer.valueOf(identifier));

			BufferedImage img = readContentIntoBI(filePath);

			if (img == null) {
				throw new ResourceNotFoundException();
			}

			HttpHeaders headers = new HttpHeaders();
			String contentType = request.getHeader("Accept");
			if (contentType != null
					&& contentType.equals("application/ld+json")) {
				headers.set("Content-Type", contentType);
			} else {
				headers.set("Content-Type", "application/json");
				headers.set(
						"Link",
						"<http://iiif.io/api/image/2/context.json>; "
								+ "rel=\"http://www.w3.org/ns/json-ld#context\"; "
								+ "type=\"application/ld+json\"");
			}

			Map<String, String> info = new HashMap<String, String>();
			// TODO try with session id, if it expired, re-login
			info.put("@context", "http://iiif.io/api/image/2/context.json");
			info.put("@id", filePath);
			info.put("width", new Integer(img.getWidth()).toString());
			info.put("height", new Integer(img.getHeight()).toString());
			info.put("protocol", "http://iiif.io/api/image");

			String json = new ObjectMapper().writeValueAsString(info);

			return new ResponseEntity(json, headers, HttpStatus.OK);
		} catch (Exception ex) {
			throw new UnsupportedFormatException(ex.getMessage());
		}
	}

	// @CrossOrigin(allowedHeaders = {"*"}, origins = {"*"})
	// @RequestMapping(value = "{identifier}", method = RequestMethod.GET)
	// public String getInfoRedirect(@PathVariable String identifier
	// ) {
	// return "redirect:/image/" + VERSION + "/" + identifier + "/info.json";
	// }

	public MiaIIIFImageService getMiaIIIFImageService() {
		return MiaIIIFImageService;
	}

	public void setMiaIIIFImageService(MiaIIIFImageService miaIIIFImageService) {
		MiaIIIFImageService = miaIIIFImageService;
	}

}