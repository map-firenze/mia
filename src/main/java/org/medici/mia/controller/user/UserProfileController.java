package org.medici.mia.controller.user;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.UserProfileChangePswdJson;
import org.medici.mia.common.json.UserProfileJson;
import org.medici.mia.common.json.administration.ChangePasswordJson;
import org.medici.mia.common.json.personalprofile.CountryJson;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.common.util.RegExUtils;
import org.medici.mia.controller.upload.FileUploadForm;
import org.medici.mia.domain.Country;
import org.medici.mia.domain.User;
import org.medici.mia.domain.UserStatistics;
import org.medici.mia.domain.UserAuthority.Authority;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.iiif.model.IIIFImage;
import org.medici.mia.iiif.model.IIIFImageBitDepth;
import org.medici.mia.iiif.model.IIIFImageFormat;
import org.medici.mia.iiif.model.RegionParameters;
import org.medici.mia.iiif.model.ResizeParameters;
import org.medici.mia.iiif.model.RotationParameters;
import org.medici.mia.service.iiif.IIIFImageService;
import org.medici.mia.service.iiif.IIIFImageServiceImpl;
import org.medici.mia.service.iiif.IIIFParameterParserService;
import org.medici.mia.service.iiif.IIIFParameterParserServiceImpl;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Controller
@RequestMapping("/userprofile")
public class UserProfileController {

	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private UserService userService;

	// find user Profile
	// http://localhost:8181/Mia/json/userprofile/findUser/
	@RequestMapping(value = "findUser", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<UserProfileJson> findUser() {

		GenericResponseDataJson<UserProfileJson> resp = new GenericResponseDataJson<UserProfileJson>();

		User user = null;
		try {
			user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername(),
					true);
		} catch (ApplicationThrowable applicationThrowable) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Error retrieving the User logged in");
			return resp;
		}

		if (user != null) {
			UserProfileJson userProfileJson = new UserProfileJson();
			userProfileJson.toJson(user);
			boolean isImage = getUserService().isUserPortrait(
					user.getPortraitImageName());
			if (isImage) {
				userProfileJson.setPhoto("yes");
				userProfileJson
						.setPhotoPath("/Mia/user/ShowPortraitUser.do?account="
								+ user.getAccount());
			} else {
				userProfileJson.setPhoto("no");
				userProfileJson.setPhotoPath("/Mia/images/1024/img_user.png");
			}

			boolean isCV = getUserService().isUserCV(user.getResumeName());
			if (isCV) {
				userProfileJson.setResume("yes");
				userProfileJson
						.setResumePath("/Mia/json/userprofile/showResumeUser");

			} else {
				userProfileJson.setResume("no");
				userProfileJson
						.setResumePath("/Mia/images/1024/img_resume.png");
			}
			resp.setData(userProfileJson);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("User logged in not found");
			return resp;
		}
	}

	// http://localhost:8181/Mia/json/userprofile/modifyUserProfile/
	@RequestMapping(value = "modifyUserProfile", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyUserProfile(
			@RequestBody UserProfileJson userProfile) {

		GenericResponseJson resp = new GenericResponseJson();
		if (userProfile == null) {
			resp.setMessage("user profile may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		if (isUserProfileValid(userProfile).getStatus().equals(StatusType.ko)) {
			return resp;
		}

		User user = null;
		try {
			user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());

		} catch (ApplicationThrowable applicationThrowable) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Error retrieving the User logged in");
			return resp;
		}

		Integer userModified = getUserService().modifyUserProfile(userProfile,
				user);

		if (userModified != null && userModified > 0) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("User modified in DB");
			return resp;
		} else {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("User not modfied in DB");
			return resp;
		}
	}

	private GenericResponseJson isUserProfileValid(UserProfileJson userProfile) {
		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.ok.toString());
		if (!RegExUtils.checkMail(userProfile.getEmail())) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("User email is not valis.");
			return resp;

		}
		if (userProfile.getFirstName() == null
				|| userProfile.getFirstName().isEmpty()
				|| userProfile.getFirstName().length() > 50) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("First name is not valid.");
			return resp;

		}
		if (userProfile.getAddress() != null
				&& !userProfile.getAddress().isEmpty()
				&& userProfile.getAddress().length() > 200) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Adress is not valid.");
			return resp;

		}
		if (userProfile.getAffiliation() == null
				|| userProfile.getAffiliation().isEmpty()
				|| userProfile.getAffiliation().length() > 50) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Affiliation [Organization] is not valid.");
			return resp;

		}

		if (userProfile.getInterests() != null
				&& !userProfile.getInterests().isEmpty()
				&& userProfile.getInterests().length() > 500) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("User Interests field is not valid.");
			return resp;

		}

		if (userProfile.getAcademiaEduLink() != null
				&& !userProfile.getAcademiaEduLink().isEmpty()
				&& userProfile.getAcademiaEduLink().length() > 500) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("AcademiaEduLink is not valid.");
			return resp;

		}
		return resp;
	}

	// Change user password
	// http://localhost:8181/Mia/json/userprofile/changeUserPassword/
	@RequestMapping(value = "changeUserPassword", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson changeUserPassword(
			@RequestBody final UserProfileChangePswdJson changeUserPswd) {

		GenericResponseJson resp = new GenericResponseJson();

		// data validation before change user password
		if (changeUserPswd != null) {

			ChangePasswordJson changePasswordJson = userService
					.isAccountAutorizedForChangePassword(changeUserPswd
							.getAccount());

			if (changePasswordJson.getAutorized() == null
					|| !changePasswordJson.getAutorized()) {
				resp.setStatus(StatusType.koPwd.toString());
				resp.setMessage("User is not autorized to change password");
				return resp;
			}

			Boolean proceed = Boolean.FALSE;

			if (Authority.ADMINISTRATORS.equals(changePasswordJson
					.getAuthority())) {
				proceed = Boolean.TRUE;
			} else {
				proceed = userService.checkUserPassword(
						changeUserPswd.getAccount(),
						changeUserPswd.getOldPassword());
			}

			if (proceed) {

				if (changeUserPswd.getNewPassword().equals(
						changeUserPswd.getNewPasswordConfirm())) {

					userService.updateUserPassword(changeUserPswd);

					resp.setStatus(StatusType.ok.toString());
					resp.setMessage("Your password has been successfully RESET");
					return resp;
				} else {
					resp.setStatus(StatusType.ko.toString());
					resp.setMessage("New Password and Confirm New Password do not match - User Password NOT MODIFIED");
					return resp;
				}

			} else {
				resp.setStatus(StatusType.koPwd.toString());
				resp.setMessage("Your Old Password was typed incorrectly - User Password NOT MODIFIED");
				return resp;
			}

		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("The Change User Password request is not available - User Password NOT MODIFIED");
			return resp;
		}
	}

	@RequestMapping(value = "uploadPortraitUser", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson uploadPortraitUser(
			@ModelAttribute("files") FileUploadForm filesForm,
			HttpSession session) {
		GenericResponseJson resp = new GenericResponseJson();
		if (filesForm == null || filesForm.getFiles() == null
				|| filesForm.getFiles().isEmpty()) {
			resp.setMessage("User portrait image may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		User user = null;

		try {
			user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());
		} catch (ApplicationThrowable applicationThrowable) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Error retrieving the User logged in");
			return resp;
		}

		Integer countwriteFile = getUserService().uploadUserPortrait(
				filesForm.getFiles().get(0), user, false);
		if (countwriteFile > 0) {
			resp.setMessage("Portrait Image copied Successfuly.");
			resp.setStatus(StatusType.ok.toString());
		} else if (countwriteFile < 0) {
			resp.setMessage("There was a problem in creating directory where the image should be copied.Control if the path in db and directoy in file system exist.");
			resp.setStatus(StatusType.ko.toString());
		} else {
			resp.setMessage("There was a problem in creating user image. User portrait image received but not copied successfuly.");
			resp.setStatus(StatusType.ko.toString());
		}

		return resp;
	}

	@RequestMapping(value = "uploadCV", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson uploadCV(
			@ModelAttribute("files") FileUploadForm filesForm,
			HttpSession session) {
		GenericResponseJson resp = new GenericResponseJson();
		if (filesForm == null || filesForm.getFiles() == null
				|| filesForm.getFiles().isEmpty()) {
			resp.setMessage("CV file may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		User user = null;

		try {
			user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());
		} catch (ApplicationThrowable applicationThrowable) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Error retrieving the User logged in");
			return resp;
		}

		Integer countwriteFile = getUserService().uploadUserPortrait(
				filesForm.getFiles().get(0), user, true);
		if (countwriteFile > 0) {
			resp.setMessage("User CV copied Successfuly.");
			resp.setStatus(StatusType.ok.toString());
		} else if (countwriteFile < 0) {
			resp.setMessage("There was a problem in reading the value of resume.person.path in DB or in creating the directory where CV should be copied. Control if the path in db and directoy in file system exist.");
			resp.setStatus(StatusType.ko.toString());
		} else {
			resp.setMessage("There was a problem in creating user CV. User CV received but not copied successfuly.");
			resp.setStatus(StatusType.ko.toString());
		}

		return resp;
	}

	@RequestMapping(value = "uploadPortraitUserTest", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson uploadPortraitUserTest(
			@ModelAttribute("portrait") File portrait) {

		GenericResponseJson resp = new GenericResponseJson();
		if (portrait == null) {
			resp.setMessage("user portrait image may be null or empty.");
			resp.setStatus(StatusType.ko.toString());

		} else {
			resp.setMessage("user portrait image received.");
			resp.setStatus(StatusType.ok.toString());
		}

		return resp;
	}

	// find user Profile
	// http://localhost:8181/Mia/json/userprofile/findCountries/IT/

	@RequestMapping(value = "findCountries/{description}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<CountryJson>> findCountries(
			@PathVariable("description") String description) {

		GenericResponseDataJson<List<CountryJson>> resp = new GenericResponseDataJson<List<CountryJson>>();

		List<Country> countries = getUserService().findCountries(description);
		if (countries != null && !countries.isEmpty()) {
			List<CountryJson> countriesJson = new ArrayList<CountryJson>();

			for (Country country : countries) {
				CountryJson countryJson = new CountryJson();
				countryJson.toJson(country);
				countriesJson.add(countryJson);

			}

			resp.setData(countriesJson);
		}

		return resp;

	}

	private byte[] readContentIntoByteArray(File file) {

		if (file == null)
			return null;

		// File file = new File("C:/Users/user/Desktop/IIIFImages/iiif.jpg");

		FileInputStream fileInputStream = null;
		byte[] bFile = new byte[(int) file.length()];
		try {
			// convert file into array of bytes
			fileInputStream = new FileInputStream(file);
			fileInputStream.read(bFile);
			fileInputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return bFile;
	}

	// http://localhost:8181/Mia/json/userprofile/showProfileImage/testuser1.jpg
	// http://localhost:8181/Mia/json/userprofile/getImage/155/full/full/0/default.jpg

	@RequestMapping(value = "getImage/.{format}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<byte[]> getImage(
			@PathVariable String format, HttpServletRequest request) {

		final HttpHeaders headers = new HttpHeaders();
		ResponseEntity<byte[]> responseEntity = null;

		try {

			User user = null;

			try {
				user = getUserService().findUser(
						((UserDetails) SecurityContextHolder.getContext()
								.getAuthentication().getPrincipal())
								.getUsername());
			} catch (ApplicationThrowable applicationThrowable) {

			}

			// http://localhost:8181/Mia/json/iiif/getIIIFImage/155/full/full/0/default.jpg
			// {identifier}/{region}/{size}/{rotation}/{quality}.{format}

			IIIFParameterParserService iiifParameterParserService = new IIIFParameterParserServiceImpl();

			RegionParameters regionParameters = iiifParameterParserService
					.parseIiifRegion("full");
			ResizeParameters sizeParameters = iiifParameterParserService
					.parseIiifSize("full");
			RotationParameters rotationParameters = iiifParameterParserService
					.parseIiifRotation("0");
			IIIFImageBitDepth bitDepthParameter = iiifParameterParserService
					.parseIiifQuality("default");
			IIIFImageFormat formatParameter = iiifParameterParserService
					.parseIiifFormat(format);

			String imageDir = ApplicationPropertyManager
					.getApplicationProperty("portrait.user.path");
			String filePath = imageDir
					+ org.medici.mia.common.util.FileUtils.OS_SLASH
					+ user.getPortraitImageName();

			byte[] file = readContentIntoByteArray(filePath);

			IIIFImageService imageService = new IIIFImageServiceImpl();

			IIIFImage image = imageService.processJAIImage(file,
					regionParameters, sizeParameters, rotationParameters,
					bitDepthParameter, formatParameter);
			final IIIFImageFormat imageFormat = image.getFormat();
			final String mimeType = imageFormat.getMimeType();
			headers.setContentType(MediaType.parseMediaType(mimeType));
			byte[] data = image.toByteArray();

			headers.setContentType(MediaType.IMAGE_JPEG);

			responseEntity = new ResponseEntity<byte[]>(data, headers,
					HttpStatus.CREATED);

		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}

		return responseEntity;

	}

	private byte[] readContentIntoByteArray(String filePath) {

		if (filePath == null)
			return null;

		// File file = new File("C:/Users/user/Desktop/IIIFImages/iiif.jpg");
		File file = new File(filePath);
		FileInputStream fileInputStream = null;
		byte[] bFile = new byte[(int) file.length()];
		try {
			// convert file into array of bytes
			fileInputStream = new FileInputStream(file);
			fileInputStream.read(bFile);
			fileInputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return bFile;
	}

	@RequestMapping(value = "showResumeUser", method = RequestMethod.GET)
	public void showResumeUser(HttpServletRequest request,
			HttpServletResponse httpServletResponse) {
		User user = null;

		try {
			user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());
		} catch (ApplicationThrowable applicationThrowable) {
			logger.error("error on reading user");
			return;
		}

		try {
			user = getUserService().findUser(user.getAccount());

			if (user.getResumeName() != null && !user.getResumeName().isEmpty()) {
				String resumeDir = ApplicationPropertyManager
						.getApplicationProperty("resume.person.path");
				String accountstring = new String(user.getAccount());
				InputStream inputStream = new FileInputStream(resumeDir + "/"
						+ accountstring + "CV.pdf");
				OutputStream outputStream = httpServletResponse
						.getOutputStream();
				httpServletResponse.reset();
				httpServletResponse.setContentType("application/pdf");
				IOUtils.copyLarge(inputStream, outputStream);
				httpServletResponse.getOutputStream().flush();
			} else {
				return;
			}
		} catch (IOException ioException) {
			logger.error("error on reading resume", ioException);
			// need to return default image error
		} catch (ApplicationThrowable applicationThrowable) {
			logger.error("error on reading image", applicationThrowable);
		}
	}
	
	@RequestMapping(
			value = "getUserStatistics", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<UserStatistics> 
	getUserStatistics(@RequestParam("account") String account) {
		
		User user = getUserService().findUser(account);
		
		GenericResponseDataJson<UserStatistics> response = 
				new GenericResponseDataJson<UserStatistics>();
		
		UserStatistics userStatistics = 
				getUserService().getUserStatistics(user);
		
		if (userStatistics == null) {
			response.setStatus(StatusType.ok.toString());
			response.setData(null);
			response.setMessage(String.format(
					"No public statistics for account '%s'", account));
		}
		else {
			response.setStatus(StatusType.ok.toString());
			response.setData(userStatistics);
		}
		
		return response;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}