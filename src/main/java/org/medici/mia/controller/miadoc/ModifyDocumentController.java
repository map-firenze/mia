package org.medici.mia.controller.miadoc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.medici.mia.common.json.ChineseSynopsisJson;
import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.LanguageJson;
import org.medici.mia.common.json.RelatedDocsJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.SynopsisJson;
import org.medici.mia.common.json.document.BasicDescriptionJson;
import org.medici.mia.common.json.document.DocumentLanguageJson;
import org.medici.mia.common.json.document.DocumentTranscriptionJson;
import org.medici.mia.common.json.document.IModifyDocJson;
import org.medici.mia.common.json.document.TopicPlaceJson;
import org.medici.mia.domain.EntityType;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.domain.LanguageEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.miadoc.JsonType;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.miadoc.ModifyDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import de.danielbechler.diff.ObjectDifferBuilder;
import de.danielbechler.diff.node.DiffNode;
import de.danielbechler.diff.node.Visit;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Controller
@RequestMapping("/modifyDocument")
public class ModifyDocumentController {

	@Autowired
	private ModifyDocumentService modifyDocumentService;

	@Autowired
	private MiaDocumentService miaDocumentService;

	@Autowired
	private HistoryLogService historyLogService;

	// localhost:8181/Mia/json/modifyDocument/findDocumentTranscriptions/208
	@RequestMapping(value = "findDocumentTranscriptions/{documentId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<IModifyDocJson>>> findDocumentTranscriptions(
			@PathVariable("documentId") Integer documentId) {

		List<IModifyDocJson> genericJsons = getModifyDocumentService()
				.findEntities(documentId, JsonType.transcription);
		if(genericJsons != null) {
			for (IModifyDocJson iModifyDocJson : genericJsons) {
				if (iModifyDocJson instanceof DocumentTranscriptionJson) {
					DocumentTranscriptionJson t = (DocumentTranscriptionJson)iModifyDocJson;
					if (t.getUploadedFileId() == null || t.getUploadedFileId().equalsIgnoreCase("null")) {
						t.setUploadedFileId("-1");
					}
				}
			}
		}

		return setResponseList(genericJsons, JsonType.transcription.toString());

	}

	// localhost:8181/Mia/json/modifyDocument/modifyTranscriptions/
	@RequestMapping(value = "modifyTranscriptions", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyTranscriptions(
			@RequestBody List<DocumentTranscriptionJson> jsons,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();

		if (jsons == null || jsons.isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("transcription list maybe null or empty.");
			return resp;
		}

		Integer documentId = null;

		List<IModifyDocJson> genericJsons = new ArrayList<IModifyDocJson>();
		for (DocumentTranscriptionJson json : jsons) {
			documentId = Integer.valueOf(json.getDocumentId());
			IModifyDocJson genjson = (IModifyDocJson) json;
			genericJsons.add(genjson);
		}
		if(documentId != null && jsons != null && jsons.size() > 0) {
			MiaDocumentEntity documentEntity = 
					miaDocumentService.findDocumentEntityById(documentId);
			
			DocumentTranscriptionJson editedTranscription = jsons.get(0);
			
			historyLogService.registerTranscriptionModification(
					documentEntity, 
					editedTranscription);
		}

		for (IModifyDocJson iModifyDocJson : genericJsons) {
			if (iModifyDocJson instanceof DocumentTranscriptionJson) {
				DocumentTranscriptionJson t = (DocumentTranscriptionJson)iModifyDocJson;
				if (t.getUploadedFileId() != null) {
					if (t.getUploadedFileId().equalsIgnoreCase("-1")) {
						t.setUploadedFileId(null);
					}
				}
			}
		}

		return getModifyDocumentService().modifyEntities(genericJsons,
				EntityType.transcription);

	}

	// Synopsis - find & modify
	// localhost:8181/Mia/json/modifyDocument/findSynopsis/208
	@RequestMapping(value = "findSynopsis/{documentId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, IModifyDocJson>> findSynopsis(
			@PathVariable("documentId") Integer documentId) {

		List<IModifyDocJson> genericJsons = getModifyDocumentService()
				.findEntities(documentId, JsonType.synopsis);

		return setResponse(genericJsons, JsonType.synopsis.toString());

	}

	// localhost:8181/Mia/json/modifyDocument/modifySynopsis/
	@RequestMapping(value = "modifySynopsis", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifySynopsis(
			@RequestBody SynopsisJson synopsisJson,
			HttpServletRequest request) {

		List<SynopsisJson> jsons = new ArrayList<SynopsisJson>();
		jsons.add(synopsisJson);

		GenericResponseJson resp = new GenericResponseJson();

		Integer documentId = null;

		if (jsons == null || jsons.isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Synopsis  maybe null or empty.");
			return resp;
		}
		List<IModifyDocJson> genericJsons = new ArrayList<IModifyDocJson>();
		for (SynopsisJson json : jsons) {
			documentId = Integer.valueOf(json.getDocumentId());
			IModifyDocJson genjson = (IModifyDocJson) json;
			genericJsons.add(genjson);
		}

		if(documentId != null) {
			MiaDocumentEntity docEnt = miaDocumentService.findDocumentEntityById(documentId);
			HashMap<String, HashMap> changes = new HashMap<String, HashMap>();
			HashMap<String, HashMap> category = new HashMap<String, HashMap>();
			HashMap<String, HashMap> action = new HashMap<String, HashMap>();
			HashMap<Object, Object> before = new HashMap<Object, Object>();
			HashMap<Object, Object> after = new HashMap<Object, Object>();
			if(docEnt.getGeneralNotesSynopsis() != null) {
				if (!docEnt.getGeneralNotesSynopsis().equals(jsons.get(0).getGeneralNotesSynopsis())) {
					before.put("synopsis", docEnt.getGeneralNotesSynopsis());
					after.put("synopsis", jsons.get(0).getGeneralNotesSynopsis());
					changes.put("before", before);
					changes.put("after", after);
					action.put("edit", changes);
					category.put("documentSynopsis", action);

					historyLogService.registerAction(
							documentId, HistoryLogEntity.UserAction.EDIT,
							HistoryLogEntity.RecordType.DE, null, category);
				}
			} else {
				before.put("synopsis", null);
				after.put("synopsis", jsons.get(0).getGeneralNotesSynopsis());
				changes.put("before", before);
				changes.put("after", after);
				action.put("edit", changes);
				category.put("documentSynopsis", action);

				historyLogService.registerAction(
						documentId, HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.DE, null, category);
			}
		}

		return getModifyDocumentService().modifyEntities(genericJsons,
				EntityType.genericdocument);

	}
	
	// ChineseSynopsis - find & modify
	// localhost:8181/Mia/json/modifyDocument/findChineseSynopsis/208
	@RequestMapping(
			value = "findChineseSynopsis/{documentId}", 
			method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap
		<String, IModifyDocJson>> 
	findChineseSynopsis(
			@PathVariable("documentId") Integer documentId) {

		List<IModifyDocJson> genericJsons = getModifyDocumentService()
				.findEntities(documentId, JsonType.chineseSynopsis);

		return setResponse(genericJsons, JsonType.chineseSynopsis.toString());

	}
	
	// localhost:8181/Mia/json/modifyDocument/modifyChineseSynopsis/
		@RequestMapping(
				value = "modifyChineseSynopsis", 
				method = RequestMethod.POST)
		public @ResponseBody GenericResponseJson modifyChinesSynopsis(
				@RequestBody ChineseSynopsisJson chineseSynopsisJson,
				HttpServletRequest request) {
			
			List<ChineseSynopsisJson> jsons = 
					new ArrayList<ChineseSynopsisJson>();
			
			jsons.add(chineseSynopsisJson);
		
			Integer documentId = Integer.decode(
					chineseSynopsisJson.getDocumentId());
			
			List<IModifyDocJson> genericJsons = new ArrayList<IModifyDocJson>();
			for (ChineseSynopsisJson json : jsons) {
				documentId = Integer.valueOf(json.getDocumentId());
				IModifyDocJson genjson = (IModifyDocJson) json;
				genericJsons.add(genjson);
			}
			
			updatesSynopsisHistory(
					documentId, 
					chineseSynopsisJson.getChineseSynopsis());



			return getModifyDocumentService().modifyEntities(genericJsons,
					EntityType.genericdocument);

		}

	// Basic Description - find & modify
	// localhost:8181/Mia/json/modifyDocument/findBasicDescription/208
	@RequestMapping(value = "findBasicDescription/{documentId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, IModifyDocJson>> findBasicDescription(
			@PathVariable("documentId") Integer documentId) {

		List<IModifyDocJson> genericJsons = getModifyDocumentService()
				.findEntities(documentId, JsonType.basicDescription);

		return setResponse(genericJsons, JsonType.basicDescription.toString());

	}

	// localhost:8181/Mia/json/modifyDocument/modifyBasciDescription/
	@RequestMapping(value = "modifyBasicDescription", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyBasicDescription(
			@RequestBody BasicDescriptionJson basicDescriptionJson,
			HttpServletRequest request) {

		List<BasicDescriptionJson> jsons = new ArrayList<BasicDescriptionJson>();
		jsons.add(basicDescriptionJson);

		GenericResponseJson resp = new GenericResponseJson();

		if (jsons == null || jsons.isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Basic Description list maybe null or empty.");
			return resp;
		}

		Integer documentId = null;

		List<IModifyDocJson> genericJsons = new ArrayList<IModifyDocJson>();
		for (BasicDescriptionJson json : jsons) {
			documentId = Integer.valueOf(json.getDocumentId());
			IModifyDocJson genjson = (IModifyDocJson) json;
			genericJsons.add(genjson);

		}

		final BasicDescriptionJson oldjson = new BasicDescriptionJson();
		oldjson.toJson(miaDocumentService.findDocumentEntityById(documentId));

		resp = getModifyDocumentService().modifyEntities(genericJsons,
				EntityType.genericdocument);

		if(documentId != null && resp.getStatus().equals(StatusType.ok.toString())) {
			final BasicDescriptionJson newjson = jsons.get(0);
			final HashMap<Object, Object> before = new HashMap<Object, Object>();
			final HashMap<Object, Object> after = new HashMap<Object, Object>();

			DiffNode diff = ObjectDifferBuilder.buildDefault().compare(oldjson,newjson);
			diff.visit(new DiffNode.Visitor()
			{
				public void node(DiffNode node, Visit visit)
				{
					final Object baseValue = node.canonicalGet(oldjson);
					final Object workingValue = node.canonicalGet(newjson);
					if (node.hasChanges() && !node.getPath().toString().equals("/")) {
						if(!node.hasChildren()) {
							before.put(node.getPath().getLastElementSelector(), baseValue);
							after.put(node.getPath().getLastElementSelector(), workingValue);
						} else {
							visit.dontGoDeeper();
						}
					}
				}
			});

			HashMap<String, Object> changes = new HashMap<String, Object>();
			HashMap<String, HashMap> category = new HashMap<String, HashMap>();
			HashMap<String, HashMap> action = new HashMap<String, HashMap>();
			changes.put("before", before);
			changes.put("after", after);
			action.put("edit", changes);
			category.put("basicDescription", action);
			if(before.size() > 0 && after.size() > 0) {
				historyLogService.registerAction(
						documentId, HistoryLogEntity.UserAction.EDIT,
						HistoryLogEntity.RecordType.DE, null, category);
			}
		}
		return resp;

	}

	// Category - find
	// localhost:8181/Mia/json/modifyDocument/findDocumentCategory/208
	@RequestMapping(value = "findDocumentCategory/{documentId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, IModifyDocJson>> findDocumentCategory(
			@PathVariable("documentId") Integer documentId) {

		List<IModifyDocJson> genericJsons = getModifyDocumentService()
				.findEntities(documentId, JsonType.category);

		return setResponse(genericJsons, JsonType.category.toString());

	}

	// Language find & Modify & finAllLanguages

	// localhost:8181/Mia/json/modifyDocument/findDocumentLanguages/208
	@RequestMapping(value = "findDocumentLanguages/{documentId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, IModifyDocJson>> findDocumentLanguages(
			@PathVariable("documentId") Integer documentId) {

		List<IModifyDocJson> genericJsons = getModifyDocumentService()
				.findEntities(documentId, JsonType.documentLanguage);

		return setResponse(genericJsons, JsonType.documentLanguage.toString());
	}

	// localhost:8181/Mia/json/modifyDocument/modifyDocumentLanguages/
	@RequestMapping(value = "modifyDocumentLanguages", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyDocumentLanguages(
			@RequestBody DocumentLanguageJson documentLanguageJson,
			HttpServletRequest request) {

		List<DocumentLanguageJson> jsons = new ArrayList<DocumentLanguageJson>();
		jsons.add(documentLanguageJson);

		GenericResponseJson resp = new GenericResponseJson();

		if (jsons == null || jsons.isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Document Languages maybe null or empty.");
			return resp;
		}

		Integer documentId = null;

		List<IModifyDocJson> genericJsons = new ArrayList<IModifyDocJson>();
		for (DocumentLanguageJson json : jsons) {
			documentId = Integer.valueOf(json.getDocumentId());
			IModifyDocJson genjson = (IModifyDocJson) json;
			genericJsons.add(genjson);

		}

		MiaDocumentEntity docEnt = miaDocumentService.findDocumentEntityById(documentId);
		List<LanguageJson> langugesOld = new ArrayList<LanguageJson>();
		for(LanguageEntity l: docEnt.getLanguages()){
			langugesOld.add(new LanguageJson(l.getId(),l.getLanguage()));
		}
		HashMap<Object, Object> before = new HashMap<Object, Object>();
		HashMap<Object, Object> after = new HashMap<Object, Object>();
		HashMap<String, HashMap> changes = new HashMap<String, HashMap>();
		HashMap<String, HashMap> category = new HashMap<String, HashMap>();
		HashMap<String, HashMap> action = new HashMap<String, HashMap>();
		boolean equal = true;
		if(langugesOld.size() == documentLanguageJson.getLanguages().size()) {
			for (int i = 0; i < documentLanguageJson.getLanguages().size(); i++) {
				if (!documentLanguageJson.getLanguages().get(i).getId().equals(langugesOld.get(i).getId())) {
					equal = false;
				}
			}
		} else {
			equal = false;
		}
		if(!equal){
			after.put("languages", documentLanguageJson.getLanguages());
			before.put("languages", langugesOld);

			changes.put("before", before);
			changes.put("after", after);
			action.put("edit", changes);
			category.put("basicDescription", action);

			historyLogService.registerAction(
					documentId, HistoryLogEntity.UserAction.EDIT,
					HistoryLogEntity.RecordType.DE, null, category);
		}

        return getModifyDocumentService().modifyEntities(genericJsons,
				EntityType.genericdocument);

	}

	// Topic Place find and modify

	// localhost:8181/Mia/json/modifyDocument/findDocumentTopics/128
	@RequestMapping(value = "findDocumentTopics/{documentId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<IModifyDocJson>>> findDocumentTopics(
			@PathVariable("documentId") Integer documentId) {

		List<IModifyDocJson> genericJsons = getModifyDocumentService()
				.findEntities(documentId, JsonType.topic);
		return setResponseList(genericJsons, JsonType.topic.toString());

	}

	// localhost:8181/Mia/json/modifyDocument/modifyDocumentTopics/
	@RequestMapping(value = "modifyDocumentTopics", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyDocumentTopics(
			@RequestBody List<TopicPlaceJson> jsons,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();

		if (jsons == null || jsons.isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("transcription list maybe null or empty.");
			return resp;
		}
		//control and get principal places
		List<TopicPlaceJson> newJsons = getMiaDocumentService().getPrinciplaTopicPlaces(jsons);

		List<IModifyDocJson> genericJsons = new ArrayList<IModifyDocJson>();
		for (TopicPlaceJson json : newJsons) {
			IModifyDocJson genjson = (IModifyDocJson) json;
			genericJsons.add(genjson);

		}
		return getModifyDocumentService().modifyEntities(genericJsons,
				EntityType.topic);

	}

	// localhost:8181/Mia/json/modifyDocument/findRelatedDocuments/125
	@RequestMapping(value = "findRelatedDocuments/{documentId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, IModifyDocJson>> findRelatedDocuments(
			@PathVariable("documentId") Integer documentId) {

		List<IModifyDocJson> genericJsons = getModifyDocumentService()
				.findEntities(documentId, JsonType.relatedDocs);

		return setResponse(genericJsons, JsonType.relatedDocs.toString());

	}

	// localhost:8181/Mia/json/modifyDocument/modifyRelatedDocuments/
	@RequestMapping(value = "modifyRelatedDocuments", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyRelatedDocuments(
			@RequestBody RelatedDocsJson relatedDocsJson,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();

		if (relatedDocsJson == null || relatedDocsJson.getRelatedDocs() == null
				|| relatedDocsJson.getRelatedDocs().isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Related Documents maybe null or empty.");
			return resp;
		}

		List<Integer> docsNotFound = getModifyDocumentService()
				.findNotFoundDocsForRelatedDocuments(
						relatedDocsJson.getRelatedDocs());
		if (docsNotFound != null && !docsNotFound.isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			String docNotFoundList = "The Document is not modifid. Some relatedDocs may not exist in DB. The list of Not found Id's: ";
			for (Integer docs : docsNotFound) {
				docNotFoundList = docNotFoundList + docs + ", ";
			}
			resp.setMessage(docNotFoundList);

			return resp;
		}

		List<RelatedDocsJson> jsons = new ArrayList<RelatedDocsJson>();
		jsons.add(relatedDocsJson);

		List<IModifyDocJson> genericJsons = new ArrayList<IModifyDocJson>();
		for (RelatedDocsJson json : jsons) {
			IModifyDocJson genjson = (IModifyDocJson) json;
			genericJsons.add(genjson);

		}
		return getModifyDocumentService().modifyEntities(genericJsons,
				EntityType.genericdocument);

	}

	private GenericResponseDataJson<HashMap<String, List<IModifyDocJson>>> setResponseList(
			List<IModifyDocJson> jsons, String jsonName) {
		GenericResponseDataJson<HashMap<String, List<IModifyDocJson>>> resp = new GenericResponseDataJson<HashMap<String, List<IModifyDocJson>>>();
		HashMap<String, List<IModifyDocJson>> docHash = new HashMap<String, List<IModifyDocJson>>();

		String jsonNameList = jsonName + "s";

		if (jsons != null && !jsons.isEmpty()) {
			docHash.put(jsonNameList, jsons);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + jsons.size() + " " + jsonName
					+ "s in total.");
			resp.setData(docHash);
			return resp;
		} else {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No " + jsonName + " exists in DB. ");
			return resp;
		}

	}

	private GenericResponseDataJson<HashMap<String, IModifyDocJson>> setResponse(
			List<IModifyDocJson> jsons, String jsonName) {
		GenericResponseDataJson<HashMap<String, IModifyDocJson>> resp = new GenericResponseDataJson<HashMap<String, IModifyDocJson>>();
		HashMap<String, IModifyDocJson> docHash = new HashMap<String, IModifyDocJson>();

		if (jsons != null && !jsons.isEmpty()) {
			docHash.put(jsonName, jsons.get(0));
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + jsonName);
			resp.setData(docHash);
			return resp;
		} else {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No " + jsonName + " exists in DB. ");
			return resp;
		}

	}
	
	private void updatesSynopsisHistory(
			Integer documentId, String synopsisAfterModifications) {
		
		MiaDocumentEntity document = 
				miaDocumentService.findDocumentEntityById(documentId);
		
		if (document == null) {
			return;
		}
		
		String synopsisBeforeModifications = document.getChineseSynopsis();
		
		if(synopsisBeforeModifications == null) {
			saveSynopsisModificationInHistory(
					documentId, 
					synopsisBeforeModifications, 
					synopsisAfterModifications);
			
		} else if (!synopsisBeforeModifications.equals(
					synopsisAfterModifications)) {
			saveSynopsisModificationInHistory(
				documentId, 
				synopsisBeforeModifications, 
				synopsisAfterModifications);
		}
	}
	
	@SuppressWarnings("rawtypes")
	private void saveSynopsisModificationInHistory (
			Integer documentId, 
			String synopsisBeforeModifications, 
			String synopsisAfterModifications) {
		
		HashMap<String, HashMap> changes = new HashMap<String, HashMap>();
		HashMap<String, HashMap> category = new HashMap<String, HashMap>();
		HashMap<String, HashMap> action = new HashMap<String, HashMap>();
		HashMap<Object, Object> before = new HashMap<Object, Object>();
		HashMap<Object, Object> after = new HashMap<Object, Object>();
		
		before.put("synopsis", synopsisBeforeModifications);
		after.put("synopsis", synopsisAfterModifications);
		changes.put("before", before);
		changes.put("after", after);
		action.put("edit", changes);
		category.put("documentSynopsis", action);

		historyLogService.registerAction(
				documentId, 
				HistoryLogEntity.UserAction.EDIT,
				HistoryLogEntity.RecordType.DE, 
				null, 
				category);
	}

	public ModifyDocumentService getModifyDocumentService() {
		return modifyDocumentService;
	}

	public void setModifyDocumentService(
			ModifyDocumentService modifyDocumentService) {
		this.modifyDocumentService = modifyDocumentService;
	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

	public void setMiaDocumentService(MiaDocumentService miaDocumentService) {
		this.miaDocumentService = miaDocumentService;
	}

	public HistoryLogService getHistoryLogService() {
		return historyLogService;
	}
}