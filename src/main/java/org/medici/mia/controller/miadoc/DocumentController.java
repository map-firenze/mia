package org.medici.mia.controller.miadoc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.medici.mia.common.json.AddNewPeopleResponseJson;
import org.medici.mia.common.json.AddNewPlaceResponseJson;
import org.medici.mia.common.json.CreatePlaceJson;
import org.medici.mia.common.json.FieldJson;
import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.LanguageJson;
import org.medici.mia.common.json.PeopleBaseJson;
import org.medici.mia.common.json.PeopleJson;
import org.medici.mia.common.json.PeopleRelatedToDocumentJson;
import org.medici.mia.common.json.PlaceJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.TopicJson;
import org.medici.mia.common.json.document.CorrespondenceJson;
import org.medici.mia.common.json.document.CreateDEResponseJson;
import org.medici.mia.common.json.document.DocumentFactoryJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.document.DocumentMetaDataJson;
import org.medici.mia.common.json.document.DocumentTranscriptionJson;
import org.medici.mia.common.json.document.FinancialJson;
import org.medici.mia.common.json.document.FiscalJson;
import org.medici.mia.common.json.document.GenericDocumentJson;
import org.medici.mia.common.json.document.InventoryJson;
import org.medici.mia.common.json.document.LiteraryJson;
import org.medici.mia.common.json.document.MiscellaneousJson;
import org.medici.mia.common.json.document.ModifyImageJson;
import org.medici.mia.common.json.document.NewsJson;
import org.medici.mia.common.json.document.NotarialJson;
import org.medici.mia.common.json.document.OfficialJson;
import org.medici.mia.common.json.document.TopicPlaceJson;
import org.medici.mia.common.json.folio.FolioJson;
import org.medici.mia.common.json.folio.FolioResponseJson;
import org.medici.mia.common.util.FileUtils;
import org.medici.mia.common.util.FolioUtils;
import org.medici.mia.domain.CaseStudyItem;
import org.medici.mia.domain.Collation;
import org.medici.mia.domain.DocumentFieldEntity;
import org.medici.mia.domain.DocumentTypologyEntity;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.domain.InsertEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.PlaceType;
import org.medici.mia.domain.RepositoryEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.UploadInfoEntity;
import org.medici.mia.domain.User;
import org.medici.mia.domain.Volume;
import org.medici.mia.service.casestudy.CaseStudyService;
import org.medici.mia.service.collation.CollationService;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.miadoc.DocumentType;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.myarchive.MyArchiveService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.underscore.U;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Controller
@RequestMapping("/document")
public class DocumentController {

	@Autowired
	private MiaDocumentService miaDocumentService;

	@Autowired
	private UserService userService;

	@Autowired
	private MyArchiveService archiveService;

	@Autowired
	private CollationService collationService;

	@Autowired
	private HistoryLogService historyLogService;


	@Autowired
	private CaseStudyService caseStudyService;

	// localhost:8080/Mia/json/document/findDocument/1
	@RequestMapping(value = "findDocument/{documentId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, DocumentJson>> findDocument(
			@PathVariable("documentId") String documentId) {

		GenericResponseDataJson<HashMap<String, DocumentJson>> resp = new GenericResponseDataJson<HashMap<String, DocumentJson>>();
		HashMap<String, DocumentJson> docHash = new HashMap<String, DocumentJson>();
		DocumentJson doc = getMiaDocumentService().findDocumentById(
				Integer.valueOf(documentId));
		if (doc.getCategory() != null) {
			docHash.put("documentEntity", doc);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + doc.getClass().getSimpleName()
					+ " Document Type");
			resp.setData(docHash);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("This document doesn't exist in the database.");
			return resp;
		}

	}

	// localhost:8080/Mia/json/document/uploadDocumentPrivacy/1
	@RequestMapping(value = "uploadDocumentPrivacy/{documentId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson uploadDocumentPrivacy(
			@PathVariable("documentId") String documentId,
			HttpServletRequest request) {
		GenericResponseDataJson resp = new GenericResponseDataJson();
		try {
			MiaDocumentEntity doc = miaDocumentService.findDocumentEntity(Integer.valueOf(documentId));
			User user = getUserService().findUser(
					((UserDetails) SecurityContextHolder.getContext()
							.getAuthentication().getPrincipal()).getUsername());
			if(doc == null){
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("Document with id = "+documentId+" does not exist");
				return resp;
			}
			if(!(doc.getCreatedBy() != null && doc.getCreatedBy().equals(user.getAccount())
					|| userService.isAccountAdministrator(user.getAccount()))){
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("You do not have an access to change privacy for this document");
				return resp;
			}
			List<Collation> collations = collationService.findByDocumentId(Integer.valueOf(documentId));
			MiaDocumentEntity document = miaDocumentService.findDocumentEntity(Integer.valueOf(documentId));
			if(document != null
					&& document.getPrivacy() != null && document.getPrivacy() == 0
					&& collations != null && !collations.isEmpty()){
				resp.setStatus(StatusType.w.toString());
				resp.setMessage("Cannot make this document private. Document has active collations");
				return resp;
			}
			getMiaDocumentService().updateDocumentPrivacyById(
					Integer.valueOf(documentId), null);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Update privacy complete.");
		} catch (NumberFormatException ex) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("Error while update document");
		}
		return resp;
	}

	// localhost:8080/Mia/json/document/findDocumentFields/correspondence
	@RequestMapping(value = "findDocumentFields/{category}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<FieldJson>>> findDocumentFields(
			@PathVariable("category") String category, HttpSession session) {
		GenericResponseDataJson<HashMap<String, List<FieldJson>>> resp = new GenericResponseDataJson<HashMap<String, List<FieldJson>>>();
		HashMap<String, List<FieldJson>> docHash = new HashMap<String, List<FieldJson>>();
		List<DocumentFieldEntity> documentFields;
		if (session.getAttribute("documentFields") != null) {
			documentFields = (List<DocumentFieldEntity>) session
					.getAttribute("documentFields");
		} else {
			documentFields = getMiaDocumentService().findDocumentFields();
			session.setAttribute("documentFields", documentFields);
		}

		DocumentJson document = DocumentFactoryJson.getDocument(category);
		List<FieldJson> fields = document.getJsonFields(documentFields);

		if (fields != null && !fields.isEmpty()) {
			docHash.put("fields", fields);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got Document fields for category: " + category);
			resp.setData(docHash);
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No fields found for the category: " + category);
		}

		return resp;

	}

	// localhost:8080/Mia/json/document/findDocumentsByUploadFile/2
	@RequestMapping(value = "findDocumentsByUploadFile/{uploadFileId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<DocumentJson>>> findDocumentsByUploadFile(
			@PathVariable("uploadFileId") Integer uploadFileId) {

		GenericResponseDataJson<HashMap<String, List<DocumentJson>>> resp = new GenericResponseDataJson<HashMap<String, List<DocumentJson>>>();
		HashMap<String, List<DocumentJson>> docHash = new HashMap<String, List<DocumentJson>>();
		List<DocumentJson> list = getMiaDocumentService()
				.findDocumentsByUploadFileId(uploadFileId);
		if (list != null && !list.isEmpty()) {
			docHash.put("documentEntities", list);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + list.size()
					+ " Documents for UploadFile with ID:" + uploadFileId);
			resp.setData(docHash);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Documents not exists in DB for this UploadFile");
			return resp;
		}

	}

	// localhost:8080/Mia/json/document/findDocumentsByUser/{username}
	@RequestMapping(value = "findDocumentsByUser/{userName}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<DocumentJson>>> findDocumentsByUser(
			@PathVariable("userName") String userName) {

		GenericResponseDataJson<HashMap<String, List<DocumentJson>>> resp = new GenericResponseDataJson<HashMap<String, List<DocumentJson>>>();
		HashMap<String, List<DocumentJson>> docHash = new HashMap<String, List<DocumentJson>>();
		List<DocumentJson> list = getMiaDocumentService().findDocumentsByUser(
				userName);

		if (list != null && !list.isEmpty()) {
			docHash.put("documentEntities", list);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + list.size() + " Documents for the user:"
					+ userName);
			resp.setData(docHash);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Documents not exists in DB for this user");
			return resp;
		}
	}

	// localhost:8080/Mia/json/document/findDocuments
	@RequestMapping(value = "findDocuments", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<DocumentJson>>> findDocuments() {

		GenericResponseDataJson<HashMap<String, List<DocumentJson>>> resp = new GenericResponseDataJson<HashMap<String, List<DocumentJson>>>();
		HashMap<String, List<DocumentJson>> docHash = new HashMap<String, List<DocumentJson>>();
		List<DocumentJson> list = getMiaDocumentService().findDocuments(false);

		if (list != null && !list.isEmpty()) {
			docHash.put("documentEntities", list);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + list.size() + " Documents in total");
			resp.setData(docHash);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No Documents exists in DB ");
			return resp;
		}

	}

	// localhost:8080/Mia/json/document/findDocuments
	@RequestMapping(value = "findUserDocuments", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, Object> findUserDocuments(
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "perPage", required = false) Integer perPage) {

//		GenericResponseDataJson<HashMap<String, List<DocumentJson>>> resp = new GenericResponseDataJson<HashMap<String, List<DocumentJson>>>();
		HashMap<String, Object> resp = new HashMap<String, Object>();
		HashMap<String, List<DocumentJson>> docHash = new HashMap<String, List<DocumentJson>>();
		List<DocumentJson> list;
		if (page != null && perPage != null) {
			list = getMiaDocumentService().findDocuments(true, page, perPage);
		} else {
			list = getMiaDocumentService().findDocuments(true);
		}
		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()).getUsername());
		Long count = miaDocumentService.countUserOwnDocuments(user);

		if (list != null && !list.isEmpty()) {
			docHash.put("documentEntities", list);
			resp.put("status", StatusType.ok.toString());
			resp.put("message", "Got " + list.size() + " Documents in total");
			resp.put("data", docHash);
			resp.put("count", count);
			return resp;
		} else {
			resp.put("status", StatusType.ko.toString());
			resp.put("message", "No Documents exists in DB");
			resp.put("count", count);
			resp.put("data", null);
			return resp;
		}

	}

	// localhost:8080/Mia/json/document/findNotDeletedDocuments
	@RequestMapping(value = "findNotDeletedDocuments", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<DocumentJson>>> findNotDeletedDocuments() {

		GenericResponseDataJson<HashMap<String, List<DocumentJson>>> resp = new GenericResponseDataJson<HashMap<String, List<DocumentJson>>>();
		HashMap<String, List<DocumentJson>> docHash = new HashMap<String, List<DocumentJson>>();
		List<DocumentJson> list = getMiaDocumentService()
				.findNotDeletedDocuments();

		if (list != null && !list.isEmpty()) {
			docHash.put("documentEntities", list);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + list.size() + " Documents in total");
			resp.setData(docHash);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No Documents exists in DB ");
			return resp;
		}

	}

	// localhost:8080/Mia/json/document/createDE/{}
	@RequestMapping(value = "createDE", method = RequestMethod.POST)
	public @ResponseBody CreateDEResponseJson createDE(
			@RequestBody GenericDocumentJson document,
			HttpServletRequest request) {

		CreateDEResponseJson ret = new CreateDEResponseJson();

		if (document == null || document.getCategory() == null
				|| document.getCategory().isEmpty()) {
			ret.setStatus(StatusType.ko.toString());
			ret.setMessage("category field has no value.");
			return ret;
		}

		return getMiaDocumentService().createDE(getDocument(document));
	}

	// localhost:8080/Mia/json/document/modifyDE/{}
	@RequestMapping(value = "modifyDE", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyDE(
			@RequestBody GenericDocumentJson document,
			HttpServletRequest request) {

		CreateDEResponseJson ret = new CreateDEResponseJson();

		if (document == null || document.getCategory() == null
				|| document.getCategory().isEmpty()) {
			ret.setStatus(StatusType.ko.toString());
			ret.setMessage("category field has no value.");
			return ret;
		}


		return getMiaDocumentService().modifyDE(getDocument(document));

	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

	public void setMiaDocumentService(MiaDocumentService miaDocumentService) {
		this.miaDocumentService = miaDocumentService;
	}

	// localhost:8080/Mia/json/document/findDocumentTypologies
	@RequestMapping(value = "findDocumentTypologies", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<DocumentTypologyEntity>>> findDocumentTypologies() {
		GenericResponseDataJson<HashMap<String, List<DocumentTypologyEntity>>> resp = new GenericResponseDataJson<HashMap<String, List<DocumentTypologyEntity>>>();
		HashMap<String, List<DocumentTypologyEntity>> respHash = new HashMap<String, List<DocumentTypologyEntity>>();
		List<DocumentTypologyEntity> list = getMiaDocumentService()
				.findDocumentTypologies();
		if (list != null && !list.isEmpty()) {
			respHash.put("categories", list);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got document typologies");
			resp.setData(respHash);
			return resp;
		} else {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Document typologies not found");
			return resp;
		}
	}

	// localhost:8080/Mia/json/document/findDocumentTypologiesByCategory/{category}
	@RequestMapping(value = "findDocumentTypologiesByCategory/{category}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<String>>> findDocumentTypologiesByCategory(
			@PathVariable("category") String category) {
		GenericResponseDataJson<HashMap<String, List<String>>> resp = new GenericResponseDataJson<HashMap<String, List<String>>>();
		HashMap<String, List<String>> respHash = new HashMap<String, List<String>>();
		List<String> list = getMiaDocumentService()
				.findDocumentTypologiesByCategory(category);
		if (list != null && !list.isEmpty()) {
			respHash.put("typologies", list);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got typologies for document category: " + category);
			resp.setData(respHash);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No typologies found for document category: "
					+ category);
			return resp;
		}
	}

	// localhost:8080/Mia/json/document/findDocumentIdsByPeople/
	@RequestMapping(value = "findDocumentIdsByPeople", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<List<Integer>> findDocumentIdsByPeople(
			@RequestBody List<String> people) {
		GenericResponseDataJson<List<Integer>> resp = new GenericResponseDataJson<List<Integer>>();
		List<Integer> docIds = getMiaDocumentService().findDocumentIdsByPeople(
				people);
		if (docIds != null && !docIds.isEmpty()) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got docs with ids: " + docIds.size());
			resp.setData(docIds);
			return resp;
		} else {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("No document found for people: ");
			return resp;
		}

	}

	// localhost:8080/Mia/json/document/findPeople/{personName}
	@RequestMapping(value = "findPeople/{personName}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<PeopleJson>>> findPeople(
			@PathVariable("personName") String personName) {
		GenericResponseDataJson<HashMap<String, List<PeopleJson>>> resp = new GenericResponseDataJson<HashMap<String, List<PeopleJson>>>();
		HashMap<String, List<PeopleJson>> respHash = new HashMap<String, List<PeopleJson>>();
		if (personName == null || personName.equalsIgnoreCase("")) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("person name is empty or null: " + personName);
			return resp;
		}
		List<PeopleJson> list = getMiaDocumentService().findPeople(personName);
		if (list != null && !list.isEmpty()) {
			respHash.put("people", list);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + list.size() + " people with name: "
					+ personName);
			resp.setData(respHash);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No people found with name: " + personName);
			return resp;
		}
	}

	// localhost:8080/Mia/json/document/addNewPeople/{}
	@RequestMapping(value = "addNewPeople", method = RequestMethod.POST)
	public @ResponseBody AddNewPeopleResponseJson addNewPeople(
			@RequestBody PeopleJson peopleToAdd) {
		AddNewPeopleResponseJson resp = new AddNewPeopleResponseJson();
		if ((peopleToAdd.getFirstName() != null || peopleToAdd.getLastName() != null)
				&& peopleToAdd.getGender() != null) {

			Integer peopleId = getMiaDocumentService()
					.insertPeople(peopleToAdd);

			if (peopleId != null) {
                historyLogService.registerAction(peopleId, HistoryLogEntity.UserAction.CREATE,
						HistoryLogEntity.RecordType.BIO, null, null);
				resp.setPeopleId(peopleId);
				resp.setPath(FileUtils.SHOW_PERSON_URL + peopleId);
				resp.setStatus(StatusType.ok.toString());
				resp.setMessage("Added new people");
			} else {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("Add new people NOT persisted in DB");
			}

		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Data not valid to add new people");
		}
		return resp;
	}

	// localhost:8080/Mia/json/document/findPeopleById/{peopleId}
	@RequestMapping(value = "findPeopleById/{peopleId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<PeopleJson> findPeopleById(
			@PathVariable("peopleId") Integer peopleId) {
		GenericResponseDataJson<PeopleJson> resp = new GenericResponseDataJson<PeopleJson>();
		PeopleJson ppl = getMiaDocumentService().findPeopleById(peopleId);
		if (ppl.getId() != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got people with id: " + peopleId);
			resp.setData(ppl);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No people found with id: " + peopleId);
			return resp;
		}

	}

	// localhost:8080/Mia/json/document/findPlaces/{placeName}
	@RequestMapping(value = "findPlaces/{placeName}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<PlaceJson>>> findPlaces(
			@PathVariable("placeName") String placeName) {
		GenericResponseDataJson<HashMap<String, List<PlaceJson>>> resp = new GenericResponseDataJson<HashMap<String, List<PlaceJson>>>();
		HashMap<String, List<PlaceJson>> respHash = new HashMap<String, List<PlaceJson>>();
		List<PlaceJson> list = getMiaDocumentService().findPlaces(placeName);
		if (list != null && !list.isEmpty()) {
			respHash.put("places", list);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got places with name: " + placeName);
			resp.setData(respHash);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No place found with name: " + placeName);
			return resp;
		}
	}

	// localhost:8080/Mia/json/document/findPlaceById/{placeId}
	@RequestMapping(value = "findPlaceById/{placeId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<PlaceJson> findPlaceById(
			@PathVariable("placeId") Integer placeId) {
		GenericResponseDataJson<PlaceJson> resp = new GenericResponseDataJson<PlaceJson>();
		PlaceJson pla = getMiaDocumentService().findPlaceById(placeId);
		if (pla.getId() != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got place with id: " + placeId);
			resp.setData(pla);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No place found with id: " + placeId);
			return resp;
		}

	}

	// localhost:8080/Mia/json/document/findPrinicipalPlaceByPlaceId/36523
	@RequestMapping(value = "findPrinicipalPlaceByPlaceId/{placeId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<PlaceJson> findPrinicipalPlaceByPlaceId(
			@PathVariable("placeId") Integer placeId) {
		GenericResponseDataJson<PlaceJson> resp = new GenericResponseDataJson<PlaceJson>();
		PlaceJson pla = getMiaDocumentService().findPrinicipalPlaceByPlaceId(
				placeId);
		if (pla != null && pla.getId() != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got place with id: " + placeId);
			resp.setData(pla);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No principal place found with id: " + placeId);
			return resp;
		}

	}

	// localhost:8080/Mia/json/document/findPrinicipalPlaceIdByVariantPlaceId/36523
	@RequestMapping(value = "findPrinicipalPlaceIdByVariantPlaceId/{placeId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<Integer> findPrinicipalPlaceIdByVariantPlaceId(
			@PathVariable("placeId") Integer placeId) {
		GenericResponseDataJson<Integer> resp = new GenericResponseDataJson<Integer>();
		Integer pId = getMiaDocumentService()
				.findPrinicipalPlaceIdByVariantPlaceId(placeId);
		if (pId != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got principla place for id: " + placeId);
			resp.setData(pId);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No principal place found with id: " + placeId);
			return resp;
		}

	}

	// localhost:8080/Mia/json/document/findTopics
	@RequestMapping(value = "findTopics", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<TopicJson>>> findTopics() {
		GenericResponseDataJson<HashMap<String, List<TopicJson>>> resp = new GenericResponseDataJson<HashMap<String, List<TopicJson>>>();
		HashMap<String, List<TopicJson>> respHash = new HashMap<String, List<TopicJson>>();
		List<TopicJson> list = (List<TopicJson>) getMiaDocumentService()
				.findTopics();
		if (list != null && !list.isEmpty()) {
			respHash.put("topics", list);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got topics");
			resp.setData(respHash);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No topics found");
			return resp;
		}
	}

	// localhost:8080/Mia/json/document/findTopicById/{topicId}
	@RequestMapping(value = "findTopicById/{topicId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<TopicJson> findTopicById(
			@PathVariable("topicId") Integer topicId) {
		GenericResponseDataJson<TopicJson> resp = new GenericResponseDataJson<TopicJson>();
		TopicJson top = getMiaDocumentService().findTopicById(topicId);
		if (top.getTopicId() != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got topic with id: " + topicId);
			resp.setData(top);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No topic found with id: " + topicId);
			return resp;
		}

	}

	// localhost:8080/Mia/json/document/checkFolio/{uploadFileId}
	@RequestMapping(value = "checkFolio/{uploadFileId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<FolioJson>> checkFolio(
			@PathVariable("uploadFileId") Integer uploadFileId) {
		GenericResponseDataJson<List<FolioJson>> resp = new GenericResponseDataJson<List<FolioJson>>();

		List<FolioJson> list = getMiaDocumentService().checkFolio(uploadFileId);
		if (list == null || list.isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Folios not exist for this uploadFileId");
			return resp;
		} else {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Folios for this uploadFileId");
			resp.setData(list);
			return resp;
		}

	}

	// localhost:8080/Mia/json/document/saveFolio/
	@RequestMapping(value = "saveFolio", method = RequestMethod.POST)
	public @ResponseBody GenericResponseDataJson<List<FolioResponseJson>> saveFolio(
			@RequestBody List<FolioJson> folios) {
		GenericResponseDataJson<List<FolioResponseJson>> resp = new GenericResponseDataJson<List<FolioResponseJson>>();

		GenericResponseJson genericResp = new GenericResponseJson();
		genericResp.setMessage("");
		genericResp.setStatus(StatusType.ok.toString());
		if (!FolioUtils.foliosIfValid(folios)) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage(genericResp.getMessage() + " Folios wrong format. Folios not saved.");
			return resp;
		}
		List<FolioJson> foliosToSave = FolioUtils.prepareFoliosToBeAdded(folios, genericResp);
		if (foliosToSave != null && !foliosToSave.isEmpty()) {
			List<FolioResponseJson> ret = getMiaDocumentService().saveFolio(
					foliosToSave);
			if (ret == null || ret.isEmpty()) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage(genericResp.getMessage() + " Folios not saved.");
				return resp;
			} else {
				resp.setStatus(genericResp.getStatus());
				resp.setMessage(genericResp.getMessage() + " Folios saved.");
				resp.setData(ret);
				return resp;
			}
		} else {
			resp.setStatus(genericResp.getStatus());
			resp.setMessage(genericResp.getMessage());
			return resp;
		}
	}

	// localhost:8080/Mia/json/document/deleteFolio/
	@RequestMapping(value = "deleteFolio", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson deleteFolio(
			@RequestBody FolioJson folioToDelete) {
		GenericResponseJson resp = new GenericResponseJson();

		if (folioToDelete == null || folioToDelete.getFolioId() == null
				|| folioToDelete.getUploadFileId() == null) {

			resp.setStatus(StatusType.w.toString());
			resp.setMessage("Json hqs not correct fields: folioId and uploadFileId should be present.");
			return resp;
		}

		Integer ret = getMiaDocumentService().deleteFolio(folioToDelete);
		if (ret == 0) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("Folios is not present in DB and not deleted:");
			return resp;
		} else {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Folio deleted:");
			return resp;
		}

	}

	// localhost:8080/Mia/json/document/addNewPlace/
	@RequestMapping(value = "addNewPlace", method = RequestMethod.POST)
	public @ResponseBody AddNewPlaceResponseJson addNewPlace(
			@RequestBody CreatePlaceJson placeToAdd) {
		AddNewPlaceResponseJson resp = new AddNewPlaceResponseJson();
		if (placeToAdd == null) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("placeToAdd is null.");
			return resp;
		}

		if (placeToAdd.getPlaceName() == null
				|| placeToAdd.getPlaceName().isEmpty()) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("place name may be null or empty");
			return resp;
		}

		if (placeToAdd.getPlType() == null || placeToAdd.getPlType().isEmpty()) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("place type may be null or empty");
			return resp;
		}

		if (placeToAdd.getPlaceParentId() == null
				|| placeToAdd.getPlaceParentId().equals("")) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("place parent Id may be null or empty");
			return resp;
		}

		Integer placeId = getMiaDocumentService().insertPlace(placeToAdd);

		if (placeId != null) {
            historyLogService.registerAction(placeId, HistoryLogEntity.UserAction.CREATE,
					HistoryLogEntity.RecordType.GEO, null, null);
			resp.setPlaceId(placeId);
			resp.setPath(FileUtils.SHOW_PLACE_URL + placeId);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Added new place");
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("Add new place NOT persisted in DB");
		}

		return resp;
	}

	// localhost:8080/Mia/json/document/findPlaceTypes
	@RequestMapping(value = "findPlaceTypes", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<PlaceType>> findPlaceTypes() {
		GenericResponseDataJson<List<PlaceType>> resp = new GenericResponseDataJson<List<PlaceType>>();
		List<PlaceType> list = getMiaDocumentService().findPlaceTypes();
		if (list != null && !list.isEmpty()) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got PlaceTypes");
			resp.setData(list);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No PlaceTypes found");
			return resp;
		}
	}

	@RequestMapping(value = "saveOrModifyTranscriptions", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson saveOrModifyTranscriptions(
			@RequestBody List<DocumentTranscriptionJson> documentTranscriptionJson,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();

		if (documentTranscriptionJson == null
				|| documentTranscriptionJson.isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("transcription list maybe null or empty.");
			return resp;

		}

		resp = getMiaDocumentService().saveOrModifyTranscriptions(
				documentTranscriptionJson);

		HashMap<String, Object> changes = new HashMap<String, Object>();
		HashMap<String, HashMap> category = new HashMap<String, HashMap>();
		HashMap<String, HashMap> action = new HashMap<String, HashMap>();
		if(documentTranscriptionJson.size() > 0 && resp.getStatus().equals(StatusType.ok.toString())){
			DocumentTranscriptionJson docTranscript = documentTranscriptionJson.get(0);
			changes.put("transcription", docTranscript.getTranscription());
			changes.put("id", docTranscript.getDocTranscriptionId());
			action.put("add", changes);
			category.put("documentTranscription", action);

			historyLogService.registerAction(
					Integer.valueOf(docTranscript.getDocumentId()), HistoryLogEntity.UserAction.EDIT,
					HistoryLogEntity.RecordType.DE, null, category);
		}

		// Update transSearch column
		getMiaDocumentService().updateTransSearch(
				Integer.valueOf(documentTranscriptionJson.get(0)
						.getDocumentId()));

		return resp;

	}

	// http://localhost:8080/Mia/json/document/saveSynopsis/
	@RequestMapping(value = "saveSynopsis/{synopsis}/{documentId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseJson saveSynopsis(
			@PathVariable("synopsis") String synopsis,
			@PathVariable("documentId") String documentId,
			HttpServletRequest request) {
		GenericResponseJson resp = new GenericResponseJson();

		if (documentId == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("doucmentId is null");
		} else {
			try {
				Integer docentid = Integer.parseInt(documentId);
				resp = getMiaDocumentService().saveSynopsis(synopsis, docentid);
			} catch (NumberFormatException ex) {
				resp.setStatus(StatusType.ko.toString());
				resp.setMessage("The given docementId is not a number");
			}
		}
		return resp;
	}

	// localhost:8080/Mia/json/document/findLanguages/
	@RequestMapping(value = "findLanguages", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<LanguageJson>>> findlanguages() {

		List<LanguageJson> languages = getMiaDocumentService().findLanguages();

		GenericResponseDataJson<HashMap<String, List<LanguageJson>>> resp = new GenericResponseDataJson<HashMap<String, List<LanguageJson>>>();
		HashMap<String, List<LanguageJson>> docHash = new HashMap<String, List<LanguageJson>>();

		if (languages != null && !languages.isEmpty()) {
			docHash.put("languages", languages);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + languages.size() + "languages in total.");
			resp.setData(docHash);
			return resp;
		} else {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No " + "languages exists in DB. ");
			return resp;
		}

	}

	// localhost:8080/Mia/json/document/deleteDE/35
	@RequestMapping(value = "deleteDE/{documentId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseJson deleteDE(
			@PathVariable("documentId") String documentId,
			HttpServletRequest request) {
		GenericResponseJson resp = new GenericResponseJson();
		if (documentId == null) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("Document not deleted. Field documentId received by FE is null.");
			return resp;
		}

		if(collationService.findByDocumentId(Integer.valueOf(documentId)).size() > 0){
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("The document can't be deleted since it is used for document collations.");
			return resp;
		}

		caseStudyService.deleteAttachedItems(Integer.valueOf(documentId), CaseStudyItem.EntityType.DE);

		resp = getMiaDocumentService().deleteDE(Integer.valueOf(documentId));

		historyLogService.registerAction(
				Integer.valueOf(documentId), HistoryLogEntity.UserAction.DELETE,
				HistoryLogEntity.RecordType.DE, null, null);

		return resp;

	}

	@RequestMapping(value = "modifyImages", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyImages(
			@RequestBody ModifyImageJson json,
			HttpServletRequest request) {



		GenericResponseJson resp = new GenericResponseJson();

		if (json == null || json.getModifyImagesInDe() == null
				|| json.getModifyImagesInDe().isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("modify image  list maybe null or empty.");
			return resp;

		}

		return getMiaDocumentService().modifyImages(json);
		

	}

	// localhost:8080/Mia/json/document/findPeopleRelatedtoDocument/125
	@RequestMapping(value = "findPeopleRelatedtoDocument/{documentId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<PeopleBaseJson>>> findPeopleRelatedtoDocument(
			@PathVariable("documentId") String documentId) {

		GenericResponseDataJson<HashMap<String, List<PeopleBaseJson>>> resp = new GenericResponseDataJson<HashMap<String, List<PeopleBaseJson>>>();
		HashMap<String, List<PeopleBaseJson>> docHash = new HashMap<String, List<PeopleBaseJson>>();
		List<PeopleBaseJson> people = getMiaDocumentService()
				.findPeopleRelatedtoDocument(Integer.valueOf(documentId));
		if (people != null) {
			docHash.put("people", people);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + people.size()
					+ " people relate to doculemt with docId: " + documentId);
			resp.setData(docHash);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No people related to the document with Id:"
					+ documentId + "found in DB");
		}

		return resp;

	}

	// localhost:8080/Mia/json/document/addPeopleRelatedToDocument/{}
	@RequestMapping(value = "addPeopleRelatedToDocument", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson addPeopleRelatedToDocument(
			@RequestBody PeopleRelatedToDocumentJson json,
			HttpServletRequest request) {

		GenericResponseJson ret = new GenericResponseJson();

		if (json == null || json.getNewPersonCitedInDoc() == null
				|| json.getDocumentId() == null) {
			ret.setStatus(StatusType.ko.toString());
			ret.setMessage("json fields has no correct value.");
			return ret;
		}

		return getMiaDocumentService().addPeopleRelatedtoDocument(json);

	}

	// localhost:8080/Mia/json/document/modifyPeopleRelatedToDocument/{}
	@RequestMapping(value = "modifyPeopleRelatedToDocument", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyPeopleRelatedToDocument(
			@RequestBody PeopleRelatedToDocumentJson json,
			HttpServletRequest request) {

		GenericResponseJson ret = new GenericResponseJson();

		if (json == null || json.getNewPersonCitedInDoc() == null
				|| json.getDocumentId() == null
				|| json.getOldPersonCitedInDoc() == null) {
			ret.setStatus(StatusType.ko.toString());
			ret.setMessage("json fields has no correct value.");
			return ret;
		}

		return getMiaDocumentService().modifyPeopleRelatedtoDocument(json);

	}

	// localhost:8080/Mia/json/document/deletePeopleRelatedToDocument/{}
	@RequestMapping(value = "deletePeopleRelatedToDocument", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson deletePeopleRelatedToDocument(
			@RequestBody PeopleRelatedToDocumentJson json,
			HttpServletRequest request) {

		GenericResponseJson ret = new GenericResponseJson();

		if (json == null || json.getDeletePersonCitedInDoc() == null
				|| json.getDocumentId() == null) {
			ret.setStatus(StatusType.ko.toString());
			ret.setMessage("json fields has no correct value.");
			return ret;
		}

		return getMiaDocumentService().deletePeopleRelatedtoDocument(json);

	}
	
	// localhost:8080/Mia/json/document/createNoImageDocument
	@RequestMapping(
			value = "createNoImageDocument", 
			method = RequestMethod.POST)
	public @ResponseBody CreateDEResponseJson createNoImageDocument(
			@RequestBody DocumentMetaDataJson documentMetaDataJson) {
		
		Integer documentId = getMiaDocumentService().createNoImageDocument(
				documentMetaDataJson);
		
		CreateDEResponseJson responseJson = new CreateDEResponseJson();
		responseJson.setStatus(StatusType.ko.toString());
		responseJson.setMessage("Unable to construct MiaDocumentEntity");
		
		if (documentId != null) {
			responseJson.setStatus(StatusType.ok.toString());
			responseJson.setMessage(String.format("Document %s persisted in DB", documentId));
			responseJson.setDocId(documentId);
		}
		
		return responseJson;
	}

	private List<PlaceJson> getPlacePrincipal(List<PlaceJson> places) {

		if (places == null || places.isEmpty()) {
			return places;
		}
		List<PlaceJson> prinPlaces = getMiaDocumentService()
				.getPrinciplaPlaces(places);

		return prinPlaces;
	}

	private GenericDocumentJson getDocument(GenericDocumentJson document) {

		if (document == null)
			return null;
		if (DocumentType.correspondence.toString().equalsIgnoreCase(
				document.getCategory())) {

			// Getting principal places for topic place
			List<TopicPlaceJson> topics = getMiaDocumentService()
					.getPrinciplaTopicPlaces(document.getTopics());
			document.setTopics(topics);

			((CorrespondenceJson) document)
					.setRecipientPlaces(getPlacePrincipal(((CorrespondenceJson) document)
							.getRecipientPlaces()));
			((CorrespondenceJson) document)
					.setPlaceOfOrigin(getPlacePrincipal(((CorrespondenceJson) document)
							.getPlaceOfOrigin()));
			return (CorrespondenceJson) document;

		} else if (DocumentType.notarialRecords.toString().equalsIgnoreCase(
				document.getCategory())) {

			((NotarialJson) document)
					.setPlaceOfOrigin(getPlacePrincipal(((NotarialJson) document)
							.getPlaceOfOrigin()));
			return (NotarialJson) document;

		} else if (DocumentType.officialRecords.toString().equalsIgnoreCase(
				document.getCategory())) {

			((OfficialJson) document)
					.setPrinterPlaces(getPlacePrincipal(((OfficialJson) document)
							.getPrinterPlaces()));
			((OfficialJson) document)
					.setPlaceOfOrigin(getPlacePrincipal(((OfficialJson) document)
							.getPlaceOfOrigin()));
			return (OfficialJson) document;

		} else if (DocumentType.inventories.toString().equalsIgnoreCase(
				document.getCategory())) {

			((InventoryJson) document)
					.setPlaceOfOrigin(getPlacePrincipal(((InventoryJson) document)
							.getPlaceOfOrigin()));
			return (InventoryJson) document;

		} else if (DocumentType.fiscalRecords.toString().equalsIgnoreCase(
				document.getCategory())) {

			((FiscalJson) document)
					.setGonfaloneDistricts(getPlacePrincipal(((FiscalJson) document)
							.getGonfaloneDistricts()));
			((FiscalJson) document)
					.setPlaceOfOrigin(getPlacePrincipal(((FiscalJson) document)
							.getPlaceOfOrigin()));
			return (FiscalJson) document;

		} else if (DocumentType.financialRecords.toString().equalsIgnoreCase(
				document.getCategory())) {

			((FinancialJson) document)
					.setPlaceOfOrigin(getPlacePrincipal(((FinancialJson) document)
							.getPlaceOfOrigin()));
			return (FinancialJson) document;

		} else if (DocumentType.miscellaneous.toString().equalsIgnoreCase(
				document.getCategory())) {

			((MiscellaneousJson) document)
					.setPrinterPlaces(getPlacePrincipal(((MiscellaneousJson) document)
							.getPrinterPlaces()));
			((MiscellaneousJson) document)
					.setPlaceOfOrigin(getPlacePrincipal(((MiscellaneousJson) document)
							.getPlaceOfOrigin()));
			return (MiscellaneousJson) document;

		} else if (DocumentType.literaryWorks.toString().equalsIgnoreCase(
				document.getCategory())) {

			((LiteraryJson) document)
					.setPrinterPlaces(getPlacePrincipal(((LiteraryJson) document)
							.getPrinterPlaces()));
			((LiteraryJson) document)
					.setPlaceOfOrigin(getPlacePrincipal(((LiteraryJson) document)
							.getPlaceOfOrigin()));
			return (LiteraryJson) document;

		} else if (DocumentType.news.toString().equalsIgnoreCase(
				document.getCategory())) {

			((NewsJson) document)
					.setNewsFroms(getPlacePrincipal(((NewsJson) document)
							.getNewsFroms()));
			((NewsJson) document)
					.setPlaceOfOrigin(getPlacePrincipal(((NewsJson) document)
							.getPlaceOfOrigin()));
			return (NewsJson) document;
		}

		return null;

	}

	@RequestMapping(value = "unDeleteDE/{documentId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseJson unDeleteDE(
			@PathVariable("documentId") Integer documentId, HttpSession session,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();
		if (documentId == null) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("document is null.");
			return resp;
		}

		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()).getUsername());

		if (!getUserService().isAccountAutorizedForDelete(user.getAccount())) {
			// User is not admin or on site fellow:
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("You are not Admin or Onsite_Fellows . you are not allowed to undelete DE");
			return resp;

		}

		historyLogService.registerAction(
				documentId, HistoryLogEntity.UserAction.UNDELETE,
				HistoryLogEntity.RecordType.DE, null, null);

		return getMiaDocumentService().unDeleteDE(documentId);

	}

	private List paginateResult(List res, Integer page, Integer perPage){
		List paginated = new ArrayList();
		int limit = page * perPage;
		int offset = limit - perPage;
		if(limit > 0 && offset >= 0 && offset < limit && offset < res.size()){
			if(limit < res.size()) {
				paginated = res.subList(offset, limit);
			}
			else if(limit >= res.size()){
				paginated = res.subList(offset, res.size());
			}
		}
		return paginated;
	}

	private List<HashMap> filterResultByName(List<HashMap> docs, String q) {
		List<HashMap> found = new ArrayList<HashMap>();
		for(HashMap doc : docs){
			if(doc.get("deTitle") != null){
				if(((String) doc.get("deTitle")).toLowerCase().contains(q.toLowerCase())){
					found.add(doc);
				}
			}
		}
		found = U.uniq(found);
		return found;
	}

	@RequestMapping(value = "findDocumentsInVolume/{volumeId}", method = RequestMethod.GET)
	public @ResponseBody HashMap findDocumentsInVolume(
			@PathVariable("volumeId") Integer volumeId,
			@RequestParam(value = "notChildren", required = false) Boolean notChildren,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "perPage", required = false) Integer perPage,
			@RequestParam(value = "q", required = false) String q){
		notChildren = notChildren == null ? false : notChildren;
		HashMap<String, Object> resp = new HashMap<String, Object>();
		Volume vol = archiveService.findVolumeById(volumeId);
		if(vol == null){
			resp.put("message", "Volume with ID = " + volumeId + " not found");
			resp.put("status", "error");
			return resp;
		}

		List<HashMap> jsons = miaDocumentService.findDocumentsInVolume(vol, notChildren);
		resp.put("totalCount", jsons.size());
		resp.put("message", "Got "+ jsons.size() + " records");
		resp.put("status", "success");
		if(q != null){
			jsons = filterResultByName(jsons, q);
		}
		if(page != null && perPage != null){
			jsons = paginateResult(jsons, page, perPage);
		}
		resp.put("data", jsons);

		return resp;
	}

	@RequestMapping(value = "findDocumentsInInsert/{insertId}", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, Object> findDocumentsInInsert(
			@PathVariable("insertId") Integer insertId,
			@RequestParam(value = "notChildren", required = false) Boolean notChildren,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "perPage", required = false) Integer perPage,
			@RequestParam(value = "q", required = false) String q){
		notChildren = notChildren == null ? false : notChildren;
		HashMap<String, Object> resp = new HashMap<String, Object>();
		InsertEntity ins = archiveService.findInsertById(insertId);
		if(ins == null){
			resp.put("message", "Insert with ID = " + insertId + " not found");
			resp.put("status", "error");
			return resp;
		}
		List<HashMap> jsons = miaDocumentService.findDocumentsInInsert(ins, notChildren);
		if(q != null){
			jsons = filterResultByName(jsons, q);
		}
		resp.put("totalCount", jsons.size());
		resp.put("message", "Got "+ jsons.size() + " records");
		resp.put("status", "success");
		if(page != null && perPage != null){
			jsons = paginateResult(jsons, page, perPage);
		}
		resp.put("data", jsons);
		return resp;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}