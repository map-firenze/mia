package org.medici.mia.controller.miadoc;

import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.document.FindPeopleForDocJson;
import org.medici.mia.common.json.document.ModifyPeopleForDocJson;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.miadoc.ModifyDocumentPeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Controller
@RequestMapping("/modifyDocumentPeople")
public class ModifyDocumentPeopleController {

	@Autowired
	private ModifyDocumentPeopleService modifyDocumentPeopleService;

	@Autowired
	private MiaDocumentService miaDocumentService;

	@Autowired
	HistoryLogService historyLogService;

	// localhost:8181/Mia/json/modifyDocumentPeople/findDocumentPeople/208/correspondence
	@RequestMapping(value = "findDocumentPeople/{documentId}/{category}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<FindPeopleForDocJson>>> findDocumentPeople(
			@PathVariable("documentId") Integer documentId,
			@PathVariable("category") String category) {

		List<FindPeopleForDocJson> jsons = getModifyDocumentPeopleService()
				.findDocumentPeoples(documentId, category);

		GenericResponseDataJson<HashMap<String, List<FindPeopleForDocJson>>> resp = new GenericResponseDataJson<HashMap<String, List<FindPeopleForDocJson>>>();
		HashMap<String, List<FindPeopleForDocJson>> docHash = new HashMap<String, List<FindPeopleForDocJson>>();
		if (jsons != null && !jsons.isEmpty()) {
			docHash.put("arrayPeople", jsons);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + jsons.size() + " peoples in total.");
			resp.setData(docHash);

		} else {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No peoples exists in DB. ");

		}
		return resp;

	}

	// localhost:8181/Mia/json/modifyDocumentPeople/modifyPeopleForDoc/
	@RequestMapping(value = "modifyPeopleForDoc", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyPeopleForDoc(
			@RequestBody ModifyPeopleForDocJson modifyPeople,
			HttpServletRequest request) {
		return getModifyDocumentPeopleService().modifyDocumentPeople(
				modifyPeople);

	}

	// localhost:8181/Mia/json/modifyDocumentPeople/deletePeopleForDoc/
	@RequestMapping(value = "deletePeopleForDoc", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson deletePeopleForDoc(
			@RequestBody ModifyPeopleForDocJson modifyPeople,
			HttpServletRequest request) {
		return getModifyDocumentPeopleService().modifyDocumentPeople(
				modifyPeople);

	}

	// localhost:8181/Mia/json/modifyDocumentPeople/addPeopleForDoc/
	@RequestMapping(value = "addPeopleForDoc", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson addPeopleForDoc(
			@RequestBody ModifyPeopleForDocJson modifyPeople,
			HttpServletRequest request) {
		return getModifyDocumentPeopleService().modifyDocumentPeople(
				modifyPeople);

	}

	public ModifyDocumentPeopleService getModifyDocumentPeopleService() {
		return modifyDocumentPeopleService;
	}

	public void setModifyDocumentPeopleService(
			ModifyDocumentPeopleService modifyDocumentPeopleService) {
		this.modifyDocumentPeopleService = modifyDocumentPeopleService;
	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

	public HistoryLogService getHistoryLogService() {
		return historyLogService;
	}
}