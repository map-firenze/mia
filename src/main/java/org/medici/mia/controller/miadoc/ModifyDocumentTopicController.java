package org.medici.mia.controller.miadoc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.PlaceJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.document.FindTopicPlaceForDocJson;
import org.medici.mia.common.json.document.ModifyTopicPlaceForDocJson;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.miadoc.ModifyDocumentTopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * @author User
 *
 */
@Controller
@RequestMapping("/modifyDocumentTopic")
public class ModifyDocumentTopicController {

	@Autowired
	private ModifyDocumentTopicService modifyDocumentTopicService;

	@Autowired
	private MiaDocumentService miaDocumentService;

	@Autowired
	private HistoryLogService historyLogService;

	// localhost:8181/Mia/json/modifyDocumentTopic/findTopicPlaceForDoc/125
	@RequestMapping(value = "findTopicPlaceForDoc/{documentId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, FindTopicPlaceForDocJson>> findTopicPlaceForDoc(
			@PathVariable("documentId") Integer documentId) {

		FindTopicPlaceForDocJson json = getModifyDocumentTopicService()
				.findDocumentTopicPlaces(documentId);

		GenericResponseDataJson<HashMap<String, FindTopicPlaceForDocJson>> resp = new GenericResponseDataJson<HashMap<String, FindTopicPlaceForDocJson>>();
		HashMap<String, FindTopicPlaceForDocJson> docHash = new HashMap<String, FindTopicPlaceForDocJson>();
		if (json != null && json.getTopicPlaces() != null) {
			docHash.put("topicPlace", json);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + json.getTopicPlaces().size()
					+ " places in total.");
			resp.setData(docHash);

		} else {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No Topic Places exists in DB. ");

		}
		return resp;

	}

	// localhost:8181/Mia/json/modifyDocumentToipc/modifyTopicPlaceForDoc/
	@RequestMapping(value = "modifyTopicPlaceForDoc", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyTopicPlaceForDoc(
			@RequestBody ModifyTopicPlaceForDocJson modifyTopicPlace,
			HttpServletRequest request) {

		if (modifyTopicPlace == null) {

			GenericResponseJson resp = new GenericResponseJson();
			resp.setMessage("The request is not complete. addTopicPlace or new TopicPlace may be null.");
			resp.setStatus(StatusType.w.toString());
			return resp;

		}

		return getModifyDocumentTopicService().modifyDocumentTopicPlaces(
				modifyTopicPlace);

	}

	@RequestMapping(value = "deleteTopicPlaceForDoc", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson deleteTopicPlaceForDoc(
			@RequestBody ModifyTopicPlaceForDocJson deleteTopicPlace,
			HttpServletRequest request) {

		if (deleteTopicPlace == null
				|| deleteTopicPlace.getTopicPlaceToBeDeleted() == null) {

			GenericResponseJson resp = new GenericResponseJson();
			resp.setMessage("The request is not complete. TopicPlaceToBeDeleted may be null.");
			resp.setStatus(StatusType.w.toString());
			return resp;

		}
		return getModifyDocumentTopicService().modifyDocumentTopicPlaces(
				deleteTopicPlace);

	}

	@RequestMapping(value = "addTopicPlaceForDoc", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson addTopicPlaceForDoc(
			@RequestBody ModifyTopicPlaceForDocJson addTopicPlace,
			HttpServletRequest request) {

		if (addTopicPlace == null || addTopicPlace.getNewTopicPlace() == null) {

			GenericResponseJson resp = new GenericResponseJson();
			resp.setMessage("The request is not complete. newTopicPlace may be null.");
			resp.setStatus(StatusType.w.toString());
			return resp;

		}

		addTopicPlace.setTopicPlaceToBeDeleted(null);
		addTopicPlace.setOldTopicPlace(null);

		// retrieve principal place
		PlaceJson placeJson = new PlaceJson();

		placeJson.setId(addTopicPlace.getNewTopicPlace().getPlaceId());
		placeJson.setUnsure(addTopicPlace.getNewTopicPlace().getUnsure());
		List<PlaceJson> places = new ArrayList<PlaceJson>();
		places.add(placeJson);
		List<PlaceJson> prinPlaces = getMiaDocumentService()
				.getPrinciplaPlaces(places);
		if (prinPlaces == null || prinPlaces.isEmpty()) {
			GenericResponseJson resp = new GenericResponseJson();
			resp.setMessage("No principal places found in DB. Document not modified");
			resp.setStatus(StatusType.w.toString());
			return resp;
		}
		addTopicPlace.getNewTopicPlace().setPlaceId(prinPlaces.get(0).getId());
		addTopicPlace.getNewTopicPlace().setUnsure(prinPlaces.get(0).getUnsure());
		addTopicPlace.getNewTopicPlace().setPrefFlag(prinPlaces.get(0).getPrefFlag());

		return getModifyDocumentTopicService().modifyDocumentTopicPlaces(
				addTopicPlace);

	}

	public ModifyDocumentTopicService getModifyDocumentTopicService() {
		return modifyDocumentTopicService;
	}

	public void setModifyDocumentTopicService(
			ModifyDocumentTopicService modifyDocumentTopicService) {
		this.modifyDocumentTopicService = modifyDocumentTopicService;
	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

	public void setMiaDocumentService(MiaDocumentService miaDocumentService) {
		this.miaDocumentService = miaDocumentService;
	}

	public HistoryLogService getHistoryLogService() {
		return historyLogService;
	}
}