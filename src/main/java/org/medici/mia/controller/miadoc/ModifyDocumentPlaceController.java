package org.medici.mia.controller.miadoc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.PlaceJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.document.FindPlaceForDocJson;
import org.medici.mia.common.json.document.ModifyPlaceForDocJson;
import org.medici.mia.domain.DocumentFieldEntity;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.miadoc.ModifyDocumentPlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Controller
@RequestMapping("/modifyDocumentPlace")
public class ModifyDocumentPlaceController {

	private final String PLACE_FIELD_NAME_FORALL_CATEGORIES = "placeOfOrigin";

	@Autowired
	private ModifyDocumentPlaceService modifyDocumentPlaceService;

	@Autowired
	private MiaDocumentService miaDocumentService;

	@Autowired
	private HistoryLogService historyLogService;

	// localhost:8181/Mia/json/modifyDocumentPlace/findDocumentPlace/131/correspondence
	@RequestMapping(value = "findDocumentPlace/{documentId}/{category}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, List<FindPlaceForDocJson>>> findDocumentPlace(
			@PathVariable("documentId") Integer documentId,
			@PathVariable("category") String category, HttpSession session) {

		String palceOfOrigin = getFieldNameForAllCategories(session, category);
		List<FindPlaceForDocJson> jsons = getModifyDocumentPlaceService()
				.findDocumentPlaces(documentId, palceOfOrigin);

		GenericResponseDataJson<HashMap<String, List<FindPlaceForDocJson>>> resp = new GenericResponseDataJson<HashMap<String, List<FindPlaceForDocJson>>>();
		HashMap<String, List<FindPlaceForDocJson>> docHash = new HashMap<String, List<FindPlaceForDocJson>>();
		if (jsons != null && !jsons.isEmpty()) {
			docHash.put("arrayPlace", jsons);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + jsons.size() + " places in total.");
			resp.setData(docHash);

		} else {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No places exists in DB. ");

		}
		return resp;

	}

	// localhost:8181/Mia/json/modifyDocumentPlace/modifyPlaceForDoc/
	@RequestMapping(value = "modifyPlaceForDoc", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyPlaceForDoc(
			@RequestBody ModifyPlaceForDocJson modifyPlace, HttpSession session,
			HttpServletRequest request) {

		if (modifyPlace == null) {

			GenericResponseJson resp = new GenericResponseJson();
			resp.setMessage("la request is not complete. DeletePeople or NewPeople may be null");
			resp.setStatus(StatusType.w.toString());
			return resp;

		}
		String placeOfOriginName = getFieldNameForAllCategories(session,
				modifyPlace.getCategory());
		if (placeOfOriginName != null) {
			placeOfOriginName = placeOfOriginName.replaceAll("\\s+", "");
		}

		// retrieve principal place
		PlaceJson placeJson = new PlaceJson();
		placeJson.setId(modifyPlace.getNewPlace().getId());
		if (modifyPlace.getNewPlace().getUnsure() == null) {
			placeJson.setUnsure("s");
		} else {
			placeJson.setUnsure(modifyPlace.getNewPlace().getUnsure());
		}

		List<PlaceJson> places = new ArrayList<PlaceJson>();
		places.add(placeJson);
		List<PlaceJson> prinPlaces = getMiaDocumentService()
				.getPrinciplaPlaces(places);
		if (prinPlaces == null || prinPlaces.isEmpty()) {
			GenericResponseJson resp = new GenericResponseJson();
			resp.setMessage("No principal places found in DB. Document not modified");
			resp.setStatus(StatusType.w.toString());
			return resp;
		}
		modifyPlace.getNewPlace().setId(prinPlaces.get(0).getId());
		modifyPlace.getNewPlace().setUnsure(prinPlaces.get(0).getUnsure());
		modifyPlace.getNewPlace().setPrefFlag(prinPlaces.get(0).getPrefFlag());

		return getModifyDocumentPlaceService().modifyDocumentPlaces(
				modifyPlace, placeOfOriginName);

	}

	// localhost:8181/Mia/json/modifyDocumentPlace/deletePlaceForDoc/
	@RequestMapping(value = "deletePlaceForDoc", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson deletePlaceForDoc(
			@RequestBody ModifyPlaceForDocJson modifyPlace, HttpSession session,
			HttpServletRequest request) {
		if (modifyPlace == null || modifyPlace.getIdPlaceToDelete() == null) {

			GenericResponseJson resp = new GenericResponseJson();
			resp.setMessage("la request is not complete. DeletePeople may be null");
			resp.setStatus(StatusType.w.toString());
			return resp;

		}
		String placeOfOriginName = getFieldNameForAllCategories(session,
				modifyPlace.getCategory());
		if (placeOfOriginName != null) {
			placeOfOriginName = placeOfOriginName.replaceAll("\\s+", "");
		}
		return getModifyDocumentPlaceService().modifyDocumentPlaces(
				modifyPlace, placeOfOriginName);
	}

	// localhost:8181/Mia/json/modifyDocumentPlace/addPlaceForDoc/
	@RequestMapping(value = "addPlaceForDoc", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson addPlaceForDoc(
			@RequestBody ModifyPlaceForDocJson modifyPlace, HttpSession session,
			HttpServletRequest request) {

		if (modifyPlace == null || modifyPlace.getNewPlace() == null
				|| modifyPlace.getNewPlace().getId() == null
				|| modifyPlace.getNewPlace().getId().equals("")) {
			GenericResponseJson resp = new GenericResponseJson();
			resp.setMessage("la request is not complete. NewPeople or id of newPeople may be null");
			resp.setStatus(StatusType.w.toString());
			return resp;

		}

		// retrieve principal place
		PlaceJson placeJson = new PlaceJson();
		placeJson.setId(modifyPlace.getNewPlace().getId());
		placeJson.setUnsure(modifyPlace.getNewPlace().getUnsure());
		List<PlaceJson> places = new ArrayList<PlaceJson>();
		places.add(placeJson);
		List<PlaceJson> prinPlaces = getMiaDocumentService()
				.getPrinciplaPlaces(places);
		if (prinPlaces == null || prinPlaces.isEmpty()) {
			GenericResponseJson resp = new GenericResponseJson();
			resp.setMessage("No principal places found in DB. Document not modified");
			resp.setStatus(StatusType.w.toString());
			return resp;
		}
		modifyPlace.getNewPlace().setId(prinPlaces.get(0).getId());
		modifyPlace.getNewPlace().setUnsure(prinPlaces.get(0).getUnsure());
		modifyPlace.getNewPlace().setPrefFlag(prinPlaces.get(0).getPrefFlag());

		String placeOfOriginName = getFieldNameForAllCategories(session,
				modifyPlace.getCategory());
		if (placeOfOriginName != null) {
			placeOfOriginName = placeOfOriginName.replaceAll("\\s+", "");
		}
		return getModifyDocumentPlaceService().modifyDocumentPlaces(
				modifyPlace, placeOfOriginName);

	}

	public String getFieldNameForAllCategories(HttpSession session,
			String category) {

		List<DocumentFieldEntity> documentFields = null;

		if (session.getAttribute("documentFields") != null) {
			documentFields = (List<DocumentFieldEntity>) session
					.getAttribute("documentFields");
		} else {
			documentFields = getMiaDocumentService().findDocumentFields();
			session.setAttribute("documentFields", documentFields);
		}

		if (documentFields != null && !documentFields.isEmpty()) {

			for (DocumentFieldEntity field : documentFields) {
				if (field.getDocumentCategory().equalsIgnoreCase(category)
						&& field.getFieldBeName().equalsIgnoreCase(
								PLACE_FIELD_NAME_FORALL_CATEGORIES)) {

					return field.getFieldName();
				}

			}

		}

		return PLACE_FIELD_NAME_FORALL_CATEGORIES;
	}

	public ModifyDocumentPlaceService getModifyDocumentPlaceService() {
		return modifyDocumentPlaceService;
	}

	public void setModifyDocumentPlaceService(
			ModifyDocumentPlaceService modifyDocumentPlaceService) {
		this.modifyDocumentPlaceService = modifyDocumentPlaceService;
	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}

	public void setMiaDocumentService(MiaDocumentService miaDocumentService) {
		this.miaDocumentService = miaDocumentService;
	}

	public HistoryLogService getHistoryLogService() {
		return historyLogService;
	}
}