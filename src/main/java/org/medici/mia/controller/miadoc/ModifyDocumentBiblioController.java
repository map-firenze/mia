package org.medici.mia.controller.miadoc;

import java.util.HashMap;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.document.FindBiblioForDocJson;
import org.medici.mia.common.json.document.ModifyBiblioForDocJson;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.domain.User;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.miadoc.ModifyDocumentBiblioService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * @author User
 *
 */
@Controller
@RequestMapping("/modifyDocumentBiblio")
public class ModifyDocumentBiblioController {

	@Autowired
	private ModifyDocumentBiblioService modifyDocumentBiblioService;

	@Autowired
	private UserService userService;

	@Autowired
	private GenericDocumentDao genericDocumentDao;

	@Autowired
	private HistoryLogService historyLogService;

	@Autowired
	private MiaDocumentService miaDocumentService;

	// localhost:8181/Mia/json/modifyDocumentBiblio/findDocumentBiblios/135
	@RequestMapping(value = "findDocumentBiblios/{documentId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, FindBiblioForDocJson>> findDocumentBiblios(
			@PathVariable("documentId") Integer documentId) {

		FindBiblioForDocJson json = getModifyDocumentBiblioService()
				.findDocumentBiblios(documentId);

		GenericResponseDataJson<HashMap<String, FindBiblioForDocJson>> resp = new GenericResponseDataJson<HashMap<String, FindBiblioForDocJson>>();
		HashMap<String, FindBiblioForDocJson> docHash = new HashMap<String, FindBiblioForDocJson>();
		if (json != null && json.getBiblios() != null
				&& !json.getBiblios().isEmpty()) {
			docHash.put("arrayBiblio", json);
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + json.getBiblios().size()
					+ " Biblios in total.");
			resp.setData(docHash);

		} else {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No Biblios exists in DB. ");

		}
		return resp;

	}

	// localhost:8181/Mia/json/modifyDocumentBiblio/modifyBiblioForDoc/
	@RequestMapping(value = "modifyBiblioForDoc", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyBiblioForDoc(
			@RequestBody ModifyBiblioForDocJson modifyBiblio,
			HttpServletRequest request) {

		return getModifyDocumentBiblioService().modifyDocumentBiblio(
				modifyBiblio);

	}

	// localhost:8181/Mia/json/modifyDocumentBiblio/deleteBiblioForDoc/
	@RequestMapping(value = "deleteBiblioForDoc", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson deleteBiblioForDoc(
			@RequestBody ModifyBiblioForDocJson modifyBiblio,
			HttpServletRequest request) {
		GenericResponseJson resp = getModifyDocumentBiblioService()
				.modifyDocumentBiblio(modifyBiblio);
		// Get the username logged in
		User user = getUserService().findUser(
				((UserDetails) SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()).getUsername());

		getGenericDocumentDao().setDocumentModInf(Integer.valueOf(modifyBiblio.getDocumentId()),
				user.getAccount());

		if (StatusType.ok.toString().equalsIgnoreCase(resp.getStatus())) {
			resp.setMessage("BiblioGraf of document deleted.");
		}
		return resp;

	}

	// localhost:8181/Mia/json/modifyDocumentBiblio/addBiblioForDoc/
	@RequestMapping(value = "addBiblioForDoc", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson addBiblioForDoc(
			@RequestBody ModifyBiblioForDocJson modifyBiblio,
			HttpServletRequest request) {

		GenericResponseJson resp = getModifyDocumentBiblioService()
				.modifyDocumentBiblio(modifyBiblio);

		if (StatusType.ok.toString().equalsIgnoreCase(resp.getStatus())) {
			resp.setMessage("BiblioGraf of document added.");
		}
		return resp;

	}

	public ModifyDocumentBiblioService getModifyDocumentBiblioService() {
		return modifyDocumentBiblioService;
	}

	public void setModifyDocumentBiblioService(
			ModifyDocumentBiblioService modifyDocumentBiblioService) {
		this.modifyDocumentBiblioService = modifyDocumentBiblioService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public GenericDocumentDao getGenericDocumentDao() {
		return genericDocumentDao;
	}

	public void setGenericDocumentDao(GenericDocumentDao genericDocumentDao) {
		this.genericDocumentDao = genericDocumentDao;
	}

	public HistoryLogService getHistoryLogService() {
		return historyLogService;
	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}
}