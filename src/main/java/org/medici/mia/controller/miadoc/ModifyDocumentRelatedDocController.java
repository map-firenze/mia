package org.medici.mia.controller.miadoc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.document.IModifyDocJson;
import org.medici.mia.common.json.document.ModifyRelatedDocsForDocJson;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.service.historylog.HistoryLogService;
import org.medici.mia.service.miadoc.JsonType;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.miadoc.ModifyDocumentRelatedDocService;
import org.medici.mia.service.miadoc.ModifyDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * @author User
 *
 */
@Controller
@RequestMapping("/relatedDocument")
public class ModifyDocumentRelatedDocController {

	@Autowired
	private ModifyDocumentService modifyDocumentService;

	@Autowired
	private ModifyDocumentRelatedDocService modifyDocumentRelatedDocService;

	@Autowired
	private HistoryLogService historyLogService;

	@Autowired
	private MiaDocumentService miaDocumentService;

	// localhost:8181/Mia/json/relatedDocument/findRelatedDocumentForDoc/125
	@RequestMapping(value = "findRelatedDocumentForDoc/{documentId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<HashMap<String, IModifyDocJson>> findRelatedDocuments(
			@PathVariable("documentId") Integer documentId) {

		List<IModifyDocJson> genericJsons = getModifyDocumentService()
				.findEntities(documentId, JsonType.relatedDocs);

		return setResponse(genericJsons, JsonType.relatedDocs.toString());

	}

	// localhost:8181/Mia/json/relatedDocument/modifyRelatedDocumentForDoc/
	@RequestMapping(value = "modifyRelatedDocumentForDoc", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyRelatedDocumentForDoc(
			@RequestBody ModifyRelatedDocsForDocJson relatedDocsJson) {

		GenericResponseJson resp = new GenericResponseJson();

		if (relatedDocsJson == null
				|| relatedDocsJson.getNewRelatedDoc() == null
				|| relatedDocsJson.getOldRelatedDoc() == null
				|| relatedDocsJson.getDocumentId() == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("documentId, oldRelatedDoc or newRelatedDoc maybe null or empty.");
			return resp;
		}

		List<Integer> relatedDocsToBeControlled = new ArrayList<Integer>();
		relatedDocsToBeControlled.add(relatedDocsJson.getOldRelatedDoc());
		relatedDocsToBeControlled.add(relatedDocsJson.getNewRelatedDoc());

		List<Integer> docsNotFound = getModifyDocumentService()
				.findNotFoundDocsForRelatedDocuments(relatedDocsToBeControlled);
		if (docsNotFound != null && !docsNotFound.isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			String docNotFoundList = "The Document is not modifid. Some relatedDocs may not exist in DB. The list of Not found Id's: ";
			for (Integer docs : docsNotFound) {
				docNotFoundList = docNotFoundList + docs + ", ";
			}
			resp.setMessage(docNotFoundList);

			return resp;
		}

		return getModifyDocumentRelatedDocService().modifyRelatedDocsForDoc(
				Integer.valueOf(relatedDocsJson.getDocumentId()),
				relatedDocsJson.getNewRelatedDoc(),
				relatedDocsJson.getOldRelatedDoc());

	}

	// localhost:8181/Mia/json/relatedDocument/addRelatedDocumentForDoc/
	@RequestMapping(value = "addRelatedDocumentForDoc", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson addRelatedDocumentForDoc(
			@RequestBody ModifyRelatedDocsForDocJson relatedDocsJson,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();

		if (relatedDocsJson == null
				|| relatedDocsJson.getNewRelatedDoc() == null
				|| relatedDocsJson.getDocumentId() == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("documentId or oldRelatedDoc  maybe null or empty.");
			return resp;
		}


		List<Integer> relatedDocsToBeControlled = new ArrayList<Integer>();
		relatedDocsToBeControlled.add(relatedDocsJson.getNewRelatedDoc());

		List<Integer> docsNotFound = getModifyDocumentService()
				.findNotFoundDocsForRelatedDocuments(relatedDocsToBeControlled);
		if (docsNotFound != null && !docsNotFound.isEmpty()) {
			resp.setStatus(StatusType.ko.toString());
			String docNotFoundList = "The Document is not modifid. Some relatedDocs may not exist in DB. The list of Not found Id's: ";
			for (Integer docs : docsNotFound) {
				docNotFoundList = docNotFoundList + docs + ", ";
			}
			resp.setMessage(docNotFoundList);

			return resp;
		}

		return getModifyDocumentRelatedDocService().addRelatedDocsForDoc(
				Integer.valueOf(relatedDocsJson.getDocumentId()),
				relatedDocsJson.getNewRelatedDoc());

	}

	// localhost:8181/Mia/json/relatedDocument/deleteRelatedDocumentForDoc/
	@RequestMapping(value = "deleteRelatedDocumentForDoc", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson deleteRelatedDocumentForDoc(
			@RequestBody ModifyRelatedDocsForDocJson relatedDocsJson,
			HttpServletRequest request) {

		GenericResponseJson resp = new GenericResponseJson();

		if (relatedDocsJson == null
				|| relatedDocsJson.getRelatedDocToBeDeleted() == null
				|| relatedDocsJson.getDocumentId() == null) {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("documentId, oldRelatedDoc or newRelatedDoc maybe null or empty.");
			return resp;
		}
		return getModifyDocumentRelatedDocService().deleteRelatedDocsForDoc(
				Integer.valueOf(relatedDocsJson.getDocumentId()),
				relatedDocsJson.getRelatedDocToBeDeleted());

	}

	private GenericResponseDataJson<HashMap<String, IModifyDocJson>> setResponse(
			List<IModifyDocJson> jsons, String jsonName) {
		GenericResponseDataJson<HashMap<String, IModifyDocJson>> resp = new GenericResponseDataJson<HashMap<String, IModifyDocJson>>();
		HashMap<String, IModifyDocJson> docHash = new HashMap<String, IModifyDocJson>();

		if (jsons != null && !jsons.isEmpty()) {
			docHash.put(jsonName, jsons.get(0));
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + jsonName);
			resp.setData(docHash);
			return resp;
		} else {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage("No " + jsonName + " exists in DB. ");
			return resp;
		}

	}

	public ModifyDocumentService getModifyDocumentService() {
		return modifyDocumentService;
	}

	public void setModifyDocumentService(
			ModifyDocumentService modifyDocumentService) {
		this.modifyDocumentService = modifyDocumentService;
	}

	public ModifyDocumentRelatedDocService getModifyDocumentRelatedDocService() {
		return modifyDocumentRelatedDocService;
	}

	public void setModifyDocumentRelatedDocService(
			ModifyDocumentRelatedDocService modifyDocumentRelatedDocService) {
		this.modifyDocumentRelatedDocService = modifyDocumentRelatedDocService;
	}

	public HistoryLogService getHistoryLogService() {
		return historyLogService;
	}

	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}
}