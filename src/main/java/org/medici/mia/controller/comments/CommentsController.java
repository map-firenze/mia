package org.medici.mia.controller.comments;


import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.comment.CommentJson;
import org.medici.mia.common.json.comment.CreateCommentJson;
import org.medici.mia.common.json.message.CommentReportMessageJson;
import org.medici.mia.dao.comment.CommentDAO;
import org.medici.mia.dao.insert.InsertDAO;
import org.medici.mia.dao.miadoc.GenericDocumentDao;
import org.medici.mia.dao.people.PeopleDAO;
import org.medici.mia.dao.place.PlaceDAO;
import org.medici.mia.dao.uploadinfo.UploadInfoDAO;
import org.medici.mia.dao.userrole.UserRoleDAO;
import org.medici.mia.dao.volume.VolumeDAO;
import org.medici.mia.domain.*;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.comment.CommentService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/comments")
public class CommentsController {

    @Autowired
    private UserService userService;

    @Autowired
    private CommentService commentService;

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody GenericResponseDataJson postComment(
            @RequestBody CreateCommentJson createjson) {
        GenericResponseDataJson response = new GenericResponseDataJson();
        if(createjson.getRecordType() == null || createjson.getRecordId() == null){
            response.setMessage("Fields `recordType` and `recordId` cannot be absent or empty");
            response.setStatus("error");
            return response;
        }
        Comment.RecordType type;
        try{
            type = Comment.RecordType.valueOf(createjson.getRecordType());
        } catch (IllegalArgumentException ex){
            response.setMessage("Record type "+createjson.getRecordType()+" is not a valid type");
            response.setStatus("error");
            return response;
        }
        if(!commentService.validateRecord(createjson.getRecordId(), type)){
            response.setMessage("Record with type "+createjson.getRecordType()+" and id = "+createjson.getRecordId()+" does not exist or is deleted");
            response.setStatus("error");
            return response;
        }
        if(createjson.getText() == null){
            response.setMessage("Text of the comment cannot be empty");
            response.setStatus("error");
            return response;
        }
        if(createjson.getText().length() > 2000){
            response.setMessage("Comment text must be no longer than 2000 characters");
            response.setStatus("error");
            return response;
        }
        if(createjson.getText().length() == 0){
            response.setMessage("Text of the comment cannot be empty");
            response.setStatus("error");
            return response;
        }
        CommentJson comment = commentService.createComment(createjson);
        if(comment == null){
            response.setMessage("Failed to create a comment");
            response.setStatus("error");
            return response;
        }
        response.setData(comment);
        response.setStatus("OK");
        response.setMessage("Successfully created a comment with id = "+comment.getId());
        return response;
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public @ResponseBody GenericResponseJson replyToComment(
            @PathVariable("id") Integer id,
            @RequestBody CreateCommentJson createjson
    ) {
        GenericResponseDataJson<CommentJson> response = new GenericResponseDataJson<CommentJson>();
        Comment parent = commentService.findActiveComment(id);
        if(parent == null){
            response.setMessage("Comment with id = "+id+" does not exist or it's deleted");
            response.setStatus("error");
            return response;
        }
        if(parent.getParent() != null){
            response.setMessage("Cannot reply to comment, that already has a parent");
            response.setStatus("error");
            return response;
        }
        if(createjson.getRecordType() == null || createjson.getRecordId() == null){
            response.setMessage("Fields `recordType` and `recordId` cannot be absent or empty");
            response.setStatus("error");
            return response;
        }
        Comment.RecordType type;
        try{
            type = Comment.RecordType.valueOf(createjson.getRecordType());
        } catch (IllegalArgumentException ex){
            response.setMessage("Record type `"+createjson.getRecordType()+"` is not a valid type");
            response.setStatus("error");
            return response;
        }
        if(!commentService.validateRecord(createjson.getRecordId(), type)){
            response.setMessage("Record with type "+createjson.getRecordType()+" and id = "+createjson.getRecordId()+" does not exist or it's deleted");
            response.setStatus("error");
            return response;
        }
        if(createjson.getText() == null){
            response.setMessage("Text of the comment cannot be empty");
            response.setStatus("error");
            return response;
        }
        if(createjson.getText().length() > 2000){
            response.setMessage("Comment text must be no longer than 2000 characters");
            response.setStatus("error");
            return response;
        }
        if(createjson.getText().length() == 0){
            response.setMessage("Text of the comment cannot be empty");
            response.setStatus("error");
            return response;
        }
        CommentJson comment = commentService.replyToComment(createjson, id);
        response.setData(comment);
        response.setStatus("OK");
        response.setMessage("Successfully replied to a comment with id = "+id);
        return response;
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public @ResponseBody GenericResponseJson modifyComment(
            @PathVariable("id") Integer id,
            @RequestBody HashMap<String, Object> modifyjson,
            HttpServletResponse httpServletResponse
    ) {
        GenericResponseDataJson<CommentJson> response = new GenericResponseDataJson<CommentJson>();
        Comment comment = commentService.findComment(id);
        if(comment == null){
            response.setMessage("Comment with id = "+id+" does not exist or it's deleted");
            return response;
        }
        if(comment.getDeletedBy() != null){
            response.setMessage("Cannot edit deleted comment");
            return response;
        }
        User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        Boolean isAuthorizedToEdit = userService.isAccountAdministrator(user.getAccount());
        if(!comment.getCreatedBy().getAccount().equals(user.getAccount())){
            if(!isAuthorizedToEdit){
                response.setMessage("User is not authorized to edit this comment");
                response.setStatus("error");
                return response;
            }
        }
        if(!modifyjson.containsKey("text") && !modifyjson.containsKey("repliedToId")){
            httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        Integer repliedToId = null;
        if(modifyjson.get("repliedToId") != null) {
            try {
                repliedToId = (Integer) modifyjson.get("repliedToId");
            } catch (ClassCastException ex) {
                response.setMessage("The value `" + modifyjson.get("repliedToId") + "` of field `repliedToId` is not integer");
                response.setStatus("error");
                return response;
            }
        }
        Comment repliedTo = null;
        if(repliedToId != null){
            repliedTo = commentService.findActiveComment(repliedToId);
            if(repliedTo == null){
                response.setMessage("Comment (repliedTo) with id = "+repliedToId+" does not exist or it's deleted");
                response.setStatus("error");
                return response;
            }
        }
        String text = null;
        if(modifyjson.get("text") != null) {
            try {
                text = (String) modifyjson.get("text");
                if(text.length() > 2000){
                    response.setMessage("Comment text must be no longer than 2000 characters");
                    response.setStatus("error");
                    return response;
                }
            } catch (ClassCastException ex) {
                response.setMessage("The value `" + modifyjson.get("text") + "` of field `text` cannot be casted to string type");
                response.setStatus("error");
                return response;
            }
        }
        CommentJson json = commentService.modifyComment(comment, text, repliedTo, modifyjson.containsKey("mentions") ? (String) modifyjson.get("mentions") : "no_field");
        response.setStatus("success");
        response.setMessage("The comment with id = "+id+" is successfully modified");
        response.setData(json);
        return response;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public @ResponseBody GenericResponseDataJson deleteComment(
            @PathVariable("id") Integer id,
            HttpServletResponse httpServletResponse
    ) {
        GenericResponseDataJson<CommentJson> response = new GenericResponseDataJson<CommentJson>();
        response.setStatus("error");
        Comment comment = commentService.findComment(id);
        if(comment == null){
            response.setMessage("Comment with id = "+id+" does not not exist");
            return response;
        }
        if(comment.getLogicalDelete()){
            response.setMessage("Comment with id = "+id+" is already logically deleted");
            return response;
        }
        User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        if(!comment.getCreatedBy().getAccount().equals(user.getAccount())){
            if(!commentService.isAuthorizedForDelete(user.getAccount())){
                response.setMessage("User is not authorized to delete this comment");
                return response;
            }
        }

        CommentJson json = commentService.deleteComment(id);
        if(json == null){
            response.setMessage("Failed to delete a comment with id = "+id);
            return response;
        }
        response.setData(commentService.deleteComment(id));
        response.setMessage("A comment with id = "+id+" was successfully deleted");
        response.setStatus("success");
        return response;
    }

    @RequestMapping(value = "/{id}/undelete", method = RequestMethod.POST)
    public @ResponseBody GenericResponseDataJson undeleteComment(
            @PathVariable("id") Integer id
    ) {
        GenericResponseDataJson<CommentJson> response = new GenericResponseDataJson<CommentJson>();
        response.setStatus("error");
        Comment comment = commentService.findComment(id);
        if(comment == null){
            response.setMessage("Comment with id = "+id+" does not exist");
            return response;
        }
        if(!comment.getLogicalDelete()){
            response.setMessage("Comment with id = "+id+" is not logically deleted");
            return response;
        }
        User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        if(comment.getCreatedBy().getAccount().equals(user.getAccount())
                && commentService.isAuthorizedForDelete(comment.getDeletedBy().getAccount())
                && !commentService.isAuthorizedForDelete(user.getAccount())){
            response.setMessage("User is not authorized to restore this comment");
            return response;
        }
        if(!comment.getCreatedBy().getAccount().equals(user.getAccount())){
            if(!commentService.isAuthorizedForDelete(user.getAccount())){
                response.setMessage("User is not authorized to delete this comment");
                return response;
            }
        }
        CommentJson json = commentService.undeleteComment(id);
        if(json == null){
            response.setMessage("Failed to restore a comment with id = "+id);
            return response;
        }
        response.setData(json);
        response.setMessage("A comment with id = "+id+" was successfully restored");
        response.setStatus("success");
        return response;
    }

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody HashMap<String, Object> getParentCommentsByRecord(
            @RequestParam(value = "recordId") Integer id,
            @RequestParam(value = "recordType") String recordType,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "perPage", required = false) Integer perPage
    ) {
        HashMap<String, Object> response = new HashMap<String, Object>();
        response.put("status", "error");
        Comment.RecordType type;
        try{
            type = Comment.RecordType.valueOf(recordType);
        } catch (IllegalArgumentException ex){
            response.put("message","Record type "+recordType+" is not a valid type");
            return response;
        }
        if(!commentService.validateRecord(id, type)){
            response.put("message","Record with type "+recordType+" and id = "+id+" does not exist or is deleted");
            return response;
        }
        List<CommentJson> json = commentService.findByRecord(id, type);
        response.put("data", json);
        response.put("status", "success");
        response.put("message","Got "+json.size()+" comment threads");
        response.put("count", json.size());
        return response;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody HashMap<String, Object> getComment(
            @PathVariable Integer id,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "perPage", required = false) Integer perPage
    ) {
        HashMap<String, Object> response = new HashMap<String, Object>();
        response.put("status", "error");
        Comment comment = commentService.findComment(id);
        if(comment == null){
            response.put("message","Comment with id = `"+id+" does not exist");
            return response;
        }
        if(comment.getParent()!=null){
            response.put("message","Cannot get comment thread for comment with id = `"+id+", because it's already a child");
        }
        User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        if(comment.getLogicalDelete()){
            if(!commentService.isAuthorized(user.getAccount())) {
                if (!user.getAccount().equals(comment.getCreatedBy().getAccount())) {
                    response.put("message", "You are not permitted to view this comment thread");
                    return response;
                }
            }
        }
        List<CommentJson> jsons = commentService.findById(id);
        response.put("parent", commentService.setPhoto(new CommentJson().toJson(comment, user), comment));
        response.put("status", "success");
        response.put("message", "Got comment thread with "+jsons.size()+" replies");
        response.put("data", jsons);
        response.put("count", jsons.size());
        return response;
    }

    @RequestMapping(value = "{id}/report", method = RequestMethod.POST)
    public @ResponseBody GenericResponseJson reportComment(
            @PathVariable("id") Integer id,
            @RequestBody CommentReportMessageJson messageJson,
            HttpServletResponse httpServletResponse
    ) {
        GenericResponseJson response = new GenericResponseJson();
        Comment comment = commentService.findComment(id);
        if(comment == null){
            response.setMessage("Comment was not found");
            response.setStatus("error");
            return response;
        }
        if(comment.getLogicalDelete()){
            response.setMessage("The comment you tried to report is deleted");
            response.setStatus("error");
            return response;
        }
        if(messageJson.getUrl() == null){
            httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        return commentService.reportComment(comment, messageJson);
    }
}
