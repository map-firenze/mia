package org.medici.mia.controller.collation;


import com.github.underscore.U;
import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.collation.*;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.domain.Collation;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.User;
import org.medici.mia.service.collation.CollationService;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("collation")
public class CollationController {

    @Autowired
    private CollationService collationService;

    @Autowired
    private MiaDocumentService miaDocumentService;

    @Autowired
    private UserService userService;

    private List<Collation> findCollationsInFiles(MiaDocumentEntity doc){
        List<Collation> foundCollations = new ArrayList<Collation>();
        for(UploadFileEntity f: doc.getUploadFileEntities()){
            for(MiaDocumentEntity d: f.getMiaDocumentEntities()){
                List<Collation> collations = collationService.findByDocumentId(d.getDocumentEntityId());
                if(collations != null && collations.size() > 0){
                    foundCollations.addAll(collations);
                }
            }
        }
        return U.uniq(foundCollations);
    }

    @RequestMapping(value = "/attachment/{id}", method = RequestMethod.GET)
    @ResponseBody
    public HashMap<String, Object> getAttachmentCollation(
            @PathVariable("id") Integer id
    ){
        HashMap<String, Object> resp = new HashMap<String, Object>();
        if(id == null){
            resp.put("status", "error");
            resp.put("message", "Wrong id");
            return resp;
        }
        Collation collation = collationService.findById(id);
        if(collation == null){
            resp.put("status", "error");
            resp.put("message", "Collation with id " + id + " not found");
            return resp;
        }
        CollationJson json = new CollationJson();
        List<MiaDocumentEntity> entities = new ArrayList<MiaDocumentEntity>();
        entities.add(collation.getChildDocument());
        entities.add(collation.getParentDocument());
        List<DocumentJson> jsons = miaDocumentService.createDocumentsJsonFromDocEntity(entities);
        json.toJson(collation, jsons);
        resp.put("status", "success");
        resp.put("message", "Got 1 collation");
        resp.put("data", json);
        return resp;
    }

    @RequestMapping(value = "/attachment", method = RequestMethod.POST)
    @ResponseBody
    public HashMap<String, Object> createAttachmentCollation(
            @RequestBody CreateAttachmentCollationJson requestJson){

        HashMap<String, Object> resp = new HashMap<String, Object>();
        resp.put("status", "error");
        resp.put("data", null);

        requestJson.setUnsure(requestJson.getUnsure() == null ? false : requestJson.getUnsure());
        if(requestJson.getParentDocumentId() == null || requestJson.getChildDocumentId() == null){
            resp.put("message", "JSON does not contain child or parent document ID");
            return resp;
        }
        if(collationService.checkIfAlreadyParent(requestJson.getChildDocumentId())){
            resp.put("message", "Child document is already a parent in another collation");
            return resp;
        }
        if(collationService.checkIfAlreadyChild(requestJson.getChildDocumentId())){
            resp.put("message", "Child document is already has a relation in another collation");
            return resp;
        }
        if(collationService.checkIfAttachmentExists(requestJson.getChildDocumentId(), requestJson.getParentDocumentId())){
            resp.put("message", "Collation with such relationship is already exists");
            return resp;
        }
        MiaDocumentEntity childDoc = collationService.findDocumentById(requestJson.getChildDocumentId());
        MiaDocumentEntity parentDoc = collationService.findDocumentById(requestJson.getParentDocumentId());
        if(childDoc == null){
            resp.put("message", "Child document with id = " + requestJson.getChildDocumentId() + " does not exist");
            return resp;
        }
        if(parentDoc == null){
            resp.put("message", "Parent document with id = " + requestJson.getParentDocumentId() + " does not exist");
            return resp;
        }
        if(!(childDoc.getPrivacy() == null) && childDoc.getPrivacy() != 0){
            resp.put("message", "Child document with id = " + requestJson.getChildDocumentId() + " is private");
            return resp;
        }
        if(!(parentDoc.getPrivacy() == null) && parentDoc.getPrivacy() != 0){
            resp.put("message", "Parent document with id = " + requestJson.getParentDocumentId() + " is private");
            return resp;
        }
        if(childDoc.getUploadFileEntities() == null || childDoc.getUploadFileEntities().isEmpty()){
            resp.put("message", "Child document with id = " + requestJson.getChildDocumentId() +" does not contain any files");
            return resp;
        }
        if(parentDoc.getUploadFileEntities() == null || parentDoc.getUploadFileEntities().isEmpty()){
            resp.put("message", "Parent document with id = " + requestJson.getParentDocumentId() + " does not contain any files");
            return resp;
        }
        if(requestJson.getChildDocumentId().equals(requestJson.getParentDocumentId())){
            resp.put("message", "Cannot collate the document to itself");
            return resp;
        }
        if(!collationService.checkIfAlreadyParent(requestJson.getParentDocumentId())){
            List<Collation> parentCollations = findCollationsInFiles(parentDoc);
            List<Collation> childCollations = findCollationsInFiles(childDoc);
            if(parentCollations.size() > 0){
                resp.put("message", "It is not possible to create a collation. There are some images that are already used in collation:");
                List<CollationJson> jsons = new ArrayList<CollationJson>();
                for(Collation c: parentCollations){
                    List<MiaDocumentEntity> entities = new ArrayList<MiaDocumentEntity>();
                    entities.add(c.getChildDocument());
                    entities.add(c.getParentDocument());
                    CollationJson json = new CollationJson();
                    json.toJson(c, miaDocumentService.createDocumentsJsonFromDocEntity(entities));
                    jsons.add(json);
                }
                resp.put("data", U.uniq(jsons));
                return resp;
            }
            if(childCollations.size() > 0){
                resp.put("message", "It is not possible to create a collation. There are some images that are already used in collation:");
                List<CollationJson> jsons = new ArrayList<CollationJson>();
                for(Collation c: childCollations){
                    List<MiaDocumentEntity> entities = new ArrayList<MiaDocumentEntity>();
                    entities.add(c.getChildDocument());
                    entities.add(c.getParentDocument());
                    CollationJson json = new CollationJson();
                    json.toJson(c, miaDocumentService.createDocumentsJsonFromDocEntity(entities));
                    jsons.add(json);
                }
                resp.put("data", U.uniq(jsons));
                return resp;
            }
        }
        if(collationService.checkIfAlreadyParent(requestJson.getParentDocumentId())) {
            List<Collation> childCollations = findCollationsInFiles(childDoc);
            if(childCollations.size() > 0){
                resp.put("message", "It is not possible to create a collation. There are some images that are already used in collation:");
                List<CollationJson> jsons = new ArrayList<CollationJson>();
                List<MiaDocumentEntity> entities = new ArrayList<MiaDocumentEntity>();
                for(Collation c: childCollations){
                    entities.add(c.getChildDocument());
                    entities.add(c.getParentDocument());
                    CollationJson json = new CollationJson();
                    json.toJson(c, miaDocumentService.createDocumentsJsonFromDocEntity(entities));
                    jsons.add(json);
                }
                U.uniq(jsons);
                resp.put("data", U.uniq(jsons));
                return resp;
            }
        }
        if(requestJson.getTitle() != null){
            if(requestJson.getTitle().length() > 100){
                resp.put("status", "error");
                resp.put("message", "Title must be no more than 100 characters");
                resp.put("data", null);
                return resp;
            }
        }
        if(requestJson.getDescription() != null){
            if(requestJson.getDescription().length() > 500){
                resp.put("status", "error");
                resp.put("message", "Description must be no more than 500 characters");
                resp.put("data", null);
                return resp;
            }
        }
        CollationJson json = collationService.createAttachment(requestJson);
        resp.put("status", "success");
        resp.put("message", "Collation with id " + json.getId() + " is successfully created");
        resp.put("data", json);
        return resp;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public HashMap<String, Object> deleteCollation(
            @PathVariable Integer id, HttpServletResponse response){
        HashMap<String, Object> resp = new HashMap<String, Object>();
        Collation collation = collationService.findById(id);
        if(collation == null){
            resp.put("status", "error");
            resp.put("message", "Collation with id " + id + " not found");
            return resp;
        }
        User user = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        boolean isAuthorized = userService.isAccountAutorizedForDelete(user.getAccount())
                || user.getAccount().equals(collation.getCreatedBy().getAccount());
        if(!isAuthorized){
            resp.put("message", "This user is not authorized to delete");
            resp.put("status", "error");
            return resp;
        }
        if(collationService.deleteCollation(collation)){
            resp.put("status", "success");
            resp.put("message", "Collation with id " + id + " successfully deleted");
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            return resp;
        } else {
            resp.put("status", "success");
            resp.put("message", "Error occured while deleting collation with id " + id);
            return resp;
        }
    }

//    @RequestMapping(value = "/{id}/undelete", method = RequestMethod.POST)
//    @ResponseBody
//    public HashMap<String, Object> undeleteCollation(
//            @PathVariable Integer id){
//        HashMap<String, Object> resp = new HashMap<String, Object>();
//        if(id == null){
//            resp.put("status", "error");
//            resp.put("message", "Wrong id");
//            return resp;
//        }
//        Collation collation = collationDAO.find(id);
//        Integer childDocId = collation.getChildDocument() == null ? null : collation.getChildDocument().getDocumentEntityId();
//        Integer parentDocId = collation.getParentDocument() == null ? null : collation.getParentDocument().getDocumentEntityId();
//        if(collation == null){
//            resp.put("status", "error");
//            resp.put("message", "Collation with id " + id + " not found");
//            return resp;
//        }
//        if(collation.getLogicalDelete()){
//            resp.put("status", "error");
//            resp.put("message", "Collation with id " + id + " is not deleted");
//        }
//        if(collationDAO.findConflictsInDeleted(childDocId, parentDocId)){
//            resp.put("status", "error");
//            resp.put("message", "Collation with id " + id + " contains conflicting document IDs of the active collations");
//        }
//        collationService.undeleteCollation(collation);
//        resp.put("status", "success");
//        resp.put("message", "Collation with id "+id+" successfully restored");
//        return resp;
//    }

    @RequestMapping(value = "/attachment", method = RequestMethod.GET)
    @ResponseBody
    public HashMap<String, Object> getAttachmentCollationByDocumentId(
            @RequestParam("documentId") Integer documentId
    ){
        HashMap<String, Object> resp = new HashMap<String, Object>();
        List<CollationJson> jsons = collationService.findAttachmentsByDocumentId(documentId);
        resp.put("status", "success");
        resp.put("message", "Got " + jsons.size() + " collations for document with id "+ documentId);
        resp.put("data", jsons);
        return resp;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    @ResponseBody
    public HashMap<String, Object> modifyCollation(
            @PathVariable Integer id,
            @RequestBody HashMap json,
            HttpServletResponse response){
        HashMap<String, Object> resp = new HashMap<String, Object>();
        Collation collation = collationService.findById(id);
        if(collation == null){
            resp.put("status", "error");
            resp.put("message", "Collation with id " + id + " not found");
            resp.put("data", null);
            return resp;
        }
        if(json == null || json.size() == 0){
            resp.put("status", "error");
            resp.put("message", "JSON is empty");
            resp.put("data", null);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return resp;
        }
        if(!json.containsKey("unsure") && !json.containsKey("title") && !json.containsKey("description")){
            resp.put("status", "error");
            resp.put("message", "JSON does not contain needed fields");
            resp.put("data", null);
            return resp;
        }
        if(json.containsKey("unsure")){
            if(!(json.get("unsure") instanceof Boolean || json.get("unsure") == null)){
                resp.put("status", "error");
                resp.put("message", "field `unsure` can be only boolean or null");
                resp.put("data", null);
                return resp;
            }
        }
        if(json.containsKey("title")){
            if(((String) json.get("title")).length() > 100){
                resp.put("status", "error");
                resp.put("message", "Title must be no more than 100 characters");
                resp.put("data", null);
                return resp;
            }
        }
        if(json.containsKey("description")){
            if(((String) json.get("description")).length() > 500){
                resp.put("status", "error");
                resp.put("message", "Description must be no more than 500 characters");
                resp.put("data", null);
                return resp;
            }
        }
        CollationJson modified = collationService.modifyCollation(collation, json);
        resp.put("status", "success");
        resp.put("message", "Collation with id "+id+" successfully modified");
        resp.put("data", modified);
        return resp;
    }

    @RequestMapping(value = "/lacuna", method = RequestMethod.POST)
    @ResponseBody
    public HashMap<String, Object> createLacunaCollation(
            @RequestBody CreateLacunaCollationJson requestJson,
            HttpServletResponse response){
        HashMap<String, Object> resp = new HashMap<String, Object>();
        resp.put("status", "error");
        resp.put("data", null);
        requestJson.setUnsure(requestJson.getUnsure() == null ? false : requestJson.getUnsure());
        if(requestJson.getVolumeId() == null && requestJson.getInsertId() == null){
            resp.put("message", "JSON does not contain volume or insert");
            return resp;
        }
        if(requestJson.getChildDocumentId() == null){
            resp.put("message", "JSON does not contain child document ID");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return resp;
        }
        if(requestJson.getUploadFileId() == null){
            resp.put("message", "JSON does not contain upload file ID");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return resp;
        }
        if(requestJson.getFolioNumber() == null){
            resp.put("message", "JSON does not contain folio number");
            return resp;
        }
        if(collationService.checkIfAlreadyParent(requestJson.getChildDocumentId())){
            resp.put("message", "Child document is already a parent in another collation");
            return resp;
        }
        if(collationService.checkIfAlreadyChild(requestJson.getChildDocumentId())){
            resp.put("message", "Child document is already has a relation in another collation");
            return resp;
        }
        MiaDocumentEntity doc = collationService.findDocumentById(requestJson.getChildDocumentId());
        if(doc == null){
            resp.put("message", "Child document does not exist");
            return resp;
        }
        if(!(doc.getPrivacy() == null) && doc.getPrivacy() != 0){
            resp.put("message", "Child document with id = " + requestJson.getChildDocumentId() + " is private");
            return resp;
        }
        List<Collation> collationsList = findCollationsInFiles(doc);
        if(collationsList.size() > 0){
            resp.put("message", "It is not possible to create a collation. There are some images that are already used in collation:");
            List<CollationJson> jsons = new ArrayList<CollationJson>();
            for(Collation c: collationsList){
                List<MiaDocumentEntity> entities = new ArrayList<MiaDocumentEntity>();
                entities.add(c.getChildDocument());
                entities.add(c.getParentDocument());
                CollationJson json = new CollationJson();
                json.toJson(c, miaDocumentService.createDocumentsJsonFromDocEntity(entities));
                jsons.add(json);
            }
            resp.put("data", U.uniq(jsons));
            return resp;
        }
        boolean fileExists = false;
        for(UploadFileEntity f: doc.getUploadFileEntities()){
            fileExists = requestJson.getUploadFileId().equals(f.getUploadFileId());
            if(fileExists) break;
        }
        if(!fileExists){
            resp.put("message", "DE with ID " + requestJson.getChildDocumentId() + " does not contain upload file with ID " + requestJson.getUploadFileId());
            return resp;
        }
        if(doc.getUploadFileEntities() == null || doc.getUploadFileEntities().isEmpty()){
            resp.put("message", "Child document does not contain any files");
            return resp;
        }
        if(doc.getUploadFileEntities().get(0).getUploadInfoEntity().getVolume() != null && requestJson.getVolumeId() != null){
            if(doc.getUploadFileEntities().get(0).getUploadInfoEntity().getVolume().equals(requestJson.getVolumeId())){
                resp.put("message", "Child and parent volumes cannot be equal");
                return resp;
            }
        }
        if(!requestJson.getUnsure() && requestJson.getDescription() != null){
            resp.put("message", "Cannot set description if `unsure` key is not present or false");
            return resp;
        }
        if(doc.getUploadFileEntities().get(0).getUploadInfoEntity().getInsert() != null && requestJson.getInsertId() != null) {
            if (doc.getUploadFileEntities().get(0).getUploadInfoEntity().getInsert().equals(requestJson.getInsertId())) {
                resp.put("message", "Child and parent insert cannot be equal");
                return resp;
            }
        }
        if(requestJson.getTitle() != null){
            if(requestJson.getTitle().length() > 100){
                resp.put("status", "error");
                resp.put("message", "Title must be no more than 100 characters");
                resp.put("data", null);
                return resp;
            }
        }
        if(requestJson.getDescription() != null){
            if(requestJson.getDescription().length() > 500){
                resp.put("status", "error");
                resp.put("message", "Description must be no more than 500 characters");
                resp.put("data", null);
                return resp;
            }
        }
        CollationJson json = collationService.createLacuna(requestJson);
        resp.put("status", "success");
        resp.put("message", "Collation with id " + json.getId() + " is successfully created");
        resp.put("data", json);
        return resp;
    }

    @RequestMapping(value = "/findInDocumentImages/{id}", method = RequestMethod.GET)
    @ResponseBody
    public GenericResponseDataJson findInDocumentImages(
            @PathVariable("id") Integer id,
            HttpServletResponse response) {
        GenericResponseDataJson<List<CollationJson>> resp = new GenericResponseDataJson<List<CollationJson>> ();
        MiaDocumentEntity doc = miaDocumentService.findDocumentEntityById(id);
        if(doc == null){
            resp.setMessage("Document with id = "+id+" does not exist or is deleted");
            resp.setStatus("error");
            return resp;
        }
        List<Collation> collationsList = findCollationsInFiles(doc);
        if(collationsList.size() > 0){
            List<CollationJson> jsons = new ArrayList<CollationJson>();
            for(Collation c: collationsList){
                List<MiaDocumentEntity> entities = new ArrayList<MiaDocumentEntity>();
                entities.add(c.getChildDocument());
                entities.add(c.getParentDocument());
                CollationJson json = new CollationJson();
                json.toJson(c, miaDocumentService.createDocumentsJsonFromDocEntity(entities));
                jsons.add(json);
            }
            resp.setData(U.uniq(jsons));
            resp.setMessage("Found " + resp.getData().size() + " collations prepending to this document");
            resp.setStatus("success");
            return resp;
        } else{
            resp.setData(new ArrayList<CollationJson>());
            resp.setMessage("Found " + 0 + " collations prepending to this document");
            resp.setStatus("success");
            return resp;
        }

    }
}
