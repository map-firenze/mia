/*
 * ImageBiaMiaThread.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.controller.checks;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.jboss.util.file.Files;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.controller.upload.UploadBean;
import org.medici.mia.domain.CollectionEntity;
import org.medici.mia.domain.GoogleLocation;
import org.medici.mia.domain.InsertEntity;
import org.medici.mia.domain.RepositoryEntity;
import org.medici.mia.domain.SeriesEntity;
import org.medici.mia.domain.User;
import org.medici.mia.domain.Volume;
import org.medici.mia.service.upload.UploadService;
import org.medici.mia.service.user.UserService;
import org.medici.mia.service.volbase.VolBaseService;
import org.medici.mia.service.volumeinsert.VolumeInsertDescriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author alai
 *
 */
public class ImageBiaMiaProcess  {

    private boolean started;
    
    private VolumeInsertDescriptionService volumeInsertDescriptionService;
    private UserService userService;
	private VolBaseService volBaseService;
	private UploadService uploadService;
	FileWriter errorFile;
	UserDetails userDetail;
    private String sourcePath;
    private int totalFile = 0;
    private int currentStep =1;
    public static String MDP = "MDP";

    public ImageBiaMiaProcess(FileWriter errorFile, UploadService uploadService, UserDetails userDetail, VolBaseService volBaseService, VolumeInsertDescriptionService volumeInsertDescriptionService, UserService userService, String sourcePath) {
    	this.uploadService = uploadService;
    	this.volumeInsertDescriptionService = volumeInsertDescriptionService;
    	this.userService = userService;
    	this.volBaseService = volBaseService;
    	this.sourcePath = sourcePath;
    	this.userDetail = userDetail;
    	this.errorFile = errorFile;
    	totalFile = 0;
    	currentStep =1;
        started = false;
    }
//    2.a) 
//    Check if volume exist in DB if not error message and exit.
//    Check if for each file in MDP177 there is its corresponding file in originalFiles and its size is > 0
//                - if problem write to file $volumename.log (do at all of them then exit 0)
//    - Check for each in MDP177  its corresponding file in originalFiles and its size is > 0
//                - if problem write to file $volumename.log (do at all of them then exit 0)
//
//    2.b) Check if volume has inserts: check if any filename has square brackets ( [] ) in it. 
//    Create insert if not exist.
//
//    For example:
//
//    Lets pretend that we are uploading volume MDP177:
//
//    I cycle through all filenames in the directory /data/fromBia/TILED/MDP177 searching for pattern ‘[‘
//    I get the following 
//    0018_[1]_C_004_R.tif 
//    00661_[2]_C_005_R.tif
//
//    There are two inserts 1 and 2
//
//    3) I create	[if they are not already existing] the necessary directories:
//    the volume directory: /data/storage/ASFI-ChIJrdbSgKZWKhMRAyrH7xd51ZM/MDP/177
//    The two inserts directories:
//    		/data/storage/ASFI-ChIJrdbSgKZWKhMRAyrH7xd51ZM/MDP/177/1
//    		/data/storage/ASFI-ChIJrdbSgKZWKhMRAyrH7xd51ZM/MDP/177/2
//
//    4) I move each file from the [$insertNumber] insert inside the right insert directory:
//
//    0018_[1]_C_004_R.tif into /data/storage/ASFI-ChIJrdbSgKZWKhMRAyrH7xd51ZM/MDP/177/1
//
//    00661_[2]_C_005_R.tif into /data/storage/ASFI-ChIJrdbSgKZWKhMRAyrH7xd51ZM/MDP/177/2
//
//
//    2.c) 
//    If there are no inserts move the images into the right directories:
//    /data/storage/ASFI-ChIJrdbSgKZWKhMRAyrH7xd51ZM/MDP/1
//
//    Create in the MIA db an upload every 30 files pertaining to that volume/insert
//
//    While creating the uploads (tblUpload entry then tblUploadedFiles), number the folios using following the filename specifications (using field tblUploadedFiles.fileOriginalName):
//
//
//
//    For each record in tblUploadedFiles create one record in tblUploadFilesFolios using tblUploadedFiles.fileOriginalName splitted. 
//
//    Above example in tblUploadsFilesFolios:
//
//    Id		uploadFileId		folioNumber		rectoverso	noNumb
//    Auto		$idothertable		994 BIS		recto		0
//
//
//
//
//    N.b. all folios are NOT double folio so each image has only one entry in the tblUploadFilesFolios.
//
//
//    Progressive number: 
//    Use this for the order of images in the upload.
//
//    Insert number can also be not present.
//    If present put the images inside the insert.
//
//    Carta type can be:
//    C=normal folio
//
//    If  A=allegato (the folio number will be Allegato 1) 
//
//    If R=rubricario (the folio number will be Rubricario 1)
//
//    If G=guardia (the folio number will be Guardia 1)
//
//    If SPI=spine (0003_SPI.tif ) (SPINE) - set as spine
//
//    If COPERTA=coperta (the folio number will be Coperta 1)
//
//    Folio number: 
//                               
//    Missed numbering:
//    If present the folio number is 994 BIS
//
//    Recto of Verso: rectoverso
    public void process() {
    	//Logica
    	//Path MDP177 che existing 
    	//String sourcePath = "/data/fromBia/TILED/MDP177";
    	try {
    		started = true;
		SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(userDetail,  userDetail.getAuthorities()));

		
    	File folder = new File(sourcePath);
    	
    	String volumeName = sourcePath.substring(sourcePath.lastIndexOf("/") + 1);
    	
    	String volumeId = volumeName.replaceFirst(MDP, "");
    	
    	String repositoryId = "1";
 
    	 //Mediceo del Principato
    	
    	//Repository
    	RepositoryEntity repository = uploadService.findRepositoryById(Integer.valueOf(repositoryId));
    	
    	CollectionEntity collectionEntity = null;
    	List<CollectionEntity> collEntity = uploadService.findAllCollections(Integer.valueOf(repositoryId));
    	for (CollectionEntity collectionEntity2 : collEntity) {
    		if (collectionEntity2.getCollectionAbbreviation().equalsIgnoreCase(MDP)) {
    			collectionEntity = collectionEntity2;
    			break;
    		}
		}
    	
    	// Cerca il summaryid esiste se esiste.
    	Volume volume = new Volume();
		volume.setVolume(volumeId);
		volume.setCollection(Integer.valueOf(collectionEntity.getCollectionId()));
		volume.setCollectionEntity(collectionEntity);

		Volume volumedb = volBaseService.getVolumeByVolumeNoAndCollectionId(volume);
		boolean existVolume = volumedb != null;
		if (!existVolume) {
			volBaseService.addNewVolume(volume);
		} else {
			volume = volumedb;
		}
		
		// Series
		SeriesEntity serEntity = new SeriesEntity();
		serEntity.setSeriesId(null);
		serEntity.setTitle("");
		serEntity.setSubtitle1("");
		
    	//Controlla se ci sono tutti i file corrispondenti
    	
    	if (!folder.exists()) {
    		writeError ("The path don't exist: " + folder.getAbsolutePath() );
            return;
        }
    	//if (checkStructureFile(folder)) {
    	//	return;
    	//}
    	Comparator<File> comparator = new Comparator<File>() {
            @Override
            public int compare(File f1, File f2){
            	 return f1.getName().compareTo(f2.getName());
            }
        };
    	
        boolean hasFiles = false;
    	HashMap<String, List<File>> insertsList = hasInsertFile(folder);
    	Set<String> insertKeys = insertsList.keySet();
    	for (Iterator iterator = insertKeys.iterator(); iterator.hasNext();) {
			String insertId = (String) iterator.next();
			//Se non esiste l'inserto allo lo inserisco
			//INIZIO AGGIUNTA INSERTO SE NON ESISTE
			InsertEntity insert = new InsertEntity();
			insert.setInsertName(insertId);
			insert.setVolume(volume.getSummaryId());
			insert.setVolumeEntity(volume);
			boolean existInsert = volumeInsertDescriptionService.getInsertByVolumeAndInsertName(insert) != null;
			if (!existInsert) {
				volumeInsertDescriptionService.addNewInsert(insert);
			}
			Collections.sort(insertsList.get(insertId), comparator);
			//FINE  AGGIUNTA INSERTO SE NON ESISTE
			List<List<File>> filesUploadInsert = chopped(insertsList.get(insertId),30);
			for (int i = 0; i< filesUploadInsert.size(); i++) {
	    		
				//Test files is valid
				String error = testFiles(filesUploadInsert.get(i));
				if (error != null) {
					writeError (error);
					return;
				}
				//Nuovo upload
				UploadBean uploadBean = createUploadBean( repository, collectionEntity,  volume,  insert, serEntity, filesUploadInsert.get(i));
				
				uploadBean = uploadService.insertUploadImage(uploadBean);
				
				hasFiles = true;
				// Cancella i file appena spostati				
				deleteFiles(filesUploadInsert.get(i));

	    	}
			
		}
        //Get Insert
    	
        // il nome ha delle parentesi quadre allora crea l'inserto
        //Crea cartella volume e inserto di destinazione
    	
    	List<File> spineFiles = getSpineFile(folder);
    	
    	if (spineFiles.size() >0 ) {
    		if (spineFiles.size()  == 1) {
    	   		//Nuovo upload spine
    			UploadBean uploadBean = createUploadBean( repository,  collectionEntity, volume,  null, serEntity, spineFiles);
    			
    			uploadBean.setOptionalImage("spine");
    			String error = testFiles(spineFiles);
				if (error != null) {
					writeError (error);
					return;
				}
    			uploadBean = uploadService.insertUploadOptionalFile(uploadBean);
				
    			hasFiles = true;
				// Cancella i file appena spostati				
				deleteFiles(spineFiles);
				
    		} else {
    			//Errore caricamento
    			writeError ("Error more files SPINE for this path " + sourcePath );
                return;
    		}
 
    	}
    	
    	List<List<File>> volumeFiles = getVolumeFile( folder);
    	
    	for (int i = 0; i< volumeFiles.size(); i++) {
    		//Nuovo upload
			String error = testFiles(volumeFiles.get(i));
			if (error != null) {
				writeError (error);
				return;
			}
			//Carica i volume

			UploadBean uploadBean = createUploadBean( repository, collectionEntity,  volume,  new InsertEntity(), serEntity, volumeFiles.get(i));

			
			uploadBean = uploadService.insertUploadImage(uploadBean);
			
			hasFiles = true;
			// Cancella i file appena spostati				
			deleteFiles(volumeFiles.get(i));
			
    	}
    	if (hasFiles) {
    		cleanFolder(folder);
    	}
    	}catch (Exception e ) {
    		writeError ("Error: " +  e.getMessage()) ;
    	}
    	finally {
    		started = false;
			volBaseService.updateAECount();
			try {

				if (errorFile != null)
					errorFile.close();

			} catch (IOException ex) {
				ex.printStackTrace();
			}
    	}
    	
    }
    
    private void writeError (String content) {
    	BufferedWriter bw = new BufferedWriter(errorFile);
    	try {
			bw.write(content + "\n");
			bw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bw.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
    	
    }

    
    private <T> List<List<T>> chopped(List<T> list, final int L) {
        List<List<T>> parts = new ArrayList<List<T>>();
        final int N = list.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<T>(
                list.subList(i, Math.min(N, i + L)))
            );
        }
        return parts;
    }

    //Check files
    private boolean cleanFolder(File folder) throws IOException {
      	String[] tifextention = new String[] { "tif" };
        Collection<File> filesTifs = FileUtils.listFiles(folder, tifextention , false);
    	if (filesTifs.size() > 0) {
    		writeError ("Folder not empty: " +  folder.getAbsolutePath() ) ;
    		return false;
    	}
    	else {
    		FileUtils.deleteDirectory(folder);
    	}
        return true;
    }
    
    //Check files
    private boolean checkStructureFile(File folder) {
    	String[] tifextention = new String[] { "tif" };
        Collection<File> filesTifs = FileUtils.listFiles(folder, tifextention , false);
        for (File file : filesTifs) {
            if (file.isFile()) {
            	//Lista di tutti i file
            	//file.getCanonicalPath();
            	String file3 = file.getParentFile().getAbsolutePath() + File.separator + org.medici.mia.common.util.FileUtils.THUMB_FILES_DIR +  File.separator + file.getName().replaceFirst(".tif", ".jpg");
            	File fileT = new File(file3) ;
            	if (!fileT.exists()) {
            		//TODO errore File file3 non esiste
            		return false;
            	}
            	String file4 = file.getParentFile().getAbsolutePath() + File.separator + org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR +  File.separator + file.getName().replaceFirst(".tif", ".jpg");
            	File fileO = new File(file4) ;
            	if (!fileO.exists()) {
            		//TODO errore File file4 non esiste
            		return false;
            	}
            	
            	//Controllo presenza dei file corrispondenti in //originalFiles e miniature altrimenti logga
            }
        }
        return true;
    }
    
    private File getJpgFromTif (File file, String type) {
    	String file4 = file.getParentFile().getAbsolutePath() + File.separator + type +  File.separator + file.getName().replaceFirst(".tif", ".jpg");
    	File fileO = new File(file4) ;
    	return fileO;
    }
    
    private void moveFiles(File source, File dest) {
    	//Copia tiff
  
    	File originalFile = getJpgFromTif (source, org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR);
    	File thumbFile = getJpgFromTif (source, org.medici.mia.common.util.FileUtils.THUMB_FILES_DIR);
    	File destthumbFile = getJpgFromTif (dest, org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR);
    	File destoriginalFile = getJpgFromTif (dest, org.medici.mia.common.util.FileUtils.THUMB_FILES_DIR);
    	source.renameTo(dest);
    	originalFile.renameTo(destoriginalFile);
    	thumbFile.renameTo(destthumbFile);
    }
    private void deleteFiles(List<File> source) {
    	//Copia tiff
    	for (File file : source) {
    		File originalFile = getJpgFromTif (file, org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR);
        	File thumbFile = getJpgFromTif (file, org.medici.mia.common.util.FileUtils.THUMB_FILES_DIR);
        	file.delete();
        	originalFile.delete();
        	thumbFile.delete();
		} 
    }
    
    private String testFiles(List<File> source) {
    	//Copia tiff
    	for (File file : source) {
    		File originalFile = getJpgFromTif (file, org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR);
        	File thumbFile = getJpgFromTif (file, org.medici.mia.common.util.FileUtils.THUMB_FILES_DIR);
        	if (!originalFile.exists()) {
        		return "File don't exist : " + originalFile.getAbsolutePath();
        	}
        	if (!thumbFile.exists()) {
        		return "File don't exist : " + thumbFile.getAbsolutePath();
        	}
        	String nome = file.getName();
        	if (!(nome.indexOf("_C_") >= 0 || nome.indexOf("_A_") >= 0
        			|| nome.indexOf("_R_") >= 0 || nome.indexOf("_G_") >= 0 
        			|| nome.indexOf("_SPI") >= 0 || nome.indexOf("_COPERTA") >= 0)) {
        		return "File don't have a supported folio type (C, A, R, G, SPI, COPERTA) : " + thumbFile.getAbsolutePath();
        	}
      
		} 
    	return null;
    }
    
    private HashMap<String, List<File>> hasInsertFile(File folder) {
    	HashMap<String, List<File>> filesInsert = new HashMap<String, List<File>>();
    	String[] tifextention = new String[] { "tif" };
        Collection<File> filesTifs = FileUtils.listFiles(folder, tifextention , false);
        for (File file : filesTifs) {
            if (file.isFile()) {
            	//Lista di tutti i file
            	//file.getCanonicalPath();
            	if (file.getName().indexOf("[") >= 0 && file.getName().indexOf("]") >= 0) {
            		String insertId = file.getName().substring(file.getName().indexOf("[")+1, file.getName().indexOf("]"));
            		List<File> files = filesInsert.get(insertId);
            		if (files == null) {
            			files = new ArrayList<File>();
            		}
            		files.add(file);
            		filesInsert.put(insertId, files);
            	}
            }
        }
        return filesInsert;
    }

    private List<List<File>> getVolumeFile(File folder) {
    	List<File> filesVolume = new ArrayList<File>();
    	String[] tifextention = new String[] { "tif" };
        Collection<File> filesTifs = FileUtils.listFiles(folder, tifextention , false);

        
        Comparator<File> comparator = new Comparator<File>() {
            @Override
            public int compare(File f1, File f2){
            	 return f1.getName().compareTo(f2.getName());
            }
        };

        for (File file : filesTifs) {
            if (file.isFile()) {
            	//Lista di tutti i file
            	//file.getCanonicalPath();
            	if (!(file.getName().indexOf("[") >= 0 && file.getName().indexOf("]") >= 0) &&
            			!(file.getName().indexOf("SPI") >= 0 )) {        		
            		filesVolume.add(file);
            	}
            }
        }
        Collections.sort(filesVolume, comparator);
        return chopped(filesVolume,30);
    }
    
    private List<File> getSpineFile(File folder) {
    	List<File> filesVolume = new ArrayList<File>();
    	String[] tifextention = new String[] { "tif" };
        Collection<File> filesTifs = FileUtils.listFiles(folder, tifextention , false);
        for (File file : filesTifs) {
            if (file.isFile()) {
            	//Lista di tutti i file
            	//file.getCanonicalPath();
            	if (file.getName().indexOf("SPI") >= 0 ) {        		
            		filesVolume.add(file);
            	}
            }
        }
        return filesVolume;
    }

    
    public int getCurrentStep() {
		return currentStep;
	}
    public int getTotalFile() {
		return totalFile;
	}

    public void setStarted(boolean started) {
        this.started = started;
    }

    public boolean isStarted() {
        return started;
    }
    
    private UploadBean createUploadBean(RepositoryEntity repository, CollectionEntity collection, Volume volume, InsertEntity insert, SeriesEntity serie, List<File> uploadFile) {
		User user = userService.findUser(
				((UserDetails) SecurityContextHolder.getContext()
						.getAuthentication().getPrincipal()).getUsername());


		List<MultipartFile> listFile = new ArrayList<MultipartFile>();
		
		String owner = user.getAccount();

		UploadBean uploadBean = new UploadBean(listFile, null,
				repository, collection, serie, volume, insert, owner,
				"");
		uploadBean.setUploadfiles(uploadFile);
		return uploadBean;
    }

}
