/*
 * ImageBiaMiaController.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.controller.checks;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.apache.xmlbeans.impl.jam.internal.elements.SourcePositionImpl;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.VolumeDescriptionJson;
import org.medici.mia.common.util.UserRoleUtils;
import org.medici.mia.dao.user.UserDAO;
import org.medici.mia.dao.userrole.UserRoleDAO;
import org.medici.mia.domain.Document;
import org.medici.mia.domain.User;
import org.medici.mia.domain.UserRole;
import org.medici.mia.service.bia.DocumentImporter;
import org.medici.mia.service.bia.ImageBiaMiaService;
import org.medici.mia.service.upload.UploadService;
import org.medici.mia.service.user.UserService;
import org.medici.mia.service.volbase.VolBaseService;
import org.medici.mia.service.volumeinsert.VolumeInsertDescriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.jmx.mbeanserver.GetPropertyAction;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 * 
 * @author alai
 *
 */
@Controller
@RequestMapping("/imageBiaMiaController")
public class ImageBiaMiaController {

	@Autowired
	private VolBaseService volBaseService;

	@Autowired
	private UploadService uploadService;
	
	@Autowired
	private UserService userService;
	@Autowired
	private org.medici.mia.security.MiaJpaUserDetailsServiceImpl miaJpaUserDetailsServiceImpl ;

	@Autowired
	private VolumeInsertDescriptionService volumeInsertDescriptionService;
	@Autowired
	private  DocumentImporter documentImporter;
	
	@Autowired
	ImageBiaMiaService imageBiaMiaService;

	@PersistenceContext
	private EntityManager entityManager;

	private ImageBiaMiaProcess imageBiaMiaProcess = null;
	//Lancia Thread
	//curl -H "Authorization: Basic $(echo -n pteam:medici.a | base64)" -H "Accept: application/json, text/plain, */*" -H "Content-Type: application/json" -X POST http://localhost:8181/Mia/json/imageBiaMiaController/start -d '{	"fileNameLog": "/Users/alai/testimport.log', "volumePath": "/Users/alai/Downloads/MDP3000", "wait": true}'
	//Parametro Path sorgente
	@RequestMapping(value = "start", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson start(@RequestBody BiaParamJson biaParamJson) throws InterruptedException {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		if (imageBiaMiaProcess != null && imageBiaMiaProcess.isStarted()) {
			resp.setMessage("Request is running.");
			return resp;
		}
		
		if (biaParamJson.getFileNameLog() == null) {
			resp.setMessage("Error log is null");
			resp.setStatus(StatusType.ko.toString());
			return resp;

		}

		if (biaParamJson.getFileNameLog() == null) {
			resp.setMessage("Error log is null");
			resp.setStatus(StatusType.ko.toString());
			return resp;

		}
		FileWriter errorfile = null;
		try {
			errorfile = new FileWriter (biaParamJson.getFileNameLog(), true);
		} catch(Exception e) {
			resp.setMessage("Error writing log");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}


		if (!new File(biaParamJson.getVolumePath()).exists()) {
			resp.setMessage("No volume path exist");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		UserDetails userDetail = miaJpaUserDetailsServiceImpl.loadUserByUsername("pteam");
		
		String volumeP = biaParamJson.getVolumePath();
		imageBiaMiaService.process(errorfile, userDetail, volumeP);

		resp.setMessage(volumeP + " imported!");

		resp.setStatus(StatusType.ok.toString());
		return resp;
		
	}
	
	
	//curl -H "Authorization: Basic $(echo -n pteam:medici.a | base64)" -H "Accept: application/json, text/plain, */*" -H "Content-Type: application/json" -X POST http://localhost:8181/Mia/json/imageBiaMiaController/document-load -d '{	"fileNameLog": "/Users/alai/testimport.log"}'
	//Parametro Path sorgente
	@RequestMapping(value = "document-load", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson documentLoad(@RequestBody BiaParamJson biaParamJson) throws InterruptedException {

		GenericResponseJson resp = new GenericResponseJson();
		resp.setStatus(StatusType.w.toString());
		resp.setMessage("Unknown errore.");
		
		
		if (biaParamJson.getFileNameLog() == null) {
			resp.setMessage("Error log is null");
			resp.setStatus(StatusType.ko.toString());
			return resp;

		}


		FileWriter errorfile = null;
		try {
			errorfile = new FileWriter (biaParamJson.getFileNameLog(), true);
		} catch(Exception e) {
			resp.setMessage("Error writing log");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		

		UserDetails userDetail = miaJpaUserDetailsServiceImpl.loadUserByUsername("pteam");
		
		try {
			documentImporter.updateMiaDocuments();
			List<Document> docsBia = documentImporter.findDocumentsImportLock();
			for (Document documentBia : docsBia) {
				try {
					documentImporter.insertDocument(errorfile, documentBia);
				} catch (Exception e ) {
					errorfile.append("Errore nell'update del documento con ID: "+ documentBia.getEntryId() + ", con errore:  " +  ((e.getMessage()!= null) ? e.getMessage() :e.getStackTrace().toString() )  + "\n");
					errorfile.flush();
				}
			}
		} catch (IOException e) {
			resp.setMessage("Error saving documents log");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		} catch (Exception e) {
			resp.setMessage("Error insertDocument " + e.getMessage());
			resp.setStatus(StatusType.ko.toString());
			e.printStackTrace();
			return resp;
		} finally {
			if(errorfile != null) {
				try {
					errorfile.close();
				} catch (IOException e) {
					resp.setMessage("Error writing log");
					resp.setStatus(StatusType.ko.toString());
					return resp;
				}
			}
		}
	
		resp.setMessage("Document imported");

		resp.setStatus(StatusType.ok.toString());
		return resp;
		
	}
	

}
