package org.medici.mia.controller.titleocc;

import java.util.List;

import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.biographical.BiographicalPeopleJson;
import org.medici.mia.common.json.titleocc.ModifyTitleOccRoleJson;
import org.medici.mia.common.json.titleocc.RoleCatJson;
import org.medici.mia.common.json.titleocc.TitleOccRoleJson;
import org.medici.mia.service.titleocc.TitleAndOccupationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author User
 *
 */
@Controller
@RequestMapping("/titleAndOccupation")
public class TitleAndOccupationController {

	@Autowired
	private TitleAndOccupationService titleAndOccupationService;

	// http://localhost:8181/Mia/json/titleAndOccupation/findTitleAndOccupation/4868
	@RequestMapping(value = "findTitleAndOccupation/{titleOccId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<TitleOccRoleJson> findTitleAndOccupation(
			@PathVariable("titleOccId") Integer titleOccId) {
		GenericResponseDataJson<TitleOccRoleJson> resp = new GenericResponseDataJson<TitleOccRoleJson>();
		TitleOccRoleJson ppl = getTitleAndOccupationService()
				.findTitleAndOccupation(titleOccId);
		if (ppl != null) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got 1 title and occupation in total with id "
					+ titleOccId);
			resp.setData(ppl);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No title and occupation found with " + titleOccId);
			return resp;
		}

	}

	// http://localhost:8181/Mia/json/titleAndOccupation/addNewTitleOccupation/
	@RequestMapping(value = "addNewTitleOccupation", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson addNewTitleOccupation(
			@RequestBody ModifyTitleOccRoleJson modifyTitleOccRoleJson) {

		GenericResponseJson resp = new GenericResponseJson();
		if (modifyTitleOccRoleJson == null
				|| modifyTitleOccRoleJson.getNewTitleOccRoleJson() == null) {
			resp.setMessage("title and occ may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		return getTitleAndOccupationService().addNewTitleOccupation(
				modifyTitleOccRoleJson.getNewTitleOccRoleJson());
	}

	@RequestMapping(value = "modifyTitleOccupation", method = RequestMethod.POST)
	public @ResponseBody GenericResponseJson modifyTitleOccupation(
			@RequestBody ModifyTitleOccRoleJson modifyTitleOccRoleJson) {

		GenericResponseJson resp = new GenericResponseJson();
		if (modifyTitleOccRoleJson == null
				|| modifyTitleOccRoleJson.getNewTitleOccRoleJson() == null
				|| modifyTitleOccRoleJson.getOldTitleOccRoleJson() == null
				|| modifyTitleOccRoleJson.getTitleOccId() == null
				|| modifyTitleOccRoleJson.getTitleOccId().equals("")) {
			resp.setMessage("Data may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		
		resp = getTitleAndOccupationService().modifyTitleOccupation(
				modifyTitleOccRoleJson);

		return resp;
	}
    // ISSUE 140
	// http://localhost:8181/Mia/json/titleAndOccupation/findPersonsByTitleOcc/9802
	@RequestMapping(value = "findPersonsByTitleOcc/{titleOccId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<Integer>> findPersonsByTitleOcc(
			@PathVariable("titleOccId") Integer titleOccId) {

		GenericResponseDataJson<List<Integer>> resp = new GenericResponseDataJson<List<Integer>>();
		if (titleOccId == null || titleOccId.equals("")) {
			resp.setMessage("titleOccId field may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}

		List<Integer> persons = getTitleAndOccupationService()
				.findPersonsByTitleOcc((titleOccId));
		if (persons == null || persons.isEmpty()) {
			resp.setMessage("No persons use this title. Title can be deleted.");
			resp.setStatus(StatusType.ok.toString());
			return resp;
		}
		resp.setMessage(" persons are usind this title. Title can't be deleted.");
		resp.setStatus(StatusType.ok.toString());
		resp.setData(persons);

		return resp;

	}

	// http://localhost:8181/Mia/json/titleAndOccupation/deleteTitleOccupation/9802
	@RequestMapping(value = "deleteTitleOccupation/{titleOccId}", method = RequestMethod.GET)
	public @ResponseBody GenericResponseJson deleteTitleOccupation(
//	public @ResponseBody GenericResponseDataJson<List<BiographicalPeopleJson>> deleteTitleOccupation(
			@PathVariable("titleOccId") Integer titleOccId) {

		GenericResponseJson resp = new GenericResponseJson();
		if (titleOccId == null || titleOccId.equals("")) {
			resp.setMessage("titleOccId field may be null or empty.");
			resp.setStatus(StatusType.ko.toString());
			return resp;
		}
		return getTitleAndOccupationService().deleteTitleOccupation(titleOccId);
	}

	// http://localhost:8181/Mia/json/titleAndOccupation/findRoleCategories/
	@RequestMapping(value = "findRoleCategories", method = RequestMethod.GET)
	public @ResponseBody GenericResponseDataJson<List<RoleCatJson>> findRoleCategories() {
		GenericResponseDataJson<List<RoleCatJson>> resp = new GenericResponseDataJson<List<RoleCatJson>>();
		List<RoleCatJson> roleCats = getTitleAndOccupationService()
				.findRoleCategories();
		if (roleCats != null && !roleCats.isEmpty()) {
			resp.setStatus(StatusType.ok.toString());
			resp.setMessage("Got " + roleCats.size() + " Role Categories.");
			resp.setData(roleCats);
			return resp;
		} else {
			resp.setStatus(StatusType.ko.toString());
			resp.setMessage("No Role Categories found in DB. ");
			return resp;
		}

	}

	public TitleAndOccupationService getTitleAndOccupationService() {
		return titleAndOccupationService;
	}

	public void setTitleAndOccupationService(
			TitleAndOccupationService titleAndOccupationService) {
		this.titleAndOccupationService = titleAndOccupationService;
	}

}