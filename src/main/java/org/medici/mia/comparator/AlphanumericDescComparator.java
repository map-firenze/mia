/*
 * AlphanumericComparator.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.comparator;

import java.util.Comparator;

/**
 * 
 * @author mbarlotti
 *
 */
public class AlphanumericDescComparator extends AlphanumericComparator {

	@Override
	public int compare(String a, String b) {
		Double bNumber = 0D;
		Double aNumber = 0D;
		String partA = a.replaceAll("[^0-9]", "");
		String partB = b.replaceAll("[^0-9]", "");
		String firstLetterA = a.equals("") ? "A" : String.valueOf(a.charAt(0));
		String firstLetterB = b.equals("") ? "A" : String.valueOf(b.charAt(0));

		if (isNumber(firstLetterA) == false && isNumber(firstLetterB) == true) {
			return 1;
		}
		if (isNumber(firstLetterA) == true && isNumber(firstLetterB) == false) {
			return -1;
		}
		if (isNumber(firstLetterA) == false && isNumber(firstLetterB) == false) {
			return b.compareTo(a);
		}
		if (partA.length() > 0) {
			aNumber = Double.valueOf(partA);
		}
		if (partB.length() > 0) {
			bNumber = Double.valueOf(partB);
		}
		if (aNumber == 0 && bNumber != 0) {
			return 1;
		}
		if (aNumber != 0 && bNumber == 0) {
			return -1;
		}
		if (aNumber < bNumber) {
			return 1;
		} else if (aNumber > bNumber) {
			return -1;
		} else {
			return b.compareTo(a);
		}
	}

}
