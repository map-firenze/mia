/*
 * HistoryLogJsonArchivalLocationDescComparator.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.comparator;

import java.util.Comparator;

import org.medici.mia.common.json.CollectionJson;
import org.medici.mia.common.json.InsertDescriptionJson;
import org.medici.mia.common.json.VolumeDescriptionJson;
import org.medici.mia.common.json.historylog.HistoryLogJson;

/**
 * 
 * @author mbarlotti
 *
 */
public class HistoryLogJsonArchivalLocationDescComparator implements Comparator<HistoryLogJson> {

	@Override
	public int compare(HistoryLogJson o1, HistoryLogJson o2) {
		return compareCollection(o1, o2);
	}

	private int compareCollection(HistoryLogJson o1, HistoryLogJson o2) {
		String collectionAbbreviation1 = o1.getCollectionAbbreviation();
		String collectionAbbreviation2 = o2.getCollectionAbbreviation();
		if (collectionAbbreviation1 != null && collectionAbbreviation2 != null) {
			if (collectionAbbreviation1.equals(collectionAbbreviation2)) {
				return compareVolume(o1, o2);
			} else {
				return new AlphanumericDescComparator().compare(collectionAbbreviation1, collectionAbbreviation2);
			}
		} else if (collectionAbbreviation1 == null && collectionAbbreviation2 != null) {
			return -1;
		} else if (collectionAbbreviation1 != null && collectionAbbreviation2 == null) {
			return 1;
		}
		return compareVolume(o1, o2);
	}

	private int compareVolume(HistoryLogJson o1, HistoryLogJson o2) {
		String volumeNo1 = o1.getVolumeNo();
		String volumeNo2 = o2.getVolumeNo();
		if (volumeNo1 != null && volumeNo2 != null) {
			if (volumeNo1.toLowerCase().equals(volumeNo2.toLowerCase())) {
				return compareInsert(o1, o2);
			} else {
				return new AlphanumericDescComparator().compare(volumeNo1.toLowerCase(), volumeNo2.toLowerCase());
			}
		} else if (volumeNo1 == null && volumeNo2 != null) {
			return -1;
		} else if (volumeNo1 != null && volumeNo2 == null) {
			return 1;
		}
		return compareInsert(o1, o2);
	}

	private int compareInsert(HistoryLogJson o1, HistoryLogJson o2) {
		String insertNo1 = o1.getInsertNo();
		String insertNo2 = o2.getInsertNo();
		if (insertNo1 != null && insertNo2 != null) {
			if (insertNo1.toLowerCase().equals(insertNo2.toLowerCase())) {
				return compareFolio(o1, o2);
			} else {
				return new AlphanumericDescComparator().compare(insertNo1.toLowerCase(), insertNo2.toLowerCase());
			}
		} else if (insertNo1 == null && insertNo2 != null) {
			return -1;
		} else if (insertNo1 != null && insertNo2 == null) {
			return 1;
		}
		return compareFolio(o1, o2);
	}

	private int compareFolio(HistoryLogJson o1, HistoryLogJson o2) {
		String folioNumber1 = o1.getFolioNumber();
		String folioNumber2 = o2.getFolioNumber();
		if (folioNumber1 != null && folioNumber2 != null) {
			return new AlphanumericDescComparator().compare(folioNumber1, folioNumber2);
		} else if (folioNumber1 == null && folioNumber2 != null) {
			return -1;
		} else if (folioNumber1 != null && folioNumber2 == null) {
			return 1;
		}
		return 0;
	}

}
