/*
 * ArchivalLocationComparator.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.comparator;

import java.util.Comparator;
import java.util.List;

import org.medici.mia.common.json.CollectionJson;
import org.medici.mia.common.json.InsertDescriptionJson;
import org.medici.mia.common.json.UploadFileJson;
import org.medici.mia.common.json.VolumeDescriptionJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.document.GenericDocumentJson;
import org.medici.mia.common.json.folio.FolioJson;

/**
 * 
 * @author mbarlotti
 *
 */
public class DocumentJsonArchivalLocationDescComparator implements Comparator<DocumentJson> {

	@Override
	public int compare(DocumentJson doc1, DocumentJson doc2) {
		GenericDocumentJson o1 = (GenericDocumentJson) doc1;
		GenericDocumentJson o2 = (GenericDocumentJson) doc2;

		return compareCollection(o1, o2);
	}

	private int compareCollection(GenericDocumentJson o1, GenericDocumentJson o2) {
		CollectionJson collection1 = o1.getCollection();
		CollectionJson collection2 = o2.getCollection();
		if (collection1 != null && collection2 != null) {
			String collectionAbbreviation1 = collection1.getCollectionAbbreviation();
			String collectionAbbreviation2 = collection2.getCollectionAbbreviation();
			if (collectionAbbreviation1.equals(collectionAbbreviation2)) {
				return compareVolume(o1, o2);
			} else {
				return new AlphanumericDescComparator().compare(collectionAbbreviation1, collectionAbbreviation2);
			}
		} else if (collection1 == null && collection2 != null) {
			return -1;
		} else if (collection1 != null && collection2 == null) {
			return 1;
		}
		return compareVolume(o1, o2);
	}

	private int compareVolume(GenericDocumentJson o1, GenericDocumentJson o2) {
		VolumeDescriptionJson volume1 = o1.getVolume();
		VolumeDescriptionJson volume2 = o2.getVolume();
		if (volume1 != null && volume2 != null) {
			String volumeNo1 = o1.getVolume().getVolumeNo().toLowerCase();
			String volumeNo2 = o2.getVolume().getVolumeNo().toLowerCase();
			if (volumeNo1.equals(volumeNo2)) {
				return compareInsert(o1, o2);
			} else {
				return new AlphanumericDescComparator().compare(volumeNo1, volumeNo2);
			}
		} else if (volume1 == null && volume2 != null) {
			return -1;
		} else if (volume1 != null && volume2 == null) {
			return 1;
		}
		return compareInsert(o1, o2);
	}

	private int compareInsert(GenericDocumentJson o1, GenericDocumentJson o2) {
		InsertDescriptionJson insert1 = o1.getInsert();
		InsertDescriptionJson insert2 = o2.getInsert();
		if (insert1 != null && insert2 != null) {
			String insertNo1 = insert1.getInsertNo().toLowerCase();
			String insertNo2 = insert2.getInsertNo().toLowerCase();
			if (insertNo1.equals(insertNo2)) {
				return compareFolio(o1, o2);
			} else {
				return new AlphanumericDescComparator().compare(insertNo1, insertNo2);
			}
		} else if (insert1 == null && insert2 != null) {
			return -1;
		} else if (insert1 != null && insert2 == null) {
			return 1;
		}
		return compareFolio(o1, o2);
	}

	private int compareFolio(GenericDocumentJson o1, GenericDocumentJson o2) {
		List<UploadFileJson> uploadFiles1 = o1.getUploadFiles();
		List<UploadFileJson> uploadFiles2 = o2.getUploadFiles();
		if (!uploadFiles1.isEmpty() && !uploadFiles2.isEmpty()) {
			UploadFileJson uploadFile1 = uploadFiles1.get(0);
			UploadFileJson uploadFile2 = uploadFiles2.get(0);
			List<FolioJson> folios1 = uploadFile1.getFolios();
			List<FolioJson> folios2 = uploadFile2.getFolios();
			if (!folios1.isEmpty() && !folios2.isEmpty()) {
				FolioJson folioJson1 = folios1.get(0);
				FolioJson folioJson2 = folios2.get(0);
				if (folioJson1 != null && folioJson2 != null) {
					String folioNumber1 = folioJson1.getFolioNumber();
					String folioNumber2 = folioJson2.getFolioNumber();
					if (folioNumber1 != null && folioNumber2 != null) {
						return new AlphanumericDescComparator().compare(folioNumber1, folioNumber2);
					} else if (folioNumber1 == null && folioNumber2 != null) {
						return -1;
					} else if (folioNumber1 != null && folioNumber2 == null) {
						return 1;
					}
				} else if (folioJson1 == null && folioJson2 != null) {
					return -1;
				} else if (folioJson1 != null && folioJson2 == null) {
					return 1;
				}
			} else if (folios1.isEmpty() && !folios2.isEmpty()) {
				return -1;
			} else if (!folios1.isEmpty() && folios2.isEmpty()) {
				return 1;
			}
		} else if (uploadFiles1.isEmpty() && !uploadFiles2.isEmpty()) {
			return -1;
		} else if (!uploadFiles1.isEmpty() && uploadFiles2.isEmpty()) {
			return 1;
		}
		return 0;
	}

}
