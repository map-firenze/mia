/*
 * DocumentExplorer.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.common.pagination;

/**
 * ArchivalEntityExplorer contains informations about an archival entity.
 * 
 */
public class ArchivalEntityExplorer extends VolumeExplorer {
	private Integer archivalEntityId;
	private String pathToImage;
	private String imageCompleteName;
	private String repository;
	private String collection;
	private String series;
	private String insert;
	private String volume;
	private String folio;
	private Integer imagePosition;
	private String nextPage;
	private String previousPage;
	private String goToPage;
	private String error;
	private Integer documentCount;
	private String singleUploadPage;
	
	// fileId = imageId
	private Integer imageId;
	
	public Integer getArchivalEntityId() {
		return archivalEntityId;
	}

	public void setArchivalEntityId(Integer archivalEntityId) {
		this.archivalEntityId = archivalEntityId;
	}

	public String getPathToImage() {
		return pathToImage;
	}

	public void setPathToImage(String pathToImage) {
		this.pathToImage = pathToImage;
	}

	public String getImageCompleteName() {
		return imageCompleteName;
	}
	
	public void setImageCompleteName(String imageCompleteName) {
		this.imageCompleteName = imageCompleteName;
	}
	
	public String getRepository() {
		return repository;
	}

	public void setRepository(String repository) {
		this.repository = repository;
	}

	public String getCollection() {
		return collection;
	}

	public void setCollection(String collection) {
		this.collection = collection;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getInsert() {
		return insert;
	}

	public void setInsert(String insert) {
		this.insert = insert;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public Integer getImagePosition() {
		return imagePosition;
	}

	public void setImagePosition(Integer imagePosition) {
		this.imagePosition = imagePosition;
	}

	public String getNextPage() {
		return nextPage;
	}
	
	public void setNextPage(String nextPage) {
		this.nextPage = nextPage;
	}
	
	public String getPreviousPage() {
		return previousPage;
	}
	
	public void setPreviousPage(String previousPage) {
		this.previousPage = previousPage;
	}
	
	public Integer getImageId() {
		return imageId;
	}
	
	public void setImageId(Integer imageId) {
		this.imageId = imageId;
	}
	
	public String getGoToPage() {
		return goToPage;
	}
	
	public void setGoToPage(String goToPage) {
		this.goToPage = goToPage;
	}
	
	public String getError() {
		return error;
	}
	
	public void setError(String error) {
		this.error = error;
	}

	public Integer getDocumentCount() {
		return documentCount;
	}

	public void setDocumentCount(Integer documentCount) {
		this.documentCount = documentCount;
	}

	public String getSingleUploadPage() {
		return singleUploadPage;
	}

	public void setSingleUploadPage(String singleUploadPage) {
		this.singleUploadPage = singleUploadPage;
	}
	
}
