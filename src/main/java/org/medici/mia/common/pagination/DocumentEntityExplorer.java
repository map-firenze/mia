/*
 * DocumentExplorer.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.common.pagination;


/**
 * DocumentExplorer contains informations about a document.
 * 
 * @author Lorenzo Pasquinelli (<a href=mailto:l.pasquinelli@gmail.com>l.pasquinelli@gmail.com</a>)
 * @author Ronny Rinaldi (<a href=mailto:rinaldi.ronny@gmail.com>rinaldi.ronny@gmail.com</a>)
 *
 */
public class DocumentEntityExplorer extends VolumeExplorer {
	private Integer documentId;
	private Boolean linkedDocument;
	private String pathToImage;
	private String volume;
	private Integer imagePosition;
	private String repository;
	private String collection;
	private String folio;
	private String series;
	private String insert;
	
	public DocumentEntityExplorer() {
		super();
	}

	public DocumentEntityExplorer(Integer documentId, Integer volNum, String volLetExt) {
		super(volNum, volLetExt);
		setDocumentId(documentId);
	}

	public DocumentEntityExplorer(Integer documentId, Integer summaryId, Integer volNum, String volLetExt) {
		super(summaryId, volNum, volLetExt);
		setDocumentId(documentId);
	}


	public DocumentEntityExplorer(Integer documentEntityId, Integer volume) {
		this.setDocumentId(documentEntityId);
		this.setVolume((volume != null)?volume.toString():null);
	}

	/**
	 * @param documentId the documentId to set
	 */
	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	/**
	 * @return the documentId
	 */
	public Integer getDocumentId() {
		return documentId;
	}

	/**
	 * @param linkedDocument the linkedDocument to set
	 */
	public void setLinkedDocument(Boolean linkedDocument) {
		this.linkedDocument = linkedDocument;
	}

	/**
	 * @return the linkedDocument
	 */
	public Boolean getLinkedDocument() {
		return linkedDocument;
	}

	public String getPathToImage() {
		return pathToImage;
	}

	public void setPathToImage(String pathToImage) {
		this.pathToImage = pathToImage;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public Integer getImagePosition() {
		return imagePosition;
	}

	public void setImagePosition(Integer imagePosition) {
		this.imagePosition = imagePosition;
	}

	public String getRepository() {
		return repository;
	}

	public void setRepository(String repository) {
		this.repository = repository;
	}

	public String getCollection() {
		return collection;
	}

	public void setCollection(String collection) {
		this.collection = collection;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getInsert() {
		return insert;
	}

	public void setInsert(String insert) {
		this.insert = insert;
	}
}
