package org.medici.mia.common.json.search;

import java.io.Serializable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class GenericSearchJson implements Serializable {

	private static final long serialVersionUID = 3787731742866353474L;

	private String owner = null;
	private boolean onlyMyEntities;
	private boolean everyoneElsesEntities;
	private boolean allEntities;
	private String searchString;
	private String searchType;

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public boolean isOnlyMyEntities() {
		return onlyMyEntities;
	}

	public void setOnlyMyEntities(boolean onlyMyEntities) {
		this.onlyMyEntities = onlyMyEntities;
	}

	public boolean isEveryoneElsesEntities() {
		return everyoneElsesEntities;
	}

	public void setEveryoneElsesEntities(boolean everyoneElsesEntities) {
		this.everyoneElsesEntities = everyoneElsesEntities;
	}

	public boolean isAllEntities() {
		return allEntities;
	}

	public void setAllEntities(boolean allEntities) {
		this.allEntities = allEntities;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

}
