package org.medici.mia.common.json.document;

import java.io.Serializable;
import java.util.List;

import org.medici.mia.common.json.PeopleBaseJson;

public class FindPeopleForDocJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private String type;
	private List<PeopleBaseJson> peoples;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<PeopleBaseJson> getPeoples() {
		return peoples;
	}

	public void setPeoples(List<PeopleBaseJson> peoples) {
		this.peoples = peoples;
	}

}