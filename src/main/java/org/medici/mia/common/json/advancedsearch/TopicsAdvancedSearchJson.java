package org.medici.mia.common.json.advancedsearch;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.util.DateUtils;
import org.medici.mia.domain.User;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@JsonTypeName("topicsAdvancedSearch")
public class TopicsAdvancedSearchJson extends GenericAdvancedSearchJson {

	private static final long serialVersionUID = 1L;

	List<TopicSearchJson> topics;

	public List<TopicSearchJson> getTopics() {
		return topics;
	}

	public void setTopics(List<TopicSearchJson> topics) {
		this.topics = topics;
	}

	@JsonIgnore
	@Override
	public List<String> getQueries() {

		List<String> queries = new ArrayList<String>();
		if (getTopics() != null && !getTopics().isEmpty()) {
			for (TopicSearchJson topic : getTopics()) {
				StringBuffer b = new StringBuffer();
				b.append("select distinct d.documentEntityId documentId from tblTopicsList t , tblDocumentTopicPlace dtp, tblDocumentEnts d where d.documentEntityId = dtp.documentEntityId and TOPICTITLE = '");
				b.append(topic.getTopicTitle());
				b.append("'");
				b.append(" and t.TOPICID = dtp.topicListId ");

				if (topic.getPlaceAllId() != null
						&& !topic.getPlaceAllId().isEmpty()) {
					b.append("and dtp.placeId ='");
					b.append(topic.getPlaceAllId());
					b.append("'");
					b.append(" and  d.flgLogicalDelete != 1");
				}

				if (getNewsfeedUser() != null) {
					User newsfeedUser = getNewsfeedUser();
					b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
							DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
					b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
							DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
				}

				queries.add(b.toString());

			}

		}
		return queries;

	}
}