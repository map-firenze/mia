package org.medici.mia.common.json.casestudy;

import org.medici.mia.domain.CaseStudy;
import org.medici.mia.domain.User;

import java.io.Serializable;
import java.util.Date;

public class CreateCaseStudyJson implements Serializable {
    private String title;
    private String description;
    private String notes;

    public CaseStudy toEntity(User createdBy){
        return new CaseStudy(
                this.title,
                this.description,
                this.notes,
                new Date(),
                createdBy
        );
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
