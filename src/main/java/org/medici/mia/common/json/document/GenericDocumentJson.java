package org.medici.mia.common.json.document;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.medici.mia.common.json.CollectionJson;
import org.medici.mia.common.json.DateJson;
import org.medici.mia.common.json.FieldJson;
import org.medici.mia.common.json.InsertDescriptionJson;
import org.medici.mia.common.json.LanguageJson;
import org.medici.mia.common.json.PeopleJson;
import org.medici.mia.common.json.PlaceJson;
import org.medici.mia.common.json.RepositoryJson;
import org.medici.mia.common.json.UploadFileJson;
import org.medici.mia.common.json.VolumeDescriptionJson;
import org.medici.mia.common.json.folio.FolioJson;
import org.medici.mia.common.util.DocumentPrivacyUtils;
import org.medici.mia.common.util.FileUtils;
import org.medici.mia.common.util.FolioUtils;
import org.medici.mia.dao.fileSharing.FileSharingDAO;
import org.medici.mia.dao.uploadfile.UploadFileDAO;
import org.medici.mia.dao.uploadinfo.UploadInfoDAO;
import org.medici.mia.domain.BiblioRefEntity;
import org.medici.mia.domain.Collation;
import org.medici.mia.domain.CollectionEntity;
import org.medici.mia.domain.DocumentFieldEntity;
import org.medici.mia.domain.DocumentTranscriptionEntity;
import org.medici.mia.domain.FileSharing;
import org.medici.mia.domain.FolioEntity;
import org.medici.mia.domain.GoogleLocation;
import org.medici.mia.domain.LanguageEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.People;
import org.medici.mia.domain.RepositoryEntity;
import org.medici.mia.domain.TopicPlaceEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.UploadInfoEntity;
import org.medici.mia.service.collation.CollationService;
import org.medici.mia.service.miadoc.PeopleFieldType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * 
 * @author Shadab Bigdel
 *         (<a href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = CorrespondenceJson.class, name = "Correspondence"),
		@JsonSubTypes.Type(value = NotarialJson.class, name = "NotarialRecords"),
		@JsonSubTypes.Type(value = OfficialJson.class, name = "OfficialRecords"),
		@JsonSubTypes.Type(value = InventoryJson.class, name = "Inventories"),
		@JsonSubTypes.Type(value = FiscalJson.class, name = "FiscalRecords"),
		@JsonSubTypes.Type(value = FinancialJson.class, name = "FinancialRecords"),
		@JsonSubTypes.Type(value = MiscellaneousJson.class, name = "Miscellaneous"),
		@JsonSubTypes.Type(value = LiteraryJson.class, name = "LiteraryWorks"),
		@JsonSubTypes.Type(value = NewsJson.class, name = "News") })
public class GenericDocumentJson implements DocumentJson, Serializable {

	@Autowired
	private CollationService collationService;

	@Autowired
	@JsonIgnore
	private FileSharingDAO fileSharingDao;

	@Autowired
	@JsonIgnore
	private UploadInfoDAO uploadInfoDAO;

	@Autowired
	@JsonIgnore
	private UploadFileDAO uploadFileDAO;

	private static final long serialVersionUID = 1L;

	private final transient SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private Integer documentId;

	@JsonProperty("category")
	private String category;

	@JsonProperty("typology")
	private String typology;

	private Integer privacy;

	private Integer flgPrinted;

	private Integer flgLogicalDelete;

	@JsonProperty("date")
	private DateJson date;

	@JsonProperty("deTitle")
	private String deTitle;

	private List<PeopleJson> producers;

	private List<PlaceJson> placeOfOrigin;

	private List<UploadFileJson> uploadFiles;

	private List<LanguageJson> languages;

	private List<PeopleJson> referredToPeople;

	private List<RelatedDocumentJson> relatedDocs;

	private List<BiblioRefJson> bibliographicReferences;

	private String generalNoteSynopsis;
	
	private String chineseSynopsis;

	private List<TopicPlaceJson> topics;

	private List<DocumentTranscriptionJson> transcriptions;

	private List<String> collections;

	private String dateCreated;

	private String dateLastUpdated;

	private String owner;

	private Boolean hasAttachment;

	private Boolean hasLacuna;

	private List<HashMap> attachments = new ArrayList<HashMap>();

	private HashMap lacuna;

	private Integer commentsCount;

	private Integer volumeId;

	private Integer insertId;

	private Boolean canView;

	private Boolean canDeleteAndChangePrivacy;
	
	private Boolean canModify;

	private VolumeDescriptionJson volume;

	private InsertDescriptionJson insert;

	private CollectionJson collection = new CollectionJson();
	
	private RepositoryJson repository = new RepositoryJson();

	public UploadFileDAO getUploadFileDAO() {
		return uploadFileDAO;
	}

	public void setUploadFileDAO(UploadFileDAO uploadFileDAO) {
		this.uploadFileDAO = uploadFileDAO;
	}
	
	public Boolean getCanDeleteAndChangePrivacy() {
		return canDeleteAndChangePrivacy;
	}

	public void setCanDeleteAndChangePrivacy(Boolean canDeleteAndChangePrivacy) {
		this.canDeleteAndChangePrivacy = canDeleteAndChangePrivacy;
	}

	public FileSharingDAO getFileSharingDao() {
		return fileSharingDao;
	}

	public void setFileSharingDao(FileSharingDAO fileSharingDao) {
		this.fileSharingDao = fileSharingDao;
	}

	public Boolean getCanView() {
		return canView;
	}

	public void setCanView(Boolean canView) {
		this.canView = canView;
	}

	public GenericDocumentJson() {
	}

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	@Override
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getTypology() {
		return typology;
	}

	public void setTypology(String typology) {
		this.typology = typology;
	}

	public Integer getPrivacy() {
		return privacy;
	}

	public void setPrivacy(Integer privacy) {
		this.privacy = privacy;
	}

	public Integer getFlgPrinted() {
		return flgPrinted;
	}

	public void setFlgPrinted(Integer flgPrinted) {
		this.flgPrinted = flgPrinted;
	}

	public DateJson getDate() {
		return date;
	}

	public void setDate(DateJson date) {
		this.date = date;
	}

	public String getDeTitle() {
		return deTitle;
	}

	public void setDeTitle(String deTitle) {
		this.deTitle = deTitle;
	}

	public List<PeopleJson> getProducers() {
		return producers;
	}

	public void setProducers(List<PeopleJson> producers) {
		this.producers = producers;
	}

	public List<PlaceJson> getPlaceOfOrigin() {
		return placeOfOrigin;
	}

	public void setPlaceOfOrigin(List<PlaceJson> placeOfOrigin) {
		this.placeOfOrigin = placeOfOrigin;
	}

	public List<LanguageJson> getLanguages() {
		return languages;
	}

	public void setLanguages(List<LanguageJson> languages) {
		this.languages = languages;
	}

	public List<PeopleJson> getReferredToPeople() {
		return referredToPeople;
	}

	public void setReferredToPeople(List<PeopleJson> referredToPeople) {
		this.referredToPeople = referredToPeople;
	}

	public List<RelatedDocumentJson> getRelatedDocs() {
		return relatedDocs;
	}

	public void setRelatedDocs(List<RelatedDocumentJson> relatedDocs) {
		this.relatedDocs = relatedDocs;
	}

	public List<BiblioRefJson> getBibliographicReferences() {
		return bibliographicReferences;
	}

	public Integer getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(Integer commentsCount) {
		this.commentsCount = commentsCount;
	}

	public void setBibliographicReferences(List<BiblioRefJson> bibliographicReferences) {
		this.bibliographicReferences = bibliographicReferences;
	}

	public String getGeneralNoteSynopsis() {
		return generalNoteSynopsis;
	}

	public void setGeneralNoteSynopsis(String generalNoteSynopsis) {
		this.generalNoteSynopsis = generalNoteSynopsis;
	}

	public String getChineseSynopsis() {
		return chineseSynopsis;
	}

	public void setChineseSynopsis(String chineseSynopsis) {
		this.chineseSynopsis = chineseSynopsis;
	}

	public List<UploadFileJson> getUploadFiles() {
		if (this.uploadFiles == null) {
			this.uploadFiles = new ArrayList<UploadFileJson>();
		}
		return (List<UploadFileJson>) ((ArrayList<UploadFileJson>) this.uploadFiles).clone();
	}

	public void setUploadFiles(List<UploadFileJson> uploadFiles) {
		this.uploadFiles = (List<UploadFileJson>) ((ArrayList<UploadFileJson>) uploadFiles).clone();
	}

	public List<TopicPlaceJson> getTopics() {
		return topics;
	}

	public Integer getVolumeId() {
		return volumeId;
	}

	public void setVolumeId(Integer volumeId) {
		this.volumeId = volumeId;
	}

	public Integer getInsertId() {
		return insertId;
	}

	public void setInsertId(Integer insertId) {
		this.insertId = insertId;
	}

	public void setTopics(List<TopicPlaceJson> topics) {
		this.topics = topics;
	}

	public Boolean getHasAttachment() {
		return hasAttachment;
	}

	public void setHasAttachment(Boolean hasAttachment) {
		this.hasAttachment = hasAttachment;
	}

	public Boolean getHasLacuna() {
		return hasLacuna;
	}

	public void setHasLacuna(Boolean hasLacuna) {
		this.hasLacuna = hasLacuna;
	}

	public List<HashMap> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<HashMap> attachments) {
		this.attachments = attachments;
	}

	public HashMap getLacuna() {
		return lacuna;
	}

	public void setLacuna(HashMap lacuna) {
		this.lacuna = lacuna;
	}
	
	private HashMap<String, Object> initCollations(Collation c, Integer documentId) {
		HashMap<String, Object> json = new HashMap<String, Object>();
		json.put("id", c.getId());
		json.put("parentDocumentId",
				c.getParentDocument() == null ? null : c.getParentDocument().getDocumentEntityId());
		json.put("childDocumentId", c.getChildDocument() == null ? null : c.getChildDocument().getDocumentEntityId());
		json.put("type", c.getType());
		json.put("parentVolumeId", c.getParentVolume() == null ? null : c.getParentVolume().getSummaryId());
		json.put("childVolumeId", c.getChildVolume() == null ? null : c.getChildVolume().getSummaryId());
		json.put("childInsertId", c.getChildInsert() == null ? null : c.getChildInsert().getInsertId());
		json.put("parentInsertId", c.getParentInsert() == null ? null : c.getParentInsert().getInsertId());
		json.put("parentVolumeName", c.getParentVolume() == null ? null : c.getParentVolume().getVolume());
		json.put("childVolumeName", c.getChildVolume() == null ? null : c.getChildVolume().getVolume());
		json.put("childInsertName", c.getChildInsert() == null ? null : c.getChildInsert().getInsertName());
		json.put("parentInsertName", c.getParentInsert() == null ? null : c.getParentInsert().getInsertName());
		if (c.getParentDocument() != null) {
			if (c.getParentDocument().getUploadFileEntities().size() > 0) {
				UploadInfoEntity u = c.getParentDocument().getUploadFileEntities().get(0).getUploadInfoEntity();
				json.put("parentCollectionName",
						u.getCollectionEntity() == null ? null : u.getCollectionEntity().getCollectionName());
				json.put("parentVolumeId", u.getVolumeEntity() == null ? null : u.getVolumeEntity().getSummaryId());
				json.put("parentVolumeName", u.getVolume() == null ? null : u.getVolumeEntity().getVolume());
				json.put("parentInsertId", u.getInsertEntity() == null ? null : u.getInsertEntity().getInsertId());
				json.put("parentInsertName", u.getInsertEntity() == null ? null : u.getInsertEntity().getInsertName());
			}
		} else if (c.getParentVolume() != null) {
			json.put("parentCollectionName", c.getParentVolume().getCollectionEntity().getCollectionName());
		} else if (c.getParentInsert() != null) {
			json.put("parentCollectionName",
					c.getParentInsert().getVolumeEntity().getCollectionEntity().getCollectionName());
			json.put("parentVolumeId", c.getParentInsert().getVolumeEntity().getSummaryId());
			json.put("parentVolumeName", c.getParentInsert().getVolumeEntity().getVolume());
		}
		if (c.getChildDocument() != null) {
			if (c.getChildDocument().getUploadFileEntities().size() > 0) {
				UploadInfoEntity u = c.getChildDocument().getUploadFileEntities().get(0).getUploadInfoEntity();
				json.put("childCollectionName",
						u.getCollectionEntity() == null ? null : u.getCollectionEntity().getCollectionName());
				json.put("childVolumeId", u.getVolumeEntity() == null ? null : u.getVolumeEntity().getSummaryId());
				json.put("childVolumeName", u.getVolume() == null ? null : u.getVolumeEntity().getVolume());
				json.put("childInsertId", u.getInsertEntity() == null ? null : u.getInsertEntity().getInsertId());
				json.put("childInsertName", u.getInsertEntity() == null ? null : u.getInsertEntity().getInsertName());
			}
		} else if (c.getChildVolume() != null) {
			json.put("childCollectionName", c.getChildVolume().getCollectionEntity().getCollectionName());
		} else if (c.getParentInsert() != null) {
			json.put("childCollectionName",
					c.getChildInsert().getVolumeEntity().getCollectionEntity().getCollectionName());
			json.put("childVolumeId", c.getChildInsert().getVolumeEntity().getSummaryId());
			json.put("childVolumeName", c.getChildInsert().getVolumeEntity().getVolume());
		}
		json.put("childDocumentTitle", c.getChildDocument() == null ? null : c.getChildDocument().getDeTitle());
		json.put("parentDocumentTitle", c.getParentDocument() == null ? null : c.getParentDocument().getDeTitle());
		json.put("createdBy", c.getCreatedBy() == null ? null : c.getCreatedBy().getAccount());
		return json;
	}

	@Override
	public void init(MiaDocumentEntity docEntity) {

		this.setCommentsCount(docEntity.getCommentsCount());
		this.setDocumentId(docEntity.getDocumentEntityId());
		this.setCategory(docEntity.getCategory());
		this.setTypology(docEntity.getTypology());
		this.setDeTitle(docEntity.getDeTitle());
		this.setPrivacy(docEntity.getPrivacy());
		this.setFlgPrinted(docEntity.getFlgPrinted());
		this.setOwner(docEntity.getCreatedBy());
		List<Collation> attachments = collationService.findByDocumentId(documentId, Collation.CollationType.ATTACHMENT);
		List<Collation> lacunas = collationService.findByDocumentId(documentId, Collation.CollationType.LACUNA);
		this.setHasAttachment(attachments.size() > 0);
		this.setHasLacuna(lacunas.size() > 0);
		if (this.getHasAttachment()) {
			for (Collation c : attachments) {
				this.attachments.add(initCollations(c, documentId));
			}
		}
		if (this.getHasLacuna()) {
			this.setLacuna(initCollations(lacunas.get(0), documentId));
		}
		if (docEntity.getDateCreated() != null) {
			this.setDateCreated(sdf.format(docEntity.getDateCreated()));
		}

		if (docEntity.getDateLastUpdate() != null) {
			this.setDateLastUpdated(sdf.format(docEntity.getDateLastUpdate()));
		}

		if (docEntity.getFlgLogicalDelete() != null && docEntity.getFlgLogicalDelete()) {
			this.setFlgLogicalDelete(1);
		} else {
			this.setFlgLogicalDelete(0);
		}

		UserDetails currentUser = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
		if (docEntity.getUploadFileEntities() != null && !docEntity.getUploadFileEntities().isEmpty()) {
			List<UploadFileJson> uploadFiles = new ArrayList<UploadFileJson>();
			List<String> collections = new ArrayList<String>();
			for (org.medici.mia.domain.UploadFileEntity uploadFile : docEntity.getUploadFileEntities()) {
				// Creation of UploadFiles fields
				UploadFileJson upFile = new UploadFileJson();
				boolean canView = this.checkIfCanViewUploadFile(uploadFile, currentUser);
				upFile.setUploadFileId(uploadFile.getUploadFileId());
				upFile.setUploadInfoId(uploadFile.getIdTblUpload());
				upFile.setImageOrder(uploadFile.getImageOrder());
				if (uploadFile.getFilename().equalsIgnoreCase(
						UploadFileEntity.NO_IMAGE)) {
					upFile.setFileName(UploadFileEntity.NO_IMAGE);
				}

				// Build of the real path
				UploadInfoEntity uploadInfo = uploadFile.getUploadInfoEntity();

				this.setInsertId(uploadInfo.getInsertId());
				this.setVolumeId(uploadInfo.getVolume());

				// Issue 618 If the inserEntity has no value, insertName is null
				String insName = null;
				if (uploadInfo.getInsertEntity() != null) {
					insName = uploadInfo.getInsertEntity().getInsertName();
				}

				if(canView) {
					upFile.setCanView(true);
					String realPath = org.medici.mia.common.util.FileUtils.getRealPathJSONResponse(
							uploadInfo.getRepositoryEntity().getLocation(),
							uploadInfo.getRepositoryEntity().getRepositoryAbbreviation(),
							uploadInfo.getCollectionEntity().getCollectionAbbreviation(),
							uploadInfo.getVolumeEntity().getVolume(), insName);
					upFile.setFilePath(realPath + FileUtils.THUMB_FILES_DIR + FileUtils.OS_SLASH + uploadFile.getFilename()
							+ "&WID=250");

				} else {
					upFile.setFilePath(null);
					upFile.setCanView(false);
				}
				if (uploadInfo.getCollectionEntity() != null) {
					upFile.setCollectionName(uploadInfo.getCollectionEntity().getCollectionName());
				}

				if (uploadInfo.getInsertEntity() != null) {
					upFile.setInserName(uploadInfo.getInsertEntity().getInsertName());
				}
				if (uploadInfo.getSeriesEntity() != null) {
					upFile.setSeriesName(uploadInfo.getSeriesEntity().getTitle());
				}
				if (uploadInfo.getVolumeEntity() != null) {
					upFile.setVolumeName(uploadInfo.getVolumeEntity().getVolume());
				}

				if (uploadInfo.getCollectionEntity() != null
						&& uploadInfo.getCollectionEntity().getCollectionName() != null) {
					String collectioName = uploadInfo.getCollectionEntity().getCollectionName().trim();
					if (!collections.contains(collectioName)) {
						collections.add(collectioName);
					}
				}
				
				upFile.setUploadInfoOwner(uploadInfo.getOwner());

				upFile.setDocumentId(docEntity.getDocumentEntityId());
				// Creation of Folio fields
				if (uploadFile.getFolioEntities() != null && !uploadFile.getFolioEntities().isEmpty()) {
					List<FolioJson> folios = new ArrayList<FolioJson>();
					// Validation of folio
					for (org.medici.mia.domain.FolioEntity folio : uploadFile.getFolioEntities()) {
						FolioJson folioJson = new FolioJson();
						folioJson.toJson(folio);
						if(!canView) {
							folioJson.setImgName(null);
						}
						folios.add(folioJson);
					}

					upFile.setFolios(FolioUtils.prepareFoliosToBeAdded(folios));
				}
				uploadFiles.add(upFile);

			}
			UploadInfoEntity upload = docEntity.getUploadFileEntities().get(0).getUploadInfoEntity();
			if(upload != null){
				
				CollectionEntity collectionEntity = upload.getCollectionEntity();
				
				this.getCollection().setCollectionAbbreviation(collectionEntity.getCollectionAbbreviation());
				this.getCollection().setCollectionDescription(collectionEntity.getCollectionDescription());
				this.getCollection().setCollectionId(Integer.toString(collectionEntity.getCollectionId()));
				this.getCollection().setCollectionName(collectionEntity.getCollectionName());
				this.setVolume(new VolumeDescriptionJson().toJson(upload.getVolumeEntity(), null));
				if(upload.getInsertEntity() != null){
					this.setInsert(new InsertDescriptionJson().toJson(upload.getInsertEntity()));
				}
				
				RepositoryEntity repositoryEntity = collectionEntity.getRepositoryEntity();
				this.setRepository(prepareRepositoryJson(repositoryEntity));
			}
			
			this.setUploadFiles(uploadFiles);
			this.setCollections(collections);
		}



		// Setting producers list
		if (docEntity.getProducers() != null && !docEntity.getProducers().isEmpty()) {
			List<PeopleJson> peoples = new ArrayList<PeopleJson>();
			for (org.medici.mia.domain.People person : docEntity.getProducers()) {
				PeopleJson perJson = new PeopleJson();
				perJson.setId(person.getPersonId());
				perJson.setMapNameLf(person.getMapNameLf());
				perJson.setGender(person.getGender());
				perJson.setActiveStart(person.getActiveStart());
				perJson.setActiveEnd(person.getActiveEnd());
				perJson.setBornYear(person.getBornYear());
				perJson.setDeathYear(person.getDeathYear());
				perJson.setFirstName(person.getFirst());
				perJson.setLastName(person.getLast());
				perJson.setUnsure(person.getUnsure());
				peoples.add(perJson);

			}
			this.setProducers(peoples);
		}

		// Setting place of Origin and sure/unsure field

		if (docEntity.getPlaceOfOriginEntity() != null) {
			List<PlaceJson> placeOfOrig = new ArrayList<PlaceJson>();
			PlaceJson plaJson = new PlaceJson();
			plaJson.setId(docEntity.getPlaceOfOriginEntity().getPlaceAllId());
			plaJson.setPlaceName(docEntity.getPlaceOfOriginEntity().getPlaceNameFull());
			plaJson.setPlaceNameId(docEntity.getPlaceOfOriginEntity().getPlaceNameId());
			plaJson.setPlType(docEntity.getPlaceOfOriginEntity().getPlType());
			plaJson.setPrefFlag(docEntity.getPlaceOfOriginEntity().getPrefFlag());
			plaJson.setUnsure(docEntity.getPlaceOfOriginUnsure());

			placeOfOrig.add(plaJson);
			this.setPlaceOfOrigin(placeOfOrig);
		}

		// Setting of Date field
		DateJson date = new DateJson();
		date.setDocYear(docEntity.getDocYear());
		date.setDocMonth(docEntity.getDocMonth());
		date.setDocDay(docEntity.getDocDay());
		date.setDocModernYear(docEntity.getDocModernYear());
		date.setDocDateUncertain(docEntity.getDocDateUncertain());
		date.setDocUndated(docEntity.getDocUndated());
		date.setDocDateNotes(docEntity.getDocDateNotes());
		this.setDate(date);

		// Setting referred to people field
		if (docEntity.getRefToPeople() != null && !docEntity.getRefToPeople().isEmpty()) {
			List<PeopleJson> peoplesRefTo = new ArrayList<PeopleJson>();
			for (org.medici.mia.domain.People personRefTo : docEntity.getRefToPeople()) {

				PeopleJson perJson = new PeopleJson();
				perJson.setId(personRefTo.getPersonId());
				perJson.setMapNameLf(personRefTo.getMapNameLf());
				perJson.setGender(personRefTo.getGender());
				perJson.setActiveStart(personRefTo.getActiveStart());
				perJson.setActiveEnd(personRefTo.getActiveEnd());
				perJson.setBornYear(personRefTo.getBornYear());
				perJson.setDeathYear(personRefTo.getDeathYear());
				perJson.setFirstName(personRefTo.getFirst());
				perJson.setLastName(personRefTo.getLast());
				perJson.setUnsure(personRefTo.getUnsure());
				peoplesRefTo.add(perJson);

			}
			this.setReferredToPeople(peoplesRefTo);
		}

		// Setting Related documents field
		if (docEntity.getRelatedDocs() != null && !docEntity.getRelatedDocs().isEmpty()) {
			List<RelatedDocumentJson> docsRelated = new ArrayList<RelatedDocumentJson>();
			for (MiaDocumentEntity relatedDoc : docEntity.getRelatedDocs()) {

				RelatedDocumentJson doc = new RelatedDocumentJson();
				doc.setDocumentId(relatedDoc.getDocumentEntityId());
				doc.setTitle(relatedDoc.getDeTitle());
				docsRelated.add(doc);

			}
			this.setRelatedDocs(docsRelated);
		}

		// Setting Bibliographic references field
		if (docEntity.getBiblioRefs() != null && !docEntity.getBiblioRefs().isEmpty()) {
			List<BiblioRefJson> bibliorefs = new ArrayList<BiblioRefJson>();
			for (BiblioRefEntity biblioref : docEntity.getBiblioRefs()) {

				BiblioRefJson ref = new BiblioRefJson();
				ref.setIdRef(biblioref.getIdRef());
				ref.setIdDoc(biblioref.getIdDoc());
				ref.setName(biblioref.getName());
				ref.setPermalink(biblioref.getPermalink());
				bibliorefs.add(ref);

			}
			this.setBibliographicReferences(bibliorefs);
		}

		// Setting of TopicPlace fields
		if (docEntity.getTopicPlaces() != null && !docEntity.getTopicPlaces().isEmpty()) {
			List<TopicPlaceJson> topics = new ArrayList<TopicPlaceJson>();
			for (TopicPlaceEntity topicPlace : docEntity.getTopicPlaces()) {
				TopicPlaceJson topic = new TopicPlaceJson();
				if (topicPlace.getDocumentEntityId() != null)
					topic.setDocumentId(String.valueOf(topicPlace.getDocumentEntityId()));
				topic.setPlaceId(topicPlace.getPlaceId());
				topic.setTopicListId(topicPlace.getTopicListId());
				if (topicPlace.getPlaceEntity() != null) {
					topic.setUnsure(topicPlace.getPlaceEntity().getUnsure());
					topic.setPrefFlag(topicPlace.getPlaceEntity().getPrefFlag());
					topic.setTopicPlaceName(topicPlace.getPlaceEntity().getPlaceNameFull());

				}

				if (topicPlace.getTopicListEntity() != null) {
					topic.setTopicName(topicPlace.getTopicListEntity().getTopicTitle());

				}
				topics.add(topic);
			}
			this.setTopics(topics);
		}

		// Setting of language field
		if (docEntity.getLanguages() != null && !docEntity.getLanguages().isEmpty()) {
			List<LanguageJson> langs = new ArrayList<LanguageJson>();
			for (LanguageEntity langEnt : docEntity.getLanguages()) {

				LanguageJson lang = new LanguageJson();
				lang.setId(langEnt.getId());
				lang.setLanguage(langEnt.getLanguage());
				langs.add(lang);

			}
			this.setLanguages(langs);
		}

		// Setting of Synopsis and transcriptions fields
		this.setGeneralNoteSynopsis(docEntity.getGeneralNotesSynopsis());
		this.setChineseSynopsis(docEntity.getChineseSynopsis());

		if (docEntity.getDocumentTranscriptions() != null && !docEntity.getDocumentTranscriptions().isEmpty()) {
			List<DocumentTranscriptionJson> transcriptions = new ArrayList<DocumentTranscriptionJson>();
			for (DocumentTranscriptionEntity transcriptionEnt : docEntity.getDocumentTranscriptions()) {
				DocumentTranscriptionJson transcriptionJson = new DocumentTranscriptionJson();
				if (transcriptionEnt.getDocTranscriptionId() != null) {
					transcriptionJson.setDocTranscriptionId(String.valueOf(transcriptionEnt.getDocTranscriptionId()));
				}
				if (transcriptionEnt.getDocumentEntityId() != null) {
					transcriptionJson.setDocumentId(String.valueOf(transcriptionEnt.getDocumentEntityId()));
				}
				if (transcriptionEnt.getUploadedFileId() != null) {
					transcriptionJson.setUploadedFileId(String.valueOf(transcriptionEnt.getUploadedFileId()));
				}
				transcriptionJson.setTranscription(transcriptionEnt.getTranscription());
				transcriptions.add(transcriptionJson);
			}

			this.setTranscriptions(transcriptions);
		}

	}

	private boolean checkIfCanViewUploadFile(UploadFileEntity upload, UserDetails currentUser) {
		if (currentUser != null) {
			if (upload.getFilePrivacy() == 1) {
				UploadInfoEntity uploadInfo = uploadInfoDAO.find(upload.getIdTblUpload());
				if (uploadInfo != null) {
					if (DocumentPrivacyUtils.isUserAdmin(currentUser)
							|| DocumentPrivacyUtils.isOnsiteFellows(currentUser)
							|| uploadInfo.getOwner().equals(currentUser.getUsername())) {
						return true;
					} else {
						List<FileSharing> sharedFile = fileSharingDao
								.findByUploadFileIdAndAccount(upload.getUploadFileId(), currentUser.getUsername());
						if (sharedFile.size() > 0) {
							return true;
						}
					}
				}
			} else {
				return true;
			}
		}
		return false;

	}

	@Override
	public MiaDocumentEntity getMiaDocumentEntity(DocumentJson docJson) {

		GenericDocumentJson genericDoc = (GenericDocumentJson) docJson;
		MiaDocumentEntity docEnt = new MiaDocumentEntity();

		if (genericDoc != null) {
			docEnt.setDocumentEntityId(genericDoc.getDocumentId());
			docEnt.setCategory(genericDoc.getCategory());
			docEnt.setTypology(genericDoc.getTypology());
			docEnt.setDeTitle(genericDoc.getDeTitle());

			// Set default values or update these values
			if (genericDoc.getDocumentId() == null) {
				docEnt.setPrivacy(0);
				docEnt.setFlgPrinted(0);
			} else {
				docEnt.setPrivacy(genericDoc.getPrivacy());
				docEnt.setFlgPrinted(genericDoc.getFlgPrinted());
			}

			// Set uploadFiles and folios
			if (genericDoc.getUploadFiles() != null && !genericDoc.getUploadFiles().isEmpty()) {
				List<UploadFileEntity> uploadFileEntities = new ArrayList<UploadFileEntity>();

				for (UploadFileJson uploadFile : genericDoc.getUploadFiles()) {
					UploadFileEntity uFile = new UploadFileEntity();
					uFile.setUploadFileId(uploadFile.getUploadFileId());
					// set folios
					if (uploadFile.getFolios() != null && !uploadFile.getFolios().isEmpty()) {
						List<FolioEntity> folioEntities = new ArrayList<FolioEntity>();
						for (FolioJson folio : uploadFile.getFolios()) {
							// Creation of Folio fields
							org.medici.mia.domain.FolioEntity folioEnt = new org.medici.mia.domain.FolioEntity();
							folioEnt.setFolioId(folio.getFolioId());
							folioEnt.setFolioNumber(folio.getFolioNumber());
							if (folio.getRectoverso() != null && !folio.getRectoverso().isEmpty()) {
								folioEnt.setRectoverso(folio.getRectoverso());
							} else {
								folioEnt.setRectoverso(null);
							}
							folioEnt.setNoNumb(folioEnt.getNoNumb());

							folioEnt.setUploadedFileId(folio.getUploadFileId());

							folioEntities.add(folioEnt);
						}
						uFile.setFolioEntities(folioEntities);
					}
					uploadFileEntities.add(uFile);
				}

				docEnt.setUploadFileEntities(uploadFileEntities);

			}

			// Setting the producers list and Place Of Origin
			List<PeopleJson> peopleJson = genericDoc.getProducers();
			if (peopleJson != null && !peopleJson.isEmpty()) {
				List<People> peoples = new ArrayList<People>();
				for (PeopleJson personJson : peopleJson) {
					// FIXME : temporary solution for receiving an element with
					// the null fields from FE.
					if (personJson.getId() != null) {
						org.medici.mia.domain.People person = new org.medici.mia.domain.People();

						person.setPersonId(personJson.getId());
						person.setUnsure(personJson.getUnsure());
						peoples.add(person);
					}
				}
				if (!peoples.isEmpty())
					docEnt.setProducers(peoples);
			}
			if (genericDoc.getPlaceOfOrigin() != null && !genericDoc.getPlaceOfOrigin().isEmpty()) {
				docEnt.setPlaceOfOrigin(genericDoc.getPlaceOfOrigin().get(0).getId());
				docEnt.setPlaceOfOriginUnsure(genericDoc.getPlaceOfOrigin().get(0).getUnsure());
			}

			// Setting of Date fields
			docEnt.setDocYear(genericDoc.getDate().getDocYear());
			docEnt.setDocMonth(genericDoc.getDate().getDocMonth());
			docEnt.setDocDay(genericDoc.getDate().getDocDay());
			docEnt.setDocModernYear(genericDoc.getDate().getDocModernYear());
			docEnt.setDocDateUncertain(genericDoc.getDate().getDocDateUncertain());
			docEnt.setDocUndated(genericDoc.getDate().getDocUndated());
			docEnt.setDocDateNotes(genericDoc.getDate().getDocDateNotes());

			// Setting the referred to people list field
			List<PeopleJson> peopleRefToJson = genericDoc.getReferredToPeople();
			if (peopleRefToJson != null && !peopleRefToJson.isEmpty()) {
				List<People> peoplesRefTo = new ArrayList<People>();
				for (PeopleJson personRefToJson : peopleRefToJson) {
					org.medici.mia.domain.People personRefTo = new org.medici.mia.domain.People();
					personRefTo.setPersonId(personRefToJson.getId());
					personRefTo.setUnsure(personRefToJson.getUnsure());
					peoplesRefTo.add(personRefTo);
				}
				docEnt.setRefToPeople(peoplesRefTo);
			}

			// Setting related documents field
			if (genericDoc.getRelatedDocs() != null && !genericDoc.getRelatedDocs().isEmpty()) {
				List<MiaDocumentEntity> relatedDocsEnts = new ArrayList<MiaDocumentEntity>();
				for (RelatedDocumentJson relatedDoc : genericDoc.getRelatedDocs()) {
					MiaDocumentEntity relatedDocEnt = new MiaDocumentEntity();
					relatedDocEnt.setDocumentEntityId(relatedDoc.getDocumentId());
					relatedDocsEnts.add(relatedDocEnt);
				}
				docEnt.setRelatedDocs(relatedDocsEnts);
			}

			// Setting Biographic References field

			if (genericDoc.getBibliographicReferences() != null && !genericDoc.getBibliographicReferences().isEmpty()) {
				List<BiblioRefEntity> biblioRefs = new ArrayList<BiblioRefEntity>();
				for (BiblioRefJson biblioRefJson : genericDoc.getBibliographicReferences()) {
					BiblioRefEntity ref = new BiblioRefEntity();
					ref.setIdDoc(biblioRefJson.getIdDoc());
					ref.setName(biblioRefJson.getName());
					ref.setPermalink(biblioRefJson.getPermalink());
					biblioRefs.add(ref);
				}
				docEnt.setBiblioRefs(biblioRefs);
			}

			// Setting of language fields
			if (genericDoc.getLanguages() != null && !genericDoc.getLanguages().isEmpty()) {
				List<LanguageEntity> languages = new ArrayList<LanguageEntity>();
				for (LanguageJson langJson : genericDoc.getLanguages()) {
					LanguageEntity language = new LanguageEntity();
					language.setId(langJson.getId());
					language.setLanguage(langJson.getLanguage());
					languages.add(language);
				}
				docEnt.setLanguages(languages);
			}

			// Setting of synopsis and transcriptions fields
			docEnt.setGeneralNotesSynopsis(genericDoc.getGeneralNoteSynopsis());

			if (genericDoc.getTranscriptions() != null && !genericDoc.getTranscriptions().isEmpty()) {
				List<DocumentTranscriptionEntity> transcriptionEnts = new ArrayList<DocumentTranscriptionEntity>();
				for (DocumentTranscriptionJson transJson : genericDoc.getTranscriptions()) {
					DocumentTranscriptionEntity transEnt = new DocumentTranscriptionEntity();
					if (transJson.getDocTranscriptionId() != null && !transJson.getDocTranscriptionId().isEmpty()) {
						transEnt.setDocTranscriptionId(Integer.valueOf(transJson.getDocTranscriptionId()));
					}
					if (transJson.getDocumentId() != null && !transJson.getDocumentId().isEmpty()) {
						transEnt.setDocumentEntityId(Integer.valueOf(transJson.getDocumentId()));
					}
					if (transJson.getUploadedFileId() != null && !transJson.getUploadedFileId().isEmpty()) {
						transEnt.setUploadedFileId(Integer.valueOf(transJson.getUploadedFileId()));
					}
					transEnt.setTranscription(transJson.getTranscription());

					transcriptionEnts.add(transEnt);
				}
				docEnt.setDocumentTranscriptions(transcriptionEnts);
			}

			// Setting of TopicsPlace fields
			if (genericDoc.getDocumentId() != null) {
				if (genericDoc.getTopics() != null && !genericDoc.getTopics().isEmpty()) {
					List<TopicPlaceEntity> listTopPl = new ArrayList<TopicPlaceEntity>();
					for (TopicPlaceJson topic : genericDoc.getTopics()) {
						TopicPlaceEntity newTopic = new TopicPlaceEntity();
						newTopic.setDocumentEntityId(genericDoc.getDocumentId());
						newTopic.setPlaceId(topic.getPlaceId());
						newTopic.setTopicListId(topic.getTopicListId());
						listTopPl.add(newTopic);
					}
					docEnt.setTopicPlaces(listTopPl);
				}
			}
		}
		return docEnt;

	}
	
	private RepositoryJson prepareRepositoryJson(RepositoryEntity repositoryEntity) {
		RepositoryJson repositoryJson = new RepositoryJson();

		if (repositoryEntity == null) {
			return repositoryJson;
		}
		
		repositoryJson.setRepositoryId(Integer.toString(repositoryEntity.getRepositoryId()));
		repositoryJson.setRepositoryName(repositoryEntity.getRepositoryName());
		repositoryJson.setRepositoryDescription(repositoryEntity.getRepositoryDescription());
		repositoryJson.setRepositoryAbbreviation(repositoryEntity.getRepositoryAbbreviation());
		
		GoogleLocation googleLocation = repositoryEntity.getGoogleLocation();
		if (googleLocation != null) {
			repositoryEntity.setGoogleLocation(googleLocation);
			repositoryJson.setRepositoryCountry(googleLocation.getCountry());
			repositoryJson.setRepositoryCity(googleLocation.getCity());
		}
		
		return repositoryJson;
	}

	@Override
	public List<FindPlaceForDocJson> findPlaceJson(MiaDocumentEntity docEntity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MiaDocumentEntity getModifiedDocumentEnt(MiaDocumentEntity docEnt, ModifyPeopleForDocJson modifyPeople) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MiaDocumentEntity getModifiedDocumentEnt(MiaDocumentEntity docEnt, ModifyPlaceForDocJson modifyPlace) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DocPeopleFieldJson> getDocPeopleFields(Integer personId, MiaDocumentEntity docEnt,
			List<DocumentFieldEntity> documentFields) {

		List<DocPeopleFieldJson> fields = null;
		if (docEnt.getProducers() == null) {
			return fields;
		}

		for (People people : docEnt.getProducers()) {
			if (people.getPersonId().equals(personId)) {
				DocPeopleFieldJson field = new DocPeopleFieldJson();
				field.setFieldBeName(PeopleFieldType.Producers.toString());
				field.setFieldName(
						getFieldName(documentFields, docEnt.getCategory(), PeopleFieldType.Producers.toString()));

				if (fields == null)
					fields = new ArrayList<DocPeopleFieldJson>();

				fields.add(field);
				return fields;
			}
		}

		return fields;

	}

	public String getFieldName(List<DocumentFieldEntity> documentFields, String category, String fileBeName) {
		if (documentFields == null || documentFields.isEmpty()) {
			return null;
		}
		for (DocumentFieldEntity field : documentFields) {
			if (field.getFieldBeName().equalsIgnoreCase(fileBeName)
					&& field.getDocumentCategory().equalsIgnoreCase(category)) {
				return field.getFieldName().replaceAll("\\s", "");
			}
		}

		return null;
	}

	public boolean hasField(String fields, String personId) {
		if (fields == null || fields.isEmpty())
			return false;

		String[] fieldAarray = fields.split(",");

		for (String field : fieldAarray) {
			String[] array = field.split(":");

			if (array == null || array.length == 0) {
				return false;
			}

			if (array[0].equalsIgnoreCase(personId)) {
				return true;
			}
		}

		return false;
	}

	public void setDocPeopleField(MiaDocumentEntity docEnt, List<DocumentFieldEntity> documentFields,
			PeopleFieldType fieldType, List<DocPeopleFieldJson> fields) {

		DocPeopleFieldJson field = new DocPeopleFieldJson();
		field.setFieldBeName(fieldType.toString());
		field.setFieldName(getFieldName(documentFields, docEnt.getCategory(), fieldType.toString()));

		if (fields == null)
			fields = new ArrayList<DocPeopleFieldJson>();

		fields.add(field);

	}

	public List<FieldJson> getJsonFields(List<DocumentFieldEntity> documentFields) {

		return new ArrayList<FieldJson>();
	}

	public List<FindPeopleForDocJson> findPeopleJson(MiaDocumentEntity docEntity) {
		return null;
	}

	public List<DocumentTranscriptionJson> getTranscriptions() {
		return transcriptions;
	}

	public void setTranscriptions(List<DocumentTranscriptionJson> transcriptions) {
		this.transcriptions = transcriptions;
	}

	@Override
	public Integer getFlgLogicalDelete() {
		return flgLogicalDelete;
	}

	public void setFlgLogicalDelete(Integer flgLogicalDelete) {
		this.flgLogicalDelete = flgLogicalDelete;
	}

	public List<String> getCollections() {
		return collections;
	}

	public void setCollections(List<String> collections) {
		this.collections = collections;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getDateLastUpdated() {
		return dateLastUpdated;
	}

	public void setDateLastUpdated(String dateLastUpdated) {
		this.dateLastUpdated = dateLastUpdated;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public VolumeDescriptionJson getVolume() {
		return volume;
	}

	public void setVolume(VolumeDescriptionJson volume) {
		this.volume = volume;
	}

	public InsertDescriptionJson getInsert() {
		return insert;
	}

	public void setInsert(InsertDescriptionJson insert) {
		this.insert = insert;
	}

	public CollectionJson getCollection() {
		return collection;
	}

	public void setCollection(CollectionJson collection) {
		this.collection = collection;
	}
	
	public RepositoryJson getRepository() {
		return repository;
	}

	public void setRepository(RepositoryJson repository) {
		this.repository = repository;
	}

	@Override
	public void initPrivate(MiaDocumentEntity docEntity) {
		this.setDocumentId(docEntity.getDocumentEntityId());
		this.setCategory(docEntity.getCategory());
		this.setTypology(docEntity.getTypology());
		this.setPrivacy(docEntity.getPrivacy());
		this.setOwner(docEntity.getCreatedBy());
		this.setDeTitle("PRIVATE");
		DateJson date = new DateJson();
		date.setDocYear(docEntity.getDocYear());
		date.setDocMonth(docEntity.getDocMonth());
		date.setDocDay(docEntity.getDocDay());
		date.setDocModernYear(docEntity.getDocModernYear());
		date.setDocDateUncertain(docEntity.getDocDateUncertain());
		date.setDocUndated(docEntity.getDocUndated());
		date.setDocDateNotes(docEntity.getDocDateNotes());
		this.setDate(date);
	}

	public Boolean getCanModify() {
		return canModify;
	}

	public void setCanModify(Boolean canModify) {
		this.canModify = canModify;
	}
}
