package org.medici.mia.common.json;

import org.medici.mia.domain.Place;

public class PlaceJson extends PlaceBaseJson {

	private static final long serialVersionUID = 1L;

	private Integer placeNameId;
	private String placeName;
	private String plType;

	public PlaceJson() {
	}

	public void toJson(Place entity) {
		if (entity == null)
			return;
		if (entity instanceof Place) {

			this.setId(entity.getPlaceAllId());
			this.setPlaceNameId(entity.getPlaceNameId());
			this.setPlaceName(entity.getPlaceNameFull());
			this.setPlType(entity.getPlType());
			this.setPrefFlag(entity.getPrefFlag());
			this.setUnsure(entity.getUnsure());
		}

	}

	public Integer getPlaceNameId() {
		return placeNameId;
	}

	public void setPlaceNameId(Integer placeNameId) {
		this.placeNameId = placeNameId;
	}

	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	public String getPlType() {
		return plType;
	}

	public void setPlType(String plType) {
		this.plType = plType;
	}

}