/*
 * placesSearchJson.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.common.json.advancedsearch;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.util.DateUtils;
import org.medici.mia.domain.User;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@JsonTypeName("placesAdvancedSearch")
public class PlacesAdvancedSearchJson extends GenericAdvancedSearchJson {

	private static final long serialVersionUID = 1L;
	private static final String NOT_DELETED_DOC = " d.flgLogicalDelete != 1";
	private static final String UNION = " ) UNION (";

	@JsonProperty("places")
	private List<String> places;

	public List<String> getPlaces() {
		return places;
	}

	public void setPlaces(List<String> places) {
		this.places = places;
	}

	@JsonIgnore
	@Override
	public List<String> getQueries() {
		List<String> queries = new ArrayList<String>();

		StringBuffer b = new StringBuffer();
		b.append("( ");

		b.append(getPlaceOfOriginQuery());
		b.append(UNION);

		b.append(getLiteraryWorksQuery());
		b.append(UNION);

		b.append(getNewsFromsQuery());
		b.append(UNION);

		b.append(getNewsPrinterPlaceQuery());
		b.append(UNION);

		b.append(getOfficialRecordsQuery());
		b.append(UNION);

		b.append(getCorrespondenceQuery());
		b.append(UNION);
		
		b.append(getTopicsQuery());
		b.append(" )");

		queries.add(b.toString());

		return queries;
	}

	private String getPlaceOfOriginQuery() {

		// select distinct d.documentEntityId documentId from tblDocumentEnts d
		// where
		// ( d.placeOfOrigin = '1105' OR d.placeOfOrigin =
		// '18' OR d.placeOfOrigin = = '9087' )and 
		// NOT_DELETED_DOC

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d where ");

		b.append("( ");
		b.append("d.placeOfOrigin = ");
		b.append("'");
		b.append(places.get(0));
		b.append("'");
		for (int i = 1; i < places.size() - 1; i++) {
			b.append(" OR ");
			b.append("d.placeOfOrigin = ");
			b.append("'");
			b.append(places.get(i));
			b.append("'");
		}
		b.append(" )");
		b.append("and " + NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getCorrespondenceQuery() {

		// select distinct d.documentEntityId from tblDocumentEnts d,
		// tblDocCorrespondence c where ( c.recipient like '1105:%' OR
		// c.recipient like ',1105:%' OR c.recipient like '1128:%' OR
		// c.recipient like ',1128:%' OR c.recipient like '9087:%' OR
		// c.recipient like ',9087:%' ) and c.documentEntityId =
		// d.documentEntityId
		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocCorrespondence c where ");

		b.append("( ");
		b.append("c.recipientPlace like ");
		b.append("'");
		b.append(places.get(0) + ":%");
		b.append("'");
		b.append(" OR c.recipientPlace like ");
		b.append("'");
		b.append("," + places.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < places.size(); i++) {
			b.append(" OR ");

			b.append("c.recipientPlace like ");
			b.append("'");
			b.append(places.get(i) + ":%");
			b.append("'");
			b.append(" OR c.recipientPlace like ");
			b.append("'");
			b.append("," + places.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and " + NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getLiteraryWorksQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocLiteraryWorks c where ");

		b.append("( ");
		b.append("c.printerPlace like ");
		b.append("'");
		b.append(places.get(0) + ":%");
		b.append("'");
		b.append(" OR c.printerPlace like ");
		b.append("'");
		b.append("," + places.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < places.size(); i++) {
			b.append(" OR ");

			b.append("c.printerPlace like ");
			b.append("'");
			b.append(places.get(i) + ":%");
			b.append("'");
			b.append(" OR c.printerPlace like ");
			b.append("'");
			b.append("," + places.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and " + NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getNewsPrinterPlaceQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocNews c where ");

		b.append("( ");
		b.append("c.printerPlace like ");
		b.append("'");
		b.append(places.get(0) + ":%");
		b.append("'");
		b.append(" OR c.printerPlace like ");
		b.append("'");
		b.append("," + places.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < places.size(); i++) {
			b.append(" OR ");

			b.append("c.printerPlace like ");
			b.append("'");
			b.append(places.get(i) + ":%");
			b.append("'");
			b.append(" OR c.printerPlace like ");
			b.append("'");
			b.append("," + places.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and " + NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getNewsFromsQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocNews c where ");

		b.append("( ");
		b.append("c.newsFrom like ");
		b.append("'");
		b.append(places.get(0) + ":%");
		b.append("'");
		b.append(" OR c.newsFrom like ");
		b.append("'");
		b.append("," + places.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < places.size(); i++) {
			b.append(" OR ");

			b.append("c.newsFrom like ");
			b.append("'");
			b.append(places.get(i) + ":%");
			b.append("'");
			b.append(" OR c.newsFrom like ");
			b.append("'");
			b.append("," + places.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and " +NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getOfficialRecordsQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocOfficialRecords c where ");

		b.append("( ");
		b.append("c.printerPlace like ");
		b.append("'");
		b.append(places.get(0) + ":%");
		b.append("'");
		b.append(" OR c.printerPlace like ");
		b.append("'");
		b.append("," + places.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < places.size(); i++) {
			b.append(" OR ");

			b.append("c.printerPlace like ");
			b.append("'");
			b.append(places.get(i) + ":%");
			b.append("'");
			b.append(" OR c.printerPlace like ");
			b.append("'");
			b.append("," + places.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and " + NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getTopicsQuery() {
		
		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocumentTopicPlace dtp where ");
		b.append("( ");
		b.append("dtp.placeId = ");
		b.append("'");
		b.append(places.get(0));
		b.append("'");
		for (int i = 1; i < places.size() - 1; i++) {
			b.append(" OR ");
			b.append("dtp.placeId = ");
			b.append("'");
			b.append(places.get(i));
			b.append("'");
		}
		b.append(" )");
		b.append("and d.documentEntityId = dtp.documentEntityId and " + NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}
}
