package org.medici.mia.common.json.message;

import java.io.Serializable;

import org.medici.mia.domain.UserMessage;

public class MessageJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer messageId;
	private String messageSubject;
	private String messageText;
	private String account;

	public MessageJson toJson(UserMessage mes) {

		this.setMessageId(mes.getMessageId());
		this.setMessageSubject(mes.getSubject());
		this.setMessageText(mes.getBody());
		this.setAccount(mes.getUser().getAccount());

		return this;
	}

	public UserMessage toEntity(UserMessage mesEnt) {

		mesEnt.setMessageId(this.getMessageId());
		mesEnt.setSubject(this.getMessageSubject());
		mesEnt.setBody(this.getMessageText());		

		return mesEnt;

	}

	public Integer getMessageId() {
		return messageId;
	}

	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}

	public String getMessageSubject() {
		return messageSubject;
	}

	public void setMessageSubject(String messageSubject) {
		this.messageSubject = messageSubject;
	}

	public String getMessageText() {
		return messageText;
	}

	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}
	

}