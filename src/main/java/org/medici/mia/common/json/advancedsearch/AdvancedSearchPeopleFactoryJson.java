package org.medici.mia.common.json.advancedsearch;

public class AdvancedSearchPeopleFactoryJson {
	public static IAdvancedSearchJson getAdvancedSearchJson(
			IAdvancedSearchJson searchJson) {
		String searchSection = ((GenericAdvancedSearchJson) searchJson)
				.getSearchSection();
		if (AdvancedSearchType.NAME_SEARCH.getValue().equalsIgnoreCase(
				searchSection)) {
			return ((WordAdvancedSearchJson) searchJson);
		} else if (AdvancedSearchType.PERSON_VITAL_STATISTIC_GENDER_SEARCH
				.getValue().equalsIgnoreCase(searchSection)) {
			return ((GenderAdvancedSearchJson) searchJson);
		} else if (AdvancedSearchType.PERSON_VITAL_STATISTICS_BIRTH_DEATH_SEARCH
				.getValue().equalsIgnoreCase(searchSection)) {
			return ((PersonVitalStatisticsBirthDeathAdvancedSearchJson) searchJson);
		} else if (AdvancedSearchType.PERSON_VITAL_STATISTICS_BADBLBBDO_SEARCH
				.getValue().equalsIgnoreCase(searchSection)) {
			return ((PersonVitalStatisticBADBLBBDOAdvancedSearchJson) searchJson);
		} else if (AdvancedSearchType.TITLE_OCCUPATION_SEARCH.getValue()
				.equalsIgnoreCase(searchSection)) {
			return ((TitleOccupationsAdvancedSearchJson) searchJson);
		}
		return null;
	}

}
