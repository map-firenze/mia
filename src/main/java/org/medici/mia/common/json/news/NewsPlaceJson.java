package org.medici.mia.common.json.news;

import java.text.SimpleDateFormat;

import org.medici.mia.domain.Place;

public class NewsPlaceJson extends NewsBaseJson {

	private static final long serialVersionUID = 1L;

	private final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	private String placeName;
	private String action;
	private String modifiedBy;
	private String placeNameFull;
	private String plType;
	private String prefFlag;
	private String placesMemo;

	public String getPlacesMemo() {
		return placesMemo;
	}

	public void setPlacesMemo(String placesMemo) {
		this.placesMemo = placesMemo;
	}

	public String getPlaceNameFull() {
		return placeNameFull;
	}

	public void setPlaceNameFull(String placeNameFull) {
		this.placeNameFull = placeNameFull;
	}

	public String getPlType() {
		return plType;
	}

	public void setPlType(String plType) {
		this.plType = plType;
	}

	public String getPrefFlag() {
		return prefFlag;
	}

	public void setPrefFlag(String prefFlag) {
		this.prefFlag = prefFlag;
	}

	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public NewsPlaceJson toJson(Place entity) {

		this.setType("place");
		this.setPlacesMemo(entity.getPlacesMemo());
		this.setId(entity.getPlaceAllId());
		if (entity.getLastUpdateBy() != null) {
			this.setModifiedBy(entity.getLastUpdateBy().getAccount());
		}
		if (entity.getCreatedBy() != null) {
			this.setCreatedBy(entity.getCreatedBy().getAccount());
		}

		if (entity.getDateCreated() != null) {
			this.setActivityDate(sdf.format(entity.getDateCreated()));
		}
		this.setPlaceName(entity.getPlaceName());		
		
		if (entity.getLogicalDelete() != null
				&& entity.getLogicalDelete()) {
			this.setAction(NewsActionType.DELETED.toString());	
			this.setActivityDate(sdf.format(entity.getLastUpdate()));
		} else if (entity.getLastUpdate() == null) {
			this.setAction(NewsActionType.CREATED.toString());
			this.setActivityDate(sdf.format(entity.getDateCreated()));
		} else if (entity.getLastUpdate() != null
				&& entity.getLastUpdate().after(entity.getDateCreated())) {
			this.setAction(NewsActionType.MODIFIED.toString());
			this.setActivityDate(sdf.format(entity.getLastUpdate()));
		} else {
			this.setAction(NewsActionType.CREATED.toString());
			this.setActivityDate(sdf.format(entity.getDateCreated()));
		}

		this.setPlaceNameFull(entity.getPlaceNameFull());
		this.setPlType(entity.getPlType());
		this.setPrefFlag(entity.getPrefFlag());

		
		// DateTime dt1 = new DateTime(place.getDateCreated());
		// DateTime dt2 = new DateTime(new Date());
		// Days.daysBetween(dt1,dt2).getDays();

		return this;
	}
}