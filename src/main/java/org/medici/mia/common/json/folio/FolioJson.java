package org.medici.mia.common.json.folio;

import java.io.Serializable;

import org.medici.mia.domain.FolioEntity;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class FolioJson implements Serializable {

	private static final long serialVersionUID = 1199199301715371481L;

	@JsonProperty("folioId")
	private Integer folioId;

	@JsonProperty("folioNumber")
	private String folioNumber;

	@JsonProperty("rectoverso")
	private String rectoverso;

	@JsonProperty("uploadFileId")
	private Integer uploadFileId;

	private String imgName;

	// property used from FE
	private Boolean noNumb;
	
	private Boolean canView;
	

	public Boolean getCanView() {
		return canView;
	}

	public void setCanView(Boolean canView) {
		this.canView = canView;
	}

	public Integer getFolioId() {
		return folioId;
	}

	public void setFolioId(Integer folioId) {
		this.folioId = folioId;
	}

	public String getFolioNumber() {
		return folioNumber;
	}

	public void setFolioNumber(String folioNumber) {
		this.folioNumber = folioNumber;
	}

	public String getRectoverso() {
		return rectoverso;
	}

	public void setRectoverso(String rectoverso) {
		this.rectoverso = rectoverso;
	}

	public Integer getUploadFileId() {
		return uploadFileId;
	}

	public void setUploadFileId(Integer uploadFileId) {
		this.uploadFileId = uploadFileId;
	}

	public Boolean getNoNumb() {
		return noNumb;
	}

	public void setNoNumb(Boolean noNumb) {
		this.noNumb = noNumb;
	}

	public String getImgName() {
		return imgName;
	}

	public void setImgName(String imgName) {
		this.imgName = imgName;
	}

	public FolioJson toJson(FolioEntity folio) {

		this.setFolioId(folio.getFolioId());
		this.setFolioNumber(folio.getFolioNumber());
		this.setRectoverso(folio.getRectoverso());
		this.setNoNumb(folio.getNoNumb());
		this.setUploadFileId(folio.getUploadedFileId());
		if (folio.getUploadFileEntity() != null) {
			this.setImgName(folio.getUploadFileEntity().getFileOriginalName());
		}

		return this;
	}

	public FolioEntity toEntity(FolioEntity folioEntity) {

		folioEntity.setFolioId(this.getFolioId());
		if(this.getFolioNumber()!=null && this.getFolioNumber().equals("")){
			folioEntity.setFolioNumber(null);
		}else{
			folioEntity.setFolioNumber(this.getFolioNumber());
		}
		
		if(this.getRectoverso()!=null && this.getRectoverso().equals("")){
			folioEntity.setRectoverso(null);
		}else{
			folioEntity.setRectoverso(this.getRectoverso());
		}

		if (this.getNoNumb() == null || this.getNoNumb() == false) {
			folioEntity.setNoNumb(Boolean.FALSE);
		} else {
			folioEntity.setNoNumb(Boolean.TRUE);
		}
		folioEntity.setUploadedFileId(this.getUploadFileId());

		return folioEntity;

	}

}
