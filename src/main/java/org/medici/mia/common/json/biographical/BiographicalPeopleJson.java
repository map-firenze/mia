package org.medici.mia.common.json.biographical;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.medici.mia.common.util.DateUtils;
import org.medici.mia.domain.BiographicalPeople;
import org.medici.mia.domain.BiographicalPeople.Gender;

public class BiographicalPeopleJson implements Serializable {

	private transient final long serialVersionUID = 1L;
	private transient final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd hh:mm:ss");

	private Integer peopleId;
	// todo: test git
	private String unsure;
	private String mapNameLf;
	private Gender gender;
	// new values for findPeople response
	private String activeStart;
	private String activeEnd;
	private Integer bornYear;
	private Integer deathYear;

	private String firstName;
	private String lastName;
	private String lastPrefix;
	private String sucNum;
	private Integer bornMonth;
	private Integer bornDay;
	private Integer bornPlaceId;
	private Integer deathMonth;
	private Integer deathDay;
	private Integer deathPlaceId;
	private String middleName;
	private String middlePrefix;
	private String portraitImageName;
	private String postLast;
	private String postLastPrefix;
	private Boolean bornApprox;// [Boolean (0,1)]
	private Boolean bornDateBc;// [Boolean (0,1)]
	private Boolean bornPlaceUnsure;// [Boolean (0,1)]
	private Boolean deathApprox;// [Boolean (0,1)]
	private Boolean deathDateBc;// [Boolean (0,1)]
	private Boolean deathPlaceUnsure;// [Boolean (0,1)]
	private Boolean Portrait;// [Boolean (0,1)]
	private Boolean logicalDelete;// [Boolean (0,1)]
	private String status;
	private String portraitAuthor;
	private String portraitSubject;
	private String createdBy;
	private String dateCreated;
	private String lastUpdateBy;
	private String lastUpdate;
	private Integer commentsCount;

	public BiographicalPeopleJson toJson(BiographicalPeople people) {

		this.setPeopleId(people.getPersonId());
		this.setMapNameLf(people.getMapNameLf());
		this.setGender(people.getGender());
		this.setActiveStart(people.getActiveStart());
		this.setActiveEnd(people.getActiveEnd());
		this.setBornYear(people.getBornYear());
		this.setDeathYear(people.getDeathYear());
		this.setFirstName(people.getFirst());
		this.setLastName(people.getLast());
		this.setLastPrefix(people.getLastPrefix());
		this.setSucNum(people.getSucNum());
		// Biographical fields
		this.setBornApprox(people.getBornApprox());
		this.setBornDateBc(people.getBornDateBc());
		this.setBornDay(people.getBornDay());
	 	this.setBornMonth(people.getBornMonthNum());
		this.setBornPlaceId(people.getBornPlaceId());
		this.setBornPlaceUnsure(people.getBornPlaceUnsure());
		this.setCreatedBy(people.getCreatedBy());

		// if (people.getDateCreated() != null) {
		// Calendar calendar = new GregorianCalendar();
		// calendar.setTime(people.getDateCreated());
		// this.setDateCreated(calendar.get(Calendar.YEAR));
		// }
		// Issue 408
		if (people.getDateCreated() != null) {
			this.setDateCreated(sdf.format(people.getDateCreated()));
		}
		this.setDeathApprox(people.getDeathApprox());
		this.setLogicalDelete(people.getLogicalDelete());
		this.setDeathDateBc(people.getDeathDateBc());
		this.setDeathDay(people.getDeathDay());
		this.setDeathMonth(people.getDeathMonthNum());
		this.setDeathPlaceId(people.getDeathPlaceId());
		this.setDeathPlaceUnsure(people.getDeathPlaceUnsure());
		// if (people.getLastUpdate() != null) {
		// Calendar calendar = new GregorianCalendar();
		// calendar.setTime(people.getLastUpdate());
		// this.setLastUpdate(calendar.get(Calendar.YEAR));
		// }

		// Issue 408
		if (people.getLastUpdate() != null) {
			this.setLastUpdate(sdf.format(people.getLastUpdate()));
		}
		this.setLastUpdateBy(people.getLastUpdateBy());
		this.setMiddleName(people.getMiddle());
		this.setMiddlePrefix(people.getMidPrefix());
		this.setPortrait(people.getPortrait());
		this.setPortraitAuthor(people.getPortraitAuthor());
		this.setPortraitSubject(people.getPortraitSubject());
		this.setPortraitImageName(people.getPortraitImageName());
		this.setPostLast(people.getPostLast());
		this.setPostLastPrefix(people.getPostLastPrefix());
		this.setStatus(people.getStatus());
		this.setCommentsCount(people.getCommentsCount());
		

		return this;
	}

	public BiographicalPeople toEntity(BiographicalPeople peopleEntity) {

		// Issue 416
		// JSON: mapNameLfStr -> $lastName, $firstName $sucNum $middlePrefix, $middleName
		// ($postLasPrefix $postLast)

		//DB: '$last' , '$FIRST' '$SUCNUM' '$midprefix' '$middle' '$lastprefix'
		// ('$postlastprefix $postlast)
		StringBuilder mapNameLfStr = new StringBuilder();
		mapNameLfStr.append(this.getLastName());
		mapNameLfStr.append(", ");
		mapNameLfStr.append(this.getFirstName());

		// $sucNum
		if (this.getSucNum() != null && !this.getSucNum().equals("")) {
			mapNameLfStr.append(" " + this.getSucNum());			
		}

		// $middlePrefix
		if (this.getMiddlePrefix() != null
				&& !this.getMiddlePrefix().equals("")) {
			mapNameLfStr.append(" " + this.getMiddlePrefix());
		}

		// $middleName
		if (this.getMiddleName() != null && !this.getMiddleName().equals("")) {
			mapNameLfStr.append(" " + this.getMiddleName());
		}

		// $postLasPrefix
		if (this.getPostLastPrefix() != null
				&& !this.getPostLastPrefix().equals("")) {
			mapNameLfStr.append(" " + this.getPostLastPrefix());
		}

		// $postLast
		if (this.getPostLast() != null && !this.getPostLast().equals("")) {
			mapNameLfStr.append(" " + this.getPostLast());
		}

		// StringBuilder mapNameLfStr = new StringBuilder();
		// mapNameLfStr.append(this.getLastName());
		// mapNameLfStr.append(", ");
		// mapNameLfStr.append(this.getFirstName());
		// if (this.getSucNum() != null && !this.getSucNum().equals("")) {
		// mapNameLfStr.append(" " + this.getSucNum());
		// }
		// if (this.getLastPrefix() != null && !this.getLastPrefix().equals(""))
		// {
		// mapNameLfStr.append(" " + this.getLastPrefix());
		// }
		//

		if (!mapNameLfStr.toString().isEmpty()) {
			peopleEntity.setMapNameLf(mapNameLfStr.toString());
		} else {
			peopleEntity.setMapNameLf(this.getMapNameLf());
		}

		peopleEntity.setFirst(this.getFirstName());
		peopleEntity.setLast(this.getLastName());
		peopleEntity.setGender(this.getGender());
		peopleEntity.setLastPrefix(this.getLastPrefix());
		peopleEntity.setSucNum(this.getSucNum());		
		// Biographical fields
		peopleEntity.setBornApprox(this.getBornApprox());
		peopleEntity.setBornDateBc(this.getBornDateBc());
		peopleEntity.setBornDay(this.getBornDay());
		peopleEntity.setBornMonthNum(this.getBornMonth());
		peopleEntity.setBornYear(this.getBornYear());

		peopleEntity.setBornPlaceId(this.getBornPlaceId());
		peopleEntity.setBornPlaceUnsure(this.getBornPlaceUnsure());
		peopleEntity.setCreatedBy(this.getCreatedBy());

		// Date date = new Date();
		// date.setYear(this.getDateCreated());
		// peopleEntity.setDateCreated(date);
		// Issue 408
		try {
			if (this.getDateCreated() != null) {
				peopleEntity.setDateCreated(sdf.parse(this.getDateCreated()));
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		peopleEntity.setDeathApprox(this.getDeathApprox());
		peopleEntity.setDeathDateBc(this.getDeathDateBc());
		peopleEntity.setDeathDay(this.getDeathDay());
		peopleEntity.setDeathMonthNum(this.getDeathMonth());
		
		peopleEntity.setDeathYear(this.getDeathYear());
		peopleEntity.setDeathDate(DateUtils.getLuceneDate(this.getDeathYear(), this.getDeathMonth(), this.getDeathDay()));
		peopleEntity.setBornDate(DateUtils.getLuceneDate(this.getBornYear(), this.getBornMonth(), this.getBornDay()));
		
		peopleEntity.setDeathMonthNum(this.getDeathMonth());
		peopleEntity.setDeathPlaceId(this.getDeathPlaceId());
		peopleEntity.setDeathPlaceUnsure(this.getDeathPlaceUnsure());

		peopleEntity.setActiveEnd(this.getActiveEnd());
		peopleEntity.setActiveStart(this.getActiveStart());

		// Date dateLast = new Date();
		// dateLast.setYear(this.getLastUpdate());
		// peopleEntity.setLastUpdate(dateLast);
		// Issue 408

		try {
			if (this.getLastUpdate() != null) {
				peopleEntity.setLastUpdate(sdf.parse(this.getLastUpdate()));
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		peopleEntity.setLastUpdateBy(this.getLastUpdateBy());

		peopleEntity.setMiddle(this.getMiddleName());
		peopleEntity.setMidPrefix(this.getMiddlePrefix());
		peopleEntity.setPortrait(this.getPortrait());
		peopleEntity.setPortraitAuthor(this.getPortraitAuthor());
		peopleEntity.setPortraitSubject(this.getPortraitSubject());
		peopleEntity.setPortraitImageName(this.getPortraitImageName());
		peopleEntity.setPostLast(this.getPostLast());
		peopleEntity.setPostLastPrefix(this.getPostLastPrefix());
		peopleEntity.setStatus(this.getStatus());

		return peopleEntity;

	}

	public Integer getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(Integer peopleId) {
		this.peopleId = peopleId;
	}

	public String getUnsure() {
		return unsure;
	}

	public void setUnsure(String unsure) {
		this.unsure = unsure;
	}

	public String getMapNameLf() {
		return mapNameLf;
	}

	public void setMapNameLf(String mapNameLf) {
		this.mapNameLf = mapNameLf;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getActiveStart() {
		return activeStart;
	}

	public void setActiveStart(String activeStart) {
		this.activeStart = activeStart;
	}

	public String getActiveEnd() {
		return activeEnd;
	}

	public void setActiveEnd(String activeEnd) {
		this.activeEnd = activeEnd;
	}

	public Integer getBornYear() {
		return bornYear;
	}

	public void setBornYear(Integer bornYear) {
		this.bornYear = bornYear;
	}

	public Integer getDeathYear() {
		return deathYear;
	}

	public void setDeathYear(Integer deathYear) {
		this.deathYear = deathYear;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastPrefix() {
		return lastPrefix;
	}

	public void setLastPrefix(String lastPrefix) {
		this.lastPrefix = lastPrefix;
	}

	public String getSucNum() {
		return sucNum;
	}

	public void setSucNum(String sucNum) {
		this.sucNum = sucNum;
	}

	public Integer getBornMonth() {
		return bornMonth;
	}

	public void setBornMonth(Integer bornMonth) {
		this.bornMonth = bornMonth;
	}

	public Integer getBornDay() {
		return bornDay;
	}

	public void setBornDay(Integer bornDay) {
		this.bornDay = bornDay;
	}

	public Integer getBornPlaceId() {
		return bornPlaceId;
	}

	public void setBornPlaceId(Integer bornPlaceId) {
		this.bornPlaceId = bornPlaceId;
	}

	public Integer getDeathMonth() {
		return deathMonth;
	}

	public void setDeathMonth(Integer deathMonth) {
		this.deathMonth = deathMonth;
	}

	public Integer getDeathDay() {
		return deathDay;
	}

	public void setDeathDay(Integer deathDay) {
		this.deathDay = deathDay;
	}

	public Integer getDeathPlaceId() {
		return deathPlaceId;
	}

	public void setDeathPlaceId(Integer deathPlaceId) {
		this.deathPlaceId = deathPlaceId;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getMiddlePrefix() {
		return middlePrefix;
	}

	public void setMiddlePrefix(String middlePrefix) {
		this.middlePrefix = middlePrefix;
	}

	public String getPortraitImageName() {
		return portraitImageName;
	}

	public void setPortraitImageName(String portraitImageName) {
		this.portraitImageName = portraitImageName;
	}

	public String getPostLast() {
		return postLast;
	}

	public void setPostLast(String postLast) {
		this.postLast = postLast;
	}

	public String getPostLastPrefix() {
		return postLastPrefix;
	}

	public void setPostLastPrefix(String postLastPrefix) {
		this.postLastPrefix = postLastPrefix;
	}

	public Boolean getBornApprox() {
		return bornApprox;
	}

	public void setBornApprox(Boolean bornApprox) {
		this.bornApprox = bornApprox;
	}

	public Boolean getBornDateBc() {
		return bornDateBc;
	}

	public void setBornDateBc(Boolean bornDateBc) {
		this.bornDateBc = bornDateBc;
	}

	public Boolean getBornPlaceUnsure() {
		return bornPlaceUnsure;
	}

	public void setBornPlaceUnsure(Boolean bornPlaceUnsure) {
		this.bornPlaceUnsure = bornPlaceUnsure;
	}

	public Boolean getDeathApprox() {
		return deathApprox;
	}

	public void setDeathApprox(Boolean deathApprox) {
		this.deathApprox = deathApprox;
	}

	public Boolean getDeathDateBc() {
		return deathDateBc;
	}

	public void setDeathDateBc(Boolean deathDateBc) {
		this.deathDateBc = deathDateBc;
	}

	public Boolean getDeathPlaceUnsure() {
		return deathPlaceUnsure;
	}

	public void setDeathPlaceUnsure(Boolean deathPlaceUnsure) {
		this.deathPlaceUnsure = deathPlaceUnsure;
	}

	public Boolean getPortrait() {
		return Portrait;
	}

	public Boolean getLogicalDelete() {
		return logicalDelete;
	}

	public void setLogicalDelete(Boolean logicalDelete) {
		this.logicalDelete = logicalDelete;
	}

	public void setPortrait(Boolean portrait) {
		Portrait = portrait;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPortraitAuthor() {
		return portraitAuthor;
	}

	public void setPortraitAuthor(String portraitAuthor) {
		this.portraitAuthor = portraitAuthor;
	}

	public String getPortraitSubject() {
		return portraitSubject;
	}

	public void setPortraitSubject(String portraitSubject) {
		this.portraitSubject = portraitSubject;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Integer getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(Integer commentsCount) {
		this.commentsCount = commentsCount;
	}
	
	
}