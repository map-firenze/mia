package org.medici.mia.common.json.biographical;

import java.io.Serializable;

public class ModifyPersonParentsJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer personId;
	private Integer newMotherId;
	private Integer newFatherId;
	private Integer oldMotherId;
	private Integer oldFatherId;
	private Integer parentToBeDeleted;

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public Integer getNewMotherId() {
		return newMotherId;
	}

	public void setNewMotherId(Integer newMotherId) {
		this.newMotherId = newMotherId;
	}

	public Integer getNewFatherId() {
		return newFatherId;
	}

	public void setNewFatherId(Integer newFatherId) {
		this.newFatherId = newFatherId;
	}

	public Integer getOldMotherId() {
		return oldMotherId;
	}

	public void setOldMotherId(Integer oldMotherId) {
		this.oldMotherId = oldMotherId;
	}

	public Integer getOldFatherId() {
		return oldFatherId;
	}

	public void setOldFatherId(Integer oldFatherId) {
		this.oldFatherId = oldFatherId;
	}

	public Integer getParentToBeDeleted() {
		return parentToBeDeleted;
	}

	public void setParentToBeDeleted(Integer parentToBeDeleted) {
		this.parentToBeDeleted = parentToBeDeleted;
	}

}