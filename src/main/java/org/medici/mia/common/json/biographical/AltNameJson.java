package org.medici.mia.common.json.biographical;

import java.io.Serializable;

import org.medici.mia.domain.AltName;

public class AltNameJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer altNameId;
	private String altName;
	private String nameType;

	public AltNameJson toJson(AltName altname) {
		if (altname != null) {
			this.setAltName(altname.getAltName());
			this.setNameType(altname.getNameType());
			this.setAltNameId(altname.getNameId());
		}

		return this;
	}

	public Integer getAltNameId() {
		return altNameId;
	}

	public void setAltNameId(Integer altNameId) {
		this.altNameId = altNameId;
	}

	public String getAltName() {
		return altName;
	}

	public void setAltName(String altName) {
		this.altName = altName;
	}

	public String getNameType() {
		return nameType;
	}

	public void setNameType(String nameType) {
		this.nameType = nameType;
	}

}