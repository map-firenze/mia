package org.medici.mia.common.json.biographical;


public enum SpouseType {
	
		H("h"), W("w");

		private final String type;

		private SpouseType(String value) {
			type = value;
		}

		@Override
		public String toString() {
			return type;
		}
	}



