package org.medici.mia.common.json.historylog;

import java.io.Serializable;

import org.medici.mia.domain.HistoryLogEntity;

public class RegisterLogActionJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private String recordType;
	private String action;
	private Integer entryId;

	public RegisterLogActionJson toJson(HistoryLogEntity entity) {
		if (entity != null) {
			this.setEntryId(entity.getEntryId());
			if (entity.getRecordType() != null) {
				this.setRecordType(entity.getRecordType().toString());
			}
			if (entity.getAction() != null) {
				this.setAction(entity.getAction().toString());
			}

		}

		return this;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Integer getEntryId() {
		return entryId;
	}

	public void setEntryId(Integer entryId) {
		this.entryId = entryId;
	}

}