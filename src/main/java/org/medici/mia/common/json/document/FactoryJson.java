package org.medici.mia.common.json.document;

import org.medici.mia.common.json.ChineseSynopsisJson;
import org.medici.mia.common.json.RelatedDocsJson;
import org.medici.mia.common.json.SynopsisJson;
import org.medici.mia.service.miadoc.JsonType;

public class FactoryJson {
	public static IModifyDocJson getJson(String jsonName) {
		if (JsonType.transcription.toString().equalsIgnoreCase(jsonName)) {
			return new DocumentTranscriptionJson();
		} else if (JsonType.synopsis.toString().equalsIgnoreCase(jsonName)) {
			return new SynopsisJson();
		} else if (JsonType.chineseSynopsis.toString().equalsIgnoreCase(
				jsonName)) {
			return new ChineseSynopsisJson();
		} else if (JsonType.basicDescription.toString().equalsIgnoreCase(
				jsonName)) {
			return new BasicDescriptionJson();
		} else if (JsonType.category.toString().equalsIgnoreCase(jsonName)) {
			return new CategoryJson();
		} else if (JsonType.documentLanguage.toString().equalsIgnoreCase(
				jsonName)) {
			return new DocumentLanguageJson();
		} else if (JsonType.topic.toString().equalsIgnoreCase(jsonName)) {
			return new TopicPlaceJson();
		} else if (JsonType.relatedDocs.toString().equalsIgnoreCase(jsonName)) {
			return new RelatedDocsJson();
		}

		return null;
	}

}
