package org.medici.mia.common.json.advancedsearch;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@JsonTypeName("headquartersAdvancedSearch")
public class HeadquartersAdvancedSearchJson extends GenericAdvancedSearchJson {

	private static final long serialVersionUID = 1L;

	@JsonProperty("personId")
	Integer personId;

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	@JsonIgnore
	@Override
	public List<String> getQueries() {

		List<String> queries = new ArrayList<String>();
		StringBuffer b = new StringBuffer();
		b.append("select distinct personId FROM People p, OrganizationEntity oe WHERE oe.personId=p.personId AND oe.personId = ");
		b.append(this.getPersonId());
		b.append(" AND p.logicalDelete=false");

		queries.add(b.toString());
		return queries;

	}
}