package org.medici.mia.common.json.comment;

import com.github.underscore.Predicate;
import com.github.underscore.U;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import org.medici.mia.domain.Comment;
import org.medici.mia.domain.User;
import org.medici.mia.domain.UserAuthority;
import org.medici.mia.domain.UserRole;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CommentJson implements Serializable {

    private transient static DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final long serialVersionUID = -3901477347126273807L;
    private Integer id;
    private String username;
    private String createdAt;
    private String firstName;
    private String lastName;
    private String text;
    private String title;
    private Boolean logicalDelete;
    private Integer parentId;
    private String modifiedAt;
    private String photo;
    private HashMap<String, Object> repliedTo;
    private Integer childrenCount = 0;
    private Boolean canUndelete;
    private String mentions;
    private Boolean canReport;

    public CommentJson() {
    }

    public CommentJson toJson(Comment c, User u){
        CommentJson j = new CommentJson();
        j.setTitle(c.getTitle());
        j.setId(c.getId());
        j.setCreatedAt(c.getCreatedAt() == null ? null : sdf.format(c.getCreatedAt()));
        j.setModifiedAt(c.getModifiedAt() == null ? null : sdf.format(c.getModifiedAt()));
        j.setUsername(c.getCreatedBy() == null ? null : c.getCreatedBy().getAccount());
        j.setFirstName(c.getCreatedBy() == null ? null : c.getCreatedBy().getFirstName());
        j.setLastName(c.getCreatedBy() == null ? null : c.getCreatedBy().getLastName());
        j.setLogicalDelete(c.getLogicalDelete());
        j.setParentId(c.getParent() == null ? null : c.getParent().getId());
        j.setText(c.getText());
        j.setMentions(c.getMentions());
        if(c.getRepliedTo() != null){
            HashMap<String, Object> target = new HashMap<String, Object>();
            target.put("id", c.getRepliedTo().getId());
            target.put("username", c.getRepliedTo().getCreatedBy() == null ? null : c.getRepliedTo().getCreatedBy().getAccount());
            target.put("firstName", c.getRepliedTo().getCreatedBy() == null ? null : c.getRepliedTo().getCreatedBy().getFirstName());
            target.put("lastName", c.getRepliedTo().getCreatedBy() == null ? null : c.getRepliedTo().getCreatedBy().getLastName());
            target.put("logicalDelete", c.getRepliedTo().getLogicalDelete());
            j.setRepliedTo(target);
        }
        boolean showDeleted = false;
        for (UserRole role : u.getUserRoles()) {
            showDeleted = role.containsAuthority(UserAuthority.Authority.ADMINISTRATORS)
                    || role.containsAuthority(UserAuthority.Authority.COMMUNITY_COORDINATORS)
                    || role.containsAuthority(UserAuthority.Authority.ONSITE_FELLOWS);
            if(showDeleted) break;
        }
        if(!showDeleted){
            for(Comment child: c.getChildren()){
                if(!child.getLogicalDelete()){
                    j.setChildrenCount(j.getChildrenCount() + 1);
                    continue;
                }
                if(child.getCreatedBy().getAccount().equals(u.getAccount())){
                    j.setChildrenCount(j.getChildrenCount() + 1);
                }
            }
        } else {
            j.setChildrenCount(c.getChildren().size());
        }
        j.setCanUndelete(false);
        if(c.getDeletedBy() != null) {
            for (UserRole role : u.getUserRoles()) {
                j.setCanUndelete(role.containsAuthority(UserAuthority.Authority.ADMINISTRATORS) || role.containsAuthority(UserAuthority.Authority.COMMUNITY_COORDINATORS));
                if(j.getCanUndelete()) break;
            }
            if(!j.getCanUndelete()){
                if(u.getAccount().equals(c.getDeletedBy().getAccount())) {
                    j.setCanUndelete(true);
                }
            }
        }

        try {
            if(u.getAccount().equals(c.getCreatedBy().getAccount())){
                j.setCanReport(false);
            } else {
                Gson gson = new Gson();
                Type listOfStrings = new TypeToken<List<String>>() {
                }.getType();
                List<String> reportedBy = gson.fromJson(c.getReportedBy(), listOfStrings);
                final String userName = u.getAccount();
                if (reportedBy != null && !reportedBy.isEmpty() && U.any(reportedBy, new Predicate<String>() {
                    @Override
                    public boolean test(String reporter) {
                        return reporter.equalsIgnoreCase(userName);
                    }
                }) || c.getLogicalDelete()) {
                    j.setCanReport(false);
                } else {
                    j.setCanReport(true);
                }
            }
        } catch (JsonSyntaxException e){
            j.setCanReport(true);
        }
        return j;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(String modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public Boolean getLogicalDelete() {
        return logicalDelete;
    }

    public void setLogicalDelete(Boolean logicalDelete) {
        this.logicalDelete = logicalDelete;
    }

    public HashMap<String, Object> getRepliedTo() {
        return repliedTo;
    }

    public void setRepliedTo(HashMap<String, Object> repliedTo) {
        this.repliedTo = repliedTo;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Integer getChildrenCount() {
        return childrenCount;
    }

    public void setChildrenCount(Integer childrenCount) {
        this.childrenCount = childrenCount;
    }

    public Boolean getCanUndelete() {
        return canUndelete;
    }

    public void setCanUndelete(Boolean canUndelete) {
        this.canUndelete = canUndelete;
    }

    public String getMentions() {
        return mentions;
    }

    public void setMentions(String mentions) {
        this.mentions = mentions;
    }

    public Boolean getCanReport() {
        return canReport;
    }

    public void setCanReport(Boolean canReport) {
        this.canReport = canReport;
    }
}
