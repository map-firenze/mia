/*
 * placesSearchJson.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.common.json.advancedsearch;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.util.DateUtils;
import org.medici.mia.domain.User;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@JsonTypeName("documentOwnerAdvancedSearch")
public class DocumentOwnerAdvancedSearchJson extends GenericAdvancedSearchJson {

	private static final long serialVersionUID = 1L;

	@JsonProperty("editType")
	private String editType;

	@JsonProperty("account")
	private String account;

	public String getEditType() {
		return editType;
	}

	public void setEditType(String editType) {
		this.editType = editType;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	@JsonIgnore
	@Override
	public List<String> getQueries() {
		// select * from tblDocumentEnts where docyear BETWEEN '1899' AND '1905'
		// and docMonth BETWEEN '3' and '9' and docDay BETWEEN '10' and '12';
		if (this.getEditType().equalsIgnoreCase(
				DocumentOwnerAdvancedSearchType.OWNER.getValue())
				|| this.getEditType().equalsIgnoreCase(
						DocumentOwnerAdvancedSearchType.AUTHOR.getValue())) {
			return getQueriesCreatedBy();
		} else if (this.getEditType().equalsIgnoreCase(
				DocumentOwnerAdvancedSearchType.EDITOR.getValue())) {
			return getQueriesEditor();

		} else {
			return getQueriesCreatedBy();
		}

	}

	private List<String> getQueriesCreatedBy() {
		// select * from tblDocumentEnts where docyear BETWEEN '1899' AND '1905'
		// and docMonth BETWEEN '3' and '9' and docDay BETWEEN '10' and '12';
		List<String> queries = new ArrayList<String>();
		StringBuffer b = new StringBuffer();
		b.append("select distinct documentEntityId documentId from tblDocumentEnts d where d.createdBy = ");
		b.append("'");
		b.append(this.getAccount());
		b.append("'");
		b.append(" and  d.flgLogicalDelete != 1");
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		queries.add(b.toString());

		return queries;
	}

	private List<String> getQueriesEditor() {
		List<String> queries = new ArrayList<String>();
		StringBuffer b = new StringBuffer();
		b.append("select distinct documentEntityId documentId from tblDocumentEnts where LastUpdateBy = ");
		b.append("'");
		b.append(this.getAccount());
		b.append("'");
		b.append(" and  flgLogicalDelete != 1");
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((dateCreated > '%s' AND createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (dateLastUpdate > '%s' AND LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}
		
		queries.add(b.toString());

		return queries;
	}

}
