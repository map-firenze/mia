package org.medici.mia.common.json;

import java.io.Serializable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public class TopicBaseJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer placeId;
	private Integer topicListId;

	public TopicBaseJson() {

	}

	public Integer getPlaceId() {
		return placeId;
	}

	public void setPlaceId(Integer placeId) {
		this.placeId = placeId;
	}

	public Integer getTopicListId() {
		return topicListId;
	}

	public void setTopicListId(Integer topicListId) {
		this.topicListId = topicListId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((placeId == null) ? 0 : placeId.hashCode());
		result = prime * result
				+ ((topicListId == null) ? 0 : topicListId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TopicBaseJson other = (TopicBaseJson) obj;
		if (placeId == null) {
			if (other.placeId != null)
				return false;
		} else if (!placeId.equals(other.placeId))
			return false;
		if (topicListId == null) {
			if (other.topicListId != null)
				return false;
		} else if (!topicListId.equals(other.topicListId))
			return false;
		return true;
	}

}
