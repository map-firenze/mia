package org.medici.mia.common.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.medici.mia.domain.User;
import org.medici.mia.domain.UserRole;

public class UserProfileJson implements Serializable {

	private final Logger logger = Logger.getLogger(this.getClass());

	private static final long serialVersionUID = 1L;

	private String account;

	private String title;

	private String firstName;

	private String lastName;

	private String email;

	private String address;

	private String interests;

	private String city;

	private String country;

	private String countryName;

	private String affiliation;

	private String photo;

	private String photoPath;

	private String portraitImageName;

	private String resume;

	private String resumePath;

	private List<String> userGroups;

	private String academiaEduLink;

	private Boolean mailNotification;
	
	private Boolean hideActivities;
	
	private Boolean hidePublications;
	
	private Boolean hideStatistics;
	
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getInterests() {
		return interests;
	}

	public void setInterests(String interests) {
		this.interests = interests;
	}

	public List<String> getUserGroups() {
		if(userGroups == null)
			userGroups = new ArrayList<String>();
		return userGroups;
	}

	public void setUserGroups(List<String> userGroups) {
		this.userGroups = userGroups;
	}

	public String getAffiliation() {
		return affiliation;
	}

	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public String getResume() {
		return resume;
	}

	public void setResume(String resume) {
		this.resume = resume;
	}

	public String getResumePath() {
		return resumePath;
	}

	public void setResumePath(String resumePath) {
		this.resumePath = resumePath;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPortraitImageName() {
		return portraitImageName;
	}

	public void setPortraitImageName(String portraitImageName) {
		this.portraitImageName = portraitImageName;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getAcademiaEduLink() {
		return academiaEduLink;
	}

	public void setAcademiaEduLink(String academiaEduLink) {
		this.academiaEduLink = academiaEduLink;
	}

	public Boolean getMailNotification() {
		return mailNotification;
	}
	
	public void setMailNotification(Boolean mailNotification) {
		this.mailNotification = mailNotification;
	}
	
	public Boolean getHideActivities() {
		return hideActivities;
	}

	public void setHideActivities(Boolean hideActivities) {
		this.hideActivities = hideActivities;
	}

	public Boolean getHidePublications() {
		return hidePublications;
	}

	public void setHidePublications(Boolean hidePublications) {
		this.hidePublications = hidePublications;
	}

	public Boolean getHideStatistics() {
		return hideStatistics;
	}

	public void setHideStatistics(Boolean hideStatistics) {
		this.hideStatistics = hideStatistics;
	}

	public UserProfileJson toJson(User user) {

		this.setAccount(user.getAccount());
		this.setTitle(user.getTitle());
		this.setFirstName(user.getFirstName());
		this.setLastName(user.getLastName());
		this.setEmail(user.getMail());
		this.setAddress(user.getAddress());
		this.setInterests(user.getInterests());
		this.setCity(user.getCity());
		this.setCountry(user.getCountry());
		this.setCountryName(user.getCountryName());
		this.setAffiliation(user.getOrganization());
		this.setPortraitImageName(user.getPortraitImageName());
		this.setAcademiaEduLink(user.getAcademiaEduLink());
		this.setMailNotification(user.getMailNotification());
		this.setHideActivities(user.getHideActivities());
		this.setHidePublications(user.getHidePublications());
		this.setHideStatistics(user.getHideStatistics());
		
		if (user.getUserRoles() != null && user.getUserRoles().size() > 0) {

			List<UserGroupJson> userGroups = new ArrayList<UserGroupJson>();

			for (UserRole userRole : user.getUserRoles()) {
				UserGroupJson userG = new UserGroupJson();
				userG.setGroup(userRole.getUserAuthority().getAuthority()
						.toString());
				userG.setPriority(userRole.getUserAuthority().getPriority());
				userGroups.add(userG);
			}

			Collections.sort(userGroups, UserGroupJson.USER_GROUP_PRIORITY);
			for (UserGroupJson userGroup : userGroups) {
				this.getUserGroups().add(userGroup.getGroup());
			}
		}

		// TODO:implemented in the future
		this.setResume("no");
		this.setResumePath("");

		return this;
	}

}