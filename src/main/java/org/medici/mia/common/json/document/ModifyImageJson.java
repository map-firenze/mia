package org.medici.mia.common.json.document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.medici.mia.common.json.FieldJson;
import org.medici.mia.common.json.PeopleBaseJson;
import org.medici.mia.common.json.PeopleJson;
import org.medici.mia.common.json.PlaceJson;
import org.medici.mia.domain.DocumentFieldEntity;
import org.medici.mia.domain.IGenericEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.NewsEntity;
import org.medici.mia.domain.People;
import org.medici.mia.domain.Place;
import org.medici.mia.domain.TopicPlaceEntity;
import org.medici.mia.service.miadoc.DocumentType;
import org.medici.mia.service.miadoc.MiaDocumentUtils;
import org.medici.mia.service.miadoc.PeopleFieldType;
import org.medici.mia.service.miadoc.PlaceFieldType;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public class ModifyImageJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer documentId;
	private List<Integer> modifyImagesInDe;

	public ModifyImageJson() {

	}

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	public List<Integer> getModifyImagesInDe() {
		return modifyImagesInDe;
	}

	public void setModifyImagesInDe(List<Integer> modifyImagesInDe) {
		this.modifyImagesInDe = modifyImagesInDe;
	}

}
