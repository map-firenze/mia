package org.medici.mia.common.json.search;

import java.io.Serializable;

import org.medici.mia.domain.Place;
import org.medici.mia.domain.PlaceType;

public class PlaceTypeJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer plTypeId;
	private String plTypeDescription;

	public PlaceTypeJson() {
	}

	public void toJson(PlaceType entity) {
		if (entity == null) {
			return;
		}
		this.setPlTypeId(entity.getId());
		this.setPlTypeDescription(entity.getDescription());

	}

	public Integer getPlTypeId() {
		return plTypeId;
	}

	public void setPlTypeId(Integer plTypeId) {
		this.plTypeId = plTypeId;
	}

	public String getPlTypeDescription() {
		return plTypeDescription;
	}

	public void setPlTypeDescription(String plTypeDescription) {
		this.plTypeDescription = plTypeDescription;
	}
	

	
}