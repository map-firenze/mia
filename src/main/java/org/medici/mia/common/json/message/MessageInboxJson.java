package org.medici.mia.common.json.message;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.medici.mia.domain.UserMessage;

public class MessageInboxJson extends MessageJson {

	private static final long serialVersionUID = 1L;
	private final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd hh:mm:ss");

	private String from;
	private String fromCompleteName;
	private String readDate;
	private String sentDate;

	public MessageInboxJson toJson(UserMessage mes) {
		
		super.toJson(mes);

		this.setFrom(mes.getSender());
		this.setFromCompleteName(mes.getUser().getFirstName() + " "
				+ mes.getUser().getLastName());
		if (mes.getReadedDate() != null) {
			this.setReadDate(sdf.format(mes.getReadedDate()));
		}

		if (mes.getSendedDate() != null) {
			this.setSentDate(sdf.format(mes.getSendedDate()));
		}

		return this;
	}

	public UserMessage toEntity(UserMessage mesEntity) {

		
		mesEntity.setSender(this.getFrom());
		mesEntity.setSendedDate(new Date());

		return mesEntity;

	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getFromCompleteName() {
		return fromCompleteName;
	}

	public void setFromCompleteName(String fromCompleteName) {
		this.fromCompleteName = fromCompleteName;
	}

	public String getReadDate() {
		return readDate;
	}

	public void setReadDate(String readDate) {
		this.readDate = readDate;
	}

	public String getSentDate() {
		return sentDate;
	}

	public void setSentDate(String sentDate) {
		this.sentDate = sentDate;
	}
}