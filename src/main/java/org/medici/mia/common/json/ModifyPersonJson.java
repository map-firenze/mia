package org.medici.mia.common.json;

import java.io.Serializable;

import org.medici.mia.common.json.biographical.AltNameJson;

public class ModifyPersonJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer personId;
	private AltNameJson oldPersonName;
	private AltNameJson newPersonName;
	private Integer altNameToBeDeleted;

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public AltNameJson getOldPersonName() {
		return oldPersonName;
	}

	public void setOldPersonName(AltNameJson oldPersonName) {
		this.oldPersonName = oldPersonName;
	}

	public AltNameJson getNewPersonName() {
		return newPersonName;
	}

	public void setNewPersonName(AltNameJson newPersonName) {
		this.newPersonName = newPersonName;
	}

	public Integer getAltNameToBeDeleted() {
		return altNameToBeDeleted;
	}

	public void setAltNameToBeDeleted(Integer altNameToBeDeleted) {
		this.altNameToBeDeleted = altNameToBeDeleted;
	}

}