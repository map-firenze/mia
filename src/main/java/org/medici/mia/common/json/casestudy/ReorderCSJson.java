package org.medici.mia.common.json.casestudy;

import java.util.List;

public class ReorderCSJson {
    private String type;
    private Integer folderId;
    private List<ReorderItemCSJson> entities;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getFolderId() {
        return folderId;
    }

    public void setFolderId(Integer folderId) {
        this.folderId = folderId;
    }

    public List<ReorderItemCSJson> getEntities() {
        return entities;
    }

    public void setEntities(List<ReorderItemCSJson> entities) {
        this.entities = entities;
    }
}
