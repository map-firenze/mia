package org.medici.mia.common.json.message;

import java.util.List;

public class FindOutboxMessageJson {

	private List<MessageOutboxJson> messages;
	private Integer messTotalCount;

	public Integer getMessTotalCount() {
		return messTotalCount;
	}

	public void setMessTotalCount(Integer messTotalCount) {
		this.messTotalCount = messTotalCount;
	}

	public List<MessageOutboxJson> getMessages() {
		return messages;
	}

	public void setMessages(List<MessageOutboxJson> messages) {
		this.messages = messages;
	}

}