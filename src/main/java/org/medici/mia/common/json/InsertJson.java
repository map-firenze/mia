/*
 * MyArchiveInsert.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.common.json;

import java.text.SimpleDateFormat;

import org.medici.mia.domain.InsertEntity;

/**
 * 
 * @author User
 *
 */
public class InsertJson {

	
	private String insertName;
	private String insertId;
	private Integer aeCount;
	
//	private String collectionId;
//	private String collectionName;
//	private String collectionAbbr;
//	private String sezione;
//	private String volumeNo;
//	private String insertNo;
//	private String startYear;
//	private String startMonth;
//	private String startDay;
//	private String endYear;
//	private String endMonth;
//	private String endDay;
//	private String dateNotes;
//	private String organizationalCriteria;
//	private String paginationNotes;
//	private String pagination;
//	private String folioCount;
//	private String missingFolios;
//	private String insertDescription;
//	private String insertTitle;
//	private String otherNotes;
//	private String createdBy;
//	private String dateCreated;
//	private String lastUpdateBy;
//	private String dateLastUpdate;
//	private String deletedBy;    
//	private String dateDeleted;    

//	public InsertJson toJson(InsertEntity insert) {
//		if (insert.getVolumeEntity() != null ) {
//			
//			if (insert.getVolumeEntity().getCollectionEntity() != null) {
//				    this.setCollectionId(insert.getVolumeEntity().getCollectionEntity().getCollectionId().toString());
//					this.setCollectionName(insert.getVolumeEntity().getCollectionEntity().getCollectionName());
//					this.setCollectionAbbr(insert.getVolumeEntity().getCollectionEntity().getCollectionAbbreviation());
//				}
//			this.setSezione(insert.getVolumeEntity().getSezione());
//			this.setVolumeNo(insert.getVolumeEntity().getVolume());
//		}
//		this.setInsertId(insert.getInsertId().toString());
//		this.setInsertNo(insert.getInsertName());
//		this.setStartYear(insert.getStartYear()== null? "": insert.getStartYear().toString());
//		this.setStartMonth(insert.getStartMonth()== null? "": insert.getStartMonth().toString());
//		this.setStartDay(insert.getStartDay()== null? "": insert.getStartDay().toString());
//		this.setEndYear(insert.getEndYear()== null? "": insert.getEndYear().toString());
//		this.setEndMonth(insert.getEndMonth()== null? "": insert.getEndMonth().toString());
//		this.setEndDay(insert.getEndDay()== null? "": insert.getStartYear().toString());
//		this.setDateNotes(insert.getDateNotes());
//		this.setOrganizationalCriteria(insert.getOrganizationalCriteria());
//		this.setPaginationNotes(insert.getPaginationNotes());
//		this.setPagination(insert.getPagination());
//		this.setFolioCount(insert.getFolioCount());
//		this.setMissingFolios(insert.getMissingFolios());
//		this.setInsertDescription(insert.getContext());
//		this.setOtherNotes(insert.getOtherNotes());
//		this.setInsertTitle(insert.getTitle());
//		if (insert.getCreatedBy() != null) {
//			this.setCreatedBy(insert.getCreatedBy().getAccount());
//		}
//		if (insert.getDateCreated() != null) {
//			this.setDateCreated(sdf.format(insert.getDateCreated()));
//		}
//
//		if (insert.getLastUpdateBy() != null) {
//			this.setLastUpdateBy(insert.getLastUpdateBy().getAccount());
//		}
//		if (insert.getDateLastUpdate() != null) {
//			this.setDateLastUpdate(sdf.format(insert.getDateLastUpdate()));
//		}
//		if (insert.getDeletedBy() != null) {
//			this.setDeletedBy(insert.getDeletedBy().getAccount());
//		}
//		if (insert.getDateDeleted() != null) {
//			this.setDateDeleted(sdf.format(insert.getDateDeleted()));
//		}
//		return this;
//	}

//	public InsertEntity toEntity() {
//		InsertEntity insert = new InsertEntity();
//		
//		insert.setInsertId(this.getInsertId() == null ? null: Integer.valueOf(this.getInsertId().toString()));
//		insert.setInsertName(this.getInsertNo());
//		insert.setStartYear(this.getStartYear() == null ? null: Integer.valueOf(this.getStartYear().toString()));
//		insert.setStartMonth(this.getStartMonth() == null ? null: Integer.valueOf(this.getStartMonth().toString()));
//		insert.setStartDay(this.getStartDay()== null? null: Integer.valueOf(this.getStartYear().toString()));
//		insert.setEndYear(this.getEndYear()== null? null: Integer.valueOf(this.getStartYear().toString()));
//		insert.setEndMonth(this.getEndMonth() == null ? null: Integer.valueOf(this.getEndMonth().toString()));
//		insert.setEndDay(this.getEndDay()== null? null: Integer.valueOf(this.getStartYear().toString()));
//		insert.setOrganizationalCriteria(this.getOrganizationalCriteria());
//		insert.setDateNotes(this.getDateNotes());
//		insert.setPaginationNotes(this.getPaginationNotes());
//		insert.setPagination(this.getPagination());
//		insert.setFolioCount(this.getFolioCount());
//		insert.setMissingFolios(this.getMissingFolios());
//		insert.setContext(this.getInsertDescription());
//		insert.setOtherNotes(this.getOtherNotes());
//		insert.setTitle(this.getInsertTitle());
//		
//		return insert;
//	}
	
	public String getInsertName() {
		return insertName;
	}
	public void setInsertName(String insertName) {
		this.insertName = insertName;
	}
	public String getInsertId() {
		return insertId;
	}
	public void setInsertId(String insertId) {
		this.insertId = insertId;
	}
	public Integer getAeCount() {
		return aeCount;
	}
	public void setAeCount(Integer aeCount) {
		this.aeCount = aeCount;
	}

//	public String getCollectionName() {
//		return collectionName;
//	}
//	public void setCollectionName(String collectionName) {
//		this.collectionName = collectionName;
//	}
//	public void setCollectionId(String collectionId) {
//		this.collectionId = collectionId;
//	}
//	public String getCollectionId() {
//		return collectionId;
//	}
//	public String getCollectionAbbr() {
//		return collectionAbbr;
//	}
//	public void setCollectionAbbr(String collectionAbbr) {
//		this.collectionAbbr = collectionAbbr;
//	}
//	public String getSezione() {
//		return sezione;
//	}
//	public void setSezione(String sezione) {
//		this.sezione = sezione;
//	}
//	public String getVolumeNo() {
//		return volumeNo;
//	}
//	public void setVolumeNo(String volumeNo) {
//		this.volumeNo = volumeNo;
//	}
//	public String getInsertNo() {
//		return insertNo;
//	}
//	public void setInsertNo(String insertNo) {
//		this.insertNo = insertNo;
//	}
//	public String getStartYear() {
//		return startYear;
//	}
//	public void setStartYear(String startYear) {
//		this.startYear = startYear;
//	}
//	public String getStartMonth() {
//		return startMonth;
//	}
//	public void setStartMonth(String startMonth) {
//		this.startMonth = startMonth;
//	}
//	public String getStartDay() {
//		return startDay;
//	}
//	public void setStartDay(String startDay) {
//		this.startDay = startDay;
//	}
//	public String getEndYear() {
//		return endYear;
//	}
//	public void setEndYear(String endYear) {
//		this.endYear = endYear;
//	}
//	public String getEndMonth() {
//		return endMonth;
//	}
//	public void setEndMonth(String endMonth) {
//		this.endMonth = endMonth;
//	}
//	public String getEndDay() {
//		return endDay;
//	}
//	public void setEndDay(String endDay) {
//		this.endDay = endDay;
//	}
//	public String getDateNotes() {
//		return dateNotes;
//	}
//	public void setDateNotes(String dateNotes) {
//		this.dateNotes = dateNotes;
//	}
//	public String getOrganizationalCriteria() {
//		return organizationalCriteria;
//	}
//	public void setOrganizationalCriteria(String organizationalCriteria) {
//		this.organizationalCriteria = organizationalCriteria;
//	}
//	public String getPaginationNotes() {
//		return paginationNotes;
//	}
//	public void setPaginationNotes(String paginationNotes) {
//		this.paginationNotes = paginationNotes;
//	}
//	public String getPagination() {
//		return pagination;
//	}
//	public void setPagination(String pagination) {
//		this.pagination = pagination;
//	}
//	public String getFolioCount() {
//		return folioCount;
//	}
//	public void setFolioCount(String folioCount) {
//		this.folioCount = folioCount;
//	}
//	public String getMissingFolios() {
//		return missingFolios;
//	}
//	public void setMissingFolios(String missingFolios) {
//		this.missingFolios = missingFolios;
//	}
//	public String getInsertDescription() {
//		return insertDescription;
//	}
//	public void setInsertDescription(String insertDescription) {
//		this.insertDescription = insertDescription;
//	}
//	public String getOtherNotes() {
//		return otherNotes;
//	}
//	public void setOtherNotes(String otherNotes) {
//		this.otherNotes = otherNotes;
//	}
//	public String getCreatedBy() {
//		return createdBy;
//	}
//	public void setCreatedBy(String createdBy) {
//		this.createdBy = createdBy;
//	}
//	public String getDateCreated() {
//		return dateCreated;
//	}
//	public void setDateCreated(String dateCreated) {
//		this.dateCreated = dateCreated;
//	}
//	public String getLastUpdateBy() {
//		return lastUpdateBy;
//	}
//	public void setLastUpdateBy(String lastUpdateBy) {
//		this.lastUpdateBy = lastUpdateBy;
//	}
//	public String getDateLastUpdate() {
//		return dateLastUpdate;
//	}
//	public void setDateLastUpdate(String dateLastUpdate) {
//		this.dateLastUpdate = dateLastUpdate;
//	}
//	public String getDeletedBy() {
//		return deletedBy;
//	}
//	public void setDeletedBy(String deletedBy) {
//		this.deletedBy = deletedBy;
//	}
//	public String getDateDeleted() {
//		return dateDeleted;
//	}
//	public void setDateDeleted(String dateDeleted) {
//		this.dateDeleted = dateDeleted;
//	}
//
//	public String getInsertTitle() {
//		return insertTitle;
//	}
//
//	public void setInsertTitle(String insertTitle) {
//		this.insertTitle = insertTitle;
//	}
}
