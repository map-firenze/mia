/*
 * PeopleSearchJson.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.common.json.advancedsearch;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.util.DateUtils;
import org.medici.mia.domain.User;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@JsonTypeName("peopleAdvancedSearch")
public class PeopleAdvancedSearchJson extends GenericAdvancedSearchJson {

	private static final long serialVersionUID = 1L;
	private static final String NOT_DELETED_DOC = " d.flgLogicalDelete != 1";
	private static final String UNION = " ) UNION ( ";

	@JsonProperty("people")
	private List<String> people;

	public List<String> getPeople() {
		return people;
	}

	public void setPeople(List<String> people) {
		this.people = people;
	}

	@JsonIgnore
	@Override
	public List<String> getQueries() {
		List<String> queries = new ArrayList<String>();

		for (String person : people) {
			StringBuffer b = new StringBuffer();
			b.append("( ");

			b.append(getProducerQuery(person));
			b.append(UNION);

			b.append(getDocumentRefQuery(person));
			b.append(UNION);

			b.append(getFinancialRecordsQuery(person));
			b.append(UNION);

			b.append(getInventoriesQuery(person));
			b.append(UNION);

			b.append(getLiteraryWorksQuery(person));
			b.append(UNION);

			b.append(getNewsQuery(person));
			b.append(UNION);

			b.append(getNotarialRecordsContractorQuery(person));
			b.append(UNION);

			b.append(getNotarialRecordsContracteeQuery(person));
			b.append(UNION);

			b.append(getOfficialRecordsQuery(person));
			b.append(UNION);

			b.append(getCorrespondenceQuery(person));
			b.append(" )");

			queries.add(b.toString());
		}
		
		return queries;
	}

	private String getProducerQuery(String person) {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocumentsEntsPeople dp where ");

		b.append("( ");
		b.append("dp.producer = ");
		b.append("'");
		b.append(person);
		b.append("'");
		b.append(" )");
		b.append("and dp.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}
	
	private String getDocumentRefQuery(String person) {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocumentEntsRefToPeople dr where ");

		b.append("( ");
		b.append("dr.peopleId = ");
		b.append("'");
		b.append(person);
		b.append("'");
		b.append(" )");
		b.append("and dr.documentId = d.documentEntityId and "
				+ NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getCorrespondenceQuery(String person) {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocCorrespondence c where ");

		b.append("( ");
		b.append("c.recipient like ");
		b.append("'");
		b.append(person + ":%");
		b.append("'");
		b.append(" OR c.recipient like ");
		b.append("'");
		b.append("," + person + ":%");
		b.append("'");
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}
		
		return b.toString();
	}

	private String getFinancialRecordsQuery(String person) {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocFinancialRecords c where ");

		b.append("( ");
		b.append("c.commissionerOfAccount like ");
		b.append("'");
		b.append(person + ":%");
		b.append("'");
		b.append(" OR c.commissionerOfAccount like ");
		b.append("'");
		b.append("," + person + ":%");
		b.append("'");
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getInventoriesQuery(String person) {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocInventories c where ");

		b.append("( ");
		b.append("c.commissioner like ");
		b.append("'");
		b.append(person + ":%");
		b.append("'");
		b.append(" OR c.commissioner like ");
		b.append("'");
		b.append("," + person + ":%");
		b.append("'");
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getLiteraryWorksQuery(String person) {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocLiteraryWorks c where ");

		b.append("( ");
		b.append("c.dedicatee like ");
		b.append("'");
		b.append(person + ":%");
		b.append("'");
		b.append(" OR c.dedicatee like ");
		b.append("'");
		b.append("," + person + ":%");
		b.append("'");
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}
		
		return b.toString();
	}

	private String getNewsQuery(String person) {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocNews c where ");

		b.append("( ");
		b.append("c.printer like ");
		b.append("'");
		b.append(person + ":%");
		b.append("'");
		b.append(" OR c.printer like ");
		b.append("'");
		b.append("," + person + ":%");
		b.append("'");
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getNotarialRecordsContractorQuery(String person) {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocNotarialRecords c where ");

		b.append("( ");
		b.append("c.contractor like ");
		b.append("'");
		b.append(person + ":%");
		b.append("'");
		b.append(" OR c.contractor like ");
		b.append("'");
		b.append("," + person + ":%");
		b.append("'");
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getNotarialRecordsContracteeQuery(String person) {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocNotarialRecords c where ");

		b.append("( ");
		b.append("c.contractee like ");
		b.append("'");
		b.append(person + ":%");
		b.append("'");
		b.append(" OR c.contractee like ");
		b.append("'");
		b.append("," + person + ":%");
		b.append("'");
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getOfficialRecordsQuery(String person) {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocOfficialRecords c where ");

		b.append("( ");
		b.append("c.printer like ");
		b.append("'");
		b.append(person + ":%");
		b.append("'");
		b.append(" OR c.printer like ");
		b.append("'");
		b.append("," + person + ":%");
		b.append("'");
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}
		
		return b.toString();
	}

	// Doc Queries

	private String getDocProducerQuery() {

		// select distinct d.documentEntityId from tblDocumentEnts d, tblPeople
		// p,tblDocumentsEntsPeople dp where ( p.PERSONID = '1105' OR p.PERSONID
		// = '1128' OR p.PERSONID = '9087' )and p.PERSONID = dp.producer and
		// dp.documentEntityId = d.documentEntityId

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.* from tblDocumentEnts d, tblPeople p, tblDocumentsEntsPeople dp where ");

		b.append("( ");
		b.append("p.PERSONID = ");
		b.append("'");
		b.append(people.get(0));
		b.append("'");
		for (int i = 1; i < people.size() - 1; i++) {
			b.append(" OR ");
			b.append("p.PERSONID = ");
			b.append("'");
			b.append(people.get(i));
			b.append("'");
		}
		b.append(" )");
		b.append("and p.PERSONID = dp.producer and dp.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}
		
		return b.toString();
	}

	private String getDocCorrespondenceQuery() {

		// select distinct d.documentEntityId from tblDocumentEnts d,
		// tblDocCorrespondence c where ( c.recipient like '1105:%' OR
		// c.recipient like ',1105:%' OR c.recipient like '1128:%' OR
		// c.recipient like ',1128:%' OR c.recipient like '9087:%' OR
		// c.recipient like ',9087:%' ) and c.documentEntityId =
		// d.documentEntityId
		StringBuffer b = new StringBuffer();

		b.append("select distinct d.* from tblDocumentEnts d, tblDocCorrespondence c where ");

		b.append("( ");
		b.append("c.recipient like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.recipient like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.recipient like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.recipient like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getDocFinancialRecordsQuery() {

		// select distinct d.documentEntityId from tblDocumentEnts d,
		// tblDocFinancialRecords c where ( c.recipient like '1105:%' OR
		// c.commissionerOfAccount like ',1105:%' OR c.commissionerOfAccount
		// like '120:%' OR
		// c.commissionerOfAccount like ',120:%' ) and c.documentEntityId =
		// d.documentEntityId
		StringBuffer b = new StringBuffer();

		b.append("select distinct d.* from tblDocumentEnts d, tblDocFinancialRecords c where ");

		b.append("( ");
		b.append("c.commissionerOfAccount like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.commissionerOfAccount like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.commissionerOfAccount like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.commissionerOfAccount like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getDocInventoriesQuery() {

		// select distinct d.documentEntityId from tblDocumentEnts d,
		// tblDocInventories c where ( c.commissioner like '1105:%' OR
		// c.commissioner like ',1105:%' OR c.commissioner like '1128:%' OR
		// c.commissioner like ',1128:%' ) and c.documentEntityId =
		// d.documentEntityId
		StringBuffer b = new StringBuffer();

		b.append("select distinct d.* from tblDocumentEnts d, tblDocInventories c where ");

		b.append("( ");
		b.append("c.commissioner like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.commissioner like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.commissioner like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.commissioner like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getDocLiteraryWorksQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.* from tblDocumentEnts d, tblDocLiteraryWorks c where ");

		b.append("( ");
		b.append("c.dedicatee like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.dedicatee like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.dedicatee like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.dedicatee like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getDocNewsQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.* from tblDocumentEnts d, tblDocNews c where ");

		b.append("( ");
		b.append("c.printer like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.printer like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.printer like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.printer like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getDocNotarialRecordsContractorQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.* from tblDocumentEnts d, tblDocNotarialRecords c where ");

		b.append("( ");
		b.append("c.contractor like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.contractor like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.contractor like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.contractor like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getDocNotarialRecordsContracteeQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.* from tblDocumentEnts d, tblDocNotarialRecords c where ");

		b.append("( ");
		b.append("c.contractee like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.contractee like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.contractee like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.contractee like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getDocOfficialRecordsQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.* from tblDocumentEnts d, tblDocOfficialRecords c where ");

		b.append("( ");
		b.append("c.printer like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.printer like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.printer like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.printer like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}
		
		return b.toString();
	}

	private String getDocDocumentRefQuery() {

		// select distinct d.* from tblDocumentEnts d,
		// tblDocumentEntsRefToPeople dp where d.documentEntityId =
		// dp.documentId and dp.peopleId = 203;

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.* from tblDocumentEnts d, tblDocumentEntsRefToPeople dp where ");

		b.append("( ");
		b.append("dp.peopleId = ");
		b.append("'");
		b.append(people.get(0));
		b.append("'");
		for (int i = 1; i < people.size() - 1; i++) {
			b.append(" OR ");
			b.append("dp.peopleId = ");
			b.append("'");
			b.append(people.get(i));
			b.append("'");
		}
		b.append(" )");
		b.append("and dp.documentId = d.documentEntityId and "
				+ NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	@JsonIgnore
	public List<String> getDocQueries() {
		List<String> queries = new ArrayList<String>();

		StringBuffer b = new StringBuffer();
		b.append("( ");

		b.append(getDocProducerQuery());
		b.append(UNION);
		
//		b.append(getDocRefQuery());
//		b.append(UNION);

		b.append(getDocFinancialRecordsQuery());
		b.append(UNION);

		b.append(getDocInventoriesQuery());
		b.append(UNION);

		b.append(getDocLiteraryWorksQuery());
		b.append(UNION);

		b.append(getDocNewsQuery());
		b.append(UNION);

		b.append(getDocNotarialRecordsContractorQuery());
		b.append(UNION);

		b.append(getDocNotarialRecordsContracteeQuery());
		b.append(UNION);

		b.append(getDocOfficialRecordsQuery());
		b.append(UNION);
		
		b.append(getDocMiscellaneousQuery());
		b.append(UNION);

		b.append(getDocCorrespondenceQuery());
		b.append(" )");

		queries.add(b.toString());

		return queries;
	}

	private String getDocMiscellaneousQuery() {
		StringBuffer stringBuffer = new StringBuffer();

		stringBuffer.append("select distinct d.* from tblDocumentEnts d, tblDocMiscellaneous c where ");

		stringBuffer.append("( ");
		stringBuffer.append("c.printer like ");
		stringBuffer.append("'");
		stringBuffer.append(people.get(0) + ":%");
		stringBuffer.append("'");
		stringBuffer.append(" OR c.printer like ");
		stringBuffer.append("'");
		stringBuffer.append("," + people.get(0) + ":%");
		stringBuffer.append("'");
		for (int i = 1; i < people.size(); i++) {
			stringBuffer.append(" OR ");

			stringBuffer.append("c.printer like ");
			stringBuffer.append("'");
			stringBuffer.append(people.get(i) + ":%");
			stringBuffer.append("'");
			stringBuffer.append(" OR c.printer like ");
			stringBuffer.append("'");
			stringBuffer.append("," + people.get(i) + ":%");
			stringBuffer.append("'");
		}
		stringBuffer.append(" ) ");

		stringBuffer.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			stringBuffer.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			stringBuffer.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return stringBuffer.toString();
	}
}
