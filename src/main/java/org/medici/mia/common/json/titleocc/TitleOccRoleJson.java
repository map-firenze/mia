package org.medici.mia.common.json.titleocc;

import java.io.Serializable;

import org.medici.mia.domain.TitleOccsList;

public class TitleOccRoleJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer roleCatId;
	private String occupationName;

	public void toJson(TitleOccsList entity) {
		if (entity == null)
			return;

		if (entity.getRoleCat() != null) {
			this.setRoleCatId(entity.getRoleCat().getRoleCatId());
		}
		this.setOccupationName(entity.getTitleOcc());

	}

	public Integer getRoleCatId() {
		return roleCatId;
	}

	public void setRoleCatId(Integer roleCatId) {
		this.roleCatId = roleCatId;
	}

	public String getOccupationName() {
		return occupationName;
	}

	public void setOccupationName(String occupationName) {
		this.occupationName = occupationName;
	}

}