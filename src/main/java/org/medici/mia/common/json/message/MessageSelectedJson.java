package org.medici.mia.common.json.message;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.medici.mia.domain.UserMessage;

public class MessageSelectedJson extends MessageJson {

	private static final long serialVersionUID = 1L;
	private final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd hh:mm:ss");

	private String to;
	private String from;

	private String readDate;
	private String sentDate;

	public MessageSelectedJson toJson(UserMessage mes) {

		super.toJson(mes);

		this.setTo(mes.getRecipient());
		this.setFrom(mes.getSender());
		// this.setToCompleteName(mes.getUser().getFirstName() + " "
		// + mes.getUser().getLastName());
		if (mes.getReadedDate() != null) {
			this.setReadDate(sdf.format(mes.getReadedDate()));
		}
		if (mes.getSendedDate() != null) {
			this.setSentDate(sdf.format(mes.getSendedDate()));
		}

		return this;
	}

	public UserMessage toEntity(UserMessage mesEntity) {

		mesEntity.setRecipient(this.getTo());
		mesEntity.setSendedDate(new Date());

		return mesEntity;

	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getSentDate() {
		return sentDate;
	}

	public void setSentDate(String sentDate) {
		this.sentDate = sentDate;
	}

	public String getReadDate() {
		return readDate;
	}

	public void setReadDate(String readDate) {
		this.readDate = readDate;
	}

}