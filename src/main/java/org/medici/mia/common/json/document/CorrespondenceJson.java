package org.medici.mia.common.json.document;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.json.FieldJson;
import org.medici.mia.common.json.PeopleJson;
import org.medici.mia.common.json.PlaceJson;
import org.medici.mia.domain.CorrespondenceEntity;
import org.medici.mia.domain.DocumentFieldEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.People;
import org.medici.mia.domain.Place;
import org.medici.mia.service.miadoc.DocumentType;
import org.medici.mia.service.miadoc.MiaDocumentUtils;
import org.medici.mia.service.miadoc.PeopleFieldType;
import org.medici.mia.service.miadoc.PlaceFieldType;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@JsonTypeName("correspondence")
public class CorrespondenceJson extends GenericDocumentJson {

	private static final long serialVersionUID = 1L;

	@JsonProperty("recipient")
	private List<PeopleJson> recipients;

	@JsonProperty("recipientPlace")
	private List<PlaceJson> recipientPlaces;

	public CorrespondenceJson() {
	}

	public List<PeopleJson> getRecipients() {
		return recipients;
	}

	public void setRecipients(List<PeopleJson> recipients) {
		this.recipients = recipients;
	}

	public List<PlaceJson> getRecipientPlaces() {
		return recipientPlaces;
	}

	public void setRecipientPlaces(List<PlaceJson> recipientPlaces) {
		this.recipientPlaces = recipientPlaces;
	}

	@Override
	public void init(MiaDocumentEntity docEntity) {
		super.init(docEntity);
		if (docEntity.getCorrespondence() != null) {

			this.recipients = getRecipients(docEntity.getCorrespondence()
					.getRecipients());

			this.recipientPlaces = getRecipientPlaces(docEntity
					.getCorrespondence().getRecipientPlaces());
		}
	}

	// Used for finding people attached to document
	@Override
	public List<FindPeopleForDocJson> findPeopleJson(MiaDocumentEntity docEntity) {
		List<FindPeopleForDocJson> modifyPeoples = new ArrayList<FindPeopleForDocJson>();

		FindPeopleForDocJson modifyPeople = new FindPeopleForDocJson();
		modifyPeople.setType(PeopleFieldType.Recipient.toString());

		if (docEntity.getCorrespondence() != null) {
			modifyPeople.setPeoples(MiaDocumentUtils.getPeopleBases(docEntity
					.getCorrespondence().getRecipient()));
		}
		modifyPeoples.add(modifyPeople);
		return modifyPeoples;
	}

	// Used for finding place attached to document
	@Override
	public List<FindPlaceForDocJson> findPlaceJson(MiaDocumentEntity docEntity) {
		List<FindPlaceForDocJson> modifyPlaces = new ArrayList<FindPlaceForDocJson>();

		FindPlaceForDocJson modifyPlace = new FindPlaceForDocJson();
		modifyPlace.setType(PlaceFieldType.RecipientPlace.toString());

		if (docEntity.getCorrespondence() != null) {
			modifyPlace.setPlaces(MiaDocumentUtils.getPlaceBases(docEntity
					.getCorrespondence().getRecipientPlace()));
		}
		modifyPlaces.add(modifyPlace);
		return modifyPlaces;
	}

	// Used for modify - add - delete people attached to document
	@Override
	public MiaDocumentEntity getModifiedDocumentEnt(MiaDocumentEntity docEnt,
			ModifyPeopleForDocJson modifyPeople) {

		if (modifyPeople.getType() == null) {
			docEnt.setErrorMsg("The type has no value)");
			return docEnt;
		}

		if (docEnt.getCorrespondence() == null) {
			docEnt.setErrorMsg("There is no " + docEnt.getCategory()
					+ " record associated with the document.");
			return docEnt;
		}

		if (PeopleFieldType.Recipient.toString().equalsIgnoreCase(
				modifyPeople.getType())) {

			String modifedStr = MiaDocumentUtils.modifyPeopleForDoc(docEnt
					.getCorrespondence().getRecipient(), modifyPeople);
			docEnt.getCorrespondence().setRecipient(modifedStr);

		} else {
			docEnt.setErrorMsg("The type " + modifyPeople.getType()
					+ " is not compatible with the category "
					+ docEnt.getCategory());
		}

		return docEnt;

	}

	// Used for modify - add - delete place attached to document
	@Override
	public MiaDocumentEntity getModifiedDocumentEnt(MiaDocumentEntity docEnt,
			ModifyPlaceForDocJson modifyPlace) {

		if (modifyPlace.getType() == null) {
			docEnt.setErrorMsg("The type has no value)");
			return docEnt;
		}

		if (docEnt.getCorrespondence() == null) {
			docEnt.setErrorMsg("There is no " + docEnt.getCategory()
					+ " record associated with the document.");
			return docEnt;
		}

		if (PlaceFieldType.RecipientPlace.toString().equalsIgnoreCase(
				modifyPlace.getType())) {

			String modifedStr = MiaDocumentUtils.modifyPlaceForDoc(docEnt
					.getCorrespondence().getRecipientPlace(), modifyPlace);
			docEnt.getCorrespondence().setRecipientPlace(modifedStr);

		} else {
			docEnt.setErrorMsg("The type " + modifyPlace.getType()
					+ " is not compatible with the category "
					+ docEnt.getCategory());
		}

		return docEnt;

	}

	@Override
	public MiaDocumentEntity getMiaDocumentEntity(DocumentJson docJson) {

		MiaDocumentEntity docEnt = super.getMiaDocumentEntity(docJson);

		docEnt.setCategory(DocumentType.correspondence.toString());
		CorrespondenceJson corrJson = (CorrespondenceJson) docJson;
		CorrespondenceEntity corrEnt = new CorrespondenceEntity();

		if (corrJson.getRecipients() != null
				&& !corrJson.getRecipients().isEmpty()) {
			String recipientStr = "";

			for (PeopleJson people : corrJson.getRecipients()) {
				if (people.getId() != null) {
					recipientStr = recipientStr + people.getId() + ":";
					if (!StringUtils.isEmpty(people.getUnsure())) {
						recipientStr = recipientStr + people.getUnsure();
					} else {
						recipientStr = recipientStr + "S";
					}
					recipientStr = recipientStr + ",";
				}
			}

			corrEnt.setRecipient(recipientStr);

		}

		if (corrJson.getRecipientPlaces() != null
				&& !corrJson.getRecipientPlaces().isEmpty()) {
			String recipientPlaceStr = "";

			for (PlaceJson place : corrJson.getRecipientPlaces()) {
				if (place.getId() != null) {
					recipientPlaceStr = recipientPlaceStr + place.getId() + ":";
					if (!StringUtils.isEmpty(place.getUnsure())) {
						recipientPlaceStr = recipientPlaceStr
								+ place.getUnsure();
					} else {
						recipientPlaceStr = recipientPlaceStr + "S";
					}
					recipientPlaceStr = recipientPlaceStr + ",";
				}
			}

			if (!recipientPlaceStr.equals("")) {
				corrEnt.setRecipientPlace(recipientPlaceStr);
			}

		}
		docEnt.setCorrespondence(corrEnt);

		return docEnt;
	}

	private ArrayList<PeopleJson> getRecipients(List<People> peoples) {

		ArrayList<PeopleJson> peopleJsons = new ArrayList<PeopleJson>();

		if (peoples != null && !peoples.isEmpty()) {
			for (People people : peoples) {

				PeopleJson peopleJson = new PeopleJson();
				peopleJson.setId(people.getPersonId());
				peopleJson.setGender(people.getGender());
				peopleJson.setMapNameLf(people.getMapNameLf());
				peopleJson.setActiveStart(people.getActiveStart());
				peopleJson.setActiveEnd(people.getActiveEnd());
				peopleJson.setBornYear(people.getBornYear());
				peopleJson.setDeathYear(people.getDeathYear());
				peopleJson.setFirstName(people.getFirst());
				peopleJson.setLastName(people.getLast());
				peopleJson.setUnsure(people.getUnsure());
				peopleJsons.add(peopleJson);

			}
		}

		return peopleJsons;

	}

	private ArrayList<PlaceJson> getRecipientPlaces(List<Place> places) {

		ArrayList<PlaceJson> placeJsons = new ArrayList<PlaceJson>();

		if (places != null && !places.isEmpty()) {
			for (Place place : places) {

				PlaceJson placeJson = new PlaceJson();
				placeJson.setId(place.getPlaceAllId());
				placeJson.setPlaceNameId(place.getPlaceNameId());
				placeJson.setPlaceName(place.getPlaceNameFull());
				placeJson.setPrefFlag(place.getPrefFlag());
				placeJson.setPlType(place.getPlType());
				placeJson.setUnsure(place.getUnsure());
				placeJsons.add(placeJson);
			}
		}

		return placeJsons;

	}

	@Override
	public List<FieldJson> getJsonFields(
			List<DocumentFieldEntity> documentFields) {

		List<FieldJson> fields = new ArrayList<FieldJson>();
		for (DocumentFieldEntity field : documentFields) {
			if (DocumentType.correspondence.toString().equalsIgnoreCase(
					field.getDocumentCategory())) {
				boolean isMultiple = false;
				if ("1".equalsIgnoreCase(field.getAllowMultiple())) {
					isMultiple = true;
				}
				FieldJson fieldJson = new FieldJson(field.getFieldName(),
						field.getFieldBeName(), field.getFieldValue(),
						field.getFieldType(), isMultiple);

				fields.add(fieldJson);

			}
		}

		return fields;
	}

	@Override
	public List<DocPeopleFieldJson> getDocPeopleFields(Integer personId,
			MiaDocumentEntity docEnt, List<DocumentFieldEntity> documentFields) {

		List<DocPeopleFieldJson> fields = super.getDocPeopleFields(personId,
				docEnt, documentFields);

		if (docEnt.getCorrespondence() != null
				&& hasField(docEnt.getCorrespondence().getRecipient(),
						String.valueOf(personId))) {

			DocPeopleFieldJson field = new DocPeopleFieldJson();
			field.setFieldBeName(PeopleFieldType.Recipient.toString());
			field.setFieldName(getFieldName(documentFields,
					docEnt.getCategory(), PeopleFieldType.Recipient.toString()));

			if (fields == null)
				fields = new ArrayList<DocPeopleFieldJson>();

			fields.add(field);

		}

		return fields;

	}

}