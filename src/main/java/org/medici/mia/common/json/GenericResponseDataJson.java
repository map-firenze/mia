package org.medici.mia.common.json;

public class GenericResponseDataJson<T> extends GenericResponseJson {
	
	private static final long serialVersionUID = 1L;

	private T data;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
