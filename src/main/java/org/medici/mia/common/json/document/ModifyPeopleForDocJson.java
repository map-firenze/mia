package org.medici.mia.common.json.document;

import java.io.Serializable;

import org.medici.mia.common.json.PeopleBaseJson;

public class ModifyPeopleForDocJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private String documentId;
	private String category;
	private String type;
	private PeopleBaseJson newPeople;
	private Integer idPeopleToDelete;

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	
	public PeopleBaseJson getNewPeople() {
		return newPeople;
	}

	public void setNewPeople(PeopleBaseJson newPeople) {
		this.newPeople = newPeople;
	}

	public Integer getIdPeopleToDelete() {
		return idPeopleToDelete;
	}

	public void setIdPeopleToDelete(Integer idPeopleToDelete) {
		this.idPeopleToDelete = idPeopleToDelete;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}