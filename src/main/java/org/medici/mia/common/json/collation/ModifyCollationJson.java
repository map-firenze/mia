package org.medici.mia.common.json.collation;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class ModifyCollationJson implements Serializable {

    @JsonProperty
    private String title;

    @JsonProperty
    private String description;

    @JsonProperty
    private Boolean unsure;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getUnsure() {
        return unsure;
    }

    public void setUnsure(Boolean unsure) {
        this.unsure = unsure;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
