package org.medici.mia.common.json.advancedsearch;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.util.DateUtils;
import org.medici.mia.domain.User;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@JsonTypeName("transcriptionAdvancedSearch")
public class TranscriptionAdvancedSearchJson extends GenericAdvancedSearchJson {

	private static final long serialVersionUID = 1L;

	@JsonProperty("transcription")
	private String transcription;

	public String getTranscription() {
		return transcription;
	}

	public void setTranscription(String transcription) {
		this.transcription = transcription;
	}

	@JsonIgnore
	@Override
	public List<String> getQueries() {
		String[] wordsToSearch = splitSearchString(getTranscription());

		List<String> queries = new ArrayList<String>();
		StringBuffer b = new StringBuffer();
		b.append("SELECT DISTINCT d.documentEntityId FROM tblDocumentEnts d "
			   + "WHERE d.flgLogicalDelete = 0 ");
		
		for (String wordToSearch : wordsToSearch) {
			b.append(getTranscriptionSearch(wordToSearch));
		}
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		queries.add(b.toString());
		return queries;

	}
	
	private String getTranscriptionSearch(String wordToSearch) {
		return " AND d.transSearch LIKE '%" + wordToSearch + "%' ";
	}
	
	private String[] splitSearchString(String searchString) {
		return searchString.trim().split(" ");
	}
}