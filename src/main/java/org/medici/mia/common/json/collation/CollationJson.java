package org.medici.mia.common.json.collation;

import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.domain.Collation;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

public class CollationJson implements Serializable {

    private static final long serialVersionUID = -67437874982042745L;
    private final transient SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    private Integer id;
    private DocumentJson childDocument;
    private DocumentJson parentDocument;
    private HashMap<String, Object> parentVolume;
    private HashMap<String, Object> childVolume;
    private HashMap<String, Object> parentInsert;
    private HashMap<String, Object> childInsert;
    private String type;
    private String title;
    private Boolean unsure;
    private String description;
    private String created;
    private String createdBy;
    private Boolean logicalDelete;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public DocumentJson getChildDocument() {
        return childDocument;
    }

    public void setChildDocument(DocumentJson childDocument) {
        this.childDocument = childDocument;
    }

    public DocumentJson getParentDocument() {
        return parentDocument;
    }

    public void setParentDocument(DocumentJson parentDocument) {
        this.parentDocument = parentDocument;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Boolean getUnsure() {
        return unsure;
    }

    public void setUnsure(Boolean unsure) {
        this.unsure = unsure;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public HashMap<String, Object> getParentVolume() {
        return parentVolume;
    }

    public void setParentVolume(HashMap<String, Object> parentVolume) {
        this.parentVolume = parentVolume;
    }

    public HashMap<String, Object> getChildVolume() {
        return childVolume;
    }

    public void setChildVolume(HashMap<String, Object> childVolume) {
        this.childVolume = childVolume;
    }

    public HashMap<String, Object> getParentInsert() {
        return parentInsert;
    }

    public void setParentInsert(HashMap<String, Object> parentInsert) {
        this.parentInsert = parentInsert;
    }

    public HashMap<String, Object> getChildInsert() {
        return childInsert;
    }

    public void setChildInsert(HashMap<String, Object> childInsert) {
        this.childInsert = childInsert;
    }

    public Boolean getLogicalDelete() {
        return logicalDelete;
    }

    public void setLogicalDelete(Boolean logicalDelete) {
        this.logicalDelete = logicalDelete;
    }

    public void toJson(Collation c, List<DocumentJson> entities) {
        if (c == null)
            return;
        this.setId(c.getId());
        this.setChildDocument(entities.get(0));
        if(entities.size() > 1) this.setParentDocument(entities.get(1));
        this.setCreated(c.getCreated() == null ? null : sdf.format(c.getCreated()));
        this.setTitle(c.getTitle());
        this.setCreatedBy(c.getCreatedBy() == null ? null : c.getCreatedBy().getAccount());
        this.setType(c.getType() == null ? null : c.getType().toString());
        this.setDescription(c.getDescription());
        this.setUnsure(c.getUnsure());
        HashMap<String, Object> parentVolume = new HashMap<String, Object>();
        HashMap<String, Object> childVolume = new HashMap<String, Object>();
        HashMap<String, Object> parentInsert = new HashMap<String, Object>();
        HashMap<String, Object> childInsert = new HashMap<String, Object>();
        if(c.getParentVolume() != null) {
            parentVolume.put("id", c.getParentVolume().getSummaryId());
            parentVolume.put("name", c.getParentVolume().getVolume());
        }
        if(c.getChildVolume() != null) {
            childVolume.put("id", c.getChildVolume().getSummaryId());
            childVolume.put("name", c.getChildVolume().getVolume());
        }
        if(c.getParentInsert() != null){
            parentInsert.put("id", c.getParentInsert().getInsertId());
            parentInsert.put("name", c.getParentInsert().getInsertName());
        }
        if(c.getChildInsert() != null) {
            childInsert.put("id", c.getChildInsert().getInsertId());
            childInsert.put("name", c.getChildInsert().getInsertName());
        }
        this.setParentVolume(parentVolume.isEmpty() ? null : parentVolume);
        this.setChildVolume(childVolume.isEmpty() ? null : childVolume);
        this.setChildInsert(childInsert.isEmpty() ? null : childInsert);
        this.setParentInsert(parentInsert.isEmpty() ? null : parentInsert);
        this.setParentInsert(parentInsert.isEmpty() ? null : parentInsert);
        this.setLogicalDelete(c.getLogicalDelete());
    }

}
