package org.medici.mia.common.json.document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.medici.mia.common.json.FieldJson;
import org.medici.mia.common.json.PeopleBaseJson;
import org.medici.mia.common.json.PeopleJson;
import org.medici.mia.domain.DocumentFieldEntity;
import org.medici.mia.domain.FinancialEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.People;
import org.medici.mia.service.miadoc.DocumentType;
import org.medici.mia.service.miadoc.MiaDocumentUtils;
import org.medici.mia.service.miadoc.PeopleFieldType;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@JsonTypeName("financialRecords")
public class FinancialJson extends GenericDocumentJson {

	private static final long serialVersionUID = 1L;

	@JsonProperty("commissionerOfAccount")
	private List<PeopleJson> commissionersOfAccount;

	public FinancialJson() {
	}

	public List<PeopleJson> getCommissionersOfAccount() {
		return commissionersOfAccount;
	}

	public void setCommissionersOfAccount(
			List<PeopleJson> commissionersOfAccount) {
		this.commissionersOfAccount = commissionersOfAccount;
	}

	@Override
	public void init(MiaDocumentEntity docEntity) {
		super.init(docEntity);
		if (docEntity.getFinancial() != null) {
			this.commissionersOfAccount = getCommissionersOfAccount(docEntity
					.getFinancial().getCommissionersOfAccount());
		}
	}

	// used for find people attached to a document
	@Override
	public List<FindPeopleForDocJson> findPeopleJson(MiaDocumentEntity docEntity) {
		List<FindPeopleForDocJson> modifyPeoples = new ArrayList<FindPeopleForDocJson>();

		FindPeopleForDocJson modifyPeople = new FindPeopleForDocJson();
		modifyPeople.setType(PeopleFieldType.CommissionerofAccount.toString());
		ArrayList<PeopleBaseJson> peopleBases = null;

		if (docEntity.getFinancial() != null
				&& docEntity.getFinancial().getCommissionerOfAccount() != null
				&& !docEntity.getFinancial().getCommissionerOfAccount()
						.isEmpty()) {

			peopleBases = new ArrayList<PeopleBaseJson>();

			for (String peopleIdStr : (List<String>) Arrays.asList(docEntity
					.getFinancial().getCommissionerOfAccount().split(","))) {

				PeopleBaseJson peopleBase = new PeopleBaseJson();
				String[] peoplesIdArr = peopleIdStr.split(":");
				if (peoplesIdArr[0] != null || !peoplesIdArr[0].isEmpty()) {
					peopleBase.setId(Integer.valueOf(peoplesIdArr[0]));
				}
				if (peoplesIdArr.length > 1) {
					peopleBase.setUnsure(peoplesIdArr[1]);
				}

				peopleBases.add(peopleBase);
			}
		}
		modifyPeople.setPeoples(peopleBases);
		modifyPeoples.add(modifyPeople);

		return modifyPeoples;
	}

	// Used for modify - add - delete people attached to document
	@Override
	public MiaDocumentEntity getModifiedDocumentEnt(MiaDocumentEntity docEnt,
			ModifyPeopleForDocJson modifyPeople) {

		if (modifyPeople.getType() == null) {
			docEnt.setErrorMsg("The type has no value)");
			return docEnt;
		}

		if (docEnt.getFinancial() == null) {
			docEnt.setErrorMsg("There is no " + docEnt.getCategory()
					+ " record associated with the document.");
			return docEnt;
		}

		if (PeopleFieldType.CommissionerofAccount.toString().equalsIgnoreCase(
				modifyPeople.getType())) {

			String modifedStr = MiaDocumentUtils.modifyPeopleForDoc(docEnt
					.getFinancial().getCommissionerOfAccount(), modifyPeople);
			docEnt.getFinancial().setCommissionerOfAccount(modifedStr);

		} else {
			docEnt.setErrorMsg("The type " + modifyPeople.getType()
					+ " is not compatible with the category "
					+ docEnt.getCategory());
		}

		return docEnt;

	}

	@Override
	public MiaDocumentEntity getMiaDocumentEntity(DocumentJson docJson) {

		MiaDocumentEntity docEnt = super.getMiaDocumentEntity(docJson);

		docEnt.setCategory(DocumentType.financialRecords.toString());
		FinancialJson finJson = (FinancialJson) docJson;
		FinancialEntity finEnt = new FinancialEntity();

		if (finJson.getCommissionersOfAccount() != null
				&& !finJson.getCommissionersOfAccount().isEmpty()) {
			String commissionerStr = "";
			for (PeopleJson people : finJson.getCommissionersOfAccount()) {
				commissionerStr = commissionerStr + people.getId() + ":";
				if (!StringUtils.isEmpty(people.getUnsure())) {
					commissionerStr = commissionerStr + people.getUnsure();
				} else {
					commissionerStr = commissionerStr + "S";
				}
				commissionerStr = commissionerStr + ",";
			}
			finEnt.setCommissionerOfAccount(commissionerStr);
		}
		docEnt.setFinancial(finEnt);
		return docEnt;
	}

	@Override
	public List<FieldJson> getJsonFields(
			List<DocumentFieldEntity> documentFields) {

		List<FieldJson> fields = new ArrayList<FieldJson>();
		for (DocumentFieldEntity field : documentFields) {
			if (DocumentType.financialRecords.toString().equalsIgnoreCase(
					field.getDocumentCategory())) {
				boolean isMultiple = false;
				if ("1".equalsIgnoreCase(field.getAllowMultiple())) {
					isMultiple = true;
				}
				FieldJson fieldJson = new FieldJson(field.getFieldName(),
						field.getFieldBeName(), field.getFieldValue(),
						field.getFieldType(), isMultiple);

				fields.add(fieldJson);

			}
		}

		return fields;
	}

	private ArrayList<PeopleJson> getCommissionersOfAccount(List<People> peoples) {

		ArrayList<PeopleJson> peopleJsons = new ArrayList<PeopleJson>();

		if (peoples != null && !peoples.isEmpty()) {
			for (People people : peoples) {

				PeopleJson peopleJson = new PeopleJson();
				peopleJson.setId(people.getPersonId());
				peopleJson.setGender(people.getGender());
				peopleJson.setMapNameLf(people.getMapNameLf());
				peopleJson.setActiveStart(people.getActiveStart());
				peopleJson.setActiveEnd(people.getActiveEnd());
				peopleJson.setBornYear(people.getBornYear());
				peopleJson.setDeathYear(people.getDeathYear());
				peopleJson.setFirstName(people.getFirst());
				peopleJson.setLastName(people.getLast());
				peopleJson.setUnsure(people.getUnsure());
				peopleJsons.add(peopleJson);
			}
		}

		return peopleJsons;
	}

	@Override
	public List<DocPeopleFieldJson> getDocPeopleFields(Integer personId,
			MiaDocumentEntity docEnt, List<DocumentFieldEntity> documentFields) {

		List<DocPeopleFieldJson> fields = super.getDocPeopleFields(personId,
				docEnt, documentFields);
		if (docEnt.getFinancial() != null
				&& hasField(docEnt.getFinancial().getCommissionerOfAccount(),
						String.valueOf(personId))) {
			DocPeopleFieldJson field = new DocPeopleFieldJson();
			field.setFieldBeName(PeopleFieldType.CommissionerofAccount
					.toString());
			field.setFieldName(getFieldName(documentFields,
					docEnt.getCategory(),
					PeopleFieldType.CommissionerofAccount.toString()));

			if (fields == null)
				fields = new ArrayList<DocPeopleFieldJson>();

			fields.add(field);

		}

		return fields;

	}

}
