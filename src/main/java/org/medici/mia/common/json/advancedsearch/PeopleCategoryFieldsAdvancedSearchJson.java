package org.medici.mia.common.json.advancedsearch;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.util.DateUtils;
import org.medici.mia.domain.User;
import org.medici.mia.service.miadoc.DocumentType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@JsonTypeName("peopleCategoryFieldsAdvancedSearch")
public class PeopleCategoryFieldsAdvancedSearchJson extends
		GenericAdvancedSearchJson {

	private static final long serialVersionUID = 1L;
	private static final String NOT_DELETED_DOC = " d.flgLogicalDelete != 1";
	private static final String UNION = " ) UNION (";

	@JsonProperty("people")
	private List<Integer> people;
	private String category;
	private String field;

	public List<Integer> getPeople() {
		return people;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public void setPeople(List<Integer> people) {
		this.people = people;
	}

	@JsonIgnore
	@Override
	public List<String> getQueries() {
		List<String> queries = new ArrayList<String>();

		StringBuffer b = new StringBuffer();

		if (this.getCategory().equalsIgnoreCase("refToPeople")) {
			getDocRefToPeopleQuery();
			queries.add(b.toString());
			return queries;
		}

		b.append("( ");

		if (this.getField().equalsIgnoreCase("producers")) {
			b.append(getProducerQuery());
		} else if (this.getCategory().equalsIgnoreCase(
				DocumentType.correspondence.toString())) {
			b.append(getCorrespondenceQuery());
		} else if (this.getCategory().equalsIgnoreCase(
				DocumentType.notarialRecords.toString())) {
			
			b.append(" ( ");
			b.append(getNotarialRecordsContractorQuery());
			b.append(" ) ");
			b.append(UNION);
			b.append(" ( ");
			b.append(getNotarialRecordsContracteeQuery());
			b.append(" ) ");
//			if (this.getField().equalsIgnoreCase("contractor")) {
//				b.append(getNotarialRecordsContractorQuery());
//			} else if (this.getField().equalsIgnoreCase("contractee")) {
//				b.append(getNotarialRecordsContracteeQuery());
//			} else if (this.getField().equalsIgnoreCase("all")) {
//				b.append(" ( ");
//				b.append(getNotarialRecordsContractorQuery());
//				b.append(" ) ");
//				b.append(UNION);
//				b.append(" ( ");
//				b.append(getNotarialRecordsContracteeQuery());
//				b.append(" ) ");
//			}
		} else if (this.getCategory().equalsIgnoreCase(
				DocumentType.financialRecords.toString())) {
			b.append(getFinancialRecordsQuery());
		} else if (this.getCategory().equalsIgnoreCase(
				DocumentType.inventories.toString())) {
			b.append(getInventoriesQuery());
		} else if (this.getCategory().equalsIgnoreCase(
				DocumentType.literaryWorks.toString())) {
			b.append(getLiteraryWorksQuery());
		} else if (this.getCategory().equalsIgnoreCase(
				DocumentType.miscellaneous.toString())) {
			b.append(getMiscellaneousQuery());
		} else if (this.getCategory().equalsIgnoreCase(
				DocumentType.news.toString())) {
			b.append(getNewsQuery());
		} else if (this.getCategory().equalsIgnoreCase(
				DocumentType.officialRecords.toString())) {
			b.append(getOfficialRecordsQuery());
		} else if (this.getCategory().equalsIgnoreCase(
				DocumentType.officialRecords.toString())) {
			b.append(getOfficialRecordsQuery());
		}

//		if (this.getField().equalsIgnoreCase("all")) {
//			b.append(" ) ");
//			b.append(UNION);
//			b.append(" ( ");
//			b.append(getProducerQuery());
//		}

		b.append(" ) ");

		queries.add(b.toString());

		return queries;
	}

	private String getProducerQuery() {

		// select distinct d.documentEntityId from tblDocumentEnts d, tblPeople
		// p,tblDocumentsEntsPeople dp where ( p.PERSONID = '1105' OR p.PERSONID
		// = '1128' OR p.PERSONID = '9087' )and p.PERSONID = dp.producer and
		// dp.documentEntityId = d.documentEntityId

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocumentsEntsPeople dp where ");

		b.append("( ");
		b.append("dp.producer = ");
		b.append("'");
		b.append(people.get(0));
		b.append("'");
		for (int i = 1; i < people.size() - 1; i++) {
			b.append(" OR ");
			b.append("dp.producer = ");
			b.append("'");
			b.append(people.get(i));
			b.append("'");
		}
		b.append(" )");
		b.append(" and dp.documentEntityId = d.documentEntityId "
				+ " and d.category ='" + this.getCategory() + "' and "
				+ NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getCorrespondenceQuery() {

		// select distinct d.documentEntityId from tblDocumentEnts d,
		// tblDocCorrespondence c where ( c.recipient like '1105:%' OR
		// c.recipient like ',1105:%' OR c.recipient like '1128:%' OR
		// c.recipient like ',1128:%' OR c.recipient like '9087:%' OR
		// c.recipient like ',9087:%' ) and c.documentEntityId =
		// d.documentEntityId
		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocCorrespondence c where ");

		b.append("( ");
		b.append("c.recipient like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.recipient like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.recipient like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.recipient like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getFinancialRecordsQuery() {

		// select distinct d.documentEntityId from tblDocumentEnts d,
		// tblDocFinancialRecords c where ( c.recipient like '1105:%' OR
		// c.commissionerOfAccount like ',1105:%' OR c.commissionerOfAccount
		// like '120:%' OR
		// c.commissionerOfAccount like ',120:%' ) and c.documentEntityId =
		// d.documentEntityId
		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocFinancialRecords c where ");

		b.append("( ");
		b.append("c.commissionerOfAccount like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.commissionerOfAccount like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.commissionerOfAccount like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.commissionerOfAccount like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getDocRefToPeopleQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId from tblDocumentEntsRefToPeople dp, tblDocumentEnts d where d.documentEntityId = dp.documentId and ");

		b.append("( ");
		b.append("dp.peopleId = ");
		b.append("'");
		b.append(people.get(0));
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");
			b.append("dp.peopleId = ");
			b.append("'");
			b.append(people.get(i));
			b.append("'");
		}
		b.append(" ) ");

		b.append(" and " + NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getInventoriesQuery() {

		// select distinct d.documentEntityId from tblDocumentEnts d,
		// tblDocInventories c where ( c.commissioner like '1105:%' OR
		// c.commissioner like ',1105:%' OR c.commissioner like '1128:%' OR
		// c.commissioner like ',1128:%' ) and c.documentEntityId =
		// d.documentEntityId
		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocInventories c where ");

		b.append("( ");
		b.append("c.commissioner like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.commissioner like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.commissioner like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.commissioner like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getLiteraryWorksQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocLiteraryWorks c where ");

		b.append("( ");
		b.append("c.dedicatee like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.dedicatee like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.dedicatee like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.dedicatee like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getNewsQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocNews c where ");

		b.append("( ");
		b.append("c.printer like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.printer like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.printer like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.printer like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getNotarialRecordsContractorQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocNotarialRecords c where ");

		b.append("( ");
		b.append("c.contractor like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.contractor like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.contractor like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.contractor like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getNotarialRecordsContracteeQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocNotarialRecords c where ");

		b.append("( ");
		b.append("c.contractee like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.contractee like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.contractee like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.contractee like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getOfficialRecordsQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocOfficialRecords c where ");

		b.append("( ");
		b.append("c.printer like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.printer like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.printer like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.printer like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getMiscellaneousQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocMiscellaneous c where ");

		b.append("( ");
		b.append("c.printer like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.printer like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.printer like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.printer like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	// Doc Queries

	private String getDocProducerQuery() {

		// select distinct d.documentEntityId from tblDocumentEnts d, tblPeople
		// p,tblDocumentsEntsPeople dp where ( p.PERSONID = '1105' OR p.PERSONID
		// = '1128' OR p.PERSONID = '9087' )and p.PERSONID = dp.producer and
		// dp.documentEntityId = d.documentEntityId

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.* from tblDocumentEnts d, tblPeople p, tblDocumentsEntsPeople dp where ");

		b.append("( ");
		b.append("p.PERSONID = ");
		b.append("'");
		b.append(people.get(0));
		b.append("'");
		for (int i = 1; i < people.size() - 1; i++) {
			b.append(" OR ");
			b.append("p.PERSONID = ");
			b.append("'");
			b.append(people.get(i));
			b.append("'");
		}
		b.append(" )");
		b.append("and p.PERSONID = dp.producer and dp.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getDocCorrespondenceQuery() {

		// select distinct d.documentEntityId from tblDocumentEnts d,
		// tblDocCorrespondence c where ( c.recipient like '1105:%' OR
		// c.recipient like ',1105:%' OR c.recipient like '1128:%' OR
		// c.recipient like ',1128:%' OR c.recipient like '9087:%' OR
		// c.recipient like ',9087:%' ) and c.documentEntityId =
		// d.documentEntityId
		StringBuffer b = new StringBuffer();

		b.append("select distinct d.* from tblDocumentEnts d, tblDocCorrespondence c where ");

		b.append("( ");
		b.append("c.recipient like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.recipient like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.recipient like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.recipient like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getDocFinancialRecordsQuery() {

		// select distinct d.documentEntityId from tblDocumentEnts d,
		// tblDocFinancialRecords c where ( c.recipient like '1105:%' OR
		// c.commissionerOfAccount like ',1105:%' OR c.commissionerOfAccount
		// like '120:%' OR
		// c.commissionerOfAccount like ',120:%' ) and c.documentEntityId =
		// d.documentEntityId
		StringBuffer b = new StringBuffer();

		b.append("select distinct d.* from tblDocumentEnts d, tblDocFinancialRecords c where ");

		b.append("( ");
		b.append("c.commissionerOfAccount like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.commissionerOfAccount like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.commissionerOfAccount like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.commissionerOfAccount like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getDocInventoriesQuery() {

		// select distinct d.documentEntityId from tblDocumentEnts d,
		// tblDocInventories c where ( c.commissioner like '1105:%' OR
		// c.commissioner like ',1105:%' OR c.commissioner like '1128:%' OR
		// c.commissioner like ',1128:%' ) and c.documentEntityId =
		// d.documentEntityId
		StringBuffer b = new StringBuffer();

		b.append("select distinct d.* from tblDocumentEnts d, tblDocInventories c where ");

		b.append("( ");
		b.append("c.commissioner like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.commissioner like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.commissioner like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.commissioner like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getDocLiteraryWorksQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.* from tblDocumentEnts d, tblDocLiteraryWorks c where ");

		b.append("( ");
		b.append("c.dedicatee like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.dedicatee like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.dedicatee like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.dedicatee like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getDocNewsQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.* from tblDocumentEnts d, tblDocNews c where ");

		b.append("( ");
		b.append("c.printer like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.printer like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.printer like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.printer like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getDocNotarialRecordsContractorQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.* from tblDocumentEnts d, tblDocNotarialRecords c where ");

		b.append("( ");
		b.append("c.contractor like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.contractor like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.contractor like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.contractor like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getDocNotarialRecordsContracteeQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.* from tblDocumentEnts d, tblDocNotarialRecords c where ");

		b.append("( ");
		b.append("c.contractee like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.contractee like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.contractee like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.contractee like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	private String getDocOfficialRecordsQuery() {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.* from tblDocumentEnts d, tblDocOfficialRecords c where ");

		b.append("( ");
		b.append("c.printer like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.printer like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.printer like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.printer like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId and "
				+ NOT_DELETED_DOC);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return b.toString();
	}

	@JsonIgnore
	public List<String> getDocQueries() {
		List<String> queries = new ArrayList<String>();

		StringBuffer b = new StringBuffer();
		b.append("( ");

		b.append(getDocProducerQuery());
		b.append(UNION);

		// b.append(getDocRefQuery());
		// b.append(UNION);

		b.append(getDocFinancialRecordsQuery());
		b.append(UNION);

		b.append(getDocInventoriesQuery());
		b.append(UNION);

		b.append(getDocLiteraryWorksQuery());
		b.append(UNION);

		b.append(getDocNewsQuery());
		b.append(UNION);

		b.append(getDocNotarialRecordsContractorQuery());
		b.append(UNION);

		b.append(getDocNotarialRecordsContracteeQuery());
		b.append(UNION);

		b.append(getDocOfficialRecordsQuery());
		b.append(UNION);

		b.append(getDocCorrespondenceQuery());
		b.append(" )");

		queries.add(b.toString());

		return queries;
	}

}
