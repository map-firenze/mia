package org.medici.mia.common.json.allcollections;

import org.medici.mia.common.json.casestudy.CaseStudyBAALJson;
import org.medici.mia.common.json.casestudy.CaseStudyJson;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class AllCollectionsThumbsFileJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private String filePath;
	private Integer uploadFileId;
	private Integer uploadId;
	private Integer privacy;
	private List<AllCollectionsFolioJson> folios;
	private Integer folioRectoVerso;
	private String fileName;
	private String owner;
	private String collationType;
	private List<CaseStudyBAALJson> caseStudies;
	private List<Object> otherVersions;
	private List<HashMap> collations;
	private Integer documentCount;

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Integer getUploadFileId() {
		return uploadFileId;
	}

	public void setUploadFileId(Integer uploadFileId) {
		this.uploadFileId = uploadFileId;
	}

	public Integer getPrivacy() {
		return privacy;
	}

	public void setPrivacy(Integer privacy) {
		this.privacy = privacy;
	}

	public List<AllCollectionsFolioJson> getFolios() {
		return folios;
	}

	public void setFolios(List<AllCollectionsFolioJson> folios) {
		this.folios = folios;
	}

	public Integer getFolioRectoVerso() {
		return folioRectoVerso;
	}

	public void setFolioRectoVerso(Integer folioRectoVerso) {
		this.folioRectoVerso = folioRectoVerso;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getCollationType() {
		return collationType;
	}

	public void setCollationType(String collationType) {
		this.collationType = collationType;
	}

	public List<CaseStudyBAALJson> getCaseStudies() {
		return caseStudies;
	}

	public void setCaseStudies(List<CaseStudyBAALJson> caseStudies) {
		this.caseStudies = caseStudies;
	}

	public List<Object> getOtherVersions() {
		return otherVersions;
	}

	public void setOtherVersions(List<Object> otherVersions) {
		this.otherVersions = otherVersions;
	}

	public List<HashMap> getCollations() {
		return collations;
	}

	public void setCollations(List<HashMap> collations) {
		this.collations = collations;
	}

	public Integer getUploadId() {
		return uploadId;
	}

	public void setUploadId(Integer uploadId) {
		this.uploadId = uploadId;
	}

	public Integer getDocumentCount() {
		return documentCount;
	}

	public void setDocumentCount(Integer documentCount) {
		this.documentCount = documentCount;
	}


	// otherVersions means if volume is used in other files the value should be
	// true.

	// public AllCollectionsThumbsFileJson toJson(UploadFileEntity entity) {
	//
	// this.set(user.getAccount());
	// this.setFirstName(user.getFirstName());
	// this.setLastName(user.getLastName());
	// this.setOrganization(user.getOrganization());
	// this.setEmail(user.getMail());
	// this.setCity(user.getCity());
	// this.setCountry(user.getCountry());
	//
	// return this;
	// }
	//
	//

}
