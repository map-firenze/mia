package org.medici.mia.common.json.complexsearch;

import java.io.Serializable;

import org.medici.mia.common.json.search.GenericAESearchJson;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class ComplexSearchJson implements Serializable {

	private static final long serialVersionUID = 3787731742866353474L;

	private String matchSearchWords;
	private String owner = null;
	private String partialSearchWords;
	private String searchType;

	public String getMatchSearchWords() {
		return matchSearchWords;
	}

	public void setMatchSearchWords(String matchSearchWords) {
		this.matchSearchWords = matchSearchWords;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getPartialSearchWords() {
		return partialSearchWords;
	}

	public void setPartialSearchWords(String partialSearchWords) {
		this.partialSearchWords = partialSearchWords;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

}
