package org.medici.mia.common.json.biographical;

import java.io.Serializable;
import java.util.List;

public class FindPersonSpousesJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer personId;

	private List<SpouseJson> spouses;

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public List<SpouseJson> getSpouses() {
		return spouses;
	}

	public void setSpouses(List<SpouseJson> spouses) {
		this.spouses = spouses;
	}

}