package org.medici.mia.common.json.document;

import org.medici.mia.common.json.GenericResponseJson;

/**
 * 
 * @author User
 *
 */
public class CreateDEResponseJson extends GenericResponseJson {
	
	private static final long serialVersionUID = 1L;
	
	private Integer docId;

	public Integer getDocId() {
		return docId;
	}

	public void setDocId(Integer docId) {
		this.docId = docId;
	}

}
