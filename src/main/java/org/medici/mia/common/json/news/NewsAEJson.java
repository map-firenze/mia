package org.medici.mia.common.json.news;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.medici.mia.common.util.FileUtils;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.UploadInfoEntity;

public class NewsAEJson extends NewsBaseJson {

	private static final long serialVersionUID = 1L;

	private final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	private String aeName;
	private List<String> thumbPaths;

	private String action;
	private String modifiedBy;
	private String repositoryAbbreviation;
	private String insertName;
	private String collectionName;
	private String volumeName;
	private String aeDescription;
	private String owner;

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getRepositoryAbbreviation() {
		return repositoryAbbreviation;
	}

	public void setRepositoryAbbreviation(String repositoryAbbreviation) {
		this.repositoryAbbreviation = repositoryAbbreviation;
	}

	public String getInsertName() {
		return insertName;
	}

	public void setInsertName(String insertName) {
		this.insertName = insertName;
	}

	public String getCollectionName() {
		return collectionName;
	}

	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}

	public String getVolumeName() {
		return volumeName;
	}

	public void setVolumeName(String volumeName) {
		this.volumeName = volumeName;
	}

	public String getAeDescription() {
		return aeDescription;
	}

	public void setAeDescription(String aeDescription) {
		this.aeDescription = aeDescription;
	}

	public String getAeName() {
		return aeName;
	}

	public void setAeName(String aeName) {
		this.aeName = aeName;
	}

	public List<String> getThumbPaths() {
		return thumbPaths;
	}

	public void setThumbPaths(List<String> thumbPaths) {
		this.thumbPaths = thumbPaths;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public NewsAEJson toJson(UploadInfoEntity ae) {

		this.setType("ae");
		this.setRepositoryAbbreviation(ae.getRepositoryEntity().getRepositoryAbbreviation());
		if(ae.getInsertEntity() == null) {
			this.setInsertName(null);
		} else {
			this.setInsertName(ae.getInsertEntity().getInsertName());
		}
		if(ae.getVolumeEntity() == null){
			this.setVolumeName(null);
		} else {
			this.setVolumeName(ae.getVolumeEntity().getVolume());
		}
		this.setAeDescription(ae.getAeDescription());
		if(ae.getCollectionEntity() == null) {
			this.setCollectionName(null);
		} else {
			this.setCollectionName(ae.getCollectionEntity().getCollectionName());
		}
		this.setId(ae.getUploadInfoId());
		this.setCreatedBy(ae.getOwner());
		this.setOwner(this.getCreatedBy());
		this.setModifiedBy(ae.getModifiedBy());
		if (ae.getDateCreated() != null) {
			this.setActivityDate(sdf.format(ae.getDateCreated()));
		}
		this.setAeName(aeName);
		ae.getAeName();

		this.setAeName(ae.getAeName());

		// Build of the real path
		// Issue 618 If the inserEntity has no value, insertName is null
		String insName = null;
		if (ae.getInsertEntity() != null) {
			insName = ae.getInsertEntity().getInsertName();
		}

		if (ae.getUploadFileEntities() != null
				&& !ae.getUploadFileEntities().isEmpty()) {
			List<String> list = new ArrayList<String>();
			String realPath = org.medici.mia.common.util.FileUtils
					.getRealPathJSONResponse(ae.getRepositoryEntity()
							.getLocation(), ae.getRepositoryEntity()
							.getRepositoryAbbreviation(), ae
							.getCollectionEntity().getCollectionAbbreviation(),
							ae.getVolumeEntity().getVolume(), insName);

			for (UploadFileEntity uf : ae.getUploadFileEntities()) {
				if (uf.getLogicalDelete() == null || uf.getLogicalDelete() == 0) {
					list.add(realPath + FileUtils.THUMB_FILES_DIR
							+ FileUtils.OS_SLASH + uf.getFilename()
							+ "&WID=250");
				}

			}
			this.setThumbPaths(list);
		}

		if (ae.getLogicalDelete() != null && ae.getLogicalDelete() == 1) {
			this.setAction(NewsActionType.DELETED.toString());
			this.setModifiedBy(ae.getModifiedBy());
			if(ae.getDateDeleted() == null){
				this.setActivityDate(null);
			}
			else {
				this.setActivityDate(sdf.format(ae.getDateDeleted()));
			}
		} else if (ae.getDateModified() == null) {
			this.setAction(NewsActionType.CREATED.toString());
			if(ae.getDateCreated() == null){
				this.setActivityDate(null);
			}
			else {
				this.setActivityDate(sdf.format(ae.getDateCreated()));
			}
		} else if (ae.getDateModified() != null
				&& ae.getDateModified().after(ae.getDateCreated())) {
			this.setAction(NewsActionType.MODIFIED.toString());
			this.setActivityDate(sdf.format(ae.getDateModified()));
			this.setModifiedBy(ae.getModifiedBy());
		} else {
			this.setAction(NewsActionType.CREATED.toString());
			if(ae.getDateCreated() == null){
				this.setActivityDate(null);
			}
			else {
				this.setActivityDate(sdf.format(ae.getDateCreated()));
			}
		}
		return this;
	}
}