package org.medici.mia.common.json;

import org.medici.mia.domain.AltName;
import org.medici.mia.domain.People;
import org.medici.mia.domain.People.Gender;

public class PeopleJson extends PeopleBaseJson {

	private static final long serialVersionUID = 1L;

	private String mapNameLf;
	private Gender gender;
	// new values for findPeople response
	private String activeStart;
	private String activeEnd;
	private Integer bornYear;
	private Integer deathYear;

	private String firstName;
	private String lastName;
	private String lastPrefix;
	private String sucNum;

	public String getMapNameLf() {
		return mapNameLf;
	}

	public void setMapNameLf(String mapNameLf) {
		this.mapNameLf = mapNameLf;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getActiveStart() {
		return activeStart;
	}

	public void setActiveStart(String activeStart) {
		this.activeStart = activeStart;
	}

	public String getActiveEnd() {
		return activeEnd;
	}

	public void setActiveEnd(String activeEnd) {
		this.activeEnd = activeEnd;
	}

	public Integer getBornYear() {
		return bornYear;
	}

	public void setBornYear(Integer bornYear) {
		this.bornYear = bornYear;
	}

	public Integer getDeathYear() {
		return deathYear;
	}

	public void setDeathYear(Integer deathYear) {
		this.deathYear = deathYear;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastPrefix() {
		return lastPrefix;
	}

	public void setLastPrefix(String lastPrefix) {
		this.lastPrefix = lastPrefix;
	}

	public String getSucNum() {
		return sucNum;
	}

	public void setSucNum(String sucNum) {
		this.sucNum = sucNum;
	}

	public void toJson(AltName entity) {
		if (entity == null)
			return;
		if (entity instanceof AltName) {

			this.setId(entity.getPerson().getPersonId());
			this.setMapNameLf(entity.getPerson().getMapNameLf());
			this.setGender(entity.getPerson().getGender());
			this.setActiveStart(entity.getPerson().getActiveStart());
			this.setActiveEnd(entity.getPerson().getActiveEnd());
			this.setBornYear(entity.getPerson().getBornYear());
			this.setDeathYear(entity.getPerson().getDeathYear());
			this.setUnsure(entity.getPerson().getUnsure());
			this.setFirstName(entity.getPerson().getFirst());
			this.setLastName(entity.getPerson().getLast());
			this.setLastPrefix(entity.getPerson().getLastPrefix());
			this.setSucNum(entity.getPerson().getSucNum());

		}

	}

	public void toJson(People people) {
		if (people == null)
			return;
		if (people instanceof People) {

			this.setId(people.getPersonId());
			this.setMapNameLf(people.getMapNameLf());
			this.setGender(people.getGender());
			this.setActiveStart(people.getActiveStart());
			this.setActiveEnd(people.getActiveEnd());
			this.setBornYear(people.getBornYear());
			this.setDeathYear(people.getDeathYear());
			this.setFirstName(people.getFirst());
			this.setLastName(people.getLast());

		}

	}

}