package org.medici.mia.common.json.collation;

import org.medici.mia.domain.Collation;

import java.io.Serializable;

public class LacunaCollationJson implements Serializable {



    private Integer id;
    private Integer childDocumentId;
    private Integer childVolumeId;
    private Integer parentVolumeId;
    private Integer childInsertId;
    private Integer parentInsertId;
    private String folioNum;
    private String folioRV;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getChildDocumentId() {
        return childDocumentId;
    }

    public void setChildDocumentId(Integer childDocumentId) {
        this.childDocumentId = childDocumentId;
    }

    public Integer getChildVolumeId() {
        return childVolumeId;
    }

    public void setChildVolumeId(Integer childVolumeId) {
        this.childVolumeId = childVolumeId;
    }

    public Integer getParentVolumeId() {
        return parentVolumeId;
    }

    public void setParentVolumeId(Integer parentVolumeId) {
        this.parentVolumeId = parentVolumeId;
    }

    public Integer getChildInsertId() {
        return childInsertId;
    }

    public void setChildInsertId(Integer childInsertId) {
        this.childInsertId = childInsertId;
    }

    public Integer getParentInsertId() {
        return parentInsertId;
    }

    public void setParentInsertId(Integer parentInsertId) {
        this.parentInsertId = parentInsertId;
    }

    public String getFolioNum() {
        return folioNum;
    }

    public void setFolioNum(String folioNum) {
        this.folioNum = folioNum;
    }

    public String getFolioRV() {
        return folioRV;
    }

    public void setFolioRV(String folioRV) {
        this.folioRV = folioRV;
    }

    public void toJson(Collation c){
        this.setId(c.getId());
        this.setChildDocumentId(c.getChildDocument() == null ? null : c.getChildDocument().getDocumentEntityId());
        this.setChildVolumeId(c.getChildVolume() == null ? null : c.getChildVolume().getSummaryId());
        this.setParentVolumeId(c.getParentVolume() == null ? null : c.getParentVolume().getSummaryId());
        this.setChildInsertId(c.getChildInsert() == null ? null : c.getChildInsert().getInsertId());
        this.setParentInsertId(c.getParentInsert() == null ? null : c.getParentInsert().getInsertId());
        this.setFolioNum(c.getFolioNumber());
        this.setFolioRV(c.getFoliorv());
    }
}
