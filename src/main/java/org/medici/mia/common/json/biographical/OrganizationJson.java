package org.medici.mia.common.json.biographical;

import java.io.Serializable;
import java.util.List;

public class OrganizationJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer personId;

	private List<HeadquarterJson> headquarters;

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public List<HeadquarterJson> getHeadquarters() {
		return headquarters;
	}

	public void setHeadquarters(List<HeadquarterJson> headquarters) {
		this.headquarters = headquarters;
	}

}