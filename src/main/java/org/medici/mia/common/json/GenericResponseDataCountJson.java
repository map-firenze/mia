package org.medici.mia.common.json;

public class GenericResponseDataCountJson<T> extends GenericResponseDataJson<T> {
    private Integer count;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
