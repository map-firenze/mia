package org.medici.mia.common.json.historylog;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.solr.common.util.Hash;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.domain.HistoryLogEntity.RecordType;
import org.medici.mia.domain.UserHistory;

public class ActionHistoryJson implements Serializable {

    private static final long serialVersionUID = 1L;
    private final SimpleDateFormat sdf = new SimpleDateFormat(
            "yyyy-MM-dd hh:mm:ss");

    private Integer recordId;
    private String accessedFrom;
    private String dateAndTime;
    private Object data;
    private String recordType;
    private String user;
    private String actionType;

    public ActionHistoryJson toJson(HistoryLogEntity entity) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        if (entity == null) {
            return null;
        } else {
            this.setRecordId(entity.getEntryId());
            this.setAccessedFrom(entity.getAccessedFrom());
            this.setDateAndTime(entity.getDateAndTime() == null ? null : sdf.format(entity.getDateAndTime()));
            this.setData(gson.fromJson(entity.getSerializedData(), Object.class));
            this.setRecordType(entity.getRecordType().toString());
            this.setUser(entity.getUser().getAccount());
            this.setActionType(entity.getAction().toString());
            return this;
        }
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public String getAccessedFrom() {
        return accessedFrom;
    }

    public void setAccessedFrom(String accessedFrom) {
        this.accessedFrom = accessedFrom;
    }

    public String getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(String dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }
}