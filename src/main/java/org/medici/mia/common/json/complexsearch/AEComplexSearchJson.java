package org.medici.mia.common.json.complexsearch;


/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class AEComplexSearchJson extends ComplexSearchJson {

	private static final long serialVersionUID = 3787731742866353474L;

	private boolean onlyMyArchivalEnties;
	private boolean everyoneElsesArchivalEntities;
	private boolean allArchivalEntities;

	

	public boolean isOnlyMyArchivalEnties() {
		return onlyMyArchivalEnties;
	}

	public void setOnlyMyArchivalEnties(boolean onlyMyArchivalEnties) {
		this.onlyMyArchivalEnties = onlyMyArchivalEnties;
	}

	public boolean isEveryoneElsesArchivalEntities() {
		return everyoneElsesArchivalEntities;
	}

	public void setEveryoneElsesArchivalEntities(
			boolean everyoneElsesArchivalEntities) {
		this.everyoneElsesArchivalEntities = everyoneElsesArchivalEntities;
	}

	public boolean isAllArchivalEntities() {
		return allArchivalEntities;
	}

	public void setAllArchivalEntities(boolean allArchivalEntities) {
		this.allArchivalEntities = allArchivalEntities;
	}

	

}
