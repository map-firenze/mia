package org.medici.mia.common.json.message;

public class CommentReportMessageJson {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
