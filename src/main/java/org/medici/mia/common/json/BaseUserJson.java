package org.medici.mia.common.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.medici.mia.domain.User;
import org.medici.mia.domain.UserRole;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public class BaseUserJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private String account;

	private String firstName;

	private String lastName;

	private String email;

	private String city;

	private String country;

	private List<String> userGroups;
	
	private Boolean hideActivities;
	
	private Boolean hidePublications;
	
	private Boolean hideStatistics;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<String> getUserGroups() {
		if(userGroups == null){
			userGroups = new ArrayList<String>();
		}
		return userGroups;
	}

	public void setUserGroups(List<String> userGroups) {
		this.userGroups = userGroups;
	}
	
	public Boolean getHideActivities() {
		return hideActivities;
	}

	public void setHideActivities(Boolean hideActivities) {
		this.hideActivities = hideActivities;
	}

	public Boolean getHidePublications() {
		return hidePublications;
	}

	public void setHidePublications(Boolean hidePublications) {
		this.hidePublications = hidePublications;
	}

	public Boolean getHideStatistics() {
		return hideStatistics;
	}

	public void setHideStatistics(Boolean hideStatistics) {
		this.hideStatistics = hideStatistics;
	}


	public BaseUserJson toJson(User user) {

		this.setAccount(user.getAccount());
		this.setFirstName(user.getFirstName());
		this.setLastName(user.getLastName());
		this.setEmail(user.getMail());
		this.setCity(user.getCity());
		this.setCountry(user.getCountry());
		this.setHideActivities(user.getHideActivities());
		this.setHidePublications(user.getHidePublications());
		this.setHideStatistics(user.getHideStatistics());
	
		if (user.getUserRoles() != null && user.getUserRoles().size() > 0) {

			List<UserGroupJson> userGroups = new ArrayList<UserGroupJson>();

			for (UserRole userRole : user.getUserRoles()) {
				UserGroupJson userG = new UserGroupJson();
				userG.setGroup(userRole.getUserAuthority().getAuthority()
						.toString());
				userG.setPriority(userRole.getUserAuthority().getPriority());
				userGroups.add(userG);
			}

			Collections.sort(userGroups, UserGroupJson.USER_GROUP_PRIORITY);
			for(UserGroupJson userGroup: userGroups){
				this.getUserGroups().add(userGroup.getGroup());
			}
			
		}

		return this;
	}

	public User toEntity(User user) {
		user.setAccount(this.getAccount());
		user.setFirstName(this.getFirstName());
		user.setLastName(this.getLastName());
		user.setMail(this.getEmail());
		user.setCity(this.getCity());
		user.setCountry(this.getCountry());
		user.setUserGroups(this.getUserGroups());
		user.setHideActivities(this.getHideActivities());
		user.setHidePublications(this.getHidePublications());
		user.setHideStatistics(this.getHideStatistics());
		
		return user;
	}
}
