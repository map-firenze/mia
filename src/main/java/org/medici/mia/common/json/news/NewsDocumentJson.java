package org.medici.mia.common.json.news;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

import org.medici.mia.common.util.FileUtils;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.UploadInfoEntity;
import org.medici.mia.domain.People;
import org.medici.mia.domain.Place;

public class NewsDocumentJson extends NewsBaseJson {

	private static final long serialVersionUID = 1L;

	private final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	private List<String> thumbPaths;

	private String action;

	private String modifiedBy;

	//new fields

	private Integer privacy;

	private String typology;

	private String deTitle;

	private HashMap<String, Integer> date;

	private List<HashMap> producers;

	private List<HashMap> placeOfOrigin;

	private String category;

	private String owner;

	private List<HashMap> transcription;

	private String collectionName;

	public List<HashMap> getTranscription() {
		return transcription;
	}

	public void setTranscription(List<HashMap> transcription) {
		this.transcription = transcription;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<String> getThumbPaths() {
		return thumbPaths;
	}

	public void setThumbPaths(List<String> thumbPaths) {
		this.thumbPaths = thumbPaths;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Integer getPrivacy() {
		return privacy;
	}

	public void setPrivacy(Integer privacy) {
		this.privacy = privacy;
	}

	public String getTypology() {
		return typology;
	}

	public void setTypology(String typology) {
		this.typology = typology;
	}

	public String getDeTitle() {
		return deTitle;
	}

	public void setDeTitle(String deTitle) {
		this.deTitle = deTitle;
	}

	public HashMap<String, Integer> getDate() {
		return date;
	}

	public void setDate(HashMap<String, Integer> date) {
		this.date = date;
	}

	public List<HashMap> getProducers() {
		return producers;
	}

	public void setProducers(List<HashMap> producers) {
		this.producers = producers;
	}

	public List<HashMap> getPlaceOfOrigin() {
		return placeOfOrigin;
	}

	public void setPlaceOfOrigin(List<HashMap> placeOfOrigin) {
		this.placeOfOrigin = placeOfOrigin;
	}

	public String getCollectionName() {
		return collectionName;
	}

	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}

	public NewsDocumentJson toJson(MiaDocumentEntity entity) {

		this.setType("document");
		this.setCategory(entity.getCategory());
		this.setId(entity.getDocumentEntityId());
		this.setCreatedBy(entity.getCreatedBy());
		this.setOwner(this.getCreatedBy());
		this.setModifiedBy(entity.getLastUpdateBy());
		if (entity.getDateCreated() != null) {
			this.setActivityDate(sdf.format(entity.getDateCreated()));
		}

		// Build of the real path
		if (entity.getUploadFileEntities() != null
				&& !entity.getUploadFileEntities().isEmpty()) {

			UploadInfoEntity ae = entity.getUploadFileEntities().get(0)
					.getUploadInfoEntity();

			this.setCollectionName(ae.getCollectionEntity().getCollectionName());

			// Issue 618 If the inserEntity has no value, insertName is null
			String insName = null;
			if (ae.getInsertEntity() != null) {
				insName = ae.getInsertEntity().getInsertName();
			}
			
			if (ae.getLogicalDelete() == null || ae.getLogicalDelete() == 0) {
				List<String> list = new ArrayList<String>();
				String realPath = org.medici.mia.common.util.FileUtils
						.getRealPathJSONResponse(ae.getRepositoryEntity()
								.getLocation(), ae.getRepositoryEntity()
								.getRepositoryAbbreviation(), ae
								.getCollectionEntity()
								.getCollectionAbbreviation(), ae
								.getVolumeEntity().getVolume(), insName);

				for (UploadFileEntity uf : entity.getUploadFileEntities()) {
					if (uf.getLogicalDelete() == null
							|| uf.getLogicalDelete() == 0) {
						list.add(realPath + FileUtils.THUMB_FILES_DIR
								+ FileUtils.OS_SLASH + uf.getFilename()
								+ "&WID=250");
					}

				}
				this.setThumbPaths(list);
			}

		}

		if (entity.getFlgLogicalDelete() != null
				&& entity.getFlgLogicalDelete()) {
			this.setAction(NewsActionType.DELETED.toString());
			this.setActivityDate(entity.getDateDeleted() == null ? null : sdf.format(entity.getDateDeleted()));
		} else if (entity.getDateLastUpdate() == null) {
			this.setAction(NewsActionType.CREATED.toString());
			this.setActivityDate(entity.getDateCreated() == null ? null : sdf.format(entity.getDateCreated()));
		} else if (entity.getDateLastUpdate() != null && entity.getDateCreated() != null
				&& entity.getDateLastUpdate().after(entity.getDateCreated())) {
			this.setAction(NewsActionType.MODIFIED.toString());
			this.setActivityDate(sdf.format(entity.getDateLastUpdate()));
		} else {
			this.setAction(NewsActionType.CREATED.toString());
			this.setActivityDate(entity.getDateCreated() == null ? null : sdf.format(entity.getDateCreated()));
		}

		this.setPrivacy(entity.getPrivacy());

		this.setDeTitle(entity.getDeTitle());

		this.setTypology(entity.getTypology());

		HashMap<String, Integer> dateHash = new HashMap<String, Integer>();
		dateHash.put("docYear", entity.getDocYear());
		dateHash.put("docMonth", entity.getDocMonth());
		dateHash.put("docDay", entity.getDocDay());
		this.setDate(dateHash);

		List<HashMap> producersHashList = new ArrayList<HashMap>();
		List<People> producersList = entity.getProducers();
		for(People person : producersList){
			HashMap<String, String> item = new HashMap<String, String>();
			item.put("mapNameLf", person.getMapNameLf());
			producersHashList.add(item);
		}
		if(producersHashList.size() > 0) {
			this.setProducers(producersHashList);
		} else {
			this.setProducers(null);
		}

		HashMap<String, String> placeName = new HashMap<String, String>();
		Place place = entity.getPlaceOfOriginEntity();
		if(place != null){
			placeName.put("placeName", place.getPlaceName());
		} else {
			placeName.put("placeName", null);
		}
		List<HashMap> placeHashList = new ArrayList<HashMap>();
		placeHashList.add(placeName);
		if(placeName.get("placeName") == null){
			this.setPlaceOfOrigin(null);
		} else {
			this.setPlaceOfOrigin(placeHashList);
		}

		List<HashMap> transcriptionList = new ArrayList<HashMap>();
		HashMap<String, Object> transcription = new HashMap<String, Object>();

		if(entity.getDocumentTranscriptions().size() > 0) {
			transcription.put("docTranscriptionId", entity.getDocumentTranscriptions().get(0).getDocTranscriptionId());
			transcription.put("transcription", entity.getDocumentTranscriptions().get(0).getTranscription());
			transcription.put("documentEntityId", entity.getDocumentTranscriptions().get(0).getDocumentEntityId());
			transcription.put("uploadedFileId", entity.getDocumentTranscriptions().get(0).getUploadedFileId());
			transcriptionList.add(transcription);
			this.setTranscription(transcriptionList);
		} else {
			this.setTranscription(null);
		}


//		private Integer privacy;
//
//		private String typology;
//
//		private String deTitle;
//
//		private Hashmap<String, String> date;
//
//		private List<HashMap> producers;
//
//		private HashMap<String, String> placeOfOrigin;

		return this;
	}
}