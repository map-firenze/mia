package org.medici.mia.common.json.document;

import java.io.Serializable;
import java.util.List;

/** 
 * 
 * @author Shadab Bigdel (<a href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class DocumentPeopleJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer personId;
	private List<DocumentPeopleFieldsJson> docPeople;
//	private List<DocumentPeopleBaseJson> docRefPeople;

	public DocumentPeopleJson() {

	}

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public List<DocumentPeopleFieldsJson> getDocPeople() {
		return docPeople;
	}

	public void setDocPeople(List<DocumentPeopleFieldsJson> docPeople) {
		this.docPeople = docPeople;
	}

//	public List<DocumentPeopleBaseJson> getDocRefPeople() {
//		return docRefPeople;
//	}
//
//	public void setDocRefPeople(List<DocumentPeopleBaseJson> docRefPeople) {
//		this.docRefPeople = docRefPeople;
//	}
	
	

	
}
