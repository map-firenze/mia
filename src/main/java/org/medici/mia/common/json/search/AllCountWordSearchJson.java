package org.medici.mia.common.json.search;

import java.io.Serializable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class AllCountWordSearchJson implements Serializable {

	private static final long serialVersionUID = 3787731742866353474L;

	private String searchType;
	private String matchSearchWords;
	private String partialSearchWords;

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getMatchSearchWords() {
		return matchSearchWords;
	}

	public void setMatchSearchWords(String matchSearchWords) {
		this.matchSearchWords = matchSearchWords;
	}

	public String getPartialSearchWords() {
		return partialSearchWords;
	}

	public void setPartialSearchWords(String partialSearchWords) {
		this.partialSearchWords = partialSearchWords;
	}


}
