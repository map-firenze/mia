package org.medici.mia.common.json.biographical;

import java.io.Serializable;

public class ModifyPersonSpouseJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer personId;

	private SpouseJson newSpouse;
	private SpouseJson oldSpouse;
	private Integer spouseToBeDeleted;

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public SpouseJson getNewSpouse() {
		return newSpouse;
	}

	public void setNewSpouse(SpouseJson newSpouse) {
		this.newSpouse = newSpouse;
	}

	public SpouseJson getOldSpouse() {
		return oldSpouse;
	}

	public void setOldSpouse(SpouseJson oldSpouse) {
		this.oldSpouse = oldSpouse;
	}

	public Integer getSpouseToBeDeleted() {
		return spouseToBeDeleted;
	}

	public void setSpouseToBeDeleted(Integer spouseToBeDeleted) {
		this.spouseToBeDeleted = spouseToBeDeleted;
	}

}