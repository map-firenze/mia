package org.medici.mia.common.json.document;

import java.io.Serializable;
import java.util.List;

public class FindBiblioForDocJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private String documentId;
	private List<BiblioBaseJson> biblios;

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public List<BiblioBaseJson> getBiblios() {
		return biblios;
	}

	public void setBiblios(List<BiblioBaseJson> biblios) {
		this.biblios = biblios;
	}

}