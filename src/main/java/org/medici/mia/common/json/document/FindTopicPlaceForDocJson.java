package org.medici.mia.common.json.document;

import java.io.Serializable;
import java.util.List;

import org.medici.mia.common.json.TopicPlaceBaseJson;

public class FindTopicPlaceForDocJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private String documentId;
	private List<TopicPlaceBaseJson> topicPlaces;

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public List<TopicPlaceBaseJson> getTopicPlaces() {
		return topicPlaces;
	}

	public void setTopicPlaces(List<TopicPlaceBaseJson> topicPlaces) {
		this.topicPlaces = topicPlaces;
	}

}