package org.medici.mia.common.json.casestudy;

import org.medici.mia.domain.CaseStudy;
import org.medici.mia.domain.CaseStudyFolder;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.WeakHashMap;

public class CaseStudyBAALJson extends CaseStudyJson implements Serializable {
    private Integer folderId;
    private Integer documentId;

    public CaseStudyBAALJson() {
    }

    public CaseStudyBAALJson(Integer id, String title, String description, String notes, String createdAt, String createdBy, Integer folderId) {
        this.setId(id);
        this.setTitle(title);
        this.setDescription(description);
        this.setNotes(notes);
        this.setCreatedAt(createdAt);
        this.setCreatedBy(createdBy);
        this.setFolderId(folderId);
    }

    public static CaseStudyBAALJson toJson(CaseStudy cs, WeakHashMap<String, Integer> ids){
        SimpleDateFormat sdf = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        CaseStudyBAALJson json = new CaseStudyBAALJson();
        json.setId(cs.getId());
        json.setTitle(cs.getTitle());
        json.setDescription(cs.getDescription());
        json.setNotes(cs.getNotes());
        json.setCreatedAt(cs.getCreatedAt() == null ? null : sdf.format(cs.getCreatedAt()));
        json.setCreatedBy(cs.getCreatedBy() == null ? null : cs.getCreatedBy().getAccount());
        if(ids != null) {
            json.setFolderId(ids.get("folderId"));
            json.setDocumentId(ids.get("documentId"));
        }
        return json;
    }

    public Integer getFolderId() {
        return folderId;
    }

    public void setFolderId(Integer folderId) {
        this.folderId = folderId;
    }

    public Integer getDocumentId() {
        return documentId;
    }

    public void setDocumentId(Integer documentId) {
        this.documentId = documentId;
    }
}
