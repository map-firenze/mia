package org.medici.mia.common.json.news;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.medici.mia.service.useractivity.INewsUserJson;

public class NewsBaseJson implements INewsUserJson, Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String type;
	private String activityDate;
	private String createdBy;

	private final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(String activityDate) {
		this.activityDate = activityDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public Date compareDate() {

		try {
			if (getActivityDate() != null) {
				return sdf.parse(getActivityDate());
			}

		} catch (Exception e) {
			// TODO: handle exception
		}

		return null;

	}

}