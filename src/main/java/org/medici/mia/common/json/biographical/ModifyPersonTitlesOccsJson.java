package org.medici.mia.common.json.biographical;

import java.io.Serializable;

public class ModifyPersonTitlesOccsJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer personId;
	private Integer oldTitleAndOccupation;
	private Integer newTitleAndOccupation;
	private Integer titleAndOccupationToBeDeleted;
	private Integer preferredRole;
	private PoLinkJson poLink;

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public Integer getOldTitleAndOccupation() {
		return oldTitleAndOccupation;
	}

	public void setOldTitleAndOccupation(Integer oldTitleAndOccupation) {
		this.oldTitleAndOccupation = oldTitleAndOccupation;
	}

	public Integer getNewTitleAndOccupation() {
		return newTitleAndOccupation;
	}

	public void setNewTitleAndOccupation(Integer newTitleAndOccupation) {
		this.newTitleAndOccupation = newTitleAndOccupation;
	}

	public Integer getPreferredRole() {
		return preferredRole;
	}

	public void setPreferredRole(Integer preferredRole) {
		this.preferredRole = preferredRole;
	}

	public Integer getTitleAndOccupationToBeDeleted() {
		return titleAndOccupationToBeDeleted;
	}

	public void setTitleAndOccupationToBeDeleted(
			Integer titleAndOccupationToBeDeleted) {
		this.titleAndOccupationToBeDeleted = titleAndOccupationToBeDeleted;
	}

	public PoLinkJson getPoLink() {
		return poLink;
	}

	public void setPoLink(PoLinkJson poLink) {
		this.poLink = poLink;
	}

}