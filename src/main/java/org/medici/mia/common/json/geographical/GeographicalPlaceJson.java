package org.medici.mia.common.json.geographical;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.medici.mia.domain.Place;
import org.medici.mia.domain.User;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public class GeographicalPlaceJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private transient final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd hh:mm:ss");

	private Integer placeId;
	private Integer principalPlaceId;
	private String placeNameFull;
	private String plNameFullPlType;
	private String placeName;
	private String termAccent;
	private String plType;
	private String prefFlag;
	private Integer plParentPlaceAllId; // refer to the attr parentPlace (Id di
										// place)
	private String plParent;
	private String placesMemo;
	private Integer logicalDelete;
	private String dateCreated;
	private String lastUpdate;
	private String createdBy; // User account
	private String lastUpdateBy; // User Account
	private Integer commentsCount;

	public GeographicalPlaceJson toJson(Place place) {

		this.setPlaceId(place.getPlaceAllId());
		if (place.getCreatedBy() != null) {
			this.setCreatedBy(place.getCreatedBy().getAccount());
		}

		if (place.getLastUpdate() != null) {
			this.setLastUpdate(sdf.format(place.getLastUpdate()));
		}

		if (place.getDateCreated() != null) {
			this.setDateCreated(sdf.format(place.getDateCreated()));
		}

		if (place.getLastUpdateBy() != null) {
			this.setLastUpdateBy(place.getLastUpdateBy().getAccount());
		}
		if (place.getLogicalDelete() != null && place.getLogicalDelete()) {
			this.setLogicalDelete(1);
		} else {
			this.setLogicalDelete(0);
		}
		this.setPlaceName(place.getPlaceName());
		this.setPlaceNameFull(place.getPlaceNameFull());
		this.setPlacesMemo(place.getPlacesMemo());
		this.setPlNameFullPlType(place.getPlNameFullPlType());
		if (place.getParentPlace() != null) {
			this.setPlParentPlaceAllId(place.getParentPlace().getPlaceAllId());
		}
//		this.setPlParent(place.getPlParent());
		this.setPlParent(place.getParentPlace().getPlaceNameFull());
		this.setPlType(place.getPlType());
		this.setPrefFlag(place.getPrefFlag());
		this.setTermAccent(place.getTermAccent());
		this.setCommentsCount(place.getCommentsCount());

		return this;
	}

	public GeographicalPlaceJson toJson(Place place, Integer principalPlaceId) {

		this.toJson(place);
		this.setPrincipalPlaceId(principalPlaceId);

		return this;
	}

	public Place toEntity(Place placeEntity) {

		placeEntity.setPlaceAllId(this.getPlaceId());
		if (this.getCreatedBy() != null) {
			User user = new User();
			user.setAccount(this.getCreatedBy());
			placeEntity.setCreatedBy(user);
		}
		if (this.getLastUpdateBy() != null) {
			User lastUpdateBy = new User();
			lastUpdateBy.setAccount(this.getLastUpdateBy());
			placeEntity.setLastUpdateBy(lastUpdateBy);
		}

		try {
			if (this.getLastUpdate() != null) {
				placeEntity.setLastUpdate(sdf.parse(this.getLastUpdate()));
			}

			if (this.getDateCreated() != null) {
				placeEntity.setDateCreated(sdf.parse(this.getDateCreated()));
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		placeEntity.setPlaceName(this.getPlaceName());
		placeEntity.setPlaceNameFull(this.getPlaceNameFull());
		placeEntity.setPlacesMemo(this.getPlacesMemo());
		placeEntity.setPlNameFullPlType(this.getPlNameFullPlType());
		if (this.getPlParentPlaceAllId() != null) {
			Place plParentPlace = new Place();
			plParentPlace.setPlaceAllId(this.getPlParentPlaceAllId());
			placeEntity.setParentPlace(plParentPlace);
		}
		placeEntity.setPlParent(this.getPlParent());
		placeEntity.setPlType(this.getPlType());
		placeEntity.setPrefFlag(this.getPrefFlag());
		placeEntity.setTermAccent(this.getTermAccent());

		return placeEntity;

	}

	public Integer getPlaceId() {
		return placeId;
	}

	public void setPlaceId(Integer placeId) {
		this.placeId = placeId;
	}

	public String getPlaceNameFull() {
		return placeNameFull;
	}

	public void setPlaceNameFull(String placeNameFull) {
		this.placeNameFull = placeNameFull;
	}

	public String getPlNameFullPlType() {
		return plNameFullPlType;
	}

	public void setPlNameFullPlType(String plNameFullPlType) {
		this.plNameFullPlType = plNameFullPlType;
	}

	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	public String getTermAccent() {
		return termAccent;
	}

	public void setTermAccent(String termAccent) {
		this.termAccent = termAccent;
	}

	public String getPlType() {
		return plType;
	}

	public void setPlType(String plType) {
		this.plType = plType;
	}

	public String getPrefFlag() {
		return prefFlag;
	}

	public void setPrefFlag(String prefFlag) {
		this.prefFlag = prefFlag;
	}

	public Integer getPlParentPlaceAllId() {
		return plParentPlaceAllId;
	}

	public void setPlParentPlaceAllId(Integer plParentPlaceAllId) {
		this.plParentPlaceAllId = plParentPlaceAllId;
	}

	public String getPlParent() {
		return plParent;
	}

	public void setPlParent(String plParent) {
		this.plParent = plParent;
	}

	public String getPlacesMemo() {
		return placesMemo;
	}

	public void setPlacesMemo(String placesMemo) {
		this.placesMemo = placesMemo;
	}

	public Integer getLogicalDelete() {
		return logicalDelete;
	}

	public void setLogicalDelete(Integer logicalDelete) {
		this.logicalDelete = logicalDelete;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

	public Integer getPrincipalPlaceId() {
		return principalPlaceId;
	}

	public void setPrincipalPlaceId(Integer principalPlaceId) {
		this.principalPlaceId = principalPlaceId;
	}

	public Integer getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(Integer commentsCount) {
		this.commentsCount = commentsCount;
	}
}