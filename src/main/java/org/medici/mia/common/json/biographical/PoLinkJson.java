package org.medici.mia.common.json.biographical;

import java.io.Serializable;

import org.medici.mia.domain.PoLink;

public class PoLinkJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer startYear;
	private String startMonth;
	private Integer startDay;
	private Integer startApprox;
	private Integer startUnsure;
	private Integer endYear;
	private String endMonth;
	private Integer endDay;
	private Integer endApprox;
	private Integer endUnsure;

	public void toJson(PoLink entity) {
		if (entity == null)
			return;

		this.setStartDay(entity.getStartDay());
		this.setStartMonth(entity.getStartMonth());
		this.setStartYear(entity.getStartYear());
		if (entity.getStartApprox() != null && entity.getStartApprox()) {
			this.setStartApprox(1);
		} else {
			this.setStartApprox(0);
		}
		if (entity.getStartUns() != null && entity.getStartUns()) {
			this.setStartUnsure(1);
		} else {
			this.setStartUnsure(0);
		}

		this.setEndDay(entity.getEndDay());
		this.setEndMonth(entity.getEndMonth());
		this.setEndYear(entity.getEndYear());
		if (entity.getEndApprox() != null && entity.getEndApprox()) {
			this.setEndApprox(1);
		} else {
			this.setEndApprox(0);
		}
		if (entity.getEndUns() != null && entity.getEndUns()) {
			this.setEndUnsure(1);
		} else {
			this.setEndUnsure(0);
		}

	}

	public Integer getStartYear() {
		return startYear;
	}

	public void setStartYear(Integer startYear) {
		this.startYear = startYear;
	}

	public String getStartMonth() {
		return startMonth;
	}

	public void setStartMonth(String startMonth) {
		this.startMonth = startMonth;
	}

	public Integer getStartDay() {
		return startDay;
	}

	public void setStartDay(Integer startDay) {
		this.startDay = startDay;
	}

	public Integer getStartApprox() {
		return startApprox;
	}

	public void setStartApprox(Integer startApprox) {
		this.startApprox = startApprox;
	}

	public Integer getStartUnsure() {
		return startUnsure;
	}

	public void setStartUnsure(Integer startUnsure) {
		this.startUnsure = startUnsure;
	}

	public Integer getEndYear() {
		return endYear;
	}

	public void setEndYear(Integer endYear) {
		this.endYear = endYear;
	}

	public String getEndMonth() {
		return endMonth;
	}

	public void setEndMonth(String endMonth) {
		this.endMonth = endMonth;
	}

	public Integer getEndApprox() {
		return endApprox;
	}

	public void setEndApprox(Integer endApprox) {
		this.endApprox = endApprox;
	}

	public Integer getEndUnsure() {
		return endUnsure;
	}

	public void setEndUnsure(Integer endUnsure) {
		this.endUnsure = endUnsure;
	}

	public Integer getEndDay() {
		return endDay;
	}

	public void setEndDay(Integer endDay) {
		this.endDay = endDay;
	}

}