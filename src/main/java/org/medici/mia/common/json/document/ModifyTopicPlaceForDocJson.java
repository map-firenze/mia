package org.medici.mia.common.json.document;

import java.io.Serializable;

import org.medici.mia.common.json.TopicPlaceBaseJson;

public class ModifyTopicPlaceForDocJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private TopicPlaceBaseJson topicPlaceToBeDeleted;
	private String documentId;
	private TopicPlaceBaseJson newTopicPlace;
	private TopicPlaceBaseJson oldTopicPlace;

	public ModifyTopicPlaceForDocJson() {

	}

	public TopicPlaceBaseJson getTopicPlaceToBeDeleted() {
		return topicPlaceToBeDeleted;
	}

	public void setTopicPlaceToBeDeleted(
			TopicPlaceBaseJson topicPlaceToBeDeleted) {
		this.topicPlaceToBeDeleted = topicPlaceToBeDeleted;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public TopicPlaceBaseJson getNewTopicPlace() {
		return newTopicPlace;
	}

	public void setNewTopicPlace(TopicPlaceBaseJson newTopicPlace) {
		this.newTopicPlace = newTopicPlace;
	}

	public TopicPlaceBaseJson getOldTopicPlace() {
		return oldTopicPlace;
	}

	public void setOldTopicPlace(TopicPlaceBaseJson oldTopicPlace) {
		this.oldTopicPlace = oldTopicPlace;
	}

}
