package org.medici.mia.common.json.message;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.medici.mia.domain.UserMessage;
import org.medici.mia.domain.UserMessage.RecipientStatus;

public class MessageSendJson extends MessageJson {

	private static final long serialVersionUID = 1L;
	private final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd hh:mm:ss");

	private String to;
	private String from;
	private String account;

	public UserMessage toEntityInbox(UserMessage mesEntity) {

		super.toEntity(mesEntity);
		mesEntity.setSender(this.getAccount());
		mesEntity.setRecipient(this.getTo());
		mesEntity.setSendedDate(new Date());
		mesEntity.setRecipientStatus(RecipientStatus.NOT_READ);

		return mesEntity;

	}

	public UserMessage toEntityOutbox(UserMessage mesEntity) {

		super.toEntity(mesEntity);
		mesEntity.setSender(this.getAccount());
		mesEntity.setRecipient(this.getTo());
		mesEntity.setSendedDate(new Date());
		mesEntity.setRecipientStatus(RecipientStatus.NOT_READ);

		return mesEntity;

	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

}