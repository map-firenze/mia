package org.medici.mia.common.json.document;

import java.io.Serializable;

public class ModifyBiblioForDocJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer idBiblioToBeDeleted;
	private String documentId;
	private BiblioBaseJson newBiblio;

	public Integer getIdBiblioToBeDeleted() {
		return idBiblioToBeDeleted;
	}

	public void setIdBiblioToBeDeleted(Integer idBiblioToBeDeleted) {
		this.idBiblioToBeDeleted = idBiblioToBeDeleted;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public BiblioBaseJson getNewBiblio() {
		return newBiblio;
	}

	public void setNewBiblio(BiblioBaseJson newBiblio) {
		this.newBiblio = newBiblio;
	}

}
