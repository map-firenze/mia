package org.medici.mia.common.json.document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.medici.mia.common.json.FieldJson;
import org.medici.mia.common.json.PeopleBaseJson;
import org.medici.mia.common.json.PeopleJson;
import org.medici.mia.common.json.PlaceJson;
import org.medici.mia.domain.DocumentFieldEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.OfficialEntity;
import org.medici.mia.domain.People;
import org.medici.mia.domain.Place;
import org.medici.mia.service.miadoc.DocumentType;
import org.medici.mia.service.miadoc.MiaDocumentUtils;
import org.medici.mia.service.miadoc.PeopleFieldType;
import org.medici.mia.service.miadoc.PlaceFieldType;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@JsonTypeName("officialRecords")
public class OfficialJson extends GenericDocumentJson {

	private static final long serialVersionUID = 1L;

	@JsonProperty("printer")
	private List<PeopleJson> printers;

	@JsonProperty("printerPlace")
	private List<PlaceJson> printerPlaces;

	public OfficialJson() {
	}

	public List<PeopleJson> getPrinters() {
		return printers;
	}

	public void setPrinters(List<PeopleJson> printers) {
		this.printers = printers;
	}

	public List<PlaceJson> getPrinterPlaces() {
		return printerPlaces;
	}

	public void setPrinterPlaces(List<PlaceJson> printerPlaces) {
		this.printerPlaces = printerPlaces;
	}

	@Override
	public void init(MiaDocumentEntity docEntity) {
		super.init(docEntity);
		if (docEntity.getOfficial() != null) {
			this.printers = getPrinters(docEntity.getOfficial().getPrinters());
			this.printerPlaces = getPrinterPlaces(docEntity.getOfficial()
					.getPrinterPlaces());
		}
	}

	// Using for finding people attached to a document
	@Override
	public List<FindPeopleForDocJson> findPeopleJson(MiaDocumentEntity docEntity) {
		List<FindPeopleForDocJson> modifyPeoples = new ArrayList<FindPeopleForDocJson>();

		FindPeopleForDocJson modifyPeople = new FindPeopleForDocJson();
		modifyPeople.setType(PeopleFieldType.Printer.toString());
		ArrayList<PeopleBaseJson> peopleBases = null;

		if (docEntity.getOfficial() != null
				&& docEntity.getOfficial().getPrinter() != null
				&& !docEntity.getOfficial().getPrinter().isEmpty()) {

			peopleBases = new ArrayList<PeopleBaseJson>();

			for (String peopleIdStr : (List<String>) Arrays.asList(docEntity
					.getOfficial().getPrinter().split(","))) {

				PeopleBaseJson peopleBase = new PeopleBaseJson();
				String[] peoplesIdArr = peopleIdStr.split(":");
				if (peoplesIdArr[0] != null || !peoplesIdArr[0].isEmpty()) {
					peopleBase.setId(Integer.valueOf(peoplesIdArr[0]));
				}
				if (peoplesIdArr.length > 1) {
					peopleBase.setUnsure(peoplesIdArr[1]);
				}

				peopleBases.add(peopleBase);
			}
		}
		modifyPeople.setPeoples(peopleBases);
		modifyPeoples.add(modifyPeople);

		return modifyPeoples;
	}

	// Used for finding place attached to document
	@Override
	public List<FindPlaceForDocJson> findPlaceJson(MiaDocumentEntity docEntity) {
		List<FindPlaceForDocJson> modifyPlaces = new ArrayList<FindPlaceForDocJson>();

		FindPlaceForDocJson modifyPlace = new FindPlaceForDocJson();
		modifyPlace.setType(PlaceFieldType.printerPlace.toString());

		if (docEntity.getOfficial() != null) {
			modifyPlace.setPlaces(MiaDocumentUtils.getPlaceBases(docEntity
					.getOfficial().getPrinterPlace()));
		}
		modifyPlaces.add(modifyPlace);
		return modifyPlaces;
	}

	// Used for modify - add - delete people attached to document
	@Override
	public MiaDocumentEntity getModifiedDocumentEnt(MiaDocumentEntity docEnt,
			ModifyPeopleForDocJson modifyPeople) {

		if (modifyPeople.getType() == null) {
			docEnt.setErrorMsg("The type has no value)");
			return docEnt;
		}

		if (docEnt.getOfficial() == null) {
			docEnt.setErrorMsg("There is no " + docEnt.getCategory()
					+ " record associated with the document.");
			return docEnt;
		}

		if (PeopleFieldType.Printer.toString().equalsIgnoreCase(
				modifyPeople.getType())) {

			String modifedStr = MiaDocumentUtils.modifyPeopleForDoc(docEnt
					.getOfficial().getPrinter(), modifyPeople);
			docEnt.getOfficial().setPrinter(modifedStr);

		} else {
			docEnt.setErrorMsg("The type " + modifyPeople.getType()
					+ " is not compatible with the category "
					+ docEnt.getCategory());
		}

		return docEnt;

	}

	// Used for modify - add - delete place attached to document
	@Override
	public MiaDocumentEntity getModifiedDocumentEnt(MiaDocumentEntity docEnt,
			ModifyPlaceForDocJson modifyPlace) {

		if (modifyPlace.getType() == null) {
			docEnt.setErrorMsg("The type has no value)");
			return docEnt;
		}

		if (docEnt.getOfficial() == null) {
			docEnt.setErrorMsg("There is no " + docEnt.getCategory()
					+ " record associated with the document.");
			return docEnt;
		}

		if (PlaceFieldType.printerPlace.toString().equalsIgnoreCase(
				modifyPlace.getType())) {

			String modifedStr = MiaDocumentUtils.modifyPlaceForDoc(docEnt
					.getOfficial().getPrinterPlace(), modifyPlace);
			docEnt.getOfficial().setPrinterPlace(modifedStr);

		} else {
			docEnt.setErrorMsg("The type " + modifyPlace.getType()
					+ " is not compatible with the category "
					+ docEnt.getCategory());
		}

		return docEnt;

	}

	@Override
	public MiaDocumentEntity getMiaDocumentEntity(DocumentJson docJson) {

		MiaDocumentEntity docEnt = super.getMiaDocumentEntity(docJson);

		docEnt.setCategory(DocumentType.officialRecords.toString());
		OfficialJson offJson = (OfficialJson) docJson;
		OfficialEntity offEnt = new OfficialEntity();

		if (offJson.getPrinters() != null && !offJson.getPrinters().isEmpty()) {
			String printerStr = "";

			for (PeopleJson people : offJson.getPrinters()) {
				printerStr = printerStr + people.getId() + ":";
				if (!StringUtils.isEmpty(people.getUnsure())) {
					printerStr = printerStr + people.getUnsure();
				} else {
					printerStr = printerStr + "S";
				}
				printerStr = printerStr + ",";
			}

			offEnt.setPrinter(printerStr);
		}

		if (offJson.getPrinterPlaces() != null
				&& !offJson.getPrinterPlaces().isEmpty()) {
			String printerPlaceStr = "";

			for (PlaceJson place : offJson.getPrinterPlaces()) {
				printerPlaceStr = printerPlaceStr + place.getId() + ":";
				if (!StringUtils.isEmpty(place.getUnsure())) {
					printerPlaceStr = printerPlaceStr + place.getUnsure();
				} else {
					printerPlaceStr = printerPlaceStr + "S";
				}
				printerPlaceStr = printerPlaceStr + ",";
			}

			offEnt.setPrinterPlace(printerPlaceStr);
		}
		docEnt.setOfficial(offEnt);

		return docEnt;
	}

	private ArrayList<PeopleJson> getPrinters(List<People> peoples) {

		ArrayList<PeopleJson> peopleJsons = new ArrayList<PeopleJson>();

		if (peoples != null && !peoples.isEmpty()) {
			for (People people : peoples) {

				PeopleJson peopleJson = new PeopleJson();
				peopleJson.setId(people.getPersonId());
				peopleJson.setGender(people.getGender());
				peopleJson.setMapNameLf(people.getMapNameLf());
				peopleJson.setActiveStart(people.getActiveStart());
				peopleJson.setActiveEnd(people.getActiveEnd());
				peopleJson.setBornYear(people.getBornYear());
				peopleJson.setDeathYear(people.getDeathYear());
				peopleJson.setFirstName(people.getFirst());
				peopleJson.setLastName(people.getLast());
				peopleJson.setUnsure(people.getUnsure());
				peopleJsons.add(peopleJson);
			}
		}

		return peopleJsons;
	}

	private ArrayList<PlaceJson> getPrinterPlaces(List<Place> places) {

		ArrayList<PlaceJson> placeJsons = new ArrayList<PlaceJson>();

		if (places != null && !places.isEmpty()) {
			for (Place place : places) {

				PlaceJson placeJson = new PlaceJson();
				placeJson.setId(place.getPlaceAllId());
				placeJson.setPlaceNameId(place.getPlaceNameId());
				placeJson.setPlaceName(place.getPlaceNameFull());
				placeJson.setPrefFlag(place.getPrefFlag());
				placeJson.setPlType(place.getPlType());
				placeJson.setUnsure(place.getUnsure());
				placeJsons.add(placeJson);
			}
		}

		return placeJsons;

	}

	@Override
	public List<FieldJson> getJsonFields(
			List<DocumentFieldEntity> documentFields) {

		List<FieldJson> fields = new ArrayList<FieldJson>();
		for (DocumentFieldEntity field : documentFields) {
			if (DocumentType.officialRecords.toString().equalsIgnoreCase(
					field.getDocumentCategory())) {
				boolean isMultiple = false;
				if ("1".equalsIgnoreCase(field.getAllowMultiple())) {
					isMultiple = true;
				}
				FieldJson fieldJson = new FieldJson(field.getFieldName(),
						field.getFieldValue(), field.getFieldBeName(),
						field.getFieldType(), isMultiple);

				fields.add(fieldJson);

			}
		}

		return fields;
	}

	@Override
	public List<DocPeopleFieldJson> getDocPeopleFields(Integer personId,
			MiaDocumentEntity docEnt, List<DocumentFieldEntity> documentFields) {

		List<DocPeopleFieldJson> fields = super.getDocPeopleFields(personId,
				docEnt, documentFields);

		if (docEnt.getOfficial() != null
				&& hasField(docEnt.getOfficial().getPrinter(),
						String.valueOf(personId))) {
			DocPeopleFieldJson field = new DocPeopleFieldJson();
			field.setFieldBeName(PeopleFieldType.Printer.toString());
			field.setFieldName(getFieldName(documentFields,
					docEnt.getCategory(), PeopleFieldType.Printer.toString()));

			if (fields == null)
				fields = new ArrayList<DocPeopleFieldJson>();

			fields.add(field);
		}

		return fields;

	}

}
