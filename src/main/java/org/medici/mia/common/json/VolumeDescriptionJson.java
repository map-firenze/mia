/*
 * MyArchiveVolume.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.common.json;

import java.text.SimpleDateFormat;

import org.hibernate.mapping.Collection;
import org.medici.mia.domain.*;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class VolumeDescriptionJson extends VolumeJson {

	private final transient SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd hh:mm:ss");

	private String collectionId;
	private String collectionName;
	private String collectionAbbr;

	private String sezione;
	private String volumeNo;
	private String volumeTitle;
	private String startYear;
	private String startMonth;
	private String startDay;
	private String endYear;
	private String endMonth;
	private String endDay;
	private String dateNotes;
	private String volumeContext;
	private String otherNotes;

	private String createdBy;
	private String dateCreated;
	private String lastUpdateBy;
	private String dateLastUpdate;
	private String deletedBy;
	private String dateDeleted;
	private String organizationalCriteria;
	private String paginationNotes;

	private String pagination;
	private String folioCount;
	private String missingFolios;
	private Boolean hasInserts;
	private Boolean hasInsertsWithAEs;

	public VolumeDescriptionJson toJson(Volume volume, Schedone schedulone) {

		super.toJson(volume);
		if (volume.getCollectionEntity() != null) {
			this.setCollectionId(volume.getCollectionEntity().getCollectionId()
					.toString());
			this.setCollectionName(volume.getCollectionEntity()
					.getCollectionName());
			this.setCollectionAbbr(volume.getCollectionEntity()
					.getCollectionAbbreviation());
		}

		this.setSezione(volume.getSezione());
		this.setVolumeNo(volume.getVolume());
		this.setVolumeTitle(volume.getTitle());
		this.setStartYear(volume.getStartYear() == null ? "" : volume
				.getStartYear().toString());
		this.setStartMonth(volume.getStartMonthNum() == null ? "" :  "" + volume.getStartMonthNum().getMonthNum());
		this.setStartDay(volume.getStartDay() == null ? "" : volume
				.getStartDay().toString());
		this.setEndYear(volume.getEndYear() == null ? "" : volume
				.getEndYear().toString());
		this.setEndMonth(volume.getEndMonthNum() == null ? "" :  "" + volume.getEndMonthNum().getMonthNum());
		this.setEndDay(volume.getEndDay() == null ? "" : volume.getEndDay()
				.toString());
		this.setDateNotes(volume.getDateNotes());
		this.setVolumeContext(volume.getCcontext());
		this.setOtherNotes(volume.getOtherNotes());

		if (volume.getCreatedBy() != null) {
			this.setCreatedBy(volume.getCreatedBy().getAccount());
		}
		if (volume.getDateCreated() != null) {
			this.setDateCreated(sdf.format(volume.getDateCreated()));
		}
		if (volume.getLastUpdateBy() != null) {
			this.setLastUpdateBy(volume.getLastUpdateBy().getAccount());
		}
		if (volume.getLastUpdate() != null) {
			this.setDateLastUpdate(sdf.format(volume.getLastUpdate()));
		}

		if (volume.getDeletedBy() != null) {
			this.setDeletedBy(volume.getDeletedBy().getAccount());
		}
		if (volume.getDateDeleted() != null) {
			this.setDateDeleted(sdf.format(volume.getDateDeleted()));
		}
		this.setOrganizationalCriteria(volume.getOrgNotes());
		if (schedulone != null) {

			this.setPaginationNotes(schedulone.getNoteCartulazioneEng());

			this.setPagination(schedulone.getCartulazione());

			this.setMissingFolios(schedulone.getCarteMancanti());
		}
		this.setFolioCount(volume.getFolioCount());
		this.setHasInserts(false);
		this.setHasInsertsWithAEs(false);
		if (volume.getInsertEntities() != null) {
			boolean hasIns = false;
			for(InsertEntity ins: volume.getInsertEntities()){
				if(hasIns) break;
				if(ins.getLogicalDelete() != null) {
					if (!ins.getLogicalDelete()) {
						for (UploadInfoEntity u : ins.getUploadInfoEntities()) {
							if (hasIns) break;
							if (u.getLogicalDelete() != null) {
								if (u.getLogicalDelete() == 0) {
									hasIns = true;
								}
							} else {
								hasIns = true;
							}
						}
					}
				}
			}
			this.setHasInsertsWithAEs(hasIns);
		}
		if (volume.getInsertEntities() != null) {
			boolean hasIns = false;
			for(InsertEntity ins: volume.getInsertEntities()){
				if(hasIns) break;
				if(ins.getLogicalDelete() != null) {
					if (!ins.getLogicalDelete()) {
						hasIns = true;
					}
				}
			}
			this.setHasInserts(hasIns);
		}

		return this;
	}

	public Volume toVolumeEntity() {
		Volume volume = new Volume();
		volume.setSummaryId(Integer.valueOf(this.getVolumeId()));
		volume.setVolume(this.getVolumeName());

		if (this.getCollectionId() != null) {
			CollectionEntity collectionEntity = new CollectionEntity();
			collectionEntity.setCollectionId(Integer.valueOf(this
					.getCollectionId()));
			collectionEntity.setCollectionName(this.getCollectionName());
			collectionEntity
					.setCollectionAbbreviation(this.getCollectionAbbr());
			volume.setCollectionEntity(collectionEntity);

		}

		volume.setSezione(this.getSezione());
		volume.setVolume(this.getVolumeNo());
		volume.setTitle(this.getVolumeTitle());
		volume.setStartYear((this.getStartYear() == null || this.getStartYear().equalsIgnoreCase("")  ) ? null : Integer
				.valueOf(this.getStartYear().toString()));


		volume.setStartDay((this.getStartDay() == null || this.getStartDay().equalsIgnoreCase("")) ? null : Integer
				.valueOf(this.getStartDay().toString()));
		volume.setEndYear((this.getEndYear() == null  || this.getEndYear().equalsIgnoreCase("")) ? null : Integer
				.valueOf(this.getEndYear().toString()));
		

		volume.setEndDay((this.getEndDay() == null || this.getEndDay().equalsIgnoreCase("")) ? null : Integer.valueOf(this
				.getEndDay().toString()));
		volume.setDateNotes(this.getDateNotes());
		volume.setCcontext(this.getVolumeContext());
		volume.setOtherNotes(this.getOtherNotes());

		volume.setOrgNotes(this.getOrganizationalCriteria());
		volume.setFolioCount(this.getFolioCount());
		return volume;
	}

	public Schedone toSchedoneEntity() {
		Schedone schedone = new Schedone();
		schedone.setNoteCartulazioneEng(this.getPaginationNotes());
		schedone.setCartulazione(this.getPagination());
		schedone.setCarteMancanti(this.getMissingFolios());
		return schedone;
	}


	public Boolean getHasInsertsWithAEs() {
		return hasInsertsWithAEs;
	}

	public void setHasInsertsWithAEs(Boolean hasInsertsWithAEs) {
		this.hasInsertsWithAEs = hasInsertsWithAEs;
	}

	public String getCollectionId() {
		return collectionId;
	}

	public void setCollectionId(String collectionId) {
		this.collectionId = collectionId;
	}

	public String getCollectionName() {
		return collectionName;
	}

	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}

	public String getCollectionAbbr() {
		return collectionAbbr;
	}

	public void setCollectionAbbr(String collectionAbbr) {
		this.collectionAbbr = collectionAbbr;
	}

	public String getSezione() {
		return sezione;
	}

	public void setSezione(String sezione) {
		this.sezione = sezione;
	}

	public String getVolumeNo() {
		return volumeNo;
	}

	public void setVolumeNo(String volumeNo) {
		this.volumeNo = volumeNo;
	}

	public String getVolumeTitle() {
		return volumeTitle;
	}

	public void setVolumeTitle(String volumeTitle) {
		this.volumeTitle = volumeTitle;
	}

	public String getStartYear() {
		return startYear;
	}

	public void setStartYear(String startYear) {
		this.startYear = startYear;
	}

	public String getStartMonth() {
		return startMonth;
	}

	public void setStartMonth(String startMonth) {
		this.startMonth = startMonth;
	}

	public String getStartDay() {
		return startDay;
	}

	public void setStartDay(String startDay) {
		this.startDay = startDay;
	}

	public String getEndYear() {
		return endYear;
	}

	public void setEndYear(String endYear) {
		this.endYear = endYear;
	}

	public String getEndMonth() {
		return endMonth;
	}

	public void setEndMonth(String endMonth) {
		this.endMonth = endMonth;
	}

	public String getEndDay() {
		return endDay;
	}

	public void setEndDay(String endDay) {
		this.endDay = endDay;
	}

	public String getDateNotes() {
		return dateNotes;
	}

	public void setDateNotes(String dateNotes) {
		this.dateNotes = dateNotes;
	}

	public String getVolumeContext() {
		return volumeContext;
	}

	public void setVolumeContext(String volumeContext) {
		this.volumeContext = volumeContext;
	}

	public String getOtherNotes() {
		return otherNotes;
	}

	public void setOtherNotes(String otherNotes) {
		this.otherNotes = otherNotes;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

	public String getDateLastUpdate() {
		return dateLastUpdate;
	}

	public void setDateLastUpdate(String dateLastUpdate) {
		this.dateLastUpdate = dateLastUpdate;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public String getDateDeleted() {
		return dateDeleted;
	}

	public void setDateDeleted(String dateDeleted) {
		this.dateDeleted = dateDeleted;
	}

	public String getOrganizationalCriteria() {
		return organizationalCriteria;
	}

	public void setOrganizationalCriteria(String organizationalCriteria) {
		this.organizationalCriteria = organizationalCriteria;
	}

	public String getPaginationNotes() {
		return paginationNotes;
	}

	public void setPaginationNotes(String paginationNotes) {
		this.paginationNotes = paginationNotes;
	}

	public String getPagination() {
		return pagination;
	}

	public void setPagination(String pagination) {
		this.pagination = pagination;
	}

	public String getFolioCount() {
		return folioCount;
	}

	public void setFolioCount(String folioCount) {
		this.folioCount = folioCount;
	}

	public String getMissingFolios() {
		return missingFolios;
	}

	public void setMissingFolios(String missingFolios) {
		this.missingFolios = missingFolios;
	}

	public void setHasInserts(Boolean hasInserts) {
		this.hasInserts = hasInserts;
	}
	
	public Boolean getHasInserts() {
		return hasInserts;
	}
}
