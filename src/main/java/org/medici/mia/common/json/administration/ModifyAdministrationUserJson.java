package org.medici.mia.common.json.administration;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.jboss.logging.Logger;
import org.medici.mia.common.json.BaseUserJson;
import org.medici.mia.domain.User;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public class ModifyAdministrationUserJson extends BaseUserJson {

	private static final long serialVersionUID = 1L;
	private final Logger logger = Logger.getLogger(this.getClass());
	private final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd hh:mm:ss");

	private String organization;
	private String accountExpirationYear;
	private String accountExpirationMonth;
	private String accountExpirationDay;
	private String expirationPasswordDate;
	private Integer approved;
	private Integer active;
	private Integer locked;
	private Integer mailhide;

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getAccountExpirationYear() {
		return accountExpirationYear;
	}

	public void setAccountExpirationYear(String accountExpirationYear) {
		this.accountExpirationYear = accountExpirationYear;
	}

	public String getAccountExpirationMonth() {
		return accountExpirationMonth;
	}

	public void setAccountExpirationMonth(String accountExpirationMonth) {
		this.accountExpirationMonth = accountExpirationMonth;
	}

	public String getAccountExpirationDay() {
		return accountExpirationDay;
	}

	public void setAccountExpirationDay(String accountExpirationDay) {
		this.accountExpirationDay = accountExpirationDay;
	}

	public String getExpirationPasswordDate() {
		return expirationPasswordDate;
	}

	public void setExpirationPasswordDate(String expirationPasswordDate) {
		this.expirationPasswordDate = expirationPasswordDate;
	}

	public Integer getApproved() {
		return approved;
	}

	public void setApproved(Integer approved) {
		this.approved = approved;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getLocked() {
		return locked;
	}

	public void setLocked(Integer locked) {
		this.locked = locked;
	}

	public Integer getMailhide() {
		return mailhide;
	}

	public void setMailhide(Integer mailhide) {
		this.mailhide = mailhide;
	}

	public User toEntity(User user) {

		user = super.toEntity(user);
		user.setOrganization(this.getOrganization());
		user.setMail(this.getEmail());
		

		try {
			// Set expiration date

			String expDateInSDF = this.getAccountExpirationYear() + "-"
					+ this.getAccountExpirationMonth() + "-"
					+ this.getAccountExpirationDay() + " 00:00:00";
			Date expDate = sdf.parse(expDateInSDF);
			user.setExpirationDate(expDate);

			// Set expiration password date
			String expPasswordDateInSDF = this.getExpirationPasswordDate();
			Date expPasswordDate = sdf.parse(expPasswordDateInSDF);
			user.setExpirationPasswordDate(expPasswordDate);

		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
		
		if (this.getApproved() != null && this.getApproved().equals(1)) {
			user.setApproved(Boolean.TRUE);
		} else {
			user.setApproved(Boolean.FALSE);
		}
		if (this.getActive() != null && this.getActive().equals(1)) {
			user.setActive(Boolean.TRUE);
		} else {
			user.setActive(Boolean.FALSE);
		}
		if (this.getLocked() != null && this.getLocked().equals(1)) {
			user.setLocked(Boolean.TRUE);
		} else {
			user.setLocked(Boolean.FALSE);
		}

		if (this.getMailhide() != null && this.getMailhide().equals(1)) {
			user.setMailHide(Boolean.TRUE);
		} else {
			user.setMailHide(Boolean.FALSE);
		}

		user.setMiddleName(" ");

		return user;
	}


}
