package org.medici.mia.common.json.historylog;

import java.text.SimpleDateFormat;
import java.io.Serializable;

import org.medici.mia.domain.HistoryLogEntity;

public class VolumeInsertJson implements Serializable {

	private static final long serialVersionUID = 1L;
	private final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd hh:mm:ss");
	private String type;
	private Integer id;
	private String date;

	public VolumeInsertJson toJson(HistoryLogEntity entity) {
		if (entity != null) {
			this.setId(entity.getEntryId());
			if (entity.getRecordType() != null) {
				this.setType(entity.getRecordType().toString());
			}
			if (entity.getDateAndTime() != null) {
				this.setDate(entity.getDateAndTime() == null ? null : sdf.format(entity.getDateAndTime()));
			}

		}

		return this;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}