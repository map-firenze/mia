package org.medici.mia.common.json.advancedsearch;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.util.DateUtils;
import org.medici.mia.domain.User;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@JsonTypeName("categoryAndTypologyAdvancedSearch")
public class CategoryAndTypologyAdvancedSearchJson extends
		GenericAdvancedSearchJson {

	private static final long serialVersionUID = 1L;

	@JsonProperty("category")
	private String category;

	@JsonProperty("typology")
	private String typology;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getTypology() {
		return typology;
	}

	public void setTypology(String typology) {
		this.typology = typology;
	}

	@JsonIgnore
	@Override
	public List<String> getQueries() {
		// example query:
		// select distinct documentEntityId documentId from tblDocumentEnts
		// WHERE category = 'correspondence' and typology = 'Minuta'

		List<String> queries = new ArrayList<String>();
		StringBuffer b = new StringBuffer();
		b.append("select distinct documentEntityId documentId from tblDocumentEnts WHERE category = '");
		b.append(this.getCategory());
		b.append("'");
		b.append(" and flgLogicalDelete != 1 ");
		
		if (this.getTypology() != null && !this.getTypology().isEmpty()) {
			b.append(" and typology = '");
			b.append(this.getTypology());
			b.append("'");	
		}

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((dateCreated > '%s' AND createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (dateLastUpdate > '%s' AND LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}
		
		queries.add(b.toString());
		return queries;
	}
}