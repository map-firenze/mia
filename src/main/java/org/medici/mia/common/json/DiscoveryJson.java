package org.medici.mia.common.json;

import java.util.List;

public class DiscoveryJson {

	private Integer id;
	private String title;
	private String whyHistImp;
	private String shortNotice;
	private String bibliography;
	private Boolean logicalDelete;
	private String status;
	private Integer documentId;
	private String submissionDate;
	private String owner;
	private List<Integer> links;
	private List<DiscoveryUploadJson> uploads;
	private String imagePath;
	private String transcriptions;
	private String synopsis;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWhyHistImp() {
		return whyHistImp;
	}

	public void setWhyHistImp(String whyHistImp) {
		this.whyHistImp = whyHistImp;
	}

	public String getShortNotice() {
		return shortNotice;
	}

	public void setShortNotice(String shortNotice) {
		this.shortNotice = shortNotice;
	}

	public String getBibliography() {
		return bibliography;
	}

	public void setBibliography(String bibliography) {
		this.bibliography = bibliography;
	}

	public Boolean getLogicalDelete() {
		return logicalDelete;
	}

	public void setLogicalDelete(Boolean logicalDelete) {
		this.logicalDelete = logicalDelete;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public Integer getDocumentId() {
		return documentId;
	}
	
	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}
	
	public String getOwner() {
		return owner;
	}
	
	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getSubmissionDate() {
		return submissionDate;
	}
	
	public void setSubmissionDate(String submissionDate) {
		this.submissionDate = submissionDate;
	}
	
	public List<Integer> getLinks() {
		return links;
	}
	
	public void setLinks(List<Integer> links) {
		this.links = links;
	}
	
	public List<DiscoveryUploadJson> getUploads() {
		return uploads;
	}
	
	public void setUploads(List<DiscoveryUploadJson> uploads) {
		this.uploads = uploads;
	}
	
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	
	public String getImagePath() {
		return imagePath;
	}

	public String getTranscriptions() {
		return transcriptions;
	}

	public void setTranscriptions(String transcriptions) {
		this.transcriptions = transcriptions;
	}

	public String getSynopsis() {
		return synopsis;
	}

	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}
	
}
