/*
 * DocumentTranscriptionJson.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.common.json.document;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.json.LanguageJson;
import org.medici.mia.domain.IGenericEntity;
import org.medici.mia.domain.LanguageEntity;
import org.medici.mia.domain.MiaDocumentEntity;

/**
 * JSON implementation for store document's transcriptions in the
 * tblDocTranscriptions database table.
 * 
 * 
 */
public class DocumentLanguageJson implements IModifyDocJson {

	private static final long serialVersionUID = 3725099586315739138L;

	private String documentId;
	private List<LanguageJson> languages;

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public List<LanguageJson> getLanguages() {
		return languages;
	}

	public void setLanguages(List<LanguageJson> languages) {
		this.languages = languages;
	}

	@Override
	public void toJson(IGenericEntity entity) {
		if (entity == null)
			return;
		if (entity instanceof MiaDocumentEntity) {
			MiaDocumentEntity langEentity = (MiaDocumentEntity) entity;
			this.setDocumentId(String.valueOf(langEentity.getDocumentEntityId()));
			List<LanguageJson> languagesJson = new ArrayList<LanguageJson>();
			if (langEentity.getLanguages() != null
					&& !langEentity.getLanguages().isEmpty()) {
				for (LanguageEntity langEnt : langEentity.getLanguages()) {
					LanguageJson langJson = new LanguageJson();
					langJson.setId(langEnt.getId());
					langJson.setLanguage(langEnt.getLanguage());

					languagesJson.add(langJson);

				}
				this.setLanguages(languagesJson);
			}
		}

	}

	@Override
	public IGenericEntity toEntity(IGenericEntity entity) {

		List<LanguageEntity> languagesEnt = new ArrayList<LanguageEntity>();
		for (LanguageJson langJson : this.getLanguages()) {
			LanguageEntity langEnt = new LanguageEntity();
			langEnt.setId(langJson.getId());
			langEnt.setLanguage(langJson.getLanguage());
			languagesEnt.add(langEnt);

		}
		((MiaDocumentEntity) entity).setLanguages(languagesEnt);
		return entity;

	}

}
