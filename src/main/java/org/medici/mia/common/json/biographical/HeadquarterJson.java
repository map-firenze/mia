package org.medici.mia.common.json.biographical;

import java.io.Serializable;

import org.medici.mia.domain.OrganizationEntity;

public class HeadquarterJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer headquarterId;
	private Integer placeId;

	private Integer year;

	public Integer getPlaceId() {
		return placeId;
	}

	public void setPlaceId(Integer placeId) {
		this.placeId = placeId;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
	
	

	public Integer getHeadquarterId() {
		return headquarterId;
	}

	public void setHeadquarterId(Integer headquarterId) {
		this.headquarterId = headquarterId;
	}

	public void toJson(OrganizationEntity entity) {
		if (entity == null)
			return;
		if (entity instanceof OrganizationEntity) {
            this.setHeadquarterId(entity.getOrganizationId());
			this.setPlaceId(entity.getPlaceAllId());
			this.setYear(entity.getYear());

		}

	}

}