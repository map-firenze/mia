/*
 * AdvancedSearchType.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.common.json.advancedsearch;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public enum AdvancedSearchType {

	PERSON_VITAL_STATISTICS_BADBLBBDO_SEARCH(
			"personVitalStatisticBADBLBBDOSearch"), PERSON_VITAL_STATISTICS_BIRTH_DEATH_SEARCH(
			"personVitalStatisticsBirthDeathSearch"), PERSON_VITAL_STATISTIC_GENDER_SEARCH(
			"personVitalStatisticsGenderSearch"), TITLE_OCCUPATION_SEARCH(
			"titleOccupationSearch"), NAME_SEARCH("namesSearch"), ARCHIVAL_LOCATION_SEARCH(
			"archivalLocationSearch"), CATEGORY_AND_TYPOLOGY_SEARCH(
			"categoryAndTypologySearch"), PEOPLE_SEARCH("peopleSearch"), PEOPLE_CATEGORY_FIELDS_SEARCH(
			"peopleCategoryFieldsSearch"), TRANSCRIPTION_SEARCH(
			"transcriptionSearch"), SYNOPSIS_SEARCH("synopsisSearch"), PLACES_SEARCH(
			"placesSearch"), TOPIC_SEARCH("topicsSearch"), DATA_SEARCH(
			"dateSearch"), DOCUMENT_OWNER("documentOwnerSearch"), LANGUEGES_SEARCH(
			"languagesSearch"), DOCUMENT_ENTITY_ID("documentEntityIdSearch");

	public String value;

	AdvancedSearchType(String value) {

		this.value = value;

	}

	public String getValue() {
		return value;
	}

}
