package org.medici.mia.common.json.folio;

import java.io.Serializable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class FolioResponseJson implements Serializable {

	private static final long serialVersionUID = 1199199301715371481L;

	private Integer folioId;
	private Integer uploadFileId;

	public FolioResponseJson(Integer folioId, Integer uploadFileId) {
		super();
		this.folioId = folioId;
		this.uploadFileId = uploadFileId;
	}

	public Integer getFolioId() {
		return folioId;
	}

	public void setFolioId(Integer folioId) {
		this.folioId = folioId;
	}

	public Integer getUploadFileId() {
		return uploadFileId;
	}

	public void setUploadFileId(Integer uploadFileId) {
		this.uploadFileId = uploadFileId;
	}

}
