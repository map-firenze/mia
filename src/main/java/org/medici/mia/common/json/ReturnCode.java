package org.medici.mia.common.json;


public enum ReturnCode {
	/** GENERIC ERRORS 001 - 010 **/
	OK("0"),
	GENERIC_ERRORR("1"),
	UNKNOWN_ERROR("2"),
	BAD_INPUT_PARAM_ERROR("3"), 
	GENERIC_WARNING("4"),
	PRIVACY_NOT_UPDATED("5");
	
	private final String errorCode;

	private ReturnCode(String value) {
		errorCode = value;
	}

	@Override
	public String toString() {
		return errorCode;
	}
}
