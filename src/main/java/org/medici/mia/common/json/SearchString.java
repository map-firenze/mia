package org.medici.mia.common.json;

import java.io.Serializable;

/**
 * 
 * @author user
 *
 */
public class SearchString implements Serializable {
	
	private static final long serialVersionUID = 2047061955578000556L;
	private String wordSearchString = null;
	private String wordSearchType = null;
	public String getWordSearchString() {
		return wordSearchString;
	}
	public void setWordSearchString(String wordSearchString) {
		this.wordSearchString = wordSearchString;
	}
	public String getWordSearchType() {
		return wordSearchType;
	}
	public void setWordSearchType(String wordSearchType) {
		this.wordSearchType = wordSearchType;
	}
	


}
