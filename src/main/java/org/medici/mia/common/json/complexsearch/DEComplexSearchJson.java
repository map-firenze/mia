package org.medici.mia.common.json.complexsearch;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class DEComplexSearchJson extends ComplexSearchJson {

	private static final long serialVersionUID = 3787731742866353474L;

	private boolean onlyMyEntities;
	private boolean everyoneElsesEntities;
	private boolean allEntities;
	public boolean isOnlyMyEntities() {
		return onlyMyEntities;
	}
	public void setOnlyMyEntities(boolean onlyMyEntities) {
		this.onlyMyEntities = onlyMyEntities;
	}
	public boolean isEveryoneElsesEntities() {
		return everyoneElsesEntities;
	}
	public void setEveryoneElsesEntities(boolean everyoneElsesEntities) {
		this.everyoneElsesEntities = everyoneElsesEntities;
	}
	public boolean isAllEntities() {
		return allEntities;
	}
	public void setAllEntities(boolean allEntities) {
		this.allEntities = allEntities;
	}

	
}
