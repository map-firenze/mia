/*
 * placesSearchJson.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.common.json.advancedsearch;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.util.DateUtils;
import org.medici.mia.domain.User;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@JsonTypeName("dateAdvancedSearch")
public class DataAdvancedSearchJson extends GenericAdvancedSearchJson {

	private static final long serialVersionUID = 1L;
	private static final String NOT_DELETED_DOC = " d.flgLogicalDelete != 1";
	
	private static final String DOCUMENT_MODERN_YEAR = "docModernYear";
	private static final String DOCUMENT_YEAR = "docYear";
	private static final String MODERN_YEAR_CHECK = " AND (d.docModernYear IS NOT NULL AND d.docModernYear <> 0)";
	private static final String NO_MODERN_YEAR_CHECK = " AND d.docYear <> 0 AND (d.docModernYear IS NULL OR d.docModernYear = 0)";
	private static final int DEFAULT_BEFORE_QUERY_VALUE = 0;
	private static final int DEFAULT_FROM_QUERY_VALUE = 0;

	@JsonProperty("dateFilterType")
	private String dateFilterType;

	private Integer dateYear;
	private Integer dateMonth;
	private Integer dateDay;

	private Integer dateBYear;
	private Integer dateBMonth;
	private Integer dateBDay;

	public String getDateFilterType() {
		return dateFilterType;
	}

	public void setDateFilterType(String dateFilterType) {
		this.dateFilterType = dateFilterType;
	}

	public Integer getDateYear() {
		return dateYear;
	}

	public void setDateYear(Integer dateYear) {
		this.dateYear = dateYear;
	}

	public Integer getDateMonth() {
		return dateMonth;
	}

	public void setDateMonth(Integer dateMonth) {
		this.dateMonth = dateMonth;
	}

	public Integer getDateDay() {
		return dateDay;
	}

	public void setDateDay(Integer dateDay) {
		this.dateDay = dateDay;
	}

	public Integer getDateBYear() {
		return dateBYear;
	}

	public void setDateBYear(Integer dateBYear) {
		this.dateBYear = dateBYear;
	}

	public Integer getDateBMonth() {
		return dateBMonth;
	}

	public void setDateBMonth(Integer dateBMonth) {
		this.dateBMonth = dateBMonth;
	}

	public Integer getDateBDay() {
		return dateBDay;
	}

	public void setDateBDay(Integer dateBDay) {
		this.dateBDay = dateBDay;
	}

	@JsonIgnore
	@Override
	public List<String> getQueries() {
		// select * from tblDocumentEnts where docyear BETWEEN '1899' AND '1905'
		// and docMonth BETWEEN '3' and '9' and docDay BETWEEN '10' and '12';
		if (this.getDateYear() == null && this.getDateMonth() == null
				&& this.getDateDay() == null) {
			new ArrayList<String>();
		}
		if (this.getDateFilterType().equalsIgnoreCase(
				DataAdvancedSearchType.BETWEEN.getValue())) {
			return getQueriesBetween();
		} else if (this.getDateFilterType().equalsIgnoreCase(
				DataAdvancedSearchType.IN.getValue())) {
			return getQueriesIn();
		} else if (this.getDateFilterType().equalsIgnoreCase(
				DataAdvancedSearchType.BEFORE.getValue())) {
			return getBeforeQueries();
		} else if (this.getDateFilterType().equalsIgnoreCase(
				DataAdvancedSearchType.FROM.getValue())) {
			return getFromQueries();

		} else {
			return getQueriesIn();
		}

	}

	private List<String> getQueriesBetween() {
        // select * from tblDocumentEnts where docyear BETWEEN '1899' AND '1905'
        // and docMonth BETWEEN '3' and '9' and docDay BETWEEN '10' and '12';
        List<String> queries = new ArrayList<String>();

        String query = String.format("%s UNION %s", createBetweenQuery(false), createBetweenQuery(true));
		queries.add(query);

		return queries;
	}
	
	private List<String> getBeforeQueries() {
		// select * from tblDocumentEnts where docyear < '1890' and docMonth <
		// '2' and docDay < '6' ;
		List<String> queries = new ArrayList<String>();
		String query = String.format("%s UNION %s", createBeforeQuery(false), createBeforeQuery(true));
		queries.add(query);
		
		return queries;
	}
	
	private List<String> getFromQueries() {
		// select * from tblDocumentEnts where docyear > '1890' and docMonth >
		// '2' and docDay > '6' ;
		List<String> queries = new ArrayList<String>();
        String query = String.format("%s UNION %s", createFromQuery(false), createFromQuery(true));
        queries.add(query);

		return queries;
	}
	
	private List<String> getQueriesIn() {
		// select * from tblDocumentEnts where docyear = '1899' and docMonth =
		// '3' and docDay = '10' ;
		List<String> queries = new ArrayList<String>();
		String query = String.format("%s UNION %s", createInQuery(false), createInQuery(true));
		queries.add(query);

		return queries;
	}
	
	private String createBetweenQuery(boolean useModernYear) {
		// select * from tblDocumentEnts where docyear BETWEEN '1899' AND '1905'
        // and docMonth BETWEEN '3' and '9' and docDay BETWEEN '10' and '12';

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT DISTINCT documentEntityId documentId FROM tblDocumentEnts d WHERE ");

        if (isNotValidDate(this.getDateYear(), this.getDateMonth(), this.getDateDay())) {
        	throw new NullPointerException(String.format("Invalid date format: %s-%s-%s", 
        			this.getDateYear(), this.getDateMonth(), this.getDateDay()));
        }
        if (isNotValidDate(this.getDateBYear(), this.getDateBMonth(), this.getDateBDay())) {
        	throw new NullPointerException(String.format("Invalid date format: %s-%s-%s", 
        			this.getDateBYear(), this.getDateBMonth(), this.getDateBDay()));
        }
        	
        if(this.getDateBMonth() == null) {
        	this.setDateBMonth(DEFAULT_BEFORE_QUERY_VALUE);
        }
        if(this.getDateMonth() == null) {
        	this.setDateMonth(DEFAULT_FROM_QUERY_VALUE);
        }
        
        if(this.getDateBDay() == null) {
        	this.setDateBDay(DEFAULT_BEFORE_QUERY_VALUE);
        }
        if(this.getDateDay() == null) {
        	this.setDateDay(DEFAULT_FROM_QUERY_VALUE);
        }

        String lowDate = String.format("%0" + 4 + "d", this.getDateBYear()) + "-"
                + String.format("%0" + 2 + "d", this.getDateBMonth()) + "-"
                + String.format("%0" + 2 + "d", this.getDateBDay());
        String highDate = String.format("%0" + 4 + "d", this.getDateYear()) + "-"
                + String.format("%0" + 2 + "d", this.getDateMonth()) + "-"
                + String.format("%0" + 2 + "d", this.getDateDay());
        
        if (this.getDateYear() < this.getDateBYear()
            || this.getDateBYear().equals(this.getDateYear()) && this.getDateBMonth() > this.getDateMonth()
            || this.getDateBYear().equals(this.getDateYear()) && this.getDateBMonth().equals(this.getDateMonth()) && this.getDateBDay() > this.getDateDay()){
            String buffer = lowDate;
            lowDate = highDate;
            highDate = buffer;
        }
        
        String yearField = getYearField(useModernYear);
        String yearCheck = getYearCheck(useModernYear);
        
        stringBuilder.append(String.format("STR_TO_DATE(CONCAT(right(10000+IFNULL(d.%s,0), 4), '-', ", yearField));
        stringBuilder.append("right(100+IFNULL(d.docMonth,0), 2), '-', right(100+IFNULL(docDay,0), 2)), '%Y-%m-%d') >= STR_TO_DATE('");
        stringBuilder.append(lowDate);
        stringBuilder.append("','%Y-%m-%d') AND ");
        stringBuilder.append(String.format("STR_TO_DATE(CONCAT(right(10000+IFNULL(d.%s,0), 4), '-', ", yearField));
        stringBuilder.append("right(100+IFNULL(d.docMonth,0), 2), '-', right(100+IFNULL(docDay,0), 2)), '%Y-%m-%d') < STR_TO_DATE('");
        stringBuilder.append(highDate);
        stringBuilder.append("','%Y-%m-%d')");
        stringBuilder.append(" and d.docUndated != 1");
		stringBuilder.append(" and " + NOT_DELETED_DOC);
		stringBuilder.append(yearCheck);

		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			stringBuilder.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			stringBuilder.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}
		
		return stringBuilder.toString();
	}
	
	private String createBeforeQuery(boolean useModernYear) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("SELECT DISTINCT documentEntityId documentId FROM tblDocumentEnts d WHERE ");

		if (isNotValidDate(this.getDateYear(), this.getDateMonth(), this.getDateDay())) {
        	throw new NullPointerException(String.format("Invalid date format: %s-%s-%s", 
        			this.getDateYear(), this.getDateMonth(), this.getDateDay()));
        }
		
		if(this.getDateMonth() == null){
			this.setDateMonth(DEFAULT_BEFORE_QUERY_VALUE);
		}
		
		if(this.getDateDay() == null){
			this.setDateDay(DEFAULT_BEFORE_QUERY_VALUE);
		}

		if (this.getDateDay() == null && this.getDateMonth() == null) {
			this.setDateYear(this.getDateYear() - 1);
		}
		
        String searchDate = String.format("%0" + 4 + "d", this.getDateYear()) + "-"
                + String.format("%0" + 2 + "d", this.getDateMonth()) + "-"
                + String.format("%0" + 2 + "d", this.getDateDay());
        
        String yearField = getYearField(useModernYear);
        String yearCheck = getYearCheck(useModernYear);
        
		stringBuilder.append(String.format("STR_TO_DATE(CONCAT(right(10000+IFNULL(d.%s,0), 4), '-', ", yearField));
		stringBuilder.append("right(100+IFNULL(d.docMonth,0), 2), '-', right(100+IFNULL(docDay,0), 2)), '%Y-%m-%d') < STR_TO_DATE('");
		stringBuilder.append(searchDate);
		stringBuilder.append("','%Y-%m-%d')");
        stringBuilder.append(" and d.docUndated != 1");
		stringBuilder.append(" and " + NOT_DELETED_DOC);
		stringBuilder.append(yearCheck);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			stringBuilder.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			stringBuilder.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}
		
		return stringBuilder.toString();
	}
	
	private String createFromQuery(boolean useModernYear) {
		StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT DISTINCT documentEntityId documentId FROM tblDocumentEnts d WHERE ");

        if (isNotValidDate(this.getDateYear(), this.getDateMonth(), this.getDateDay())) {
        	throw new NullPointerException(String.format("Invalid date format: %s-%s-%s", 
        			this.getDateYear(), this.getDateMonth(), this.getDateDay()));
        }
		
		if(this.getDateMonth() == null){
			this.setDateMonth(DEFAULT_FROM_QUERY_VALUE);
		}
		
		if(this.getDateDay() == null){
			this.setDateDay(DEFAULT_FROM_QUERY_VALUE);
		}

        String searchDate = String.format("%0" + 4 + "d", this.getDateYear()) + "-"
                + String.format("%0" + 2 + "d", this.getDateMonth()) + "-"
                + String.format("%0" + 2 + "d", this.getDateDay());
        
        String yearField = getYearField(useModernYear);
        String yearCheck = getYearCheck(useModernYear);
        
        stringBuilder.append(String.format("STR_TO_DATE(CONCAT(right(10000+IFNULL(d.%s,0), 4), '-', ", yearField));
        stringBuilder.append("right(100+IFNULL(d.docMonth,0), 2), '-', right(100+IFNULL(docDay,0), 2)), '%Y-%m-%d') >= STR_TO_DATE('");
        stringBuilder.append(searchDate);
        stringBuilder.append("','%Y-%m-%d')");
        stringBuilder.append(" and d.docUndated != 1");
		stringBuilder.append(" AND " + NOT_DELETED_DOC);
		stringBuilder.append(yearCheck);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			stringBuilder.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			stringBuilder.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		return stringBuilder.toString();
	}

	private String createInQuery(boolean useModernYear) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("select distinct documentEntityId documentId from tblDocumentEnts d where ");

		String yearField = getYearField(useModernYear);
		String yearCheck = getYearCheck(useModernYear); 
		
		if (this.getDateYear() != null) {
			stringBuilder.append(String.format(" d.%s = ", yearField)); 
			stringBuilder.append(this.getDateYear());
			stringBuilder.append(yearCheck);
		} else {
			stringBuilder.append(" d.docyear >= 0");
		}

		stringBuilder.append(" AND ");

		if (this.getDateMonth() != null) {
			stringBuilder.append(" d.docMonth = ");
			stringBuilder.append(this.getDateMonth());
		} else {
			stringBuilder.append(" d.docMonth >= 0 "); 
		}

		stringBuilder.append(" AND ");

		if (this.getDateDay() != null) {
			stringBuilder.append(" d.docDay = ");
			stringBuilder.append(this.getDateDay());
		} else {
			stringBuilder.append(" d.docDay >= 0");
		}

		stringBuilder.append(" and " + NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			stringBuilder.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			stringBuilder.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}
		
		return stringBuilder.toString();
	}
	
	private String getYearField(boolean useModernYear) {
		if (useModernYear) {
			return DOCUMENT_MODERN_YEAR;
		}
		
		return DOCUMENT_YEAR;
	}
	
	private String getYearCheck(boolean useModernYear) {
		if (useModernYear) {
			return MODERN_YEAR_CHECK;
		}
		
		return NO_MODERN_YEAR_CHECK;
	}
	
	private boolean isNotValidDate(Integer year, Integer month, Integer day) {
		if (year == null) {
        	return true;
        }
		
		if (day != null && (year == null || month == null)) {
			return true;
		}
		
		return false;
	}

}
