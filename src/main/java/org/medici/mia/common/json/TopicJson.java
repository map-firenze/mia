package org.medici.mia.common.json;

import java.io.Serializable;

public class TopicJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer topicId;
	private String topicTitle;
	private String description;

	public TopicJson() {
	}

	public Integer getTopicId() {
		return topicId;
	}

	public void setTopicId(Integer topicId) {
		this.topicId = topicId;
	}

	public String getTopicTitle() {
		return topicTitle;
	}

	public void setTopicTitle(String topicTitle) {
		this.topicTitle = topicTitle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
