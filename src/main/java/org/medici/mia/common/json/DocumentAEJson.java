package org.medici.mia.common.json;

import java.io.Serializable;
import java.util.List;

public class DocumentAEJson implements Serializable {

	private static final long serialVersionUID = 1L;
	private String isOwner;
	private List<Integer> documentIds;

	public String getIsOwner() {
		return isOwner;
	}

	public void setIsOwner(String isOwner) {
		this.isOwner = isOwner;
	}

	public List<Integer> getDocumentIds() {
		return documentIds;
	}

	public void setDocumentIds(List<Integer> documentIds) {
		this.documentIds = documentIds;
	}

}