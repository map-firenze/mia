package org.medici.mia.common.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.medici.mia.domain.User;
import org.medici.mia.domain.UserRole;

public class RotateImageJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Integer> uploadFileIds;
	private String rotateType;

	public List<Integer> getUploadFileIds() {
		return uploadFileIds;
	}

	public void setUploadFileIds(List<Integer> uploadFileIds) {
		this.uploadFileIds = uploadFileIds;
	}

	public String getRotateType() {
		return rotateType;
	}

	public void setRotateType(String rotateType) {
		this.rotateType = rotateType;
	}

}
