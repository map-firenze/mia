package org.medici.mia.common.json.advancedsearch;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.util.DateUtils;
import org.medici.mia.domain.User;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@JsonTypeName("languagesAdvancedSearch")
public class LanguageSearchJson extends GenericAdvancedSearchJson {

	private static final long serialVersionUID = 1L;

	List<String> languages;

	public List<String> getLanguages() {
		return languages;
	}

	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}

	@JsonIgnore
	@Override
	public List<String> getQueries() {

		List<String> queries = new ArrayList<String>();
		if (getLanguages() != null && !getLanguages().isEmpty()) {
			for (String language : getLanguages()) {
				StringBuffer b = new StringBuffer();
				b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblLanguages l, tblDocumentEntsLanguages dl where dl.langId = l.id and d.documentEntityId = dl.docId and l.language = '");
				b.append(language);
				b.append("'");
				b.append(" and  d.flgLogicalDelete != 1");
				
				if (getNewsfeedUser() != null) {
					User newsfeedUser = getNewsfeedUser();
					b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
							DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
					b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
							DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
				}

				queries.add(b.toString());

			}

		}
		return queries;

	}
}