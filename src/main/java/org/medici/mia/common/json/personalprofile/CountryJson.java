package org.medici.mia.common.json.personalprofile;

import java.io.File;
import java.io.Serializable;

import org.apache.log4j.Logger;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.domain.Country;
import org.medici.mia.exception.ApplicationThrowable;

public class CountryJson implements Serializable {
	
	private final Logger logger = Logger.getLogger(this.getClass());

	private static final long serialVersionUID = 1L;

	private String name;

	private String code;

	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public CountryJson toJson(Country country) {

		this.setCode(country.getCode());
		this.setName(country.getName());		

		return this;
	}

	public boolean isUserPortrait(String portraitImageName) throws Exception {
		try {
			File imageFile = new File(
					ApplicationPropertyManager
							.getApplicationProperty("portrait.user.path")
							+ "/" + portraitImageName);
			if (!imageFile.exists()) {
				return false;
			}
			return true;
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}

	}

}