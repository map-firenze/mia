package org.medici.mia.common.json.advancedsearch;

import java.io.Serializable;
import java.util.List;

import org.medici.mia.domain.User;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
		@JsonSubTypes.Type(value = ArchivalLocationAdvancedSearchJson.class, name = "archivalLocationAdvancedSearch"),
		@JsonSubTypes.Type(value = CategoryAndTypologyAdvancedSearchJson.class, name = "categoryAndTypologyAdvancedSearch"),
		@JsonSubTypes.Type(value = TranscriptionAdvancedSearchJson.class, name = "transcriptionAdvancedSearch"),
		@JsonSubTypes.Type(value = SynopsisAdvancedSearchJson.class, name = "synopsisAdvancedSearch"),
		@JsonSubTypes.Type(value = PeopleAdvancedSearchJson.class, name = "peopleAdvancedSearch"),
		@JsonSubTypes.Type(value = PlacesAdvancedSearchJson.class, name = "placesAdvancedSearch"),
		@JsonSubTypes.Type(value = DocumentEntityIdAdvancedSearchJson.class, name = "documentEntityIdAdvancedSearch"),
		@JsonSubTypes.Type(value = DataAdvancedSearchJson.class, name = "dateAdvancedSearch"),
		@JsonSubTypes.Type(value = DocumentOwnerAdvancedSearchJson.class, name = "documentOwnerAdvancedSearch"),
		@JsonSubTypes.Type(value = LanguageSearchJson.class, name = "languagesAdvancedSearch"),
		@JsonSubTypes.Type(value = TopicsAdvancedSearchJson.class, name = "topicsAdvancedSearch"),
		@JsonSubTypes.Type(value = WordAdvancedSearchJson.class, name = "wordAdvancedSearch"),
		@JsonSubTypes.Type(value = HeadquartersAdvancedSearchJson.class, name = "headquartersAdvancedSearch"),
		@JsonSubTypes.Type(value = TitleOccupationsAdvancedSearchJson.class, name = "titleOccupationAdvancedSearch"),
		@JsonSubTypes.Type(value = GenderAdvancedSearchJson.class, name = "genderAdvancedSearch"),
		@JsonSubTypes.Type(value = PersonVitalStatisticsBirthDeathAdvancedSearchJson.class, name = "personVitalStatisticsBirthDeathAdvancedSearch"),
		@JsonSubTypes.Type(value = PersonVitalStatisticBADBLBBDOAdvancedSearchJson.class, name = "personVitalStatisticBADBLBBDOAdvancedSearch"),
		@JsonSubTypes.Type(value = PeopleCategoryFieldsAdvancedSearchJson.class, name = "peopleCategoryFieldsAdvancedSearch")})
public class GenericAdvancedSearchJson implements IAdvancedSearchJson,
		Serializable {

	private static final long serialVersionUID = 1L;

	private String searchSection;

	@JsonProperty("isActiveFilter")
	private boolean isActiveFilter;
	
	@JsonIgnore
	private User newsfeedUser;

	public String getSearchSection() {
		return searchSection;
	}

	public void setSearchSection(String searchSection) {
		this.searchSection = searchSection;
	}

	public boolean isActiveFilter() {
		return isActiveFilter;
	}

	public void setActiveFilter(boolean isActiveFilter) {
		this.isActiveFilter = isActiveFilter;
	}
	
	public User getNewsfeedUser() {
		return newsfeedUser;
	}

	public void setNewsfeedUser(User newsfeedUser) {
		this.newsfeedUser = newsfeedUser;
	}

	@JsonIgnore
	@Override
	public List<String> getQueries() {
		// TODO Auto-generated method stub
		return null;
	}

}
