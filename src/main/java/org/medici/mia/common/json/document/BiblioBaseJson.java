package org.medici.mia.common.json.document;

import java.io.Serializable;

public class BiblioBaseJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer biblioId;
	private String name;
	private String link;

	public BiblioBaseJson() {

	}

	public Integer getBiblioId() {
		return biblioId;
	}

	public void setBiblioId(Integer biblioId) {
		this.biblioId = biblioId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

}
