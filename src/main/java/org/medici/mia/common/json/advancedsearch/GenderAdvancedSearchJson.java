package org.medici.mia.common.json.advancedsearch;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.search.AdvancedSearchAbstract.Gender;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@JsonTypeName("genderAdvancedSearch")
public class GenderAdvancedSearchJson extends GenericAdvancedSearchJson {

	private static final long serialVersionUID = 1L;
	private static final String NOT_DELETED_PEOPLE = " AND  p.LOGICALDELETE = 0";

	String gender;
	Integer headquarter;
	private String headquarterName;

	@JsonIgnore
	@Override
	public List<String> getQueries() {

		List<String> queries = new ArrayList<String>();
		StringBuffer b = new StringBuffer();
		if (Gender.M.toString().equalsIgnoreCase(getGender())
				|| Gender.F.toString().equalsIgnoreCase(getGender())) {
			b.append("select distinct p.PERSONID from tblPeople p where p.GENDER = '");
			b.append(getGender() + "'");
			b.append(NOT_DELETED_PEOPLE);
		} else if (Gender.X.toString().equalsIgnoreCase(getGender())) {
			if(getHeadquarter()==null || getHeadquarter().equals("")){
				b.append("select distinct p.PERSONID from tblPeople p where p.GENDER = 'X' ");
			}else{
				b.append("select distinct p.PERSONID from tblPeople p, tblOrganizationHeadquarters o where o.personId = p.PERSONID and p.GENDER = 'X' and o.headquarters = ");
				b.append(getHeadquarter());
			}		
			
			b.append(NOT_DELETED_PEOPLE);
		}

		queries.add(b.toString());
		return queries;

	}
	
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getHeadquarter() {
		return headquarter;
	}

	public void setHeadquarter(Integer headquarter) {
		this.headquarter = headquarter;
	}

	String getHeadquarterName() {
		return headquarterName;
	}

	void setHeadquarterName(String headquarterName) {
		this.headquarterName = headquarterName;
	}
}