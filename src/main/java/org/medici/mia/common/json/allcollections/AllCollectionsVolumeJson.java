package org.medici.mia.common.json.allcollections;

import java.io.Serializable;
import java.util.List;

import org.medici.mia.common.json.GenericResponseJson;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class AllCollectionsVolumeJson extends GenericResponseJson implements
		Serializable {

	private static final long serialVersionUID = 1L;

	private List<AllCollectionsThumbsFileJson> thumbsFiles;
	private Integer colectionId;
	private Integer repositoryId;
	private Integer volumeId;
	private String volumeNo;
	private Integer insertId;
	private String insertNo;
	private String collectionName;
	private Integer count;
	private Integer currentPage;

	public List<AllCollectionsThumbsFileJson> getThumbsFiles() {
		return thumbsFiles;
	}

	public void setThumbsFiles(List<AllCollectionsThumbsFileJson> thumbsFiles) {
		this.thumbsFiles = thumbsFiles;
	}

	public Integer getColectionId() {
		return colectionId;
	}

	public void setColectionId(Integer colectionId) {
		this.colectionId = colectionId;
	}

	public Integer getRepositoryId() {
		return repositoryId;
	}

	public void setRepositoryId(Integer repositoryId) {
		this.repositoryId = repositoryId;
	}

	public String getVolumeNo() {
		return volumeNo;
	}

	public void setVolumeNo(String volumeNo) {
		this.volumeNo = volumeNo;
	}

	public String getInsertNo() {
		return insertNo;
	}

	public void setInsertNo(String insertNo) {
		this.insertNo = insertNo;
	}

	public String getCollectionName() {
		return collectionName;
	}

	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}

	public Integer getVolumeId() {
		return volumeId;
	}

	public void setVolumeId(Integer volumeId) {
		this.volumeId = volumeId;
	}

	public Integer getInsertId() {
		return insertId;
	}

	public void setInsertId(Integer insertId) {
		this.insertId = insertId;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
	
}
