package org.medici.mia.common.json;

import java.io.Serializable;

/**
 * 
 * @author User
 *
 */
public class GenericResponseJson implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String status;

	private String message;

	public GenericResponseJson() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GenericResponseJson(String status, String message) {
		super();
		this.status = status;
		this.message = message;

	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}