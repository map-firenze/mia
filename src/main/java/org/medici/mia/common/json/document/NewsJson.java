package org.medici.mia.common.json.document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.medici.mia.common.json.FieldJson;
import org.medici.mia.common.json.PeopleBaseJson;
import org.medici.mia.common.json.PeopleJson;
import org.medici.mia.common.json.PlaceJson;
import org.medici.mia.domain.DocumentFieldEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.NewsEntity;
import org.medici.mia.domain.People;
import org.medici.mia.domain.Place;
import org.medici.mia.service.miadoc.DocumentType;
import org.medici.mia.service.miadoc.MiaDocumentUtils;
import org.medici.mia.service.miadoc.PeopleFieldType;
import org.medici.mia.service.miadoc.PlaceFieldType;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@JsonTypeName("news")
public class NewsJson extends GenericDocumentJson {

	private static final long serialVersionUID = 1L;

	@JsonProperty("newsFrom")
	private List<PlaceJson> newsFroms;

	@JsonProperty("printer")
	private List<PeopleJson> printers;

	@JsonProperty("printerPlace")
	private List<PlaceJson> printerPlaces;

	public NewsJson() {
	}

	public List<PlaceJson> getNewsFroms() {
		return newsFroms;
	}

	public void setNewsFroms(List<PlaceJson> newsFroms) {
		this.newsFroms = newsFroms;
	}

	public List<PeopleJson> getPrinters() {
		return printers;
	}

	public void setPrinters(List<PeopleJson> printers) {
		this.printers = printers;
	}

	public List<PlaceJson> getPrinterPlaces() {
		return printerPlaces;
	}

	public void setPrinterPlaces(List<PlaceJson> printerPlaces) {
		this.printerPlaces = printerPlaces;
	}

	@Override
	public void init(MiaDocumentEntity docEntity) {
		super.init(docEntity);
		if (docEntity.getNews() != null) {
			this.newsFroms = getPrinterPlaces(docEntity.getNews()
					.getNewsFroms());
			this.printers = getPrinters(docEntity.getNews().getPrinters());
			this.printerPlaces = getPrinterPlaces(docEntity.getNews()
					.getPrinterPlaces());
		}
	}

	@Override
	public List<FindPeopleForDocJson> findPeopleJson(MiaDocumentEntity docEntity) {
		List<FindPeopleForDocJson> modifyPeoples = new ArrayList<FindPeopleForDocJson>();

		FindPeopleForDocJson modifyPeople = new FindPeopleForDocJson();
		modifyPeople.setType(PeopleFieldType.Printer.toString());
		ArrayList<PeopleBaseJson> peopleBases = null;

		if (docEntity.getNews() != null
				&& docEntity.getNews().getPrinter() != null
				&& !docEntity.getNews().getPrinter().isEmpty()) {

			peopleBases = new ArrayList<PeopleBaseJson>();

			for (String peopleIdStr : (List<String>) Arrays.asList(docEntity
					.getNews().getPrinter().split(","))) {

				PeopleBaseJson peopleBase = new PeopleBaseJson();
				String[] peoplesIdArr = peopleIdStr.split(":");
				if (peoplesIdArr[0] != null || !peoplesIdArr[0].isEmpty()) {
					peopleBase.setId(Integer.valueOf(peoplesIdArr[0]));
				}
				if (peoplesIdArr.length > 1) {
					peopleBase.setUnsure(peoplesIdArr[1]);
				}

				peopleBases.add(peopleBase);
			}
		}
		modifyPeople.setPeoples(peopleBases);
		modifyPeoples.add(modifyPeople);

		return modifyPeoples;
	}

	// Used for finding place attached to document
	@Override
	public List<FindPlaceForDocJson> findPlaceJson(MiaDocumentEntity docEntity) {
		List<FindPlaceForDocJson> modifyPlaces = new ArrayList<FindPlaceForDocJson>();

		FindPlaceForDocJson modifyNewsPlace = new FindPlaceForDocJson();
		modifyNewsPlace.setType(PlaceFieldType.newsFrom.toString());

		if (docEntity.getNews() != null) {
			modifyNewsPlace.setPlaces(MiaDocumentUtils.getPlaceBases(docEntity
					.getNews().getNewsFrom()));
		}
		modifyPlaces.add(modifyNewsPlace);

		FindPlaceForDocJson modifyPrinterPlace = new FindPlaceForDocJson();
		modifyPrinterPlace.setType(PlaceFieldType.printerPlace.toString());

		if (docEntity.getNews() != null) {
			modifyPrinterPlace.setPlaces(MiaDocumentUtils
					.getPlaceBases(docEntity.getNews().getPrinterPlace()));
		}
		modifyPlaces.add(modifyPrinterPlace);

		return modifyPlaces;
	}

	// Used for modify - add - delete people attached to document
	@Override
	public MiaDocumentEntity getModifiedDocumentEnt(MiaDocumentEntity docEnt,
			ModifyPeopleForDocJson modifyPeople) {

		if (modifyPeople.getType() == null) {
			docEnt.setErrorMsg("The type has no value)");
			return docEnt;
		}

		if (docEnt.getNews() == null) {
			docEnt.setErrorMsg("There is no " + docEnt.getCategory()
					+ " record associated with the document.");
			return docEnt;
		}

		if (PeopleFieldType.Printer.toString().equalsIgnoreCase(
				modifyPeople.getType())) {

			String modifedStr = MiaDocumentUtils.modifyPeopleForDoc(docEnt
					.getNews().getPrinter(), modifyPeople);
			docEnt.getNews().setPrinter(modifedStr);

		} else {
			docEnt.setErrorMsg("The type " + modifyPeople.getType()
					+ " is not compatible with the category "
					+ docEnt.getCategory());
		}

		return docEnt;

	}

	// Used for modify - add - delete people attached to document
	@Override
	public MiaDocumentEntity getModifiedDocumentEnt(MiaDocumentEntity docEnt,
			ModifyPlaceForDocJson modifyPlace) {

		if (modifyPlace.getType() == null) {
			docEnt.setErrorMsg("The type has no value)");
			return docEnt;
		}

		if (docEnt.getNews() == null) {
			docEnt.setErrorMsg("There is no " + docEnt.getCategory()
					+ " record associated with the document.");
			return docEnt;
		}

		if (PlaceFieldType.printerPlace.toString().equalsIgnoreCase(
				modifyPlace.getType())) {

			String modifedStr = MiaDocumentUtils.modifyPlaceForDoc(docEnt
					.getNews().getPrinterPlace(), modifyPlace);
			docEnt.getNews().setPrinterPlace(modifedStr);

		} else if (PlaceFieldType.newsFrom.toString().equalsIgnoreCase(
				modifyPlace.getType())) {

			String modifedStr = MiaDocumentUtils.modifyPlaceForDoc(docEnt
					.getNews().getNewsFrom(), modifyPlace);
			docEnt.getNews().setNewsFrom(modifedStr);

		} else {
			docEnt.setErrorMsg("The type " + modifyPlace.getType()
					+ " is not compatible with the category "
					+ docEnt.getCategory());
		}

		return docEnt;

	}

	@Override
	public MiaDocumentEntity getMiaDocumentEntity(DocumentJson docJson) {

		MiaDocumentEntity docEnt = super.getMiaDocumentEntity(docJson);

		docEnt.setCategory(DocumentType.news.toString());
		NewsJson newJson = (NewsJson) docJson;
		NewsEntity newEnt = new NewsEntity();

		if (newJson.getNewsFroms() != null && !newJson.getNewsFroms().isEmpty()) {
			String newsFromStr = "";
			for (PlaceJson place : newJson.getNewsFroms()) {
				newsFromStr = newsFromStr + place.getId() + ":";
				if (!StringUtils.isEmpty(place.getUnsure())) {
					newsFromStr = newsFromStr + place.getUnsure();
				} else {
					newsFromStr = newsFromStr + "S";
				}
				newsFromStr = newsFromStr + ",";
			}
			newEnt.setNewsFrom(newsFromStr);
		}

		if (newJson.getPrinters() != null && !newJson.getPrinters().isEmpty()) {
			String printerStr = "";
			for (PeopleJson people : newJson.getPrinters()) {
				printerStr = printerStr + people.getId() + ":";
				if (!StringUtils.isEmpty(people.getUnsure())) {
					printerStr = printerStr + people.getUnsure();
				} else {
					printerStr = printerStr + "S";
				}
				printerStr = printerStr + ",";
			}
			newEnt.setPrinter(printerStr);
		}

		if (newJson.getPrinterPlaces() != null
				&& !newJson.getPrinterPlaces().isEmpty()) {
			String printerPlaceStr = "";
			for (PlaceJson place : newJson.getPrinterPlaces()) {
				printerPlaceStr = printerPlaceStr + place.getId() + ":";
				if (!StringUtils.isEmpty(place.getUnsure())) {
					printerPlaceStr = printerPlaceStr + place.getUnsure();
				} else {
					printerPlaceStr = printerPlaceStr + "S";
				}
				printerPlaceStr = printerPlaceStr + ",";
			}
			newEnt.setPrinterPlace(printerPlaceStr);
		}
		docEnt.setNews(newEnt);

		return docEnt;
	}

	@Override
	public List<FieldJson> getJsonFields(
			List<DocumentFieldEntity> documentFields) {

		List<FieldJson> fields = new ArrayList<FieldJson>();
		for (DocumentFieldEntity field : documentFields) {
			if (DocumentType.news.toString().equalsIgnoreCase(
					field.getDocumentCategory())) {
				boolean isMultiple = false;
				if ("1".equalsIgnoreCase(field.getAllowMultiple())) {
					isMultiple = true;
				}
				FieldJson fieldJson = new FieldJson(field.getFieldName(),
						field.getFieldBeName(), field.getFieldValue(),
						field.getFieldType(), isMultiple);

				fields.add(fieldJson);

			}
		}

		return fields;
	}

	private ArrayList<PeopleJson> getPrinters(List<People> peoples) {

		ArrayList<PeopleJson> peopleJsons = new ArrayList<PeopleJson>();

		if (peoples != null && !peoples.isEmpty()) {
			for (People people : peoples) {

				PeopleJson peopleJson = new PeopleJson();
				peopleJson.setId(people.getPersonId());
				peopleJson.setGender(people.getGender());
				peopleJson.setMapNameLf(people.getMapNameLf());
				peopleJson.setActiveStart(people.getActiveStart());
				peopleJson.setActiveEnd(people.getActiveEnd());
				peopleJson.setBornYear(people.getBornYear());
				peopleJson.setDeathYear(people.getDeathYear());
				peopleJson.setFirstName(people.getFirst());
				peopleJson.setLastName(people.getLast());
				peopleJson.setUnsure(people.getUnsure());
				peopleJsons.add(peopleJson);
			}
		}

		return peopleJsons;
	}

	private ArrayList<PlaceJson> getPrinterPlaces(List<Place> places) {

		ArrayList<PlaceJson> placeJsons = new ArrayList<PlaceJson>();

		if (places != null && !places.isEmpty()) {
			for (Place place : places) {

				PlaceJson placeJson = new PlaceJson();
				placeJson.setId(place.getPlaceAllId());
				placeJson.setPlaceNameId(place.getPlaceNameId());
				placeJson.setPlaceName(place.getPlaceNameFull());
				placeJson.setPrefFlag(place.getPrefFlag());
				placeJson.setPlType(place.getPlType());
				placeJson.setUnsure(place.getUnsure());
				placeJsons.add(placeJson);
			}
		}

		return placeJsons;

	}

	@Override
	public List<DocPeopleFieldJson> getDocPeopleFields(Integer personId,
			MiaDocumentEntity docEnt, List<DocumentFieldEntity> documentFields) {

		List<DocPeopleFieldJson> fields = super.getDocPeopleFields(personId,
				docEnt, documentFields);

		if (docEnt.getNews() != null
				&& hasField(docEnt.getNews().getPrinter(),
						String.valueOf(personId))) {
			DocPeopleFieldJson field = new DocPeopleFieldJson();
			field.setFieldBeName(PeopleFieldType.Printer.toString());
			field.setFieldName(getFieldName(documentFields,
					docEnt.getCategory(), PeopleFieldType.Printer.toString()));

			if (fields == null)
				fields = new ArrayList<DocPeopleFieldJson>();

			fields.add(field);

		}

		return fields;

	}

}
