package org.medici.mia.common.json.advancedsearch;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.util.DateUtils;
import org.medici.mia.domain.User;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@JsonTypeName("archivalLocationAdvancedSearch")
public class ArchivalLocationAdvancedSearchJson extends
		GenericAdvancedSearchJson {

	private static final long serialVersionUID = 1L;
	
	private static final String MIA_BASE_QUERY = 
			  "SELECT DISTINCT docEnt.documentEntityId "
			+ "FROM tblDocumentEnts docEnt ";
	private static final String BIA_BASE_QUERY = 
			  "SELECT DISTINCT docEnt.ENTRYID FROM tblDocuments docEnt ";
	
	private static final String BASE_MIA_JOIN_CLAUSE =
			  "JOIN tblDocumentEntsUploadedFiles entsUp "
			+ "ON entsUp.documentId = docEnt.documentEntityId "
			+ "JOIN tblUploadedFiles upFile ON upFile.id = entsUp.fileId "
			+ "JOIN tblUploads uploads ON uploads.id = upFile.idTblUpload "
			+ "JOIN tblCollections coll ON coll.id = uploads.collection ";
	private static final String BASE_BIA_JOIN_CLAUSE = 
			  "JOIN tblVolumes vol ON vol.SUMMARYID = docEnt.SUMMARYID "
			+ "JOIN tblCollections coll ON coll.id = vol.collection ";
	
	private static final String BASE_WHERE_CLAUSE = 
			"WHERE coll.collectionName = \"%s\" ";
	
	private static final String MIA_DOCUMENTS_NOT_DELETED = 
			" AND docEnt.flgLogicalDelete != 1";
	private static final String BIA_DOCUMENTS_NOT_DELETED = 
			" AND docEnt.logicalDelete != 1";
	
	private static String MIA = "mia";
	private static String BIA = "bia";

	@JsonProperty("repository")
	private String repository;
	
	@JsonProperty("collection")
	private String collection;

	@JsonProperty("series")
	private String series;

	@JsonProperty("volume")
	private String volume;

	@JsonProperty("insert")
	private String insert;
	
	public String getRepository() {
		return repository;
	}
	
	public void setRepository(String repository) {
		this.repository = repository;
	}

	public String getCollection() {
		return collection;
	}

	public void setCollection(String collection) {
		this.collection = collection;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getInsert() {
		return insert;
	}

	public void setInsert(String insert) {
		this.insert = insert;
	}
	
	@JsonIgnore
	@Override
	public List<String> getQueries() {
		List<String> queries = new ArrayList<String>();
		
		StringBuffer stringBuffer = new StringBuffer();

		String miaJoinClause = 
				createJoinClause(repository, collection, volume, insert, MIA);
		String biaJoinClause = 
				createJoinClause(repository, collection, volume, insert, BIA);
		
		String miaWhereClause = 
				createWhereClause(repository, collection, volume, insert, MIA);
		String biaWhereClause = 
				createWhereClause(repository, collection, volume, insert, BIA);
		
		String miaQuery = createQuery(
				MIA_BASE_QUERY, miaJoinClause, miaWhereClause);
		String biaQuery = createQuery(
				BIA_BASE_QUERY, biaJoinClause, biaWhereClause);

		stringBuffer.append(miaQuery);
		stringBuffer.append(" UNION ");
		stringBuffer.append(biaQuery);
		stringBuffer.append(";");
		
		queries.add(stringBuffer.toString());
		return queries;
	}
	
	private String createQuery(
			String baseQuery, String joinClause, String whereClause) {
		return String.format("%s %s %s", baseQuery, joinClause, whereClause);
	}
	
	private String createJoinClause(
			String repository, String collection, String volume, 
			String insert, String database) {
		String joinClause = "";
		
		if (database.equalsIgnoreCase(MIA)) {
			joinClause = BASE_MIA_JOIN_CLAUSE;
			
			if (shouldAddToFilter(repository)) {
				joinClause += 
						  "JOIN tblRepositories repo "
						+ "ON repo.id = uploads.repository ";
			}
			
			if (shouldAddToFilter(volume)) {
				joinClause += 
						  "JOIN tblVolumes vol "
						+ "ON vol.SUMMARYID = uploads.volume ";
			}
			if (shouldAddToFilter(insert)) {
				joinClause += 
						"LEFT OUTER JOIN tblInserts ins ON ins.id = uploads.insertN";
			}
		}
		else {
			joinClause += BASE_BIA_JOIN_CLAUSE;
			if (shouldAddToFilter(insert)) {
				joinClause += 
						"JOIN tblInserts ins ON ins.volume = vol.SUMMARYID";
			}
		}
		
		
		
		return joinClause;
	}
	
	private String createWhereClause(
			String repository, String collection, String volume, String insert, 
			String database) {
		String whereClause = String.format(BASE_WHERE_CLAUSE, collection);
		
		if (shouldAddToFilter(volume)) {
			whereClause += String.format("AND vol.volume = \"%s\" ", volume);
			
			if (shouldAddToFilter(insert)) {
				whereClause += String.format("AND ins.insertN = \"%s\" ", insert);
			}
		}
		
		if (database.equalsIgnoreCase(MIA)) {
			if (shouldAddToFilter(repository)) {
				whereClause += 
						String.format("AND repo.repositoryName = \"%s\" ", 
								repository);
			}
			
			whereClause += MIA_DOCUMENTS_NOT_DELETED;
		}
		else {
			whereClause += BIA_DOCUMENTS_NOT_DELETED;
		}
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			if (database.equalsIgnoreCase(MIA)) {
				whereClause += String.format(" AND ((docEnt.dateCreated > \"%s\" AND docEnt.createdBy != \"%s\")",
						DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount());
				whereClause += String.format(" OR (docEnt.dateLastUpdate > \"%s\" AND docEnt.LastUpdateBy != \"%s\"))",
						DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount());
			} else {
				whereClause += String.format(" AND ((docEnt.DateCreated > \"%s\" AND docEnt.createdBy != \"%s\")",
						DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount());
				whereClause += String.format(" OR (docEnt.LastUpdate > \"%s\" AND docEnt.lastUpdateBy != \"%s\"))",
						DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount());
			}
		}
		
		return whereClause;
	}
	
	private boolean shouldAddToFilter(String field) {
		return field != null && !field.isEmpty();
	}
}