package org.medici.mia.common.json.casestudy;

import org.medici.mia.domain.CaseStudy;

import java.io.Serializable;
import java.text.SimpleDateFormat;

public class CaseStudyJson implements Serializable {
    private Integer id;
    private String title;
    private String description;
    private String notes;
    private String createdAt;
    private String createdBy;




    public CaseStudyJson() {
    }

    public CaseStudyJson(Integer id, String title, String description, String notes, String createdAt, String createdBy) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.notes = notes;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
    }

    public static CaseStudyJson toJson(CaseStudy cs){
        SimpleDateFormat sdf = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        CaseStudyJson json = new CaseStudyJson();
        json.setId(cs.getId());
        json.setTitle(cs.getTitle());
        json.setDescription(cs.getDescription());
        json.setNotes(cs.getNotes());
        json.setCreatedAt(cs.getCreatedAt() == null ? null : sdf.format(cs.getCreatedAt()));
        json.setCreatedBy(cs.getCreatedBy() == null ? null : cs.getCreatedBy().getAccount());
        return json;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
