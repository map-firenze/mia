package org.medici.mia.common.json.advancedsearch;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.util.DateUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@JsonTypeName("personVitalStatisticBADBLBBDOAdvancedSearch")
public class PersonVitalStatisticBADBLBBDOAdvancedSearchJson extends
		GenericAdvancedSearchJson {

	private static final long serialVersionUID = 1L;
	private static final String NOT_DELETED_PEOPLE = "and  p.LOGICALDELETE = 0";

	String searchType;

	Integer year;
	Integer month;
	Integer day;

	Integer andYear;
	Integer andMonth;
	Integer andDay;

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Integer getAndYear() {
		return andYear;
	}

	public void setAndYear(Integer andYear) {
		this.andYear = andYear;
	}

	public Integer getAndMonth() {
		return andMonth;
	}

	public void setAndMonth(Integer andMonth) {
		this.andMonth = andMonth;
	}

	public Integer getAndDay() {
		return andDay;
	}

	public void setAndDay(Integer andDay) {
		this.andDay = andDay;
	}

	@JsonIgnore
	@Override
	public List<String> getQueries() {

		List<String> queries = new ArrayList<String>();
		StringBuffer b = new StringBuffer();
		Integer date = DateUtils.getLuceneDate(getYear(), getMonth(), getDay());
		if ("bornAfter".equalsIgnoreCase(getSearchType())) {
			// query for birthdate

			b.append("select distinct p.PERSONID from tblPeople p where BORNDATE > '");
			b.append(date + "'");
			b.append(" and  p.logicalDelete = 0");
		} else if ("deadBy".equalsIgnoreCase(getSearchType())) {

			b.append("select distinct p.PERSONID from tblPeople p where DEATHDATE = '");
			b.append(date + "'");
			b.append(NOT_DELETED_PEOPLE);
		} else if ("livedBetween".equalsIgnoreCase(getSearchType())) {
			Integer andDate = DateUtils.getLuceneDate(getAndYear(),
					getAndMonth(), getAndDay());

			b.append("select distinct p.PERSONID from tblPeople p where BORNDATE between '");
			b.append(date + "'");
			b.append(" and '");
			b.append(andDate + "'");
			b.append(NOT_DELETED_PEOPLE);
		} else if ("bornDiedOn".equalsIgnoreCase(getSearchType())) {

			b.append("select distinct p.PERSONID from tblPeople p where BORNDATE = '");
			b.append(date + "'");
			b.append("or DEATHDATE = '");
			b.append(date + "'");
			b.append(NOT_DELETED_PEOPLE);
		}

		queries.add(b.toString());
		return queries;

	}
}