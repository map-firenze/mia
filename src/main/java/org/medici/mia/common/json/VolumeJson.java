/*
 * MyArchiveVolume.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.common.json;

import org.medici.mia.domain.InsertEntity;
import org.medici.mia.domain.Volume;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class VolumeJson {

	private String volumeName;
	private String volumeId;
	private Integer aeCount;
	private Boolean hasInserts;

	public VolumeJson toJson(Volume volume) {

		this.setVolumeId(volume.getSummaryId().toString());
		this.setVolumeName(volume.getVolume());
		this.setAeCount(volume.getAeCount());
		this.setHasInserts(false);
		if (volume.getInsertEntities() != null) {
			boolean hasIns = false;
			for(InsertEntity ins: volume.getInsertEntities()){
				if(hasIns) break;
				if(ins.getLogicalDelete() != null) {
					if (!ins.getLogicalDelete()) {
						hasIns = true;
					}
				}
			}
			this.setHasInserts(hasIns);
		}
		return this;
	}



	public String getVolumeName() {
		return volumeName;
	}

	public void setVolumeName(String volumeName) {
		this.volumeName = volumeName;
	}

	public String getVolumeId() {
		return volumeId;
	}

	public void setVolumeId(String volumeId) {
		this.volumeId = volumeId;
	}

	public Integer getAeCount() {
		return aeCount;
	}

	public void setAeCount(Integer aeCount) {
		this.aeCount = aeCount;
	}

	public Boolean getHasInserts() {
		return hasInserts;
	}

	public void setHasInserts(Boolean hasInserts) {
		this.hasInserts = hasInserts;
	}
}
