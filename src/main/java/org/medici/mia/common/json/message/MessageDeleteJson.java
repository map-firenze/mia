package org.medici.mia.common.json.message;

import java.util.List;

public class MessageDeleteJson {

	private static final long serialVersionUID = 1L;
	
	private List<Integer> messagesId;

	public List<Integer> getMessagesId() {
		return messagesId;
	}

	public void setMessagesId(List<Integer> messagesId) {
		this.messagesId = messagesId;
	}

	

	
}