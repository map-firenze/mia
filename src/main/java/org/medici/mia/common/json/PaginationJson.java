package org.medici.mia.common.json;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class PaginationJson {

	private Integer totalPagesForSingleAe;
	private Integer totalPagesForAllAes;

	public Integer getTotalPagesForSingleAe() {
		return totalPagesForSingleAe;
	}

	public void setTotalPagesForSingleAe(Integer totalPagesForSingleAe) {
		this.totalPagesForSingleAe = totalPagesForSingleAe;
	}

	public Integer getTotalPagesForAllAes() {
		return totalPagesForAllAes;
	}

	public void setTotalPagesForAllAes(Integer totalPagesForAllAes) {
		this.totalPagesForAllAes = totalPagesForAllAes;
	}

}
