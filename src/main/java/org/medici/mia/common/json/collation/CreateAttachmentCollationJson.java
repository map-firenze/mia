package org.medici.mia.common.json.collation;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.medici.mia.domain.Collation;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.User;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class CreateAttachmentCollationJson implements Serializable {

    private static final long serialVersionUID = -1260384401173886242L;

    @JsonProperty(required = true)
    private Integer childDocumentId;

    @JsonProperty(required = true)
    private Integer parentDocumentId;

    @JsonProperty
    private String title;

    @JsonProperty
    private Boolean unsure;

    @JsonProperty
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getUnsure() {
        return unsure;
    }

    public void setUnsure(Boolean unsure) {
        this.unsure = unsure;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getChildDocumentId() {
        return childDocumentId;
    }

    public void setChildDocumentId(Integer childDocumentId) {
        this.childDocumentId = childDocumentId;
    }

    public Integer getParentDocumentId() {
        return parentDocumentId;
    }

    public void setParentDocumentId(Integer parentDocumentId) {
        this.parentDocumentId = parentDocumentId;
    }

    public Collation toEntity(List<MiaDocumentEntity> entities, User user) {
        Collation collation = new Collation();
        collation.setChildDocument(entities.get(0));
        collation.setParentDocument(entities.get(1));
        collation.setTitle(this.getTitle());
        collation.setDescription(this.getDescription());
        collation.setUnsure(this.getUnsure() == null ? false : this.getUnsure());
        collation.setCreated(new Date());
        collation.setCreatedBy(user);
        collation.setType(Collation.CollationType.ATTACHMENT);
        collation.setLogicalDelete(false);
        return collation;
    }

}
