/*
 * DocumentTranscriptionJson.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.common.json.document;

import org.medici.mia.domain.DocumentTranscriptionEntity;
import org.medici.mia.domain.IGenericEntity;

/**
 * JSON implementation for store document's transcriptions in the
 * tblDocTranscriptions database table.
 * 
 * 
 */
public class DocumentTranscriptionJson implements IModifyDocJson {

	private static final long serialVersionUID = 3725099586315739138L;

	private String docTranscriptionId;
	private String documentId;
	private String uploadedFileId;

	private String transcription;

	public String getDocTranscriptionId() {
		return docTranscriptionId;
	}

	public void setDocTranscriptionId(String docTranscriptionId) {
		this.docTranscriptionId = docTranscriptionId;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getTranscription() {
		return transcription;
	}

	public void setTranscription(String transcription) {
		this.transcription = transcription;
	}

	public String getUploadedFileId() {
		return uploadedFileId;
	}

	public void setUploadedFileId(String uploadedFileId) {
		this.uploadedFileId = uploadedFileId;
	}

	@Override
	public void toJson(IGenericEntity entity) {
		if (entity == null)
			return;
		if (entity instanceof DocumentTranscriptionEntity) {
			DocumentTranscriptionEntity transEentity = (DocumentTranscriptionEntity) entity;
			this.setDocTranscriptionId(String.valueOf(transEentity
					.getDocTranscriptionId()));
			this.setDocumentId(String.valueOf(transEentity
					.getDocumentEntityId()));
			this.setUploadedFileId(String.valueOf(transEentity
					.getUploadedFileId()));
			this.setTranscription(transEentity.getTranscription());
		}

	}

	@Override
	public IGenericEntity toEntity(IGenericEntity entity) {

		if (entity == null)
			entity = new DocumentTranscriptionEntity();

		if (entity instanceof DocumentTranscriptionEntity) {
			if (this.getDocTranscriptionId() != null
					&& !this.getDocTranscriptionId().isEmpty()) {
				((DocumentTranscriptionEntity) entity)
						.setDocTranscriptionId(Integer.valueOf(this
								.getDocTranscriptionId()));
			}
			if (this.getDocumentId() != null && !this.getDocumentId().isEmpty()) {
				((DocumentTranscriptionEntity) entity)
						.setDocumentEntityId(Integer.valueOf(this
								.getDocumentId()));
			}

			if (this.getUploadedFileId() != null
					&& !this.getUploadedFileId().isEmpty()) {
				((DocumentTranscriptionEntity) entity)
						.setUploadedFileId(Integer.valueOf(this
								.getUploadedFileId()));
			}
			((DocumentTranscriptionEntity) entity).setTranscription(this
					.getTranscription());

			return (DocumentTranscriptionEntity) entity;
		}

		return null;

	}

}
