package org.medici.mia.common.json.advancedsearch;

public class AdvancedSearchFactoryJson {
	public static IAdvancedSearchJson getAdvancedSearchJson(
			IAdvancedSearchJson searchJson) {
		String searchSection = ((GenericAdvancedSearchJson) searchJson)
				.getSearchSection();
		if (AdvancedSearchType.ARCHIVAL_LOCATION_SEARCH.getValue()
				.equalsIgnoreCase(searchSection)) {
			return ((ArchivalLocationAdvancedSearchJson) searchJson);
		} else if (AdvancedSearchType.PEOPLE_SEARCH.getValue()
				.equalsIgnoreCase(searchSection)) {
			return ((PeopleAdvancedSearchJson) searchJson);
		} else if (AdvancedSearchType.PEOPLE_CATEGORY_FIELDS_SEARCH.getValue()
				.equalsIgnoreCase(searchSection)) {
			return ((PeopleCategoryFieldsAdvancedSearchJson) searchJson);
		} else if (AdvancedSearchType.PLACES_SEARCH.getValue()
				.equalsIgnoreCase(searchSection)) {
			return ((PlacesAdvancedSearchJson) searchJson);
		} else if (AdvancedSearchType.CATEGORY_AND_TYPOLOGY_SEARCH.getValue()
				.equalsIgnoreCase(searchSection)) {
			return ((CategoryAndTypologyAdvancedSearchJson) searchJson);
		} else if (AdvancedSearchType.TRANSCRIPTION_SEARCH.getValue()
				.equalsIgnoreCase(searchSection)) {
			return ((TranscriptionAdvancedSearchJson) searchJson);
		} else if (AdvancedSearchType.SYNOPSIS_SEARCH.getValue()
				.equalsIgnoreCase(searchSection)) {
			return ((SynopsisAdvancedSearchJson) searchJson);
		} else if (AdvancedSearchType.DOCUMENT_ENTITY_ID.getValue()
				.equalsIgnoreCase(searchSection)) {
			return ((DocumentEntityIdAdvancedSearchJson) searchJson);
		} else if (AdvancedSearchType.TOPIC_SEARCH.getValue().equalsIgnoreCase(
				searchSection)) {
			return ((TopicsAdvancedSearchJson) searchJson);
		} else if (AdvancedSearchType.DATA_SEARCH.getValue().equalsIgnoreCase(
				searchSection)) {
			return ((DataAdvancedSearchJson) searchJson);
		} else if (AdvancedSearchType.DOCUMENT_OWNER.getValue()
				.equalsIgnoreCase(searchSection)) {
			return ((DocumentOwnerAdvancedSearchJson) searchJson);
		} else if (AdvancedSearchType.LANGUEGES_SEARCH.getValue()
				.equalsIgnoreCase(searchSection)) {
			return ((LanguageSearchJson) searchJson);
		}else if (AdvancedSearchType.LANGUEGES_SEARCH.getValue()
				.equalsIgnoreCase(searchSection)) {
			return ((LanguageSearchJson) searchJson);
		}
		return null;
	}

}
