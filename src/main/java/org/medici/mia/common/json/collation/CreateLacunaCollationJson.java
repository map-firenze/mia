package org.medici.mia.common.json.collation;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.medici.mia.domain.*;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class CreateLacunaCollationJson implements Serializable {


    private static final long serialVersionUID = -3915317499206714616L;

    @JsonProperty(required = true)
    private Integer childDocumentId;

    @JsonProperty(required = true)
    private Integer volumeId;

    @JsonProperty(required = true)
    private Integer insertId;

    @JsonProperty(required = true)
    private Integer uploadFileId;

    @JsonProperty(required = true)
    private String folioNumber;

    @JsonProperty
    private String folioRV;

    @JsonProperty
    private String title;

    @JsonProperty
    private Boolean unsure;

    @JsonProperty
    private String description;

    public Integer getChildDocumentId() {
        return childDocumentId;
    }

    public void setChildDocumentId(Integer childDocumentId) {
        this.childDocumentId = childDocumentId;
    }

    public Integer getVolumeId() {
        return volumeId;
    }

    public void setVolumeId(Integer volumeId) {
        this.volumeId = volumeId;
    }

    public Integer getInsertId() {
        return insertId;
    }

    public void setInsertId(Integer insertId) {
        this.insertId = insertId;
    }

    public Integer getUploadFileId() {
        return uploadFileId;
    }

    public void setUploadFileId(Integer uploadFileId) {
        this.uploadFileId = uploadFileId;
    }

    public String getFolioNumber() {
        return folioNumber;
    }

    public void setFolioNumber(String folioNumber) {
        this.folioNumber = folioNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getUnsure() {
        return unsure;
    }

    public void setUnsure(Boolean unsure) {
        this.unsure = unsure;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFolioRV() {
        return folioRV;
    }

    public void setFolioRV(String folioRV) {
        this.folioRV = folioRV;
    }

    public Collation toEntity(MiaDocumentEntity childDocument, User user, UploadFileEntity file, HashMap<String,Object> volInsHash) {
        Collation collation = new Collation();
        collation.setChildDocument(childDocument);
        collation.setTitle(this.getTitle());
        collation.setDescription(this.getDescription());
        collation.setUnsure(this.getUnsure() == null ? false : this.getUnsure());
        collation.setCreated(new Date());
        collation.setCreatedBy(user);
        collation.setFile(file);
        collation.setFolioNumber(this.getFolioNumber());
        collation.setType(Collation.CollationType.LACUNA);
        collation.setLogicalDelete(false);
        collation.setChildVolume((Volume) volInsHash.get("childVolume"));
        collation.setParentVolume((Volume) volInsHash.get("parentVolume"));
        collation.setChildInsert((InsertEntity) volInsHash.get("childInsert"));
        collation.setParentInsert((InsertEntity) volInsHash.get("parentInsert"));
        collation.setFolioNumber(this.getFolioNumber());
        collation.setFoliorv(this.getFolioRV());
        return collation;
    }

}
