package org.medici.mia.common.json;

import java.io.Serializable;

import org.medici.mia.domain.DocumentRefToPeopleEntity;

public class PeopleRelatedToDocumentJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer documentId;

	private Integer deletePersonCitedInDoc;

	private PeopleBaseJson newPersonCitedInDoc;
	private PeopleBaseJson oldPersonCitedInDoc;

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	public PeopleBaseJson getNewPersonCitedInDoc() {
		return newPersonCitedInDoc;
	}

	public void setNewPersonCitedInDoc(PeopleBaseJson newPersonCitedInDoc) {
		this.newPersonCitedInDoc = newPersonCitedInDoc;
	}

	public Integer getDeletePersonCitedInDoc() {
		return deletePersonCitedInDoc;
	}

	public void setDeletePersonCitedInDoc(Integer deletePersonCitedInDoc) {
		this.deletePersonCitedInDoc = deletePersonCitedInDoc;
	}

	public PeopleBaseJson getOldPersonCitedInDoc() {
		return oldPersonCitedInDoc;
	}

	public void setOldPersonCitedInDoc(PeopleBaseJson oldPersonCitedInDoc) {
		this.oldPersonCitedInDoc = oldPersonCitedInDoc;
	}

}