package org.medici.mia.common.json.document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.medici.mia.common.json.FieldJson;
import org.medici.mia.common.json.PeopleBaseJson;
import org.medici.mia.common.json.PeopleJson;
import org.medici.mia.common.json.PlaceJson;
import org.medici.mia.domain.DocumentFieldEntity;
import org.medici.mia.domain.LiteraryEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.People;
import org.medici.mia.domain.Place;
import org.medici.mia.service.miadoc.DocumentType;
import org.medici.mia.service.miadoc.MiaDocumentUtils;
import org.medici.mia.service.miadoc.PeopleFieldType;
import org.medici.mia.service.miadoc.PlaceFieldType;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@JsonTypeName("literaryWorks")
public class LiteraryJson extends GenericDocumentJson {

	private static final long serialVersionUID = 1L;

	@JsonProperty("dedicatee")
	private List<PeopleJson> dedicatees;

	@JsonProperty("printer")
	private List<PeopleJson> printers;

	@JsonProperty("printerPlace")
	private List<PlaceJson> printerPlaces;

	public LiteraryJson() {
	}

	public List<PeopleJson> getDedicatees() {
		return dedicatees;
	}

	public void setDedicatees(List<PeopleJson> dedicatees) {
		this.dedicatees = dedicatees;
	}

	public List<PeopleJson> getPrinters() {
		return printers;
	}

	public void setPrinters(List<PeopleJson> printers) {
		this.printers = printers;
	}

	public List<PlaceJson> getPrinterPlaces() {
		return printerPlaces;
	}

	public void setPrinterPlaces(List<PlaceJson> printerPlaces) {
		this.printerPlaces = printerPlaces;
	}

	@Override
	public void init(MiaDocumentEntity docEntity) {
		super.init(docEntity);
		if (docEntity.getLiterary() != null) {
			this.dedicatees = getPrinters(docEntity.getLiterary()
					.getDedicatees());
			this.printers = getPrinters(docEntity.getLiterary().getPrinters());
			this.printerPlaces = getPrinterPlaces(docEntity.getLiterary()
					.getPrinterPlaces());
		}
	}

	// Find people for attached document
	@Override
	public List<FindPeopleForDocJson> findPeopleJson(MiaDocumentEntity docEntity) {
		List<FindPeopleForDocJson> modifyPeoples = new ArrayList<FindPeopleForDocJson>();

		FindPeopleForDocJson modifyPeopleDedicatees = new FindPeopleForDocJson();
		modifyPeopleDedicatees.setType(PeopleFieldType.Dedicatee.toString());
		ArrayList<PeopleBaseJson> peopleBasesDedicatees = null;

		if (docEntity.getLiterary() != null
				&& docEntity.getLiterary().getDedicatee() != null
				&& !docEntity.getLiterary().getDedicatee().isEmpty()) {

			peopleBasesDedicatees = new ArrayList<PeopleBaseJson>();

			for (String peopleIdStr : (List<String>) Arrays.asList(docEntity
					.getLiterary().getDedicatee().split(","))) {

				PeopleBaseJson peopleBase = new PeopleBaseJson();
				String[] peoplesIdArr = peopleIdStr.split(":");
				if (peoplesIdArr[0] != null || !peoplesIdArr[0].isEmpty()) {
					peopleBase.setId(Integer.valueOf(peoplesIdArr[0]));
				}
				if (peoplesIdArr.length > 1) {
					peopleBase.setUnsure(peoplesIdArr[1]);
				}

				peopleBasesDedicatees.add(peopleBase);
			}
		}
		modifyPeopleDedicatees.setPeoples(peopleBasesDedicatees);
		modifyPeoples.add(modifyPeopleDedicatees);

		// setting of Printer

		FindPeopleForDocJson modifyPeoplePrinter = new FindPeopleForDocJson();
		modifyPeoplePrinter.setType(PeopleFieldType.Printer.toString());
		ArrayList<PeopleBaseJson> peopleBasesPrinters = null;

		if (docEntity.getLiterary() != null
				&& docEntity.getLiterary().getPrinter() != null
				&& !docEntity.getLiterary().getPrinter().isEmpty()) {

			peopleBasesPrinters = new ArrayList<PeopleBaseJson>();

			for (String peopleIdStr : (List<String>) Arrays.asList(docEntity
					.getLiterary().getPrinter().split(","))) {

				PeopleBaseJson peopleBase = new PeopleBaseJson();
				String[] peoplesIdArr = peopleIdStr.split(":");
				if (peoplesIdArr[0] != null || !peoplesIdArr[0].isEmpty()) {
					peopleBase.setId(Integer.valueOf(peoplesIdArr[0]));
				}
				if (peoplesIdArr.length > 1) {
					peopleBase.setUnsure(peoplesIdArr[1]);
				}

				peopleBasesPrinters.add(peopleBase);
			}
		}
		modifyPeoplePrinter.setPeoples(peopleBasesPrinters);
		modifyPeoples.add(modifyPeoplePrinter);

		return modifyPeoples;
	}

	// Used for finding place attached to document
	@Override
	public List<FindPlaceForDocJson> findPlaceJson(MiaDocumentEntity docEntity) {
		List<FindPlaceForDocJson> modifyPlaces = new ArrayList<FindPlaceForDocJson>();

		FindPlaceForDocJson modifyPlace = new FindPlaceForDocJson();
		modifyPlace.setType(PlaceFieldType.printerPlace.toString());

		if (docEntity.getLiterary() != null) {
			modifyPlace.setPlaces(MiaDocumentUtils.getPlaceBases(docEntity
					.getLiterary().getPrinterPlace()));
		}
		modifyPlaces.add(modifyPlace);
		return modifyPlaces;
	}

	// Used for modify - add - delete people attached to document
	@Override
	public MiaDocumentEntity getModifiedDocumentEnt(MiaDocumentEntity docEnt,
			ModifyPeopleForDocJson modifyPeople) {

		String modifedStr = "";

		if (modifyPeople.getType() == null) {
			docEnt.setErrorMsg("The type has no value)");
			return docEnt;
		}

		if (docEnt.getLiterary() == null) {
			docEnt.setErrorMsg("There is no " + docEnt.getCategory()
					+ " record associated with the document.");
			return docEnt;
		}

		if (PeopleFieldType.Dedicatee.toString().equalsIgnoreCase(
				modifyPeople.getType())) {
			modifedStr = MiaDocumentUtils.modifyPeopleForDoc(docEnt
					.getLiterary().getDedicatee(), modifyPeople);
			docEnt.getLiterary().setDedicatee(modifedStr);

		} else if (PeopleFieldType.Printer.toString().equalsIgnoreCase(
				modifyPeople.getType())) {

			modifedStr = MiaDocumentUtils.modifyPeopleForDoc(docEnt
					.getLiterary().getPrinter(), modifyPeople);
			docEnt.getLiterary().setPrinter(modifedStr);

		} else {
			docEnt.setErrorMsg("The type " + modifyPeople.getType()
					+ " is not compatible with the category "
					+ docEnt.getCategory());
		}

		return docEnt;

	}

	// Used for modify - add - delete place attached to document
	@Override
	public MiaDocumentEntity getModifiedDocumentEnt(MiaDocumentEntity docEnt,
			ModifyPlaceForDocJson modifyPlace) {

		if (modifyPlace.getType() == null) {
			docEnt.setErrorMsg("The type has no value)");
			return docEnt;
		}

		if (docEnt.getLiterary() == null) {
			docEnt.setErrorMsg("There is no " + docEnt.getCategory()
					+ " record associated with the document.");
			return docEnt;
		}

		if (PlaceFieldType.printerPlace.toString().equalsIgnoreCase(
				modifyPlace.getType())) {

			String modifedStr = MiaDocumentUtils.modifyPlaceForDoc(docEnt
					.getLiterary().getPrinterPlace(), modifyPlace);
			docEnt.getLiterary().setPrinterPlace(modifedStr);

		} else {
			docEnt.setErrorMsg("The type " + modifyPlace.getType()
					+ " is not compatible with the category "
					+ docEnt.getCategory());
		}

		return docEnt;

	}

	@Override
	public MiaDocumentEntity getMiaDocumentEntity(DocumentJson docJson) {

		MiaDocumentEntity docEnt = super.getMiaDocumentEntity(docJson);

		docEnt.setCategory(DocumentType.literaryWorks.toString());
		LiteraryJson litJson = (LiteraryJson) docJson;
		LiteraryEntity litEnt = new LiteraryEntity();

		if (litJson.getDedicatees() != null
				&& !litJson.getDedicatees().isEmpty()) {
			String dedicateesStr = "";
			for (PeopleJson people : litJson.getDedicatees()) {
				dedicateesStr = dedicateesStr + people.getId() + ":";
				if (!StringUtils.isEmpty(people.getUnsure())) {
					dedicateesStr = dedicateesStr + people.getUnsure();
				} else {
					dedicateesStr = dedicateesStr + "S";
				}
				dedicateesStr = dedicateesStr + ",";
			}

			litEnt.setDedicatee(dedicateesStr);
		}

		if (litJson.getPrinters() != null && !litJson.getPrinters().isEmpty()) {
			String printerStr = "";
			for (PeopleJson people : litJson.getPrinters()) {
				printerStr = printerStr + people.getId() + ":";
				if (!StringUtils.isEmpty(people.getUnsure())) {
					printerStr = printerStr + people.getUnsure();
				} else {
					printerStr = printerStr + "S";
				}
				printerStr = printerStr + ",";
			}

			litEnt.setPrinter(printerStr);
		}

		if (litJson.getPrinterPlaces() != null
				&& !litJson.getPrinterPlaces().isEmpty()) {
			String printerPlaceStr = "";
			for (PlaceJson place : litJson.getPrinterPlaces()) {
				printerPlaceStr = printerPlaceStr + place.getId() + ":";
				if (!StringUtils.isEmpty(place.getUnsure())) {
					printerPlaceStr = printerPlaceStr + place.getUnsure();
				} else {
					printerPlaceStr = printerPlaceStr + "S";
				}
				printerPlaceStr = printerPlaceStr + ",";
			}

			litEnt.setPrinterPlace(printerPlaceStr);

		}

		docEnt.setLiterary(litEnt);
		return docEnt;
	}

	@Override
	public List<FieldJson> getJsonFields(
			List<DocumentFieldEntity> documentFields) {

		List<FieldJson> fields = new ArrayList<FieldJson>();
		for (DocumentFieldEntity field : documentFields) {
			if (DocumentType.literaryWorks.toString().equalsIgnoreCase(
					field.getDocumentCategory())) {
				boolean isMultiple = false;
				if ("1".equalsIgnoreCase(field.getAllowMultiple())) {
					isMultiple = true;
				}
				FieldJson fieldJson = new FieldJson(field.getFieldName(),
						field.getFieldBeName(), field.getFieldValue(),
						field.getFieldType(), isMultiple);

				fields.add(fieldJson);

			}
		}

		return fields;
	}

	private ArrayList<PeopleJson> getPrinters(List<People> peoples) {

		ArrayList<PeopleJson> peopleJsons = new ArrayList<PeopleJson>();

		if (peoples != null && !peoples.isEmpty()) {
			for (People people : peoples) {

				PeopleJson peopleJson = new PeopleJson();
				peopleJson.setId(people.getPersonId());
				peopleJson.setGender(people.getGender());
				peopleJson.setMapNameLf(people.getMapNameLf());
				peopleJson.setActiveStart(people.getActiveStart());
				peopleJson.setActiveEnd(people.getActiveEnd());
				peopleJson.setBornYear(people.getBornYear());
				peopleJson.setDeathYear(people.getDeathYear());
				peopleJson.setFirstName(people.getFirst());
				peopleJson.setLastName(people.getLast());
				peopleJson.setUnsure(people.getUnsure());
				peopleJsons.add(peopleJson);
			}
		}

		return peopleJsons;
	}

	private ArrayList<PlaceJson> getPrinterPlaces(List<Place> places) {

		ArrayList<PlaceJson> placeJsons = new ArrayList<PlaceJson>();

		if (places != null && !places.isEmpty()) {
			for (Place place : places) {

				PlaceJson placeJson = new PlaceJson();
				placeJson.setId(place.getPlaceAllId());
				placeJson.setPlaceNameId(place.getPlaceNameId());
				placeJson.setPlaceName(place.getPlaceNameFull());
				placeJson.setPrefFlag(place.getPrefFlag());
				placeJson.setPlType(place.getPlType());
				placeJson.setUnsure(place.getUnsure());
				placeJsons.add(placeJson);
			}
		}

		return placeJsons;

	}

	@Override
	public List<DocPeopleFieldJson> getDocPeopleFields(Integer personId,
			MiaDocumentEntity docEnt, List<DocumentFieldEntity> documentFields) {

		List<DocPeopleFieldJson> fields = super.getDocPeopleFields(personId,
				docEnt, documentFields);

		if (docEnt.getLiterary() != null
				&& hasField(docEnt.getLiterary().getDedicatee(),
						String.valueOf(personId))) {
			DocPeopleFieldJson field = new DocPeopleFieldJson();
			field.setFieldBeName(PeopleFieldType.Dedicatee.toString());
			field.setFieldName(getFieldName(documentFields,
					docEnt.getCategory(), PeopleFieldType.Dedicatee.toString()));

			if (fields == null)
				fields = new ArrayList<DocPeopleFieldJson>();

			fields.add(field);
		}

		if (docEnt.getLiterary() != null
				&& hasField(docEnt.getLiterary().getPrinter(),
						String.valueOf(personId))) {
			DocPeopleFieldJson field = new DocPeopleFieldJson();
			field.setFieldBeName(PeopleFieldType.Printer.toString());
			field.setFieldName(getFieldName(documentFields,
					docEnt.getCategory(), PeopleFieldType.Printer.toString()));

			if (fields == null)
				fields = new ArrayList<DocPeopleFieldJson>();

			fields.add(field);
		}

		return fields;

	}

}
