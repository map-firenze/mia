package org.medici.mia.common.json;

import java.io.Serializable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class DateJson implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer docYear;
	private Integer docMonth;
	private Integer docDay;
	private Integer docModernYear;
	private Integer docDateUncertain;
	private Integer docUndated;
	private String  docDateNotes;
	
	public DateJson() {
		super();
	}

	public DateJson(Integer docYear, Integer docMonth, Integer docDay,
			Integer docModernYear, Integer docDateUncertain, Integer docUndated,
			String docDateNotes) {
		super();
		this.docYear = docYear;
		this.docMonth = docMonth;
		this.docDay = docDay;
		this.docModernYear = docModernYear;
		this.docDateUncertain = docDateUncertain;
		this.docUndated = docUndated;
		this.docDateNotes = docDateNotes;
	}

	public Integer getDocYear() {
		return docYear;
	}

	public void setDocYear(Integer docYear) {
		this.docYear = docYear;
	}

	public Integer getDocMonth() {
		return docMonth;
	}

	public void setDocMonth(Integer docMonth) {
		this.docMonth = docMonth;
	}

	public Integer getDocDay() {
		return docDay;
	}

	public void setDocDay(Integer docDay) {
		this.docDay = docDay;
	}

	public Integer getDocModernYear() {
		return docModernYear;
	}

	public void setDocModernYear(Integer docModernYear) {
		this.docModernYear = docModernYear;
	}

	public Integer getDocDateUncertain() {
		return docDateUncertain;
	}

	public void setDocDateUncertain(Integer docDateUncertain) {
		this.docDateUncertain = docDateUncertain;
	}

	public Integer getDocUndated() {
		return docUndated;
	}

	public void setDocUndated(Integer docUndated) {
		this.docUndated = docUndated;
	}

	public String getDocDateNotes() {
		return docDateNotes;
	}

	public void setDocDateNotes(String docDateNotes) {
		this.docDateNotes = docDateNotes;
	}
		
}
