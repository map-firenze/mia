package org.medici.mia.common.json;

public class CreatePlaceJson extends PlaceJson {
	
	private static final long serialVersionUID = 1L;

	private String placeNameWithAccent;
	private Integer placeParentId;
	private String placeNotes;

	public String getPlaceNameWithAccent() {
		return placeNameWithAccent;
	}

	public void setPlaceNameWithAccent(String placeNameWithAccent) {
		this.placeNameWithAccent = placeNameWithAccent;
	}

	public String getPlaceNotes() {
		return placeNotes;
	}

	public void setPlaceNotes(String placeNotes) {
		this.placeNotes = placeNotes;
	}

	public Integer getPlaceParentId() {
		return placeParentId;
	}

	public void setPlaceParentId(Integer placeParentId) {
		this.placeParentId = placeParentId;
	}
}