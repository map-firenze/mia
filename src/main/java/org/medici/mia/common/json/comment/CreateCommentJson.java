package org.medici.mia.common.json.comment;

import org.medici.mia.domain.Comment;
import org.medici.mia.domain.User;

import java.io.Serializable;
import java.util.Date;

public class CreateCommentJson implements Serializable {

    private static final long serialVersionUID = 7707453179865415715L;
    private Integer recordId;
    private String recordType;
    private String text;
    private String title;
    private Integer repliedToId;
    private String mentions;

    public CreateCommentJson() {
    }

    public Comment toEntity(User createdBy){
        if(recordId == null || recordType == null || createdBy == null){
            return null;
        }
        Comment c = new Comment();
        c.setCreatedBy(createdBy);
        c.setText(text);
        c.setTitle(title);
        c.setRecordId(recordId);
        c.setRecordType(Comment.RecordType.valueOf(recordType));
        c.setCreatedAt(new Date());
        c.setLogicalDelete(false);
        c.setMentions(mentions);
        return c;
    }

    public Comment toEntity(User createdBy, Comment parent, Comment repliedTo){
        Comment c = this.toEntity(createdBy);
        c.setRepliedTo(repliedTo);
        c.setParent(parent);
        return c;
    }

    public Comment toEntity(User createdBy, Comment parent){
        Comment c = this.toEntity(createdBy);
        c.setParent(parent);
        return c;
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getRepliedToId() {
        return repliedToId;
    }

    public void setRepliedToId(Integer repliedToId) {
        this.repliedToId = repliedToId;
    }

    public String getMentions() {
        return mentions;
    }

    public void setMentions(String mentions) {
        this.mentions = mentions;
    }
}
