package org.medici.mia.common.json.administration;

import java.io.Serializable;

import org.medici.mia.domain.User;

public class AdminUserJson implements Serializable {

	private static final long serialVersionUID = 1L;	

	private String account;

	private String firstName;

	private String lastName;

	private String organization;

	private String email;

	private String city;

	private String country;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public AdminUserJson toJson(User user) {

		this.setAccount(user.getAccount());
		this.setFirstName(user.getFirstName());
		this.setLastName(user.getLastName());
		this.setOrganization(user.getOrganization());
		this.setEmail(user.getMail());
		this.setCity(user.getCity());
		this.setCountry(user.getCountry());

		return this;
	}

}
