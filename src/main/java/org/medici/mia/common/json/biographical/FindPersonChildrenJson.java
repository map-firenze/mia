package org.medici.mia.common.json.biographical;

import java.io.Serializable;
import java.util.List;

public class FindPersonChildrenJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer personId;

	private List<Integer> children;

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public List<Integer> getChildren() {
		return children;
	}

	public void setChildren(List<Integer> children) {
		this.children = children;
	}

}