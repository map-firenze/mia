package org.medici.mia.common.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private String account;

	private String firstName;

	private String lastName;

	private String organization;

	private String city;

	private String country;

	private String email;

	private String password;

	private List<String> userGroups;

	private String accountExpirationYear;

	private String accountExpirationMonth;

	private String accountExpirationDay;

	private String expirationDate;

	private String photo;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<String> getUserGroups() {
		if (userGroups == null) 
			userGroups = new ArrayList<String>();
		return userGroups;
	}

	public void setUserGroups(List<String> userGroups) {
		this.userGroups = userGroups;
	}

	public String getAccountExpirationYear() {
		return accountExpirationYear;
	}

	public void setAccountExpirationYear(String accountExpirationYear) {
		this.accountExpirationYear = accountExpirationYear;
	}

	public String getAccountExpirationMonth() {
		return accountExpirationMonth;
	}

	public void setAccountExpirationMonth(String accountExpirationMonth) {
		this.accountExpirationMonth = accountExpirationMonth;
	}

	public String getAccountExpirationDay() {
		return accountExpirationDay;
	}

	public void setAccountExpirationDay(String accountExpirationDay) {
		this.accountExpirationDay = accountExpirationDay;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
}
