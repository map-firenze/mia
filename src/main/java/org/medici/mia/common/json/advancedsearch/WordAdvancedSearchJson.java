package org.medici.mia.common.json.advancedsearch;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.json.search.PeopleWordSearchType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@JsonTypeName("wordAdvancedSearch")
public class WordAdvancedSearchJson extends GenericAdvancedSearchJson {

	private static final long serialVersionUID = 1L;
	private static final String NOT_DELETED_PEOPLE = "and  p.logicalDelete !=1";

	String searchString;
	String nameType;

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public String getNameType() {
		return nameType;
	}

	public void setNameType(String nameType) {
		this.nameType = nameType;
	}

	@JsonIgnore
	@Override
	public List<String> getQueries() {

		// select distinct a.PersonID from tblPeople p, tblAltNames a where
		// a.PersonID = p.PERSONID and a.AltName like '%cos%' and a.NameType =
		// 'Family';

		List<String> queries = new ArrayList<String>();
		StringBuffer b = new StringBuffer();
		b.append("select distinct p.PERSONID from tblPeople p, tblAltNames a where a.PERSONID = p.PERSONID and a.ALTNAME like ");
		b.append("'%" + this.getSearchString() + "%'");

		if (this.getNameType() != null
				&& !this.getNameType().isEmpty()
				&& !PeopleWordSearchType.ALL_NAME_TYPES.toString()
						.equalsIgnoreCase(getNameType())) {
			b.append(" and a.NAMETYPE = '");
			b.append(this.getNameType() + "'");
		}

		b.append(NOT_DELETED_PEOPLE);

		queries.add(b.toString());
		return queries;

	}
}