package org.medici.mia.common.json.search;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.medici.mia.common.json.PlaceJson;
import org.medici.mia.domain.Place;

public class PlaceSearchJson extends PlaceJson {

	private static final long serialVersionUID = 1L;
	private final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd hh:mm:ss");
	private String placeNameFull;
	private String plNameFullPlType;
	private String termAccent;
	private Integer plParentPlaceAllId;
	private String plParent;
	private String placesMemo;
	private Integer LogicalDelete;
	private String dateCreated;
	private String lastUpdate;
	private String createdBy;
	private String lastUpdateBy;

	public PlaceSearchJson() {
	}

	public void toJson(Place entity) {
		if (entity == null)
			return;
		if (entity instanceof Place) {

			super.toJson(entity);
			if (entity.getCreatedBy() != null) {
				this.setCreatedBy(entity.getCreatedBy().getAccount());
			}
			if (entity.getDateCreated() != null) {
				this.setDateCreated(sdf.format(entity.getDateCreated()));
			}
			if (entity.getLastUpdate() != null) {
				this.setLastUpdate(sdf.format(entity.getLastUpdate()));
			}
			if (entity.getLastUpdateBy() != null) {
				this.setLastUpdateBy(entity.getLastUpdateBy().getAccount());
			}
			if (entity.getLogicalDelete()) {
				this.setLogicalDelete(1);
			} else {
				this.setLogicalDelete(0);
			}
			this.setPlaceNameFull(entity.getPlaceNameFull());
			this.setPlacesMemo(entity.getPlacesMemo());
			this.setPlNameFullPlType(entity.getPlNameFullPlType());
			this.setPlParent(entity.getPlParent());
			if (entity.getParentPlace() != null) {
				this.setPlParentPlaceAllId(entity.getParentPlace()
						.getPlaceAllId());
			}
			this.setTermAccent(entity.getTermAccent());

		}

	}

	public String getPlaceNameFull() {
		return placeNameFull;
	}

	public void setPlaceNameFull(String placeNameFull) {
		this.placeNameFull = placeNameFull;
	}

	public String getPlNameFullPlType() {
		return plNameFullPlType;
	}

	public void setPlNameFullPlType(String plNameFullPlType) {
		this.plNameFullPlType = plNameFullPlType;
	}

	public String getTermAccent() {
		return termAccent;
	}

	public void setTermAccent(String termAccent) {
		this.termAccent = termAccent;
	}

	public Integer getPlParentPlaceAllId() {
		return plParentPlaceAllId;
	}

	public void setPlParentPlaceAllId(Integer plParentPlaceAllId) {
		this.plParentPlaceAllId = plParentPlaceAllId;
	}

	public String getPlParent() {
		return plParent;
	}

	public void setPlParent(String plParent) {
		this.plParent = plParent;
	}

	public String getPlacesMemo() {
		return placesMemo;
	}

	public void setPlacesMemo(String placesMemo) {
		this.placesMemo = placesMemo;
	}

	public Integer getLogicalDelete() {
		return LogicalDelete;
	}

	public void setLogicalDelete(Integer logicalDelete) {
		LogicalDelete = logicalDelete;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

}