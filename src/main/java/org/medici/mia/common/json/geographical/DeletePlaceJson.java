package org.medici.mia.common.json.geographical;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public class DeletePlaceJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer principalPlaceId;
	private List<Integer> documentEntities;
	private List<Integer> peopleRecords;

	public List<Integer> getDocumentEntities() {
		return documentEntities;
	}

	public void setDocumentEntities(List<Integer> documentEntities) {
		this.documentEntities = documentEntities;
	}

	public List<Integer> getPeopleRecords() {
		return peopleRecords;
	}

	public void setPeopleRecords(List<Integer> peopleRecords) {
		this.peopleRecords = peopleRecords;
	}

	public Integer getPrincipalPlaceId() {
		return principalPlaceId;
	}

	public void setPrincipalPlaceId(Integer principalPlaceId) {
		this.principalPlaceId = principalPlaceId;
	}

}