package org.medici.mia.common.json.advancedsearch;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TopicSearchJson implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("topicTitle")
	private String topicTitle;

	private String placeAllId;
	
	private String topicId;

	public String getTopicId() {
		return topicId;
	}

	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}

	public String getTopicTitle() {
		return topicTitle;
	}

	public void setTopicTitle(String topicTitle) {
		this.topicTitle = topicTitle;
	}

	public String getPlaceAllId() {
		return placeAllId;
	}

	public void setPlaceAllId(String placeAllId) {
		this.placeAllId = placeAllId;
	}

}
