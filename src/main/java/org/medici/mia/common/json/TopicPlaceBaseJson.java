package org.medici.mia.common.json;
/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class TopicPlaceBaseJson extends TopicBaseJson {

	private static final long serialVersionUID = 1L;

	private String unsure;
	private String prefFlag;

	public TopicPlaceBaseJson() {

	}

	public String getUnsure() {
		return unsure;
	}

	public void setUnsure(String unsure) {
		this.unsure = unsure;
	}

	public String getPrefFlag() {
		return prefFlag;
	}

	public void setPrefFlag(String prefFlag) {
		this.prefFlag = prefFlag;
	}

}
