package org.medici.mia.common.json.document;

import java.io.Serializable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class BiblioRefJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer idRef;
	private Integer idDoc;
	private String name;
	private String permalink;

	public BiblioRefJson() {
		super();
	}

	public BiblioRefJson(Integer idRef, Integer idDoc, String name,
			String permalink) {
		super();
		this.idRef = idRef;
		this.idDoc = idDoc;
		this.name = name;
		this.permalink = permalink;
	}

	public Integer getIdRef() {
		return idRef;
	}

	public void setIdRef(Integer idRef) {
		this.idRef = idRef;
	}

	public Integer getIdDoc() {
		return idDoc;
	}

	public void setIdDoc(Integer idDoc) {
		this.idDoc = idDoc;
	}

	public String getPermalink() {
		return permalink;
	}

	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
