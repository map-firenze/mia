package org.medici.mia.common.json.titleocc;

import java.io.Serializable;

import org.medici.mia.domain.RoleCat;

public class RoleCatJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer roleCateId;
	private String roleCatMajor;
	private String roleCatMinor;

	public void toJson(RoleCat entity) {
		if (entity == null)
			return;

		this.setRoleCateId(entity.getRoleCatId());
		this.setRoleCatMinor(entity.getRoleCatMinor());
		this.setRoleCatMajor(entity.getRoleCatMajor());

	}

	public Integer getRoleCateId() {
		return roleCateId;
	}

	public void setRoleCateId(Integer roleCateId) {
		this.roleCateId = roleCateId;
	}

	public String getRoleCatMajor() {
		return roleCatMajor;
	}

	public void setRoleCatMajor(String roleCatMajor) {
		this.roleCatMajor = roleCatMajor;
	}

	public String getRoleCatMinor() {
		return roleCatMinor;
	}

	public void setRoleCatMinor(String roleCatMinor) {
		this.roleCatMinor = roleCatMinor;
	}

}