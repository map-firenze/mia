package org.medici.mia.common.json.historylog;

import java.io.Serializable;
import java.text.SimpleDateFormat;

import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.domain.HistoryLogEntity.RecordType;

public class LastAccessedRecordJson implements Serializable {

	private static final long serialVersionUID = 1L;
	private final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd hh:mm:ss");

	Integer lastUpload;
	String lastUploadDate;
	Integer lastDocument;
	String  lastDocumentDate;
	Integer lastPerson;
	String lastPersonDate;
	Integer lastPlace;
	String lastPlaceDate;
	VolumeInsertJson lastVolumeOrInsert;

	public LastAccessedRecordJson toJson(HistoryLogEntity entity) {
		if (entity != null) {

			if (RecordType.VOL.equals(entity.getRecordType())
					|| RecordType.INS.equals(entity.getRecordType())) {
				VolumeInsertJson vi = new VolumeInsertJson();
				vi.toJson(entity);
				this.setLastVolumeOrInsert(vi);
			}
			if (RecordType.AE.equals(entity.getRecordType())) {
				this.setLastUpload(entity.getEntryId());
				this.setLastUploadDate(entity.getDateAndTime() == null ? null : sdf.format(entity.getDateAndTime()));
			}
			if (RecordType.DE.equals(entity.getRecordType())) {
				this.setLastDocument(entity.getEntryId());
				this.setLastDocumentDate(entity.getDateAndTime() == null ? null : sdf.format(entity.getDateAndTime()));
			}
			if (RecordType.BIO.equals(entity.getRecordType())) {
				this.setLastPerson(entity.getEntryId());
				this.setLastPersonDate(entity.getDateAndTime() == null ? null : sdf.format(entity.getDateAndTime()));
			}
			if (RecordType.GEO.equals(entity.getRecordType())) {
				this.setLastPlace(entity.getEntryId());
				this.setLastPlaceDate(entity.getDateAndTime() == null ? null : sdf.format(entity.getDateAndTime()));
			}

		}

		return this;
	}

	public Integer getLastUpload() {
		return lastUpload;
	}

	public void setLastUpload(Integer lastUpload) {
		this.lastUpload = lastUpload;
	}

	public Integer getLastDocument() {
		return lastDocument;
	}

	public void setLastDocument(Integer lastDocument) {
		this.lastDocument = lastDocument;
	}

	public Integer getLastPerson() {
		return lastPerson;
	}

	public void setLastPerson(Integer lastPerson) {
		this.lastPerson = lastPerson;
	}

	public Integer getLastPlace() {
		return lastPlace;
	}

	public void setLastPlace(Integer lastPlace) {
		this.lastPlace = lastPlace;
	}

	public VolumeInsertJson getLastVolumeOrInsert() {
		return lastVolumeOrInsert;
	}

	public void setLastVolumeOrInsert(VolumeInsertJson lastVolumeOrInsert) {
		this.lastVolumeOrInsert = lastVolumeOrInsert;
	}

	public String getLastUploadDate() {
		return lastUploadDate;
	}

	public void setLastUploadDate(String lastUploadDate) {
		this.lastUploadDate = lastUploadDate;
	}

	public String getLastDocumentDate() {
		return lastDocumentDate;
	}

	public void setLastDocumentDate(String lastDocumentDate) {
		this.lastDocumentDate = lastDocumentDate;
	}

	public String getLastPersonDate() {
		return lastPersonDate;
	}

	public void setLastPersonDate(String lastPersonDate) {
		this.lastPersonDate = lastPersonDate;
	}

	public String getLastPlaceDate() {
		return lastPlaceDate;
	}

	public void setLastPlaceDate(String lastPlaceDate) {
		this.lastPlaceDate = lastPlaceDate;
	}
}