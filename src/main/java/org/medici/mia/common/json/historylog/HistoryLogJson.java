/*
 * HistoryLogJson.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.common.json.historylog;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.medici.mia.domain.HistoryLogEntity.RecordType;
import org.medici.mia.domain.HistoryLogEntity.UserAction;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * @author mbarlotti
 *
 */
public class HistoryLogJson implements Serializable {

	private static final long serialVersionUID = -3623995058530021148L;

	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private Integer id;
	private String type;
	private String activityDate;
	private String action;
	private String title;
	private String archivalLocation;
	private String personName;
	private String placeName;
	private String user;

	@JsonIgnore
	private String collectionAbbreviation;
	@JsonIgnore
	private String volumeNo;
	@JsonIgnore
	private String insertNo;
	@JsonIgnore
	private boolean foliosPresent;
	@JsonIgnore
	private String folioNumber;
	@JsonIgnore
	private String rectoVerso;

	public HistoryLogJson toJson(Object[] result) {
		if (result == null) {
			return null;
		} else {
			this.setId((Integer) result[0]);
			this.setType((String) result[1]);
			this.setActivityDate(result[2] == null ? null : sdf.format((Date) result[2]));
			this.setAction((String) result[3]);
			this.setTitle((String) result[4]);
			this.setPersonName((String) result[5]);
			this.setPlaceName((String) result[6]);
			this.setUser((String) result[7]);
			this.setCollectionAbbreviation((String) result[8]);
			this.setVolumeNo((String) result[9]);
			this.setInsertNo((String) result[10]);
			return this;
		}
	}

	private void buildArchivalLocation() {
		StringBuilder sb = new StringBuilder();
		if (!StringUtils.isEmpty(collectionAbbreviation)) {
			sb.append(collectionAbbreviation);
		}
		if (!StringUtils.isEmpty(volumeNo)) {
			sb.append(" - ");
			sb.append(volumeNo);
		}
		if (!StringUtils.isEmpty(insertNo)) {
			sb.append(" - ");
			sb.append(insertNo);
		}
		if (foliosPresent) {
			if (!StringUtils.isEmpty(folioNumber)) {
				sb.append(" - f. ");
				sb.append(folioNumber);
				if (!StringUtils.isEmpty(rectoVerso)) {
					sb.append(rectoVerso.substring(0, 1));
				}
			} else {
				sb.append(" - nn");
			}
		}
		if (sb.length() > 0) {
			this.archivalLocation = sb.toString();
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(String activityDate) {
		this.activityDate = activityDate;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getArchivalLocation() {
		buildArchivalLocation();
		return archivalLocation;
	}

	public void setArchivalLocation(String archivalLocation) {
		this.archivalLocation = archivalLocation;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getCollectionAbbreviation() {
		return collectionAbbreviation;
	}

	public void setCollectionAbbreviation(String collectionAbbreviation) {
		this.collectionAbbreviation = collectionAbbreviation;
	}

	public String getVolumeNo() {
		return volumeNo;
	}

	public void setVolumeNo(String volumeNo) {
		this.volumeNo = volumeNo;
	}

	public String getInsertNo() {
		return insertNo;
	}

	public void setInsertNo(String insertNo) {
		this.insertNo = insertNo;
	}

	public boolean isFoliosPresent() {
		return foliosPresent;
	}

	public void setFoliosPresent(boolean foliosPresent) {
		this.foliosPresent = foliosPresent;
	}

	public String getFolioNumber() {
		return folioNumber;
	}

	public void setFolioNumber(String folioNumber) {
		this.folioNumber = folioNumber;
	}

	public String getRectoVerso() {
		return rectoVerso;
	}

	public void setRectoVerso(String rectoVerso) {
		this.rectoVerso = rectoVerso;
	}

}
