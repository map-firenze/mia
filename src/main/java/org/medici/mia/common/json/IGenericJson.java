package org.medici.mia.common.json;

import java.io.Serializable;

import org.medici.mia.domain.IGenericEntity;

/**
 * 
 * @author user
 *
 */
public interface IGenericJson extends Serializable {
	public void toJson(IGenericEntity entity);
	public IGenericEntity toEntity(IGenericEntity entity);

}
