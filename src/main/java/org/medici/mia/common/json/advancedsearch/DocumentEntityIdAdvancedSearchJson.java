package org.medici.mia.common.json.advancedsearch;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.util.DateUtils;
import org.medici.mia.domain.User;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@JsonTypeName("documentEntityIdAdvancedSearch")
public class DocumentEntityIdAdvancedSearchJson extends
		GenericAdvancedSearchJson {

	private static final long serialVersionUID = 1L;
	private static final String NOT_DELETED_DOC = " d.flgLogicalDelete != 1";

	@JsonProperty("documentEntityId")
	private String documentEntityId;

	public String getDocumentEntityId() {
		return documentEntityId;
	}

	public void setDocumentEntityId(String documentEntityId) {
		this.documentEntityId = documentEntityId;
	}

	@JsonIgnore
	@Override
	public List<String> getQueries() {

		List<String> queries = new ArrayList<String>();
		StringBuffer b = new StringBuffer();
		b.append("select distinct documentEntityId documentId from tblDocumentEnts d where documentEntityId = ");
		b.append(this.getDocumentEntityId());
		b.append(" and " + NOT_DELETED_DOC);
		
		if (getNewsfeedUser() != null) {
			User newsfeedUser = getNewsfeedUser();
			b.append(String.format(" AND ((d.dateCreated > '%s' AND d.createdBy != '%s')",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
			b.append(String.format(" OR (d.dateLastUpdate > '%s' AND d.LastUpdateBy != '%s'))",
					DateUtils.getMYSQLDate(newsfeedUser.getNewsfeedLoginDate()), newsfeedUser.getAccount()));
		}

		queries.add(b.toString());
		return queries;

	}
}