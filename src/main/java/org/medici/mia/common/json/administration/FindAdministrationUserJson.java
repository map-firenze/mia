package org.medici.mia.common.json.administration;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.jboss.logging.Logger;
import org.medici.mia.common.json.BaseUserJson;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.domain.User;

public class FindAdministrationUserJson extends BaseUserJson {

	private static final long serialVersionUID = 1L;
	private final Logger logger = Logger.getLogger(this.getClass());
	private final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd hh:mm:ss");

	private String organization;
	private String accountExpirationYear;
	private String accountExpirationMonth;
	private String accountExpirationDay;
	private String expirationPasswordDate;
	private Integer approved;
	private Integer active;
	private Integer locked;
	private Integer mailhide;
	private Boolean portrait;
	private String portraitImageName;
	private String interests;
	private String photoPath;

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getAccountExpirationYear() {
		return accountExpirationYear;
	}

	public void setAccountExpirationYear(String accountExpirationYear) {
		this.accountExpirationYear = accountExpirationYear;
	}

	public String getAccountExpirationMonth() {
		return accountExpirationMonth;
	}

	public void setAccountExpirationMonth(String accountExpirationMonth) {
		this.accountExpirationMonth = accountExpirationMonth;
	}

	public String getAccountExpirationDay() {
		return accountExpirationDay;
	}

	public void setAccountExpirationDay(String accountExpirationDay) {
		this.accountExpirationDay = accountExpirationDay;
	}

	public String getExpirationPasswordDate() {
		return expirationPasswordDate;
	}

	public void setExpirationPasswordDate(String expirationPasswordDate) {
		this.expirationPasswordDate = expirationPasswordDate;
	}

	public Integer getApproved() {
		return approved;
	}

	public void setApproved(Integer approved) {
		this.approved = approved;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getLocked() {
		return locked;
	}

	public void setLocked(Integer locked) {
		this.locked = locked;
	}

	public Integer getMailhide() {
		return mailhide;
	}

	public void setMailhide(Integer mailhide) {
		this.mailhide = mailhide;
	}

	public FindAdministrationUserJson toJson(User user) {

		super.toJson(user);

		this.setOrganization(user.getOrganization());
		this.setPortraitImageName(user.getPortraitImageName());
		this.setInterests(user.getInterests());
		this.setPortrait(user.getPortrait());
		
		if (!user.getPortrait()) {
			this.setPhotoPath("/Mia/images/1024/img_user.png");
		}
		else {
			this.setPhotoPath("/Mia/user/ShowPortraitUser.do?account=" + user.getAccount());
		}
		
		try {
			// Set expiration date
			
			if (user.getExpirationDate() != null) {
				String dateStr = sdf.format(user.getExpirationDate());				
			    Calendar cal = Calendar.getInstance();
			    cal.setTime(sdf.parse(dateStr));			  
				this.setAccountExpirationDay(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
				this.setAccountExpirationMonth(String.valueOf(cal.get(Calendar.MONTH)+1));
				this.setAccountExpirationYear(String.valueOf(cal.get(Calendar.YEAR)));
			}
			// Set expiration password date
			if (user.getExpirationPasswordDate() != null) {
				this.setExpirationPasswordDate(sdf.format(user
						.getExpirationPasswordDate()));
			}

		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}

		if (user.getApproved()) {
			this.setApproved(1);
		} else {
			this.setApproved(0);
		}
		if (user.getActive()) {
			this.setActive(1);
		} else {
			this.setActive(0);
		}
		if (user.getLocked()) {
			this.setLocked(1);
		} else {
			this.setLocked(0);
		}

		if (user.getMailHide()) {
			this.setMailhide(1);
		} else {
			this.setMailhide(0);
		}

		return this;
	}

	public String getPortraitImageName() {
		return portraitImageName;
	}

	public void setPortraitImageName(String portraitImageName) {
		this.portraitImageName = portraitImageName;
	}

	public String getInterests() {
		return interests;
	}

	public void setInterests(String interests) {
		this.interests = interests;
	}

	public Boolean getPortrait() {
		return portrait;
	}

	public void setPortrait(Boolean portrait) {
		this.portrait = portrait;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

}
