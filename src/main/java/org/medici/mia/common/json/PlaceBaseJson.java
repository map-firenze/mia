package org.medici.mia.common.json;

import java.io.Serializable;
/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public class PlaceBaseJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String unsure;

	private String prefFlag;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUnsure() {
		return unsure;
	}

	public void setUnsure(String unsure) {
		this.unsure = unsure;
	}

	public String getPrefFlag() {
		return prefFlag;
	}

	public void setPrefFlag(String prefFlag) {
		this.prefFlag = prefFlag;
	}

}