package org.medici.mia.common.json.document;

import java.io.Serializable;

import org.medici.mia.common.json.PlaceBaseJson;

public class ModifyPlaceForDocJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private String documentId;
	private String category;
	private String type;
	private PlaceBaseJson newPlace;
	private Integer idPlaceToDelete;

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public PlaceBaseJson getNewPlace() {
		return newPlace;
	}

	public void setNewPlace(PlaceBaseJson newPlace) {
		this.newPlace = newPlace;
	}

	public Integer getIdPlaceToDelete() {
		return idPlaceToDelete;
	}

	public void setIdPlaceToDelete(Integer idPlaceToDelete) {
		this.idPlaceToDelete = idPlaceToDelete;
	}

	public String getType() {

		if (type != null)
			return type.replaceAll("\\s+", "");

		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}