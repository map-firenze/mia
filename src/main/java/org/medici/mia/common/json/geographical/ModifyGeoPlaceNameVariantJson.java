package org.medici.mia.common.json.geographical;

import java.io.Serializable;

/** 
 * 
 * @author Shadab Bigdel (<a href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public class ModifyGeoPlaceNameVariantJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer placeAllId;
	private PlaceNameVariantJson newPlaceNameVariant;

	public PlaceNameVariantJson getNewPlaceNameVariant() {
		return newPlaceNameVariant;
	}

	public void setNewPlaceNameVariant(PlaceNameVariantJson newPlaceNameVariant) {
		this.newPlaceNameVariant = newPlaceNameVariant;
	}

	public Integer getPlaceAllId() {
		return placeAllId;
	}

	public void setPlaceAllId(Integer placeAllId) {
		this.placeAllId = placeAllId;
	}

}