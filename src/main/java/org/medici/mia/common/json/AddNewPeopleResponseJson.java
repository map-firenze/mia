package org.medici.mia.common.json;

public class AddNewPeopleResponseJson extends GenericResponseJson {

	private static final long serialVersionUID = 1L;

	private Integer peopleId;
	private String path;

	public Integer getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(Integer peopleId) {
		this.peopleId = peopleId;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
