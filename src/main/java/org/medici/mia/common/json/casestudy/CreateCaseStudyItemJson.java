package org.medici.mia.common.json.casestudy;

import org.medici.mia.domain.CaseStudyFolder;
import org.medici.mia.domain.CaseStudyItem;

import java.io.Serializable;
import java.util.Date;

public class CreateCaseStudyItemJson implements Serializable {

    private String title;
    private String link;
    private String entityType;
    private Integer entityId;

    public CaseStudyItem toEntity(CaseStudyFolder folder){
        CaseStudyItem.EntityType entityType = CaseStudyItem.EntityType.valueOf(this.entityType);
        return new CaseStudyItem(this.title, this.entityId, entityType, folder, new Date(), 0, this.link);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public Integer getEntityId() {
        return entityId;
    }

    public void setEntityId(Integer entityId) {
        this.entityId = entityId;
    }
}
