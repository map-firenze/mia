package org.medici.mia.common.json.casestudy;

import com.github.underscore.U;
import org.medici.mia.domain.CaseStudy;
import org.medici.mia.domain.CaseStudyFolder;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CaseStudyFolderJson implements Serializable {
    private Integer id;
    private Integer caseStudyId;
    private String title;
    private Integer order;
    private List<CaseStudyItemJson> items = new ArrayList<CaseStudyItemJson>();
    private String createdAt;
    private String createdBy;

    public static CaseStudyFolderJson toJson(CaseStudyFolder f) {
        SimpleDateFormat sdf = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        CaseStudyFolderJson json = new CaseStudyFolderJson();
        json.setId(f.getId());
        json.setCaseStudyId(f.getCaseStudy() == null ? null : f.getCaseStudy().getId());
        json.setTitle(f.getTitle());
        json.setOrder(f.getOrder());
        json.setCreatedAt(f.getCreatedAt() == null ? null : sdf.format(f.getCreatedAt()));
        json.setCreatedBy(f.getCreatedBy() == null ? null : f.getCreatedBy().getAccount());
        return json;
    }

    public CaseStudyFolderJson() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCaseStudyId() {
        return caseStudyId;
    }

    public void setCaseStudyId(Integer caseStudyId) {
        this.caseStudyId = caseStudyId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public List<CaseStudyItemJson> getItems() {
        return items;
    }

    public void setItems(List<CaseStudyItemJson> items) {
        this.items = items;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
