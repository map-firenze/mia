/*
 * DocumentTranscriptionJson.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.common.json.document;

import org.medici.mia.domain.IGenericEntity;
import org.medici.mia.domain.MiaDocumentEntity;

/**
 * JSON implementation for store document's transcriptions in the
 * tblDocTranscriptions database table.
 * 
 * 
 */
public class CategoryJson implements IModifyDocJson {

	private static final long serialVersionUID = 3725099586315739138L;

	private String documentEntityId;
	private String category;

	public String getDocumentId() {
		return documentEntityId;
	}

	public void setDocumentEntityId(String documentEntityId) {
		this.documentEntityId = documentEntityId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public void toJson(IGenericEntity entity) {
		if (entity == null)
			return;
		if (entity instanceof MiaDocumentEntity) {
			MiaDocumentEntity catEntity = (MiaDocumentEntity) entity;
			if (catEntity.getDocumentEntityId() != null) {
				this.setDocumentEntityId(String.valueOf(catEntity
						.getDocumentEntityId()));
			}
			this.setCategory(catEntity.getCategory());
		}

	}

	@Override
	public IGenericEntity toEntity(IGenericEntity entity) {

		if (entity == null)
			entity = new MiaDocumentEntity();

		if (entity instanceof MiaDocumentEntity) {

			if (this.getDocumentId() != null
					&& !this.getDocumentId().isEmpty()) {
				((MiaDocumentEntity) entity).setDocumentEntityId(Integer
						.valueOf(this.getDocumentId()));
			}
			((MiaDocumentEntity) entity).setCategory(this.getCategory());

			return (MiaDocumentEntity) entity;
		}

		return null;

	}

}
