package org.medici.mia.common.json.news;

import java.io.Serializable;
import java.util.List;

public class NewsUserJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<NewsAEJson> aes;
	private List<NewsPeopleJson> people;
	private List<NewsDocumentJson> documents;
	private List<NewsPlaceJson> places;

	public List<NewsAEJson> getAes() {
		return aes;
	}

	public void setAes(List<NewsAEJson> aes) {
		this.aes = aes;
	}

	public List<NewsPeopleJson> getPeople() {
		return people;
	}

	public void setPeople(List<NewsPeopleJson> people) {
		this.people = people;
	}

	public List<NewsDocumentJson> getDocuments() {
		return documents;
	}

	public void setDocuments(List<NewsDocumentJson> documents) {
		this.documents = documents;
	}

	public List<NewsPlaceJson> getPlaces() {
		return places;
	}

	public void setPlaces(List<NewsPlaceJson> places) {
		this.places = places;
	}

}