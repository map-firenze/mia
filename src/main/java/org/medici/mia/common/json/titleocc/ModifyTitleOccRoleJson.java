package org.medici.mia.common.json.titleocc;

import java.io.Serializable;

public class ModifyTitleOccRoleJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer titleOccId;
	private TitleOccRoleJson newTitleOccRoleJson;
	private TitleOccRoleJson oldTitleOccRoleJson;
	private Integer titleAndOccupationToBeDeleted;
	public Integer getTitleOccId() {
		return titleOccId;
	}
	public void setTitleOccId(Integer titleOccId) {
		this.titleOccId = titleOccId;
	}
	public TitleOccRoleJson getNewTitleOccRoleJson() {
		return newTitleOccRoleJson;
	}
	public void setNewTitleOccRoleJson(TitleOccRoleJson newTitleOccRoleJson) {
		this.newTitleOccRoleJson = newTitleOccRoleJson;
	}
	public TitleOccRoleJson getOldTitleOccRoleJson() {
		return oldTitleOccRoleJson;
	}
	public void setOldTitleOccRoleJson(TitleOccRoleJson oldTitleOccRoleJson) {
		this.oldTitleOccRoleJson = oldTitleOccRoleJson;
	}
	public Integer getTitleAndOccupationToBeDeleted() {
		return titleAndOccupationToBeDeleted;
	}
	public void setTitleAndOccupationToBeDeleted(
			Integer titleAndOccupationToBeDeleted) {
		this.titleAndOccupationToBeDeleted = titleAndOccupationToBeDeleted;
	}

	
}