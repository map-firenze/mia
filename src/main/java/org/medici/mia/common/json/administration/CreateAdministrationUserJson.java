package org.medici.mia.common.json.administration;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.jboss.logging.Logger;
import org.medici.mia.common.json.BaseUserJson;
import org.medici.mia.domain.User;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateAdministrationUserJson extends BaseUserJson {

	private static final long serialVersionUID = 1L;
	private final Logger logger = Logger.getLogger(this.getClass());
	private final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd hh:mm:ss");

	private String organization;
	private String password;

	@JsonProperty("accountExpirationYear")
	private String accountExpirationYear;

	@JsonProperty("accountExpirationMonth")
	private String accountExpirationMonth;

	@JsonProperty("accountExpirationDay")
	private String accountExpirationDay;

	private String approvedBy;
	private String activationdate;
	private Integer approved;
	private Integer active;
	private Integer locked;
	private Integer mailhide;

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAccountExpirationYear() {
		return accountExpirationYear;
	}

	public void setAccountExpirationYear(String accountExpirationYear) {
		this.accountExpirationYear = accountExpirationYear;
	}

	public String getAccountExpirationMonth() {
		return accountExpirationMonth;
	}

	public void setAccountExpirationMonth(String accountExpirationMonth) {
		this.accountExpirationMonth = accountExpirationMonth;
	}

	public String getAccountExpirationDay() {
		return accountExpirationDay;
	}

	public void setAccountExpirationDay(String accountExpirationDay) {
		this.accountExpirationDay = accountExpirationDay;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getActivationdate() {
		return activationdate;
	}

	public void setActivationdate(String activationdate) {
		this.activationdate = activationdate;
	}

	public Integer getApproved() {
		return approved;
	}

	public void setApproved(Integer approved) {
		this.approved = approved;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getLocked() {
		return locked;
	}

	public void setLocked(Integer locked) {
		this.locked = locked;
	}

	public Integer getMailhide() {
		return mailhide;
	}

	public void setMailhide(Integer mailhide) {
		this.mailhide = mailhide;
	}

	public User toEntity(User user) {

		user = super.toEntity(user);
		User appBy = new User();
		appBy.setAccount(this.getApprovedBy());
		user.setApprovedBy(appBy);
		user.setActivationDate(new Date());

		user.setOrganization(this.getOrganization());

		user.setMail(this.getEmail());
		user.setPassword(this.getPassword());

		try {
			// Set expiration date

			String expDateInSDF = this.getAccountExpirationYear() + "-"
					+ this.getAccountExpirationMonth() + "-"
					+ this.getAccountExpirationDay() + " 00:00:00";
			Date expDate = sdf.parse(expDateInSDF);
			user.setExpirationDate(expDate);

			user.setRegistrationDate(new Date());
			user.setBadLogin(0);
			user.setActive(true);

			user.setActivationDate(new Date());
			user.setForumNumberOfPost(new Long(0));

			user.setPortrait(Boolean.FALSE);
			user.setMailNotification(Boolean.FALSE);
			user.setForumTopicSubscription(Boolean.TRUE);

		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
		if (this.getApproved() != null && this.getApproved().equals(1)) {
			user.setApproved(Boolean.TRUE);
		} else {
			user.setApproved(Boolean.FALSE);
		}
		if (this.getActive() != null && this.getActive().equals(1)) {
			user.setActive(Boolean.TRUE);
		} else {
			user.setActive(Boolean.FALSE);
		}
		if (this.getLocked() != null && this.getLocked().equals(1)) {
			user.setLocked(Boolean.TRUE);
		} else {
			user.setLocked(Boolean.FALSE);
		}

		if (this.getMailhide() != null && this.getMailhide().equals(1)) {
			user.setMailHide(Boolean.TRUE);
		} else {
			user.setMailHide(Boolean.FALSE);
		}

		user.setMiddleName(" ");
		
		if (this.getHideActivities() == null) {
			user.setHideActivities(Boolean.FALSE);
		}
		if (this.getHidePublications() == null) {
			user.setHidePublications(Boolean.FALSE);
		}
		if (this.getHideStatistics() == null) {
			user.setHideStatistics(Boolean.FALSE);
		}
		
		return user;
	}

}
