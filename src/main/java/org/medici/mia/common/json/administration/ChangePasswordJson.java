package org.medici.mia.common.json.administration;

import java.io.Serializable;

import org.medici.mia.domain.User;
import org.medici.mia.domain.UserAuthority.Authority;

public class ChangePasswordJson implements Serializable {

	private static final long serialVersionUID = 1L;

	Boolean autorized;
	Authority authority;

	public Boolean getAutorized() {
		return autorized;
	}

	public void setAutorized(Boolean isAutorized) {
		this.autorized = isAutorized;
	}

	public Authority getAuthority() {
		return authority;
	}

	public void setAuthority(Authority authority) {
		this.authority = authority;
	}

}
