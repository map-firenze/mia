package org.medici.mia.common.json.geographical;

import java.io.Serializable;
import java.util.List;

public class FindGeoPlaceNameVariantJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer placeAllId;
	private List<PlaceNameVariantJson> placeNameVariants;

	public Integer getPlaceAllId() {
		return placeAllId;
	}

	public void setPlaceAllId(Integer placeAllId) {
		this.placeAllId = placeAllId;
	}

	public List<PlaceNameVariantJson> getPlaceNameVariants() {
		return placeNameVariants;
	}

	public void setPlaceNameVariants(
			List<PlaceNameVariantJson> placeNameVariants) {
		this.placeNameVariants = placeNameVariants;
	}

}