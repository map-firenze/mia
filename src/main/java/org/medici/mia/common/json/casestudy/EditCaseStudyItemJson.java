package org.medici.mia.common.json.casestudy;

import java.io.Serializable;

public class EditCaseStudyItemJson implements Serializable {

    private static final long serialVersionUID = -8208571303118301719L;

    private String title;
    private String link;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
