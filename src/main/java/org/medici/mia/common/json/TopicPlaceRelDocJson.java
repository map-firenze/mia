package org.medici.mia.common.json;

import java.io.Serializable;
import java.util.List;
/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class TopicPlaceRelDocJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer countTotalDocuments;
	private List<TopicBaseJson> topics;

	public TopicPlaceRelDocJson() {

	}

	public Integer getCountTotalDocuments() {
		return countTotalDocuments;
	}

	public void setCountTotalDocuments(Integer countTotalDocuments) {
		this.countTotalDocuments = countTotalDocuments;
	}

	public List<TopicBaseJson> getTopics() {
		return topics;
	}

	public void setTopics(List<TopicBaseJson> topics) {
		this.topics = topics;
	}

	
}
