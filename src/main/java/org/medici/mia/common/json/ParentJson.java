package org.medici.mia.common.json;

import java.io.Serializable;

public class ParentJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer parentId;
	private String gender;
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

	

	

}