/*
 * DocumentMetaDataJson.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.common.json.document;

import java.io.Serializable;

/**
 * 
 * @author fsolfato
 *
 */
public class DocumentMetaDataJson implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5581676434648018150L;
	
	private Integer repositoryId;
	private Integer collectionId;
	private Integer volumeId;
	private String folioNumber;
	private String rectoverso;
	private Boolean noNumb;
	private Integer insertId;
	
	private GenericDocumentJson documentData;
	
	public Integer getRepositoryId() {
		return repositoryId;
	}
	public void setRepositoryId(Integer repositoryId) {
		this.repositoryId = repositoryId;
	}
	
	public Integer getCollectionId() {
		return collectionId;
	}
	public void setCollectionId(Integer collectionId) {
		this.collectionId = collectionId;
	}
	
	public Integer getVolumeId() {
		return volumeId;
	}
	public void setVolumeId(Integer volumeId) {
		this.volumeId = volumeId;
	}
	
	public String getFolioNumber() {
		return folioNumber;
	}
	public void setFolioNumber(String folioNumber) {
		this.folioNumber = folioNumber;
	}
	
	public String getRectoverso() {
		return rectoverso;
	}
	public void setRectoverso(String rectoverso) {
		this.rectoverso = rectoverso;
	}
	
	public Boolean getNoNumb() {
		return noNumb;
	}
	public void setNoNumb(Boolean noNumb) {
		this.noNumb = noNumb;
	}
	
	public Integer getInsertId() {
		return insertId;
	}
	public void setInsertId(Integer insertId) {
		this.insertId = insertId;
	}
	
	public GenericDocumentJson getDocumentData() {
		return documentData;
	}
	public void setDocumentData(GenericDocumentJson documentData) {
		this.documentData = documentData;
	}
}
