package org.medici.mia.common.json.advancedsearch;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@JsonTypeName("titleOccupationAdvancedSearch")
public class TitleOccupationsAdvancedSearchJson extends
		GenericAdvancedSearchJson {

	private static final long serialVersionUID = 1L;
		
	private static final String ROLE_CATEGORY_BASE_QUERY = 
			 "SELECT DISTINCT p.PERSONID FROM tblPeople "
			+ "p WHERE p.PERSONID in ";
	
	private static final String INNER_ROLE_CATEGORY_QUERY = 
			  " (SELECT DISTINCT po.PERSONID "
			+ "FROM tblPoLink po, tblTitleOccsList t, tblRoleCats cat "
			+ "WHERE po.TITLEOCCID = t.TITLEOCCID "
			+ "AND t.ROLECATMINORID = cat.RoleCatID "
			+ "AND cat.RoleCatID = %s) ";
	
	private static final String WORD_TITLE_BASE_QUERY = 
			  "SELECT DISTINCT p.PERSONID "
			+ "FROM tblPoLink po, tblPeople p, tblTitleOccsList oc "
			+ "WHERE p.PERSONID = po.PERSONID "
			+ "AND po.TitleOccID = oc.TitleOccID ";
	
	private static final String NOT_DELETED_PEOPLE = " AND p.LOGICALDELETE = 0";

	private List<TitleOccupationSearchJson> roles;
	private String searchType;

	@JsonIgnore
	@Override
	public List<String> getQueries() {

		List<String> queries = new ArrayList<String>();
		String query = new String();

		if (TitleAndOccupationSearchType.ROLE_CATEGORY.getValue()
				.equalsIgnoreCase(getSearchType())) {
			query = getRoleCategoryQuery(roles);
		} else if (TitleAndOccupationSearchType.WORD_TITLE_OCC.getValue()
				.equalsIgnoreCase(getSearchType())) {
			query = getWordTitleQuery(roles);
		} else if (TitleAndOccupationSearchType.MATCH_TITLE_OCC.getValue()
				.equalsIgnoreCase(searchType)) {
			query = getMatchQuery(roles.get(0).getRoleCateId());
		}

		queries.add(query);
		return queries;
	}
	
	private String getRoleCategoryQuery(List<TitleOccupationSearchJson> roles) {
		StringBuffer stringBuffer = new StringBuffer(ROLE_CATEGORY_BASE_QUERY);
		
		for (int index = 0; index < roles.size(); index++) {
			if (index > 0) {
				stringBuffer.append(" AND p.PERSONID in ");
			}
			
			stringBuffer.append(String.format(
					INNER_ROLE_CATEGORY_QUERY, 
					roles.get(index).getRoleCateId()));
		}

		stringBuffer.append(NOT_DELETED_PEOPLE);
		
		return stringBuffer.toString();
	}
	
	private String getWordTitleQuery(List<TitleOccupationSearchJson> roles) {
		StringBuffer stringBuffer = new StringBuffer(WORD_TITLE_BASE_QUERY);
		
		for (TitleOccupationSearchJson role : roles) {
			stringBuffer.append(" AND UPPER(oc.TitleOcc) like UPPER('%");
			stringBuffer.append(role.getRoleCatMinor());
			stringBuffer.append("%') ");
		}

		stringBuffer.append(NOT_DELETED_PEOPLE);
		
		return stringBuffer.toString();
	}
	
	private String getMatchQuery(Integer roleId) {
		StringBuffer stringBuffer = new StringBuffer(String.format(
				  "SELECT p.PERSONID "
				+ "FROM tblPoLink po, tblPeople p, tblTitleOccsList oc "
				+ "WHERE p.PERSONID = po.PERSONID "
				+ "AND po.TITLEOCCID = oc.TITLEOCCID "
				+ "AND oc.TITLEOCCID = %s ", roleId));
		stringBuffer.append(NOT_DELETED_PEOPLE);
		
		return stringBuffer.toString();
	}

	public List<TitleOccupationSearchJson> getRoles() {
		return roles;
	}

	public void setRoles(List<TitleOccupationSearchJson> roles) {
		this.roles = roles;
	}
	
	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

}