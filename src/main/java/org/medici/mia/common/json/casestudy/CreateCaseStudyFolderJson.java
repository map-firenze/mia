package org.medici.mia.common.json.casestudy;

import org.medici.mia.domain.CaseStudy;
import org.medici.mia.domain.CaseStudyFolder;

import java.io.Serializable;

public class CreateCaseStudyFolderJson implements Serializable {
    private String title;
    private Integer order;

    public CreateCaseStudyFolderJson() {
    }

    public CreateCaseStudyFolderJson(String title, Integer order) {
        this.title = title;
        this.order = order;
    }

    public CaseStudyFolder toEntity(CaseStudy caseStudy){
        return new CaseStudyFolder(caseStudy, this.title, this.order == null ? 0 : this.order);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
}
