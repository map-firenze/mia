package org.medici.mia.common.json.message;


public class CountMessageJson {	
	
	private Integer messTotalCount;
	
	public Integer getMessTotalCount() {
		return messTotalCount;
	}
	public void setMessTotalCount(Integer messTotalCount) {
		this.messTotalCount = messTotalCount;
	}

}