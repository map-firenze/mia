package org.medici.mia.common.json.advancedsearch;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.medici.mia.common.util.DateUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@JsonTypeName("personVitalStatisticsBirthDeathAdvancedSearch")
public class PersonVitalStatisticsBirthDeathAdvancedSearchJson extends
		GenericAdvancedSearchJson {

	private static final long serialVersionUID = 1L;
	private static final String NOT_DELETED_PEOPLE = " AND  p.LOGICALDELETE != 1 ";
	private static final Logger LOGGER = Logger
			.getLogger(PersonVitalStatisticsBirthDeathAdvancedSearchJson.class
					.toString());

	String searchType;
	// birth date
	Integer byear;
	Integer bmonth;
	Integer bday;
	// deathDate
	Integer dyear;
	Integer dmonth;
	Integer dday;
	// deathPlace
	Integer birthPlaceId;
	// birthPlace
	Integer deathPlaceId;
	
	private String birthPlaceName;
	private String deathPlaceName;

	@JsonIgnore
	@Override
	public List<String> getQueries() {

		List<String> queries = new ArrayList<String>();
		StringBuffer b = new StringBuffer();
		if ("birthDate".equalsIgnoreCase(getSearchType())) {
			// query for birthdate
			Integer bornDate = DateUtils.getLuceneDate(getByear(), getBmonth(),
					getBday());
			b.append("select distinct p.PERSONID from tblPeople p where BORNDATE > '");
			b.append(bornDate + "'");
			b.append(NOT_DELETED_PEOPLE);

		} else if ("deathDate".equalsIgnoreCase(getSearchType())) {
			// query for dateDate
			Integer deathDate = DateUtils.getLuceneDate(getDyear(),
					getDmonth(), getDday());
			b.append("select distinct p.PERSONID from tblPeople p where DEATHDATE > '");
			b.append(deathDate + "'");
			b.append(NOT_DELETED_PEOPLE);
		} else if (isSearchByPlace(getSearchType())) {
			if (deathPlaceId != null && birthPlaceId != null) {
				b.append("select distinct p.PERSONID from tblPeople p where ( p.DPLACEID = ");
				b.append("'" + deathPlaceId + "'");
			    b.append(" AND " );
				b.append(" p.BPLACEID = ");
				b.append("'" + birthPlaceId + "'");
				b.append(" ) ");
			}else if (deathPlaceId != null && birthPlaceId == null) {
				b.append("select distinct p.PERSONID from tblPeople p where ( p.DPLACEID = ");
				b.append("'" + deathPlaceId + "'");
				b.append(" ) ");
			}else if (deathPlaceId == null && birthPlaceId != null) {
				b.append("select distinct p.PERSONID from tblPeople p where ( p.BPLACEID = ");
				b.append("'" + birthPlaceId + "'");
				b.append(" ) ");
			}
			b.append(NOT_DELETED_PEOPLE);

		}
		LOGGER.info(b.toString());
		queries.add(b.toString());
		return queries;
	}
	
	private boolean isSearchByPlace(String searchType) {
		if (searchType.trim().equalsIgnoreCase("birthAndDeathPlace")) {
			return true;
		}
		if (searchType.trim().equalsIgnoreCase("birthPlace")) {
			return true;
		}
		if (searchType.trim().equalsIgnoreCase("deathPlace")) {
			return true;
		}
		
		return false;
	}
	
	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public Integer getByear() {
		return byear;
	}

	public void setByear(Integer byear) {
		this.byear = byear;
	}

	public Integer getBmonth() {
		return bmonth;
	}

	public void setBmonth(Integer bmonth) {
		this.bmonth = bmonth;
	}

	public Integer getBday() {
		return bday;
	}

	public void setBday(Integer bday) {
		this.bday = bday;
	}

	public Integer getDyear() {
		return dyear;
	}

	public void setDyear(Integer dyear) {
		this.dyear = dyear;
	}

	public Integer getDmonth() {
		return dmonth;
	}

	public void setDmonth(Integer dmonth) {
		this.dmonth = dmonth;
	}

	public Integer getDday() {
		return dday;
	}

	public void setDday(Integer dday) {
		this.dday = dday;
	}

	public Integer getBirthPlaceId() {
		return birthPlaceId;
	}

	public void setBirthPlaceId(Integer bPlace) {
		this.birthPlaceId = bPlace;
	}

	public Integer getDeathPlaceId() {
		return deathPlaceId;
	}

	public void setDeathPlaceId(Integer dPlace) {
		this.deathPlaceId = dPlace;
	}

	String getBirthPlaceName() {
		return birthPlaceName;
	}

	void setBirthPlaceName(String birthPlaceName) {
		this.birthPlaceName = birthPlaceName;
	}

	String getDeathPlaceName() {
		return deathPlaceName;
	}

	void setDeathPlaceName(String deathPlaceName) {
		this.deathPlaceName = deathPlaceName;
	}
}