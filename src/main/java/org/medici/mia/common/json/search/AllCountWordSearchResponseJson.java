package org.medici.mia.common.json.search;

import java.io.Serializable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class AllCountWordSearchResponseJson implements Serializable {

	private static final long serialVersionUID = 3787731742866353474L;

	Integer archivalEntities;
	Integer transcriptions;
	Integer synopses;
	Integer people;
	Integer places;

	public Integer getArchivalEntities() {
		return archivalEntities;
	}

	public void setArchivalEntities(Integer archivalEntities) {
		this.archivalEntities = archivalEntities;
	}

	public Integer getTranscriptions() {
		return transcriptions;
	}

	public void setTranscriptions(Integer transcriptions) {
		this.transcriptions = transcriptions;
	}

	public Integer getSynopses() {
		return synopses;
	}

	public void setSynopses(Integer synopses) {
		this.synopses = synopses;
	}

	public Integer getPeople() {
		return people;
	}

	public void setPeople(Integer people) {
		this.people = people;
	}

	public Integer getPlaces() {
		return places;
	}

	public void setPlaces(Integer places) {
		this.places = places;
	}

}
