package org.medici.mia.common.json.document;

import org.medici.mia.service.miadoc.DocumentType;

public class DocumentFactoryJson {
	public static DocumentJson getDocument(String category) {
		if (DocumentType.correspondence.toString().equalsIgnoreCase(category)) {
			return new CorrespondenceJson();
		} else if (DocumentType.notarialRecords.toString().equalsIgnoreCase(category)) {
			return new NotarialJson();
		} else if(DocumentType.officialRecords.toString().equalsIgnoreCase(category)){
			return new OfficialJson();
		} else if(DocumentType.inventories.toString().equalsIgnoreCase(category)){
			return new InventoryJson();
		} else if(DocumentType.fiscalRecords.toString().equalsIgnoreCase(category)){
			return new FiscalJson();
		} else if(DocumentType.financialRecords.toString().equalsIgnoreCase(category)){
			return new FinancialJson();
		} else if(DocumentType.miscellaneous.toString().equalsIgnoreCase(category)){
			return new MiscellaneousJson();
		} else if(DocumentType.literaryWorks.toString().equalsIgnoreCase(category)){
			return new LiteraryJson();
		} else if(DocumentType.news.toString().equalsIgnoreCase(category)){
			return new NewsJson();
		}

		return new GenericDocumentJson();
	}

}
