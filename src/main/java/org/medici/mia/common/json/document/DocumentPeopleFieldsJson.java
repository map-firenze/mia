package org.medici.mia.common.json.document;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.domain.MiaDocumentEntity;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public class DocumentPeopleFieldsJson extends DocumentPeopleBaseJson {

	private static final long serialVersionUID = 1L;

	private List<DocPeopleFieldJson> fields;

	public DocumentPeopleFieldsJson() {

	}

	public List<DocPeopleFieldJson> getFields() {
		if (this.fields == null) {
			this.fields = new ArrayList<DocPeopleFieldJson>();
		}
		return this.fields;
	}

	public void setFields(List<DocPeopleFieldJson> fields) {
		this.fields = fields;
	}

	public void toJson(MiaDocumentEntity docEnt, List<DocPeopleFieldJson> fileds) {
		if (docEnt == null)
			return;
		if (docEnt instanceof MiaDocumentEntity) {

			super.toJson(docEnt);
			this.setFields(fileds);

		}
	}

}
