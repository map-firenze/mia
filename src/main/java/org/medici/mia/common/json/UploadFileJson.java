package org.medici.mia.common.json;

import java.io.Serializable;
import java.util.List;

import org.medici.mia.common.json.folio.FolioJson;

public class UploadFileJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer uploadFileId;

	private Integer imageOrder;

	private Integer documentId;

	private String filePath;

	private Integer uploadInfoId;

	private String inserName;

	private String volumeName;

	private String collectionName;

	private String seriesName;
	
	private List<FolioJson> folios;
	
	private Boolean canView;
	
	private String uploadInfoOwner;

	private String fileName;
	
	public Boolean getCanView() {
		return canView;
	}

	public void setCanView(Boolean canView) {
		this.canView = canView;
	}

	public Integer getUploadFileId() {
		return uploadFileId;
	}

	public void setUploadFileId(Integer uploadFileId) {
		this.uploadFileId = uploadFileId;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public List<FolioJson> getFolios() {
		return folios;
	}

	public void setFolios(List<FolioJson> folios) {
		this.folios = folios;
	}

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	public Integer getUploadInfoId() {
		return uploadInfoId;
	}

	public void setUploadInfoId(Integer uploadInfoId) {
		this.uploadInfoId = uploadInfoId;
	}

	public Integer getImageOrder() {
		return imageOrder;
	}

	public void setImageOrder(Integer imageOrder) {
		this.imageOrder = imageOrder;
	}

	public String getInserName() {
		return inserName;
	}

	public void setInserName(String inserName) {
		this.inserName = inserName;
	}

	public String getVolumeName() {
		return volumeName;
	}

	public void setVolumeName(String volumeName) {
		this.volumeName = volumeName;
	}

	public String getCollectionName() {
		return collectionName;
	}

	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}

	public String getSeriesName() {
		return seriesName;
	}

	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}
	
	public String getUploadInfoOwner() {
		return uploadInfoOwner;
	}
	
	public void setUploadInfoOwner(String uploadInfoOwner) {
		this.uploadInfoOwner = uploadInfoOwner;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
