package org.medici.mia.common.json;

public class AddNewPlaceResponseJson extends GenericResponseJson {
	
	private static final long serialVersionUID = 1L;

	private Integer placeId;
	private String path;

	public Integer getPlaceId() {
		return placeId;
	}

	public void setPlaceId(Integer placeId) {
		this.placeId = placeId;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}