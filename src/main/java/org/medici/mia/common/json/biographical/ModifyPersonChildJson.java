package org.medici.mia.common.json.biographical;

import java.io.Serializable;

public class ModifyPersonChildJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer personId;

	private Integer newChildId;
	private Integer oldChildId;
	private Integer childToBeDeleted;

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public Integer getNewChildId() {
		return newChildId;
	}

	public void setNewChildId(Integer newChildId) {
		this.newChildId = newChildId;
	}

	public Integer getOldChildId() {
		return oldChildId;
	}

	public void setOldChildId(Integer oldChildId) {
		this.oldChildId = oldChildId;
	}

	public Integer getChildToBeDeleted() {
		return childToBeDeleted;
	}

	public void setChildToBeDeleted(Integer childToBeDeleted) {
		this.childToBeDeleted = childToBeDeleted;
	}

}