/*
 * MyNewsfeedItemJson.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.common.json.news;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import org.medici.mia.common.json.advancedsearch.GenericAdvancedSearchJson;
import org.medici.mia.domain.UserPreference;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author mbarlotti
 *
 */
public class MyNewsfeedItemJson implements Serializable {

	private static final long serialVersionUID = 4770543822176715816L;

	private Integer id;

	private String title;

	private List<GenericAdvancedSearchJson> searchFilter;

	private Integer matchingDocuments;

	public MyNewsfeedItemJson toJson(UserPreference entity)
			throws JsonParseException, JsonMappingException, IOException {
		if (entity == null) {
			return null;
		} else {
			this.setId(entity.getId());
			this.setTitle(entity.getTitle());
			this.setSearchFilter(new ObjectMapper().readValue(entity.getSerializedData(),
					new TypeReference<List<GenericAdvancedSearchJson>>() {
					}));
			return this;
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<GenericAdvancedSearchJson> getSearchFilter() {
		return searchFilter;
	}

	public void setSearchFilter(List<GenericAdvancedSearchJson> searchFilter) {
		this.searchFilter = searchFilter;
	}

	public Integer getMatchingDocuments() {
		return matchingDocuments;
	}

	public void setMatchingDocuments(Integer matchingDocuments) {
		this.matchingDocuments = matchingDocuments;
	}

}
