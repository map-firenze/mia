package org.medici.mia.common.json.advancedsearch;

import java.io.Serializable;

public class PeopleCategory implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer peopleId;
	private String category;
	private String field;

	public Integer getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(Integer peopleId) {
		this.peopleId = peopleId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

}
