
package org.medici.mia.common.json.biographical;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class SpouseJson {

	private Integer personId;
	private String spouseType;

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public String getSpouseType() {
		return spouseType;
	}

	public void setSpouseType(String spouseType) {
		this.spouseType = spouseType;
	}

}
