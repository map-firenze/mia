package org.medici.mia.common.json.document;

import org.medici.mia.common.json.TopicPlaceBaseJson;
import org.medici.mia.domain.DocumentTranscriptionEntity;
import org.medici.mia.domain.IGenericEntity;
import org.medici.mia.domain.Place;
import org.medici.mia.domain.TopicPlaceEntity;

public class TopicPlaceJson extends TopicPlaceBaseJson implements
		IModifyDocJson {

	private static final long serialVersionUID = 1L;

	// private Integer documentId;
	private String documentId;
	private Integer id;
	private String topicName;
	private String topicPlaceName;

	public TopicPlaceJson() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public String getTopicPlaceName() {
		return topicPlaceName;
	}

	public void setTopicPlaceName(String topicPlaceName) {
		this.topicPlaceName = topicPlaceName;
	}

	@Override
	public void toJson(IGenericEntity entity) {
		if (entity == null)
			return;
		if (entity instanceof TopicPlaceEntity) {
			TopicPlaceEntity topicEentity = (TopicPlaceEntity) entity;
			if (topicEentity.getDocumentEntityId() != null)
				this.setDocumentId(String.valueOf(topicEentity
						.getDocumentEntityId()));
			this.setPlaceId(topicEentity.getPlaceId());
			this.setTopicListId(topicEentity.getTopicListId());
			if (topicEentity.getPlaceEntity() != null) {
				this.setPrefFlag(topicEentity.getPlaceEntity().getPrefFlag());
				this.setUnsure(topicEentity.getPlaceEntity().getUnsure());
				this.setTopicPlaceName(topicEentity.getPlaceEntity().getPlaceNameFull());
			}
			
			if (topicEentity.getTopicListEntity() != null) {
				this.setTopicName(topicEentity.getTopicListEntity().getTopicTitle());
				
			}

		} else if (entity instanceof Place) {
			Place peopleEentity = (Place) entity;
			this.setPlaceId(peopleEentity.getPlaceAllId());
			this.setPrefFlag(peopleEentity.getPrefFlag());
			this.setUnsure(peopleEentity.getUnsure());
		}

	}

	@Override
	public IGenericEntity toEntity(IGenericEntity entity) {

		if (entity == null)
			entity = new DocumentTranscriptionEntity();

		if (this.getDocumentId() != null && !this.getDocumentId().isEmpty()) {
			((TopicPlaceEntity) entity).setDocumentEntityId(Integer
					.valueOf(this.getDocumentId()));
		}

		((TopicPlaceEntity) entity).setPlaceId(this.getPlaceId());
		((TopicPlaceEntity) entity).setTopicListId(this.getTopicListId());

		return (TopicPlaceEntity) entity;

	}
}
