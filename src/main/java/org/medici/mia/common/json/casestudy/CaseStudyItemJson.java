package org.medici.mia.common.json.casestudy;

import org.medici.mia.domain.CaseStudyItem;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.HashMap;

public class CaseStudyItemJson implements Serializable {
    private Integer id;
    private Integer folder_id;
    private String title;
    private String entityType;
    private Object entity;
    private String createdBy;
    private String createdAt;
    private Integer order;

    public static CaseStudyItemJson toJson(CaseStudyItem item, Object entity){
        SimpleDateFormat sdf = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        CaseStudyItemJson json = new CaseStudyItemJson();
        json.setId(item.getId());
        json.setTitle(item.getTitle());
        json.setFolder_id(item.getFolder() == null ? null : item.getFolder().getId());
        json.setEntityType(item.getEntityType().toString());
        json.setCreatedBy(item.getCreatedBy() == null ? null : item.getCreatedBy().getAccount());
        json.setCreatedAt(item.getCreatedAt() == null ? null : sdf.format(item.getCreatedAt()));
        json.setOrder(item.getOrder());
        json.setEntity(entity);
        return json;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFolder_id() {
        return folder_id;
    }

    public void setFolder_id(Integer folder_id) {
        this.folder_id = folder_id;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public Object getEntity() {
        return entity;
    }

    public void setEntity(Object entity) {
        this.entity = entity;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
