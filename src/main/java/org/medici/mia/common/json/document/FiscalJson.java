package org.medici.mia.common.json.document;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.common.json.FieldJson;
import org.medici.mia.common.json.PlaceJson;
import org.medici.mia.domain.DocumentFieldEntity;
import org.medici.mia.domain.FiscalEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.Place;
import org.medici.mia.service.miadoc.DocumentType;
import org.medici.mia.service.miadoc.MiaDocumentUtils;
import org.medici.mia.service.miadoc.PlaceFieldType;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@JsonTypeName("fiscalRecords")
public class FiscalJson extends GenericDocumentJson {

	private static final long serialVersionUID = 1L;

	@JsonProperty("gonfaloneDistrict")
	private List<PlaceJson> gonfaloneDistricts;

	@JsonProperty("taxpayersOccupation")
	private List<PlaceJson> taxpayersOccupations;

	@JsonProperty("placeofOccupation")
	private List<PlaceJson> placeofOccupations;

	public FiscalJson() {
	}

	public List<PlaceJson> getGonfaloneDistricts() {
		return gonfaloneDistricts;
	}

	public void setGonfaloneDistricts(List<PlaceJson> gonfaloneDistricts) {
		this.gonfaloneDistricts = gonfaloneDistricts;
	}

	public List<PlaceJson> getTaxpayersOccupations() {
		return taxpayersOccupations;
	}

	public void setTaxpayersOccupations(List<PlaceJson> taxpayersOccupations) {
		this.taxpayersOccupations = taxpayersOccupations;
	}

	public List<PlaceJson> getPlaceofOccupations() {
		return placeofOccupations;
	}

	public void setPlaceofOccupations(List<PlaceJson> placeofOccupations) {
		this.placeofOccupations = placeofOccupations;
	}

	@Override
	public void init(MiaDocumentEntity docEntity) {
		super.init(docEntity);
		if (docEntity.getFiscal() != null) {

			this.gonfaloneDistricts = getPlaces(docEntity.getFiscal()
					.getGonfaloneDistricts());
			this.taxpayersOccupations = getPlaces(docEntity.getFiscal()
					.getTaxpayersOccupations());
			this.taxpayersOccupations = getPlaces(docEntity.getFiscal()
					.getPlaceofOccupations());

		}
	}

	// Used for finding place attached to document
	@Override
	public List<FindPlaceForDocJson> findPlaceJson(MiaDocumentEntity docEntity) {
		List<FindPlaceForDocJson> modifyPlaces = new ArrayList<FindPlaceForDocJson>();

		FindPlaceForDocJson modifyPlaceGonf = new FindPlaceForDocJson();
		modifyPlaceGonf.setType(PlaceFieldType.gonfaloneDistrict.toString());

		if (docEntity.getFiscal() != null) {
			modifyPlaceGonf.setPlaces(MiaDocumentUtils.getPlaceBases(docEntity
					.getFiscal().getGonfaloneDistrict()));
		}
		modifyPlaces.add(modifyPlaceGonf);
		
		FindPlaceForDocJson occModifyPlace = new FindPlaceForDocJson();
		occModifyPlace.setType(PlaceFieldType.placeofOccupation.toString());

		if (docEntity.getFiscal() != null) {
			occModifyPlace.setPlaces(MiaDocumentUtils.getPlaceBases(docEntity
					.getFiscal().getPlaceofOccupation()));
		}
		
		modifyPlaces.add(occModifyPlace);
		
		FindPlaceForDocJson modifyPlaceTax = new FindPlaceForDocJson();
		modifyPlaceTax.setType(PlaceFieldType.taxpayersOccupation.toString());

		if (docEntity.getFiscal() != null) {
			modifyPlaceTax.setPlaces(MiaDocumentUtils.getPlaceBases(docEntity
					.getFiscal().getTaxpayersOccupation()));
		}
		modifyPlaces.add(modifyPlaceTax);
		
		
		return modifyPlaces;
	}

	// Used for modify - add - delete place attached to document
	@Override
	public MiaDocumentEntity getModifiedDocumentEnt(MiaDocumentEntity docEnt,
			ModifyPlaceForDocJson modifyPlace) {

		if (modifyPlace.getType() == null) {
			docEnt.setErrorMsg("The type has no value)");
			return docEnt;
		}

		if (docEnt.getFiscal() == null) {
			docEnt.setErrorMsg("There is no " + docEnt.getCategory()
					+ " record associated with the document.");
			return docEnt;
		}

		if (PlaceFieldType.gonfaloneDistrict.toString().equalsIgnoreCase(
				modifyPlace.getType())) {

			String modifedStr = MiaDocumentUtils.modifyPlaceForDoc(docEnt
					.getFiscal().getGonfaloneDistrict(), modifyPlace);
			docEnt.getFiscal().setGonfaloneDistrict(modifedStr);

		} else if (PlaceFieldType.placeofOccupation.toString().equalsIgnoreCase(
				modifyPlace.getType())) {

			String modifedStr = MiaDocumentUtils.modifyPlaceForDoc(docEnt
					.getFiscal().getPlaceofOccupation(), modifyPlace);
			docEnt.getFiscal().setPlaceofOccupation(modifedStr);

		}else if (PlaceFieldType.taxpayersOccupation.toString().equalsIgnoreCase(
				modifyPlace.getType())) {

			String modifedStr = MiaDocumentUtils.modifyPlaceForDoc(docEnt
					.getFiscal().getTaxpayersOccupation(), modifyPlace);
			docEnt.getFiscal().setTaxpayersOccupation(modifedStr);

		}else {
			docEnt.setErrorMsg("The type " + modifyPlace.getType()
					+ " is not compatible with the category "
					+ docEnt.getCategory());
		}

		return docEnt;

	}

	@Override
	public MiaDocumentEntity getMiaDocumentEntity(DocumentJson docJson) {

		MiaDocumentEntity docEnt = super.getMiaDocumentEntity(docJson);

		docEnt.setCategory(DocumentType.fiscalRecords.toString());
		FiscalJson fiscJson = (FiscalJson) docJson;
		FiscalEntity fiscEnt = new FiscalEntity();

		if (fiscJson.getGonfaloneDistricts() != null
				&& !fiscJson.getGonfaloneDistricts().isEmpty()) {
			String gonfStr = "";
			for (PlaceJson place : fiscJson.getGonfaloneDistricts()) {
				gonfStr = gonfStr + place.getId() + ":";
				if (!StringUtils.isEmpty(place.getUnsure())) {
					gonfStr = gonfStr + place.getUnsure();
				} else {
					gonfStr = gonfStr + "S";
				}
				gonfStr = gonfStr + ",";
			}

			fiscEnt.setGonfaloneDistrict(gonfStr);
		}

		if (fiscJson.getTaxpayersOccupations() != null
				&& !fiscJson.getTaxpayersOccupations().isEmpty()) {
			String taxStr = "";
			for (PlaceJson place : fiscJson.getTaxpayersOccupations()) {
				taxStr = taxStr + place.getId() + ":";
				if (!StringUtils.isEmpty(place.getUnsure())) {
					taxStr = taxStr + place.getUnsure();
				} else {
					taxStr = taxStr + "S";
				}
				taxStr = taxStr + ",";
			}

			fiscEnt.setTaxpayersOccupation(taxStr);
		}

		if (fiscJson.getPlaceofOccupations() != null
				&& !fiscJson.getPlaceofOccupations().isEmpty()) {
			String pStr = "";
			for (PlaceJson place : fiscJson.getPlaceofOccupations()) {
				pStr = pStr + place.getId() + ":";
				if (!StringUtils.isEmpty(place.getUnsure())) {
					pStr = pStr + place.getUnsure();
				} else {
					pStr = pStr + "S";
				}
				pStr = pStr + ",";
			}

			fiscEnt.setPlaceofOccupation(pStr);
		}

		docEnt.setFiscal(fiscEnt);

		return docEnt;
	}

	@Override
	public List<FieldJson> getJsonFields(
			List<DocumentFieldEntity> documentFields) {

		List<FieldJson> fields = new ArrayList<FieldJson>();
		for (DocumentFieldEntity field : documentFields) {
			if (DocumentType.fiscalRecords.toString().equalsIgnoreCase(
					field.getDocumentCategory())) {
				boolean isMultiple = false;
				if ("1".equalsIgnoreCase(field.getAllowMultiple())) {
					isMultiple = true;
				}
				FieldJson fieldJson = new FieldJson(field.getFieldName(),
						field.getFieldBeName(), field.getFieldValue(),
						field.getFieldType(), isMultiple);

				fields.add(fieldJson);

			}
		}

		return fields;
	}

	private ArrayList<PlaceJson> getPlaces(List<Place> places) {

		ArrayList<PlaceJson> placeJsons = new ArrayList<PlaceJson>();

		if (places != null && !places.isEmpty()) {
			for (Place place : places) {

				PlaceJson placeJson = new PlaceJson();
				placeJson.setId(place.getPlaceAllId());
				placeJson.setPlaceNameId(place.getPlaceNameId());
				placeJson.setPlaceName(place.getPlaceNameFull());
				placeJson.setPrefFlag(place.getPrefFlag());
				placeJson.setPlType(place.getPlType());
				placeJson.setUnsure(place.getUnsure());
				placeJsons.add(placeJson);
			}
		}

		return placeJsons;

	}

}
