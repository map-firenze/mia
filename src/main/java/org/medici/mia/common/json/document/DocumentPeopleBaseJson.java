package org.medici.mia.common.json.document;

import java.io.Serializable;

import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.Place;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public class DocumentPeopleBaseJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer documentId;
	private String category;
	private String typology;
	private String deTitle;
	private String dateCreated;
	private String dateLastUpdate;

	public DocumentPeopleBaseJson() {

	}

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getTypology() {
		return typology;
	}

	public void setTypology(String typology) {
		this.typology = typology;
	}

	public String getDeTitle() {
		return deTitle;
	}

	public void setDeTitle(String deTitle) {
		this.deTitle = deTitle;
	}

	public String getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getDateLastUpdate() {
		return dateLastUpdate;
	}

	public void setDateLastUpdate(String dateLastUpdate) {
		this.dateLastUpdate = dateLastUpdate;
	}
	
	public void toJson(MiaDocumentEntity docEnt) {
		if (docEnt == null)
			return;
		if (docEnt instanceof MiaDocumentEntity) {
			
			this.setCategory(docEnt.getCategory());
			this.setTypology(docEnt.getTypology());
			this.setDocumentId(docEnt.getDocumentEntityId());
			this.setDeTitle(docEnt.getDeTitle());
			if (docEnt.getDateCreated() != null) {
				this.setDateCreated(docEnt.getDateCreated()
						.toString());
			}
			if (docEnt.getDateLastUpdate() != null) {
				this.setDateLastUpdate(docEnt
						.getDateLastUpdate().toString());
			}

			
		}

	}

}
