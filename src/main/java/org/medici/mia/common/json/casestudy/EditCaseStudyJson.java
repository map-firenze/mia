package org.medici.mia.common.json.casestudy;

import java.io.Serializable;

public class EditCaseStudyJson implements Serializable {

    private static final long serialVersionUID = 7554781266040557099L;

    private String title;
    private String description;
    private String notes;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
