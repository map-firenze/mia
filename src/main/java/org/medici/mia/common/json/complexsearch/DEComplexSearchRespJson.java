package org.medici.mia.common.json.complexsearch;

import java.io.Serializable;
import java.util.List;

import org.medici.mia.common.json.document.DocumentJson;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class DEComplexSearchRespJson implements Serializable {

	private static final long serialVersionUID = 3787731742866353474L;

	private List<DocumentJson> docs;
	private int count;

	public List<DocumentJson> getDocs() {
		return docs;
	}

	public void setDocs(List<DocumentJson> docs) {
		this.docs = docs;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
