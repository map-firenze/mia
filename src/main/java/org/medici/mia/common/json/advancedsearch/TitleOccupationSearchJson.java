package org.medici.mia.common.json.advancedsearch;

import java.io.Serializable;


public class TitleOccupationSearchJson implements Serializable {
	
	private static final long serialVersionUID = -3812321516658275242L;
	
	private Integer roleCateId;
	private String roleCatMinor;
	private String roleCatMajor;

	
	public Integer getRoleCateId() {
		return roleCateId;
	}
	
	public void setRoleCateId(Integer roleCateId) {
		this.roleCateId = roleCateId;
	}

	public String getRoleCatMinor() {
		return roleCatMinor;
	}

	public void setRoleCatMinor(String roleCatMinor) {
		this.roleCatMinor = roleCatMinor;
	}

	public String getRoleCatMajor() {
		return roleCatMajor;
	}

	public void setRoleCatMajor(String roleCatMajor) {
		this.roleCatMajor = roleCatMajor;
	}
}