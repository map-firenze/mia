package org.medici.mia.common.json.message;

import java.util.List;

public class FindInboxMessageJson extends CountMessageJson {

	private List<MessageInboxJson> messages;

	public List<MessageInboxJson> getMessages() {
		return messages;
	}

	public void setMessages(List<MessageInboxJson> messages) {
		this.messages = messages;
	}

}