package org.medici.mia.common.json;

import java.io.Serializable;
import java.util.List;

import org.medici.mia.common.json.biographical.AltNameJson;

public class PersonJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer personId;
	private List<AltNameJson> personNames;	

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public List<AltNameJson> getPersonNames() {
		return personNames;
	}

	public void setPersonNames(List<AltNameJson> personNames) {
		this.personNames = personNames;
	}

}