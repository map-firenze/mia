package org.medici.mia.common.json.biographical;

import java.io.Serializable;

public class AddHeadquarterJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer personId;
	private Integer placeId;
	private Integer year;
	public Integer getPersonId() {
		return personId;
	}
	public void setPersonId(Integer personId) {
		this.personId = personId;
	}
	public Integer getPlaceId() {
		return placeId;
	}
	public void setPlaceId(Integer placeId) {
		this.placeId = placeId;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}

	
}