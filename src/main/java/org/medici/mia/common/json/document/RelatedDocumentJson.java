package org.medici.mia.common.json.document;

import java.io.Serializable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class RelatedDocumentJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer documentId;
	private String title;

	public RelatedDocumentJson() {
		super();
	}

	public RelatedDocumentJson(Integer documentId, String title) {
		super();
		this.documentId = documentId;
		this.title = title;
	}

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Integer documentId) {
		this.documentId = documentId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
