package org.medici.mia.common.json.complexsearch;

import java.io.Serializable;
import java.util.List;

import org.medici.mia.controller.archive.MyArchiveBean;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class AEComplexSearchResponseJson implements Serializable {

	private static final long serialVersionUID = 3787731742866353474L;

	private Integer count;
	private List<MyArchiveBean> aentities;

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public List<MyArchiveBean> getAentities() {
		return aentities;
	}

	public void setAentities(List<MyArchiveBean> aentities) {
		this.aentities = aentities;
	}

}
