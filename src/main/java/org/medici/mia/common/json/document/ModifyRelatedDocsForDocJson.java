/*
 * DocumentTranscriptionJson.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.common.json.document;

import java.io.Serializable;

public class ModifyRelatedDocsForDocJson implements Serializable {

	private static final long serialVersionUID = 3725099586315739138L;

	private String documentId;
	private Integer newRelatedDoc;
	private Integer oldRelatedDoc;
	private Integer relatedDocToBeDeleted;

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public Integer getNewRelatedDoc() {
		return newRelatedDoc;
	}

	public void setNewRelatedDoc(Integer newRelatedDoc) {
		this.newRelatedDoc = newRelatedDoc;
	}

	public Integer getOldRelatedDoc() {
		return oldRelatedDoc;
	}

	public void setOldRelatedDoc(Integer oldRelatedDoc) {
		this.oldRelatedDoc = oldRelatedDoc;
	}

	public Integer getRelatedDocToBeDeleted() {
		return relatedDocToBeDeleted;
	}

	public void setRelatedDocToBeDeleted(Integer relatedDocToBeDeleted) {
		this.relatedDocToBeDeleted = relatedDocToBeDeleted;
	}

}
