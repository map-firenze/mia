package org.medici.mia.common.json.biographical;

import java.io.Serializable;

import org.medici.mia.domain.BiographicalPeople;

public class PersonPortraitJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer personId;
	private String portraitPath;
	private String portraitAuthor;
	private String portraitSubject;

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public String getPortraitPath() {
		return portraitPath;
	}

	public void setPortraitPath(String portraitPath) {
		this.portraitPath = portraitPath;
	}

	public String getPortraitAuthor() {
		return portraitAuthor;
	}

	public void setPortraitAuthor(String portraitAuthor) {
		this.portraitAuthor = portraitAuthor;
	}

	public String getPortraitSubject() {
		return portraitSubject;
	}

	public void setPortraitSubject(String portraitSubject) {
		this.portraitSubject = portraitSubject;
	}

	public void toJson(BiographicalPeople entity) {

		if (entity == null) {
			return;
		}

		this.setPersonId(entity.getPersonId());
		this.setPortraitAuthor(entity.getPortraitAuthor());
		this.setPortraitPath("src/peoplebase/ShowPortraitPerson.do?personId="
				+ entity.getPersonId());
		this.setPortraitSubject(entity.getPortraitSubject());

	}

}