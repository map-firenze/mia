/*
 * DocumentTranscriptionJson.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.common.json.document;

import org.medici.mia.domain.IGenericEntity;
import org.medici.mia.domain.MiaDocumentEntity;

/**
 * JSON implementation for store document's transcriptions in the
 * tblDocTranscriptions database table.
 * 
 * 
 */
public class BasicDescriptionJson implements IModifyDocJson {

	private static final long serialVersionUID = 3725099586315739138L;

	private String documentId;
	private String deTitle;
	private Integer docYear;
	private Integer docMonth;
	private Integer docDay;
	private Integer docModernYear;
	private Integer docDateUncertain;
	private Integer docUndated;
	private String docDateNotes;
	private String printed;
	private String typology;

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getDeTitle() {
		return deTitle;
	}

	public void setDeTitle(String deTitle) {
		this.deTitle = deTitle;
	}

	public Integer getDocYear() {
		return docYear;
	}

	public void setDocYear(Integer docYear) {
		this.docYear = docYear;
	}

	public Integer getDocMonth() {
		return docMonth;
	}

	public void setDocMonth(Integer docMonth) {
		this.docMonth = docMonth;
	}

	public Integer getDocDay() {
		return docDay;
	}

	public void setDocDay(Integer docDay) {
		this.docDay = docDay;
	}

	public Integer getDocModernYear() {
		return docModernYear;
	}

	public void setDocModernYear(Integer docModernYear) {
		this.docModernYear = docModernYear;
	}

	public Integer getDocDateUncertain() {
		return docDateUncertain;
	}

	public void setDocDateUncertain(Integer docDateUncertain) {
		this.docDateUncertain = docDateUncertain;
	}

	public Integer getDocUndated() {
		return docUndated;
	}

	public void setDocUndated(Integer docUndated) {
		this.docUndated = docUndated;
	}

	public String getDocDateNotes() {
		return docDateNotes;
	}

	public void setDocDateNotes(String docDateNotes) {
		this.docDateNotes = docDateNotes;
	}

	public String getPrinted() {
		return printed;
	}

	public void setPrinted(String printed) {
		this.printed = printed;
	}

	public String getTypology() {
		return typology;
	}

	public void setTypology(String typology) {
		this.typology = typology;
	}

	@Override
	public void toJson(IGenericEntity entity) {
		if (entity == null)
			return;
		if (entity instanceof MiaDocumentEntity) {
			MiaDocumentEntity basicEntity = (MiaDocumentEntity) entity;
			if (basicEntity.getDocumentEntityId() != null) {
				this.setDocumentId(String.valueOf(basicEntity
						.getDocumentEntityId()));
			}
			this.setDeTitle(basicEntity.getDeTitle());
			this.setDocYear(basicEntity.getDocYear());
			this.setDocMonth(basicEntity.getDocMonth());
			this.setDocDay(basicEntity.getDocDay());
			this.setDocModernYear(basicEntity.getDocModernYear());
			this.setDocDateUncertain(basicEntity.getDocDateUncertain());
			this.setDocUndated(basicEntity.getDocUndated());
			this.setDocDateNotes(basicEntity.getDocDateNotes());
			if (basicEntity.getFlgPrinted() == null
					|| basicEntity.getFlgPrinted() == 0) {
				this.setPrinted("No");
			} else {
				this.setPrinted("Si");
			}

			this.setTypology(basicEntity.getTypology());

		}

	}

	@Override
	public IGenericEntity toEntity(IGenericEntity entity) {

		if (entity == null)
			entity = new MiaDocumentEntity();

		if (entity instanceof MiaDocumentEntity) {

			if (this.getDocumentId() != null && !this.getDocumentId().isEmpty()) {
				((MiaDocumentEntity) entity).setDocumentEntityId(Integer
						.valueOf(this.getDocumentId()));
			}

			((MiaDocumentEntity) entity).setDeTitle(this.getDeTitle());
			((MiaDocumentEntity) entity).setDocYear(this.getDocYear());
			((MiaDocumentEntity) entity).setDocMonth(this.getDocMonth());
			((MiaDocumentEntity) entity).setDocDay(this.getDocDay());
			((MiaDocumentEntity) entity).setDocModernYear(this
					.getDocModernYear());
			((MiaDocumentEntity) entity).setDocDateUncertain(this
					.getDocDateUncertain());
			((MiaDocumentEntity) entity).setDocUndated(this.getDocUndated());
			((MiaDocumentEntity) entity)
					.setDocDateNotes(this.getDocDateNotes());
			if (this.getPrinted() == null || this.getPrinted().isEmpty()
					|| this.getPrinted().equalsIgnoreCase("No")) {
				((MiaDocumentEntity) entity).setFlgPrinted(0);
			} else {
				((MiaDocumentEntity) entity).setFlgPrinted(1);
			}

			((MiaDocumentEntity) entity).setTypology(this.getTypology());

			return (MiaDocumentEntity) entity;
		}

		return null;

	}

}
