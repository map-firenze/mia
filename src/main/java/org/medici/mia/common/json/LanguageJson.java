package org.medici.mia.common.json;

import java.io.Serializable;

public class LanguageJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String language;

	public LanguageJson() {
		super();
	}

	public LanguageJson(Integer id, String language) {
		super();
		this.id = id;
		this.language = language;
	}

	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

}
