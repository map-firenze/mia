package org.medici.mia.common.json.search;

import java.io.Serializable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public class GenericAESearchJson implements Serializable {

	private static final long serialVersionUID = 3787731742866353474L;

	private String owner = null;
	private boolean onlyMyArchivalEnties;
	private boolean everyoneElsesArchivalEntities;
	private boolean allArchivalEntities;
	private String searchString;
	private String searchType;

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public boolean isOnlyMyArchivalEnties() {
		return onlyMyArchivalEnties;
	}

	public void setOnlyMyArchivalEnties(boolean onlyMyArchivalEnties) {
		this.onlyMyArchivalEnties = onlyMyArchivalEnties;
	}

	public boolean isEveryoneElsesArchivalEntities() {
		return everyoneElsesArchivalEntities;
	}

	public void setEveryoneElsesArchivalEntities(
			boolean everyoneElsesArchivalEntities) {
		this.everyoneElsesArchivalEntities = everyoneElsesArchivalEntities;
	}

	public boolean isAllArchivalEntities() {
		return allArchivalEntities;
	}

	public void setAllArchivalEntities(boolean allArchivalEntities) {
		this.allArchivalEntities = allArchivalEntities;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

}
