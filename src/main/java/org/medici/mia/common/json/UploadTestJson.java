package org.medici.mia.common.json;

import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.sql.Blob;

import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

public class UploadTestJson implements Serializable {

	private final Logger logger = Logger.getLogger(this.getClass());

	private static final long serialVersionUID = 1L;

	private String imageString;

	 private byte[] imageByteArray;

	 private BufferedImage bufferedImage;

	 private MultipartFile imageMultipart;

	 private Blob imageBlob;

	public String getImageString() {
		return imageString;
	}

	public void setImageString(String imageString) {
		this.imageString = imageString;
	}

	public byte[] getImageByteArray() {
		return imageByteArray;
	}

	public void setImageByteArray(byte[] imageByteArray) {
		this.imageByteArray = imageByteArray;
	}

	public BufferedImage getBufferedImage() {
		return bufferedImage;
	}

	public void setBufferedImage(BufferedImage bufferedImage) {
		this.bufferedImage = bufferedImage;
	}

	public MultipartFile getImageMultipart() {
		return imageMultipart;
	}

	public void setImageMultipart(MultipartFile imageMultipart) {
		this.imageMultipart = imageMultipart;
	}

	public Blob getImageBlob() {
		return imageBlob;
	}

	public void setImageBlob(Blob imageBlob) {
		this.imageBlob = imageBlob;
	}

	

}