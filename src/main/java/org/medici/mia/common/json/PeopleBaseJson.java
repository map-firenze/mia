package org.medici.mia.common.json;

import java.io.Serializable;

import org.medici.mia.domain.DocumentRefToPeopleEntity;

public class PeopleBaseJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String unsure;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUnsure() {
		return unsure;
	}

	public void setUnsure(String unsure) {
		this.unsure = unsure;
	}

	public void toJson(DocumentRefToPeopleEntity entity) {
		if (entity == null)
			return;
		if (entity instanceof DocumentRefToPeopleEntity) {

			this.setId(entity.getPeopleId());
			this.setUnsure(entity.getUnSure());

		}

	}

}