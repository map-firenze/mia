package org.medici.mia.common.json.geographical;

import java.io.Serializable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public class PlaceNameVariantJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer placeAllId;
	private String placeName;
	private String prefFlag;

	public Integer getPlaceAllId() {
		return placeAllId;
	}

	public void setPlaceAllId(Integer placeAllId) {
		this.placeAllId = placeAllId;
	}

	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	public String getPrefFlag() {
		return prefFlag;
	}

	public void setPrefFlag(String prefFlag) {
		this.prefFlag = prefFlag;
	}

}