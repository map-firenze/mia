package org.medici.mia.common.json.biographical;

import java.io.Serializable;
import java.util.List;

public class FindPersonTitlesOccsJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer personId;
	private List<TitleOccJson> tileOcc;
	private Integer preferredRole;

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public List<TitleOccJson> getTileOcc() {
		return tileOcc;
	}

	public void setTileOcc(List<TitleOccJson> tileOcc) {
		this.tileOcc = tileOcc;
	}

	public Integer getPreferredRole() {
		return preferredRole;
	}

	public void setPreferredRole(Integer preferredRole) {
		this.preferredRole = preferredRole;
	}

}