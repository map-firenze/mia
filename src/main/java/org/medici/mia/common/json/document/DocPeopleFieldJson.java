package org.medici.mia.common.json.document;

import java.io.Serializable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public class DocPeopleFieldJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private String fieldName;
	private String fieldBeName;

	public DocPeopleFieldJson() {

	}

	public DocPeopleFieldJson(String fieldName, String fieldBeName) {
		super();
		this.fieldName = fieldName;
		this.fieldBeName = fieldBeName;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldBeName() {
		return fieldBeName;
	}

	public void setFieldBeName(String fieldBeName) {
		this.fieldBeName = fieldBeName;
	}

}
