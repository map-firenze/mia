package org.medici.mia.common.json.biographical;

import java.io.Serializable;

import org.medici.mia.domain.PoLink;
import org.medici.mia.domain.TitleOccsList;

public class TitleOccJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer titleOccId;
	private String titleOcc;
	private String roleCatMinor;
	private String roleCatMajor;
	private PoLinkJson poLink;

	public void toJson(PoLink polinkEntity) {

		if (polinkEntity == null)
			return;

		PoLinkJson poLinkJson = new PoLinkJson();
		poLinkJson.toJson(polinkEntity);
		this.setPoLink(poLinkJson);
		this.toJson(polinkEntity.getTitleOccList());

	}

	public void toJson(TitleOccsList entity) {

		if (entity == null)
			return;

		this.setTitleOccId(entity.getTitleOccId());
		this.setTitleOcc(entity.getTitleOcc());
		if (entity.getRoleCat() != null) {
			this.setRoleCatMajor(entity.getRoleCat().getRoleCatMajor());
			this.setRoleCatMinor(entity.getRoleCat().getRoleCatMinor());
		}

	}

	public Integer getTitleOccId() {
		return titleOccId;
	}

	public void setTitleOccId(Integer titleOccId) {
		this.titleOccId = titleOccId;
	}

	public String getTitleOcc() {
		return titleOcc;
	}

	public void setTitleOcc(String titleOcc) {
		this.titleOcc = titleOcc;
	}

	public String getRoleCatMinor() {
		return roleCatMinor;
	}

	public void setRoleCatMinor(String roleCatMinor) {
		this.roleCatMinor = roleCatMinor;
	}

	public String getRoleCatMajor() {
		return roleCatMajor;
	}

	public void setRoleCatMajor(String roleCatMajor) {
		this.roleCatMajor = roleCatMajor;
	}

	public PoLinkJson getPoLink() {
		return poLink;
	}

	public void setPoLink(PoLinkJson poLink) {
		this.poLink = poLink;
	}

}