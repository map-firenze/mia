package org.medici.mia.common.json.document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.medici.mia.common.json.FieldJson;
import org.medici.mia.common.json.PeopleBaseJson;
import org.medici.mia.common.json.PeopleJson;
import org.medici.mia.domain.DocumentFieldEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.NotarialEntity;
import org.medici.mia.domain.People;
import org.medici.mia.service.miadoc.DocumentType;
import org.medici.mia.service.miadoc.MiaDocumentUtils;
import org.medici.mia.service.miadoc.PeopleFieldType;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@JsonTypeName("notarialRecords")
public class NotarialJson extends GenericDocumentJson {

	private static final long serialVersionUID = 1L;

	@JsonProperty("contractor")
	private List<PeopleJson> contractors;

	@JsonProperty("contractee")
	private List<PeopleJson> contractees;

	public NotarialJson() {
	}

	public List<PeopleJson> getContractors() {
		return contractors;
	}

	public void setContractors(List<PeopleJson> contractors) {
		this.contractors = contractors;
	}

	public List<PeopleJson> getContractees() {
		return contractees;
	}

	public void setContractees(List<PeopleJson> contractees) {
		this.contractees = contractees;
	}

	@Override
	public void init(MiaDocumentEntity docEntity) {
		super.init(docEntity);
		if (docEntity.getNotarial() != null) {

			this.contractees = getPeoples(docEntity.getNotarial()
					.getContractees());
			this.contractors = getPeoples(docEntity.getNotarial()
					.getContractors());
		}
	}

	// Used for finding people attached to document
	@Override
	public List<FindPeopleForDocJson> findPeopleJson(MiaDocumentEntity docEntity) {
		List<FindPeopleForDocJson> modifyPeoples = new ArrayList<FindPeopleForDocJson>();

		FindPeopleForDocJson modifyPeopleContractees = new FindPeopleForDocJson();
		modifyPeopleContractees.setType(PeopleFieldType.Contractee.toString());
		ArrayList<PeopleBaseJson> peopleBasesContractee = null;

		if (docEntity.getNotarial() != null
				&& docEntity.getNotarial().getContractee() != null
				&& !docEntity.getNotarial().getContractee().isEmpty()) {

			peopleBasesContractee = new ArrayList<PeopleBaseJson>();

			for (String peopleIdStr : (List<String>) Arrays.asList(docEntity
					.getNotarial().getContractee().split(","))) {

				PeopleBaseJson peopleBase = new PeopleBaseJson();
				String[] peoplesIdArr = peopleIdStr.split(":");
				if (peoplesIdArr[0] != null || !peoplesIdArr[0].isEmpty()) {
					peopleBase.setId(Integer.valueOf(peoplesIdArr[0]));
				}
				if (peoplesIdArr.length > 1) {
					peopleBase.setUnsure(peoplesIdArr[1]);
				}

				peopleBasesContractee.add(peopleBase);
			}
		}
		modifyPeopleContractees.setPeoples(peopleBasesContractee);
		modifyPeoples.add(modifyPeopleContractees);

		// setting of contractors

		FindPeopleForDocJson modifyPeopleContractors = new FindPeopleForDocJson();
		modifyPeopleContractors.setType(PeopleFieldType.Contractor.toString());
		ArrayList<PeopleBaseJson> peopleBasesContractors = null;

		if (docEntity.getNotarial() != null
				&& docEntity.getNotarial().getContractor() != null
				&& !docEntity.getNotarial().getContractor().isEmpty()) {

			peopleBasesContractors = new ArrayList<PeopleBaseJson>();

			for (String peopleIdStr : (List<String>) Arrays.asList(docEntity
					.getNotarial().getContractor().split(","))) {

				PeopleBaseJson peopleBase = new PeopleBaseJson();
				String[] peoplesIdArr = peopleIdStr.split(":");
				if (peoplesIdArr[0] != null || !peoplesIdArr[0].isEmpty()) {
					peopleBase.setId(Integer.valueOf(peoplesIdArr[0]));
				}
				if (peoplesIdArr.length > 1) {
					peopleBase.setUnsure(peoplesIdArr[1]);
				}

				peopleBasesContractors.add(peopleBase);
			}
		}
		modifyPeopleContractors.setPeoples(peopleBasesContractors);
		modifyPeoples.add(modifyPeopleContractors);

		return modifyPeoples;
	}

	// Used for modify - add - delete people attached to document
	@Override
	public MiaDocumentEntity getModifiedDocumentEnt(MiaDocumentEntity docEnt,
			ModifyPeopleForDocJson modifyPeople) {

		String modifedStr = "";

		if (modifyPeople.getType() == null) {
			docEnt.setErrorMsg("The type has no value)");
			return docEnt;
		}

		if (docEnt.getNotarial() == null) {
			docEnt.setErrorMsg("There is no " + docEnt.getCategory()
					+ " record associated with the document.");
			return docEnt;
		}

		if (PeopleFieldType.Contractor.toString().equalsIgnoreCase(
				modifyPeople.getType())) {
			modifedStr = MiaDocumentUtils.modifyPeopleForDoc(docEnt
					.getNotarial().getContractor(), modifyPeople);
			docEnt.getNotarial().setContractor(modifedStr);

		} else if (PeopleFieldType.Contractee.toString().equalsIgnoreCase(
				modifyPeople.getType())) {

			modifedStr = MiaDocumentUtils.modifyPeopleForDoc(docEnt
					.getNotarial().getContractee(), modifyPeople);
			docEnt.getNotarial().setContractee(modifedStr);

		} else {
			docEnt.setErrorMsg("The type " + modifyPeople.getType()
					+ " is not compatible with the category "
					+ docEnt.getCategory());
		}

		return docEnt;

	}

	@Override
	public MiaDocumentEntity getMiaDocumentEntity(DocumentJson docJson) {

		MiaDocumentEntity docEnt = super.getMiaDocumentEntity(docJson);

		docEnt.setCategory(DocumentType.notarialRecords.toString());
		NotarialJson notarialJson = (NotarialJson) docJson;
		NotarialEntity notarialEnt = new NotarialEntity();
		if (notarialJson.getContractees() != null
				&& !notarialJson.getContractees().isEmpty()) {
			String contarcteeStr = "";
			for (PeopleJson people : notarialJson.getContractees()) {
				contarcteeStr = contarcteeStr + people.getId() + ":";
				if (!StringUtils.isEmpty(people.getUnsure())) {
					contarcteeStr = contarcteeStr + people.getUnsure();
				} else {
					contarcteeStr = contarcteeStr + "S";
				}
				contarcteeStr = contarcteeStr + ",";
			}

			notarialEnt.setContractee(contarcteeStr);

		}

		if (notarialJson.getContractors() != null
				&& !notarialJson.getContractors().isEmpty()) {
			String contarctorStr = "";
			for (PeopleJson people : notarialJson.getContractors()) {
				contarctorStr = contarctorStr + people.getId() + ":";
				if (!StringUtils.isEmpty(people.getUnsure())) {
					contarctorStr = contarctorStr + people.getUnsure();
				} else {
					contarctorStr = contarctorStr + "S";
				}
				contarctorStr = contarctorStr + ",";
			}

			notarialEnt.setContractor(contarctorStr);

		}

		docEnt.setNotarial(notarialEnt);

		return docEnt;
	}

	private ArrayList<PeopleJson> getPeoples(List<People> peoples) {

		ArrayList<PeopleJson> peopleJsons = new ArrayList<PeopleJson>();

		if (peoples != null && !peoples.isEmpty()) {
			for (People people : peoples) {

				PeopleJson peopleJson = new PeopleJson();
				peopleJson.setId(people.getPersonId());
				peopleJson.setGender(people.getGender());
				peopleJson.setMapNameLf(people.getMapNameLf());
				peopleJson.setActiveStart(people.getActiveStart());
				peopleJson.setActiveEnd(people.getActiveEnd());
				peopleJson.setBornYear(people.getBornYear());
				peopleJson.setDeathYear(people.getDeathYear());
				peopleJson.setFirstName(people.getFirst());
				peopleJson.setLastName(people.getLast());
				peopleJson.setUnsure(people.getUnsure());
				peopleJsons.add(peopleJson);
			}
		}

		return peopleJsons;

	}

	@Override
	public List<FieldJson> getJsonFields(
			List<DocumentFieldEntity> documentFields) {

		List<FieldJson> fields = new ArrayList<FieldJson>();
		for (DocumentFieldEntity field : documentFields) {
			if (DocumentType.notarialRecords.toString().equalsIgnoreCase(
					field.getDocumentCategory())) {
				boolean isMultiple = false;
				if ("1".equalsIgnoreCase(field.getAllowMultiple())) {
					isMultiple = true;
				}
				FieldJson fieldJson = new FieldJson(field.getFieldName(),
						field.getFieldBeName(), field.getFieldValue(),
						field.getFieldType(), isMultiple);

				fields.add(fieldJson);

			}
		}

		return fields;
	}

	@Override
	public List<DocPeopleFieldJson> getDocPeopleFields(Integer personId,
			MiaDocumentEntity docEnt, List<DocumentFieldEntity> documentFields) {

		List<DocPeopleFieldJson> fields = super.getDocPeopleFields(personId,
				docEnt, documentFields);

		if (docEnt.getNotarial() != null
				&& hasField(docEnt.getNotarial().getContractee(),
						String.valueOf(personId))) {
			DocPeopleFieldJson field = new DocPeopleFieldJson();
			field.setFieldBeName(PeopleFieldType.Contractee.toString());
			field.setFieldName(getFieldName(documentFields,
					docEnt.getCategory(), PeopleFieldType.Contractee.toString()));

			if (fields == null)
				fields = new ArrayList<DocPeopleFieldJson>();

			fields.add(field);
		}

		if (docEnt.getNotarial() != null
				&& hasField(docEnt.getNotarial().getContractor(),
						String.valueOf(personId))) {
			DocPeopleFieldJson field = new DocPeopleFieldJson();
			field.setFieldBeName(PeopleFieldType.Contractor.toString());
			field.setFieldName(getFieldName(documentFields,
					docEnt.getCategory(), PeopleFieldType.Contractor.toString()));

			if (fields == null)
				fields = new ArrayList<DocPeopleFieldJson>();

			fields.add(field);

		}

		return fields;

	}

}
