package org.medici.mia.common.json.biographical;

import java.io.Serializable;
import java.util.List;

import org.medici.mia.common.json.ParentJson;

public class FindPersonParentsJson implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer personId;
	private List<ParentJson> parents;

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public List<ParentJson> getParents() {
		return parents;
	}

	public void setParents(List<ParentJson> parents) {
		this.parents = parents;
	}

}