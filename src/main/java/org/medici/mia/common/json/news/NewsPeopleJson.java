package org.medici.mia.common.json.news;

import java.text.SimpleDateFormat;

import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.domain.People;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NewsPeopleJson extends NewsBaseJson {

	private static final long serialVersionUID = 1L;

	private final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	private String mapNameLf;
	private String action;
	private String modifiedBy;
	private Integer deathYear;
	private String deathMonth;
	private Integer deathDay;
	private Integer bornYear;
	private String bornMonth;
	private Integer bornDay;
	private String activeStart;
	private String activeEnd;

	public Integer getDeathYear() {
		return deathYear;
	}

	public void setDeathYear(Integer deathYear) {
		this.deathYear = deathYear;
	}

	public String getDeathMonth() {
		return deathMonth;
	}

	public void setDeathMonth(String deathMonth) {
		this.deathMonth = deathMonth;
	}

	public Integer getDeathDay() {
		return deathDay;
	}

	public void setDeathDay(Integer deathDay) {
		this.deathDay = deathDay;
	}

	public Integer getBornYear() {
		return bornYear;
	}

	public void setBornYear(Integer bornYear) {
		this.bornYear = bornYear;
	}

	public String getBornMonth() {
		return bornMonth;
	}

	public void setBornMonth(String bornMonth) {
		this.bornMonth = bornMonth;
	}

	public Integer getBornDay() {
		return bornDay;
	}

	public void setBornDay(Integer bornDay) {
		this.bornDay = bornDay;
	}

	public String getActiveStart() {
		return activeStart;
	}

	public void setActiveStart(String activeStart) {
		this.activeStart = activeStart;
	}

	public String getActiveEnd() {
		return activeEnd;
	}

	public void setActiveEnd(String activeEnd) {
		this.activeEnd = activeEnd;
	}



	// Added for the request of Lorenzo and for now is empty.
	private String portraitPath;

	private final String PORTRAIT_IMAGE_PATH = ApplicationPropertyManager
			.getApplicationProperty("portrait.person.path");
	private final static String OS_SLASH = org.medici.mia.common.util.FileUtils.OS_SLASH;

	public String getMapNameLf() {
		return mapNameLf;
	}

	public void setMapNameLf(String mapNameLf) {
		this.mapNameLf = mapNameLf;
	}

	public String getPortraitPath() {
		return portraitPath;
	}

	public void setPortraitPath(String portraitPath) {
		this.portraitPath = portraitPath;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public NewsPeopleJson toJson(People entity) {

		this.setType("people");
		this.setId(entity.getPersonId());
		this.setMapNameLf(entity.getMapNameLf());
		if (entity.getLastUpdateBy() != null) {
			this.setModifiedBy(entity.getLastUpdateBy().getAccount());
		}
		if (entity.getCreatedBy() != null) {
			this.setCreatedBy(entity.getCreatedBy().getAccount());
		}

		this.setPortraitPath(PORTRAIT_IMAGE_PATH + OS_SLASH
				+ entity.getPortraitImageName());

		if (entity.getLogicalDelete() != null && entity.getLogicalDelete()) {
			this.setAction(NewsActionType.DELETED.toString());
			this.setActivityDate(sdf.format(entity.getLastUpdate()));
			if (entity.getLastUpdateBy() != null) {
				this.setModifiedBy(entity.getLastUpdateBy().getAccount());
			}
		} else if (entity.getLastUpdate() == null) {
			this.setAction(NewsActionType.CREATED.toString());
			this.setActivityDate(sdf.format(entity.getDateCreated()));

		} else if (entity.getLastUpdate().after(entity.getDateCreated())) {
			this.setAction(NewsActionType.MODIFIED.toString());
			this.setActivityDate(sdf.format(entity.getLastUpdate()));
			if (entity.getLastUpdateBy() != null) {
				this.setModifiedBy(entity.getLastUpdateBy().getAccount());
			}
		} else {
			this.setAction(NewsActionType.CREATED.toString());
			this.setActivityDate(sdf.format(entity.getDateCreated()));
		}
		this.setBornDay(entity.getBornDay());
		if(entity.getBornMonth() != null){
			this.setBornMonth(entity.getBornMonth().toString());
		} else {
			this.setBornMonth(null);
		}
		this.setBornYear(entity.getBornYear());
		this.setDeathDay(entity.getDeathDay());
		if(entity.getDeathMonth() != null){
			this.setDeathMonth(entity.getDeathMonth().toString());
		} else {
			this.setDeathMonth(null);
		}
		this.setDeathYear(entity.getDeathYear());
		this.setActiveStart(entity.getActiveStart());
		this.setActiveEnd(entity.getActiveEnd());


		return this;
	}
}