/*
 * DocumentPrivacyUtils.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.common.util;

import java.util.List;


import org.medici.mia.domain.FileSharing;
import org.medici.mia.domain.User;
import org.medici.mia.domain.UserAuthority.Authority;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public class DocumentPrivacyUtils {

	private static final String ADMIN_AUTHORITY = UserRoleUtils.getRolePrefix() + Authority.ADMINISTRATORS;
	private static final String ONSITE_FELLOWS_AUTHORITY = UserRoleUtils.getRolePrefix() + Authority.ONSITE_FELLOWS;
	
	public enum PrivacyType {
		PUBLIC, PRIVATE, PRIVATE_SHARED
	}
	
	private DocumentPrivacyUtils() {
	}
	
	public static PrivacyType getPrivacyType(String fileOwner, Integer filePrivacy, List<User> sharedUsers) {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		// if is not private
		if(filePrivacy <= 0) {
			return PrivacyType.PUBLIC;
		}
		
		// if i'm the owner or i'm an admin
		if(isUserAdmin(userDetails) || isOnsiteFellows(userDetails)  || userDetails.getUsername().equals(fileOwner)) {
			return PrivacyType.PUBLIC;
		}
		
		// the document has been shared with me
		for(User sharedUser : sharedUsers) {
			if(userDetails.getUsername().equals(sharedUser.getAccount())) {
				return PrivacyType.PRIVATE_SHARED;
			}
		}
		
		return PrivacyType.PRIVATE;
	}
	
	public static PrivacyType getPrivacyType(String fileOwner, Integer filePrivacy) {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		// if is not private
		if(filePrivacy <= 0) {
			return PrivacyType.PUBLIC;
		}
		
		// if i'm the owner or i'm an admin
		if(isUserAdmin(userDetails)  || userDetails.getUsername().equals(fileOwner)) {
			return PrivacyType.PUBLIC;
		}
		
		return PrivacyType.PRIVATE;
	}
	
	public static boolean isSharedWithMe(List<FileSharing> fileSharings) {
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		for(FileSharing fileSharing : fileSharings) {
			if(userDetails.getUsername().equals(fileSharing.getUser().getAccount())) {
				return true;
			}
		}
		
		return false;
	}
	
	public static boolean isUserAdmin(UserDetails userDetails) {
		for(GrantedAuthority ga : userDetails.getAuthorities()) {
			if(ADMIN_AUTHORITY.equals(ga.getAuthority())) {
				return true;
			}
		}
		
		return false;
	}
	
	public static boolean isOnsiteFellows(UserDetails userDetails) {
		for(GrantedAuthority ga : userDetails.getAuthorities()) {
			if(ONSITE_FELLOWS_AUTHORITY.equals(ga.getAuthority())) {
				return true;
			}
		}
		
		return false;
	}
}
