package org.medici.mia.common.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.medici.mia.common.json.GenericResponseJson;
import org.medici.mia.common.json.StatusType;
import org.medici.mia.common.json.folio.FolioJson;
import org.medici.mia.service.folio.Folio;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public class FolioUtils {

	private static final Logger logger = Logger.getLogger(FolioUtils.class);
	private static final String FOLIOS_EMPTY_UPLOAD_FILE_ID_WARN = "The field uploadFileId is empty or null. The folio with uploadfileid empty or null can't be added in DB!";
	private static final String FOLIOS_DOUBLE_FOLIO_WARN = "Double folios problem. Only the first folio will be added in DB!";
	private static final String FOLIOS_INVALID_JSON_FIELD_COMBINATION_WARN = "The combination of Json request was not correct!. The noNum fiels will be corrected Authomaticaly.";
	private static final String FOLIOS_EMPTY_REQUEST_ERROR = "There is no folios in the folio list received by FE. No folios added in DB!";
	private static final String FOLIOS_SIZE_REQUEST_WARN = "ATTENTION: The folios have more than 2 folios. The only first due folios will be considered as valid!";

	public static List<FolioJson> prepareFoliosToBeAdded(
			List<FolioJson> folios, GenericResponseJson resp) {
		if (folios == null || folios.isEmpty()) {
			resp.setStatus(StatusType.w.toString());
			resp.setMessage(FOLIOS_EMPTY_REQUEST_ERROR);
			return null;

		}

		List<FolioJson> foliosToSave = new ArrayList<FolioJson>();

		for (FolioJson folio : folios) {
			if (foliosToSave.size() > 2) {
				logger.info(FOLIOS_SIZE_REQUEST_WARN);
				break;
			}
			
			if (folio.getNoNumb() == null) {
				folio.setNoNumb(Boolean.FALSE);
			}

			// Set all empty fields to null
			if ("".equalsIgnoreCase(folio.getFolioNumber())) {
				folio.setFolioNumber(null);
			}
			if ("".equalsIgnoreCase(folio.getRectoverso())) {
				folio.setRectoverso(null);
			}

			if (folio.getRectoverso() == null && folio.getFolioNumber() == null
					&& !folio.getNoNumb()) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage(FOLIOS_INVALID_JSON_FIELD_COMBINATION_WARN);
				logger.info(FOLIOS_SIZE_REQUEST_WARN);
				folio.setNoNumb(Boolean.TRUE);
			}
			if (folio.getFolioNumber() != null && folio.getNoNumb()) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage(FOLIOS_INVALID_JSON_FIELD_COMBINATION_WARN);
				logger.info(FOLIOS_SIZE_REQUEST_WARN);
				folio.setNoNumb(Boolean.FALSE);
			}

			if (folio.getUploadFileId() != null) {
				foliosToSave.add(folio);
			} else {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage(FOLIOS_EMPTY_UPLOAD_FILE_ID_WARN);
				logger.info(FOLIOS_EMPTY_UPLOAD_FILE_ID_WARN);

			}

		}

		// Additional control for Double Folios
		if (foliosToSave.size() > 1) {

			Folio folio1 = new Folio(foliosToSave.get(0).getFolioNumber(),
					foliosToSave.get(0).getRectoverso(), foliosToSave.get(0)
							.getNoNumb());

			Folio folio2 = new Folio(foliosToSave.get(1).getFolioNumber(),
					foliosToSave.get(1).getRectoverso(), foliosToSave.get(1)
							.getNoNumb());

			if (folio1.equals(folio2) && !folio1.getNoNumb()) {
				resp.setStatus(StatusType.w.toString());
				resp.setMessage(FOLIOS_DOUBLE_FOLIO_WARN);
				foliosToSave.remove(1);
			}

		}

		return foliosToSave;

	}

	public static List<FolioJson> prepareFoliosToBeAdded(List<FolioJson> folios) {
		if (folios == null || folios.isEmpty()) {
			return null;
		}

		List<FolioJson> foliosToSave = new ArrayList<FolioJson>();

		for (FolioJson folio : folios) {
			if (foliosToSave.size() > 2) {
				logger.info(FOLIOS_SIZE_REQUEST_WARN);
				break;
			}

			// Set all empty fields to null
			if ("".equalsIgnoreCase(folio.getFolioNumber())) {
				folio.setFolioNumber(null);
			}
			if ("".equalsIgnoreCase(folio.getRectoverso())) {
				folio.setRectoverso(null);
			}

			if (folio.getRectoverso() == null && folio.getFolioNumber() == null
					&& !folio.getNoNumb()) {
				logger.info(FOLIOS_SIZE_REQUEST_WARN);
				folio.setNoNumb(Boolean.TRUE);
			}
			if (folio.getFolioNumber() != null && folio.getNoNumb()) {
				logger.info(FOLIOS_SIZE_REQUEST_WARN);
				folio.setNoNumb(Boolean.FALSE);
			}

			if (folio.getUploadFileId() != null) {
				foliosToSave.add(folio);
			} else {
				logger.info(FOLIOS_EMPTY_UPLOAD_FILE_ID_WARN);

			}

		}

		// Additional control for Double Folios
		if (foliosToSave.size() > 1) {

			Folio folio1 = new Folio(foliosToSave.get(0).getFolioNumber(),
					foliosToSave.get(0).getRectoverso(), foliosToSave.get(0)
							.getNoNumb());

			Folio folio2 = new Folio(foliosToSave.get(1).getFolioNumber(),
					foliosToSave.get(1).getRectoverso(), foliosToSave.get(1)
							.getNoNumb());

			if (folio1.equals(folio2) && !folio1.getNoNumb()) {
				foliosToSave.remove(1);
			}

		}

		return foliosToSave;

	}

	public static boolean foliosIfValid(List<FolioJson> folios) {
		for (Iterator<FolioJson> folioJsonIterator = folios.iterator(); folioJsonIterator.hasNext();) {
			FolioJson folioJson = folioJsonIterator.next();
			String rectoverso = folioJson.getRectoverso();
			if (rectoverso == null || rectoverso.equalsIgnoreCase(""))
			    return true;
			if (rectoverso.equalsIgnoreCase("recto") || rectoverso.equalsIgnoreCase("verso"))
				return true;
		}
		return false;
	}
}
