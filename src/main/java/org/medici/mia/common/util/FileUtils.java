package org.medici.mia.common.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.List;

import org.apache.commons.lang.SystemUtils;
import org.apache.log4j.Logger;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.exception.ApplicationThrowable;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public class FileUtils {

	/**
	 * Size of a byte buffer to read/write file
	 */
	private static final int BUFFER_SIZE = 4096;
	private static final Logger logger = Logger.getLogger(FileUtils.class);
	public static final String OS_SLASH;
	public static final String ORIGINAL_FILES_DIR = "originalFiles";
	public static final String THUMB_FILES_DIR = "thumbs";
	public static final int DEFAULT_CONVERSION = 0;
	public static final int THUMBS_CONVERSION_INPROGRESS = 1;
	public static final int THUMBS_CONVERSION_SUCCESS = 2;
	public static final int THUMBS_CONVERSION_ERROR = 3;
	public static final int TIFFS_CONVERSION_INPROGRESS = 4;
	public static final int TIFFS_CONVERSION_SUCCESS = 5;
	public static final int TIFFS_CONVERSION_ERROR = 6;
	public static final Integer MAX_NUM_OF_THUMBS_PER_AE = 50;
	public static final Integer MAX_NUM_OF_THUMBS_PER_UPLOADFILE = 20;
	public static final Integer MAX_NUM_OF_THUMBS_PER_UPLOAD = 20;
	public static final Integer MAX_NUM_OF_AE_PER_PAGE = 20;
	public static final Integer MAX_NUM_OF_THUMBS_PER_ROW = 20;
	public static final String SHOW_PERSON_URL;
	public static final String SHOW_PLACE_URL;

	/**
	 * Checks the existence of a path.
	 * 
	 * @param path
	 *            the path to check
	 * @param createIfNotExist
	 *            true for creating the path if it does not exist, false
	 *            otherwise
	 * @return true if path the exists, false otherwise
	 */
	static {
		if (SystemUtils.IS_OS_WINDOWS) {
			OS_SLASH = "\\";
			// Modified by AL 06052018
		} else {
			OS_SLASH = "/";
		}
		//
		SHOW_PERSON_URL = "http://"
				+ ApplicationPropertyManager
						.getApplicationProperty("website.domain")
				+ ApplicationPropertyManager
						.getApplicationProperty("website.contextPath")
				+ "src/peoplebase/ShowPerson.do?personId=";
		SHOW_PLACE_URL = "http://"
				+ ApplicationPropertyManager
						.getApplicationProperty("website.domain")
				+ ApplicationPropertyManager
						.getApplicationProperty("website.contextPath")
				+ "src/geobase/ShowPlace.do?placeAllId=";
	}

	public static boolean checkPath(String path, boolean createIfNotExist) {
		if (path == null || "".equals(path)) {
			throw new IllegalArgumentException("You should provide valid path");
		}

		File oaPath = new File(path);
		if (!oaPath.exists() && createIfNotExist) {
			return oaPath.mkdirs();
		}
		return oaPath.exists();
	}

	public static long getContent(File file, OutputStream output)
			throws FileNotFoundException, IOException {
		if (!file.exists()) {
			throw new FileNotFoundException("File ["
					+ (file != null ? file.getAbsolutePath() : "???")
					+ "] does not exist");
		}
		BufferedReader reader = null;
		try {
			FileInputStream input = new FileInputStream(file);
			reader = new BufferedReader(new InputStreamReader(input));
			byte[] buffer = new byte[BUFFER_SIZE];
			int bytesRead = -1;
			while ((bytesRead = input.read(buffer)) != -1) {
				output.write(buffer, 0, bytesRead);
			}
		} catch (Exception e) {
			throw new IOException(e);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
				}
			}
		}
		return file.length();
	}

	public static String fileSize(File file) {
		if (file == null || !file.exists() || file.isDirectory()) {
			return null;
		}
		return convertFileSize(file.length(), false);
	}

	/**
	 * Renames a file with new file name. If the target file exists it is
	 * overridden.
	 * 
	 * @param fileName
	 *            the file name (including path) to rename
	 * @param targetFileName
	 *            the target file name (including path)
	 * @return true if and only if the renaming succeeded
	 * @throws IOException
	 */
	public static boolean renameTo(String fileName, String targetFileName)
			throws IOException {
		File current = new File(fileName);
		File newF = new File(targetFileName);

		if (!current.exists()) {
			throw new IOException("File [" + fileName + "] does not exits");
		}

		if (newF.exists()) {
			newF.delete();
		}

		return current.renameTo(new File(targetFileName));
	}

	/**
	 * Returns true if the file exists.
	 * 
	 * @param fileName
	 *            the file name (including path) to check
	 * @return true if the file exists
	 * @throws IOException
	 */
	public static boolean exists(String fileName) throws IOException {
		return new File(fileName).exists();
	}

	/* Privates */

	private static String convertFileSize(long bytes, boolean si) {
		int unit = si ? 1000 : 1024;
		if (bytes < unit)
			return bytes + " B";
		int exp = (int) (Math.log(bytes) / Math.log(unit));
		String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1)
				+ (si ? "" : "i");
		return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
	}

	public static String getRealPath(String gPlaceId,
			String repositoryAbbreviation, String collectionAbbreviation,
			String volumePath, String insertPath) throws ApplicationThrowable {
		// Implementation of real path
		StringBuilder path = new StringBuilder();
		path.append(ApplicationPropertyManager
				.getApplicationProperty("images.storage.basedir"));

		if (repositoryAbbreviation != null && !repositoryAbbreviation.isEmpty()) {
			path.append(repositoryAbbreviation);
			path.append("-");
			path.append(gPlaceId);
			path.append(OS_SLASH);
		} else {
			logger.error("ERROR: Repository is needed to build the Real Path");
			throw new ApplicationThrowable();
		}
		if (collectionAbbreviation != null && !collectionAbbreviation.isEmpty()) {
			path.append(collectionAbbreviation);
			path.append(OS_SLASH);
		} else {
			logger.error("ERROR: Collection is needed to build the Real Path");
			throw new ApplicationThrowable();
		}
		path.append(volumePath);
		path.append(OS_SLASH);
		if (insertPath != null && !insertPath.isEmpty()) {
			path.append(insertPath);
			path.append(OS_SLASH);
		}
		logger.info("Real Path: " + path.toString());
		return path.toString();
	}

	public static String getRealPathJSONResponse(String gPlaceId,
			String repositoryAbbreviation, String collectionAbbreviation,
			String volumePath, String insertPath) throws ApplicationThrowable {
		// Implementation of real path
		StringBuilder path = new StringBuilder();
		path.append("/Mia"); // change to $context_path
		path.append("/mview/IIPImageServer.do?FIF=");

		if (repositoryAbbreviation != null && !repositoryAbbreviation.isEmpty()) {
			path.append(repositoryAbbreviation);
			path.append("-");
			path.append(gPlaceId);
			path.append(OS_SLASH);
		} else {
			logger.error("ERROR: Repository is needed to build the Real Path");
			throw new ApplicationThrowable();
		}
		if (collectionAbbreviation != null && !collectionAbbreviation.isEmpty()) {
			path.append(collectionAbbreviation);
			path.append(OS_SLASH);
		} else {
			logger.error("ERROR: Collection is needed to build the Real Path");
			throw new ApplicationThrowable();
		}
		if (volumePath != null && !volumePath.isEmpty()) {
			path.append(volumePath);
			path.append(OS_SLASH);
		}

		if (insertPath != null && !insertPath.isEmpty()) {
			path.append(insertPath);
			path.append(OS_SLASH);
		}
		logger.info("Real Path: " + path.toString());
		return path.toString();
	}

	public static String getPathIIPImageServer(String gPlaceId,
			String repositoryAbbreviation, String collectionAbbreviation,
			String volumePath, String insertPath) throws ApplicationThrowable {
		StringBuilder path = new StringBuilder();

		if (repositoryAbbreviation != null && !repositoryAbbreviation.isEmpty()) {
			path.append(repositoryAbbreviation);
			path.append("-");
			path.append(gPlaceId);
			path.append('/');
		} else {
			logger.error("ERROR: Repository is needed to build the Real Path");
			throw new ApplicationThrowable();
		}
		if (collectionAbbreviation != null && !collectionAbbreviation.isEmpty()) {
			path.append(collectionAbbreviation);
			path.append('/');
		} else {
			logger.error("ERROR: Collection is needed to build the Real Path");
			throw new ApplicationThrowable();
		}
		path.append(volumePath);
		path.append('/');
		if (insertPath != null && !insertPath.isEmpty()) {
			path.append(insertPath);
			path.append('/');
		}
		logger.info("Real Path: " + path.toString());
		return path.toString();
	}

	public static String getRealPathImagePreview(String gPlaceId,
			String repositoryAbbreviation, String collectionAbbreviation,
			String volumePath, String insertPath) throws ApplicationThrowable {
		// Implementation of real path
		StringBuilder path = new StringBuilder();

		if (repositoryAbbreviation != null && !repositoryAbbreviation.isEmpty()) {
			path.append(repositoryAbbreviation);
			path.append("-");
			path.append(gPlaceId);
			path.append(OS_SLASH);
			path.append(OS_SLASH);
		} else {
			logger.error("ERROR: Repository is needed to build the Real Path");
			throw new ApplicationThrowable();
		}
		if (collectionAbbreviation != null && !collectionAbbreviation.isEmpty()) {
			path.append(collectionAbbreviation);
			path.append(OS_SLASH);
			path.append(OS_SLASH);
		} else {
			logger.error("ERROR: Collection is needed to build the Real Path");
			throw new ApplicationThrowable();
		}
		path.append(volumePath);
		path.append(OS_SLASH);
		path.append(OS_SLASH);
		if (insertPath != null && !insertPath.isEmpty()) {
			path.append(insertPath);
			path.append(OS_SLASH);
			path.append(OS_SLASH);
		}
		logger.info("Real Path: " + path.toString());
		return path.toString();
	}

	public static void moveToNewDir(String oldRealPath, String newRealPath,
			List<UploadFileEntity> fileList) throws Exception {

		if (newRealPath == null || oldRealPath == null
				|| newRealPath.equalsIgnoreCase(oldRealPath))
			return;

		if (fileList == null || fileList.isEmpty())
			return;

		// TODO: handle what happens in Exception cases or block process
		// Exception is thrown to the caller method in Service which is
		// transational so it will rollback.
		// But the rollback for the files is not handled: TODO

		// Setting up source and destinations directories
		File srcDir = new File(oldRealPath);
		File srcDirOriginalFile = new File(oldRealPath + OS_SLASH
				+ org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR
				+ OS_SLASH);
		File srcDirThumbs = new File(oldRealPath + OS_SLASH
				+ org.medici.mia.common.util.FileUtils.THUMB_FILES_DIR
				+ OS_SLASH + "");
		File destDir = new File(newRealPath);
		File destDirOriginalFile = new File(newRealPath + OS_SLASH
				+ org.medici.mia.common.util.FileUtils.ORIGINAL_FILES_DIR
				+ OS_SLASH);
		File destDirThumbs = new File(newRealPath + OS_SLASH
				+ org.medici.mia.common.util.FileUtils.THUMB_FILES_DIR
				+ OS_SLASH);

		// 1) Take the list of the files from UploadId and copy in newDir
		for (UploadFileEntity uploadFile : fileList) {

			String fileName = uploadFile.getFilename();

			File srcOriginalFile = new File(srcDirOriginalFile + OS_SLASH
					+ fileName);
			if (srcOriginalFile.exists()) {
				org.apache.commons.io.FileUtils.copyFileToDirectory(
						srcOriginalFile, destDirOriginalFile);
			}

			File srcThumbFile = new File(srcDirThumbs + OS_SLASH + fileName);
			if (srcThumbFile.exists()) {
				org.apache.commons.io.FileUtils.copyFileToDirectory(
						srcThumbFile, destDirThumbs);
			}

			String fileNameNoExtens = org.apache.commons.io.FilenameUtils
					.removeExtension(fileName);

			File srcFile = new File(srcDir + OS_SLASH + fileNameNoExtens
					+ ".tif");
			if (srcFile.exists()) {
				org.apache.commons.io.FileUtils.copyFileToDirectory(srcFile,
						destDir);
			}

		}

		// 2) Take the list of the files from UploadId and check if they are
		// in newDir, in case delete srcFile
		// We check if the copy is done if the file exists and if the size
		// in the same of the old one
		// If some file miss delete all the content in the newDir folders
		for (UploadFileEntity uploadFile : fileList) {

			String fileName = uploadFile.getFilename();

			File destOriginalFile = new File(destDirOriginalFile + OS_SLASH
					+ fileName);
			File srcOriginalFile = new File(srcDirOriginalFile + OS_SLASH
					+ fileName);
			if (destOriginalFile.exists()
					&& (destOriginalFile.length() == srcOriginalFile.length())) {
				org.apache.commons.io.FileUtils.forceDelete(srcOriginalFile);
			}

			File destThumbFile = new File(destDirThumbs + OS_SLASH + fileName);
			File srcThumbFile = new File(srcDirThumbs + OS_SLASH + fileName);
			if (destThumbFile.exists()
					&& (destThumbFile.length() == srcThumbFile.length())) {
				org.apache.commons.io.FileUtils.forceDelete(srcThumbFile);
			}

			String fileNameNoExtens = org.apache.commons.io.FilenameUtils
					.removeExtension(fileName);

			File destFile = new File(destDir + OS_SLASH + fileNameNoExtens
					+ ".tif");
			File srcFile = new File(srcDir + OS_SLASH + fileNameNoExtens
					+ ".tif");
			if (destFile.exists() && (destFile.length() == srcFile.length())) {
				org.apache.commons.io.FileUtils.forceDelete(srcFile);
			}
		}
	}

	public static String getRealPathIIIFImage(String gPlaceId,
			String repositoryAbbreviation, String collectionAbbreviation,
			String volumePath, String insertPath) throws ApplicationThrowable {

		StringBuilder path = new StringBuilder();
		path.append(getRealPath(gPlaceId, repositoryAbbreviation,
				collectionAbbreviation, volumePath, insertPath));
		path.append(OS_SLASH);
		path.append(ORIGINAL_FILES_DIR);
		path.append(OS_SLASH);
		logger.info("Real Path: " + path.toString());
		return path.toString();

	}

}
