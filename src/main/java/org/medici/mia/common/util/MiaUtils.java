package org.medici.mia.common.util;

import org.medici.mia.common.json.GenericResponseJson;

public class MiaUtils {

	/**
	 * Size of a byte buffer to read/write file
	 */

	public static final String ERROR_GENERIC = "ERROR: Generic error happened.";
	public static final String WARNING_GENERIC = "";

	public static GenericResponseJson fillResponse(String status, String message) {

		GenericResponseJson res = new GenericResponseJson();
		res.setStatus(status);
		res.setMessage(message);

		return res;

	}

}
