package org.medici.mia.common.util;

import java.util.ArrayList;
import java.util.List;

import org.medici.mia.controller.archive.BrowseArchivalLocationJson;
import org.medici.mia.service.useractivity.NewsType;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public class NativeQueryUtils {
	private static final String NOT_DELETED_DOC = " d.flgLogicalDelete != 1";
	public static List<String> findMiscellaneousQuery(List<String> people) {

		StringBuffer b = new StringBuffer();

		List<String> queries = new ArrayList<String>();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocMiscellaneous c where ");

		b.append("( ");
		b.append("c.printer like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.printer like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.printer like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.printer like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId");

		queries.add(b.toString());

		return queries;

	}

	private static List<String> findMiscellaneousPlaceQuery(List<String> places) {

		StringBuffer b = new StringBuffer();

		List<String> queries = new ArrayList<String>();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocMiscellaneous c where ");

		b.append("( ");
		b.append("c.printerPlace like ");
		b.append("'");
		b.append(places.get(0) + ":%");
		b.append("'");
		b.append(" OR c.printerPlace like ");
		b.append("'");
		b.append("," + places.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < places.size(); i++) {
			b.append(" OR ");

			b.append("c.printerPlace like ");
			b.append("'");
			b.append(places.get(i) + ":%");
			b.append("'");
			b.append(" OR c.printerPlace like ");
			b.append("'");
			b.append("%," + places.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId");

		queries.add(b.toString());

		return queries;

	}

	public static List<String> findNotarialRecordsQuery(List<String> people) {

		List<String> queries = new ArrayList<String>();
		queries.add(getNotarialPeopleQuery(people, "contractor"));
		queries.add(getNotarialPeopleQuery(people, "contractee"));

		return queries;

	}

	private static String getNotarialPeopleQuery(List<String> people,
			String filedName) {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocNotarialRecords c where ");

		b.append("( ");
		b.append("c.");
		b.append(filedName);
		b.append(" like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.");
		b.append(filedName);
		b.append(" like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.");
			b.append(filedName);
			b.append(" like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.");
			b.append(filedName);
			b.append(" like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId");

		return b.toString();

	}

	public static String findProducerQuery(List<String> people) {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblPeople p, tblDocumentsEntsPeople dp where ");

		b.append("( ");
		b.append("p.PERSONID = ");
		b.append("'");
		b.append(people.get(0));
		b.append("'");
		for (int i = 1; i < people.size() - 1; i++) {
			b.append(" OR ");
			b.append("p.PERSONID = ");
			b.append("'");
			b.append(people.get(i));
			b.append("'");
		}
		b.append(" )");
		b.append("and p.PERSONID = dp.producer and dp.documentEntityId = d.documentEntityId");

		return b.toString();
	}

	private static String findPlaceOfOriginQuery(List<String> places) {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d where ");

		b.append("( ");
		b.append("d.placeOfOrigin = ");
		b.append("'");
		b.append(places.get(0));
		b.append("'");
		for (int i = 1; i < places.size() - 1; i++) {
			b.append(" OR ");
			b.append("d.placeOfOrigin = ");
			b.append("'");
			b.append(places.get(i));
			b.append("'");
		}
		b.append(" )");
		b.append("and " + NOT_DELETED_DOC);

		return b.toString();
	}

	public static String getGeneralArchiveQuery(
			BrowseArchivalLocationJson locationJson) {

		if (locationJson.getRepositoryId() == null
				&& locationJson.getCollectionId() == null
				&& locationJson.getInsertId() == null
				&& locationJson.getSeriesId() == null
				&& locationJson.getVolumeId() == null) {
			return null;
		}

		StringBuffer b = new StringBuffer();

		b.append("SELECT DISTINCT u FROM UploadInfoEntity u WHERE ");

		if (locationJson.getRepositoryId() != null) {
			b.append("u.repository = ");
			b.append(locationJson.getRepositoryId());
			b.append(" AND ");
		}

		if (locationJson.getCollectionId() != null) {
			b.append("u.collection = ");
			b.append(locationJson.getCollectionId());
			b.append(" AND ");
		}

		if (locationJson.getVolumeId() != null) {
			b.append("u.volume = ");
			b.append(locationJson.getVolumeId());
			b.append(" AND ");
		}

		if (locationJson.getInsertId() != null) {
			b.append("u.insertId = ");
			b.append(locationJson.getInsertId());
			b.append(" AND ");
		}

		if (locationJson.getSeriesId() != null) {
			b.append("u.series = ");
			b.append(locationJson.getSeriesId());
			b.append(" AND ");
		}

		b.append("u.logicalDelete=0 ORDER BY u.dateModified DESC");
		return b.toString();
	}

	public static List<String> findOfficialRecordsQuery(List<String> people) {

		StringBuffer b = new StringBuffer();

		List<String> queries = new ArrayList<String>();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocOfficialRecords c where ");

		b.append("( ");
		b.append("c.printer like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.printer like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.printer like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.printer like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId");

		queries.add(b.toString());

		return queries;
	}

	private static List<String> findOfficialRecordsPlaceQuery(List<String> places) {

		StringBuffer b = new StringBuffer();

		List<String> queries = new ArrayList<String>();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocOfficialRecords c where ");

		b.append("( ");
		b.append("c.printerPlace like ");
		b.append("'");
		b.append(places.get(0) + ":%");
		b.append("'");
		b.append(" OR c.printerPlace like ");
		b.append("'");
		b.append("," + places.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < places.size(); i++) {
			b.append(" OR ");

			b.append("c.printerPlace like ");
			b.append("'");
			b.append(places.get(i) + ":%");
			b.append("'");
			b.append(" OR c.printerPlace like ");
			b.append("'");
			b.append("%," + places.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId");

		queries.add(b.toString());

		return queries;
	}

	public static List<String> findFinancialRecordsQuery(List<String> people) {

		StringBuffer b = new StringBuffer();

		List<String> queries = new ArrayList<String>();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocFinancialRecords c where ");

		b.append("( ");
		b.append("c.commissionerOfAccount like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.commissionerOfAccount like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.commissionerOfAccount like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.commissionerOfAccount like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId");
		queries.add(b.toString());
		return queries;

	}

	public static List<String> findCorrespondenceQuery(List<String> people) {

		StringBuffer b = new StringBuffer();

		List<String> queries = new ArrayList<String>();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocCorrespondence c where ");

		b.append("( ");
		b.append("c.recipient like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.recipient like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.recipient like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.recipient like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId");

		queries.add(b.toString());
		return queries;

	}

	private static List<String> findCorrespondencePlaceQuery(List<String> places) {

		StringBuffer b = new StringBuffer();

		List<String> queries = new ArrayList<String>();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocCorrespondence c where ");

		b.append("( ");
		b.append("c.recipientPlace like ");
		b.append("'");
		b.append(places.get(0) + ":%");
		b.append("'");
		b.append(" OR c.recipientPlace like ");
		b.append("'");
		b.append("%," + places.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < places.size(); i++) {
			b.append(" OR ");

			b.append("c.recipientPlace like ");
			b.append("'");
			b.append(places.get(i) + ":%");
			b.append("'");
			b.append(" OR c.recipientPlace like ");
			b.append("'");
			b.append("%," + places.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId");

		queries.add(b.toString());
		return queries;

	}

	public static List<String> findLiteraryWorksQuery(List<String> people) {

		List<String> queries = new ArrayList<String>();
		queries.add(getLiteraryPeopleQuery(people, "dedicatee"));
		queries.add(getLiteraryPeopleQuery(people, "printer"));

		return queries;

	}

	private static String getLiteraryPeopleQuery(List<String> people,
			String filedName) {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocLiteraryWorks c where ");

		b.append("( ");
		b.append("c.");
		b.append(filedName);
		b.append(" like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.");
		b.append(filedName);
		b.append(" like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.");
			b.append(filedName);
			b.append(" like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.");
			b.append(filedName);
			b.append(" like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId");

		return b.toString();

	}

	private static List<String> findNewsPlaceQuery(List<String> places) {

		List<String> queries = new ArrayList<String>();
		queries.add(getNewsPlaceQuery(places, "newsFrom"));
		queries.add(getNewsPlaceQuery(places, "printerPlace"));

		return queries;

	}

	private static String getNewsPlaceQuery(List<String> places,
			String filedName) {

		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocNews c where ");

		b.append("( ");
		b.append("c.");
		b.append(filedName);
		b.append(" like ");
		b.append("'");
		b.append(places.get(0) + ":%");
		b.append("'");
		b.append(" OR c.");
		b.append(filedName);
		b.append(" like ");
		b.append("'");
		b.append("%," + places.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < places.size(); i++) {
			b.append(" OR ");

			b.append("c.");
			b.append(filedName);
			b.append(" like ");
			b.append("'");
			b.append(places.get(i) + ":%");
			b.append("'");
			b.append(" OR c.");
			b.append(filedName);
			b.append(" like ");
			b.append("'");
			b.append("%," + places.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId");

		return b.toString();

	}

	private static List<String> findLiteraryPlaceQuery(List<String> places) {

		List<String> queries = new ArrayList<String>();
		StringBuffer b = new StringBuffer();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocLiteraryWorks c where ");

		b.append("( ");
		b.append("c.printerPlace");
		b.append(" like ");
		b.append("'");
		b.append(places.get(0) + ":%");
		b.append("'");
		b.append(" OR c.printerPlace");
		b.append(" like ");
		b.append("'");
		b.append("%," + places.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < places.size(); i++) {
			b.append(" OR ");
			b.append("c.printerPlace");
			b.append(" like ");
			b.append("'");
			b.append(places.get(i) + ":%");
			b.append("'");
			b.append(" OR c.printerPlace");
			b.append(" like ");
			b.append("'");
			b.append("%," + places.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId");
		queries.add(b.toString());

		return queries;

	}

	public static List<String> findNewsQuery(List<String> people) {

		StringBuffer b = new StringBuffer();

		List<String> queries = new ArrayList<String>();

		b.append("select distinct d.documentEntityId documentId from tblDocumentEnts d, tblDocNews c where ");

		b.append("( ");
		b.append("c.printer like ");
		b.append("'");
		b.append(people.get(0) + ":%");
		b.append("'");
		b.append(" OR c.printer like ");
		b.append("'");
		b.append("," + people.get(0) + ":%");
		b.append("'");
		for (int i = 1; i < people.size(); i++) {
			b.append(" OR ");

			b.append("c.printer like ");
			b.append("'");
			b.append(people.get(i) + ":%");
			b.append("'");
			b.append(" OR c.printer like ");
			b.append("'");
			b.append("," + people.get(i) + ":%");
			b.append("'");
		}
		b.append(" ) ");

		b.append("and c.documentEntityId = d.documentEntityId");

		queries.add(b.toString());
		return queries;

	}

	public static List<String> findDocumentsByPeopleQuery(List<String> people) {
		List<String> queries = new ArrayList<String>();

		queries.addAll(findCorrespondenceQuery(people));
		queries.addAll(findFinancialRecordsQuery(people));
		queries.addAll(findLiteraryWorksQuery(people));
		queries.addAll(findMiscellaneousQuery(people));
		queries.addAll(findNotarialRecordsQuery(people));
		queries.addAll(findNewsQuery(people));
		queries.addAll(findOfficialRecordsQuery(people));
		queries.add(findProducerQuery(people));

		return queries;

	}
	
	public static List<String> findDocumentsByPlacesQuery(List<String> places) {
		List<String> queries = new ArrayList<String>();

		queries.addAll(findCorrespondencePlaceQuery(places));
		queries.addAll(findNewsPlaceQuery(places));
		queries.addAll(findLiteraryPlaceQuery(places));
		queries.addAll(findMiscellaneousPlaceQuery(places));		
		queries.addAll(findOfficialRecordsPlaceQuery(places));
		queries.add(findPlaceOfOriginQuery(places));

		return queries;

	}

	public static String getNewsQuery(String dateCreated, NewsType type) {

		StringBuffer b = new StringBuffer();

		if (NewsType.AE.equals(type)) {
			b.append("Select u from UploadInfoEntity u where u.dateCreated >= ");
		} else if (NewsType.DOCUMENT.equals(type)) {
			b.append("Select u from MiaDocumentEntity u where u.dateCreated >= ");
		} else if (NewsType.PEOPLE.equals(type)) {
			b.append("Select u from People u where u.dateCreated >= ");
		} else if (NewsType.PLACE.equals(type)) {
			b.append("Select u from Place u where u.dateCreated >= ");
		}

		b.append("'");
		b.append(dateCreated);
		b.append("'");

		return b.toString();

	}
}
