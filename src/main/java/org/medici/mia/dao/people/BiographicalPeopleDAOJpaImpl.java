/*
 * PeopleDAOJpaImpl.java
 * 
 * Developed by Medici Archive Project (2010-2012).
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.dao.people;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.medici.mia.common.util.DateUtils;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.BiographicalPeople;
import org.springframework.stereotype.Repository;

@Repository
public class BiographicalPeopleDAOJpaImpl extends
		JpaDao<Integer, BiographicalPeople> implements BiographicalPeopleDAO {

	private static final long serialVersionUID = 1L;

	@Override
	public BiographicalPeople findPeopleById(Integer personId)
			throws PersistenceException {
		BiographicalPeople people = getEntityManager().find(
				BiographicalPeople.class, personId);
		return people;
	}

	@Override
	public List<BiographicalPeople> findPersonParents(Integer childId)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT p FROM BiographicalPeople p, Parent par WHERE par.parent.personId=p.personId AND par.child.personId=:childId");
		query.setParameter("childId", childId);

		return (List<BiographicalPeople>) query.getResultList();
	}

	@Override
	public Integer deletePerson(Integer personId, String owner,
			boolean isAutorizedUser) throws PersistenceException {

		BiographicalPeople personEnt = findPeopleById(personId);

		if (isAutorizedUser
				|| (personEnt.getCreatedBy() != null && personEnt
						.getCreatedBy().equalsIgnoreCase(owner))) {
			personEnt.setLogicalDelete(Boolean.TRUE);
			personEnt.setLastUpdate(new Date());
			personEnt.setLastUpdateBy(owner);

			getEntityManager().merge(personEnt);
			return 1;
		}

		return -1;

	}

	@Override
	public Integer undeletePerson(Integer personId, String owner,
			boolean isAutorizedUser) throws PersistenceException {

		BiographicalPeople personEnt = findPeopleById(personId);

		if ((personEnt == null)) {
			return 0;
		}

		if (isAutorizedUser
				|| (personEnt.getCreatedBy() != null && personEnt
						.getCreatedBy().equalsIgnoreCase(owner))) {
			personEnt.setLogicalDelete(Boolean.FALSE);
			personEnt.setLastUpdate(new Date());
			personEnt.setLastUpdateBy(owner);

			getEntityManager().merge(personEnt);
			return 1;
		}

		return -1;

	}

	@Override
	public List<BiographicalPeople> findBiographicalPeopleById(
			List<Integer> personIds) throws PersistenceException {

		if (personIds != null && !personIds.isEmpty()) {

			List<BiographicalPeople> bioPeople = new ArrayList<BiographicalPeople>();
			for (Integer personId : personIds) {
				try {
					BiographicalPeople person = getEntityManager().find(
							BiographicalPeople.class, personId);
					bioPeople.add(person);

				} catch (Exception e) {
					// TODO Auto-generated catch block

				}
			}

			return bioPeople;
		}

		return null;
	}

	@Override
	public void setPeopleModInf(Integer personId, String username)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT u FROM BiographicalPeople u WHERE u.personId=:personId");
		query.setParameter("personId", personId);
		List<BiographicalPeople> list = (List<BiographicalPeople>) query
				.getResultList();
		if (list != null && !list.isEmpty()) {
			BiographicalPeople entity = list.get(0);
			entity.setLastUpdateBy(username);
			entity.setLastUpdate(new Date());
			getEntityManager().merge(entity);
		}

	}

	@Override
	public List<BiographicalPeople> findPeopleByPlace(Integer placeId)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT p FROM BiographicalPeople p WHERE (p.bornPlaceId=:bornPlaceId OR p.deathPlaceId=:deathPlaceId) AND logicalDelete = false");
		query.setParameter("bornPlaceId", placeId);
		query.setParameter("deathPlaceId", placeId);

		return (List<BiographicalPeople>) query.getResultList();
	}

	@Override
	public Integer setPeopleDeathDate() throws PersistenceException {

		Query query = getEntityManager().createQuery(
				"SELECT u FROM BiographicalPeople u");
		List<BiographicalPeople> list = (List<BiographicalPeople>) query
				.getResultList();
		int count = 0;

		if (list != null && !list.isEmpty()) {
			for (BiographicalPeople people : list) {
				Integer deathDate = DateUtils.getLuceneDate(
						people.getDeathYear(), people.getDeathMonthNum(),
						people.getDeathDay());
				people.setDeathDate(deathDate);
				getEntityManager().merge(people);
				count++;
			}

		}

		return count;

	}

	@Override
	public List<Integer> countPeopleForAdvancedSearch(List<String> queryList)
			throws PersistenceException {

		if (queryList == null || queryList.isEmpty()) {
			return null;
		}

		Query q = getEntityManager().createNativeQuery(queryList.get(0));

		List<Integer> peopleIdList = (List<Integer>) q.getResultList();
		if (peopleIdList == null || peopleIdList.isEmpty()) {
			return null;
		}

		for (int i = 1; i < queryList.size(); i++) {
			q = getEntityManager().createNativeQuery(queryList.get(i));
			List<Integer> pIds = (List<Integer>) q.getResultList();

			if (pIds == null || pIds.isEmpty()) {
				return null;
			}
			peopleIdList.retainAll(pIds);
		}

		return peopleIdList;

	}

	@Override
	public List<BiographicalPeople> findBiographicalPeople(List<Integer> peopleList,
			Integer firstResult, Integer maxResult, String orderColumn, String ascOrDesc)
			throws PersistenceException {

		if (orderColumn == null || orderColumn.isEmpty()) {
			orderColumn = "personId";
		}

		if (firstResult == null || firstResult.equals("")) {
			firstResult = 0;
		}
		if (maxResult == null || maxResult.equals("")) {
			maxResult = 20;
		}
		if (ascOrDesc == null || ascOrDesc.isEmpty()) {
			ascOrDesc = "asc";
		}

		Query query = getEntityManager()
				.createQuery(
						"SELECT p FROM BiographicalPeople p where p.personId IN :peopleList order by p."
								+ orderColumn + " " + ascOrDesc);
		query.setParameter("peopleList", peopleList);
		query.setFirstResult(firstResult).setMaxResults(maxResult);
		return (List<BiographicalPeople>) query.getResultList();
	}

	@Override
	public List<BiographicalPeople> getBiographicalPeopleCreateByUser(
			String account) {
		
		String queryString = 
				  "SELECT b FROM BiographicalPeople b "
				+ "WHERE b.createdBy = :account "
				+ "AND b.logicalDelete = 0 "
				+ "ORDER BY b.dateCreated DESC";
		
		Query query = getEntityManager().createQuery(queryString);
		query.setParameter("account", account);
		
		@SuppressWarnings("unchecked")
		List<BiographicalPeople> results = 
			(List<BiographicalPeople>) query.getResultList();
		
		return results;
	}
}
