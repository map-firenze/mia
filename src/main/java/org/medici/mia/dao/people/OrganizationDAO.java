package org.medici.mia.dao.people;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.common.json.biographical.AddHeadquarterJson;
import org.medici.mia.dao.Dao;
import org.medici.mia.domain.OrganizationEntity;

public interface OrganizationDAO extends Dao<Integer, OrganizationEntity> {

	public Integer deleteOrganization(Integer organizationId)
			throws PersistenceException;

	public Integer insertOrganization(OrganizationEntity entity)
			throws PersistenceException;

	public List<OrganizationEntity> findOrganization(AddHeadquarterJson head)
			throws PersistenceException;

	public List<OrganizationEntity> findOrganizationByPersonId(Integer orgId)
			throws PersistenceException;

}
