/*
 * PeopleDAOJpaImpl.java
 * 
 * Developed by Medici Archive Project (2010-2012).
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.dao.people;

import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.medici.mia.common.json.biographical.AddHeadquarterJson;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.OrganizationEntity;
import org.medici.mia.exception.ApplicationThrowable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class OrganizationDAOJpaImpl extends JpaDao<Integer, OrganizationEntity>
		implements OrganizationDAO {

	private static final long serialVersionUID = 1L;

	@Override
	public Integer insertOrganization(OrganizationEntity entity)
			throws PersistenceException {
		getEntityManager().persist(entity);

		return entity.getOrganizationId();
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<OrganizationEntity> findOrganization(AddHeadquarterJson head)
			throws PersistenceException {
		try {
			Query query = getEntityManager()
					.createQuery(
							"SELECT f from OrganizationEntity f WHERE f.personId=:personId and f.placeAllId=:placeAllId and f.year=:year");

			query.setParameter("personId", head.getPersonId());
			query.setParameter("placeAllId", head.getPlaceId());
			query.setParameter("year", head.getYear());

			return (List<OrganizationEntity>) query.getResultList();
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public List<OrganizationEntity> findOrganizationByPersonId(Integer personId)
			throws PersistenceException {
		try {
			Query query = getEntityManager()
					.createQuery(
							"SELECT f from OrganizationEntity f WHERE f.personId=:personId");

			query.setParameter("personId", personId);
			return (List<OrganizationEntity>) query.getResultList();
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public Integer deleteOrganization(Integer organizationId)
			throws PersistenceException {
		try {
			
			Integer personId = 0;
			Query query = getEntityManager()
					.createQuery(
							"SELECT f from OrganizationEntity f WHERE f.organizationId=:organizationId");

			query.setParameter("organizationId", organizationId);

			List<OrganizationEntity> list = (List<OrganizationEntity>) query
					.getResultList();
			if (list == null || list.isEmpty()) {
				return 0;
			}
			
			personId = list.get(0).getPersonId();

			getEntityManager().remove(list.get(0));
			return personId;

		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}

}
