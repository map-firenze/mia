/*
 * PeopleDAOJpaImpl.java
 * 
 * Developed by Medici Archive Project (2010-2012).
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.dao.people;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.util.Version;
import org.hibernate.ejb.HibernateEntityManager;
import org.hibernate.search.FullTextQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.transform.Transformers;
import org.medici.mia.common.json.advancedsearch.PeopleAdvancedSearchJson;
import org.medici.mia.common.json.complexsearch.PeopleComplexSearchJson;
import org.medici.mia.common.json.search.AllCountWordSearchJson;
import org.medici.mia.common.pagination.Page;
import org.medici.mia.common.pagination.PaginationFilter;
import org.medici.mia.common.pagination.PaginationFilter.Order;
import org.medici.mia.common.pagination.PaginationFilter.SortingCriteria;
import org.medici.mia.common.search.Search;
import org.medici.mia.common.util.HtmlUtils;
import org.medici.mia.common.util.RegExUtils;
import org.medici.mia.controller.genericsearch.PeopleSearchPaginationParam;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.AltName;
import org.medici.mia.domain.People;
import org.medici.mia.domain.Sitemap;
import org.medici.mia.domain.Sitemap.ChangeFrequency;
import org.medici.mia.service.genericsearch.ComplexSearchUtils;
import org.medici.mia.service.sitemap.SitemapSectionConfiguration;
import org.springframework.stereotype.Repository;

@Repository
public class PeopleDAOJpaImpl extends JpaDao<Integer, People> implements
		PeopleDAO {
	/**
	 * 
	 * If a serializable class does not explicitly declare a serialVersionUID,
	 * then the serialization runtime will calculate a default serialVersionUID
	 * value for that class based on various aspects of the class, as described
	 * in the Java(TM) Object Serialization Specification. However, it is
	 * strongly recommended that all serializable classes explicitly declare
	 * serialVersionUID values, since the default serialVersionUID computation
	 * is highly sensitive to class details that may vary depending on compiler
	 * implementations, and can thus result in unexpected InvalidClassExceptions
	 * during deserialization. Therefore, to guarantee a consistent
	 * serialVersionUID value across different java compiler implementations, a
	 * serializable class must declare an explicit serialVersionUID value. It is
	 * also strongly advised that explicit serialVersionUID declarations use the
	 * private modifier where possible, since such declarations apply only to
	 * the immediately declaring class--serialVersionUID fields are not useful
	 * as inherited members.
	 */
	private static final long serialVersionUID = -2964902298903431093L;

	private final Logger logger = Logger.getLogger(this.getClass());

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long countPeopleCreatedAfterDate(Date inputDate)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT COUNT(personId) FROM People WHERE dateCreated>=:inputDate");
		query.setParameter("inputDate", inputDate);

		return (Long) query.getSingleResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public People findLastEntryPerson() throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"FROM People where logicalDelete = false ORDER BY dateCreated DESC");
		query.setMaxResults(1);

		return (People) query.getSingleResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer findNumberOfActiveEndInPlace(Integer placeAllId)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT COUNT(personId) FROM People WHERE deathPlace.placeAllId =:placeAllId AND activeEnd!=NULL AND logicalDelete=false");
		query.setParameter("placeAllId", placeAllId);
		Long result = (Long) query.getSingleResult();
		return new Integer(result.intValue());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer findNumberOfActiveStartInPlace(Integer placeAllId)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT COUNT(personId) FROM People WHERE bornPlace.placeAllId =:placeAllId AND activeStart!=NULL AND logicalDelete=false");
		query.setParameter("placeAllId", placeAllId);
		Long result = (Long) query.getSingleResult();
		return new Integer(result.intValue());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer findNumberOfBirthInPlace(Integer placeAllId)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT COUNT(personId) FROM People WHERE bornPlace.placeAllId =:placeAllId AND activeStart=NULL AND logicalDelete=false");
		query.setParameter("placeAllId", placeAllId);
		Long result = (Long) query.getSingleResult();
		return new Integer(result.intValue());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer findNumberOfDeathInPlace(Integer placeAllId)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT COUNT(personId) FROM People WHERE deathPlace.placeAllId =:placeAllId AND activeEnd=NULL AND logicalDelete=false");
		query.setParameter("placeAllId", placeAllId);
		Long result = (Long) query.getSingleResult();
		return new Integer(result.intValue());
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Map<Integer, Long> findNumbersOfPeopleRelatedPlace(
			List<Integer> placeAllIds) throws PersistenceException {
		StringBuilder stringBuilderBorn = new StringBuilder(
				"SELECT bornPlace.placeAllId, COUNT(personId) FROM People WHERE logicalDelete=false AND ");
		StringBuilder stringBuilderDeath = new StringBuilder(
				"SELECT deathPlace.placeAllId, COUNT(personId) FROM People WHERE logicalDelete=false AND ");
		for (int i = 0; i < placeAllIds.size(); i++) {
			if (i > 0) {
				stringBuilderBorn.append(" OR ");
			}
			if (i > 0) {
				stringBuilderDeath.append(" OR ");
			}
			stringBuilderBorn.append("(bornPlace.placeAllId=");
			stringBuilderBorn.append(placeAllIds.get(i) + ")");
			stringBuilderDeath.append("(deathPlace.placeAllId=");
			stringBuilderDeath.append(placeAllIds.get(i)
					+ " AND bornPlace.placeAllId!=" + placeAllIds.get(i) + ")");
		}
		stringBuilderBorn.append(" group by bornPlace.placeAllId");
		stringBuilderDeath.append(" group by deathPlace.placeAllId");

		Map<Integer, Long> returnValues = new HashMap<Integer, Long>();
		List tempValuesBorn;
		if (placeAllIds.size() > 0) {
			Query query = getEntityManager().createQuery(
					stringBuilderBorn.toString());
			tempValuesBorn = query.getResultList();
			for (Iterator i = tempValuesBorn.iterator(); i.hasNext();) {
				Object[] data = (Object[]) i.next();
				returnValues.put((Integer) data[0], (Long) data[1]);
			}
		}
		List tempValuesDeath;
		if (placeAllIds.size() > 0) {
			Query query = getEntityManager().createQuery(
					stringBuilderDeath.toString());
			tempValuesDeath = query.getResultList();
			for (Iterator i = tempValuesDeath.iterator(); i.hasNext();) {
				Object[] data = (Object[]) i.next();
				if (returnValues.containsKey((Integer) data[0])) {
					data[1] = returnValues.remove((Integer) data[0])
							+ (Long) data[1];
				}
				returnValues.put((Integer) data[0], (Long) data[1]);
			}
		}

		return returnValues;
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Page searchActiveEndPeoplePlace(String placeToSearch,
			PaginationFilter paginationFilter) throws PersistenceException {
		Page page = new Page(paginationFilter);

		Query query = null;
		String toSearch = new String(
				"FROM People WHERE (deathPlace.placeAllId=" + placeToSearch
						+ ") AND activeEnd!=NULL AND logicalDelete=false");

		if (paginationFilter.getTotal() == null) {
			String countQuery = "SELECT COUNT(*) " + toSearch;
			query = getEntityManager().createQuery(countQuery);
			page.setTotal(new Long((Long) query.getSingleResult()));
		}

		paginationFilter = generatePaginationFilterMYSQL(paginationFilter);

		query = getEntityManager().createQuery(
				toSearch
						+ getOrderByQuery(paginationFilter
								.getSortingCriterias()));

		query.setFirstResult(paginationFilter.getFirstRecord());
		query.setMaxResults(paginationFilter.getLength());

		page.setList(query.getResultList());

		return page;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Page searchActiveStartPeoplePlace(String placeToSearch,
			PaginationFilter paginationFilter) throws PersistenceException {
		Page page = new Page(paginationFilter);

		Query query = null;
		String toSearch = new String("FROM People WHERE (bornPlace.placeAllId="
				+ placeToSearch
				+ ") AND activeStart!=NULL AND logicalDelete=false");

		if (paginationFilter.getTotal() == null) {
			String countQuery = "SELECT COUNT(*) " + toSearch;
			query = getEntityManager().createQuery(countQuery);
			page.setTotal(new Long((Long) query.getSingleResult()));
		}

		paginationFilter = generatePaginationFilterMYSQL(paginationFilter);

		query = getEntityManager().createQuery(
				toSearch
						+ getOrderByQuery(paginationFilter
								.getSortingCriterias()));

		query.setFirstResult(paginationFilter.getFirstRecord());
		query.setMaxResults(paginationFilter.getLength());

		page.setList(query.getResultList());

		return page;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Page searchBirthPeoplePlace(String placeToSearch,
			PaginationFilter paginationFilter) throws PersistenceException {
		Page page = new Page(paginationFilter);

		Query query = null;
		String toSearch = new String("FROM People WHERE (bornPlace.placeAllId="
				+ placeToSearch
				+ ") AND activeStart=NULL AND logicalDelete=false");

		if (paginationFilter.getTotal() == null) {
			String countQuery = "SELECT COUNT(*) " + toSearch;
			query = getEntityManager().createQuery(countQuery);
			page.setTotal(new Long((Long) query.getSingleResult()));
		}

		paginationFilter = generatePaginationFilterMYSQL(paginationFilter);

		query = getEntityManager().createQuery(
				toSearch
						+ getOrderByQuery(paginationFilter
								.getSortingCriterias()));

		query.setFirstResult(paginationFilter.getFirstRecord());
		query.setMaxResults(paginationFilter.getLength());

		page.setList(query.getResultList());

		return page;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<People> searchChildLinkableToPerson(Integer personId,
			String searchText) throws PersistenceException {
		String[] outputFields = new String[] { "personId", "mapNameLf",
				"activeStart", "activeEnd", "bornYear", "deathYear" };

		FullTextSession fullTextSession = org.hibernate.search.Search
				.getFullTextSession(((HibernateEntityManager) getEntityManager())
						.getSession());

		QueryParser parserMapNameLf = new QueryParser(Version.LUCENE_30,
				"mapNameLf", fullTextSession.getSearchFactory().getAnalyzer(
						"peopleAnalyzer"));

		try {
			org.apache.lucene.search.Query queryMapNameLf = parserMapNameLf
					.parse(searchText.toLowerCase() + "*");

			BooleanQuery booleanQuery = new BooleanQuery();
			booleanQuery.add(new BooleanClause(queryMapNameLf,
					BooleanClause.Occur.SHOULD));
			String[] words = RegExUtils
					.splitPunctuationAndSpaceChars(searchText);
			for (String singleWord : words) {
				booleanQuery.add(new BooleanClause(new WildcardQuery(new Term(
						"altName.altName", singleWord.toLowerCase() + "*")),
						BooleanClause.Occur.SHOULD));
			}

			final FullTextQuery fullTextQuery = fullTextSession
					.createFullTextQuery(booleanQuery, People.class);
			// Projection permits to extract only a subset of domain class,
			// tuning application.
			fullTextQuery.setProjection(outputFields);
			// Projection returns an array of Objects, using Transformer we can
			// return a list of domain object
			fullTextQuery.setResultTransformer(Transformers
					.aliasToBean(People.class));

			return fullTextQuery.list();
		} catch (ParseException parseException) {
			logger.error("Unable to parse query with text " + searchText,
					parseException);
			return new ArrayList<People>(0);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Page searchDeathPeoplePlace(String placeToSearch,
			PaginationFilter paginationFilter) throws PersistenceException {
		Page page = new Page(paginationFilter);

		Query query = null;
		String toSearch = new String(
				"FROM People WHERE (deathPlace.placeAllId=" + placeToSearch
						+ ") AND activeEnd=NULL AND logicalDelete=false");

		if (paginationFilter.getTotal() == null) {
			String countQuery = "SELECT COUNT(*) " + toSearch;
			query = getEntityManager().createQuery(countQuery);
			page.setTotal(new Long((Long) query.getSingleResult()));
		}

		paginationFilter = generatePaginationFilterMYSQL(paginationFilter);

		query = getEntityManager().createQuery(
				toSearch
						+ getOrderByQuery(paginationFilter
								.getSortingCriterias()));

		query.setFirstResult(paginationFilter.getFirstRecord());
		query.setMaxResults(paginationFilter.getLength());

		page.setList(query.getResultList());

		return page;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Page searchFamilyPerson(String familyName, String familyNamePrefix,
			PaginationFilter paginationFilter) throws PersistenceException {
		Page page = new Page(paginationFilter);

		Query query = null;
		// MD: If the name of the family contains apostrophe, I replace it with
		// two apostrophes for the SQL query
		if (familyName.contains("'")) {
			familyName = familyName.replace("'", "\'\'");
		}

		String toSearch = new String(
				"FROM People WHERE personId IN (SELECT person.personId FROM org.medici.mia.domain.AltName WHERE (altName.altName like '"
						+ familyName
						+ "%' OR altName.altName like '% "
						+ familyName
						+ "%' OR altName.altName like '%-"
						+ familyName
						+ "%') AND altName.nameType like 'Family') AND logicalDelete = false");

		// MD: We ignore the familyNamePrefix, because e.g. the family "Medici"
		// is the same of "de' Medici"
		/*
		 * if(familyNamePrefix != null){ if(familyNamePrefix.contains("'"))
		 * familyNamePrefix = familyNamePrefix.replace("'", "\'\'"); toSearch +=
		 * " AND altName.namePrefix like '" + familyNamePrefix + "')"; }else{
		 * toSearch += ")"; }
		 */

		if (paginationFilter.getTotal() == null) {
			String countQuery = "SELECT COUNT(*) " + toSearch;
			query = getEntityManager().createQuery(countQuery);
			page.setTotal(new Long((Long) query.getSingleResult()));
		}

		paginationFilter = generatePaginationFilterMYSQL(paginationFilter);

		query = getEntityManager().createQuery(
				toSearch
						+ getOrderByQuery(paginationFilter
								.getSortingCriterias()));

		query.setFirstResult(paginationFilter.getFirstRecord());
		query.setMaxResults(paginationFilter.getLength());

		page.setList(query.getResultList());

		return page;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<People> searchFatherLinkableToPerson(String searchText)
			throws PersistenceException {
		// Query query = getEntityManager().createQuery(
		// "FROM AltName a WHERE a.mapNameLf LIKE :mapNameLf");

		Query query = getEntityManager().createQuery(
				"FROM AltName a WHERE a.altName LIKE :altName");
		query.setParameter("altName", "%" + searchText + "%");

		ArrayList<AltName> altNames = (ArrayList<AltName>) query
				.getResultList();
		ArrayList<People> peoples = new ArrayList<People>();
		if (altNames != null && !altNames.isEmpty()) {

			for (AltName altName : altNames) {
				// Added check if the person has logical delete = 1, reject it
				// from the response
				if (altName.getPerson() != null
						&& altName.getPerson().getLogicalDelete() == false) {
					peoples.add(altName.getPerson());
				}
			}

		}

		return peoples;

	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<People> searchPersonLinkableToPerson(String searchText)
			throws PersistenceException {
		// Query query = getEntityManager().createQuery(
		// "FROM AltName a WHERE a.mapNameLf LIKE :mapNameLf");

		Query query = getEntityManager().createQuery(
				"FROM AltName a WHERE a.altName LIKE :altName");
		query.setParameter("altName", "%" + searchText + "%");

		ArrayList<AltName> altNames = (ArrayList<AltName>) query
				.getResultList();
		ArrayList<People> peoples = new ArrayList<People>();
		if (altNames != null && !altNames.isEmpty()) {

			for (AltName altName : altNames) {
				// Added check if the person has logical delete = 1, reject it
				// from the response
				if (altName.getPerson() != null
						&& altName.getPerson().getLogicalDelete() == false) {
					peoples.add(altName.getPerson());
				}
			}

		}

		return peoples;

	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<People> searchMotherLinkableToPerson(String searchText)
			throws PersistenceException {
		String[] outputFields = new String[] { "personId", "mapNameLf",
				"activeStart", "activeEnd", "bornYear", "deathYear" };

		FullTextSession fullTextSession = org.hibernate.search.Search
				.getFullTextSession(((HibernateEntityManager) getEntityManager())
						.getSession());

		QueryParser parserMapNameLf = new QueryParser(Version.LUCENE_30,
				"mapNameLf", fullTextSession.getSearchFactory().getAnalyzer(
						"peopleAnalyzer"));

		try {
			org.apache.lucene.search.Query queryMapNameLf = parserMapNameLf
					.parse(searchText.toLowerCase() + "*");

			BooleanQuery booleanQuery = new BooleanQuery();
			booleanQuery.add(new BooleanClause(queryMapNameLf,
					BooleanClause.Occur.SHOULD));
			String[] words = RegExUtils
					.splitPunctuationAndSpaceChars(searchText);
			for (String singleWord : words) {
				booleanQuery.add(new BooleanClause(new WildcardQuery(new Term(
						"altName.altName", singleWord.toLowerCase() + "*")),
						BooleanClause.Occur.SHOULD));
			}

			final FullTextQuery fullTextQuery = fullTextSession
					.createFullTextQuery(booleanQuery, People.class);
			// Projection permits to extract only a subset of domain class,
			// tuning application.
			fullTextQuery.setProjection(outputFields);
			// Projection returns an array of Objects, using Transformer we can
			// return a list of domain object
			fullTextQuery.setResultTransformer(Transformers
					.aliasToBean(People.class));

			return fullTextQuery.list();
		} catch (ParseException parseException) {
			logger.error("Unable to parse query with text " + searchText,
					parseException);
			return new ArrayList<People>(0);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Page searchPeople(Search searchContainer,
			PaginationFilter paginationFilter) throws PersistenceException {
		// We prepare object of return method.
		Page page = new Page(paginationFilter);

		// We obtain hibernate-search session
		FullTextSession fullTextSession = org.hibernate.search.Search
				.getFullTextSession(((HibernateEntityManager) getEntityManager())
						.getSession());

		// We convert AdvancedSearchContainer to luceneQuery
		org.apache.lucene.search.Query query = searchContainer.toLuceneQuery();
		logger.info("Lucene Query " + query.toString());

		// We execute search
		org.hibernate.search.FullTextQuery fullTextQuery = fullTextSession
				.createFullTextQuery(query, People.class);

		// We set size of result.
		if (paginationFilter.getTotal() == null) {
			page.setTotal(new Long(fullTextQuery.getResultSize()));
		}

		// We set pagination
		fullTextQuery.setFirstResult(paginationFilter.getFirstRecord());
		fullTextQuery.setMaxResults(paginationFilter.getLength());

		// We manage sorting (this manages sorting on multiple fields)
		paginationFilter = this
				.generatePaginationFilterHibernateSearch(paginationFilter);
		List<SortingCriteria> sortingCriterias = paginationFilter
				.getSortingCriterias();
		if (sortingCriterias.size() > 0) {
			SortField[] sortFields = new SortField[sortingCriterias.size()];
			for (int i = 0; i < sortingCriterias.size(); i++) {
				sortFields[i] = new SortField(
						sortingCriterias.get(i).getColumn(),
						sortingCriterias.get(i).getColumnType(),
						(sortingCriterias.get(i).getOrder().equals(Order.ASC) ? true
								: false));
			}
			fullTextQuery.setSort(new Sort(sortFields));
		}

		// We set search result on return method
		page.setList(fullTextQuery.list());

		return page;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<People> searchPeople(String searchText)
			throws PersistenceException {
		String[] outputFields = new String[] { "personId", "mapNameLf",
				"activeStart", "activeEnd", "bornYear", "deathYear" };

		FullTextSession fullTextSession = org.hibernate.search.Search
				.getFullTextSession(((HibernateEntityManager) getEntityManager())
						.getSession());

		QueryParser parserMapNameLf = new QueryParser(Version.LUCENE_30,
				"mapNameLf", fullTextSession.getSearchFactory().getAnalyzer(
						"peopleAnalyzer"));

		try {
			org.apache.lucene.search.Query queryMapNameLf = parserMapNameLf
					.parse(searchText.toLowerCase() + "*");

			BooleanQuery booleanQuery = new BooleanQuery();
			booleanQuery.add(new BooleanClause(queryMapNameLf,
					BooleanClause.Occur.SHOULD));
			String[] words = RegExUtils
					.splitPunctuationAndSpaceChars(searchText);
			for (String singleWord : words) {
				booleanQuery.add(new BooleanClause(new WildcardQuery(new Term(
						"altName.altName", singleWord.toLowerCase() + "*")),
						BooleanClause.Occur.SHOULD));
			}

			final FullTextQuery fullTextQuery = fullTextSession
					.createFullTextQuery(booleanQuery, People.class);
			// Projection permits to extract only a subset of domain class,
			// tuning application.
			fullTextQuery.setProjection(outputFields);
			// Projection returns an array of Objects, using Transformer we can
			// return a list of domain object
			fullTextQuery.setResultTransformer(Transformers
					.aliasToBean(People.class));

			return fullTextQuery.list();
		} catch (ParseException parseException) {
			logger.error("Unable to parse query with text " + searchText,
					parseException);
			return new ArrayList<People>(0);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<People> searchPersonLinkableToDocument(
			List<Integer> peopleIdList, String searchText)
			throws PersistenceException {
		String[] outputFields = new String[] { "personId", "mapNameLf",
				"activeStart", "activeEnd", "bornYear", "deathYear" };

		FullTextSession fullTextSession = org.hibernate.search.Search
				.getFullTextSession(((HibernateEntityManager) getEntityManager())
						.getSession());

		QueryParser parserMapNameLf = new QueryParser(Version.LUCENE_30,
				"mapNameLf", fullTextSession.getSearchFactory().getAnalyzer(
						"peopleAnalyzer"));

		try {
			org.apache.lucene.search.Query queryMapNameLf = parserMapNameLf
					.parse(searchText.toLowerCase() + "*");
			BooleanQuery booleanQuery = new BooleanQuery();
			booleanQuery.add(new BooleanClause(queryMapNameLf,
					BooleanClause.Occur.SHOULD));
			String[] words = RegExUtils
					.splitPunctuationAndSpaceChars(searchText);
			for (String singleWord : words) {
				booleanQuery.add(new BooleanClause(new WildcardQuery(new Term(
						"altName.altName", singleWord.toLowerCase() + "*")),
						BooleanClause.Occur.SHOULD));
			}
			for (int i = 0; i < peopleIdList.size(); i++) {
				booleanQuery.add(new BooleanClause(new TermQuery(new Term(
						"personId", peopleIdList.get(i).toString())),
						BooleanClause.Occur.MUST_NOT));
			}

			final FullTextQuery fullTextQuery = fullTextSession
					.createFullTextQuery(booleanQuery, People.class);
			// Projection permits to extract only a subset of domain class,
			// tuning application.
			fullTextQuery.setProjection(outputFields);
			// Projection returns an array of Objects, using Transformer we can
			// return a list of domain object
			fullTextQuery.setResultTransformer(Transformers
					.aliasToBean(People.class));

			return fullTextQuery.list();
		} catch (ParseException parseException) {
			logger.error("Unable to parse query with text " + searchText,
					parseException);
			return new ArrayList<People>(0);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<People> searchRecipientsPeople(List<Integer> peopleIdList,
			String searchText) throws PersistenceException {
		String[] outputFields = new String[] { "personId", "mapNameLf",
				"activeStart", "activeEnd", "bornYear", "deathYear" };

		FullTextSession fullTextSession = org.hibernate.search.Search
				.getFullTextSession(((HibernateEntityManager) getEntityManager())
						.getSession());

		QueryParser parserMapNameLf = new QueryParser(Version.LUCENE_30,
				"mapNameLf", fullTextSession.getSearchFactory().getAnalyzer(
						"peopleAnalyzer"));

		try {
			org.apache.lucene.search.Query queryMapNameLf = parserMapNameLf
					.parse(searchText.toLowerCase() + "*");

			BooleanQuery booleanQuery = new BooleanQuery();
			booleanQuery.add(new BooleanClause(queryMapNameLf,
					BooleanClause.Occur.SHOULD));
			String[] words = RegExUtils
					.splitPunctuationAndSpaceChars(searchText);
			for (String singleWord : words) {
				booleanQuery.add(new BooleanClause(new WildcardQuery(new Term(
						"altName.altName", singleWord.toLowerCase() + "*")),
						BooleanClause.Occur.SHOULD));
			}
			for (int i = 0; i < peopleIdList.size(); i++) {
				booleanQuery.add(new BooleanClause(new TermQuery(new Term(
						"personId", peopleIdList.get(i).toString())),
						BooleanClause.Occur.MUST_NOT));
			}

			final FullTextQuery fullTextQuery = fullTextSession
					.createFullTextQuery(booleanQuery, People.class);
			// Projection permits to extract only a subset of domain class,
			// tuning application.
			fullTextQuery.setProjection(outputFields);
			// Projection returns an array of Objects, using Transformer we can
			// return a list of domain object
			fullTextQuery.setResultTransformer(Transformers
					.aliasToBean(People.class));

			fullTextQuery.toString();
			return fullTextQuery.list();
		} catch (ParseException parseException) {
			logger.error("Unable to parse query with text " + searchText,
					parseException);
			return new ArrayList<People>(0);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Page searchRoleCatPeople(String roleCatToSearch,
			PaginationFilter paginationFilter) throws PersistenceException {
		Page page = new Page(paginationFilter);

		Query query = null;
		String toSearch = new String(
				"FROM People WHERE personId IN (SELECT DISTINCT person.personId FROM org.medici.mia.domain.PoLink WHERE titleOccList.roleCat.roleCatId="
						+ roleCatToSearch + " AND person.logicalDelete=false)");

		if (paginationFilter.getTotal() == null) {
			String countQuery = "SELECT COUNT(*) " + toSearch;
			query = getEntityManager().createQuery(countQuery);
			page.setTotal(new Long((Long) query.getSingleResult()));
		}

		paginationFilter = generatePaginationFilterMYSQL(paginationFilter);

		query = getEntityManager().createQuery(
				toSearch
						+ getOrderByQuery(paginationFilter
								.getSortingCriterias()));

		query.setFirstResult(paginationFilter.getFirstRecord());
		query.setMaxResults(paginationFilter.getLength());

		page.setList(query.getResultList());

		return page;
	}

	/**
	 * This method search senders using jpa implementation.
	 * 
	 * @param alias
	 * @return
	 * @throws PersistenceException
	 * @deprecated
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<People> searchSendersJpaImpl(String alias)
			throws PersistenceException {
		// Create criteria objects
		CriteriaBuilder criteriaBuilder = getEntityManager()
				.getCriteriaBuilder();
		CriteriaQuery<People> criteriaQuery = criteriaBuilder
				.createQuery(People.class);
		Root root = criteriaQuery.from(People.class);

		// Construct literal and predicates
		Expression<String> literal = criteriaBuilder.upper(criteriaBuilder
				.literal("%" + alias + "%"));
		Predicate predicateFirst = criteriaBuilder.like(
				criteriaBuilder.upper(root.get("first")), literal);
		Predicate predicateLast = criteriaBuilder.like(
				criteriaBuilder.upper(root.get("last")), literal);

		// Add where clause
		criteriaQuery.where(criteriaBuilder.or(predicateFirst, predicateLast));

		// create query using criteria
		TypedQuery<People> typedQuery = getEntityManager().createQuery(
				criteriaQuery);

		return typedQuery.getResultList();
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<People> searchSendersPeople(List<Integer> peopleIdList,
			String searchText) throws PersistenceException {
		String[] outputFields = new String[] { "personId", "mapNameLf",
				"activeStart", "activeEnd", "bornYear", "deathYear" };

		FullTextSession fullTextSession = org.hibernate.search.Search
				.getFullTextSession(((HibernateEntityManager) getEntityManager())
						.getSession());

		QueryParser parserMapNameLf = new QueryParser(Version.LUCENE_30,
				"mapNameLf", fullTextSession.getSearchFactory().getAnalyzer(
						"peopleAnalyzer"));

		try {
			org.apache.lucene.search.Query queryMapNameLf = parserMapNameLf
					.parse(searchText.toLowerCase() + "*");

			BooleanQuery booleanQuery = new BooleanQuery();
			booleanQuery.add(new BooleanClause(queryMapNameLf,
					BooleanClause.Occur.SHOULD));
			String[] words = RegExUtils
					.splitPunctuationAndSpaceChars(searchText);
			for (String singleWord : words) {
				booleanQuery.add(new BooleanClause(new WildcardQuery(new Term(
						"altName.altName", singleWord.toLowerCase() + "*")),
						BooleanClause.Occur.SHOULD));
			}
			for (int i = 0; i < peopleIdList.size(); i++) {
				booleanQuery.add(new BooleanClause(new TermQuery(new Term(
						"personId", peopleIdList.get(i).toString())),
						BooleanClause.Occur.MUST_NOT));
			}

			final FullTextQuery fullTextQuery = fullTextSession
					.createFullTextQuery(booleanQuery, People.class);
			// Projection permits to extract only a subset of domain class,
			// tuning application.
			fullTextQuery.setProjection(outputFields);
			fullTextQuery.setResultTransformer(Transformers
					.aliasToBean(People.class));

			return fullTextQuery.list();
		} catch (ParseException parseException) {
			logger.error("Unable to parse query with text " + searchText,
					parseException);
			return new ArrayList<People>(0);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<People> searchSpouseLinkableToPerson(String searchText)
			throws PersistenceException {
		String[] outputFields = new String[] { "personId", "mapNameLf",
				"activeStart", "activeEnd", "bornYear", "deathYear" };

		FullTextSession fullTextSession = org.hibernate.search.Search
				.getFullTextSession(((HibernateEntityManager) getEntityManager())
						.getSession());

		QueryParser parserMapNameLf = new QueryParser(Version.LUCENE_30,
				"mapNameLf", fullTextSession.getSearchFactory().getAnalyzer(
						"peopleAnalyzer"));

		try {
			org.apache.lucene.search.Query queryMapNameLf = parserMapNameLf
					.parse(searchText.toLowerCase() + "*");

			BooleanQuery booleanQuery = new BooleanQuery();
			booleanQuery.add(new BooleanClause(queryMapNameLf,
					BooleanClause.Occur.SHOULD));
			String[] words = RegExUtils
					.splitPunctuationAndSpaceChars(searchText);
			for (String singleWord : words) {
				booleanQuery.add(new BooleanClause(new WildcardQuery(new Term(
						"altName.altName", singleWord.toLowerCase() + "*")),
						BooleanClause.Occur.SHOULD));
			}

			final FullTextQuery fullTextQuery = fullTextSession
					.createFullTextQuery(booleanQuery, People.class);
			// Projection permits to extract only a subset of domain class,
			// tuning application.
			fullTextQuery.setProjection(outputFields);
			// Projection returns an array of Objects, using Transformer we can
			// return a list of domain object
			fullTextQuery.setResultTransformer(Transformers
					.aliasToBean(People.class));

			return fullTextQuery.list();
		} catch (ParseException parseException) {
			logger.error("Unable to parse query with text " + searchText,
					parseException);
			return new ArrayList<People>(0);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Page searchTitlesOrOccupationsPeople(String titleOccToSearch,
			PaginationFilter paginationFilter) throws PersistenceException {
		Page page = new Page(paginationFilter);

		Query query = null;
		// String toSearch = new
		// String("FROM People WHERE personId IN (SELECT DISTINCT person.personId FROM org.medici.mia.domain.PoLink WHERE titleOccList.titleOccId="
		// + titleOccToSearch + ")");
		// MD: The next query is builded for test if is possible order result by
		// date of Title Occupation
		String toSearch = new String(
				"FROM People p, org.medici.mia.domain.PoLink t WHERE p.personId = t.person.personId AND t.titleOccList.titleOccId="
						+ titleOccToSearch + " AND p.logicalDelete=false");

		if (paginationFilter.getTotal() == null) {
			String countQuery = "SELECT COUNT(*) " + toSearch;
			query = getEntityManager().createQuery(countQuery);
			page.setTotal(new Long((Long) query.getSingleResult()));
		}

		// MD: We have a pagination filter already parameterized
		// paginationFilter = generatePaginationFilterMYSQL(paginationFilter);

		query = getEntityManager().createQuery(
				"SELECT t.person "
						+ toSearch
						+ getOrderByQuery(paginationFilter
								.getSortingCriterias()));

		query.setFirstResult(paginationFilter.getFirstRecord());
		query.setMaxResults(paginationFilter.getLength());

		page.setList(query.getResultList());

		return page;
	}

	@Override
	public People findPeopleById(Integer personId) throws PersistenceException {
		People people = getEntityManager().find(People.class, personId);
		return people;
	}

	@Override
	public List<People> findPeople(String peopleName)
			throws PersistenceException {

		Query query = getEntityManager().createQuery(
				"FROM AltName a WHERE a.altName LIKE :altName");
		query.setParameter("altName", "%" + peopleName + "%");

		ArrayList<AltName> altNames = (ArrayList<AltName>) query
				.getResultList();
		ArrayList<People> peoples = new ArrayList<People>();
		if (altNames != null && !altNames.isEmpty()) {

			for (AltName altName : altNames) {
				// Added check if the person has logical delete = 1, reject it
				// from the response
				if (altName.getPerson() != null
						&& (altName.getPerson().getLogicalDelete() == null || altName
								.getPerson().getLogicalDelete() == false)) {

					if (!peoples.contains(altName.getPerson())) {
						peoples.add(altName.getPerson());
					}
				}
			}

		}

		return peoples;

	}

	@Override
	public Integer getAllCountPeopleBySerachWords(String searchWord)
			throws PersistenceException {
		if (searchWord == null || searchWord.isEmpty()) {
			return 0;
		}
		searchWord = searchWord.replaceAll("\\“", "\"");
		searchWord = searchWord.replaceAll(" ", "");
		String[] words = searchWord.split("\"");

		List<String> list = new ArrayList<String>();

		for (int i = 0; i < words.length; i++) {
			if (!words[i].isEmpty()) {
				list.add(words[i].trim());
			}
		}

		ArrayList<People> peoples = new ArrayList<People>();

		for (String peopleName : list) {

			Query query = getEntityManager().createQuery(
					"FROM AltName a WHERE a.altName LIKE :altName");
			query.setParameter("altName", "%" + peopleName + "%");

			ArrayList<AltName> altNames = (ArrayList<AltName>) query
					.getResultList();
			if (altNames != null && !altNames.isEmpty()) {

				for (AltName altName : altNames) {
					// Added check if the person has logical delete = 1, reject
					// it
					// from the response
					if (altName.getPerson() != null
							&& (altName.getPerson().getLogicalDelete() == null || altName
									.getPerson().getLogicalDelete() == false)) {

						if (!peoples.contains(altName.getPerson())) {
							peoples.add(altName.getPerson());
						}
					}
				}

			}
		}

		return peoples.size();
	}

	@Override
	public Integer getAllCountPeople() throws PersistenceException {

		ArrayList<People> people = new ArrayList<People>();

		Query query = getEntityManager().createQuery("FROM AltName a ");

		ArrayList<AltName> altNames = (ArrayList<AltName>) query
				.getResultList();
		if (altNames != null && !altNames.isEmpty()) {

			for (AltName altName : altNames) {
				// Added check if the person has logical delete = 1, reject
				// it
				// from the response
				if (altName.getPerson() != null
						&& (altName.getPerson().getLogicalDelete() == null || altName
								.getPerson().getLogicalDelete() == false)) {

					if (!people.contains(altName.getPerson())) {
						people.add(altName.getPerson());
					}
				}
			}

		}

		return people.size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<People> getNews(Date date) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"FROM People e WHERE e.dateCreated>=:dateCreated OR e.lastUpdate>=:lastUpdate");
		query.setParameter("dateCreated", date);
		query.setParameter("lastUpdate", date);

		return (List<People>) query.getResultList();

	}

	@Override
	public List<People> getPersons(String searchWord)
			throws PersistenceException {

		if (searchWord == null || searchWord.isEmpty()) {
			return null;
		}
		
		String[] words = searchWord.trim().split("\\s+");

		List<String> list = new ArrayList<String>();

		for (int i = 0; i < words.length; i++) {
			if (!words[i].trim().isEmpty()) {
				if (i == 0) { // First word of a search string should have length >= 3
					if (words[i].trim().length() >= 3) {
						list.add(words[i].trim());
					}
				}
				else {
					list.add(words[i].trim());
				}
			}
		}

		List<List<AltName>> altNameQueries = new ArrayList<List<AltName>>();
		for(String word: list){
			String jpaQuery = "select distinct a.*,"
					+ "(SELECT documentsCount FROM tblPeople p WHERE p.PersonID = a.PersonID) as documentsCount "
					+ "from tblAltNames a "
					+ "where a.altName LIKE '%" + word + "%' ORDER BY documentsCount DESC, AltName ASC";
			Query query = getEntityManager().createNativeQuery(jpaQuery, AltName.class);
			@SuppressWarnings("unchecked")
			List<AltName> queryRes = (List<AltName>) query.getResultList();
			altNameQueries.add(queryRes);
		}

		List<AltName> altNames = new ArrayList<AltName>();

		if (altNameQueries.size() > 1) {
			for (int i = 0; i < altNameQueries.size(); i++) {
				for (AltName rec1 : altNameQueries.get(i)) {
					for (int j = 0; j < altNameQueries.size(); j++) {
						if (j == i)
							continue;
						for (AltName rec2 : altNameQueries.get(j)) {
							if (rec1.getPerson().getPersonId() == rec2
									.getPerson().getPersonId()) {
								altNames.add(rec2);
							}
						}
					}
				}
			}
		} else if (altNameQueries.size() == 1) {
			altNames = altNameQueries.get(0);
		}
		if (!altNames.isEmpty()) {

			ArrayList<People> persons = new ArrayList<People>();

			for (AltName altName : altNames) {
				// Added check if the person has logical delete = 1, reject it
				// from the response
				if (altName.getPerson() != null
						&& (altName.getPerson().getLogicalDelete() == null || !altName
								.getPerson().getLogicalDelete())) {
					if (!persons.contains(altName.getPerson())) {
						persons.add(altName.getPerson());
					}
				}
			}

			return persons;

		}

		return null;

	}

	@Override
	public List<People> getComplexPersonsFromAltName(
			PeopleComplexSearchJson searchReq,
			PeopleSearchPaginationParam pagParam) throws PersistenceException {

		String queryString = ComplexSearchUtils
				.getPeopleFromAltNameComplexQueryString(searchReq);
		queryString = queryString + " order By " +pagParam.getOrderColumn()+ " " + pagParam.getAscOrdDesc();
		Query query = getEntityManager().createNativeQuery(queryString,
				AltName.class);
		@SuppressWarnings("unchecked")
		List<AltName> altNames = (List<AltName>) query.getResultList();

		if (altNames != null && !altNames.isEmpty()) {

			ArrayList<People> persons = new ArrayList<People>();

			for (AltName altName : altNames) {
				// Added check if the person has logical delete = 1, reject it
				// from the response
				if (altName.getPerson() != null
						&& (altName.getPerson().getLogicalDelete() == null || altName
								.getPerson().getLogicalDelete() == false)) {

					if (!persons.contains(altName.getPerson())) {
						persons.add(altName.getPerson());
					}
				}
			}

			return persons;

		}

		return null;

	}

	@Override
	public List<People> getComplexPeopleFromTblPeople(
			PeopleComplexSearchJson searchReq) throws PersistenceException {

		String queryString = ComplexSearchUtils
				.getPeopleFromTblPeopleComplexQueryString(searchReq);
		Query query = getEntityManager().createNativeQuery(queryString,
				People.class);
		@SuppressWarnings("unchecked")
		List<People> people = (List<People>) query.getResultList();
		List<People> notDeletedPeople = new ArrayList<People>();

		if (people != null && !people.isEmpty()) {

			for (People person : people) {
				// Added check if the person has logical delete = 1, reject it
				// from the response
				if (person.getLogicalDelete() == null
						|| person.getLogicalDelete() == false) {
					notDeletedPeople.add(person);

				}
			}

			return notDeletedPeople;

		}

		return null;

	}

@Override
	public List<People> getComplexPeopleFromTblPeople(
			PeopleComplexSearchJson searchReq, PeopleSearchPaginationParam pagination) throws PersistenceException {

		String queryString = ComplexSearchUtils
				.getPeopleComplexQueryString(searchReq);
		if(pagination != null){
			if(pagination.getOrderColumn() != null && !pagination.getOrderColumn().isEmpty()){
				queryString = queryString + " order by " + pagination.getOrderColumn();
				if(pagination.getAscOrdDesc() != null && (pagination.getAscOrdDesc().equalsIgnoreCase("asc")
						|| pagination.getAscOrdDesc().equalsIgnoreCase("desc"))){
					queryString = queryString + " " + pagination.getAscOrdDesc();
				}
			}
		}

		Query query = getEntityManager().createNativeQuery(queryString,
				People.class);
		if(pagination != null && pagination.getFirstResult() != null && pagination.getFirstResult() >= 0
			&& pagination.getMaxResult() != null && pagination.getMaxResult() > 0){
			query.setFirstResult(pagination.getFirstResult());
			query.setMaxResults(pagination.getMaxResult());
		}
		@SuppressWarnings("unchecked")
		List<People> people = (List<People>) query.getResultList();
		List<People> notDeletedPeople = new ArrayList<People>();

		if (people != null && !people.isEmpty()) {

			for (People person : people) {
				// Added check if the person has logical delete = 1, reject it
				// from the response
				if (person.getLogicalDelete() == null
						|| person.getLogicalDelete() == false) {
					notDeletedPeople.add(person);

				}
			}

			return notDeletedPeople;

		}

		return null;

	}

	@Override
	public List<Integer> getAllCountPeopleFromTblPeople(
			AllCountWordSearchJson searchReq) throws PersistenceException {

		String queryString = ComplexSearchUtils
				.getPeopleAllCountFromTblPeople(searchReq);
		if (queryString == null)
			return new ArrayList<Integer>();

		Query query = getEntityManager().createNativeQuery(queryString);
		@SuppressWarnings("unchecked")
		List<Integer> people = (List<Integer>) query.getResultList();

		if (people != null && !people.isEmpty()) {
			return people;

		}

		return new ArrayList<Integer>();

	}

	@Override
	public int getAllCountPeople(
			AllCountWordSearchJson searchReq) throws PersistenceException {

		String queryString = ComplexSearchUtils
				.getPeopleAllCount(searchReq);
		try {
			Query query = getEntityManager().createNativeQuery(queryString);
			@SuppressWarnings("unchecked")
			BigInteger people = (BigInteger) query.getSingleResult();
			if(people != null) return people.intValue();
			else return 0;
		} catch (PersistenceException e){
			return 0;
		}
	}

	@Override
	public void updatePeopleDocumentsCount(List<Integer> peopleIds) throws PersistenceException {
		
		for (Integer peopleId : peopleIds) {
			
			updatePersonDocumentsCount(peopleId);
		}
	}
	
	@Override
	public void updatePersonDocumentsCount(int personId) throws PersistenceException {
		int documentsCount = getDocumentsCount(personId);
		
		String updateCountQueryCode = String.format(
				"UPDATE People p " +
				"SET p.documentsCount = %d WHERE p.personId = %s", 
				documentsCount, personId);
		Query updateCountQuery = getEntityManager().createQuery(updateCountQueryCode);
		updateCountQuery.executeUpdate();
	}
	
	private int getDocumentsCount(int personId) {
		PeopleAdvancedSearchJson peopleAdvancedSearchJson = new PeopleAdvancedSearchJson();

		String id = String.valueOf(personId);
		peopleAdvancedSearchJson.setPeople( new ArrayList<String>(Arrays.asList(id)));
		
		String countQueryCode = peopleAdvancedSearchJson.getQueries().get(0);
		Query countQuery = getEntityManager().createNativeQuery(countQueryCode);
		int documentsCount = countQuery.getResultList().size();
		
		return documentsCount;
	}
	
	@Override
	public int getMinPersonId() {
		int personId = -1;
		
		try {
			Query query = getEntityManager().createQuery("SELECT MIN(personId) FROM People ORDER BY personId");
			personId = (Integer) query.getSingleResult();
		} catch (PersistenceException e){
			e.printStackTrace();
		}
		
		return personId;
	}
	
	public int getMaxPersonId() {
		int personId = -1;
		
		try {
			Query query = getEntityManager().createQuery("SELECT MAX(personId) FROM People ORDER BY personId");
			personId = (Integer) query.getSingleResult();
		} catch (PersistenceException e){
			e.printStackTrace();
		}
		
		return personId;
	}
	
	@Override
	public List<Integer> getIdsFromTo(int from, int to) throws PersistenceException {
		String queryCode = String.format("SELECT p.PERSONID FROM tblPeople p ORDER BY p.PERSONID LIMIT %d, %d", from, to);

		Query query = getEntityManager().createNativeQuery(queryCode);
		List<Integer> people = (List<Integer>) query.getResultList();

		return people;
	}

	@Override
	public List<Sitemap> generatePeopleSitemaps(
			ChangeFrequency changeFrequency, Double priority) {
		
		String queryCode = 
			"SELECT p.PERSONID, p.DateCreated, p.LastUpdate FROM tblPeople p "
			+ "WHERE p.LOGICALDELETE = 0";

		Query query = getEntityManager().createNativeQuery(queryCode);
				
		@SuppressWarnings("unchecked")
		List<Object[]> sitemapsInfos = query.getResultList();
				
		String baseUrl = HtmlUtils.getSectionUrl(
				HtmlUtils.PEOPLE_SECTION_PATH);
		
		SitemapSectionConfiguration sitemapSectionConfiguration = 
				new SitemapSectionConfiguration(
						baseUrl, changeFrequency, priority);
			
		return sitemapSectionConfiguration.createSitemaps(sitemapsInfos);
	} 
}
