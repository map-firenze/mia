package org.medici.mia.dao.people;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import org.medici.mia.common.json.complexsearch.PeopleComplexSearchJson;
import org.medici.mia.common.json.search.AllCountWordSearchJson;
import org.medici.mia.common.pagination.Page;
import org.medici.mia.common.pagination.PaginationFilter;
import org.medici.mia.common.search.Search;
import org.medici.mia.controller.genericsearch.PeopleSearchPaginationParam;
import org.medici.mia.dao.Dao;
import org.medici.mia.domain.People;
import org.medici.mia.domain.Sitemap;
import org.medici.mia.domain.Sitemap.ChangeFrequency;
import org.medici.mia.exception.ApplicationThrowable;

public interface PeopleDAO extends Dao<Integer, People> {

	/**
	 * 
	 * @param inputDate
	 * @return
	 * @throws PersistenceException
	 */
	Long countPeopleCreatedAfterDate(Date inputDate)
			throws PersistenceException;

	/**
	 * This method returns last entry {@link org.medici.mia.domain.People}
	 * created on database.
	 * 
	 * @return Last entry {@link org.medici.mia.domain.People}
	 * 
	 * @throws PersistenceException
	 */
	People findLastEntryPerson() throws PersistenceException;

	/**
	 * 
	 * @param placeAllId
	 * @return
	 * @throws PersistenceException
	 */
	Integer findNumberOfActiveEndInPlace(Integer placeAllId)
			throws PersistenceException;

	/**
	 * 
	 * @param placeAllId
	 * @return
	 * @throws PersistenceException
	 */
	Integer findNumberOfActiveStartInPlace(Integer placeAllId)
			throws PersistenceException;

	/**
	 * 
	 * @param placeAllId
	 * @return
	 * @throws PersistenceException
	 */
	Integer findNumberOfBirthInPlace(Integer placeAllId)
			throws PersistenceException;

	/**
	 * 
	 * @param placeAllId
	 * @return
	 * @throws PersistenceException
	 */
	Integer findNumberOfDeathInPlace(Integer placeAllId)
			throws PersistenceException;

	/**
	 * 
	 * @param placeAllIds
	 * @return
	 * @throws PersistenceException
	 */
	Map<Integer, Long> findNumbersOfPeopleRelatedPlace(List<Integer> placeAllIds)
			throws PersistenceException;

	/**
	 * This method generate Hibernate search index.
	 * 
	 * @throws PersistenceException
	 */
	void generateIndex() throws PersistenceException;

	/**
	 * 
	 * @param placeToSearch
	 * @param paginationFilter
	 * @return
	 * @throws PersistenceException
	 */
	Page searchActiveEndPeoplePlace(String placeToSearch,
			PaginationFilter paginationFilter) throws PersistenceException;

	/**
	 * 
	 * @param placeToSearch
	 * @param paginationFilter
	 * @return
	 * @throws PersistenceException
	 */
	Page searchActiveStartPeoplePlace(String placeToSearch,
			PaginationFilter paginationFilter) throws PersistenceException;

	/**
	 * 
	 * @param placeToSearch
	 * @param paginationFilter
	 * @return
	 * @throws PersistenceException
	 */
	Page searchBirthPeoplePlace(String placeToSearch,
			PaginationFilter paginationFilter) throws PersistenceException;

	/**
	 * This method searches for children which could be related to a person
	 * which contains a text parameter (String query).
	 * 
	 * @param personId
	 *            Person identifier
	 * @param query
	 *            Text to be searched
	 * @return A List<People> of children that could be related to a person.
	 * @throws PersistenceException
	 */
	List<People> searchChildLinkableToPerson(Integer personId, String query)
			throws PersistenceException;

	/**
	 * 
	 * @param placeToSearch
	 * @param paginationFilter
	 * @return
	 * @throws PersistenceException
	 */
	Page searchDeathPeoplePlace(String placeToSearch,
			PaginationFilter paginationFilter) throws PersistenceException;

	/**
	 * 
	 * @param familyToSearch
	 * @param familyNamePrefix
	 * @param paginationFilter
	 * @return
	 * @throws PersistenceException
	 */
	Page searchFamilyPerson(String familyName, String familyNamePrefix,
			PaginationFilter paginationFilter) throws PersistenceException;

	/**
	 * This method searches for fathers which could be related to a person which
	 * contains a text parameter (String query).
	 * 
	 * @param query
	 *            Text to be searched
	 * @return A List<People> of fathers that could be related to a person.
	 * @throws PersistenceException
	 */
	List<People> searchPersonLinkableToPerson(String query)
			throws PersistenceException;

	/**
	 * This method searches for fathers which could be related to a person which
	 * contains a text parameter (String query).
	 * 
	 * @param query
	 *            Text to be searched
	 * @return A List<People> of fathers that could be related to a person.
	 * @throws PersistenceException
	 */
	List<People> searchFatherLinkableToPerson(String query)
			throws PersistenceException;

	/**
	 * This method searches for mothers which could be related to a person which
	 * contains a text parameter (String query).
	 * 
	 * @param query
	 *            Text to be searched
	 * @return A List<People> of mothers that could be related to a person.
	 * @throws PersistenceException
	 */
	List<People> searchMotherLinkableToPerson(String query)
			throws PersistenceException;

	/**
	 * This method searches for person entities which contains the parameters
	 * set in {@link org.medici.mia.common.search} object and return a result
	 * page.
	 * 
	 * @param searchContainer
	 * @param paginationFilter
	 * @return A result Page of people.
	 * @throws PersistenceException
	 */
	Page searchPeople(Search searchContainer, PaginationFilter paginationFilter)
			throws PersistenceException;

	/**
	 * This method searches for person entities which contains the parameters
	 * set in query
	 * 
	 * @param query
	 * @return
	 * @throws PersistenceException
	 */
	List<People> searchPeople(String searchText) throws PersistenceException;

	/**
	 * This method searches for person entities which could be related to a
	 * document which contains a text parameter (String query).
	 * 
	 * @param peopleIdList
	 *            List of person identifier
	 * @param searchText
	 *            Text to be searched
	 * @return A List<People> of person entities that could be related to a
	 *         document.
	 * @throws PersistenceException
	 */
	List<People> searchPersonLinkableToDocument(List<Integer> peopleIdList,
			String searchText) throws PersistenceException;

	/**
	 * This method searches for recipients which could be related to a document
	 * which contains a text parameter (String searchText).
	 * 
	 * @param peopleIdList
	 *            List of person identifier
	 * @param searchText
	 *            Text to be searched
	 * @return A List<People> of recipients that could be related to a document.
	 * @throws PersistenceException
	 */
	List<People> searchRecipientsPeople(List<Integer> peopleIdList,
			String searchText) throws PersistenceException;

	/**
	 * 
	 * @param roleCatToSearch
	 * @param paginationFilter
	 * @return
	 * @throws PersistenceException
	 */
	Page searchRoleCatPeople(String roleCatToSearch,
			PaginationFilter paginationFilter) throws PersistenceException;

	/**
	 * This method searches for senders which could be related to a document
	 * which contains a text parameter (String searchText).
	 * 
	 * @param peopleIdList
	 *            List of person identifier
	 * @param searchText
	 *            Text to be searched
	 * @return A List<People> of senders that could be related to a document.
	 * @throws PersistenceException
	 */
	List<People> searchSendersPeople(List<Integer> peopleIdList,
			String searchText) throws PersistenceException;

	/**
	 * This method searches for spouse which could be related to a person which
	 * contains a text parameter (String query)
	 * 
	 * @param query
	 *            Text to be searched
	 * @return A List<People> of spouses that could be related to a person.
	 * @throws PersistenceException
	 */
	List<People> searchSpouseLinkableToPerson(String query)
			throws PersistenceException;

	/**
	 * 
	 * @param titleOccToSearch
	 * @param paginationFilter
	 * @return
	 * @throws PersistenceException
	 */
	Page searchTitlesOrOccupationsPeople(String titleOccToSearch,
			PaginationFilter paginationFilter) throws PersistenceException;

	public People findPeopleById(Integer personId) throws PersistenceException;

	public List<People> findPeople(String peopleName)
			throws PersistenceException;

	public Integer getAllCountPeopleBySerachWords(String placeName)
			throws PersistenceException;

	public List<People> getNews(Date date) throws PersistenceException;

	public List<People> getPersons(String searchWord)
			throws PersistenceException;

	public Integer getAllCountPeople() throws PersistenceException;

	public List<People> getComplexPersonsFromAltName(
			PeopleComplexSearchJson searchReq,
			PeopleSearchPaginationParam pagParam) throws PersistenceException;

	public List<People> getComplexPeopleFromTblPeople(
			PeopleComplexSearchJson searchReq) throws PersistenceException;

	public List<People> getComplexPeopleFromTblPeople(
			PeopleComplexSearchJson searchReq, PeopleSearchPaginationParam param) throws PersistenceException;

	public List<Integer> getAllCountPeopleFromTblPeople(
			AllCountWordSearchJson searchReq) throws PersistenceException;

	public int getAllCountPeople(
			AllCountWordSearchJson searchReq) throws PersistenceException;
	
	public void updatePeopleDocumentsCount(List<Integer> peopleIds) throws PersistenceException;
	
	public void updatePersonDocumentsCount(int personId) throws PersistenceException;
	
	public int getMinPersonId();
	
	public int getMaxPersonId();
	
	public List<Integer> getIdsFromTo(int from, int to) throws PersistenceException;

	public List<Sitemap> generatePeopleSitemaps(
			ChangeFrequency changeFrequency, Double priority);
}
