package org.medici.mia.dao.people;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.dao.Dao;
import org.medici.mia.domain.BiographicalPeople;

public interface BiographicalPeopleDAO extends Dao<Integer, BiographicalPeople> {

	public BiographicalPeople findPeopleById(Integer personId)
			throws PersistenceException;

	public List<BiographicalPeople> findPersonParents(Integer childId)
			throws PersistenceException;

	public Integer deletePerson(Integer documentEntityId, String owner,
			boolean isAutorizedUser) throws PersistenceException;

	public List<BiographicalPeople> findBiographicalPeopleById(
			List<Integer> personIds) throws PersistenceException;

	public void setPeopleModInf(Integer personId, String username)
			throws PersistenceException;

	public Integer undeletePerson(Integer personId, String owner,
			boolean isAutorizedUser) throws PersistenceException;

	public List<BiographicalPeople> findPeopleByPlace(Integer placeId)
			throws PersistenceException;

	public Integer setPeopleDeathDate() throws PersistenceException;

	public List<Integer> countPeopleForAdvancedSearch(List<String> queryList)
			throws PersistenceException;

	public List<BiographicalPeople> findBiographicalPeople(List<Integer> docList,
			Integer firstResult, Integer maxResult, String orderColumn, String ascOrDesc)
			throws PersistenceException;
	
	public List<BiographicalPeople> getBiographicalPeopleCreateByUser(
			String account);

}
