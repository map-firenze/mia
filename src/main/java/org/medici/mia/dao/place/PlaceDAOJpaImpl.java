/*
 * PlaceDAOJpaImpl.java
 * 
 * Developed by Medici Archive Project (2010-2012).
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.dao.place;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.hibernate.ejb.HibernateEntityManager;
import org.hibernate.search.FullTextSession;
import org.medici.mia.common.json.GenericResponseDataJson;
import org.medici.mia.common.json.complexsearch.PlaceComplexSearchJson;
import org.medici.mia.common.json.geographical.GeographicalPlaceJson;
import org.medici.mia.common.json.geographical.ModifyGeoPlaceNameVariantJson;
import org.medici.mia.common.json.search.AllCountWordSearchJson;
import org.medici.mia.common.json.search.PlaceSearchJson;
import org.medici.mia.common.json.search.PlaceWordSearchJson;
import org.medici.mia.common.pagination.Page;
import org.medici.mia.common.pagination.PaginationFilter;
import org.medici.mia.common.pagination.PaginationFilter.Order;
import org.medici.mia.common.pagination.PaginationFilter.SortingCriteria;
import org.medici.mia.common.search.Search;
import org.medici.mia.common.util.HtmlUtils;
import org.medici.mia.controller.genericsearch.AESearchPaginationParam;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.Place;
import org.medici.mia.domain.Sitemap;
import org.medici.mia.domain.User;
import org.medici.mia.domain.Sitemap.ChangeFrequency;
import org.medici.mia.service.genericsearch.ComplexSearchUtils;
import org.medici.mia.service.sitemap.SitemapSectionConfiguration;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Repository
public class PlaceDAOJpaImpl extends JpaDao<Integer, Place> implements PlaceDAO {

	private static final long serialVersionUID = -2993971980020334157L;
	
	private static final String BY_OWNER_QUERY = 
			"FROM Place p "
			+ "WHERE p.createdBy = :user "
			+ "AND p.logicalDelete = 0 "
			+ "ORDER BY p.dateCreated DESC";

	private final Logger logger = Logger.getLogger(this.getClass());

	private final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd hh:mm:ss");

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long countPlaceCreatedAfterDate(Date inputDate)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT COUNT(placeAllId) FROM Place WHERE dateCreated>=:inputDate");
		query.setParameter("inputDate", inputDate);

		return (Long) query.getSingleResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Place> findByGeogKey(Integer geogKey)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"FROM Place WHERE geogkey=:geogkey AND prefflag='P' AND logicalDelete = false");
		query.setParameter("geogkey", geogKey);
		List<Place> result = query.getResultList();
		query = getEntityManager()
				.createQuery(
						"FROM Place WHERE geogkey=:geogkey AND prefflag='V' AND logicalDelete = false");
		query.setParameter("geogkey", geogKey);
		result.addAll(query.getResultList());

		return result;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Place findLastEntryPlace() throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"FROM Place WHERE logicalDelete = false ORDER BY dateEntered DESC");
		query.setMaxResults(1);

		return (Place) query.getSingleResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Place findNewGeogKey(String plSource) throws PersistenceException {
		Query query = null;
		if (plSource.equals("MAPPLACE")) {
			query = getEntityManager()
					.createQuery(
							"FROM Place WHERE plSource=:plSource AND geogkey>=100000 AND geogkey<=400000 ORDER BY geogkey DESC");
		}
		if (plSource.equals("MAPSITE")) {
			query = getEntityManager()
					.createQuery(
							"FROM Place WHERE plSource=:plSource AND geogkey>=400000 AND geogkey<=1000000 AND logicalDelete = false ORDER BY geogkey DESC");
		}
		query.setParameter("plSource", plSource);
		query.setMaxResults(1);

		return (Place) query.getSingleResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Place findPrinicipalPlace(Integer geogKey)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"FROM Place WHERE geogKey=:geogkey AND prefflag='P' AND logicalDelete=false");
		query.setParameter("geogkey", geogKey);

		query.setMaxResults(1);

		return (Place) query.getSingleResult();

	}

	@Override
	public Place findPrinicipalPlaceByPlaceId(Integer placeAllId)
			throws PersistenceException {

		Place place = getEntityManager().find(Place.class, placeAllId);

		if ("P".equalsIgnoreCase(place.getPrefFlag())) {
			return place;
		}

		Query query = getEntityManager()
				.createQuery(
						"FROM Place WHERE geogKey=:geogkey AND prefflag='P' AND logicalDelete=false");
		query.setParameter("geogkey", place.getGeogKey());
		query.setMaxResults(1);
		return (Place) query.getSingleResult();

	}

	@Override
	public Integer findPrinicipalPlaceIdFromVariantPlaceId(Integer variantGeoKey)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"Select placeAllId FROM Place WHERE geogKey=:geogkey AND prefflag='P' AND logicalDelete=false");
		query.setParameter("geogkey", variantGeoKey);
		query.setMaxResults(1);

		return (Integer) query.getSingleResult();

	}

	private List<Place> findVariants(Integer variantGeoKey)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"Select p FROM Place p WHERE p.geogKey=:geogKey AND p.prefFlag='V' AND logicalDelete=false");
		query.setParameter("geogKey", variantGeoKey);

		return (List<Place>) query.getResultList();

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Place> searchBornPlace(String query)
			throws PersistenceException {
		return findPlaces(query);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Place> searchDeathPlace(String query)
			throws PersistenceException {
		return findPlaces(query);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Place> searchPlaceLinkableToTopicDocument(String searchText)
			throws PersistenceException {
		return findPlaces(searchText);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Place> searchPlaceParent(String query)
			throws PersistenceException {
		// return doSearch(query);
		return findPlaces(query);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Page searchPlaces(Search searchContainer,
			PaginationFilter paginationFilter) throws PersistenceException {
		// We prepare object of return method.
		Page page = new Page(paginationFilter);

		// We obtain hibernate-search session
		FullTextSession fullTextSession = org.hibernate.search.Search
				.getFullTextSession(((HibernateEntityManager) getEntityManager())
						.getSession());

		org.apache.lucene.search.Query query = searchContainer.toLuceneQuery();
		logger.info("Lucene Query " + query.toString());

		// We execute search
		org.hibernate.search.FullTextQuery fullTextQuery = fullTextSession
				.createFullTextQuery(query, Place.class);

		// We set size of result.
		if (paginationFilter.getTotal() == null) {
			page.setTotal(new Long(fullTextQuery.getResultSize()));
		}

		// We set pagination
		fullTextQuery.setFirstResult(paginationFilter.getFirstRecord());
		fullTextQuery.setMaxResults(paginationFilter.getLength());

		// We manage sorting (this manages sorting on multiple fields)
		paginationFilter = this
				.generatePaginationFilterHibernateSearch(paginationFilter);
		List<SortingCriteria> sortingCriterias = paginationFilter
				.getSortingCriterias();
		if (sortingCriterias.size() > 0) {
			SortField[] sortFields = new SortField[sortingCriterias.size()];
			for (int i = 0; i < sortingCriterias.size(); i++) {
				sortFields[i] = new SortField(
						sortingCriterias.get(i).getColumn(),
						sortingCriterias.get(i).getColumnType(),
						(sortingCriterias.get(i).getOrder().equals(Order.ASC) ? true
								: false));
			}
			fullTextQuery.setSort(new Sort(sortFields));
		}

		// We set search result on return method
		page.setList(fullTextQuery.list());

		return page;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Place> searchPlaces(String searchText)
			throws PersistenceException {
		return findPlaces(searchText);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Place> searchRecipientsPlace(String searchText)
			throws PersistenceException {
		return findPlaces(searchText);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Place> searchSendersPlace(String searchText)
			throws PersistenceException {
		return findPlaces(searchText);
	}

	@Override
	public Place findPlaceById(Integer placeId) throws PersistenceException {

		Place place = null;
		try {
			place = getEntityManager().find(Place.class, placeId);
		} catch (NoResultException nre) {
			logger.info("No Place found in DB for placeALLId: " + placeId);
		}

		return place;
	}

	@Override
	public List<Place> findNotDeletedPlaces() throws PersistenceException {

		Query query = getEntityManager().createQuery(
				"FROM Place WHERE logicalDelete=false");

		return (List<Place>) query.getResultList();

	}

	@Override
	public List<Place> findPlaces(String placeName) throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"FROM Place p WHERE (p.placeName LIKE :placeName OR p.termAccent LIKE :placeName) AND p.logicalDelete=false ORDER BY p.deCount DESC, p.placeName ASC, CASE WHEN p.placeNameFull LIKE :placeNameFull THEN 1 ELSE 2 END");
		query.setParameter("placeName", "%" + placeName + "%");
		query.setParameter("placeNameFull", placeName + "%");
		return query.getResultList();
	}

	@Override
	public List<Place> findPlacesByNameAndType(
			PlaceWordSearchJson placeSearchWord) throws PersistenceException {

		// NOTE: Using query with like bqse on the Issue 495

		Query query = null;
		if (placeSearchWord.getPlaceType() == null
				|| placeSearchWord.getPlaceType().isEmpty()) {
			query = getEntityManager()
					.createQuery(
							"select p FROM Place p WHERE p.placeName  LIKE :placeName AND p.logicalDelete=false");
			query.setParameter("placeName",
					"%" + placeSearchWord.getSearchString() + "%");

		} else {
			query = getEntityManager()
					.createQuery(
							"select p FROM Place p WHERE (p.placeName  LIKE :placeName AND p.plType =:plType) AND p.logicalDelete=false");
			query.setParameter("placeName",
					"%" + placeSearchWord.getSearchString() + "%");
			query.setParameter("plType", placeSearchWord.getPlaceType());
		}

		return (List<Place>) query.getResultList();
	}

	@Override
	public List<Place> findPlacesByComplexSearchNameAndType(
			PlaceComplexSearchJson placeSearchWord,
			AESearchPaginationParam pagParam) throws PersistenceException {

		GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>> toret = new GenericResponseDataJson<HashMap<String, List<PlaceSearchJson>>>();

		String queryString = ComplexSearchUtils
				.getPlaceComplexQueryString(placeSearchWord);
		queryString = queryString + " order by " + pagParam.getOrderColumn()
				+ " " + pagParam.getAscOrdDesc();
		logger.info("Native query for the service search/places/wordSearch: "
				+ queryString);
		Query query = getEntityManager().createNativeQuery(queryString,
				Place.class);
		query.setFirstResult(pagParam.getFirstResult());
		query.setMaxResults(pagParam.getMaxResult());

		return (List<Place>) query.getResultList();
	}

	@Override
	public Integer getAllCountPlacesBySerachWords(String searchWord)
			throws PersistenceException {

		if (searchWord == null || searchWord.isEmpty()) {
			return 0;
		}
		searchWord = searchWord.replaceAll("\\“", "\"");
		searchWord = searchWord.replaceAll(" ", "");
		String[] words = searchWord.split("\"");

		List<String> list = new ArrayList<String>();

		for (int i = 0; i < words.length; i++) {
			if (!words[i].isEmpty()) {
				list.add(words[i].trim());
			}
		}

		StringBuilder jpaQuery = new StringBuilder(
				"select count(*) FROM tblPlaces p WHERE ");
		if (list.size() > 1) {
			for (int i = 0; i < list.size() - 1; i++) {
				jpaQuery.append("placeName LIKE " + "'%" + list.get(i) + "%'");
				jpaQuery.append(" AND ");
			}
		}

		jpaQuery.append("placeName LIKE ");
		jpaQuery.append("'%" + list.get(list.size() - 1) + "%'");
		jpaQuery.append(" AND logicalDelete = false");

		Query q = getEntityManager().createNativeQuery(jpaQuery.toString());
		logger.info("All count Place search query: " + jpaQuery.toString());
		return ((BigInteger) q.getSingleResult()).intValue();
	}

	@Override
	public Integer getAllCountPlaces() throws PersistenceException {

		StringBuilder jpaQuery = new StringBuilder(
				"select count(*) FROM tblPlaces p WHERE ");
		jpaQuery.append(" logicalDelete = false");

		Query q = getEntityManager().createNativeQuery(jpaQuery.toString());
		logger.info("All count Place search query: " + jpaQuery.toString());
		return ((BigInteger) q.getSingleResult()).intValue();
	}

	@Override
	public Integer modifyGeographicalPlace(GeographicalPlaceJson modifyJson,
			User user) throws PersistenceException {

		if (modifyJson == null || modifyJson.getPlaceId() == null) {
			return 0;
		}

		Place place = null;
		try {
			place = getEntityManager().find(Place.class,
					modifyJson.getPlaceId());
		} catch (NoResultException nre) {
			logger.info("No Place found in DB for placeALLId: "
					+ modifyJson.getPlaceId());
		}

		if (place == null) {
			return 0;
		}
		
		if (modifyJson.getPlNameFullPlType() != null) {
			modifyJson = updateFullNames(modifyJson);
		}

		modifyJson.setLastUpdate(sdf.format(new Date()));
		modifyJson.setLastUpdateBy(user.getAccount());

		modifyJson.toEntity(place);
		getEntityManager().merge(place);
		return 1;
	}

	@Override
	public List<Place> findPlaceNameVariants(String placeName)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"FROM Place p WHERE p.placeName LIKE :placeName AND p.logicalDelete=false AND prefflag='V'");
		query.setParameter("placeName", "%" + placeName + "%");
		return query.getResultList();
	}

	@Override
	public List<Place> findPlaceNameVariantsById(Integer placeId)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"Select p.geogKey FROM Place p WHERE p.placeAllId=:placeId AND p.logicalDelete=false");
		query.setParameter("placeId", placeId);
		List<Integer> geogKeys = (List<Integer>) query.getResultList();
		if (geogKeys == null || geogKeys.isEmpty()) {
			return null;
		}
		Integer geogKey = geogKeys.get(0);

		// query = getEntityManager()
		// .createQuery(
		// "Select p FROM Place p WHERE p.geogKey=:geogKey AND p.logicalDelete=false AND prefflag='V'");
		query = getEntityManager()
				.createQuery(
						"Select p FROM Place p WHERE p.geogKey=:geogKey AND p.logicalDelete=false");

		query.setParameter("geogKey", geogKey);
		return (List<Place>) query.getResultList();

	}

	@Override
	public Integer modifyPlaceNameVariant(
			ModifyGeoPlaceNameVariantJson modifyJson, User user)
			throws PersistenceException {

		if (modifyJson == null || modifyJson.getNewPlaceNameVariant() == null
				|| modifyJson.getNewPlaceNameVariant().getPlaceAllId() == null
				|| modifyJson.getNewPlaceNameVariant().getPlaceName() == null) {
			return 0;
		}

		Place place = null;

		try {
			place = getEntityManager().find(Place.class,
					modifyJson.getNewPlaceNameVariant().getPlaceAllId());
		} catch (NoResultException nre) {
			logger.info("No Place found in DB for placeALLId: "
					+ modifyJson.getNewPlaceNameVariant().getPlaceAllId());
		}

		if (place == null || !"V".equalsIgnoreCase(place.getPrefFlag()))
			return 0;

		String placeName = place.getPlaceName();
		String newPlNameFull = place.getPlaceNameFull().replace(placeName,
				modifyJson.getNewPlaceNameVariant().getPlaceName());
		String newPlNameFullType = place.getPlNameFullPlType().replace(
				placeName, modifyJson.getNewPlaceNameVariant().getPlaceName());
		place.setPlaceName(modifyJson.getNewPlaceNameVariant().getPlaceName());
		place.setPlaceNameFull(newPlNameFull);
		place.setPlNameFullPlType(newPlNameFullType);
		place.setLastUpdate(new Date());
		place.setLastUpdateBy(user);

		getEntityManager().merge(place);

		Integer principalPlaceId = findPrinicipalPlaceIdFromVariantPlaceId(place
				.getGeogKey());

		setPlaceModInf(principalPlaceId, user);

		return 1;
	}

	public Integer addPlaceNameVariant(
			ModifyGeoPlaceNameVariantJson modifyJson, User user)
			throws PersistenceException {

		if (modifyJson == null || modifyJson.getNewPlaceNameVariant() == null
				|| modifyJson.getNewPlaceNameVariant().getPlaceName() == null) {
			return 0;
		}

		Place newPlace = new Place();

		Query query = getEntityManager()
				.createQuery(
						"Select p FROM Place p WHERE p.placeAllId=:placeAllId AND p.logicalDelete=false AND prefflag='P'");

		query.setParameter("placeAllId", modifyJson.getPlaceAllId());
		List<Place> places = (List<Place>) query.getResultList();
		if (places == null || places.isEmpty()) {
			return 0;
		}

		Place principal = places.get(0);

		String newPlNameFull = principal.getPlaceNameFull().replace(
				principal.getPlaceName(),
				modifyJson.getNewPlaceNameVariant().getPlaceName());
		String newPlNameFullType = principal.getPlNameFullPlType().replace(
				principal.getPlaceName(),
				modifyJson.getNewPlaceNameVariant().getPlaceName());

		newPlace.setPlaceName(modifyJson.getNewPlaceNameVariant()
				.getPlaceName());
		newPlace.setGeogKey(principal.getGeogKey());
		newPlace.setPlaceNameFull(newPlNameFull);
		newPlace.setPlNameFullPlType(newPlNameFullType);
		newPlace.setCreatedBy(user);
		newPlace.setPrefFlag("V");
		newPlace.setDateCreated(new Date());
		newPlace.setLastUpdate(new Date());
		newPlace.setAddlRes(Boolean.FALSE);
		newPlace.setLogicalDelete(Boolean.FALSE);
		newPlace.setPlType(principal.getPlType());
		newPlace.setPlSource(principal.getPlSource());
		newPlace.setPlParent(principal.getPlParent());
		newPlace.setParentType(principal.getParentType());
		newPlace.setPlParentTermId(principal.getPlParentTermId());
		newPlace.setPlParentSubjectId(principal.getPlParentSubjectId());
		newPlace.setParentPlace(principal.getParentPlace());
		newPlace.setgParent(principal.getgParent());
		newPlace.setGpType(principal.getGgpType());
		newPlace.setGgp(principal.getGgp());
		newPlace.setGgpType(principal.getGgpType());
		newPlace.setGp2(principal.getGp2());
		newPlace.setGp2Ttype(principal.getGp2Ttype());

		getEntityManager().persist(newPlace);

		principal.setLastUpdate(new Date());
		principal.setLastUpdateBy(user);
		getEntityManager().merge(principal);

		return 1;
	}

	// @Override
	// public Integer deletePlace(Integer placeId, User user)
	// throws PersistenceException {
	// if (placeId == null || placeId.equals("")) {
	// return 0;
	// }
	//
	// Place place = getEntityManager().find(Place.class, placeId);
	// if (place == null) {
	// return 0;
	// }
	//
	// if ("V".equalsIgnoreCase(place.getPrefFlag())) {
	// // Issue 483 Delete VARIANT Place Record - do not allow
	// return -1;
	// }
	//
	// place.setLogicalDelete(Boolean.TRUE);
	// place.setLastUpdate(new Date());
	// place.setLastUpdateBy(user);
	// getEntityManager().merge(place);
	//
	// setPlaceModInf(placeId, user);
	// return 1;
	//
	// }

	@Override
	public Integer deletePlace(Integer placeId, User user)
			throws PersistenceException {

		// NOTE: This service should be called only by a principal
		if (placeId == null || placeId.equals("")) {
			return 0;
		}

		Place place = null;
		try {
			place = getEntityManager().find(Place.class, placeId);
		} catch (NoResultException nre) {
			logger.info("No Place found in DB for placeALLId: " + placeId);
		}
		if (place == null) {
			return 0;
		}

		// Delete principal Place
		place.setLogicalDelete(Boolean.TRUE);
		place.setLastUpdate(new Date());
		place.setLastUpdateBy(user);
		getEntityManager().merge(place);

		// Delete Variants of principal
		List<Place> variants = findVariants(place.getGeogKey());

		if (variants != null && !variants.isEmpty()) {
			for (Place var : variants) {

				var.setLogicalDelete(Boolean.TRUE);
				var.setLastUpdate(new Date());
				var.setLastUpdateBy(user);
				getEntityManager().merge(var);

			}
		}

		return 1;

	}

	@Override
	public Integer deletePlaceVariant(Integer placeId, User user)
			throws PersistenceException {
		if (placeId == null || placeId.equals("")) {
			return 0;
		}

		Place place = null;
		try {
			place = getEntityManager().find(Place.class, placeId);
		} catch (NoResultException nre) {
			logger.info("No Place found in DB for placeALLId: " + placeId);
		}

		if (place == null) {
			return 0;
		}

		if (!"V".equalsIgnoreCase(place.getPrefFlag())) {
			return 0;
		}

		// TODO

		Integer principalId = findPrinicipalPlaceIdFromVariantPlaceId(place
				.getGeogKey());

		place.setLogicalDelete(Boolean.TRUE);
		place.setLastUpdate(new Date());
		place.setLastUpdateBy(user);
		getEntityManager().merge(place);

		setPlaceModInf(principalId, user);
		return 1;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Place> getNews(Date date) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"FROM Place e WHERE e.dateCreated>=:dateCreated OR e.lastUpdate>=:lastUpdate");
		query.setParameter("dateCreated", date);
		query.setParameter("lastUpdate", date);
		return (List<Place>) query.getResultList();

	}

	@Override
	public void setPlaceModInf(Integer placeAllId, User user)
			throws PersistenceException {

		Query query = getEntityManager().createQuery(
				"SELECT u FROM Place u WHERE u.placeAllId=:placeAllId");
		query.setParameter("placeAllId", placeAllId);
		List<Place> list = (List<Place>) query.getResultList();
		if (list != null && !list.isEmpty()) {
			Place entity = list.get(0);
			entity.setLastUpdateBy(user);
			entity.setLastUpdate(new Date());
			getEntityManager().merge(entity);
		}

	}

	@Override
	public List<Integer> getAllCountPlace(AllCountWordSearchJson searchReq)
			throws PersistenceException {

		String queryString = ComplexSearchUtils.getPlaceAllCount(searchReq);
		if (queryString == null)
			return new ArrayList<Integer>();

		Query query = getEntityManager().createNativeQuery(queryString);
		@SuppressWarnings("unchecked")
		List<Integer> places = (List<Integer>) query.getResultList();

		if (places != null && !places.isEmpty()) {
			return places;

		}

		return new ArrayList<Integer>();

	}
	
	@Override
	public List<Place> getPlacesCreatedByUser(User user) {
		Query query = getEntityManager().createQuery(BY_OWNER_QUERY);
		query.setParameter("user", user);
		
		@SuppressWarnings("unchecked")
		List<Place> results = (List<Place>) query.getResultList();
		
		return results;
	}
	
	@Override
	public List<Place> getPlacesCreatedByUser(
			User user, 
			AESearchPaginationParam paginationParameters) {
	
		Query query = getEntityManager().createQuery(BY_OWNER_QUERY);
		query.setParameter("user", user);
		
		query.setFirstResult(paginationParameters.getFirstResult());
		query.setMaxResults(paginationParameters.getMaxResult());
		
		@SuppressWarnings("unchecked")
		List<Place> results = (List<Place>) query.getResultList();
		
		return results;
	}
	
	@Override
	public List<Sitemap> generatePlacesSitemaps(
			ChangeFrequency changeFrequency, Double priority) {
		
		String queryCode = 
			"SELECT p.PLACEALLID, p.DATECREATED, p.LASTUPDATE FROM tblPlaces p "
			+ "WHERE p.LOGICALDELETE = 0";

		Query query = getEntityManager().createNativeQuery(queryCode);
				
		@SuppressWarnings("unchecked")
		List<Object[]> sitemapsInfos = query.getResultList();
				
		String baseUrl = HtmlUtils.getSectionUrl(
				HtmlUtils.PLACES_SECTION_PATH);
			
		SitemapSectionConfiguration sitemapSectionConfiguration = 
				new SitemapSectionConfiguration(
						baseUrl, changeFrequency, priority);
			
		return sitemapSectionConfiguration.createSitemaps(sitemapsInfos);
	} 
	
	private GeographicalPlaceJson updateFullNames(GeographicalPlaceJson place) {
		Place currentParent = findPlaceById(place.getPlParentPlaceAllId());
		
		place.setPlaceNameFull(getNewNameFull(place.getPlaceName(), 
				currentParent));
		place.setPlNameFullPlType(getNewPlNameFullPlType(place.getPlaceName(), 
				currentParent));
		

		return place;
	}
	
	private String getNewNameFull(String placeName, Place parentPlace) {
		String newNameFull = 
			placeName + 
			" / " + parentPlace.getPlParent() + " / " + parentPlace.getgParent() 
			+ " / " + parentPlace.getGgp() + " /" + parentPlace.getGp2();
				
		return newNameFull;
	}
	
	private String getNewPlNameFullPlType(String placeName, Place parentPlace) {
		String newPlNameFull = 
			placeName + 
			" (" + parentPlace.getPlType() + ") / " 
			+ parentPlace.getPlParent() + " (" + parentPlace.getParentType() 
			+ ") / " + parentPlace.getgParent() + " (" + parentPlace.getGpType() 
			+ ") / " + parentPlace.getGgp() 
			+ " (" + parentPlace.getGgpType() 
			+ ") /" + parentPlace.getGp2() 
			+ " (" + parentPlace.getGp2Ttype() + ")";
		
		return newPlNameFull;
	}
	
}
