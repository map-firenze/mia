package org.medici.mia.dao.casestudy;

import org.medici.mia.dao.Dao;
import org.medici.mia.domain.CaseStudy;
import org.medici.mia.domain.CaseStudyFolder;
import org.medici.mia.domain.CaseStudyItem;
import org.medici.mia.domain.User;

import javax.persistence.PersistenceException;
import java.util.List;

public interface CaseStudyDAO extends Dao<Integer, CaseStudy> {
    CaseStudy findCaseStudy(Integer id);
    List<CaseStudy> findStudiesByUser(User user) throws PersistenceException;
    List<CaseStudy> findStudiesByUser(User user, int offset, int limit) throws PersistenceException;
    List<CaseStudy> findStudiesByFolderId(Integer id) throws PersistenceException;
    List<CaseStudy> findStudiesByItemId(Integer id) throws PersistenceException;
    List<CaseStudyFolder> findFoldersByStudyId(Integer id) throws PersistenceException;
    List<CaseStudyItem> findItemsByFolderId(Integer id) throws PersistenceException;
    List<CaseStudy> findByDocumentId(Integer documentId, User user) throws PersistenceException;
    List<CaseStudyFolder> findFoldersByDocumentId(Integer documentId, User user) throws PersistenceException;
    void saveEntity(Object entity) throws PersistenceException;
    void mergeEntity(Object entity) throws PersistenceException;
    CaseStudyItem findItem(Integer id) throws PersistenceException;
    CaseStudyFolder findFolder(Integer id) throws PersistenceException;
    boolean removeCaseStudy(CaseStudy caseStudy) throws PersistenceException;
    boolean removeFolder(CaseStudyFolder folder) throws PersistenceException;
    boolean removeItem(CaseStudyItem item) throws PersistenceException;
    List<CaseStudy> findAttachedCS(Integer id, CaseStudyItem.EntityType type) throws PersistenceException;
    List<CaseStudyItem> findAttachedItems(Integer id, CaseStudyItem.EntityType type) throws PersistenceException;
    Boolean isDocumentSharedWithMe(Integer id) throws PersistenceException;
}