package org.medici.mia.dao.casestudy;

import com.github.underscore.Predicate;
import com.github.underscore.U;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.dao.userrole.UserRoleDAO;
import org.medici.mia.domain.*;
import org.medici.mia.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CaseStudyJPADAOImpl extends JpaDao<Integer, CaseStudy> implements CaseStudyDAO {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRoleDAO userRoleDAO;

    @Override
    public CaseStudy findCaseStudy(Integer id) {
        Query query = getEntityManager().createQuery(
                "FROM CaseStudy cs WHERE cs.id=:id"
        );
        query.setParameter("id", id);
        CaseStudy cs;
        try {
            cs = (CaseStudy) query.getSingleResult();
        } catch (PersistenceException ex){
            return null;
        }
        return cs;
    }

    private boolean isAdmin(){
        User contextUser = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername());
        List<UserRole> userRoles = userRoleDAO.findUserRoles(contextUser.getAccount());
        return U.any(userRoles, new Predicate<UserRole>() {
            @Override
            public boolean test(UserRole userRole) {
                return userRole.containsAuthority(UserAuthority.Authority.ADMINISTRATORS);
            }
        });
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CaseStudy> findStudiesByUser(User user) throws PersistenceException {
        try {
            Query query;
            List resultList;
            if(isAdmin()){
                query = getEntityManager().createQuery(
                        "SELECT cs " +
                                "FROM CaseStudy cs" +
                                " ORDER BY cs.createdAt DESC"
                );
                resultList = query.getResultList();
            } else {
                query = getEntityManager().createQuery(
                        "SELECT DISTINCT cs FROM CaseStudy cs where cs.createdBy = :user" +
                                " ORDER BY cs.createdAt DESC"
                );
                query.setParameter("user", user);
                resultList = query.getResultList();
                query = getEntityManager().createQuery(
                        "SELECT DISTINCT cs FROM CaseStudy cs JOIN cs.userGroup u where u = :user" +
                                " ORDER BY cs.createdAt DESC"
                );
                query.setParameter("user", user);
                List joinResult = query.getResultList();
                if (joinResult != null) {
                    resultList.addAll(query.getResultList());
                }
            }
            return U.uniq(resultList);
        } catch (PersistenceException ex){
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CaseStudy> findStudiesByUser(User user, int limit, int offset) throws PersistenceException {
        try {
            Query query = getEntityManager().createQuery(
                    "SELECT DISTINCT cs FROM CaseStudy cs where cs.createdBy = :user" +
                            " ORDER BY cs.createdAt DESC"
            );
            query.setParameter("user", user);
            List resultList = query.getResultList();
            query = getEntityManager().createQuery(
                    "SELECT DISTINCT cs FROM CaseStudy cs JOIN cs.userGroup u where u = :user" +
                            " ORDER BY cs.createdAt DESC"
            );
            query.setParameter("user", user);
            List joinResult = query.getResultList();
            if(joinResult != null){
                resultList.addAll(query.getResultList());
            }
            return U.uniq(resultList);
        } catch (PersistenceException ex){
            return null;
        }

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CaseStudy> findStudiesByFolderId(Integer id) throws PersistenceException {
        Query query = getEntityManager().createQuery(
                "SELECT f.caseStudy FROM CaseStudyFolder f WHERE f.caseStudy.id = :id" +
                        " ORDER BY f.createdAt DESC"
        );
        query.setParameter("id", id);
        try {
            return query.getResultList();
        } catch (PersistenceException ex){
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CaseStudy> findStudiesByItemId(Integer id) throws PersistenceException {
        Query query = getEntityManager().createQuery(
                "SELECT i.caseStudy FROM CaseStudyItem i WHERE i.folder.caseStudy.id = :id" +
                        " ORDER BY i.createdAt DESC"
        );
        query.setParameter("id", id);
        try {
            return query.getResultList();
        } catch (PersistenceException ex){
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CaseStudyFolder> findFoldersByStudyId(Integer id) throws PersistenceException {
        Query query = getEntityManager().createQuery(
                "FROM CaseStudyFolder f WHERE f.caseStudy.id = :id" +
                        " ORDER BY f.order ASC, f.createdAt DESC"
        );
        query.setParameter("id", id);
        try {
            return query.getResultList();
        } catch (PersistenceException ex){
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CaseStudyItem> findItemsByFolderId(Integer id) throws PersistenceException {
        Query query = getEntityManager().createQuery(
                "FROM CaseStudyItem i WHERE i.folder.id = :id" +
                        " ORDER BY f.order ASC, i.createdAt DESC"
        );
        query.setParameter("id", id);
        try {
            return query.getResultList();
        } catch (PersistenceException ex){
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CaseStudy> findByDocumentId(Integer documentId, User user) throws PersistenceException {
        Query query;
        if(isAdmin()) {
            query = getEntityManager().createQuery(
                    "SELECT DISTINCT i.folder.caseStudy" +
                            " FROM CaseStudyItem i" +
                            " WHERE i.entityId = :id AND i.entityType = :type"
            );
        } else {
            query = getEntityManager().createQuery(
                    "SELECT DISTINCT i.folder.caseStudy" +
                            " FROM CaseStudyItem i" +
                            " JOIN i.folder.caseStudy.userGroup userGroup" +
                            " WHERE i.entityId = :id AND i.entityType = :type AND :user in userGroup"
            );
            query.setParameter("user", user);
        }
        query.setParameter("id", documentId);
        query.setParameter("type", CaseStudyItem.EntityType.DE);

        try {
            return query.getResultList();
        } catch (PersistenceException ex){
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CaseStudyFolder> findFoldersByDocumentId(Integer documentId, User user) throws PersistenceException {
        Query query;
        if(isAdmin()) {
            query = getEntityManager().createQuery(
                    "SELECT DISTINCT i.folder" +
                            " FROM CaseStudyItem i" +
                            " WHERE i.entityId = :id AND i.entityType = :type"
            );
        } else {
            query = getEntityManager().createQuery(
                    "SELECT DISTINCT i.folder" +
                            " FROM CaseStudyItem i" +
                            " JOIN i.folder.caseStudy.userGroup userGroup" +
                            " WHERE i.entityId = :id AND i.entityType = :type AND :user in userGroup"
            );
            query.setParameter("user", user);
        }
        query.setParameter("id", documentId);
        query.setParameter("type", CaseStudyItem.EntityType.DE);

        try {
            return query.getResultList();
        } catch (PersistenceException ex){
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Boolean isDocumentSharedWithMe(Integer id) throws PersistenceException {
        Query query = getEntityManager().createQuery(
                "SELECT DISTINCT i.folder.caseStudy.userGroup FROM CaseStudyItem i WHERE i.entityType = :type AND i.entityId = :id"
        );
        query.setParameter("id", id);
        query.setParameter("type", CaseStudyItem.EntityType.DE);
        final String account = userService.findUser(
                ((UserDetails) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).getUsername()).getAccount();
        try {
            List<User> sharedWith = query.getResultList();
            if(sharedWith == null){
                return false;
            }
            return U.any(sharedWith, new Predicate<User>() {
                @Override
                public boolean test(User user) {
                    return user.getAccount().equals(account);
                }
            });
        } catch (PersistenceException ex){
            return false;
        }
    }

    @Override
    public void saveEntity(Object entity) throws PersistenceException {
        getEntityManager().persist(entity);
        getEntityManager().flush();
    }

    @Override
    public void mergeEntity(Object entity) throws PersistenceException {
        getEntityManager().merge(entity);
    }

    @Override
    public boolean removeCaseStudy(CaseStudy caseStudy) throws PersistenceException {
        try {
            for(CaseStudyFolder folder: caseStudy.getFolders()){
                removeFolder(folder);
            }
            getEntityManager().remove(caseStudy);
            return true;
        } catch (PersistenceException ex){
            return false;
        }
    }

    @Override
    public boolean removeFolder(CaseStudyFolder folder) throws PersistenceException {
        try{
            for(CaseStudyItem item: folder.getItems()){
                removeItem(item);
            }
            getEntityManager().remove(folder);
            return true;
        } catch (PersistenceException ex) {
            return false;
        }
    }

    @Override
    public boolean removeItem(CaseStudyItem item) throws PersistenceException {
        try{
            if(item.getEntityType().equals(CaseStudyItem.EntityType.FILE) && item.getPath() != null){
                try {
                    File file = new File(item.getPath());
                    if (file.exists()) {
                        file.delete();
                    }
                } catch (SecurityException e) {
                    throw e;
                }
            }
            getEntityManager().remove(item);
            return true;
        } catch (PersistenceException ex) {
            return false;
        }
    }

    @Override
    public CaseStudyItem findItem(Integer id) throws PersistenceException {
        return getEntityManager().find(CaseStudyItem.class, id);
    }

    @Override
    public CaseStudyFolder findFolder(Integer id) throws PersistenceException {
        return getEntityManager().find(CaseStudyFolder.class, id);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CaseStudy> findAttachedCS(Integer id, CaseStudyItem.EntityType type) throws PersistenceException {
        Query query = getEntityManager().createQuery(
                "SELECT i.folder.caseStudy FROM CaseStudyItem i WHERE i.entityId = :id AND i.entityType = :type"
        );
        query.setParameter("id", id);
        query.setParameter("type", type);
        try {
            return query.getResultList();
        } catch (PersistenceException e){
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CaseStudyItem> findAttachedItems(Integer id, CaseStudyItem.EntityType type) throws PersistenceException {
        Query query = getEntityManager().createQuery(
                "FROM CaseStudyItem i WHERE i.entityId = :id AND i.entityType = :type"
        );
        query.setParameter("id", id);
        query.setParameter("type", type);
        try {
            return query.getResultList();
        } catch (PersistenceException e){
            return null;
        }
    }
}
