package org.medici.mia.dao.googlelocation;

import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.GoogleLocation;
import org.springframework.stereotype.Repository;

@Repository
public class GoogleLocationDAOImpl extends JpaDao<Integer, GoogleLocation>
		implements GoogleLocationDAO {

	private static final long serialVersionUID = 1983037420201461403L;

	@Override
	public List<GoogleLocation> getLocationsByCity(String cityName)
			throws PersistenceException {
		Query query = getEntityManager().createQuery(
				"FROM GoogleLocation WHERE city = :city");
		query.setParameter("city", cityName);
		return getResultList(query);
	}

	@Override
	public List<GoogleLocation> getLocationsByCountry(String countryName)
			throws PersistenceException {
		Query query = getEntityManager().createQuery(
				"FROM GoogleLocation WHERE country = :country");
		query.setParameter("country", countryName);
		return getResultList(query);
	}

	@Override
	public void saveLocation(GoogleLocation location)
			throws PersistenceException {
		if(location!=null && location.getgPlaceId()!=null && !location.getgPlaceId().isEmpty()){
			getEntityManager().persist(location);
			
		}
		
	}

	@Override
	public void saveLocations(List<GoogleLocation> locations)
			throws PersistenceException {
		for(GoogleLocation location: locations){			
			saveLocation(location);
		}

	}

	@Override
	public GoogleLocation getLocationById(String gplaceId)
			throws PersistenceException {
		if(gplaceId==null || gplaceId.isEmpty())
			return null;
		GoogleLocation gLoc = getEntityManager().find(GoogleLocation.class, gplaceId);
		return gLoc;
	}

}
