package org.medici.mia.dao.googlelocation;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.dao.Dao;
import org.medici.mia.domain.GoogleLocation;


public interface GoogleLocationDAO extends Dao<Integer, GoogleLocation> {	
	
	public List<GoogleLocation> getLocationsByCity(String cityName) throws PersistenceException;
	public List<GoogleLocation> getLocationsByCountry(String countryName) throws PersistenceException;
	public void saveLocation(GoogleLocation location) throws PersistenceException;
	public void saveLocations(List<GoogleLocation> locations)throws PersistenceException;
	public GoogleLocation getLocationById(String googleId) throws PersistenceException;	
	
}
