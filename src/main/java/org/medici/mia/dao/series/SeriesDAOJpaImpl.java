/*
 * SeriesDAOJpaImpl.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.dao.series;

import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.SeriesEntity;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author User
 *
 */
@Repository
public class SeriesDAOJpaImpl extends JpaDao<Integer, SeriesEntity> implements
		SeriesDAO {

	private static final long serialVersionUID = 7879457802562349809L;

	public List<SeriesEntity> findSeriesByTitle(String seriesTitle,
			Integer collectionId) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"FROM SeriesEntity s WHERE s.title LIKE :title and s.collection = :collection");
		query.setParameter("title", "%" + seriesTitle + "%");
		query.setParameter("collection", collectionId);

		return query.getResultList();
	}

	public List<SeriesEntity> findAllSeries(Integer collectionId)
			throws PersistenceException {

		Query query = getEntityManager().createQuery(
				"FROM SeriesEntity s WHERE s.collection = :collection");

		query.setParameter("collection", collectionId);

		return query.getResultList();
	}

	public List<SeriesEntity> findMatchSeriesByTitle(String seriesTitle,
			Integer collectionId) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"FROM SeriesEntity s WHERE s.title LIKE :title and s.collection = :collection");
		query.setParameter("title", seriesTitle);
		query.setParameter("collection", collectionId);

		return query.getResultList();
	}

	public Integer insertSeries(SeriesEntity serEntity)
			throws PersistenceException {
		getEntityManager().persist(serEntity);

		return serEntity.getSeriesId();
	}

	public SeriesEntity mergeSeries(SeriesEntity serEntity)
			throws PersistenceException {

		SeriesEntity ser = getEntityManager().find(SeriesEntity.class,
				serEntity.getSeriesId());

		if (ser == null)
			return null;
		ser.setTitle(serEntity.getTitle());
		ser.setSubtitle1(serEntity.getSubtitle1());
		ser.setCollection(serEntity.getCollection());

		getEntityManager().merge(ser);

		return ser;

	}

}
