/*
 * MarriageDAOJpaImpl.java
 * 
 * Developed by Medici Archive Project (2010-2012).
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.dao.marriage;

import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.medici.mia.common.json.biographical.ModifyPersonSpouseJson;
import org.medici.mia.common.json.biographical.SpouseJson;
import org.medici.mia.common.json.biographical.SpouseType;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.Marriage;
import org.medici.mia.domain.People;
import org.medici.mia.domain.People.Gender;
import org.springframework.stereotype.Repository;

@Repository
public class MarriageDAOJpaImpl extends JpaDao<Integer, Marriage> implements
		MarriageDAO {

	/**
	 * 
	 * If a serializable class does not explicitly declare a serialVersionUID,
	 * then the serialization runtime will calculate a default serialVersionUID
	 * value for that class based on various aspects of the class, as described
	 * in the Java(TM) Object Serialization Specification. However, it is
	 * strongly recommended that all serializable classes explicitly declare
	 * serialVersionUID values, since the default serialVersionUID computation
	 * is highly sensitive to class details that may vary depending on compiler
	 * implementations, and can thus result in unexpected InvalidClassExceptions
	 * during deserialization. Therefore, to guarantee a consistent
	 * serialVersionUID value across different java compiler implementations, a
	 * serializable class must declare an explicit serialVersionUID value. It is
	 * also strongly advised that explicit serialVersionUID declarations use the
	 * private modifier where possible, since such declarations apply only to
	 * the immediately declaring class--serialVersionUID fields are not useful
	 * as inherited members.
	 */
	private static final long serialVersionUID = 1983037420201461403L;

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Marriage> findMarriagesPerson(Integer personId, Gender gender)
			throws PersistenceException {
		String queryJPQL = "FROM Marriage WHERE ";
		if ((gender == null) || (gender.equals(Gender.X))) {
			queryJPQL += "husband.personId = :husbandId or wife.personId = :wifeId ";
		} else if (gender.equals(Gender.M)) {
			queryJPQL += "husband.personId = :husbandId";
		} else if (gender.equals(Gender.F)) {
			queryJPQL += "wife.personId = :wifeId";
		}
		queryJPQL += " ORDER BY startYear asc, startMonthNum asc, startDay asc";

		Query query = getEntityManager().createQuery(queryJPQL);

		if ((gender == null) || (gender.equals(Gender.X))) {
			query.setParameter("husbandId", personId);
			query.setParameter("wifeId", personId);
		} else if (gender.equals(Gender.M)) {
			query.setParameter("husbandId", personId);
		} else if (gender.equals(Gender.F)) {
			query.setParameter("wifeId", personId);
		}

		List<Marriage> result = query.getResultList();

		return result;
	}

	@Override
	public Marriage findMarriagePerson(Integer marriageId, Integer personId,
			Gender gender) throws PersistenceException {
		String queryJPQL = "FROM Marriage WHERE marriageId = :marriageId";
		if ((gender == null) || (gender.equals(Gender.X))) {
			queryJPQL += " AND (husband.personId = :husbandId or wife.personId = :wifeId) ";
		} else if (gender.equals(Gender.M)) {
			queryJPQL += " AND (husband.personId = :husbandId)";
		} else if (gender.equals(Gender.F)) {
			queryJPQL += " AND (wife.personId = :wifeId)";
		}

		Query query = getEntityManager().createQuery(queryJPQL);
		query.setParameter("marriageId", marriageId);

		if ((gender == null) || (gender.equals(Gender.X))) {
			query.setParameter("husbandId", personId);
			query.setParameter("wifeId", personId);
		} else if (gender.equals(Gender.M)) {
			query.setParameter("husbandId", personId);
		} else if (gender.equals(Gender.F)) {
			query.setParameter("wifeId", personId);
		}

		return (Marriage) query.getSingleResult();
	}

	public Integer modifyMarriagePerson(ModifyPersonSpouseJson modifyJson)
			throws PersistenceException {

		Integer counter = 0;

		List<Marriage> newMarriages = findMarriages(modifyJson.getPersonId(),
				modifyJson.getNewSpouse());
		// new marriages exist in DB - NOT execute modification
		if (newMarriages != null && !newMarriages.isEmpty()) {
			return 0;
		}

		List<Marriage> oldMarriages = findMarriages(modifyJson.getPersonId(),
				modifyJson.getOldSpouse());
		// old marriages doesn,t exist in DB - NOT execute modification
		if (oldMarriages == null || oldMarriages.isEmpty()) {
			return 0;
		}

		for (Marriage marriage : oldMarriages) {

			if (modifyJson.getPersonId().equals(
					marriage.getWife().getPersonId())) {

				People husband = new People();
				husband.setPersonId(modifyJson.getNewSpouse().getPersonId());
				marriage.setHusband(husband);

			} else if (modifyJson.getPersonId().equals(
					marriage.getHusband().getPersonId())) {
				People wife = new People();
				wife.setPersonId(modifyJson.getNewSpouse().getPersonId());
				marriage.setWife(wife);
			} else {
				return 0;
			}
			getEntityManager().merge(marriage);
			counter++;
		}

		return counter;
	}

	public Integer addMarriagePerson(ModifyPersonSpouseJson modifyJson)
			throws PersistenceException {

		List<Marriage> marriages = findMarriages(modifyJson.getPersonId(),
				modifyJson.getNewSpouse());

		if (marriages != null && !marriages.isEmpty()) {
			return 0;
		}

		People husband = new People();
		People wife = new People();

		Marriage marriage = new Marriage();
		if (modifyJson.getNewSpouse().getSpouseType()
				.equalsIgnoreCase(SpouseType.H.toString())) {

			husband.setPersonId(modifyJson.getNewSpouse().getPersonId());
			wife.setPersonId(modifyJson.getPersonId());

		} else {
			wife.setPersonId(modifyJson.getNewSpouse().getPersonId());
			husband.setPersonId(modifyJson.getPersonId());
		}

		marriage.setHusband(husband);
		marriage.setWife(wife);
		marriage.setDateCreated(new Date());
		marriage.setEndUns(false);
		marriage.setStartUns(false);

		getEntityManager().persist(marriage);

		return 1;
	}

	public Integer deleteMarriagePerson(ModifyPersonSpouseJson modifyJson)
			throws PersistenceException {

		List<Marriage> marriages = findMarriages(modifyJson.getPersonId(),
				modifyJson.getSpouseToBeDeleted());

		if (marriages != null && !marriages.isEmpty()) {
			for (Marriage marriage : marriages) {
				getEntityManager().remove(marriage);
			}

			return marriages.size();
		}

		return 0;
	}

	private List<Marriage> findMarriages(Integer personId, Integer spouseId,
			Gender spouseGender) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"FROM Marriage WHERE husband.personId = :husbandId AND wife.personId = :wifeId");

		if (spouseGender != null && spouseGender.equals(Gender.M)) {
			query.setParameter("husbandId", spouseId);
			query.setParameter("wifeId", personId);
		} else if (spouseGender != null && spouseGender.equals(Gender.F)) {
			query.setParameter("wifeId", spouseId);
			query.setParameter("husbandId", personId);
		} else {
			return null;
		}

		List<Marriage> result = query.getResultList();

		return result;
	}

	private List<Marriage> findMarriages(Integer personId, Integer spouseId)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"FROM Marriage WHERE husband.personId = :husbandId AND wife.personId = :wifeId");

		query.setParameter("husbandId", spouseId);
		query.setParameter("wifeId", personId);
		List<Marriage> result = (List<Marriage>) query.getResultList();
		if (result == null || result.isEmpty()) {
			query.setParameter("husbandId", personId);
			query.setParameter("wifeId", spouseId);
			result = (List<Marriage>) query.getResultList();
		}

		return result;
	}

	private List<Marriage> findMarriages(Integer personId, SpouseJson spouse)
			throws PersistenceException {
		List<Marriage> marriages = null;

		if (spouse.getSpouseType().equalsIgnoreCase(SpouseType.H.toString())) {
			marriages = findMarriages(personId, spouse.getPersonId(), Gender.M);

		} else if (spouse.getSpouseType()
				.equalsIgnoreCase(SpouseType.W.toString())) {
			marriages = findMarriages(personId, spouse.getPersonId(), Gender.F);

		} else {
			return null;
		}

		return marriages;

	}

}
