package org.medici.mia.dao.comment;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.dao.Dao;
import org.medici.mia.domain.Comment;
import org.medici.mia.domain.User;

public interface CommentDAO extends Dao<Integer, Comment> {

    Comment findById(Integer id) throws PersistenceException;
    List<Comment> findByRecord(Integer id, Comment.RecordType recordType) throws PersistenceException;
    List<Comment> findByRecord(Integer id, Comment.RecordType recordType, Integer limit, Integer offset) throws PersistenceException;
    Long countByRecord(Integer id, Comment.RecordType recordType) throws PersistenceException;
    Boolean saveComment(Comment c) throws PersistenceException;
    List<Comment> getCommentsByUser(User user);
}
