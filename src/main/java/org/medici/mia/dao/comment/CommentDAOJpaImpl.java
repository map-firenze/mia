package org.medici.mia.dao.comment;

import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.Comment;
import org.medici.mia.domain.User;
import org.springframework.stereotype.Repository;

import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.util.List;

@Repository
public class CommentDAOJpaImpl extends JpaDao<Integer, Comment> implements CommentDAO {

    @Override
    public Comment findById(Integer id) throws PersistenceException {
        Query query = getEntityManager().createQuery(
                "FROM Comment c WHERE c.id=:id AND c.logicalDelete = FALSE"
        );
        query.setParameter("id", id);
        Comment c;
        try {
            c = (Comment) query.getSingleResult();
        } catch (PersistenceException ex){
            return null;
        }
        return c;
    }

    @Override
    public List<Comment> findByRecord(Integer id, Comment.RecordType recordType) throws PersistenceException {
        Query query = getEntityManager().createQuery(
            "FROM Comment c WHERE c.recordId=:id AND c.recordType=:recordType AND c.parent IS NULL"
                + " ORDER BY createdBy DESC"
        );
        query.setParameter("id", id);
        query.setParameter("recordType", recordType);
        return query.getResultList();
    }

    @Override
    public List<Comment> findByRecord(Integer id, Comment.RecordType recordType, Integer limit, Integer offset) throws PersistenceException {
        Query query = getEntityManager().createQuery(
            "FROM Comment c WHERE c.recordId=:id AND c.recordType=:recordType AND c.parent IS NULL"
                + " ORDER BY createdBy DESC"
        );
        query.setParameter("id", id);
        query.setParameter("recordType", recordType);
        if(offset != null && limit != null){
            query.setMaxResults(limit);
            query.setFirstResult(offset);
        }
        return query.getResultList();
    }

    @Override
    public Long countByRecord(Integer id, Comment.RecordType recordType) throws PersistenceException {
        Query query = getEntityManager().createQuery(
            "SELECT COUNT(c) FROM Comment c WHERE c.recordId=:id AND c.recordType=:recordType"
        );
        query.setParameter("id", id);
        query.setParameter("recordType", recordType);
        return (Long) query.getSingleResult();
    }

    @Override
    public Boolean saveComment(Comment c) throws PersistenceException {
        try {
            getEntityManager().persist(c);
            getEntityManager().flush();
            return true;
        } catch (PersistenceException ex){
            return false;
        }
    }

	@Override
	public List<Comment> getCommentsByUser(User user) {
		String queryString = 
				  "SELECT c FROM Comment c "
				+ "WHERE c.createdBy = :user "
				+ "AND c.logicalDelete = 0 "
				+ "ORDER BY c.createdAt DESC";
		
		Query query = getEntityManager().createQuery(queryString);
		query.setParameter("user", user);
		
		@SuppressWarnings("unchecked")
		List<Comment> results = (List<Comment>) query.getResultList();
		
		return results;
	}
}
