/*
 * ParentDAOJpaImpl.java
 * 
 * Developed by Medici Archive Project (2010-2012).
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.dao.parent;

import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.medici.mia.common.json.biographical.ModifyPersonChildJson;
import org.medici.mia.common.json.biographical.ModifyPersonParentsJson;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.Parent;
import org.medici.mia.domain.People;
import org.medici.mia.domain.People.Gender;
import org.springframework.stereotype.Repository;

@Repository
public class ParentDAOJpaImpl extends JpaDao<Integer, Parent> implements
		ParentDAO {
	/**
	 * 
	 * If a serializable class does not explicitly declare a serialVersionUID,
	 * then the serialization runtime will calculate a default serialVersionUID
	 * value for that class based on various aspects of the class, as described
	 * in the Java(TM) Object Serialization Specification. However, it is
	 * strongly recommended that all serializable classes explicitly declare
	 * serialVersionUID values, since the default serialVersionUID computation
	 * is highly sensitive to class details that may vary depending on compiler
	 * implementations, and can thus result in unexpected InvalidClassExceptions
	 * during deserialization. Therefore, to guarantee a consistent
	 * serialVersionUID value across different java compiler implementations, a
	 * serializable class must declare an explicit serialVersionUID value. It is
	 * also strongly advised that explicit serialVersionUID declarations use the
	 * private modifier where possible, since such declarations apply only to
	 * the immediately declaring class--serialVersionUID fields are not useful
	 * as inherited members.
	 */
	private static final long serialVersionUID = 6508807200930580427L;

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public People findChild(Integer parentId, Integer childId)
			throws PersistenceException {
		String queryJPQL = "FROM Parent WHERE ";
		queryJPQL += " parent.personId = :parentId and child.personId = :childId";

		Query query = getEntityManager().createQuery(queryJPQL);
		query.setParameter("parentId", parentId);
		query.setParameter("childId", childId);

		List<People> result = query.getResultList();
		if (result.size() == 1) {
			return result.get(0);
		} else {
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<People> findChildren(Integer parentId)
			throws PersistenceException {
		String queryJPQL = "FROM Parent WHERE ";
		queryJPQL += " parent.personId = :parentId ORDER BY child.bornYear ASC, child.bornMonth.monthNum ASC, child.bornDay ASC";

		Query query = getEntityManager().createQuery(queryJPQL);
		query.setParameter("parentId", parentId);

		return query.getResultList();
	}

	// Person Parents

	@Override
	public Integer modifyPersonParents(ModifyPersonParentsJson modifyJson)
			throws PersistenceException {

		Integer modifiedCounter = 0;

		Query query = getEntityManager().createQuery(
				"FROM Parent par WHERE par.child.personId=:childId");
		query.setParameter("childId", modifyJson.getPersonId());

		List<Parent> parents = (List<Parent>) query.getResultList();
		if (parents != null && !parents.isEmpty()) {
			for (Parent par : parents) {
				People people = new People();
				if (Gender.M.equals(par.getParent().getGender())) {
					if (modifyJson.getOldFatherId().equals(
							par.getParent().getPersonId())) {
						people.setPersonId(modifyJson.getNewFatherId());
						par.setParent(people);
						par.setLastUpdate(new Date());
						getEntityManager().merge(par);
						modifiedCounter++;
					}

				} else if (Gender.F.equals(par.getParent().getGender())) {
					if (modifyJson.getOldMotherId().equals(
							par.getParent().getPersonId())) {
						people.setPersonId(modifyJson.getNewMotherId());
						par.setParent(people);
						par.setLastUpdate(new Date());
						getEntityManager().merge(par);
						modifiedCounter++;
					}
				}
			}
		}
		return modifiedCounter;
	}

	@Override
	public Integer addPersonParents(ModifyPersonParentsJson modifyJson)
			throws PersistenceException {

		People par = null;
		Integer parentId = null;
		if (modifyJson.getNewFatherId() != null
				&& !modifyJson.getNewFatherId().equals("")) {
			par = new People();
			parentId = modifyJson.getNewFatherId();
		} else if (modifyJson.getNewMotherId() != null
				&& !modifyJson.getNewMotherId().equals("")) {
			par = new People();
			parentId = modifyJson.getNewMotherId();
		} else {
			return 0;
		}

		Query query = getEntityManager()
				.createQuery(
						"SELECT count(*) FROM Parent p WHERE p.child.personId=:childId AND p.parent.personId=:parentId");
		query.setParameter("childId", modifyJson.getPersonId());
		query.setParameter("parentId", parentId);
		if ((Long) query.getSingleResult() > 0) {
			return 0;
		}

		Parent parent = new Parent();
		People peoplePar = new People();
		peoplePar.setPersonId(parentId);
		parent.setParent(peoplePar);

		People child = new People();
		child.setPersonId(modifyJson.getPersonId());
		parent.setChild(child);

		parent.setDateCreated(new Date());
		parent.setLastUpdate(new Date());

		getEntityManager().persist(parent);
		return 1;

	}

	@Override
	public Integer deletePersonParents(ModifyPersonParentsJson modifyJson)
			throws PersistenceException {

		if (modifyJson == null || modifyJson.getParentToBeDeleted() == null
				|| modifyJson.getPersonId() == null) {

			return 0;
		}

		Query query = getEntityManager()
				.createQuery(
						"DELETE FROM Parent par WHERE par.child.personId=:childId AND par.parent.personId=:parentId");
		query.setParameter("childId", modifyJson.getPersonId());
		query.setParameter("parentId", modifyJson.getParentToBeDeleted());
		return query.executeUpdate();

	}

	// Person child

	@Override
	public Integer modifyPersonChild(ModifyPersonChildJson modifyJson)
			throws PersistenceException {

		Integer modifiedCounter = 0;

		Query query = getEntityManager()
				.createQuery(
						"SELECT p FROM Parent p WHERE p.child.personId=:childId AND p.parent.personId=:personId");

		query.setParameter("childId", modifyJson.getOldChildId());
		query.setParameter("personId", modifyJson.getPersonId());
		List<Parent> parents = (List<Parent>) query.getResultList();

		if (parents == null || parents.isEmpty()) {
			return modifiedCounter;
		}

		for (Parent parent : parents) {
			modifiedCounter++;
			People child = new People();
			child.setPersonId(modifyJson.getNewChildId());
			parent.setChild(child);
			getEntityManager().merge(parent);
		}

		return modifiedCounter;
	}

	@Override
	public Integer addPersonChild(ModifyPersonChildJson modifyJson)
			throws PersistenceException {

		Integer modifiedCounter = 0;

		Query query = getEntityManager()
				.createQuery(
						"SELECT count(*) FROM Parent p WHERE p.child.personId=:childId AND p.parent.personId=:personId");

		query.setParameter("childId", modifyJson.getNewChildId());
		query.setParameter("personId", modifyJson.getPersonId());
		Long parents = (Long) query.getSingleResult();

		if (parents > 0) {
			return 0;
		}

		Parent parent = new Parent();
		People peoplePar = new People();
		peoplePar.setPersonId(modifyJson.getPersonId());
		parent.setParent(peoplePar);

		People child = new People();
		child.setPersonId(modifyJson.getNewChildId());
		parent.setChild(child);

		parent.setDateCreated(new Date());
		parent.setLastUpdate(new Date());

		getEntityManager().persist(parent);
		return 1;

	}
	
	@Override
	public Integer deletePersonChild(ModifyPersonChildJson modifyJson)
			throws PersistenceException {

		if (modifyJson == null || modifyJson.getChildToBeDeleted() == null
				|| modifyJson.getPersonId() == null) {

			return 0;
		}

		Query query = getEntityManager()
				.createQuery(
						"SElECT p FROM Parent p WHERE p.child.personId=:childId AND p.parent.personId=:parentId");
		query.setParameter("childId", modifyJson.getChildToBeDeleted());
		query.setParameter("parentId", modifyJson.getPersonId());
		List<Parent> parents = (List<Parent>)query.getResultList();
		if(parents==null || parents.isEmpty()){
			return 0;
		}
		for(Parent parent:parents){
			getEntityManager().remove(parent);
			
		}
		
		return parents.size();

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> findPersonChildren(Integer personId)
			throws PersistenceException {
		Query query = getEntityManager().createQuery("Select p.child.personId FROM Parent p WHERE p.parent.personId=:parentId");
		query.setParameter("parentId", personId);

		return (List<Integer>) query.getResultList();
	}

}
