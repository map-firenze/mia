/*
 * UserPreferenceDAOJpaImpl.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.dao.userpreference;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.User;
import org.medici.mia.domain.UserPreference;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author mbarlotti
 *
 */
@Repository
public class UserPreferenceDAOJpaImpl extends JpaDao<Integer, UserPreference> implements UserPreferenceDAO {

	private static final long serialVersionUID = 8698627499334753020L;

	private final Logger logger = Logger.getLogger(this.getClass());

	@SuppressWarnings("unchecked")
	@Override
	public List<UserPreference> getUserPreferences(User user) throws PersistenceException {
		String queryString = "SELECT e FROM UserPreference e WHERE e.user = :user ORDER BY e.title";

		Query query = getEntityManager().createQuery(queryString);

		query.setParameter("user", user);

		List<UserPreference> res = query.getResultList();
		if (res == null || res.isEmpty()) {
			return new ArrayList<UserPreference>();
		}
		return res;
	}

	@Override
	public Long getUserPreferenceCount(User user) throws PersistenceException {
		String queryString = "SELECT COUNT(e) FROM UserPreference e WHERE e.user = :user";

		Query query = getEntityManager().createQuery(queryString);

		query.setParameter("user", user);

		Long res = (Long) query.getSingleResult();
		if (res == null) {
			return null;
		}
		return res;
	}

	@Override
	public Integer deleteUserPreference(User user, Integer id) throws PersistenceException {
		String queryString = "DELETE FROM UserPreference WHERE user = :user AND id = :id";

		Query query = getEntityManager().createQuery(queryString);

		query.setParameter("user", user);
		query.setParameter("id", id);

		return query.executeUpdate();
	}
}
