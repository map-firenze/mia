/*
 * PoLinkDAOJpaImpl.java
 * 
 * Developed by Medici Archive Project (2010-2012).
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.dao.polink;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.medici.mia.common.json.biographical.ModifyPersonTitlesOccsJson;
import org.medici.mia.common.json.biographical.PoLinkJson;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.PoLink;
import org.medici.mia.domain.TitleOccsList;
import org.springframework.stereotype.Repository;

@Repository
public class PoLinkDAOJpaImpl extends JpaDao<Integer, PoLink> implements
		PoLinkDAO {

	/**
	 * 
	 * If a serializable class does not explicitly declare a serialVersionUID,
	 * then the serialization runtime will calculate a default serialVersionUID
	 * value for that class based on various aspects of the class, as described
	 * in the Java(TM) Object Serialization Specification. However, it is
	 * strongly recommended that all serializable classes explicitly declare
	 * serialVersionUID values, since the default serialVersionUID computation
	 * is highly sensitive to class details that may vary depending on compiler
	 * implementations, and can thus result in unexpected InvalidClassExceptions
	 * during deserialization. Therefore, to guarantee a consistent
	 * serialVersionUID value across different java compiler implementations, a
	 * serializable class must declare an explicit serialVersionUID value. It is
	 * also strongly advised that explicit serialVersionUID declarations use the
	 * private modifier where possible, since such declarations apply only to
	 * the immediately declaring class--serialVersionUID fields are not useful
	 * as inherited members.
	 */
	private static final long serialVersionUID = 2663955366990151079L;

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public PoLink find(Integer personId, Integer prfLinkId)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"from PoLink where prfLinkId=:prfLinkId and person.personId=:personId");
		query.setParameter("prfLinkId", prfLinkId);
		query.setParameter("personId", personId);

		List<PoLink> result = query.getResultList();
		if (result.size() == 0) {
			return null;
		} else {
			return result.get(0);
		}
	}

	@Override
	public Integer addPersonTittleOcc(PoLink poLink)
			throws PersistenceException {

		if (poLink == null || poLink.getPerson() == null) {
			return -1;
		}

		Query query = getEntityManager()
				.createQuery(
						"from PoLink where titleOccList.titleOccId=:titleOccId and person.personId=:personId");
		query.setParameter("titleOccId", poLink.getTitleOccList()
				.getTitleOccId());
		query.setParameter("personId", poLink.getPerson().getPersonId());

		if (query.getResultList() == null || query.getResultList().isEmpty()) {
			getEntityManager().persist(poLink);
			if (poLink.getPreferredRole() != null
					&& poLink.getPreferredRole().booleanValue()) {
				resetPreferredRoleForPersonTitles(poLink.getPrfLinkId(), poLink
						.getPerson().getPersonId());
			}
			return poLink.getPrfLinkId();
		}

		return 0;

	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void resetPreferredRoleForPersonTitles(Integer prfLinkId,
			Integer personId) throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"from PoLink where prfLinkId!=:prfLinkId and person.personId=:personId and preferredRole=:booleanValue");
		query.setParameter("prfLinkId", prfLinkId);
		query.setParameter("personId", personId);
		query.setParameter("booleanValue", Boolean.TRUE);

		List<PoLink> result = query.getResultList();

		for (int i = 0; i < result.size(); i++) {
			PoLink singleResult = result.get(i);
			singleResult.setPreferredRole(Boolean.FALSE);
			getEntityManager().merge(singleResult);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Integer deletePersonTittleOcc(Integer personId, Integer titleId)
			throws PersistenceException {

		if (personId == null || titleId == null) {
			return -1;
		}

		Query query = getEntityManager()
				.createQuery(
						"from PoLink where titleOccList.titleOccId=:titleOccId and person.personId=:personId");
		query.setParameter("titleOccId", titleId);
		query.setParameter("personId", personId);

		List<PoLink> result = query.getResultList();
		if (result == null || result.isEmpty()) {
			return 0;
		}
		getEntityManager().remove((PoLink) result.get(0));
		return 1;

	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<PoLink> getOccupationsDetails(String alias,
			List<Integer> peopleIds) throws PersistenceException {
		StringBuilder query = new StringBuilder(
				"from PoLink WHERE titleOccList.titleOccId=:titleOccId AND (");
		for (int i = 0; i < peopleIds.size(); i++) {
			query.append("person.personId=" + peopleIds.get(i));
			if (i != peopleIds.size() - 1) {
				query.append(" OR ");
			}
		}
		Query toQuery = getEntityManager().createQuery(query.toString() + ")");
		toQuery.setParameter("titleOccId", Integer.parseInt(alias));

		return toQuery.getResultList();
	}

	public Integer modifyPersonTittleOcc(ModifyPersonTitlesOccsJson modifyJson)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"from PoLink where titleOccList.titleOccId=:titleOccId and person.personId=:personId");
		query.setParameter("titleOccId", modifyJson.getOldTitleAndOccupation());
		query.setParameter("personId", modifyJson.getPersonId());

		List<PoLink> oldresult = query.getResultList();

		if (oldresult == null || oldresult.isEmpty()) {
			return -1;
		}

		PoLink poLink = oldresult.get(0);
		// Modifica solo poLink
		if (modifyJson.getNewTitleAndOccupation() != null
				&& modifyJson.getNewTitleAndOccupation().equals(
						modifyJson.getOldTitleAndOccupation())) {
			updatePolink(modifyJson, poLink);
			getEntityManager().merge(poLink);
			if (modifyJson.getPreferredRole() != null
					&& modifyJson.getPreferredRole() == 1) {
				setPrRolesToZero(modifyJson.getPersonId(),
						modifyJson.getNewTitleAndOccupation());
			}
			return 1;
		}

		query = getEntityManager()
				.createQuery(
						"from PoLink where titleOccList.titleOccId=:titleOccId and person.personId=:personId");
		query.setParameter("titleOccId", modifyJson.getNewTitleAndOccupation());
		query.setParameter("personId", modifyJson.getPersonId());

		List<PoLink> newresult = query.getResultList();

		if (newresult == null || newresult.isEmpty()) {

			TitleOccsList titleOcc = new TitleOccsList();
			titleOcc.setTitleOccId(modifyJson.getNewTitleAndOccupation());
			poLink.setTitleOcc(titleOcc);

			if (modifyJson.getPreferredRole() != null
					&& modifyJson.getPreferredRole() == 1) {
				poLink.setPreferredRole(true);
			} else {
				poLink.setPreferredRole(false);
			}

			PoLinkJson poLinkJson = modifyJson.getPoLink();
			if (poLinkJson == null) {
				poLink.setEndApprox(false);
				poLink.setEndUns(false);
				poLink.setStartApprox(false);
				poLink.setStartUns(false);

			} else {

				if (poLinkJson.getEndApprox() == null
						|| poLinkJson.getEndApprox().equals(0)) {
					poLink.setEndApprox(false);
				} else {
					poLink.setEndApprox(true);
				}

				if (poLinkJson.getEndUnsure() == null
						|| poLinkJson.getEndUnsure().equals(0)) {
					poLink.setEndUns(false);
				} else {
					poLink.setEndUns(true);
				}

				if (poLinkJson.getStartApprox() == null
						|| poLinkJson.getStartApprox().equals(0)) {
					poLink.setStartApprox(false);
				} else {
					poLink.setStartApprox(true);
				}

				if (poLinkJson.getStartUnsure() == null
						|| poLinkJson.getStartUnsure().equals(0)) {
					poLink.setStartUns(false);
				} else {
					poLink.setStartUns(true);
				}

				poLink.setStartYear(poLinkJson.getStartYear());
				poLink.setStartMonth(poLinkJson.getStartMonth());
				poLink.setStartDay(poLinkJson.getStartDay());
				poLink.setEndYear(poLinkJson.getEndYear());
				poLink.setEndMonth(poLinkJson.getEndMonth());
				poLink.setEndDay(poLinkJson.getEndDay());
				poLink.setDateCreated(new Date());
			}
			getEntityManager().merge(poLink);
			setPrRolesToZero(modifyJson.getPersonId(),
					modifyJson.getNewTitleAndOccupation());

			return 1;
		}
		return 0;
	}

	private void updatePolink(ModifyPersonTitlesOccsJson modifyJson,
			PoLink poLink) {
		if (modifyJson.getPreferredRole() != null
				&& modifyJson.getPreferredRole() == 1) {
			poLink.setPreferredRole(true);
		} else {
			poLink.setPreferredRole(false);
		}

		PoLinkJson poLinkJson = modifyJson.getPoLink();
		if (poLinkJson == null) {
			poLink.setEndApprox(false);
			poLink.setEndUns(false);
			poLink.setStartApprox(false);
			poLink.setStartUns(false);

		} else {

			if (poLinkJson.getEndApprox() == null
					|| poLinkJson.getEndApprox().equals(0)) {
				poLink.setEndApprox(false);
			} else {
				poLink.setEndApprox(true);
			}

			if (poLinkJson.getEndUnsure() == null
					|| poLinkJson.getEndUnsure().equals(0)) {
				poLink.setEndUns(false);
			} else {
				poLink.setEndUns(true);
			}

			if (poLinkJson.getStartApprox() == null
					|| poLinkJson.getStartApprox().equals(0)) {
				poLink.setStartApprox(false);
			} else {
				poLink.setStartApprox(true);
			}

			if (poLinkJson.getStartUnsure() == null
					|| poLinkJson.getStartUnsure().equals(0)) {
				poLink.setStartUns(false);
			} else {
				poLink.setStartUns(true);
			}

			poLink.setStartYear(poLinkJson.getStartYear());
			poLink.setStartMonth(poLinkJson.getStartMonth());
			poLink.setStartDay(poLinkJson.getStartDay());
			poLink.setEndYear(poLinkJson.getEndYear());
			poLink.setEndMonth(poLinkJson.getEndMonth());
			poLink.setEndDay(poLinkJson.getEndDay());

		}
	}

	private void setPrRolesToZero(Integer personId, Integer titleOccListId) {

		// Controlling active preferred role
		PoLink prefRolePolink = null;

		Query queryPrefRole = getEntityManager()
				.createQuery(
						"from PoLink where person.personId=:personId and preferredRole IS NOT NULL and preferredRole=1");
		queryPrefRole.setParameter("personId", personId);

		List<PoLink> prefRoleResult = (List<PoLink>) queryPrefRole
				.getResultList();

		if (prefRoleResult != null && !prefRoleResult.isEmpty()) {
			for (PoLink polink : prefRoleResult) {

				if (polink.getTitleOccList() != null
						&& !titleOccListId.equals(polink.getTitleOccList()
								.getTitleOccId())) {

					polink.setPreferredRole(Boolean.FALSE);
					getEntityManager().merge(polink);
					return;

				}

			}
		}

	}

	// ISSUE 140
	public List<Integer> findPersonsByTitleOcc(Integer titleOccId)
			throws PersistenceException {

		// Controlling active preferred role
		List<Integer> peopleIds = null;

		Query query = getEntityManager().createQuery(
				"from PoLink where titleOccList.titleOccId=:titleOccId");
		query.setParameter("titleOccId", titleOccId);

		List<PoLink> results = (List<PoLink>) query.getResultList();

		if (results != null && !results.isEmpty()) {
			peopleIds = new ArrayList<Integer>();
			for (PoLink polink : results) {
				if (polink.getPerson() != null) {
					peopleIds.add(polink.getPerson().getPersonId());

				}

			}
		}

		return peopleIds;

	}
}
