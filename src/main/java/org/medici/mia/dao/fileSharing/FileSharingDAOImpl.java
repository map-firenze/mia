/*
 * FileSharingDAOImpl.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.dao.fileSharing;

import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.FileSharing;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.User;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Alessandro Binchi
 *
 */

@Repository
public class FileSharingDAOImpl extends JpaDao<Integer, FileSharing> implements
FileSharingDAO {

	@Override
	public void saveSharedUsers(FileSharing fileSharing) throws PersistenceException {
		if (fileSharing != null) {
			getEntityManager().persist(fileSharing);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<FileSharing> getByUploadAndFile(Integer uploadId, Integer fileId) throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"FROM FileSharing f WHERE f.tblUpload.uploadInfoId=:uploadId AND f.tblUploadedFiles.uploadFileId=:fileId ORDER BY f.user.account");
		query.setParameter("uploadId", uploadId);
		query.setParameter("fileId", fileId);
		
		return (List<FileSharing>) query.getResultList();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<User> findSharedUsersByUpdateId(Integer uploadId) throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT DISTINCT f.user FROM FileSharing f WHERE f.tblUpload.uploadInfoId=:uploadId ORDER BY f.user.account");
		query.setParameter("uploadId", uploadId);
		
		return (List<User>) query.getResultList();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<UploadFileEntity> findSharedByUpdateId(Integer uploadId) throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT DISTINCT f.tblUploadedFiles FROM FileSharing f WHERE f.tblUpload.uploadInfoId=:uploadId");
		query.setParameter("uploadId", uploadId);
		
		return (List<UploadFileEntity>) query.getResultList();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<FileSharing> findSharedEntityByUpdateId(Integer uploadId) throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT DISTINCT f FROM FileSharing f WHERE f.tblUpload.uploadInfoId=:uploadId");
		query.setParameter("uploadId", uploadId);

		return (List<FileSharing>) query.getResultList();
	}
	
	public Integer deleteSharedUsers(Integer uploadId, Integer fileId) throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"DELETE FROM FileSharing f WHERE f.tblUpload.uploadInfoId=:uploadId AND f.tblUploadedFiles.uploadFileId=:fileId");
		query.setParameter("uploadId", uploadId);
		query.setParameter("fileId", fileId);
		return query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FileSharing> findByUploadFileIdAndAccount(Integer uploadFileId, String username) {
		Query query = getEntityManager()
				.createQuery(
						"SELECT DISTINCT f FROM FileSharing f WHERE f.tblUploadedFiles.uploadFileId=:uploadFileId AND f.user.account=:username");
		query.setParameter("uploadFileId", uploadFileId);
		query.setParameter("username", username);
		return (List<FileSharing>) query.getResultList();
		
	}
	
}
