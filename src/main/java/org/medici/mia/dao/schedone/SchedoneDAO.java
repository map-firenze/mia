/*
 * SchedoneDAO.java
 * 
 * Developed by Medici Archive Project (2010-2012).
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.dao.schedone;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.dao.Dao;
import org.medici.mia.domain.Schedone;
import org.medici.mia.domain.Volume;

/**
 * Schedone DAO.
 * 
 * @author Lorenzo Pasquinelli (<a
 *         href=mailto:l.pasquinelli@gmail.com>l.pasquinelli@gmail.com</a>)
 *         @author Matteo Doni (<a href=mailto:donimatteo@gmail.com>donimatteo@gmail.com</a>)
 */
public interface SchedoneDAO extends Dao<Integer, Schedone> {

	/**
	 * 
	 * @param summaryId
	 * @return
	 * @throws PersistenceException
	 */
	Schedone findBySummaryId(Integer summaryId) throws PersistenceException;
	
	/**
	 * 
	 * @param volNum
	 * @param volLetExt
	 * @return
	 * @throws PersistenceException
	 */
	Schedone findByVolume(Integer volNum, String volLetExt) throws PersistenceException;
	
	/**
	 * 
	 * @param volNum
	 * @param volNumBetween
	 * @return
	 * @throws PersistenceException
	 */
	List<Schedone> findByVolumesNumber(Integer volNum, Integer volNumBetween) throws PersistenceException;
	
	public Integer insertSchedone(Schedone schedoneEntity) throws PersistenceException;

	void updateSchedone(Schedone schedoneEntity) throws PersistenceException;

}
