package org.medici.mia.dao.collation;

import org.medici.mia.dao.Dao;
import org.medici.mia.domain.Collation;
import org.medici.mia.domain.MiaDocumentEntity;

import javax.persistence.PersistenceException;
import java.util.List;

public interface CollationDAO extends Dao<Integer, Collation> {
    Collation findById(Integer id) throws PersistenceException;
    Collation saveCollation(Collation collation) throws PersistenceException;
    List<Collation> findByDocumentId(Integer documentId) throws PersistenceException;
    List<Collation> findByDocumentId(Integer documentId, Collation.CollationType type) throws PersistenceException;
    List<Collation> findByUser(String account) throws PersistenceException;
    Boolean checkIfAlreadyParent(Integer parentId) throws PersistenceException;
    Boolean checkIfAlreadyChild(Integer childId) throws PersistenceException;
    Boolean checkIfAttachmentExists(Integer childId, Integer parentId) throws PersistenceException;
    List<Collation> findByChildVolumeId(Integer volumeId) throws PersistenceException;
    List<Collation> findByChildInsertId(Integer insertId) throws PersistenceException;
    List<Collation> findByParentVolumeId(Integer volumeId) throws PersistenceException;
    List<Collation> findByParentInsertId(Integer insertId) throws PersistenceException;
    Boolean findConflictsInDeleted(Integer childDocId, Integer parentDocId) throws PersistenceException;
}
