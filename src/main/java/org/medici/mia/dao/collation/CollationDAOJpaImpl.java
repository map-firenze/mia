package org.medici.mia.dao.collation;

import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.Collation;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.util.List;

@Repository
public class CollationDAOJpaImpl extends JpaDao<Integer, Collation> implements CollationDAO {

    @Override
    public Collation findById(Integer id) throws PersistenceException {
        Query query = getEntityManager()
                .createQuery("FROM Collation c where c.id=:id"
//                        +" AND c.logicalDelete = FALSE"
                );
        query.setParameter("id", id);
        Collation c;
        try{
            c = (Collation) query.getSingleResult();
        } catch (NoResultException ex){
            c = null;
        }
        return c;
    }

    @Override
    public List<Collation> findByDocumentId(Integer documentId) throws PersistenceException {
        Query query = getEntityManager()
                .createQuery("FROM Collation c " +
                        "where (c.childDocument.documentEntityId=:documentId OR c.parentDocument.documentEntityId=:documentId)"
//                        +" AND logicalDelete = FALSE"
                );
        query.setParameter("documentId", documentId);
        return query.getResultList();
    }

    @Override
    public List<Collation> findByDocumentId(Integer documentId, Collation.CollationType type) throws PersistenceException {
        Query query = getEntityManager()
                .createQuery("FROM Collation c "
                        +"where (c.childDocument.documentEntityId=:documentId OR c.parentDocument.documentEntityId=:documentId)"
                        +" AND c.type=:type"
//                        +" AND logicalDelete=FALSE"
                );
        query.setParameter("documentId", documentId);
        query.setParameter("type", type);
        return query.getResultList();
    }

    @Override
    public Boolean checkIfAlreadyParent(Integer childId) throws PersistenceException {
        Query query = getEntityManager()
                .createQuery("SELECT COUNT(c) FROM Collation c where c.parentDocument.documentEntityId=:childId"
//                        + " AND logicalDelete=FALSE"
                );
        query.setParameter("childId", childId);
        return (Long) query.getSingleResult() > 0;
    }

    @Override
    public Boolean checkIfAlreadyChild(Integer childId) throws PersistenceException {
        Query query = getEntityManager()
                .createQuery("SELECT COUNT(c) FROM Collation c where c.childDocument.documentEntityId=:childId"
//                        +" AND logicalDelete = FALSE"
                );
        query.setParameter("childId", childId);
        return (Long) query.getSingleResult() > 0;
    }

    @Override
    public Boolean checkIfAttachmentExists(Integer childId, Integer parentId) throws PersistenceException {
        Query query = getEntityManager()
                .createQuery("SELECT COUNT(c) FROM Collation c where c.parentDocument.documentEntityId=:parentId"
                        +" AND c.childDocument.documentEntityId=:childId"
//                        +" AND logicalDelete = FALSE"
                );
        query.setParameter("childId", childId);
        query.setParameter("parentId", parentId);
        return (Long) query.getSingleResult() > 0;
    }

    @Override
    public List<Collation> findByUser(String account) throws PersistenceException {
        Query query = getEntityManager()
                .createQuery("FROM Collation c WHERE c.createdBy.account=:account"
//                        +" AND logicalDelete = FALSE"
                );
        query.setParameter("account", account);
        return query.getResultList();
    }

    @Override
    public Collation saveCollation(Collation collation) throws PersistenceException {
        getEntityManager().persist(collation);
        getEntityManager().flush();
        return collation;
    }

    @Override
    public List<Collation> findByChildVolumeId(Integer volumeId) throws PersistenceException {
        Query query = getEntityManager()
                .createQuery("FROM Collation c WHERE c.childVolume.summaryId=:volumeId"
//                        +" AND logicalDelete = FALSE"
                );
        query.setParameter("volumeId", volumeId);
        return query.getResultList();
    }

    @Override
    public List<Collation> findByChildInsertId(Integer insertId) throws PersistenceException {
        Query query = getEntityManager()
                .createQuery("FROM Collation c WHERE c.childInsert.insertId=:insertId"
//                        +" AND logicalDelete = FALSE"
                );
        query.setParameter("insertId", insertId);
        return query.getResultList();
    }

    @Override
    public List<Collation> findByParentVolumeId(Integer volumeId) throws PersistenceException {
        Query query = getEntityManager()
                .createQuery("FROM Collation c WHERE c.parentVolume.summaryId=:volumeId"
//                        +" AND logicalDelete = FALSE"
                );
        query.setParameter("volumeId", volumeId);
        return query.getResultList();
    }

    @Override
    public List<Collation> findByParentInsertId(Integer insertId) throws PersistenceException {
        Query query = getEntityManager()
                .createQuery("FROM Collation c WHERE c.parentInsert.insertId=:insertId"
//                        +" AND logicalDelete = FALSE"
                );
        query.setParameter("insertId", insertId);
        return query.getResultList();
    }

    @Override
    public Boolean findConflictsInDeleted(Integer childDocId, Integer parentDocId) throws PersistenceException {
        Query query;
        if(parentDocId == null){
            query = getEntityManager()
                    .createQuery("SELECT COUNT(c) FROM Collation c where" +
                            " c.parentDocument.documentEntityId=:childId" +
                            " OR c.childDocument.documentEntityId=:childId");
            query.setParameter("childId", childDocId);
        } else {
            query = getEntityManager()
                    .createQuery("SELECT COUNT(c) FROM Collation c where" +
                            " c.parentDocument.documentEntityId=:parentId AND c.childDocument.documentEntityId=:childId" +
                            " OR c.parentDocument.documentEntityId=:childId" +
                            " OR c.childDocument.documentEntityId=:parentId");
            query.setParameter("childId", childDocId);
            query.setParameter("parentId", parentDocId);
        }

        return ((Long) query.getSingleResult()) > 0;
    }

    //    @Override
//    public List<Collation> findByDestinationFileId(Integer fileId) throws PersistenceException {
//        Query query = getEntityManager()
//                .createQuery("FROM Collation c where c.createdBy=:account");
//        query.setParameter("account", fileId);
//        return query.getResultList();
//    }


}