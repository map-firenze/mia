package org.medici.mia.dao.folio;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.common.json.folio.FolioJson;
import org.medici.mia.common.json.folio.FolioResponseJson;
import org.medici.mia.dao.Dao;
import org.medici.mia.domain.FolioEntity;
/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

public interface FolioDao extends Dao<Integer, FolioEntity> {

	// Return the list of DocumentId by UploadFileId
	public List<FolioEntity> findFoliosByUploadFileId(Integer uploadFileId)
			throws PersistenceException;

	public Integer insertFolio(FolioEntity folio) throws PersistenceException;

	public void insertFolios(List<FolioEntity> folios)
			throws PersistenceException;

	public Integer updateFolio(FolioEntity folio) throws PersistenceException;

	public Integer insertOrUpdateFolio(FolioEntity folio)
			throws PersistenceException;

	public Integer updateFolio(FolioJson folio) throws PersistenceException;

	public Integer deleteFolio(FolioJson folio) throws PersistenceException;

	public List<FolioResponseJson>  insertAllFolios(List<FolioEntity> folios)
			throws PersistenceException;
	
	public Integer updateFolios(List<FolioEntity> folios) throws PersistenceException;
	
	public Integer insertFolio(FolioJson folioJson);
	
	public FolioEntity findById(Integer folioId);

}
