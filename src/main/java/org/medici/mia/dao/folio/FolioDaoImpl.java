package org.medici.mia.dao.folio;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.medici.mia.common.json.folio.FolioJson;
import org.medici.mia.common.json.folio.FolioResponseJson;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.FolioEntity;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Repository
public class FolioDaoImpl extends JpaDao<Integer, FolioEntity> implements
		FolioDao {

	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	@Override
	public List<FolioEntity> findFoliosByUploadFileId(Integer uploadFileId)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT u FROM FolioEntity u WHERE u.uploadedFileId =:uploadFileId");
		query.setParameter("uploadFileId", uploadFileId);

		return query.getResultList();

	}

	@Override
	public Integer insertFolio(FolioEntity folio) throws PersistenceException {

		if (folio == null)
			return 0;

		List<FolioEntity> folioents = findFoliosByUploadFileId(folio
				.getUploadedFileId());
		// New implementation to avoid insert of duplicate folio
		if (folioents != null && !folioents.isEmpty()) {
			for (FolioEntity fol : folioents) {
				// if ((fol.getRectoverso()
				// .equalsIgnoreCase(folio.getRectoverso()))
				// && (fol.getFolioNumber().equalsIgnoreCase(folio
				// .getFolioNumber()))) {
				// return 0;
				// }
				if (fol.equals(folio)) {
					return fol.getFolioId();
				}

			}
		}
		folio.setFolioId(getMaxFolioId() + 1);
		getEntityManager().persist(folio);
		return folio.getFolioId();

	}

	@Override
	public List<FolioResponseJson> insertAllFolios(List<FolioEntity> foliosToAdd)
			throws PersistenceException {

		if (foliosToAdd == null || foliosToAdd.isEmpty())
			return null;

		List<FolioResponseJson> folioResp = new ArrayList<FolioResponseJson>();

		int count = 0;
		// step1: Delete all folios
		List<FolioEntity> foliosToDelete = findFoliosByUploadFileId(foliosToAdd
				.get(0).getUploadedFileId());
		if (foliosToDelete != null && !foliosToDelete.isEmpty()) {
			for (FolioEntity folioToDelete : foliosToDelete) {
				getEntityManager().remove(folioToDelete);
			}

			getEntityManager().flush();
		}

		// step2: Add all folios

		Integer maxFolioId = getMaxFolioId();
		for (FolioEntity folio : foliosToAdd) {
			count++;
			folio.setFolioId(maxFolioId + count);
			getEntityManager().persist(folio);
			folioResp.add(new FolioResponseJson(folio.getFolioId(), folio
					.getUploadedFileId()));

		}

		return folioResp;

	}

	@Override
	public Integer insertOrUpdateFolio(FolioEntity folio)
			throws PersistenceException {

		if (folio != null) {

			List<FolioEntity> folioents = findFoliosByUploadFileId(folio
					.getUploadedFileId());
			// New implementation to avoid insert of duplicate folio
			if (folioents != null && !folioents.isEmpty()) {
				for (FolioEntity fol : folioents) {
					if ((fol.getRectoverso() != null && fol.getRectoverso()
							.equalsIgnoreCase(folio.getRectoverso()))
							&& (fol.getFolioNumber() != null && fol
									.getFolioNumber().equalsIgnoreCase(
											folio.getFolioNumber()))) {
						return 0;
					}
				}
			}

			if (folio.getFolioId() == null
					|| String.valueOf(folio.getFolioId()).isEmpty()) {
				folio.setFolioId(getMaxFolioId() + 1);
			} else {
				FolioEntity folioEntity = findFolio(folio.getUploadedFileId(),
						folio.getFolioId());
				if (folioEntity != null) {
					updateFolio(folio);
					return folio.getFolioId();
				}
			}

			getEntityManager().persist(folio);
			return folio.getFolioId();

		} else {
			return 0;
		}
	}

	@Override
	public void insertFolios(List<FolioEntity> folios)
			throws PersistenceException {

		if (folios != null && !folios.isEmpty()) {
			for (FolioEntity folio : folios) {
				insertFolio(folio);
			}

		}

	}

	@Override
	public Integer updateFolio(FolioEntity folio) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"UPDATE FolioEntity f SET f.folioNumber=:folioNumber, f.rectoverso=:rectoverso WHERE f.folioId=:folioId and f.uploadedFileId=:uploadedFileId");
		query.setParameter("folioNumber", folio.getFolioNumber());
		query.setParameter("rectoverso", folio.getRectoverso());
		query.setParameter("uploadedFileId", folio.getUploadedFileId());
		query.setParameter("folioId", folio.getFolioId());
		return query.executeUpdate();
	}

	@Override
	public Integer updateFolio(FolioJson folio) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT f from FolioEntity f WHERE f.folioId=:folioId and f.uploadedFileId=:uploadedFileId");

		query.setParameter("uploadedFileId", folio.getUploadFileId());
		query.setParameter("folioId", folio.getFolioId());

		@SuppressWarnings("unchecked")
		List<FolioEntity> folios = (List<FolioEntity>) query.getResultList();
		if (folios == null || folios.isEmpty()) {
			return 0;
		}

		FolioEntity folioEnt = folios.get(0);
		folioEnt.setRectoverso(folio.getRectoverso());
		folioEnt.setFolioNumber(folio.getFolioNumber());
		if (folio.getNoNumb() == null || folio.getNoNumb() == false) {
			folioEnt.setNoNumb(Boolean.FALSE);
		} else {
			folioEnt.setNoNumb(Boolean.TRUE);
		}

		getEntityManager().merge(folioEnt);
		return 1;

	}

	@Override
	public Integer updateFolios(List<FolioEntity> folios)
			throws PersistenceException {

		if (folios == null || folios.isEmpty()) {
			return 0;
		}

		Integer count = 0;

		for (FolioEntity folio : folios) {
			count = count + updateFolio(folio);
		}

		return count;

	}

	@Override
	public Integer deleteFolio(FolioJson folio) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT f from FolioEntity f WHERE f.folioId=:folioId and f.uploadedFileId=:uploadedFileId");

		query.setParameter("uploadedFileId", folio.getUploadFileId());
		query.setParameter("folioId", folio.getFolioId());

		@SuppressWarnings("unchecked")
		List<FolioEntity> folios = (List<FolioEntity>) query.getResultList();
		if (folios == null || folios.isEmpty()) {
			return 0;
		}

		FolioEntity folioEnt = folios.get(0);

		getEntityManager().remove(folioEnt);
		return 1;

	}
	
	@Override
	public Integer insertFolio(FolioJson folioJson) {
		FolioEntity folioEntity = new FolioEntity();
		folioEntity.setFolioNumber(folioJson.getFolioNumber());
		folioEntity.setNoNumb(folioJson.getNoNumb());
		folioEntity.setRectoverso(folioJson.getRectoverso());
		folioEntity.setUploadedFileId(folioJson.getUploadFileId());
		
		return insertFolio(folioEntity);
	}

	private Integer getMaxFolioId() {

		Query query = getEntityManager().createQuery(
				"SELECT max(u.folioId) from FolioEntity u");

		Object obj = query.getSingleResult();
		if (obj == null)
			return 0;
		return (Integer) obj;

	}
	
	@Override
	public FolioEntity findById(Integer folioId) {
		Query query = getEntityManager()
				.createQuery(
						"SELECT f FROM FolioEntity f WHERE f.folioId =:folioId");
		query.setParameter("folioId", folioId);

		@SuppressWarnings("unchecked")
		List<FolioEntity> folios = (List<FolioEntity>) query.getResultList();

		if (folios != null && !folios.isEmpty()) {
			return folios.get(0);
		}

		return null;
	}

	private FolioEntity findFolio(Integer uploadFileId, Integer folioId)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT u FROM FolioEntity u WHERE u.uploadedFileId =:uploadFileId and u.folioId =:folioId");
		query.setParameter("uploadFileId", uploadFileId);
		query.setParameter("folioId", folioId);

		@SuppressWarnings("unchecked")
		List<FolioEntity> folios = (List<FolioEntity>) query.getResultList();

		if (folios != null && !folios.isEmpty()) {
			return folios.get(0);
		}

		return null;

	}
}
