/*
 * PeopleDAOJpaImpl.java
 *
 * Developed by Medici Archive Project (2010-2012).
 *
 * This file is part of DocSources.
 *
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.dao.historylog;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.medici.mia.common.util.StringUtils;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.domain.User;
import org.medici.mia.domain.HistoryLogEntity.RecordType;
import org.springframework.stereotype.Repository;

@Repository
public class HistoyLogDAOImpl extends JpaDao<Integer, HistoryLogEntity>
		implements HistoyLogDAO {

	private static final long serialVersionUID = 1L;
	//	private static String USER_HISTORY = "SELECT T1 FROM HistoryLogEntity T1 WHERE T1.dateAndTime = ( SELECT max(T2.dateAndTime) FROM HistoryLogEntity T2 WHERE T1.idLastAccessedItems=T2.idLastAccessedItems ) and T1.recordType = :recordType and T1.user.account = :account";
	private static String USER_HISTORY = "SELECT e FROM HistoryLogEntity e WHERE  e.recordType = :recordType and e.user.account = :account order by e.dateAndTime desc";
	@Override
	public Integer registerLoaAction(HistoryLogEntity entity)
			throws PersistenceException {
		getEntityManager().persist(entity);

		return entity.getIdLastAccessedItems();
	}

	@Override
	public HistoryLogEntity getLogAction(String account, RecordType recordType)
			throws PersistenceException {

		if (recordType == null || account == null) {
			return null;
		}
		Query query;
		if (recordType.toString().equals("Insert") || recordType.toString().equals("Volume")){
			USER_HISTORY = "SELECT e FROM HistoryLogEntity e WHERE e.recordType in (:insert,:volume) and e.user.account = :account order by e.dateAndTime desc";
			query = getEntityManager().createQuery(USER_HISTORY);
			query.setParameter("insert", RecordType.INS);
			query.setParameter("volume", RecordType.VOL);
		} else {
			USER_HISTORY = "SELECT e FROM HistoryLogEntity e WHERE e.recordType = :recordType and e.user.account = :account order by e.dateAndTime desc";
			query = getEntityManager().createQuery(USER_HISTORY);
			query.setParameter("recordType", recordType);
		}
		query.setParameter("account", account);
		query.setMaxResults(1);
		List<HistoryLogEntity> hist = query.getResultList();

		if (hist == null || hist.isEmpty()) {
			return null;
		}

		return hist.get(0);

	}

	@Override
	public List<HistoryLogEntity> getActionsHistory(Integer page, Integer perPage) throws PersistenceException {
		String actionHistoryQuery = "SELECT e FROM HistoryLogEntity e WHERE e.action IN ('EDIT', 'CREATE', 'DELETE', 'UNDELETE') " +
				"ORDER BY e.dateAndTime DESC";
		Query query = getEntityManager().createQuery(actionHistoryQuery);
		if(page != null && perPage != null && page > 0 && perPage > 0) {
			Integer limit = page * perPage;
			Integer offset = limit - perPage;
			if(limit >= offset){
				query.setMaxResults(perPage);
				query.setFirstResult(offset);
			}
		}
		List<HistoryLogEntity> res = query.getResultList();
		if(res == null || res.isEmpty()) {
			return new ArrayList<HistoryLogEntity>();
		}

		return res;
	}

	@Override
	public List<HistoryLogEntity> getActionsHistory(Integer entryId, RecordType recordType, Integer page, Integer perPage) throws PersistenceException {
		String actionHistoryQuery = "SELECT e FROM HistoryLogEntity e WHERE e.entryId = :entryId " +
				"AND e.recordType = :recordType AND e.action IN ('EDIT', 'CREATE', 'DELETE', 'UNDELETE')  " +
				"ORDER BY e.dateAndTime DESC";
		if(recordType == null || entryId == null){
			return null;
		}
		Query query = getEntityManager().createQuery(actionHistoryQuery);
		query.setParameter("entryId", entryId);
		query.setParameter("recordType", recordType);
		if(page != null && perPage != null && page > 0 && perPage > 0) {
			Integer limit = page * perPage;
			Integer offset = limit - perPage;
			if(limit >= offset){
				query.setMaxResults(perPage);
				query.setFirstResult(offset);
			}
		}
		List<HistoryLogEntity> res = query.getResultList();
		if(res == null || res.isEmpty()) {
			return new ArrayList<HistoryLogEntity>();
		}

		return res;
	}

	@Override
	public Long getActionsHistoryCount() throws PersistenceException {
		String actionHistoryQuery = "SELECT COUNT(e.idLastAccessedItems) FROM HistoryLogEntity e WHERE e.action IN ('EDIT', 'CREATE', 'DELETE', 'UNDELETE') " +
				"ORDER BY e.dateAndTime DESC";
		Query query = getEntityManager().createQuery(actionHistoryQuery);
		Long res = (Long) query.getSingleResult();
		if(res == null) {
			return null;
		}
		return res;
	}

	@Override
	public Long getActionsHistoryCount(Integer entryId, RecordType recordType) throws PersistenceException {
		String actionHistoryQuery = "SELECT COUNT(e.idLastAccessedItems) FROM HistoryLogEntity e WHERE e.entryId = :entryId " +
				"AND e.recordType = :recordType AND e.action IN ('EDIT', 'CREATE', 'DELETE', 'UNDELETE')  " +
				"ORDER BY e.dateAndTime DESC";
		if(recordType == null || entryId == null){
			return null;
		}
		Query query = getEntityManager().createQuery(actionHistoryQuery);
		query.setParameter("entryId", entryId);
		query.setParameter("recordType", recordType);
		Long res = (Long) query.getSingleResult();
		if(res == null) {
			return null;
		}

		return res;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getHistoryItems(String account, String type, Integer firstResult, Integer maxResult,
			String orderColumn, String ascOrDesc) throws PersistenceException {
		if (orderColumn == null || orderColumn.isEmpty()) {
			orderColumn = "activityDate";
		}

		if (firstResult == null || firstResult.equals("")) {
			firstResult = 0;
		}

		if (maxResult == null || maxResult.equals("")) {
			maxResult = 20;
		}

		String queryStr = "SELECT h.entryId, CASE WHEN h.recordType = 'DE' THEN 'Document' WHEN h.recordType = 'AE' THEN 'Upload' WHEN h.recordType = 'BIO' THEN 'Person' " +
				"WHEN h.recordType = 'GEO' THEN 'Place' WHEN h.recordType = 'VOL' THEN 'Volume' WHEN h.recordType = 'INS' THEN 'Insert' ELSE NULL END AS type, " +
				"h.dateAndTime AS activityDate, CASE WHEN h.action = 'CREATE' THEN 'Created' WHEN h.action = 'EDIT' THEN 'Edit' WHEN h.action = 'VIEW' THEN 'Viewed' " +
				"WHEN h.action = 'DELETE' THEN 'Deleted' WHEN h.action = 'UNDELETE' THEN 'Restored' ELSE NULL END AS action, " + 
				"CASE WHEN h.recordType = 'DE' THEN d.deTitle WHEN h.recordType = 'AE' THEN u.aeName ELSE NULL END AS title, " + 
				"p.MAPNameLF AS personName, SUBSTRING(pl.PLACENAMEFULL, 1, 50) AS placeName, h.account, " + 
				"CASE WHEN h.recordType = 'AE' THEN uc.collectionAbbreviation WHEN h.recordType = 'VOL' THEN vc.collectionAbbreviation WHEN h.recordType = 'INS' THEN ic.collectionAbbreviation ELSE NULL END AS collectionAbbreviation, " + 
				"CASE WHEN h.recordType = 'AE' THEN uv.volume WHEN h.recordType = 'VOL' THEN v.volume	WHEN recordType = 'INS' THEN iv.volume ELSE NULL END AS volumeNo, " + 
				"CASE WHEN h.recordType = 'AE' THEN ui.insertN WHEN h.recordType = 'INS' THEN i.insertN ELSE NULL END AS insertNo " + 
				"FROM tblLastAccessedRecords h " + 
				"LEFT OUTER JOIN tblDocumentEnts d ON h.recordType = 'DE' AND h.entryId = d.documentEntityId " + 
				"LEFT OUTER JOIN tblUploads u ON h.recordType = 'AE' AND h.entryId = u.id " + 
				"LEFT OUTER JOIN tblCollections uc ON h.recordType = 'AE' AND u.collection = uc.id " + 
				"LEFT OUTER JOIN tblVolumes uv ON h.recordType = 'AE' AND u.volume = uv.SUMMARYID " + 
				"LEFT OUTER JOIN tblInserts ui ON h.recordType = 'AE' AND u.insertN = ui.id " + 
				"LEFT OUTER JOIN tblPeople p ON h.recordType = 'BIO' AND h.entryId = p.PERSONID " + 
				"LEFT OUTER JOIN tblPlaces pl ON h.recordType = 'GEO' AND h.entryId = pl.PLACEALLID " + 
				"LEFT OUTER JOIN tblVolumes v ON h.recordType = 'VOL' AND h.entryId = v.SUMMARYID " + 
				"LEFT OUTER JOIN tblCollections vc ON h.recordType = 'VOL' AND v.collection = vc.id " + 
				"LEFT OUTER JOIN tblInserts i ON h.recordType = 'INS' AND h.entryId = i.id " + 
				"LEFT OUTER JOIN tblVolumes iv ON h.recordType = 'INS' AND i.volume = iv.SUMMARYID " + 
				"LEFT OUTER JOIN tblCollections ic ON h.recordType = 'INS' AND iv.collection = ic.id " + 
				"WHERE h.account = '" + account + "'";
		if (!StringUtils.isNullableString(type)) {
			queryStr += " AND h.recordType = '" + type + "'";
		}
		if (!orderColumn.equalsIgnoreCase("archivalLocation")) {
			queryStr += " ORDER BY " + orderColumn + " " + ascOrDesc;
		}
		
		Query query = getEntityManager().createNativeQuery(queryStr);
		if (!orderColumn.equalsIgnoreCase("archivalLocation")) {
			query.setFirstResult(firstResult).setMaxResults(maxResult);
		}
		
		return (List<Object[]>) query.getResultList();
	}

	@Override
	public Long getHistoryItemsCount(String account, String type) {
		if (account == null) {
			return null;
		}
		String historyItemsCountQuery = "SELECT COUNT(e.idLastAccessedItems) FROM HistoryLogEntity e "
				+ "WHERE e.user.account = :account";
		if (type != null) {
			historyItemsCountQuery += " AND e.recordType = :recordType";
		}
		Query query = getEntityManager().createQuery(historyItemsCountQuery);
		query.setParameter("account", account);
		if (type != null) {
			query.setParameter("recordType", RecordType.valueOf(type));
		}
		return (Long) query.getSingleResult();
	}
	
	@Override
	public Integer deleteHistory(String account) throws PersistenceException {
		String jpql = "DELETE FROM HistoryLogEntity e WHERE e.user.account = :account";

        Query query = getEntityManager().createQuery(jpql);
        query.setParameter("account", account);
        
        return query.executeUpdate();
	}
}
