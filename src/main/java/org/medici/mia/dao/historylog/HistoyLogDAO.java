package org.medici.mia.dao.historylog;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.dao.Dao;
import org.medici.mia.domain.HistoryLogEntity;
import org.medici.mia.domain.HistoryLogEntity.RecordType;

public interface HistoyLogDAO extends Dao<Integer, HistoryLogEntity> {

	public Integer registerLoaAction(HistoryLogEntity entity)
			throws PersistenceException;

	public HistoryLogEntity getLogAction(String account,
			RecordType recordType) throws PersistenceException;

	public List<HistoryLogEntity> getActionsHistory(Integer page, Integer perPage)
			throws PersistenceException;

	public List<HistoryLogEntity> getActionsHistory(Integer entryId, RecordType recordType, Integer page, Integer perPage)
			throws PersistenceException;

	public Long getActionsHistoryCount() throws PersistenceException;

	public Long getActionsHistoryCount(Integer entryId, RecordType recordType) throws PersistenceException;

	public List<Object[]> getHistoryItems(String account, String type, Integer firstResult, Integer maxResult,
			String orderColumn, String ascOrDesc) throws PersistenceException;

	public Long getHistoryItemsCount(String account, String type);

	Integer deleteHistory(String account) throws PersistenceException;

}
