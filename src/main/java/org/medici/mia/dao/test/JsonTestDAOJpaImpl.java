/*
 * JsonTestDAOImpl.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.dao.test;

import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;
import org.medici.mia.controller.test.JsonTest;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.CollectionEntity;
import org.medici.mia.domain.GoogleLocation;
import org.medici.mia.domain.JsonTestEntity;
import org.medici.mia.domain.RepositoryEntity;
import org.medici.mia.domain.SeriesEntity;
import org.medici.mia.exception.ApplicationThrowable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Repository
public class JsonTestDAOJpaImpl extends JpaDao<Integer, JsonTestEntity> implements JsonTestDAO{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final Logger logger = Logger.getLogger(this.getClass());
	
	
	@Override
	public void save (JsonTest jsonTest) throws PersistenceException{
		logger.info("SUCCESS: Inside JsonTestDAO to Persist data - Json Values on DAO are" + jsonTest.getId() + " "+ jsonTest.getName());

		JsonTestEntity jsonTestEntity = new JsonTestEntity();
		jsonTestEntity.setIdVal(jsonTest.getId());
		jsonTestEntity.setNameVal(jsonTest.getName());

		getEntityManager().persist(jsonTestEntity);
		
		logger.info("SUCCESS: Insert into DB is DONE!!!");
	}

	
	@Override
	public void delete (JsonTest jsonTest) throws PersistenceException{
		logger.info("SUCCESS: Inside JsonTestDAO to Delete data- Json Values on DAO are" + jsonTest.getId() + " "+ jsonTest.getName());

		JsonTestEntity jsonTestEntity = getEntityManager().find(JsonTestEntity.class, jsonTest.getId());
		getEntityManager().remove(jsonTestEntity);
		
		logger.info("SUCCESS: Delete from DB is DONE!!!");
	}
	
	@Override
	public void update (JsonTest jsonTest) throws PersistenceException{
		logger.info("SUCCESS: Inside JsonTestDAO to Update data- Json Values on DAO are" + jsonTest.getId() + " "+ jsonTest.getName());

		JsonTestEntity jsonTestEntity = getEntityManager().find(JsonTestEntity.class, jsonTest.getId());
		jsonTestEntity.setNameVal(jsonTest.getName());
		getEntityManager().merge(jsonTestEntity);
		
		logger.info("SUCCESS: Update from DB is DONE!!!");
		logger.info("Data Updated are: " + jsonTestEntity.getIdVal() + " " + jsonTestEntity.getNameVal());
	}
	
	
	@Override
	public void retrieve (JsonTest jsonTest) throws PersistenceException{
		logger.info("SUCCESS: Inside JsonTestDAO to Retrieve data- Json Values on DAO are" + jsonTest.getId() + " "+ jsonTest.getName());

		JsonTestEntity jsonTestEntity = getEntityManager().find(JsonTestEntity.class, jsonTest.getId());
		
		logger.info("SUCCESS: Retrieve from DB is DONE!!!");
		logger.info("Data retrieved are: " + jsonTestEntity.getIdVal() + " " + jsonTestEntity.getNameVal());
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public Integer insertRepository(RepositoryEntity repEntity)
			throws ApplicationThrowable {
		try {
			getEntityManager().persist(repEntity);
			return repEntity.getRepositoryId();
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public Integer insertCollection(CollectionEntity collEntity)
			throws ApplicationThrowable {
		try {
			getEntityManager().persist(collEntity);
			return collEntity.getCollectionId();
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public Integer insertSeries(SeriesEntity serEntity)
			throws ApplicationThrowable {
		try {
			getEntityManager().persist(serEntity);
			return serEntity.getSeriesId();
		} catch (Throwable th) {
			throw new ApplicationThrowable(th);
		}
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public GoogleLocation getGoogleLocation(String gPlaceId){
		return getEntityManager().find(GoogleLocation.class, gPlaceId);
		
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public String insertGoogleLocation(GoogleLocation glEntity){
		getEntityManager().persist(glEntity);
		return glEntity.getgPlaceId();
		
	}
	
	
}
