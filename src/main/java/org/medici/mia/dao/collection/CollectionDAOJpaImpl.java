package org.medici.mia.dao.collection;

import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.CollectionEntity;
import org.springframework.stereotype.Repository;

@Repository
public class CollectionDAOJpaImpl extends JpaDao<Integer, CollectionEntity>
		implements CollectionDAO {

	private static final long serialVersionUID = -5796533789041210555L;

	
	@Override
	public List<CollectionEntity> findCollectionByName(String collectionName, Integer repositoryId)
			throws PersistenceException {	
	     
	     Query query = getEntityManager().createQuery(
	    		 "FROM CollectionEntity c "
	    	   + "WHERE LOWER(c.collectionName) LIKE LOWER(:collectionName) AND "
	    	   + "c.repository = :repository");
	     
	     query.setParameter("collectionName","%"+collectionName+"%");
	     query.setParameter("repository",repositoryId);
	     
	     return query.getResultList();
	}
	
	@Override
	public List<CollectionEntity> findAllCollections(Integer repositoryId)
			throws PersistenceException {	
	     
	     Query query = getEntityManager().createQuery("FROM CollectionEntity c WHERE c.repository = :repository");	    
	     query.setParameter("repository",repositoryId);
	     
	     return query.getResultList();
	}
	
	@Override
	public List<CollectionEntity> checkCollectionByName(String collectionName, Integer repositoryId)
			throws PersistenceException {	
	     
	     Query query = getEntityManager().createQuery("FROM CollectionEntity c WHERE c.collectionName LIKE :collectionName and c.repository = :repository");
	     query.setParameter("collectionName",collectionName);
	     query.setParameter("repository",repositoryId);
	     
	     return query.getResultList();
	}
	
	@Override
	public CollectionEntity findAbbrevById(Integer id)
			throws PersistenceException {
		CollectionEntity collEntity = getEntityManager().find(CollectionEntity.class, id);
		return collEntity;
	}
	
	@Override
	public Integer insertCollection(CollectionEntity collEntity) throws PersistenceException {
		getEntityManager().persist(collEntity);
        
         return collEntity.getCollectionId(); 
    }

	@Override
	public Integer findNumberOfCollectionAbbreviation(String collectionAbbrev,
			Integer repositoryId) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"FROM CollectionEntity c WHERE c.collectionAbbreviation LIKE :collectionAbbreviation and c.repository = :repository");
		query.setParameter("collectionAbbreviation", collectionAbbrev + "%");
		query.setParameter("repository", repositoryId);

		return query.getResultList().size();
	}
	
}
