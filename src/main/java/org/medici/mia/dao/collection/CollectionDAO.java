package org.medici.mia.dao.collection;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.dao.Dao;
import org.medici.mia.domain.CollectionEntity;

public interface CollectionDAO extends Dao<Integer, CollectionEntity> {

	public List<CollectionEntity> findCollectionByName(String collectionName,
			Integer repositoryId) throws PersistenceException;
	
	public List<CollectionEntity> findAllCollections(
			Integer repositoryId) throws PersistenceException;

	public List<CollectionEntity> checkCollectionByName(String collectionName,
			Integer repositoryId) throws PersistenceException;

	public Integer insertCollection(CollectionEntity collEntity)
			throws PersistenceException;

	public CollectionEntity findAbbrevById(Integer id)
			throws PersistenceException;

	public Integer findNumberOfCollectionAbbreviation(String collectionAbbrev,
			Integer repositoryId) throws PersistenceException;
}
