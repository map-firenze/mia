
/*
 * InsertDAOJpaImpl.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.dao.insert;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.medici.mia.common.json.InsertGuardiaJson;
import org.medici.mia.common.util.FileUtils;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.InsertEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.UploadInfoEntity;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author User
 *
 */
@Repository
public class InsertDAOJpaImpl extends JpaDao<Integer, InsertEntity> implements
		InsertDAO {


	private final Logger logger = Logger.getLogger(this.getClass());
	
	private static final long serialVersionUID = -8410473648403133440L;

	@Override
	public List<InsertEntity> findInsertByName(String insertName,
			Integer volumeId) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"FROM InsertEntity i WHERE i.insertName LIKE :insert and i.volume = :volume and (i.logicalDelete IS NULL OR i.logicalDelete = 0)");
		query.setParameter("insert", "%" + insertName + "%");
		query.setParameter("volume", volumeId);

		return query.getResultList();
	}

	// Get Guardia by insert
	@Override
	public InsertGuardiaJson findGuardiaByInsert(Integer insertId)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT u FROM UploadInfoEntity u WHERE u.insertId =:insertId AND u.logicalDelete = 0 AND u.optionalImage = 'guardia' ");
		query.setParameter("insertId", insertId);
		UploadInfoEntity upload = null;
		// TODO: It is necessqry to:
		// Option 1: catch an exception in the case we use getSingleResult
		// function.
		// Option 2: Call getResultList instead of getSingelResult
		try {
			upload = (UploadInfoEntity) query.getSingleResult();
		} catch (NoResultException e) {
			logger.info("no record found for spine with insert " +insertId );
			return null;
		}
		InsertGuardiaJson guardiaJson = new InsertGuardiaJson();
		List<UploadFileEntity> uploadFile = upload.getUploadFileEntities();
		for (int i = 0; i < uploadFile.size(); i++) {
			if (uploadFile.get(i).getLogicalDelete() != 0) {
				uploadFile.remove(i);
			}
		}
		// Issue 618 If the inserEntity has no value, insertName is null
		String insName = null;
		if (uploadFile.get(0)
				.getUploadInfoEntity().getInsertEntity() != null) {
			insName = uploadFile.get(0)
					.getUploadInfoEntity().getInsertEntity()
					.getInsertName();
		}
		String realPath = org.medici.mia.common.util.FileUtils
				.getRealPathImagePreview(uploadFile.get(0)
						.getUploadInfoEntity().getRepositoryEntity()
						.getLocation(), uploadFile.get(0).getUploadInfoEntity()
						.getRepositoryEntity().getRepositoryAbbreviation(),
						uploadFile.get(0).getUploadInfoEntity()
								.getCollectionEntity()
								.getCollectionAbbreviation(), uploadFile.get(0)
								.getUploadInfoEntity().getVolumeEntity()
								.getVolume(), uploadFile.get(0)
								.getUploadInfoEntity().getInsertEntity()
								.getInsertName());
		guardiaJson.setFileName(uploadFile.get(0).getFilename());
		guardiaJson.setFilePath(realPath + FileUtils.THUMB_FILES_DIR
				+ FileUtils.OS_SLASH);
		return guardiaJson;
	}

	@Override
	public InsertEntity findInsertById(Integer insertId)
			throws PersistenceException {

		Query query = getEntityManager().createQuery(
				"FROM InsertEntity i WHERE i.id = :insertId ");
		query.setParameter("insertId", insertId);

		List<InsertEntity> res = query.getResultList();
		if (res.size() > 0) {
			return (InsertEntity) res.get(0);
		}
		return null;
	}

	@Override
	public List<InsertEntity> findAllInserts(Integer volumeId)
			throws PersistenceException {

		Query query = getEntityManager().createQuery(
				"FROM InsertEntity i WHERE i.volume = :volume");
		query.setParameter("volume", volumeId);

		return query.getResultList();
	}

	@Override
	public List<InsertEntity> findMatchInsertByName(String insertName,
			Integer volumeId) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"FROM InsertEntity i WHERE i.insertName LIKE :insert and i.volume = :volume");
		query.setParameter("insert", insertName);
		query.setParameter("volume", volumeId);

		return query.getResultList();
	}

	@Override
	public Integer insertInsert(InsertEntity insEntity)
			throws PersistenceException {
		getEntityManager().persist(insEntity);

		return insEntity.getInsertId();
	}

	@Override
	public InsertEntity mergeInsert(InsertEntity insEntity)
			throws PersistenceException {
		if (insEntity == null)
			return null;

		InsertEntity ins = getEntityManager().find(InsertEntity.class,
				insEntity.getInsertId());
		if (ins != null) {
			ins.setInsertName(insEntity.getInsertName());
			ins.setVolume(insEntity.getVolume());
			getEntityManager().merge(ins);
		}

		return ins;

	}

}