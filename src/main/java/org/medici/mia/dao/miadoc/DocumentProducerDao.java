package org.medici.mia.dao.miadoc;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.common.json.document.ModifyPeopleForDocJson;
import org.medici.mia.dao.Dao;
import org.medici.mia.domain.DocumentProducerJoinEntity;
import org.medici.mia.domain.People;

public interface DocumentProducerDao extends
		Dao<Integer, DocumentProducerJoinEntity> {

	public void insertDocProducers(
			List<DocumentProducerJoinEntity> documentProducerEntities)
			throws PersistenceException;

	public void insertDocProducer(
			DocumentProducerJoinEntity documentProducerEntity)
			throws PersistenceException;

	public DocumentProducerJoinEntity findDocProducer(Integer documentEntityId,
			Integer producer) throws PersistenceException;

	public void modifyProducers(List<People> producers, Integer documentEntityId)
			throws PersistenceException;

	public void modifyProducerForDoc(ModifyPeopleForDocJson modifyPeople)
			throws PersistenceException;

	void insertDocProducerNative(
			DocumentProducerJoinEntity documentProducerEntity)
			throws PersistenceException;
}