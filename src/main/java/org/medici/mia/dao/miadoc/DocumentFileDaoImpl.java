package org.medici.mia.dao.miadoc;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.medici.mia.common.json.document.ModifyImageJson;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.DocumentFileJoinEntity;
import org.springframework.stereotype.Repository;

@Repository
public class DocumentFileDaoImpl extends
		JpaDao<Integer, DocumentFileJoinEntity> implements DocumentFileDao {

	private static final long serialVersionUID = 1L;

	@Override
	public void insertDocFile(List<DocumentFileJoinEntity> documentFileEntities)
			throws PersistenceException {
		if (documentFileEntities != null && !documentFileEntities.isEmpty()) {

			for (DocumentFileJoinEntity documentFileJoinEntity : documentFileEntities) {
				getEntityManager().persist(documentFileJoinEntity);
			}

		}

	}

	@Override
	public void insertDocFile(DocumentFileJoinEntity documentFileEntity)
			throws PersistenceException {
		if (documentFileEntity != null) {
			getEntityManager().persist(documentFileEntity);
		}

	}

	@Override
	public void modifyDocFiles(
			List<DocumentFileJoinEntity> documentFileEntities,
			Integer documentEntityId) throws PersistenceException {

		// Delete all the relation between this document id with the uploadfiles
		deleteDocuFilesByDocId(documentEntityId);
		// Insert new relation between document and uploadFiles after delete
		if (documentFileEntities != null && !documentFileEntities.isEmpty()) {
			insertDocFile(documentFileEntities);
		}
	}

	@Override
	public Integer modifyDocFiles(ModifyImageJson json)
			throws PersistenceException {

		if (json.getModifyImagesInDe() == null
				|| json.getModifyImagesInDe().isEmpty()) {
			return 0;
		}

		List<DocumentFileJoinEntity> fileJoinEnts = new ArrayList<DocumentFileJoinEntity>();

		for (Integer fileId : json.getModifyImagesInDe()) {
			DocumentFileJoinEntity ent = new DocumentFileJoinEntity();
			ent.setDocumentId(json.getDocumentId());
			ent.setFileId(fileId);
			fileJoinEnts.add(ent);
		}

		if (fileJoinEnts != null && !fileJoinEnts.isEmpty()) {

			Integer documentEntityId = json.getDocumentId();
			// Delete all the relation between this document id with the
			// uploadfiles
			Query query = getEntityManager()
					.createQuery(
							"SELECT u FROM DocumentFileJoinEntity u WHERE u.documentId =:documentId");
			query.setParameter("documentId", documentEntityId);

			List<DocumentFileJoinEntity> list = (List<DocumentFileJoinEntity>) query
					.getResultList();
			if (list != null && !list.isEmpty()) {
				for (DocumentFileJoinEntity docfile : list) {
					getEntityManager().remove(docfile);
				}
			}
			// Insert new relation between document and uploadFiles after delete
			insertDocFile(fileJoinEnts);
			return fileJoinEnts.size();
		} else {
			return 0;
		}

	}

	private void deleteDocuFilesByDocId(Integer documentEntityId)
			throws PersistenceException {

		// Delete all the relation between this document id with the uploadFiles

		String jpql = "DELETE FROM DocumentFileJoinEntity u WHERE u.documentId=:documentEntityId";

		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("documentEntityId", documentEntityId);
		query.executeUpdate();
		getEntityManager().flush();
	}

	public List<DocumentFileJoinEntity> findDocumentsByFileId(Integer fileId)
			throws PersistenceException {

		// Delete all the relation between this document id with the uploadFiles

		Query query = getEntityManager()
				.createQuery(
						"SELECT u FROM DocumentFileJoinEntity u WHERE u.fileId =:fileId");
		query.setParameter("fileId", fileId);

		return (List<DocumentFileJoinEntity>) query.getResultList();

	}

	@Override
	public Integer insertFiles(ModifyImageJson json)
			throws PersistenceException {

		if (json == null || json.getDocumentId() == null
				|| json.getModifyImagesInDe() == null
				|| json.getModifyImagesInDe().isEmpty()) {
			return 0;
		}

		for (Integer fileId : json.getModifyImagesInDe()) {
			DocumentFileJoinEntity docFile = new DocumentFileJoinEntity();

			docFile.setDocumentId(json.getDocumentId());
			docFile.setFileId(fileId);
			DocumentFileJoinEntity ent = getEntityManager().find(
					DocumentFileJoinEntity.class, docFile);
			if (ent == null) {
				getEntityManager().persist(docFile);
			}
		}

		return json.getModifyImagesInDe().size();

	}
}