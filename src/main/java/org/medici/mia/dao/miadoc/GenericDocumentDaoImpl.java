package org.medici.mia.dao.miadoc;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.medici.mia.common.json.complexsearch.DEComplexSearchJson;
import org.medici.mia.common.json.document.IModifyDocJson;
import org.medici.mia.common.json.document.ModifyPlaceForDocJson;
import org.medici.mia.common.json.search.AllCountWordSearchJson;
import org.medici.mia.common.json.search.DEWordSearchJson;
import org.medici.mia.controller.genericsearch.DESearchPaginationParam;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.CorrespondenceEntity;
import org.medici.mia.domain.FinancialEntity;
import org.medici.mia.domain.FiscalEntity;
import org.medici.mia.domain.IGenericEntity;
import org.medici.mia.domain.InventoryEntity;
import org.medici.mia.domain.LiteraryEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.MiscellaneousEntity;
import org.medici.mia.domain.NewsEntity;
import org.medici.mia.domain.NotarialEntity;
import org.medici.mia.domain.OfficialEntity;
import org.medici.mia.domain.User;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.genericsearch.ComplexSearchUtils;
import org.medici.mia.service.genericsearch.SearchUtils;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Repository
public class GenericDocumentDaoImpl extends JpaDao<Integer, MiaDocumentEntity>
		implements GenericDocumentDao {

	private static final long serialVersionUID = 1L;
	private final Logger logger = Logger.getLogger(this.getClass());

	@Override
	public MiaDocumentEntity findDocumentById(Integer documentId)
			throws PersistenceException {
		MiaDocumentEntity docEntity = null;
		try {
			docEntity = getEntityManager().find(MiaDocumentEntity.class,
					documentId);
		} catch (Exception e) {
			logger.info("No document found in DB");
		}
		return docEntity;
	}

	@Override
	public List<MiaDocumentEntity> findDocuments() throws PersistenceException {

		Query query = getEntityManager().createQuery(
				"SELECT u FROM MiaDocumentEntity u");
		return (List<MiaDocumentEntity>) query.getResultList();
	}

	@Override
	public List<MiaDocumentEntity> findDocuments(List<Integer> docList,
			Integer firstResult, Integer maxResult, String orderColumn,
			String ascOrDesc) throws PersistenceException {

		if (docList == null || docList.isEmpty()) {
			return null;
		}

		if (orderColumn == null || orderColumn.isEmpty()) {
			orderColumn = "documentEntityId";
		}

		if (firstResult == null || firstResult.equals("")) {
			firstResult = 0;
		}

		if (maxResult == null || maxResult.equals("")) {
			maxResult = 20;
		}

		StringBuffer dList = new StringBuffer("(");
		int count = 0;
		
		if (docList.size() == 1) {
			dList.append(docList.get(0));
			count = 1;
			dList.append(")");
		}
		else {
			for (int i = 0; i < docList.size() - 1; i++) {
				dList.append(docList.get(i));
				dList.append(",");
				count++;
			}
			
			dList.append(docList.get(count) + ")");
		}
		
		String queryStr = null;

		if (orderColumn.equalsIgnoreCase("producers")) {
			queryStr = "SELECT * FROM tblDocumentEnts d"
					+ " LEFT JOIN tblDocumentsEntsPeople dp ON (dp.documentEntityId = d.documentEntityId)"
					+ " LEFT JOIN tblPeople p ON (dp.producer = p.personId)"
					+ " where d.documentEntityId IN " + dList
					+ " order by p.MAPNameLF " + ascOrDesc;
		} else if (orderColumn.equalsIgnoreCase("archivalLocation")) {
			queryStr = "SELECT * FROM tblDocumentEnts u where u.documentEntityId IN "
					+ dList;
		} else if (orderColumn.equalsIgnoreCase("docYear")) {
			queryStr = createQueryOrderByDate(dList.toString(), ascOrDesc);
		}
		else {
			queryStr = "SELECT * FROM tblDocumentEnts u where u.documentEntityId IN "
					+ dList + " order by " + orderColumn + " " + ascOrDesc;
			;
		}

		Query query = getEntityManager().createNativeQuery(queryStr,
				MiaDocumentEntity.class);
		if (!orderColumn.equalsIgnoreCase("archivalLocation")) {
			query.setFirstResult(firstResult).setMaxResults(maxResult);
		}
		return (List<MiaDocumentEntity>) query.getResultList();
	}

	public List<MiaDocumentEntity> findDocuments(User user, Integer page,
			Integer perPage) throws PersistenceException {
		Query query = getEntityManager().createQuery(
				"SELECT u FROM MiaDocumentEntity u where u.createdBy = :user");
		int limit, offset;
		limit = perPage;
		offset = (page * perPage) - perPage;
		if (limit > 0 && offset >= 0) {
			query.setFirstResult(offset);
			query.setMaxResults(limit);
		}
		query.setParameter("user", user.getAccount());
		return (List<MiaDocumentEntity>) query.getResultList();
	}

	@Override
	public List<MiaDocumentEntity> findUserOwnDocuments(User user,
			Integer page, Integer perPage) throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT u "
								+ "FROM MiaDocumentEntity u "
								+ "WHERE u.createdBy = :user AND u.flgLogicalDelete != 1 "
								+ "ORDER BY u.dateCreated DESC");
		int limit, offset;
		limit = perPage;
		offset = (page * perPage) - perPage;
		if (limit > 0 && offset >= 0) {
			query.setFirstResult(offset);
			query.setMaxResults(limit);
		}
		query.setParameter("user", user.getAccount());
		return (List<MiaDocumentEntity>) query.getResultList();
	}

	@Override
	public Long countUserOwnDocuments(User user) throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT COUNT(u.documentEntityId) "
								+ "FROM MiaDocumentEntity u "
								+ "WHERE u.createdBy = :user AND u.flgLogicalDelete != 1");
		query.setParameter("user", user.getAccount());
		return (Long) query.getSingleResult();
	}

	@Override
	public List<MiaDocumentEntity> findNotDeletedDocuments()
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT u FROM MiaDocumentEntity u where u.flgLogicalDelete != 1");
		// query.setParameter("flgLogicalDelete", Boolean.FALSE);
		return (List<MiaDocumentEntity>) query.getResultList();
	}

	@Override
	public List<MiaDocumentEntity> findDocumentsBiaWithOutImage()
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT u FROM MiaDocumentEntity u where u.flgLogicalDelete != 1");
		// query.setParameter("flgLogicalDelete", Boolean.FALSE);
		return (List<MiaDocumentEntity>) query.getResultList();
	}

	@Override
	public Integer updateDocument(Integer docId, Integer privacy)
			throws PersistenceException {
		String hql = "UPDATE MiaDocumentEntity SET privacy=:privacy WHERE documentEntityId =:docId";

		Query query = getEntityManager().createQuery(hql);
		query.setParameter("privacy", privacy);
		query.setParameter("docId", docId);

		return query.executeUpdate();
	}

	// Find Document Related To Volume
	@SuppressWarnings("unchecked")
	@Override
	public List<MiaDocumentEntity> findDocumentRelatedToVolume(Integer volumeId)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT DISTINCT m FROM MiaDocumentEntity m"
								+ "	WHERE m.documentEntityId != null AND m.documentEntityId IN (SELECT j.documentId FROM DocumentFileJoinEntity j WHERE j.fileId != null AND j.fileId IN"
								+ " (SELECT f.uploadFileId FROM UploadFileEntity f WHERE f.idTblUpload != null AND f.idTblUpload IN"
								+ " (SELECT u.uploadInfoId FROM UploadInfoEntity u WHERE u.volume =:volumeId AND u.flgLogicalDelete != 1)))");
		query.setParameter("volumeId", volumeId);
		return (List<MiaDocumentEntity>) query.getResultList();
	}

	// Find Document Related To Insert
	@SuppressWarnings("unchecked")
	@Override
	public List<MiaDocumentEntity> findDocumentRelatedToInsert(Integer insertId)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT DISTINCT m FROM MiaDocumentEntity m"
								+ "	WHERE m.documentEntityId != null AND m.documentEntityId IN (SELECT j.documentId FROM DocumentFileJoinEntity j WHERE j.fileId != null AND j.fileId IN"
								+ " (SELECT f.uploadFileId FROM UploadFileEntity f WHERE f.idTblUpload != null AND f.idTblUpload IN"
								+ " (SELECT u.uploadInfoId FROM UploadInfoEntity u WHERE u.insertId =:insertId AND u.flgLogicalDelete != 1)))");
		query.setParameter("insertId", insertId);
		return (List<MiaDocumentEntity>) query.getResultList();
	}

	@Override
	public List<MiaDocumentEntity> findDocumentsByUser(String userName)
			throws PersistenceException {

		Query query = getEntityManager().createQuery(
				"FROM MiaDocumentEntity u WHERE u.createdBy=:userName");
		query.setParameter("userName", userName);
		return (List<MiaDocumentEntity>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MiaDocumentEntity> findDocumentByVolumeId(Integer volumeId)
			throws PersistenceException {

		Query query = getEntityManager().createQuery(
				"FROM MiaDocumentEntity u JOIN WHERE u.createdBy=:userName");
		query.setParameter("userName", volumeId);
		return (List<MiaDocumentEntity>) query.getResultList();
	}

	@Override
	public Boolean findDocLogicalDelete(Integer documentEntityId)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT u.flgLogicalDelete FROM MiaDocumentEntity u WHERE u.documentEntityId=:documentEntityId");
		query.setParameter("documentEntityId", documentEntityId);
		Boolean isLogicalDelete = (Boolean) query.getSingleResult();
		if (isLogicalDelete == null || !isLogicalDelete) {
			return false;

		}
		return true;
	}

	@Override
	public Integer insertDE(MiaDocumentEntity docEnt)
			throws PersistenceException {
		if (docEnt == null) {
			logger.error("Impossible creation of Document null.");
			return 0;
		}

		if (docEnt.getCorrespondence() != null) {
			CorrespondenceEntity cor = docEnt.getCorrespondence();
			docEnt.setCorrespondence(null);
			try {
				getEntityManager().persist(docEnt);
			} catch (HibernateException e) {
				e.printStackTrace();
			}
			cor.setDocumentEntityId(docEnt.getDocumentEntityId());
			docEnt.setCorrespondence(cor);

		} else if (docEnt.getNotarial() != null) {
			NotarialEntity not = docEnt.getNotarial();
			docEnt.setNotarial(null);
			getEntityManager().persist(docEnt);
			not.setDocumentEntityId(docEnt.getDocumentEntityId());
			docEnt.setNotarial(not);

		} else if (docEnt.getOfficial() != null) {
			OfficialEntity off = docEnt.getOfficial();
			docEnt.setOfficial(null);
			getEntityManager().persist(docEnt);
			off.setDocumentEntityId(docEnt.getDocumentEntityId());
			docEnt.setOfficial(off);

		} else if (docEnt.getInventory() != null) {
			InventoryEntity inv = docEnt.getInventory();
			docEnt.setInventory(null);
			getEntityManager().persist(docEnt);
			inv.setDocumentEntityId(docEnt.getDocumentEntityId());
			docEnt.setInventory(inv);

		} else if (docEnt.getFiscal() != null) {
			FiscalEntity fisc = docEnt.getFiscal();
			docEnt.setFiscal(null);
			getEntityManager().persist(docEnt);
			fisc.setDocumentEntityId(docEnt.getDocumentEntityId());
			docEnt.setFiscal(fisc);

		} else if (docEnt.getFinancial() != null) {
			FinancialEntity fin = docEnt.getFinancial();
			docEnt.setFinancial(null);

			getEntityManager().persist(docEnt);
			fin.setDocumentEntityId(docEnt.getDocumentEntityId());
			docEnt.setFinancial(fin);

		} else if (docEnt.getMiscellaneous() != null) {
			MiscellaneousEntity misc = docEnt.getMiscellaneous();
			docEnt.setMiscellaneous(null);
			getEntityManager().persist(docEnt);
			misc.setDocumentEntityId(docEnt.getDocumentEntityId());
			docEnt.setMiscellaneous(misc);

		} else if (docEnt.getLiterary() != null) {
			LiteraryEntity lit = docEnt.getLiterary();
			docEnt.setLiterary(null);
			getEntityManager().persist(docEnt);
			lit.setDocumentEntityId(docEnt.getDocumentEntityId());
			docEnt.setLiterary(lit);

		} else if (docEnt.getNews() != null) {
			NewsEntity news = docEnt.getNews();
			docEnt.setNews(null);
			getEntityManager().persist(docEnt);
			news.setDocumentEntityId(docEnt.getDocumentEntityId());
			docEnt.setNews(news);

		} else {
			logger.error("Impossible creation of Document with NO subtype");
			throw new ApplicationThrowable();
		}

		getEntityManager().merge(docEnt);
		return docEnt.getDocumentEntityId();
	}

	@Override
	public Integer updateDE(MiaDocumentEntity docEnt, String username,
			Date lastUpdate) throws PersistenceException {
		if (docEnt == null) {
			logger.error("Impossible creation of Document null.");
			return null;
		}

		docEnt.setLastUpdateBy(username);
		docEnt.setDateLastUpdate(lastUpdate);
		if (docEnt.getCategory().equalsIgnoreCase("correspondence")) {
			getEntityManager().merge(docEnt);
		} else if (docEnt.getCorrespondence() != null) {
			docEnt.getCorrespondence().setDocumentEntityId(
					docEnt.getDocumentEntityId());
			getEntityManager().merge(docEnt);
		} else if (docEnt.getNotarial() != null) {
			docEnt.getNotarial().setDocumentEntityId(
					docEnt.getDocumentEntityId());
			getEntityManager().merge(docEnt);
		} else if (docEnt.getOfficial() != null) {
			docEnt.getOfficial().setDocumentEntityId(
					docEnt.getDocumentEntityId());
			getEntityManager().merge(docEnt);
		} else if (docEnt.getInventory() != null) {
			docEnt.getInventory().setDocumentEntityId(
					docEnt.getDocumentEntityId());
			getEntityManager().merge(docEnt);
		} else if (docEnt.getFiscal() != null) {
			docEnt.getFiscal()
					.setDocumentEntityId(docEnt.getDocumentEntityId());
			getEntityManager().merge(docEnt);
		} else if (docEnt.getFinancial() != null) {
			docEnt.getFinancial().setDocumentEntityId(
					docEnt.getDocumentEntityId());
			getEntityManager().merge(docEnt);
		} else if (docEnt.getLiterary() != null) {
			docEnt.getLiterary().setDocumentEntityId(
					docEnt.getDocumentEntityId());
			getEntityManager().merge(docEnt);
		} else if (docEnt.getNews() != null) {
			docEnt.getNews().setDocumentEntityId(docEnt.getDocumentEntityId());
			getEntityManager().merge(docEnt);
		} else if (docEnt.getMiscellaneous() != null) {
			docEnt.getMiscellaneous().setDocumentEntityId(
					docEnt.getDocumentEntityId());
			getEntityManager().merge(docEnt);
		} else {
			logger.error("Impossible creation of Document with NO subtype");
			throw new ApplicationThrowable();
		}

		return docEnt.getDocumentEntityId();
	}
	
	@Override
	public Integer countDocumentsByUploadFileId(Integer uploadFileId) throws PersistenceException {
		
		Query query = getEntityManager().createQuery(
				"SELECT COUNT(d) FROM DocumentFileJoinEntity df, MiaDocumentEntity d WHERE df.documentId = d.documentEntityId and df.fileId=:uploadFileId and d.flgLogicalDelete != 1");
		
		query.setParameter("uploadFileId", uploadFileId);

		return ((Long) query.getSingleResult()).intValue();
	}

	@Override
	public List<Integer> findDocsIdByUploadFileId(Integer uploadFileId)
			throws PersistenceException {

		// Query query = getEntityManager()
		// .createQuery(
		// "FROM DocumentFileJoinEntity d WHERE d.fileId=:uploadFileId and d.flgLogicalDelete=false");
		Query query = getEntityManager()
				.createQuery(
						"SELECT d FROM DocumentFileJoinEntity df, MiaDocumentEntity d WHERE df.documentId = d.documentEntityId and df.fileId=:uploadFileId and d.flgLogicalDelete != 1");

		query.setParameter("uploadFileId", uploadFileId);
		List<MiaDocumentEntity> list = (List<MiaDocumentEntity>) query
				.getResultList();
		if (list == null || list.isEmpty()) {
			return null;
		}

		List<Integer> docsList = new ArrayList<Integer>();
		for (MiaDocumentEntity row : list) {
			if (row.getFlgLogicalDelete() == null || !row.getFlgLogicalDelete()) {
				docsList.add(row.getDocumentEntityId());
			}
		}

		return docsList;
	}

	public void saveSynopsis(String generalNotesSynopsis,
			MiaDocumentEntity documentEntity) throws PersistenceException {
		if (documentEntity != null) {
			documentEntity.setGeneralNotesSynopsis(generalNotesSynopsis);
			getEntityManager().merge(documentEntity);
		}

	}

	@Override
	public List<IGenericEntity> findEntities(Integer entityId)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"FROM MiaDocumentEntity e WHERE e.documentEntityId=:documentEntityId");
		query.setParameter("documentEntityId", entityId);

		List<IGenericEntity> entities = new ArrayList<IGenericEntity>();
		entities.add((IGenericEntity) query.getSingleResult());

		return entities;

	}

	@Override
	public Integer modifyEntities(List<IModifyDocJson> jsons)
			throws PersistenceException {
		Integer modifiedEntities = 0;

		if (jsons == null || jsons.isEmpty()) {
			return modifiedEntities;
		}
		for (IModifyDocJson json : jsons) {
			modifiedEntities = modifiedEntities + modify(json);
		}

		return modifiedEntities;
	}

	private Integer modify(IModifyDocJson json) throws PersistenceException {

		if (json == null || json.getDocumentId() == null
				|| json.getDocumentId().isEmpty())
			return 0;
		MiaDocumentEntity doc = null;
		doc = getEntityManager().find(MiaDocumentEntity.class,
				Integer.valueOf(json.getDocumentId()));

		if (doc == null)
			return 0;
		json.toEntity(doc);
		getEntityManager().merge(doc);
		return 1;

	}

	@Override
	public MiaDocumentEntity modifyPlaceOfOrigin(
			ModifyPlaceForDocJson modifyPlace) throws PersistenceException {

		if (modifyPlace.getDocumentId() == null
				|| modifyPlace.getDocumentId().isEmpty()) {
			MiaDocumentEntity docent = new MiaDocumentEntity();
			docent.setErrorMsg("documentId has no value");
			return docent;
		}

		MiaDocumentEntity doc = getEntityManager().find(
				MiaDocumentEntity.class,
				Integer.valueOf(modifyPlace.getDocumentId()));

		if (doc == null) {
			MiaDocumentEntity docent = new MiaDocumentEntity();
			docent.setErrorMsg("No document Found In DB");
			return docent;
		}

		if (modifyPlace.getNewPlace() != null) {
			doc.setPlaceOfOrigin(modifyPlace.getNewPlace().getId());
			doc.setPlaceOfOriginUnsure(modifyPlace.getNewPlace().getUnsure());
		} else if (modifyPlace.getIdPlaceToDelete() != null) {
			if (doc.getPlaceOfOrigin() != null
					&& doc.getPlaceOfOrigin() != null
					&& modifyPlace.getIdPlaceToDelete() != null
					&& (doc.getPlaceOfOrigin().equals(modifyPlace
							.getIdPlaceToDelete()))) {
				doc.setPlaceOfOrigin(null);
				doc.setPlaceOfOriginUnsure(null);
			}
		}

		getEntityManager().merge(doc);
		return doc;

	}

	@Override
	public Integer deleteDE(Integer documentEntityId, String owner)
			throws PersistenceException {

		MiaDocumentEntity docEnt = findDocumentById(documentEntityId);

		if ((docEnt == null)
				|| (docEnt.getFlgLogicalDelete() != null && docEnt
						.getFlgLogicalDelete())) {
			return null;
		}

		if ((docEnt.getCreatedBy() == null || !docEnt.getCreatedBy()
				.equalsIgnoreCase(owner))) {
			return 0;
		}

		docEnt.setFlgLogicalDelete(true);
		docEnt.setDateDeleted(new Date());
		docEnt.setDeletedBy(owner);

		getEntityManager().merge(docEnt);
		return 1;

	}

	@Override
	public Integer deleteDE(Integer documentEntityId, String owner,
			boolean isAutorizedUser) throws PersistenceException {

		MiaDocumentEntity docEnt = findDocumentById(documentEntityId);

		if ((docEnt == null)
				|| (docEnt.getFlgLogicalDelete() != null && docEnt
						.getFlgLogicalDelete())) {
			return null;
		}

		if ((docEnt.getCreatedBy() == null || !docEnt.getCreatedBy()
				.equalsIgnoreCase(owner)) && !isAutorizedUser) {
			return 0;
		}

		docEnt.setFlgLogicalDelete(true);
		docEnt.setDateDeleted(new Date());
		docEnt.setDeletedBy(owner);

		getEntityManager().merge(docEnt);
		return 1;

	}

	@Override
	public List<MiaDocumentEntity> getDocumentsBySerachWords(
			DEWordSearchJson searchReq) throws PersistenceException {

		Query q = prepareSearchQuery(searchReq);
		if (q == null) {
			return null;
		}
		@SuppressWarnings("unchecked")
		List<MiaDocumentEntity> entities = q.getResultList();
		List<MiaDocumentEntity> notRepeatedentities = null;
		if (entities != null && !entities.isEmpty()) {
			notRepeatedentities = new ArrayList<MiaDocumentEntity>();
			for (MiaDocumentEntity entity : entities) {
				if (!notRepeatedentities.contains(entity)
						&& (entity.getFlgLogicalDelete() == null || !entity
								.getFlgLogicalDelete())) {
					notRepeatedentities.add(entity);
				}
			}
		}

		return notRepeatedentities;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MiaDocumentEntity> getDocumentsByComplexSerach(
			DEComplexSearchJson searchReq, String queryString,
			DESearchPaginationParam pagParam) throws PersistenceException {

		if (queryString == null)
			return null;

		if (pagParam.isOrderByProducer()) {
			queryString = queryString + " order by p.MAPNameLF" + " "
					+ pagParam.getAscOrdDesc();

		} else if (!pagParam.isOrderByArchivalLocation()){
			queryString = queryString + " order by d."
					+ pagParam.getOrderColumn() + " "
					+ pagParam.getAscOrdDesc();
		}

		Query q = getEntityManager().createNativeQuery(queryString,
				MiaDocumentEntity.class);
		if (!pagParam.isOrderByArchivalLocation()) {
			q.setFirstResult(pagParam.getFirstResult()).setMaxResults(pagParam.getMaxResult());
		}
		if (!searchReq.isAllEntities()) {
			q.setParameter("owner", searchReq.getOwner());
		}

		return q.getResultList();
	}

	@Override
	public Integer countDocumentsByComplexSerach(DEComplexSearchJson searchReq,
			String queryString) throws PersistenceException {

		if (queryString == null)
			return null;

		Query q = getEntityManager().createNativeQuery(queryString);

		if (!searchReq.isAllEntities()) {
			q.setParameter("owner", searchReq.getOwner());
		}

		return ((BigInteger) q.getSingleResult()).intValue();

	}

	@Override
	public Integer getAllCountSynopsesBySerachWords(String searchWord)
			throws PersistenceException {

		if (searchWord == null || searchWord.isEmpty()) {
			return 0;
		}
		searchWord = searchWord.replaceAll(" ", "");
		String[] words = searchWord.split("\"");

		List<String> list = new ArrayList<String>();

		for (int i = 0; i < words.length; i++) {
			if (!words[i].isEmpty()) {
				list.add(words[i].trim());
			}
		}

		StringBuilder jpaQuery = new StringBuilder(
				"select count(distinct d.documentEntityId) from tblDocumentEnts d where ");
		if (list.size() > 1) {
			for (int i = 0; i < list.size() - 1; i++) {
				jpaQuery.append("match(generalNotesSynopsis) against ('"
						+ list.get(i) + "')");
				jpaQuery.append(" OR ");
			}
		}

		jpaQuery.append("generalNotesSynopsis like '%");
		jpaQuery.append(list.get(list.size() - 1) + "%'");
		jpaQuery.append(" AND  flgLogicalDelete != 1");

		Query q = getEntityManager().createNativeQuery(jpaQuery.toString());
		logger.info("All count Synopses search quesry: " + jpaQuery.toString());
		return ((BigInteger) q.getSingleResult()).intValue();

	}

	@Override
	public Integer getAllCountSynopses() throws PersistenceException {

		StringBuilder jpaQuery = new StringBuilder(
				"select count(distinct d.documentEntityId) from tblDocumentEnts d where ");

		jpaQuery.append(" d.flgLogicalDelete != 1");

		Query q = getEntityManager().createNativeQuery(jpaQuery.toString());
		logger.info("All count Synopses search quesry: " + jpaQuery.toString());
		return ((BigInteger) q.getSingleResult()).intValue();

	}

	private Query prepareSearchQuery(DEWordSearchJson searchReq) {

		Query q = null;
		String queryString = null;
		if (searchReq == null || searchReq.getSearchString() == null
				|| searchReq.getSearchString().trim().isEmpty()) {
			return null;
		}

		queryString = SearchUtils.getQueryString((DEWordSearchJson) searchReq);

		if (queryString == null)
			return null;

		q = getEntityManager().createNativeQuery(queryString,
				MiaDocumentEntity.class);

		q.setParameter("searchWords", searchReq.getSearchString());
		if (searchReq instanceof DEWordSearchJson) {
			if (!searchReq.isAllEntities()) {
				q.setParameter("owner", searchReq.getOwner());
			}
		}

		return q;
	}

	@Override
	public List<Integer> countDocumentsForAdvancedSearch(List<String> queryList)
			throws PersistenceException {

		if (queryList == null || queryList.isEmpty()) {
			return null;
		}

		Query q = getEntityManager().createNativeQuery(queryList.get(0));

		// NOTE: If there is no result for one of queries. The process stop and
		// return null since we are in and operation from the result of
		// queries

		List<Integer> dEIdList = (List<Integer>) q.getResultList();
		if (dEIdList == null || dEIdList.isEmpty()) {
			return null;
		}

		for (int i = 1; i < queryList.size(); i++) {
			q = getEntityManager().createNativeQuery(queryList.get(i));
			List<Integer> docIds = (List<Integer>) q.getResultList();

			if (docIds == null || docIds.isEmpty()) {
				return null;
			}
			dEIdList.retainAll(docIds);
		}

		return dEIdList;

	}

	@Override
	public List<MiaDocumentEntity> getDocumentsForPeopleAdvancedSearch(
			List<String> queryList) throws PersistenceException {

		if (queryList == null || queryList.isEmpty()) {
			return null;
		}

		Query q = getEntityManager().createNativeQuery(queryList.get(0),
				MiaDocumentEntity.class);

		// NOTE: If there is no result for one of queries. The process stop and
		// return null since we are in and operation from the result of
		// queries

		List<MiaDocumentEntity> dEIdList = (List<MiaDocumentEntity>) q
				.getResultList();
		if (dEIdList == null || dEIdList.isEmpty()) {
			return null;
		}

		for (int i = 1; i < queryList.size(); i++) {
			q = getEntityManager().createNativeQuery(queryList.get(i),
					MiaDocumentEntity.class);
			List<MiaDocumentEntity> docIds = (List<MiaDocumentEntity>) q
					.getResultList();

			if (docIds == null || docIds.isEmpty()) {
				return null;
			}
			dEIdList.retainAll(docIds);
		}

		return dEIdList;

	}

	public List<Integer> finDocumentsByPeople(List<String> queryList)
			throws PersistenceException {

		if (queryList == null || queryList.isEmpty()) {
			return null;
		}

		Query q = null;
		List<Integer> dEIdList = new ArrayList<Integer>();

		for (int i = 0; i < queryList.size(); i++) {
			q = getEntityManager().createNativeQuery(queryList.get(i));
			List<Integer> docIds = (List<Integer>) q.getResultList();

			if (docIds != null && !docIds.isEmpty()) {
				for (Integer docId : docIds) {
					if (!dEIdList.contains(docId)) {
						dEIdList.add(docId);
					}
				}

			}

		}

		return dEIdList;

	}

	public List<Integer> finDocumentsByPlaces(List<String> queryList)
			throws PersistenceException {

		if (queryList == null || queryList.isEmpty()) {
			return null;
		}

		Query q = null;
		List<Integer> dEIdList = new ArrayList<Integer>();

		for (int i = 0; i < queryList.size(); i++) {
			q = getEntityManager().createNativeQuery(queryList.get(i));
			List<Integer> docIds = (List<Integer>) q.getResultList();

			if (docIds != null && !docIds.isEmpty()) {
				for (Integer docId : docIds) {
					if (!dEIdList.contains(docId)) {
						dEIdList.add(docId);
					}
				}

			}

		}

		return dEIdList;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MiaDocumentEntity> getNews(Date date, int limit)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"FROM MiaDocumentEntity e WHERE e.dateCreated>=:dateCreated OR e.dateLastUpdate>=:dateLastUpdate order by dateLastUpdate desc , dateCreated desc ");
		query.setParameter("dateCreated", date);
		query.setParameter("dateLastUpdate", date);

		query.setMaxResults(limit);
		return (List<MiaDocumentEntity>) query.getResultList();

	}

	@Override
	public void setDocumentModInf(Integer documentEntityId, String username)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT u FROM MiaDocumentEntity u WHERE u.documentEntityId=:documentEntityId");
		query.setParameter("documentEntityId", documentEntityId);
		List<MiaDocumentEntity> list = (List<MiaDocumentEntity>) query
				.getResultList();
		if (list != null && !list.isEmpty()) {
			MiaDocumentEntity entity = list.get(0);
			entity.setLastUpdateBy(username);
			entity.setDateLastUpdate(new Date());
			getEntityManager().merge(entity);
		}

	}

	@Override
	public Integer setModifiedTransSearch(Integer documentEntityId,
			String transSearch) throws PersistenceException {

		if (documentEntityId == null || transSearch == null) {
			return 0;
		}
		MiaDocumentEntity entity = null;
		try {
			entity = getEntityManager().find(MiaDocumentEntity.class,
					documentEntityId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("No enetity found for documentId -> "
					+ documentEntityId);
			return 0;
		}

		entity.setTransSearch(transSearch);
		getEntityManager().merge(entity);
		return 1;

	}

	@Override
	public List<Integer> getDEAllCountTranscriptionAndSynopsis(
			AllCountWordSearchJson searchReq, String field)
			throws PersistenceException {

		String queryString = ComplexSearchUtils.getDEAllCountQuery(
				searchReq.getMatchSearchWords(),
				searchReq.getPartialSearchWords(), field);

		if (queryString == null)
			return new ArrayList<Integer>();

		Query q = getEntityManager().createNativeQuery(queryString);

		@SuppressWarnings("unchecked")
		List<Integer> entities = q.getResultList();

		if (entities != null && !entities.isEmpty()) {
			return entities;
		}

		return new ArrayList<Integer>();

	}

	@Override
	public List<MiaDocumentEntity> findDocumentsByPlaceOfOrigin(
			Integer placeOfOrigin) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT u FROM MiaDocumentEntity u where u.placeOfOrigin=:placeOfOrigin");
		query.setParameter("placeOfOrigin", placeOfOrigin);
		return (List<MiaDocumentEntity>) query.getResultList();
	}

	@Override
	public void associateImagesDocuments() {

		// Priority 1 if TRANSCRIBEFOLIONUM
		String stringQuery1 = "INSERT INTO tblDocumentEntsUploadedFiles(documentId, fileId)  "
				+ "select documentEntityId, uploadedFileId from tblDocumentEnts, tblDocuments, tblUploadFilesFolios, tblUploadedFiles, tblUploads   "
				+ "where documentEntityId = ENTRYID and documentEntityId not in (select documentId from tblDocumentEntsUploadedFiles)   "
				+ "and TRIM(folioNumber) = TRIM(CONCAT(TRANSCRIBEFOLIONUM, IF(TRANSCRIBEFOLIOMOD is null , '' , CONCAT(' ', TRANSCRIBEFOLIOMOD) )))   "
				+ "and rectoverso = IF(TRANSCRIBEFOLIORV = 'N', 'notapplicable', IF(TRANSCRIBEFOLIORV = 'V', 'verso', IF(TRANSCRIBEFOLIORV = 'R', 'recto' ,  'recto') ))  "
				+ "and uploadedFileId = tblUploadedFiles.id and tblUploads.id = idTblUpload and tblUploads.volume = summaryid and tblUploadedFiles.logicalDelete != 1 ";

		// Priority 2 if FOLIONUM
		String stringQuery2 = "INSERT INTO tblDocumentEntsUploadedFiles(documentId, fileId)  "
				+ "select documentEntityId, uploadedFileId from tblDocumentEnts, tblDocuments, tblUploadFilesFolios, tblUploadedFiles, tblUploads  "
				+ " where documentEntityId = ENTRYID and documentEntityId not in (select documentId from tblDocumentEntsUploadedFiles)  "
				+ " and TRIM(folioNumber) = TRIM(CONCAT(FOLIONUM, IF(FOLIOMOD is null , '' , CONCAT(' ', FOLIOMOD) )))  "
				+ " and rectoverso = IF(FOLIORV = 'N', 'notapplicable', IF(FOLIORV = 'V', 'verso', IF(FOLIORV = 'R', 'recto' , 'recto') ))  "
				+ " and uploadedFileId = tblUploadedFiles.id and tblUploads.id = idTblUpload and tblUploads.volume = summaryid and tblUploadedFiles.logicalDelete = 0 ";

		// Priority 3 attach uploadedFileId in tblDocTranscriptions
		String stringQuery3 = "UPDATE tblDocTranscriptions t set t.uploadedFileId = (select f.fileId FROM tblDocumentEntsUploadedFiles f WHERE f.documentId = t.documentEntityId limit 1)   "
				+ " WHERE t.uploadedFileId is null ";
		Query query1 = getEntityManager().createNativeQuery(stringQuery1);
		Query query2 = getEntityManager().createNativeQuery(stringQuery2);

		Query query3 = getEntityManager().createNativeQuery(stringQuery3);

		query1.executeUpdate();
		query2.executeUpdate();

		query3.executeUpdate();
	}
	
	@Override
	public void updateDocumentsPrivacy() {
		String queryStatement = 
				"UPDATE tblDocumentEnts " + 
				"SET tblDocumentEnts.privacy = 0 " + 
				"WHERE tblDocumentEnts.documentEntityId < 50000 AND tblDocumentEnts.documentEntityId in "
				+ 	"(SELECT entsUploadedFiles.documentId  FROM tblDocumentEntsUploadedFiles entsUploadedFiles " + 
					"WHERE entsUploadedFiles.fileId in "
				+ 		"(SELECT uploadedFiles.id FROM tblUploadedFiles uploadedFiles " + 
						"WHERE uploadedFiles.id = entsUploadedFiles.fileId and uploadedFiles.filePrivacy = 1))";
		
		Query query = getEntityManager().createNativeQuery(queryStatement);
		query.executeUpdate();
	}
	
	@Override
	public void removePrivateImagesFromDocuments() {
		String queryStatement = "DELETE FROM tblDocumentEntsUploadedFiles " + 
				"WHERE documentId < 50000 AND fileId IN (SELECT uploadedFiles.id FROM tblUploadedFiles uploadedFiles " + 
								 "WHERE uploadedFiles.id = fileId AND uploadedFiles.filePrivacy = 1)";
		
		Query query = getEntityManager().createNativeQuery(queryStatement);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> findUsersRelatedDocOfImageId(Integer uploadFileId) {
		Query query = getEntityManager()
				.createQuery(
						"SELECT d.createdBy FROM MiaDocumentEntity d LEFT OUTER JOIN  d.uploadFileEntities as e WHERE e.uploadFileId=:uploadFileId  AND d.flgLogicalDelete = false");
		query.setParameter("uploadFileId", uploadFileId);
		
		return (List<String>) query.getResultList();
	}
	
	private String createQueryOrderByDate(String documentsIds, String sortingMethod) {
		return String.format(
					"SELECT * FROM tblDocumentEnts u WHERE "
					+ "u.documentEntityId IN %s "
					+ "ORDER BY %s %s, %s %s, %s %s", 
					documentsIds, 
					"docSortingYear", sortingMethod,
					"docMonth", sortingMethod,
					"docDay", sortingMethod);
	}

}