package org.medici.mia.dao.miadoc;

import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.DocumentRelatedDocsJoinEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
@Repository
public class DocumentRelatedDocsDaoImpl extends
		JpaDao<Integer, DocumentRelatedDocsJoinEntity> implements
		DocumentRelatedDocsDao {

	private static final long serialVersionUID = 1L;
	private final Logger logger = Logger.getLogger(this.getClass());

	@Override
	public void insertDocRelatedDocs(
			DocumentRelatedDocsJoinEntity documentRelatedDocsJoinEntity)
			throws PersistenceException {
		if (documentRelatedDocsJoinEntity != null) {
			getEntityManager().persist(documentRelatedDocsJoinEntity);
		}

	}

	@Override
	public void modifyDocRelatedDocs(List<MiaDocumentEntity> relatedDocs,
			Integer documentId) throws PersistenceException {

		deleteRelatedDocsByDocId(documentId);

		if (relatedDocs != null && !relatedDocs.isEmpty()) {
			for (MiaDocumentEntity relatedDoc : relatedDocs) {
				DocumentRelatedDocsJoinEntity documentRelatedDocsJoinEntity = new DocumentRelatedDocsJoinEntity();
				documentRelatedDocsJoinEntity.setDocumentId(documentId);
				documentRelatedDocsJoinEntity.setDocumentRelatedId(relatedDoc
						.getDocumentEntityId());
				insertDocRelatedDocs(documentRelatedDocsJoinEntity);
			}

		}

	}

	@Override
	public void modifyRelatedDocsForDoc(Integer documentId, Integer newDocId,
			Integer oldDocId) throws PersistenceException {

		// DocumentRelatedDocsJoinEntity relDoc = getEntityManager().find(
		// DocumentRelatedDocsJoinEntity.class, documentId);
		//
		String jpql = "SELECT u FROM DocumentRelatedDocsJoinEntity u WHERE u.documentId=:documentId and u.documentRelatedId=:documentRelatedId";

		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("documentId", documentId);
		query.setParameter("documentRelatedId", oldDocId);
		DocumentRelatedDocsJoinEntity docEnt = (DocumentRelatedDocsJoinEntity) query
				.getSingleResult();

		if (docEnt != null) {
			deleteRelatedDocsForDoc(documentId, oldDocId);
			DocumentRelatedDocsJoinEntity documentRelatedDocsJoinEntity = new DocumentRelatedDocsJoinEntity();
			documentRelatedDocsJoinEntity.setDocumentId(documentId);
			documentRelatedDocsJoinEntity.setDocumentRelatedId(newDocId);
			insertDocRelatedDocs(documentRelatedDocsJoinEntity);
		} else {
			logger.info("Attention: No related documents with oldDocId = "
					+ oldDocId + " with  found in DB for being updated");
		}

	}

	@Override
	public void addRelatedDocsForDoc(Integer documentId, Integer newDocId)
			throws PersistenceException {

		DocumentRelatedDocsJoinEntity doc = new DocumentRelatedDocsJoinEntity();
		doc.setDocumentId(documentId);
		doc.setDocumentRelatedId(newDocId);
		getEntityManager().persist(doc);

	}

	public void deleteRelatedDocsForDoc(Integer documentId, Integer docToDeleted)
			throws PersistenceException {

		DocumentRelatedDocsJoinEntity doc = new DocumentRelatedDocsJoinEntity();
		doc.setDocumentId(documentId);
		doc.setDocumentRelatedId(docToDeleted);
		getEntityManager().find(DocumentRelatedDocsJoinEntity.class, doc);

		// Insert new related Docs after delete
		DocumentRelatedDocsJoinEntity relDoc = getEntityManager().find(
				DocumentRelatedDocsJoinEntity.class, doc);

		if (relDoc != null) {
			getEntityManager().remove(relDoc);
		} else {
			logger.info("Attention: No DocumentRelatedDocsJoinEntity found in DB for updating");
		}

	}

	private void deleteRelatedDocsByDocId(Integer documentId)
			throws PersistenceException {

		// Delete all the relation between this document id with the related
		// Docs

		String jpql = "DELETE FROM DocumentRelatedDocsJoinEntity u WHERE u.documentId=:documentId";

		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("documentId", documentId);
		query.executeUpdate();
		getEntityManager().flush();
	}

}
