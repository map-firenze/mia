package org.medici.mia.dao.miadoc;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.common.json.PeopleRelatedToDocumentJson;
import org.medici.mia.dao.Dao;
import org.medici.mia.domain.DocumentRefToPeopleEntity;
import org.medici.mia.domain.People;

public interface DocumentRefToPeopleDao extends
		Dao<Integer, DocumentRefToPeopleEntity> {

	public Integer insertDocRefToPeople(
			DocumentRefToPeopleEntity documentRefToPeopleEntity)
			throws PersistenceException;

	public void modifyDocRefToPeople(List<People> refToPeople,
			Integer documentId) throws PersistenceException;

	public List<DocumentRefToPeopleEntity> findDocRefToPeople(Integer documentId)
			throws PersistenceException;

	public Integer deleteDocRefToPeople(Integer documentId, Integer personId)
			throws PersistenceException;
	
	public Integer modifyDocRefToPeople(PeopleRelatedToDocumentJson json)
			throws PersistenceException;
	
	public List<DocumentRefToPeopleEntity> findDocRefToPeopleByPersonId(Integer personId)
			throws PersistenceException;

}
