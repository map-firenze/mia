package org.medici.mia.dao.miadoc;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.dao.Dao;
import org.medici.mia.domain.DocumentRelatedDocsJoinEntity;
import org.medici.mia.domain.MiaDocumentEntity;

public interface DocumentRelatedDocsDao extends
		Dao<Integer, DocumentRelatedDocsJoinEntity> {

	public void insertDocRelatedDocs(
			DocumentRelatedDocsJoinEntity documentRelatedDocsJoinEntity)
			throws PersistenceException;

	public void modifyDocRelatedDocs(List<MiaDocumentEntity> relatedDocs,
			Integer documentId) throws PersistenceException;

	public void modifyRelatedDocsForDoc(Integer documentId, Integer newDocId,
			Integer oldDocId) throws PersistenceException;

	public void addRelatedDocsForDoc(Integer documentId, Integer newDocId)
			throws PersistenceException;

	public void deleteRelatedDocsForDoc(Integer documentId, Integer docToDeleted)
			throws PersistenceException;

}
