package org.medici.mia.dao.miadoc;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.common.json.document.ModifyBiblioForDocJson;
import org.medici.mia.dao.Dao;
import org.medici.mia.domain.BiblioRefEntity;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public interface DocumentBiblioRefDao extends Dao<Integer, BiblioRefEntity> {

	public void insertBiblioRef(BiblioRefEntity biblioRef)
			throws PersistenceException;

	public void modifyBiblioRefs(List<BiblioRefEntity> biblioRefs,
			Integer documentId) throws PersistenceException;
	
	public List<BiblioRefEntity> findBiblios(Integer entityId)
			throws PersistenceException;
	
	public void modifyBiblio(ModifyBiblioForDocJson biblio)
			throws PersistenceException;

}