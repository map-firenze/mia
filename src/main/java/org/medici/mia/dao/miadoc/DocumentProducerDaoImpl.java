package org.medici.mia.dao.miadoc;

import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.medici.mia.common.json.document.ModifyPeopleForDocJson;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.DocumentProducerJoinEntity;
import org.medici.mia.domain.People;
import org.springframework.stereotype.Repository;

@Repository
public class DocumentProducerDaoImpl extends
		JpaDao<Integer, DocumentProducerJoinEntity> implements
		DocumentProducerDao {

	private static final long serialVersionUID = 1L;
	
	private final Logger logger = Logger.getLogger(this.getClass());

	@Override
	public void insertDocProducers(
			List<DocumentProducerJoinEntity> documentProducerEntities)
			throws PersistenceException {
		if (documentProducerEntities != null
				&& !documentProducerEntities.isEmpty()) {

			for (DocumentProducerJoinEntity documentProducerJoinEntity : documentProducerEntities) {
				getEntityManager().persist(documentProducerJoinEntity);
			}

		}

	}

	@Override
	public void insertDocProducer(
			DocumentProducerJoinEntity documentProducerEntity)
			throws PersistenceException {
		if (documentProducerEntity != null) {
			getEntityManager().persist(documentProducerEntity);
		}

	}
	

	@Override
	public void insertDocProducerNative(
			DocumentProducerJoinEntity documentProducerEntity)
			throws PersistenceException {
		//Priority 3 attach uploadedFileId in tblDocTranscriptions
		String stringQuery3 = "INSERT into  tblDocumentsEntsPeople (documentEntityId, producer, producerUnsure) values (:documentEntityId, :producer, :producerUnsure)";

		String completeSql = stringQuery3;
		completeSql = completeSql.replaceAll(":documentEntityId", documentProducerEntity.getDocumentEntityId() + "");
		completeSql = completeSql.replaceAll(":producerUnsure", "'" + documentProducerEntity.getProducerUnsure() + "'");
		completeSql = completeSql.replaceAll(":producer", documentProducerEntity.getProducer() + "");
		
		logger.info(completeSql);
		
		
		Query query = getEntityManager().createNativeQuery(stringQuery3);
		query.setParameter("documentEntityId", documentProducerEntity.getDocumentEntityId());
		query.setParameter("producer", documentProducerEntity.getProducer());
		query.setParameter("producerUnsure", documentProducerEntity.getProducerUnsure());
		query.executeUpdate();

		getEntityManager().flush();
	}


	@Override
	public DocumentProducerJoinEntity findDocProducer(Integer documentEntityId,
			Integer producer) throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT u FROM DocumentProducerJoinEntity u WHERE u.documentEntityId=:documentEntityId and u.producer=:producer");
		query.setParameter("documentEntityId", documentEntityId);
		query.setParameter("producer", producer);
		return (DocumentProducerJoinEntity) query.getSingleResult();

	}

	@Override
	public void modifyProducers(List<People> producers, Integer documentEntityId)
			throws PersistenceException {

		// Delete all the relation between this document id with the peoples
		deleteDocProducersByDocId(documentEntityId);
		// Insert new producers after delete

		if (producers != null && !producers.isEmpty()) {
			for (People producer : producers) {
				DocumentProducerJoinEntity documentProducerJoinEntity = new DocumentProducerJoinEntity();
				documentProducerJoinEntity
						.setDocumentEntityId(documentEntityId);
				documentProducerJoinEntity.setProducer(producer.getPersonId());
				documentProducerJoinEntity.setProducerUnsure(producer
						.getUnsure());
				insertDocProducer(documentProducerJoinEntity);

			}

		}

	}

	@Override
	public void modifyProducerForDoc(ModifyPeopleForDocJson modifyPeople)
			throws PersistenceException {

		if (modifyPeople == null || modifyPeople.getDocumentId() == null)
			return;

		if (modifyPeople.getIdPeopleToDelete() != null) {
			DocumentProducerJoinEntity entity = new DocumentProducerJoinEntity();
			entity.setDocumentEntityId(Integer.valueOf(modifyPeople.getDocumentId()));
			entity.setProducer(modifyPeople.getIdPeopleToDelete());
			
			entity = getEntityManager().find(
					DocumentProducerJoinEntity.class,
					entity);
			if (entity != null) {
				getEntityManager().remove(entity);
			}
		}

		if (modifyPeople.getNewPeople() != null
				&& modifyPeople.getNewPeople().getId() != null) {
			DocumentProducerJoinEntity documentProducerJoinEntity = new DocumentProducerJoinEntity();
			documentProducerJoinEntity.setDocumentEntityId(Integer
					.valueOf(modifyPeople.getDocumentId()));
			documentProducerJoinEntity.setProducer(modifyPeople.getNewPeople()
					.getId());
			documentProducerJoinEntity.setProducerUnsure(modifyPeople
					.getNewPeople().getUnsure());

			if (getEntityManager().find(DocumentProducerJoinEntity.class,
					documentProducerJoinEntity) == null) {
				getEntityManager().persist(documentProducerJoinEntity);
			}
		}

	}

	private void deleteDocProducersByDocId(Integer documentEntityId)
			throws PersistenceException {

		// Delete all the relation between this document id with the peoples

		String jpql = "DELETE FROM DocumentProducerJoinEntity u WHERE u.documentEntityId=:documentEntityId";

		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("documentEntityId", documentEntityId);
		query.executeUpdate();
		getEntityManager().flush();
	}

}