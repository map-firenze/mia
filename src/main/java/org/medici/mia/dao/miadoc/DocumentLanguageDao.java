package org.medici.mia.dao.miadoc;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.dao.Dao;
import org.medici.mia.domain.DocumentLanguageJoinEntity;

public interface DocumentLanguageDao extends
		Dao<Integer, DocumentLanguageJoinEntity> {

	public void insertDocLanguage(
			DocumentLanguageJoinEntity documentLanguageJoinEntity)
			throws PersistenceException;

	public void modifyLanguages(List<Integer> langIds, Integer documentId)
			throws PersistenceException;

}
