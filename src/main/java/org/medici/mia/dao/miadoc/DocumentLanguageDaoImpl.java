package org.medici.mia.dao.miadoc;

import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.DocumentLanguageJoinEntity;
import org.springframework.stereotype.Repository;

@Repository
public class DocumentLanguageDaoImpl extends
		JpaDao<Integer, DocumentLanguageJoinEntity> implements
		DocumentLanguageDao {

	private static final long serialVersionUID = 1L;

	@Override
	public void insertDocLanguage(
			DocumentLanguageJoinEntity documentLanguageJoinEntity)
			throws PersistenceException {
		if (documentLanguageJoinEntity != null) {
			getEntityManager().persist(documentLanguageJoinEntity);
		}

	}

	@Override
	public void modifyLanguages(List<Integer> langIds, Integer documentId)
			throws PersistenceException {

		// Delete all the relation between this document id with the peoples
		deleteDocLanguageByDocId(documentId);
		// Insert new referred people after delete

		if (langIds != null && !langIds.isEmpty()) {
			for (Integer langId : langIds) {
				DocumentLanguageJoinEntity documentLanguageJoinEntity = new DocumentLanguageJoinEntity();
				documentLanguageJoinEntity.setDocId(documentId);
				documentLanguageJoinEntity.setLangId(langId);
				insertDocLanguage(documentLanguageJoinEntity);
			}
		}

	}

	private void deleteDocLanguageByDocId(Integer documentId)
			throws PersistenceException {

		// Delete all the relation between this document id with the languages
		String jpql = "DELETE FROM DocumentLanguageJoinEntity u WHERE u.docId=:docId";

		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("docId", documentId);
		query.executeUpdate();
		getEntityManager().flush();
	}
}
