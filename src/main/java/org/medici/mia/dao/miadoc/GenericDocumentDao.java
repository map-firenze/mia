package org.medici.mia.dao.miadoc;

import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.common.json.complexsearch.DEComplexSearchJson;
import org.medici.mia.common.json.document.DocumentJson;
import org.medici.mia.common.json.document.IModifyDocJson;
import org.medici.mia.common.json.document.ModifyPlaceForDocJson;
import org.medici.mia.common.json.search.AllCountWordSearchJson;
import org.medici.mia.common.json.search.DEWordSearchJson;
import org.medici.mia.controller.genericsearch.DESearchPaginationParam;
import org.medici.mia.dao.Dao;
import org.medici.mia.domain.IGenericEntity;
import org.medici.mia.domain.MiaDocumentEntity;
import org.medici.mia.domain.User;

/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */
public interface GenericDocumentDao extends Dao<Integer, MiaDocumentEntity> {

	public List<MiaDocumentEntity> findDocumentRelatedToInsert(Integer insertId)
			throws PersistenceException;

	public List<MiaDocumentEntity> findDocumentRelatedToVolume(Integer volumeId)
			throws PersistenceException;

	public MiaDocumentEntity findDocumentById(Integer documentId)
			throws PersistenceException;

	public List<MiaDocumentEntity> findDocuments() throws PersistenceException;

	public List<MiaDocumentEntity> findDocuments(User user, Integer page,
			Integer perPage) throws PersistenceException;

	public List<MiaDocumentEntity> findDocumentsByUser(String userName)
			throws PersistenceException;

	public Integer insertDE(MiaDocumentEntity document)
			throws PersistenceException;

	public Integer updateDE(MiaDocumentEntity document, String username,
			Date lastUpdate) throws PersistenceException;

	public Integer countDocumentsByUploadFileId(Integer uploadFileId) throws PersistenceException;
	
	public List<Integer> findDocsIdByUploadFileId(Integer uploadFileId)
			throws PersistenceException;

	public void saveSynopsis(String synopsis, MiaDocumentEntity documentEntityId)
			throws PersistenceException;

	public List<IGenericEntity> findEntities(Integer entityId)
			throws PersistenceException;

	public Integer modifyEntities(List<IModifyDocJson> jsons)
			throws PersistenceException;

	public MiaDocumentEntity modifyPlaceOfOrigin(
			ModifyPlaceForDocJson modifyPlace) throws PersistenceException;

	public Boolean findDocLogicalDelete(Integer documentEntityId)
			throws PersistenceException;

	public Integer deleteDE(Integer documentEntityId, String owner)
			throws PersistenceException;

	public List<MiaDocumentEntity> findNotDeletedDocuments()
			throws PersistenceException;

	public List<MiaDocumentEntity> getDocumentsBySerachWords(
			DEWordSearchJson searchReq) throws PersistenceException;

	public Integer getAllCountSynopsesBySerachWords(String searchWord)
			throws PersistenceException;

	public Integer getAllCountSynopses() throws PersistenceException;

	public List<Integer> countDocumentsForAdvancedSearch(List<String> queryList)
			throws PersistenceException;

	public List<Integer> finDocumentsByPeople(List<String> queryList)
			throws PersistenceException;

	public List<Integer> finDocumentsByPlaces(List<String> queryList)
			throws PersistenceException;

	public Integer deleteDE(Integer documentEntityId, String owner,
			boolean isAutorizedUser) throws PersistenceException;

	public List<MiaDocumentEntity> getNews(Date dateCreated, int limit)
			throws PersistenceException;

	public List<MiaDocumentEntity> getDocumentsForPeopleAdvancedSearch(
			List<String> queryList) throws PersistenceException;

	public void setDocumentModInf(Integer documentId, String username)
			throws PersistenceException;

	public List<MiaDocumentEntity> getDocumentsByComplexSerach(
			DEComplexSearchJson searchReq, String queryString,
			DESearchPaginationParam pagParam) throws PersistenceException;

	public Integer countDocumentsByComplexSerach(DEComplexSearchJson searchReq,
			String queryString) throws PersistenceException;

	public Integer setModifiedTransSearch(Integer documentEntityId,
			String transcription) throws PersistenceException;

	public List<MiaDocumentEntity> findDocumentByVolumeId(Integer volumeId)
			throws PersistenceException;

	public List<Integer> getDEAllCountTranscriptionAndSynopsis(
			AllCountWordSearchJson searchReq, String field)
			throws PersistenceException;

	public Integer updateDocument(Integer docId, Integer privacy)
			throws PersistenceException;

	public List<MiaDocumentEntity> findDocumentsByPlaceOfOrigin(
			Integer placeOfOrigin) throws PersistenceException;

	List<MiaDocumentEntity> findDocumentsBiaWithOutImage()
			throws PersistenceException;

	public void associateImagesDocuments();
	
	public void updateDocumentsPrivacy();
	
	public void removePrivateImagesFromDocuments();

	public List<MiaDocumentEntity> findDocuments(List<Integer> docList,
			Integer firstResult, Integer maxResult, String orderColumn,
			String ascOrDesc) throws PersistenceException;

	public List<MiaDocumentEntity> findUserOwnDocuments(User user,
			Integer page, Integer perPage) throws PersistenceException;

	public Long countUserOwnDocuments(User user) throws PersistenceException;

	public List<String> findUsersRelatedDocOfImageId(Integer uploadFileId);

}
