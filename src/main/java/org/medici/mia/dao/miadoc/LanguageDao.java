package org.medici.mia.dao.miadoc;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.dao.Dao;
import org.medici.mia.domain.LanguageEntity;

public interface LanguageDao extends Dao<Integer, LanguageEntity> {

	public List<LanguageEntity> findLanguages() throws PersistenceException;

}
