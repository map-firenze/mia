package org.medici.mia.dao.miadoc;

import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.medici.mia.common.json.document.IModifyDocJson;
import org.medici.mia.common.json.document.ModifyTopicPlaceForDocJson;
import org.medici.mia.common.json.document.TopicPlaceJson;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.IGenericEntity;
import org.medici.mia.domain.TopicPlaceEntity;
import org.springframework.stereotype.Repository;

@Repository
public class TopicPlaceDaoImpl extends JpaDao<Integer, TopicPlaceEntity>
		implements TopicPlaceDao {

	private static final long serialVersionUID = 1L;

	@Override
	public void insertTopicPlace(List<TopicPlaceEntity> topicsPlaces)
			throws PersistenceException {
		if (topicsPlaces != null && !topicsPlaces.isEmpty()) {

			for (TopicPlaceEntity topicPlace : topicsPlaces) {
				// Check if topicPlace is already in DB
				Query query = getEntityManager()
						.createQuery(
								"FROM TopicPlaceEntity t WHERE t.documentEntityId=:documentEntityId AND t.placeId=:placeId AND t.topicListId=:topicListId");
				query.setParameter("documentEntityId",
						topicPlace.getDocumentEntityId());
				query.setParameter("placeId", topicPlace.getPlaceId());
				query.setParameter("topicListId", topicPlace.getTopicListId());
				List<TopicPlaceEntity> list = (List<TopicPlaceEntity>) query
						.getResultList();
				if (list == null || list.isEmpty()) {
					// If not in DB: insert
					getEntityManager().persist(topicPlace);
				}
			}

		}

	}

	@Override
	public Integer modifyEntities(List<IModifyDocJson> jsons)
			throws PersistenceException {
		Integer modifiedEntities = 0;

		if (jsons == null || jsons.isEmpty()) {
			return modifiedEntities;
		}
		for (IModifyDocJson json : jsons) {
			modifiedEntities = modifiedEntities + modify((TopicPlaceJson) json);
		}

		return modifiedEntities;
	}

	private Integer modify(IModifyDocJson json) throws PersistenceException {
		if (json == null)
			return 0;
		TopicPlaceJson tJson = (TopicPlaceJson) json;
		TopicPlaceEntity topicPlaceEnt = new TopicPlaceEntity();
		topicPlaceEnt
				.setDocumentEntityId(Integer.valueOf(tJson.getDocumentId()));
		topicPlaceEnt.setPlaceId(tJson.getPlaceId());
		topicPlaceEnt.setTopicListId(tJson.getPlaceId());

		TopicPlaceEntity entity = getEntityManager().find(
				TopicPlaceEntity.class, topicPlaceEnt);
		if (entity == null)
			return 0;

		tJson.toEntity(entity);
		getEntityManager().merge(entity);
		return 1;

	}

	@Override
	public List<IGenericEntity> findEntities(Integer entityId)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"FROM TopicPlaceEntity e WHERE e.documentEntityId=:documentEntityId");
		query.setParameter("documentEntityId", entityId);

		return query.getResultList();

	}

	@Override
	public List<TopicPlaceEntity> findTopicPlaces(Integer entityId)
			throws PersistenceException {

		Query query = getEntityManager()
				.createNativeQuery(
						"select * FROM tblDocumentTopicPlace e LEFT JOIN tblTopicsList tl ON (tl.TOPICID = e.topicListId) where e.documentEntityId=:documentEntityId order by tl.TOPICTITLE",
						TopicPlaceEntity.class);

		query.setParameter("documentEntityId", entityId);

		return query.getResultList();

	}

	@Override
	public List<TopicPlaceEntity> findTopicPlacesByPlaceId(Integer placeId)
			throws PersistenceException {

		Query query = getEntityManager().createQuery(
				"FROM TopicPlaceEntity e WHERE e.placeId=:placeId");
		query.setParameter("placeId", placeId);

		return (List<TopicPlaceEntity>) query.getResultList();

	}

	@Override
	public List<Integer> findDccumentsByPlaceAndTopic(Integer placeId,
			Integer topicListId) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT e.documentEntityId FROM TopicPlaceEntity e WHERE e.placeId=:placeId AND e.topicListId=:topicListId");
		query.setParameter("placeId", placeId);
		query.setParameter("topicListId", topicListId);

		return (List<Integer>) query.getResultList();

	}

	@Override
	public void modifyTopicPlaces(ModifyTopicPlaceForDocJson topicPlace)
			throws PersistenceException {

		if (topicPlace.getTopicPlaceToBeDeleted() != null) {
			// delete entity
			TopicPlaceEntity entityToDeleted = new TopicPlaceEntity();
			entityToDeleted.setDocumentEntityId(Integer.valueOf(topicPlace
					.getDocumentId()));
			entityToDeleted.setPlaceId(topicPlace.getTopicPlaceToBeDeleted()
					.getPlaceId());
			entityToDeleted.setTopicListId(topicPlace
					.getTopicPlaceToBeDeleted().getTopicListId());

			if (getEntityManager()
					.find(TopicPlaceEntity.class, entityToDeleted) != null) {
				getEntityManager().remove(entityToDeleted);
			}

		} else if (topicPlace.getOldTopicPlace() != null
				&& topicPlace.getNewTopicPlace() != null) {
			// Modify
			TopicPlaceEntity entityTobeModified = new TopicPlaceEntity();
			entityTobeModified.setDocumentEntityId(Integer.valueOf(topicPlace
					.getDocumentId()));
			entityTobeModified.setPlaceId(topicPlace.getOldTopicPlace()
					.getPlaceId());
			entityTobeModified.setTopicListId(topicPlace.getOldTopicPlace()
					.getTopicListId());

			if (getEntityManager().find(TopicPlaceEntity.class,
					entityTobeModified) != null) {
				getEntityManager().remove(entityTobeModified);

				TopicPlaceEntity entity = new TopicPlaceEntity();
				entity.setDocumentEntityId(Integer.valueOf(topicPlace
						.getDocumentId()));
				entity.setPlaceId(topicPlace.getNewTopicPlace().getPlaceId());
				entity.setTopicListId(topicPlace.getNewTopicPlace()
						.getTopicListId());
				if (getEntityManager().find(TopicPlaceEntity.class, entity) == null) {
					getEntityManager().persist(entity);
				}

			}

		} else if (topicPlace.getNewTopicPlace() != null) {
			// Modify
			TopicPlaceEntity entityTobeAdded = new TopicPlaceEntity();
			entityTobeAdded.setDocumentEntityId(Integer.valueOf(topicPlace
					.getDocumentId()));
			entityTobeAdded.setPlaceId(topicPlace.getNewTopicPlace()
					.getPlaceId());
			entityTobeAdded.setTopicListId(topicPlace.getNewTopicPlace()
					.getTopicListId());
			if (getEntityManager()
					.find(TopicPlaceEntity.class, entityTobeAdded) == null) {
				getEntityManager().persist(entityTobeAdded);
			}

		}

	}

}
