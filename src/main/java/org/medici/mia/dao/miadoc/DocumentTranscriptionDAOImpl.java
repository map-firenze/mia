package org.medici.mia.dao.miadoc;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.medici.mia.common.json.document.DocumentTranscriptionJson;
import org.medici.mia.common.json.document.IModifyDocJson;
import org.medici.mia.common.json.document.ModifyImageJson;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.DocumentTranscriptionEntity;
import org.medici.mia.domain.IGenericEntity;
import org.springframework.stereotype.Repository;
/**
 * 
 * @author Shadab Bigdel (<a
 *         href=mailto:shbigdel@gmail.com>shbigdel@gmail.com</a>)
 *
 */

@Repository
public class DocumentTranscriptionDAOImpl extends
		JpaDao<Integer, DocumentTranscriptionEntity> implements
		DocumentTranscriptionDAO {

	private static final long serialVersionUID = 7211118023405517179L;

	@Override
	public void saveOrModifyTranscriptions(
			List<DocumentTranscriptionEntity> documentTranscriptions,
			boolean isModify) throws PersistenceException {
		for (DocumentTranscriptionEntity documentTranscription : documentTranscriptions) {
			if (isModify) {
				DocumentTranscriptionEntity entity = getEntityManager().find(
						DocumentTranscriptionEntity.class,
						documentTranscription.getDocTranscriptionId());
				if (entity == null)
					return;

				entity.setTranscription(documentTranscription
						.getTranscription());
				getEntityManager().merge(entity);
			} else {
				saveTranscription(documentTranscription);
			}
		}
	}

	@Override
	public Integer modifyEntities(List<IModifyDocJson> jsons)
			throws PersistenceException {
		Integer modifiedEntities = 0;

		if (jsons == null || jsons.isEmpty()) {
			return modifiedEntities;
		}
		for (IModifyDocJson json : jsons) {
			modifiedEntities = modifiedEntities
					+ modify((DocumentTranscriptionJson) json);
		}

		return modifiedEntities;
	}

	@Override
	public void saveTranscription(
			DocumentTranscriptionEntity documentTranscription)
			throws PersistenceException {
		if (documentTranscription != null) {
			getEntityManager().persist(documentTranscription);
			getEntityManager().flush();
		}
	}

	private Integer modify(IModifyDocJson json) throws PersistenceException {
		if (json == null)
			return 0;
		DocumentTranscriptionJson tarnsJson = (DocumentTranscriptionJson) json;
		if (tarnsJson.getDocTranscriptionId() != null) {
			DocumentTranscriptionEntity entity = getEntityManager().find(
					DocumentTranscriptionEntity.class,
					Integer.valueOf(tarnsJson.getDocTranscriptionId()));
			if (entity == null)
				return 0;

			tarnsJson.toEntity(entity);
			getEntityManager().merge(entity);
			return 1;
		}

		return 0;
	}

	@Override
	public List<IGenericEntity> findEntities(Integer entityId)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"FROM DocumentTranscriptionEntity e WHERE e.documentEntityId=:documentEntityId");
		query.setParameter("documentEntityId", entityId);

		return query.getResultList();

	}
	
	@Override
	public List<IGenericEntity> findExistingEntities(Integer documentEntityId, Integer uploadedFileId)
			throws PersistenceException {

		Query query = getEntityManager().createQuery(
				"FROM DocumentTranscriptionEntity e WHERE e.documentEntityId=:documentEntityId AND e.uploadedFileId=:uploadedFileId");
		query.setParameter("documentEntityId", documentEntityId);
		query.setParameter("uploadedFileId", uploadedFileId);

		return query.getResultList();

	}

	@Override
	public List<DocumentTranscriptionEntity> findAllDocTrans()
			throws PersistenceException {

		Query query = getEntityManager().createQuery(
				"FROM DocumentTranscriptionEntity");

		return query.getResultList();

	}

	@Override
	public Integer getAllCountTranscriptionBySerachWords(String searchWord)
			throws PersistenceException {

		if (searchWord == null || searchWord.isEmpty()) {
			return 0;
		}
		searchWord = searchWord.replaceAll(" ", "");
		String[] words = searchWord.split("\"");

		List<String> list = new ArrayList<String>();

		for (int i = 0; i < words.length; i++) {
			if (!words[i].isEmpty()) {
				list.add(words[i].trim());
			}
		}

		StringBuilder jpaQuery = new StringBuilder(
				"select count(distinct d.documentEntityId) from tblDocTranscriptions d , tblDocumentEnts doc where ");
		if (list.size() > 1) {
			for (int i = 0; i < list.size() - 1; i++) {
				jpaQuery.append("match(d.transcription) against ('"
						+ list.get(i) + "')");
				jpaQuery.append(" OR ");
			}
		}

		jpaQuery.append("d.transcription like '%");
		jpaQuery.append(list.get(list.size() - 1) + "%'");
		jpaQuery.append(" and doc.documentEntityId = d.documentEntityId");
		jpaQuery.append(" and  doc.flgLogicalDelete = false");

		Query q = getEntityManager().createNativeQuery(jpaQuery.toString());

		return ((BigInteger) q.getSingleResult()).intValue();

	}

	@Override
	public Integer getAllCountTranscription() throws PersistenceException {
		StringBuilder jpaQuery = new StringBuilder(
				"select count(distinct d.documentEntityId) from tblDocTranscriptions d , tblDocumentEnts doc where ");

		jpaQuery.append(" doc.documentEntityId = d.documentEntityId");
		jpaQuery.append(" and  doc.flgLogicalDelete = false");

		Query q = getEntityManager().createNativeQuery(jpaQuery.toString());

		return ((BigInteger) q.getSingleResult()).intValue();

	}

	@Override
	public String getDocTransFilesToRemove(ModifyImageJson json)
			throws PersistenceException {

		if (json.getModifyImagesInDe() == null
				|| json.getModifyImagesInDe().isEmpty()) {
			return null;
		}
		Query query = getEntityManager()
				.createQuery(
						"FROM DocumentTranscriptionEntity e WHERE e.documentEntityId=:documentEntityId");
		query.setParameter("documentEntityId", json.getDocumentId());
		String transSearch = "";

		List<DocumentTranscriptionEntity> transDocs = query.getResultList();
		if (transDocs != null && !transDocs.isEmpty()) {

			for (DocumentTranscriptionEntity docTran : transDocs) {
				if (!json.getModifyImagesInDe().contains(
						docTran.getUploadedFileId())) {
					if (docTran.getUploadedFileId() != null) {
						getEntityManager().remove(docTran);

					} else {

						transSearch = transSearch + " - "
								+ docTran.getTranscription();
					}
				} else {
					transSearch = transSearch + " - "
							+ docTran.getTranscription();
				}
			}

		}

		return transSearch;

	}

}
