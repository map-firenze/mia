package org.medici.mia.dao.miadoc;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.common.json.document.ModifyImageJson;
import org.medici.mia.dao.Dao;
import org.medici.mia.domain.DocumentFileJoinEntity;

public interface DocumentFileDao extends Dao<Integer, DocumentFileJoinEntity> {

	public void insertDocFile(List<DocumentFileJoinEntity> documentFileEntities) throws PersistenceException;
	public void insertDocFile(DocumentFileJoinEntity documentFileEntity) throws PersistenceException;
	public void modifyDocFiles(List<DocumentFileJoinEntity> documentFileEntities, Integer documentEntityId) throws PersistenceException;
	public List<DocumentFileJoinEntity> findDocumentsByFileId(Integer fileId) throws PersistenceException;
	public Integer insertFiles(ModifyImageJson json) throws PersistenceException;
	public Integer modifyDocFiles(ModifyImageJson json) throws PersistenceException;
}