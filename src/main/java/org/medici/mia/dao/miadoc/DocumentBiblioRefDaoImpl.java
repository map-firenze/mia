package org.medici.mia.dao.miadoc;

import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.medici.mia.common.json.document.BiblioBaseJson;
import org.medici.mia.common.json.document.ModifyBiblioForDocJson;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.BiblioRefEntity;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author User
 *
 */
@Repository
public class DocumentBiblioRefDaoImpl extends JpaDao<Integer, BiblioRefEntity>
		implements DocumentBiblioRefDao {

	private static final long serialVersionUID = 7211118023405517179L;

	@Override
	public void insertBiblioRef(BiblioRefEntity biblioRef)
			throws PersistenceException {
		if (biblioRef != null) {
			getEntityManager().persist(biblioRef);
		}

	}

	@Override
	public void modifyBiblioRefs(List<BiblioRefEntity> biblioRefs,
			Integer documentId) throws PersistenceException {

		// Delete all the relation between this document id with the peoples
		deleteDocRefToPeopleByDocId(documentId);
		// Insert new referred people after delete

		if (biblioRefs != null && !biblioRefs.isEmpty()) {
			for (BiblioRefEntity biblioRef : biblioRefs) {
				biblioRef.setIdDoc(documentId);
				insertBiblioRef(biblioRef);
			}

		}

	}

	private void deleteDocRefToPeopleByDocId(Integer documentId)
			throws PersistenceException {

		// Delete all the relation between this document id and the
		// bibliographic references
		String jpql = "DELETE FROM BiblioRefEntity u WHERE u.idDoc=:documentId";

		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("documentId", documentId);
		query.executeUpdate();
		getEntityManager().flush();
	}

	public List<BiblioRefEntity> findBiblios(Integer idDoc)
			throws PersistenceException {

		Query query = getEntityManager().createQuery(
				"FROM BiblioRefEntity e WHERE e.idDoc=:idDoc");
		query.setParameter("idDoc", idDoc);

		return query.getResultList();
	}

	@Override
	public void modifyBiblio(ModifyBiblioForDocJson biblio)
			throws PersistenceException {

		if (biblio.getIdBiblioToBeDeleted() != null) {
			// delete entity

			BiblioRefEntity entityToDeleted = getEntityManager().find(
					BiblioRefEntity.class,
					Integer.valueOf(biblio.getIdBiblioToBeDeleted()));
			if (entityToDeleted != null) {
				getEntityManager().remove(entityToDeleted);
			}

		} else if (biblio.getNewBiblio() != null) {

			if (biblio.getNewBiblio().getBiblioId() != null) {

				BiblioRefEntity entityToModified = getEntityManager().find(
						BiblioRefEntity.class,
						Integer.valueOf(biblio.getNewBiblio().getBiblioId()));
				if (entityToModified != null) {
					entityToModified.setName(biblio.getNewBiblio().getName());
					entityToModified.setPermalink(biblio.getNewBiblio().getLink());
					getEntityManager().merge(entityToModified);
				}
			} else{
				BiblioRefEntity entityToPersist = new BiblioRefEntity();
				entityToPersist.setName(biblio.getNewBiblio().getName());
				entityToPersist.setIdDoc(Integer.valueOf(biblio.getDocumentId()));
				entityToPersist.setPermalink(biblio.getNewBiblio().getLink());
				getEntityManager().persist(entityToPersist);
				getEntityManager().flush();
				BiblioBaseJson b = new BiblioBaseJson();
				b.setBiblioId(entityToPersist.getIdRef());
				b.setLink(entityToPersist.getPermalink());
				b.setName(entityToPersist.getName());
				biblio.setNewBiblio(b);
			}

		}

	}

}
