package org.medici.mia.dao.miadoc;

import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.medici.mia.common.json.PeopleRelatedToDocumentJson;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.DocumentRefToPeopleEntity;
import org.medici.mia.domain.People;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author User
 *
 */
@Repository
public class DocumentRefToPeopelDaoImpl extends
		JpaDao<Integer, DocumentRefToPeopleEntity> implements
		DocumentRefToPeopleDao {

	private static final long serialVersionUID = 1L;

	@Override
	public Integer insertDocRefToPeople(
			DocumentRefToPeopleEntity entity)
			throws PersistenceException {
		
		String jpql = "Select u FROM DocumentRefToPeopleEntity u WHERE u.documentId=:documentId and u.peopleId=:peopleId";

		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("documentId", entity.getDocumentId());
		query.setParameter("peopleId", entity.getPeopleId());
		List<DocumentRefToPeopleEntity> ret = (List<DocumentRefToPeopleEntity> ) query.getResultList();
		if (ret == null || ret.size() == 0) {
			getEntityManager().persist(entity);
			return 1;
		}
		return 0;

	}
	
	@Override
	public Integer deleteDocRefToPeople(Integer documentId, Integer personId)
			throws PersistenceException {
		
		String jpql = "Select u FROM DocumentRefToPeopleEntity u WHERE u.documentId=:documentId and u.peopleId=:peopleId";

		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("documentId", documentId);
		query.setParameter("peopleId", personId);
		List<DocumentRefToPeopleEntity> ret = (List<DocumentRefToPeopleEntity> ) query.getResultList();
		if (ret == null || ret.size() == 0) {			
			return 0;
		}
		getEntityManager().remove(ret.get(0));
		return 1;

	}
	
	

	@Override
	public void modifyDocRefToPeople(List<People> refToPeople,
			Integer documentId) throws PersistenceException {

		// Delete all the relation between this document id with the peoples
		deleteDocRefToPeopleByDocId(documentId);
		// Insert new referred people after delete

		if (refToPeople != null && !refToPeople.isEmpty()) {
			for (People refToPerson : refToPeople) {
				DocumentRefToPeopleEntity documentRefToPeopleEntity = new DocumentRefToPeopleEntity();
				documentRefToPeopleEntity.setDocumentId(documentId);
				documentRefToPeopleEntity
						.setPeopleId(refToPerson.getPersonId());
				insertDocRefToPeople(documentRefToPeopleEntity);
			}

		}

	}

	private void deleteDocRefToPeopleByDocId(Integer documentId)
			throws PersistenceException {

		// Delete all the relation between this document id with the peoples

		String jpql = "DELETE FROM DocumentRefToPeopleEntity u WHERE u.documentId=:documentId";

		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("documentId", documentId);
		query.executeUpdate();
		getEntityManager().flush();
	}

	public List<DocumentRefToPeopleEntity> findDocRefToPeople(Integer documentId)
			throws PersistenceException {

		String jpql = "SELECT u FROM DocumentRefToPeopleEntity u, People peo "
				+ "WHERE u.documentId=:documentId AND peo.personId = u.peopleId "
				+ "ORDER BY peo.mapNameLf ASC";

		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("documentId", documentId);
		return (List<DocumentRefToPeopleEntity>) query.getResultList();

	}
	
	public List<DocumentRefToPeopleEntity> findDocRefToPeopleByPersonId(Integer personId)
			throws PersistenceException {

		String jpql = "SELECT u FROM DocumentRefToPeopleEntity u WHERE u.peopleId=:peopleId";

		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("peopleId", personId);
		return (List<DocumentRefToPeopleEntity>) query.getResultList();

	}
	
	@Override
	public Integer modifyDocRefToPeople(PeopleRelatedToDocumentJson json)
			throws PersistenceException {

		String jpql = "Select u FROM DocumentRefToPeopleEntity u WHERE u.documentId=:documentId and u.peopleId=:peopleId";

		Query query = getEntityManager().createQuery(jpql);
		query.setParameter("documentId", json.getDocumentId());
		query.setParameter("peopleId", json.getOldPersonCitedInDoc().getId());
		List<DocumentRefToPeopleEntity> ret = (List<DocumentRefToPeopleEntity>) query
				.getResultList();
		if (ret == null || ret.size() == 0) {
			return 0;
		}

		query = getEntityManager().createQuery(jpql);
		query.setParameter("documentId", json.getDocumentId());
		query.setParameter("peopleId", json.getNewPersonCitedInDoc().getId());
		List<DocumentRefToPeopleEntity> retNew = (List<DocumentRefToPeopleEntity>) query
				.getResultList();
		if (retNew == null || retNew.size() == 0) {
			DocumentRefToPeopleEntity entity = new DocumentRefToPeopleEntity();
			entity.setDocumentId(json.getDocumentId());
			entity.setPeopleId(json.getNewPersonCitedInDoc().getId());
			entity.setUnSure(json.getNewPersonCitedInDoc().getUnsure());
			getEntityManager().remove(ret.get(0));
			getEntityManager().persist(entity);
			return 1;
		}
		return 0;

	}


}
