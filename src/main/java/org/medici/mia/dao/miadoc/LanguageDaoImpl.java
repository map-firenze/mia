package org.medici.mia.dao.miadoc;

import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.LanguageEntity;
import org.springframework.stereotype.Repository;

@Repository
public class LanguageDaoImpl extends JpaDao<Integer, LanguageEntity> implements
		LanguageDao {

	private static final long serialVersionUID = 1L;

	@Override
	public List<LanguageEntity> findLanguages() throws PersistenceException {

		Query query = getEntityManager().createQuery("FROM LanguageEntity");

		return (List<LanguageEntity>) query.getResultList();

	}

}
