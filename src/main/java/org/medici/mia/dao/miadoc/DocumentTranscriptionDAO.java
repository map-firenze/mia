package org.medici.mia.dao.miadoc;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.common.json.document.IModifyDocJson;
import org.medici.mia.common.json.document.ModifyImageJson;
import org.medici.mia.dao.Dao;
import org.medici.mia.domain.DocumentTranscriptionEntity;
import org.medici.mia.domain.IGenericEntity;

/**
 * 
 * @author User
 *
 */
public interface DocumentTranscriptionDAO extends
		Dao<Integer, DocumentTranscriptionEntity> {

	public void saveOrModifyTranscriptions(
			List<DocumentTranscriptionEntity> documentTranscriptions,
			boolean isModify) throws PersistenceException;

	public void saveTranscription(
			DocumentTranscriptionEntity documentTranscription)
			throws PersistenceException;

	public List<IGenericEntity> findEntities(Integer entityId)
			throws PersistenceException;
	
	public List<IGenericEntity> findExistingEntities(Integer documentEntityId, Integer uploadedFileId)
			throws PersistenceException;

	public Integer modifyEntities(List<IModifyDocJson> jsons)
			throws PersistenceException;

	public Integer getAllCountTranscriptionBySerachWords(String searchWord)
			throws PersistenceException;

	public Integer getAllCountTranscription() throws PersistenceException;

	public List<DocumentTranscriptionEntity> findAllDocTrans()
			throws PersistenceException;
	
	public String getDocTransFilesToRemove(ModifyImageJson json)
			throws PersistenceException;

	
}