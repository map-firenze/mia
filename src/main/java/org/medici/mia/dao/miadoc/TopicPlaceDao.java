package org.medici.mia.dao.miadoc;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.common.json.document.IModifyDocJson;
import org.medici.mia.common.json.document.ModifyTopicPlaceForDocJson;
import org.medici.mia.dao.Dao;
import org.medici.mia.domain.IGenericEntity;
import org.medici.mia.domain.TopicPlaceEntity;

public interface TopicPlaceDao extends Dao<Integer, TopicPlaceEntity> {

	public void insertTopicPlace(List<TopicPlaceEntity> topicsPlaces)
			throws PersistenceException;

	public List<IGenericEntity> findEntities(Integer entityId)
			throws PersistenceException;

	public List<TopicPlaceEntity> findTopicPlaces(Integer entityId)
			throws PersistenceException;

	public Integer modifyEntities(List<IModifyDocJson> jsons)
			throws PersistenceException;
	
	public void modifyTopicPlaces(ModifyTopicPlaceForDocJson topicPlace)
			throws PersistenceException;
	
	public List<TopicPlaceEntity> findTopicPlacesByPlaceId(Integer placeId)
			throws PersistenceException;
	
	public List<Integer> findDccumentsByPlaceAndTopic(Integer placeId, Integer topicId)
			throws PersistenceException;
	
	

}
