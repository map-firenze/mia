package org.medici.mia.dao.documentfield;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.dao.Dao;
import org.medici.mia.domain.DocumentFieldEntity;

public interface DocumentFieldDAO extends Dao<Integer, DocumentFieldEntity> {

	public List<DocumentFieldEntity> getDocumentFields()
			throws PersistenceException;

	public List<DocumentFieldEntity> getDocumentFieldsByCategory(
			String documentCategory) throws PersistenceException;

}
