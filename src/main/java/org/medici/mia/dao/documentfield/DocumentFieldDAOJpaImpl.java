package org.medici.mia.dao.documentfield;

import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.DocumentFieldEntity;
import org.springframework.stereotype.Repository;

@Repository
public class DocumentFieldDAOJpaImpl extends
		JpaDao<Integer, DocumentFieldEntity> implements DocumentFieldDAO {

	private static final long serialVersionUID = -5796533789041210555L;

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentFieldEntity> getDocumentFields()
			throws PersistenceException {

		Query query = getEntityManager().createQuery(
				"SELECT u FROM DocumentFieldEntity u");
		return (List<DocumentFieldEntity>) query.getResultList();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentFieldEntity> getDocumentFieldsByCategory(
			String documentCategory) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT u FROM DocumentFieldEntity u where u.documentCategory=:documentCategory");
		query.setParameter("documentCategory", documentCategory);
		return (List<DocumentFieldEntity>) query.getResultList();

	}

}
