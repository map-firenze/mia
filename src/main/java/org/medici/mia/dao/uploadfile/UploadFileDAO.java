package org.medici.mia.dao.uploadfile;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.dao.Dao;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.UploadInfoEntity;


public interface UploadFileDAO extends Dao<Integer, UploadFileEntity> {
	
	public Integer insertUploadFile(UploadFileEntity uploadFile) throws PersistenceException;
	public void insertUploadFiles(List<UploadFileEntity> uploadFiles) throws PersistenceException;
	public void updateUploadFilesConvertField (List<Integer> uploadFilesId, Integer convertedVal) throws PersistenceException;
	public void updateUploadFileConvertField(Integer uploadFileId, Integer convertedVal) throws PersistenceException;
	public int updateUploadFilePrivacyField(Integer uploadFileId, Integer filePrivacy) throws PersistenceException;
	public boolean updateUploadFilesImageField(List<UploadFileEntity> uploadFiles) throws PersistenceException;
	public UploadFileEntity findUploadFile(Integer uploadFileId ) throws PersistenceException;
	//This method is used for getArchive
	public List<UploadFileEntity> findUploadFileForArchive(Integer uploadInfoId ) throws PersistenceException;
	public List<UploadFileEntity> findUploadFileForArchive(Integer uploadInfoId, Integer page, Integer perPage) throws PersistenceException;
	public boolean updateLogicalDelete(Integer uploadFileId) throws PersistenceException;
	public boolean uploadFilesDelete(List<UploadFileEntity> uploadFiles) throws PersistenceException;
	public List<UploadFileEntity> findUploadFilesByUploadInfoId(Integer uploadInfoId) throws PersistenceException;
	public boolean updateDateModified(Integer uploadFileId) throws PersistenceException;
	public boolean updateIdDocumentEnts(Integer uploadFileId, Integer idDocumentEnts) throws PersistenceException;
	public Integer replaceFiles(Integer oldUploadFileId, Integer newUploadFileId) throws PersistenceException;
	public Boolean isOwnerOrPublic(String username, Integer uploadFileId);
	public Integer countUploadFileForArchive(Integer uploadInfoId) throws PersistenceException;
	public Integer findFilePrivacy(Integer uploadInfoId) throws PersistenceException;
	public UploadFileEntity findLastUploadFileByUploadInfoId(Integer uploadInfoId) throws PersistenceException;
	public UploadFileEntity createNoImageUploadFile(Integer uploadInfoId);
}