package org.medici.mia.dao.uploadfile;

import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.service.genericsearch.ComplexSearchUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class UploadFileDAOJpaImpl extends JpaDao<Integer, UploadFileEntity>
		implements UploadFileDAO {

	private static final long serialVersionUID = 5036755198146289205L;
	private static final Logger logger = Logger
			.getLogger(ComplexSearchUtils.class);

	@Transactional(readOnly = false)
	@Override
	public Integer insertUploadFile(UploadFileEntity uploadFileEntity) {
		getEntityManager().persist(uploadFileEntity);

		return uploadFileEntity.getUploadFileId();
	}

	@Override
	public void insertUploadFiles(List<UploadFileEntity> uploadFiles)
			throws PersistenceException {
		if (uploadFiles != null && !uploadFiles.isEmpty()) {
			for (UploadFileEntity uploadFile : uploadFiles) {
				getEntityManager().persist(uploadFile);

			}
		}

	}

	@Override
	public void updateUploadFilesConvertField(List<Integer> uploadFilesId,
			Integer convertedVal) throws PersistenceException {

		if (uploadFilesId != null && !uploadFilesId.isEmpty()) {

			for (Integer uploadFileId : uploadFilesId) {
				UploadFileEntity uploadFileEntity = getEntityManager().find(
						UploadFileEntity.class, uploadFileId);
				uploadFileEntity.setTiledConversionProcess(convertedVal);
				getEntityManager().merge(uploadFileEntity);

			}

		}

	}

	@Override
	public void updateUploadFileConvertField(Integer uploadFileId,
			Integer tiledConversionProcess) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"UPDATE UploadFileEntity e SET e.tiledConversionProcess=:tiledConversionProcess WHERE e.uploadFileId=:uploadFileId");
		query.setParameter("uploadFileId", uploadFileId);
		query.setParameter("tiledConversionProcess", tiledConversionProcess);
		query.executeUpdate();

	}

	@Override
	public int updateUploadFilePrivacyField(Integer uploadFileId,
			Integer filePrivacy) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"UPDATE UploadFileEntity e SET e.filePrivacy=:filePrivacy, e.dateModified=:dateModified WHERE e.uploadFileId=:uploadFileId");
		query.setParameter("uploadFileId", uploadFileId);
		query.setParameter("filePrivacy", filePrivacy);
		query.setParameter("dateModified", new Date());

		return query.executeUpdate();

	}

	@Override
	public UploadFileEntity findUploadFile(Integer uploadFileId)
			throws PersistenceException {

		UploadFileEntity uploadFileEntity = getEntityManager().find(
				UploadFileEntity.class, uploadFileId);

		return uploadFileEntity;

	}

	public boolean updateUploadFilesImageField(
			List<UploadFileEntity> uploadFiles) throws PersistenceException {

		int result = 0;
		for (UploadFileEntity uploadFileEntity : uploadFiles) {

			Query query = getEntityManager()
					.createQuery(
							"UPDATE UploadFileEntity e SET e.imageOrder=:imageOrder, e.dateModified=:dateModified WHERE e.uploadFileId=:uploadFileId");
			query.setParameter("uploadFileId",
					uploadFileEntity.getUploadFileId());
			query.setParameter("imageOrder", uploadFileEntity.getImageOrder());
			query.setParameter("dateModified", new Date());
			result = query.executeUpdate();

			if (result == 0) {
				return false;
			}
		}
		return true;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UploadFileEntity> findUploadFileForArchive(Integer uploadInfoId)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"FROM UploadFileEntity e WHERE e.idTblUpload=:uploadInfoId AND e.logicalDelete=0 ORDER BY e.imageOrder");
		query.setParameter("uploadInfoId", uploadInfoId);

		return (List<UploadFileEntity>) query.getResultList();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UploadFileEntity> findUploadFileForArchive(Integer uploadInfoId, Integer page, Integer perPage)
			throws PersistenceException {
		int offset = page * perPage - perPage;
		Query query = getEntityManager()
				.createQuery(
						"FROM UploadFileEntity e WHERE e.idTblUpload=:uploadInfoId AND e.logicalDelete=0 ORDER BY e.imageOrder");
		if(perPage > 0 && offset >= 0){
			query.setMaxResults(perPage);
			query.setFirstResult(offset);
		}
		query.setParameter("uploadInfoId", uploadInfoId);

		return (List<UploadFileEntity>) query.getResultList();

	}

	@Override
	public Integer countUploadFileForArchive(Integer uploadInfoId)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT COUNT(e.id) FROM UploadFileEntity e WHERE e.idTblUpload=:uploadInfoId AND e.logicalDelete=0 ORDER BY e.imageOrder");
		query.setParameter("uploadInfoId", uploadInfoId);
		Long res = (Long) query.getSingleResult();
		if(res == null){
			return 0;
		}
		return res.intValue();

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<UploadFileEntity> findUploadFilesByUploadInfoId(
			Integer uploadInfoId) throws PersistenceException {

		Query query = getEntityManager().createQuery(
				"FROM UploadFileEntity e WHERE e.idTblUpload=:uploadInfoId");
		query.setParameter("uploadInfoId", uploadInfoId);

		return (List<UploadFileEntity>) query.getResultList();

	}

	@Override
	public boolean updateLogicalDelete(Integer uploadFileId)
			throws PersistenceException {
		Date date = new Date();
		Query query = getEntityManager()
				.createQuery(
						"UPDATE UploadFileEntity e SET e.logicalDelete=1, e.imageOrder=-1, e.dateDeleted=:date WHERE e.uploadFileId=:uploadFileId");
		query.setParameter("uploadFileId", uploadFileId);
		query.setParameter("date", date);
		int result = query.executeUpdate();

		if (result != 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean uploadFilesDelete(List<UploadFileEntity> uploadFiles)
			throws PersistenceException {
			
			int result = 0;
			if(uploadFiles ==null || uploadFiles.isEmpty()){
				logger.info("No upload file to delete");
				return true;
				
			}
			for (UploadFileEntity uploadFileEntity : uploadFiles) {

				Query query = getEntityManager()
						.createQuery(
								"UPDATE UploadFileEntity e SET e.logicalDelete=:logicalDelete, e.dateDeleted=:dateDeleted, e.imageOrder=:imageOrder WHERE e.uploadFileId=:uploadFileId");
				query.setParameter("uploadFileId", uploadFileEntity.getUploadFileId());
				query.setParameter("logicalDelete", new Integer(1));
				query.setParameter("dateDeleted", new Date());
				query.setParameter("imageOrder", new Integer(-1));
				result = query.executeUpdate();

				if(result==0){
					logger.info("UPDATE UploadFileEntity has no update Query -> : " + query.toString());
					return false;
				}
			}
			return true;
	}

	@Override
	public boolean updateDateModified(Integer uploadFileId)
			throws PersistenceException {
		Date date = new Date();
		Query query = getEntityManager()
				.createQuery(
						"UPDATE UploadFileEntity u SET u.dateModified=:date WHERE u.uploadFileId=:uploadFileId");
		query.setParameter("uploadFileId", uploadFileId);
		query.setParameter("date", date);
		int result = query.executeUpdate();

		if (result != 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean updateIdDocumentEnts(Integer uploadFileId,
			Integer idDocumentEnts) throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"UPDATE UploadFileEntity u SET u.idDocumentEnts=:idDocumentEnts WHERE u.uploadFileId=:uploadFileId");
		query.setParameter("uploadFileId", uploadFileId);
		query.setParameter("idDocumentEnts", idDocumentEnts);
		int result = query.executeUpdate();

		if (result != 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Integer replaceFiles(Integer oldUploadFileId, Integer newUploadFileId)
			throws PersistenceException {
		Integer modifiedCounter = 0;

		UploadFileEntity oldFile = getEntityManager().find(
				UploadFileEntity.class, oldUploadFileId);
		if (oldFile == null) {
			return 0;
		}

		UploadFileEntity newFile = getEntityManager().find(
				UploadFileEntity.class, newUploadFileId);
		if (newFile == null) {
			return 0;
		}

		oldFile.setDateModified(new Date());
		oldFile.setFilename(newFile.getFilename());
		oldFile.setFileOriginalName(newFile.getFileOriginalName());
		oldFile.setTiledConversionProcess(newFile.getTiledConversionProcess());
		getEntityManager().merge(oldFile);
		modifiedCounter++;
		Query query = getEntityManager().createQuery("DELETE FROM UploadFileEntity WHERE id=:newFileId");
		query.setParameter("newFileId", newUploadFileId);
		query.executeUpdate();

		return modifiedCounter;
	}

	@Override
	public Boolean isOwnerOrPublic(String username, Integer uploadFileId) {
		Query query = getEntityManager()
				.createQuery(
						"select case when (count(u) > 0)  then true else false end "
						+ " from UploadFileEntity u where u.id =:uploadFileId AND u.filePrivacy = 0 OR u.idTblUpload in (select upload from UploadInfoEntity upload where upload.owner =:username)", Boolean.class);
		query.setParameter("username", username);
		query.setParameter("uploadFileId", uploadFileId);
		Boolean result = (Boolean) query.getSingleResult();
		
		return result;
	}

	@Override
	public Integer findFilePrivacy(Integer uploadInfoId) throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"select u.filePrivacy "
						+ " from UploadFileEntity u where u.id =:uploadInfoId");
		query.setParameter("uploadInfoId", uploadInfoId);
		Integer result = (Integer) query.getSingleResult();
		
		return result;
	}
	
	@Override
	public UploadFileEntity findLastUploadFileByUploadInfoId(
			Integer uploadInfoId) throws PersistenceException {

		Query query = getEntityManager().createQuery(
				"FROM UploadFileEntity uploadFile "
				+ "WHERE uploadFile.idTblUpload=:uploadInfoId "
				+ "ORDER BY "
				+ "uploadFile.dateCreated DESC, "
				+ "uploadFile.imageOrder DESC");
		
		query.setParameter("uploadInfoId", uploadInfoId);
		query.setMaxResults(1);

		return (UploadFileEntity) query.getResultList().get(0);
	}
	
	@Transactional(readOnly = false)
	@Override
	public UploadFileEntity createNoImageUploadFile(Integer uploadInfoId) {
		UploadFileEntity uploadFileEntity = new UploadFileEntity();
		
		uploadFileEntity.setFilename(UploadFileEntity.NO_IMAGE);
		uploadFileEntity.setFileOriginalName(UploadFileEntity.NO_IMAGE);
		uploadFileEntity.setIdTblUpload(uploadInfoId);
		uploadFileEntity.setDateCreated(new Date());
		uploadFileEntity.setDateModified(new Date());
		uploadFileEntity.setImageOrder(0);
		uploadFileEntity.setLogicalDelete(0);
		uploadFileEntity.setFilePrivacy(0);
		
		Integer id = insertUploadFile(uploadFileEntity);
		uploadFileEntity.setUploadFileId(id);
				
		return uploadFileEntity;
	}
}