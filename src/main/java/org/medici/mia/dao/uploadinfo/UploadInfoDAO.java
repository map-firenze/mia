package org.medici.mia.dao.uploadinfo;

import java.util.Date;
import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.common.json.complexsearch.AEComplexSearchJson;
import org.medici.mia.common.json.search.AEArchivalSearchJson;
import org.medici.mia.common.json.search.AEWordSearchJson;
import org.medici.mia.common.json.search.AllCountWordSearchJson;
import org.medici.mia.controller.archive.BrowseArchivalLocationJson;
import org.medici.mia.controller.archive.CountAESandDESJson;
import org.medici.mia.controller.genericsearch.AESearchPaginationParam;
import org.medici.mia.dao.Dao;
import org.medici.mia.domain.UploadInfoEntity;

public interface UploadInfoDAO extends Dao<Integer, UploadInfoEntity> {

	public List<UploadInfoEntity> getUpdateByVolumeId(Integer volumeId)
			throws PersistenceException;

	public List<UploadInfoEntity> getUpdateByInsertId(Integer insertId)
			throws PersistenceException;

	public CountAESandDESJson countVolumeAESandDES(Integer volumeId)
			throws PersistenceException;

	public CountAESandDESJson countInsertAESandDES(Integer insertId)
			throws PersistenceException;

	public Integer insertUplodInfo(UploadInfoEntity uplodInfo)
			throws PersistenceException;

	public void updateUpload(Integer uploadId, String aeName, String username)
			throws PersistenceException;

	public List<UploadInfoEntity> getUploadsByOwner(String owner)
			throws PersistenceException;

	public List<UploadInfoEntity> getUploadsByOwnerForArchive(String owner, int page, int perPage)
			throws PersistenceException;

	public Integer countUploadsByOwnerForArchive(String owner)
			throws PersistenceException;

	public List<UploadInfoEntity> getUploadsByComplexSerach(
			AEComplexSearchJson searchReq, AESearchPaginationParam pagParam)
			throws PersistenceException;

	public UploadInfoEntity getUploadById(Integer uploadId)
			throws PersistenceException;

	// the following methods return only the entities that have logicalDelete=0
	public List<UploadInfoEntity> getUploadsByOwnerForArchive(String owner)
			throws PersistenceException;

	public UploadInfoEntity getUploadByIdForArchive(Integer uploadId)
			throws PersistenceException;

	public boolean updateLogicalDelete(Integer uploadInfoId, String username)
			throws PersistenceException;

	public boolean updateUploadInfo(Integer uploadInfoId, String aeName,
			String aeDescription, Integer repInfoId, Integer collInfoId,
			Integer serInfoId, Integer volInfoId, Integer insInfoId,
			String username) throws PersistenceException;

	public int updateUploadInfoPrivacyField(Integer uploadInfoId,
			Integer filePrivacy, String username) throws PersistenceException;

	public boolean updateDateModified(Integer uploadInfoId, String username)
			throws PersistenceException;

	public List<UploadInfoEntity> getUploadsBySerachWords(
			AEWordSearchJson searchReq) throws PersistenceException;

	public List<UploadInfoEntity> getUploadsByArchivalInfo(
			AEArchivalSearchJson searchReq) throws PersistenceException;

	public Integer countUploadsByArchivalInfo(AEArchivalSearchJson searchReq)
			throws PersistenceException;

	public Integer getAllCountUploadInfosBySerachWords(
			AllCountWordSearchJson searchReq) throws PersistenceException;

	public Integer getAllCountUploadInfos() throws PersistenceException;

	public List<UploadInfoEntity> getUploadsByLocationForArchive(
			BrowseArchivalLocationJson locationJson)
			throws PersistenceException;

	public List<UploadInfoEntity> getNews(Date date, int numberOfUpdates)
			throws PersistenceException;

	public List<UploadInfoEntity> getArchivalRelatedToVolume(Integer volumeId)
			throws PersistenceException;

	public List<UploadInfoEntity> getArchivialRelatedToInsert(Integer insertId)
			throws PersistenceException;

	public List<UploadInfoEntity> getUploadByVolumeId(Integer volumeId)
			throws PersistenceException;

	public List<UploadInfoEntity> getUploadByInsertId(Integer insertId)
			throws PersistenceException;

	Integer findPageForFile(Integer uploadId, Integer fileId, Integer perPage) throws PersistenceException;

	public Integer getUploadsCountByComplexSerach(AEComplexSearchJson searchReq)
			throws PersistenceException;

	public List<String> findOwnerById(Integer idTblUpload);
	
	public List<UploadInfoEntity> getNotDeletedUploadsByOwner(String owner)
			throws PersistenceException;
	
	public UploadInfoEntity createNoImageUpload(
			Integer repositoryId, 
			Integer collectionId, 
			Integer volumeId,
			Integer insertId);
}
