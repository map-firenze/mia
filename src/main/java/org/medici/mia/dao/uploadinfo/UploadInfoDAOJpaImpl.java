package org.medici.mia.dao.uploadinfo;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.medici.mia.common.json.complexsearch.AEComplexSearchJson;
import org.medici.mia.common.json.search.AEArchivalSearchJson;
import org.medici.mia.common.json.search.AEWordSearchJson;
import org.medici.mia.common.json.search.AllCountWordSearchJson;
import org.medici.mia.common.util.NativeQueryUtils;
import org.medici.mia.controller.archive.BrowseArchivalLocationJson;
import org.medici.mia.controller.archive.CountAESandDESJson;
import org.medici.mia.controller.genericsearch.AESearchPaginationParam;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.dao.insert.InsertDAO;
import org.medici.mia.dao.volume.VolumeDAO;
import org.medici.mia.domain.InsertEntity;
import org.medici.mia.domain.UploadFileEntity;
import org.medici.mia.domain.UploadInfoEntity;
import org.medici.mia.domain.Volume;
import org.medici.mia.service.genericsearch.ComplexSearchUtils;
import org.medici.mia.service.genericsearch.SearchUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class UploadInfoDAOJpaImpl extends JpaDao<Integer, UploadInfoEntity>
		implements UploadInfoDAO {

	private static final long serialVersionUID = -5796533789041210555L;
	private static final Logger logger = Logger
			.getLogger(ComplexSearchUtils.class);
	
	@Autowired
	VolumeDAO volumeDAO;
	
	@Autowired
	InsertDAO insertDAO;

	@Transactional(readOnly = false)
	@Override
	public Integer insertUplodInfo(UploadInfoEntity uploadInfoEntity)
			throws PersistenceException {
		getEntityManager().persist(uploadInfoEntity);

		return uploadInfoEntity.getUploadInfoId();
	}

	@Override
	public void updateUpload(Integer uploadInfoId, String aeName,
			String username) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"UPDATE UploadInfoEntity e SET e.aeName=:aeName, e.modifiedBy=:modifiedBy, e.dateModified=:dateModified WHERE e.uploadInfoId=:uploadInfoId");
		query.setParameter("uploadInfoId", uploadInfoId);
		query.setParameter("aeName", aeName);
		query.setParameter("modifiedBy", username);
		query.setParameter("dateModified", new Date());
		query.executeUpdate();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UploadInfoEntity> getUpdateByVolumeId(Integer volumeId)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT e FROM UploadInfoEntity e WHERE e.volume =:volumeId AND e.optionalImage = 'spine' AND e.logicalDelete = 0");
		query.setParameter("volumeId", volumeId);

		return (List<UploadInfoEntity>) query.getResultList();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UploadInfoEntity> getUpdateByInsertId(Integer insertId)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT e FROM UploadInfoEntity e WHERE e.insertId =:insertId AND e.optionalImage = 'guardia' AND e.logicalDelete = 0");
		query.setParameter("insertId", insertId);

		return (List<UploadInfoEntity>) query.getResultList();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UploadInfoEntity> getUploadByVolumeId(Integer volumeId)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT e FROM UploadInfoEntity e WHERE e.volume =:volumeId AND e.logicalDelete = 0");
		query.setParameter("volumeId", volumeId);

		return (List<UploadInfoEntity>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UploadInfoEntity> getUploadByInsertId(Integer insertId)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT e FROM UploadInfoEntity e WHERE e.insertId =:insertId AND e.logicalDelete = 0");
		query.setParameter("insertId", insertId);

		return (List<UploadInfoEntity>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UploadInfoEntity> getUploadsByOwner(String owner)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT DISTINCT u FROM UploadInfoEntity u WHERE u.owner =:owner AND u.optionalImage is null");
		query.setParameter("owner", owner);

		return (List<UploadInfoEntity>) query.getResultList();

	}

	@Override
	public UploadInfoEntity getUploadById(Integer uploadId)
			throws PersistenceException {

		UploadInfoEntity upl = getEntityManager().find(UploadInfoEntity.class,
				uploadId);

		return upl;

	}

	// the following method returns only the entities that have logicalDelete=0
	@SuppressWarnings("unchecked")
	@Override
	public List<UploadInfoEntity> getUploadsByOwnerForArchive(String owner)
			throws PersistenceException {
		String desc = "DESC";
		Query query = getEntityManager()
				.createQuery(
						"SELECT DISTINCT u FROM UploadInfoEntity u WHERE u.owner =:owner AND u.logicalDelete=0 AND u.optionalImage is null ORDER BY u.dateModified "
								+ desc);
		query.setParameter("owner", owner);

		return (List<UploadInfoEntity>) query.getResultList();

	}

	// the following method returns only the entities that have logicalDelete=0 and paginates result
	@SuppressWarnings("unchecked")
	@Override
	public List<UploadInfoEntity> getUploadsByOwnerForArchive(String owner, int page, int perPage)
			throws PersistenceException {
		int offset = page * perPage - perPage;
		String desc = "DESC";
		Query query = getEntityManager()
				.createQuery(
						"SELECT DISTINCT u FROM UploadInfoEntity u WHERE u.owner =:owner AND u.logicalDelete=0 AND u.optionalImage is null ORDER BY u.dateModified "
								+ desc);
		query.setParameter("owner", owner);
		query.setFirstResult(offset);
		query.setMaxResults(perPage);

		return (List<UploadInfoEntity>) query.getResultList();

	}

	@Override
	public Integer countUploadsByOwnerForArchive(String owner)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT COUNT(DISTINCT u) FROM UploadInfoEntity u WHERE u.owner =:owner AND u.logicalDelete=0 AND u.optionalImage is null");
		query.setParameter("owner", owner);
		Long res = (Long) query.getSingleResult();
		if(res == null){
			return 0;
		}
		return res.intValue();

	}



	// the following method return the entity only if the entity has
	// logicalDelete=0
	@Override
	public UploadInfoEntity getUploadByIdForArchive(Integer uploadId)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT DISTINCT u FROM UploadInfoEntity u WHERE u.uploadInfoId =:uploadId AND u.logicalDelete=0 AND u.optionalImage is null");
		query.setParameter("uploadId", uploadId);

		return (UploadInfoEntity) query.getResultList();

	}

	// Find Archivial related to Volume
	@SuppressWarnings("unchecked")
	@Override
	public List<UploadInfoEntity> getArchivalRelatedToVolume(Integer volumeId)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"FROM UploadInfoEntity e WHERE e.volume =:volumeId AND e.logicalDelete=0 AND e.optionalImage is null");
		query.setParameter("volumeId", volumeId);

		return (List<UploadInfoEntity>) query.getResultList();

	}

	// Find Archivial related to Volume
	@SuppressWarnings("unchecked")
	@Override
	public List<UploadInfoEntity> getArchivialRelatedToInsert(Integer insertId)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"FROM UploadInfoEntity e WHERE e.insertId =:insertId AND e.logicalDelete=0 AND e.optionalImage is null");
		query.setParameter("insertId", insertId);

		return (List<UploadInfoEntity>) query.getResultList();

	}

	// Count Archivial ad Document related to Volume
	@SuppressWarnings("unchecked")
	@Override
	public CountAESandDESJson countVolumeAESandDES(Integer volumeId)
			throws PersistenceException {

		CountAESandDESJson count = new CountAESandDESJson();
		Query miaDocsWithImg = getEntityManager().createQuery(
				"SELECT DISTINCT(m.documentEntityId)"
				+ " FROM MiaDocumentEntity m"
				+ " INNER JOIN m.uploadFileEntities f"
				+ " WHERE (m.flgLogicalDelete = 0 OR m.flgLogicalDelete is NULL)"
				+ " AND f.idTblUpload != null AND f.idTblUpload IN"
				+ " (SELECT u.uploadInfoId FROM UploadInfoEntity u"
				+ " WHERE u.volume =:volumeId AND (u.optionalImage IS NULL OR u.optionalImage = 'no_image')"
				+ " AND (u.logicalDelete IS NULL or u.logicalDelete != 1))))");

		Query docsFromBia = getEntityManager().createQuery("SELECT i.entryId FROM Document i "
				+ "WHERE i.volume.summaryId = :volumeId "
				+ "AND (i.logicalDelete IS NULL or i.logicalDelete != 1)");

		miaDocsWithImg.setParameter("volumeId", volumeId);
		docsFromBia.setParameter("volumeId", volumeId);

		List<Integer> miaDocsIds = (List<Integer>) miaDocsWithImg.getResultList();
		List<Integer> docsFromBiaIds = (List<Integer>) docsFromBia.getResultList();
		HashSet<Integer> unionDistinctIds = new HashSet<Integer>();
		unionDistinctIds.addAll(miaDocsIds);
		unionDistinctIds.addAll(docsFromBiaIds);
		count.setTotalDEs((Integer) unionDistinctIds.size());
		
		Query miaAes = getEntityManager()
		.createQuery("SELECT COUNT(e)" + " FROM UploadInfoEntity e" + " WHERE e.volume =:volumeId"
				+ " AND (e.logicalDelete != 1 OR e.logicalDelete is NULL)" + " AND e.optionalImage IS NULL");
		miaAes.setParameter("volumeId", volumeId);
		Long val = (Long) miaAes.getSingleResult();
		count.setTotalAEs(val.intValue());
		return count;


	}

	// Count Archivial ad Document related to Insert
	@Override
	public CountAESandDESJson countInsertAESandDES(Integer insertId)
			throws PersistenceException {

		CountAESandDESJson count = new CountAESandDESJson();
		Query query = getEntityManager()
				.createQuery(
						"SELECT COUNT(DISTINCT m)"
								+ " FROM MiaDocumentEntity m"
								+ " INNER JOIN m.uploadFileEntities f"
								+ " WHERE (m.flgLogicalDelete != 1 OR m.flgLogicalDelete is NULL)"
								+ " AND f.idTblUpload != null AND f.idTblUpload IN"
								+ " (SELECT u.uploadInfoId FROM UploadInfoEntity u WHERE u.insertId =:insertId"
								+ " AND (u.logicalDelete IS NULL OR u.logicalDelete != 1)"
								+ " AND (u.optionalImage IS NULL OR u.optionalImage = 'no_image')))");
		query.setParameter("insertId", insertId);
		Long val = (Long) query.getSingleResult();
		count.setTotalDEs(val.intValue());
		query = getEntityManager()
				.createQuery(
						"SELECT COUNT(e) FROM UploadInfoEntity e WHERE e.insertId =:insertId AND (e.logicalDelete = 0 OR e.logicalDelete is NULL)"
								+ "AND e.optionalImage is NULL");
		query.setParameter("insertId", insertId);
		val = (Long) query.getSingleResult();
		count.setTotalAEs(val.intValue());
		return count;

	}

	@Override
	public boolean updateLogicalDelete(Integer uploadInfoId, String username)
			throws PersistenceException {
		Date date = new Date();
		Query query = getEntityManager()
				.createQuery(
						"UPDATE UploadInfoEntity u SET u.logicalDelete=1, u.dateDeleted=:date, u.modifiedBy=:modifiedBy WHERE u.uploadInfoId=:uploadInfoId");
		query.setParameter("uploadInfoId", uploadInfoId);
		query.setParameter("date", date);
		query.setParameter("modifiedBy", username);
		int result = query.executeUpdate();

		if (result != 0) {
			return true;
		} else {
			logger.info("UPDATE UploadInfoEntity has no update / Query -> : "
					+ query.toString());
			return false;
		}
	}

	@Override
	public boolean updateUploadInfo(Integer uploadInfoId, String aeName,
			String aeDescription, Integer repInfoId, Integer collInfoId,
			Integer serInfoId, Integer volInfoId, Integer insInfoId,
			String username) throws PersistenceException {
		Date date = new Date();
		UploadInfoEntity upload = getEntityManager().find(
				UploadInfoEntity.class, uploadInfoId);
		if (upload == null) {
			return false;
		}
		upload.setAeName(aeName);
		upload.setAeDescription(aeDescription);
		upload.setRepository(repInfoId);
		upload.setCollection(collInfoId);
		upload.setSeries(serInfoId);
		upload.setVolume(volInfoId);
		upload.setInsertId(insInfoId);
		upload.setDateModified(date);
		upload.setModifiedBy(username);

		getEntityManager().merge(upload);

		return true;

	}

	@Override
	public int updateUploadInfoPrivacyField(Integer uploadInfoId,
			Integer filePrivacy, String username) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"UPDATE UploadInfoEntity e SET e.filePrivacy=:filePrivacy, e.dateModified=:dateModified, e.modifiedBy=:modifiedBy WHERE e.uploadInfoId=:uploadInfoId");
		query.setParameter("uploadInfoId", uploadInfoId);
		query.setParameter("filePrivacy", filePrivacy);
		query.setParameter("dateModified", new Date());
		query.setParameter("modifiedBy", username);

		return query.executeUpdate();

	}

	@Override
	public boolean updateDateModified(Integer uploadInfoId, String username)
			throws PersistenceException {
		Date date = new Date();
		Query query = getEntityManager()
				.createQuery(
						"UPDATE UploadInfoEntity u SET u.dateModified=:date, u.modifiedBy=:modifiedBy WHERE u.uploadInfoId=:uploadInfoId");
		query.setParameter("uploadInfoId", uploadInfoId);
		query.setParameter("date", date);
		query.setParameter("modifiedBy", username);
		int result = query.executeUpdate();

		if (result != 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<UploadInfoEntity> getUploadsBySerachWords(
			AEWordSearchJson searchReq) throws PersistenceException {

		Query q = prepareSearchQuery(searchReq);
		if (q == null) {
			return null;
		}
		@SuppressWarnings("unchecked")
		List<UploadInfoEntity> entities = q.getResultList();

		return entities;

	}

	@Override
	public List<UploadInfoEntity> getUploadsByComplexSerach(
			AEComplexSearchJson searchReq, AESearchPaginationParam pagParam)
			throws PersistenceException {

		Query q = prepareAEComplexSearchQuery(searchReq, pagParam);
		if (q == null) {
			return null;
		}
		@SuppressWarnings("unchecked")
		List<UploadInfoEntity> entities = q.getResultList();

		return entities;

	}

	@Override
	public Integer getUploadsCountByComplexSerach(
			AEComplexSearchJson searchReq)
			throws PersistenceException {

		Query q = prepareAECountComplexSearchQuery(searchReq);
		if (q == null) {
			return null;
		}

		return ((BigInteger) q.getSingleResult()).intValue();

	}

	@Override
	public List<UploadInfoEntity> getUploadsByArchivalInfo(
			AEArchivalSearchJson searchReq) throws PersistenceException {

		if ((searchReq.getInsertId() == null || searchReq.getInsertId()
				.isEmpty())
				&& (searchReq.getCollectionId() == null || searchReq
						.getCollectionId().isEmpty())
				&& (searchReq.getRepositoryId() == null || searchReq
						.getRepositoryId().isEmpty())
				&& (searchReq.getSeriesId() == null || searchReq.getSeriesId()
						.isEmpty())
				&& (searchReq.getVolumeId() == null || searchReq.getVolumeId()
						.isEmpty())) {
			return new ArrayList<UploadInfoEntity>();
		}

		CriteriaQuery<UploadInfoEntity> q = prepareSearchQuery(searchReq);
		List<UploadInfoEntity> entities = getEntityManager().createQuery(q)
				.getResultList();
		return entities;

	}

	@Override
	public Integer countUploadsByArchivalInfo(AEArchivalSearchJson searchReq)
			throws PersistenceException {

		if ((searchReq.getInsertId() == null || searchReq.getInsertId()
				.isEmpty())
				&& (searchReq.getCollectionId() == null || searchReq
						.getCollectionId().isEmpty())
				&& (searchReq.getRepositoryId() == null || searchReq
						.getRepositoryId().isEmpty())
				&& (searchReq.getSeriesId() == null || searchReq.getSeriesId()
						.isEmpty())
				&& (searchReq.getVolumeId() == null || searchReq.getVolumeId()
						.isEmpty())) {
			return null;
		}
		// as per this bug http://bugs.mysql.com/bug.php?id=81031 we cannot use
		// count()
		CriteriaQuery<UploadInfoEntity> q = prepareSearchQuery(searchReq);
		List<UploadInfoEntity> entities = getEntityManager().createQuery(q)
				.getResultList();

		if (entities == null || entities.isEmpty()) {
			return 0;
		}

		List<UploadInfoEntity> nonDeletedEntities = new ArrayList<UploadInfoEntity>();

		for (UploadInfoEntity entity : entities) {
			if (entity.getLogicalDelete() == null
					|| entity.getLogicalDelete().equals(0)) {
				nonDeletedEntities.add(entity);
			}
		}

		return nonDeletedEntities.size();

	}

	@SuppressWarnings("unchecked")
	@Override
	public Integer getAllCountUploadInfosBySerachWords(
			AllCountWordSearchJson searchReq) throws PersistenceException {

		Query q = prepareSearchQuery(searchReq);
		if (q == null) {
			return null;
		}

		List<Integer> dEIdList = (List<Integer>) q.getResultList();
		if (dEIdList != null && !dEIdList.isEmpty())
			return dEIdList.size();

		// return ((BigInteger) q.getSingleResult()).intValue();
		return 0;

	}

	@Override
	public Integer getAllCountUploadInfos() throws PersistenceException {

		Query q = prepareSearchQueryAll();
		if (q == null) {
			return null;
		}

		return ((BigInteger) q.getSingleResult()).intValue();

	}

	private CriteriaQuery<UploadInfoEntity> prepareSearchQuery(
			AEArchivalSearchJson searchReq) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<UploadInfoEntity> query = cb
				.createQuery(UploadInfoEntity.class);

		Root<UploadInfoEntity> uploadInfoEntityRoot = query
				.from(UploadInfoEntity.class);

		Predicate whereClause = getWhereClause(searchReq, cb,
				uploadInfoEntityRoot);

		query.where(whereClause);
		return query;
	}

	private Predicate getWhereClause(AEArchivalSearchJson searchReq,
			CriteriaBuilder cb, Root<UploadInfoEntity> uploadInfoEntityRoot) {
		Predicate whereClause = null;
		if (searchReq.isEveryoneElsesArchivalEntities()) {
			whereClause = cb.not(cb.equal(uploadInfoEntityRoot.get("owner"),
					searchReq.getOwner()));
		} else if (searchReq.isOnlyMyArchivalEnties()) {
			whereClause = cb.equal(uploadInfoEntityRoot.get("owner"),
					searchReq.getOwner());

		} else {
			whereClause = cb.or(cb.equal(uploadInfoEntityRoot.get("owner"),
					searchReq.getOwner()), cb.not(cb.equal(
					uploadInfoEntityRoot.get("owner"), searchReq.getOwner())));
		}
		if (searchReq.getCollectionId() != null
				&& !searchReq.getCollectionId().isEmpty()) {
			whereClause = cb.and(
					whereClause,
					cb.equal(uploadInfoEntityRoot.get("collection"),
							searchReq.getCollectionId()));
		}
		if (searchReq.getInsertId() != null
				&& !searchReq.getInsertId().isEmpty()) {
			whereClause = cb.and(
					whereClause,
					cb.equal(uploadInfoEntityRoot.get("insertId"),
							searchReq.getInsertId()));
		}
		if (searchReq.getRepositoryId() != null
				&& !searchReq.getRepositoryId().isEmpty()) {
			whereClause = cb.and(
					whereClause,
					cb.equal(uploadInfoEntityRoot.get("repository"),
							searchReq.getRepositoryId()));
		}
		if (searchReq.getSeriesId() != null
				&& !searchReq.getSeriesId().isEmpty()) {
			whereClause = cb.and(
					whereClause,
					cb.equal(uploadInfoEntityRoot.get("series"),
							searchReq.getSeriesId()));
		}
		if (searchReq.getVolumeId() != null
				&& !searchReq.getVolumeId().isEmpty()) {
			whereClause = cb.and(
					whereClause,
					cb.equal(uploadInfoEntityRoot.get("volume"),
							searchReq.getVolumeId()));
		}
		return whereClause;
	}

	private Query prepareSearchQuery(AEWordSearchJson searchReq) {
		Query q = null;

		String queryString = null;
		if (searchReq == null || searchReq.getSearchString() == null
				|| searchReq.getSearchString().trim().isEmpty()) {
			return null;
		}

		queryString = SearchUtils.getQueryString(searchReq);

		if (queryString == null)
			return null;

		q = getEntityManager().createNativeQuery(queryString,
				UploadInfoEntity.class);
		q.setParameter("searchWords", searchReq.getSearchString());
		if (!searchReq.isAllArchivalEntities()) {
			q.setParameter("owner", searchReq.getOwner());
		}

		return q;
	}

	private Query prepareAEComplexSearchQuery(AEComplexSearchJson json,
			AESearchPaginationParam pagParam) {
		Query q = null;

		String queryString = ComplexSearchUtils.getAEComplexQueryString(json);

		if (queryString == null || queryString.equals(""))
			return null;

		queryString = queryString + " ORDER BY " + pagParam.getOrderColumn()
				+ " " + pagParam.getAscOrdDesc();
		logger.info("Query String for the service /search/ae/wordSearch: "
				+ queryString);

		q = getEntityManager().createNativeQuery(queryString,
				UploadInfoEntity.class);
		q.setFirstResult(pagParam.getFirstResult());
		q.setMaxResults(pagParam.getMaxResult());

		if (!json.isAllArchivalEntities()) {
			q.setParameter("owner", json.getOwner());
		}

		return q;
	}

	private Query prepareAECountComplexSearchQuery(AEComplexSearchJson json) {
		Query q = null;

		String queryString = ComplexSearchUtils
				.getAECountComplexQueryString(json);

		if (queryString == null || queryString.equals(""))
			return null;

		q = getEntityManager().createNativeQuery(queryString);

		if (!json.isAllArchivalEntities()) {
			q.setParameter("owner", json.getOwner());
		}

		return q;
	}

	private Query prepareSearchQuery(AllCountWordSearchJson searchReq) {
		Query q = null;

		String queryString = null;

		queryString = ComplexSearchUtils
				.getAllCountAEComplexQueryString(searchReq);

		if (queryString == null)
			return null;

		q = getEntityManager().createNativeQuery(queryString);

		return q;
	}

	private Query prepareSearchQueryAll() {
		Query q = null;

		String queryString = null;

		queryString = SearchUtils.getQueryStringAll();

		if (queryString == null)
			return null;

		q = getEntityManager().createNativeQuery(queryString);

		return q;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UploadInfoEntity> getUploadsByLocationForArchive(
			BrowseArchivalLocationJson locationJson)
			throws PersistenceException {

		String queryString = NativeQueryUtils
				.getGeneralArchiveQuery(locationJson);
		if (queryString == null || queryString.isEmpty()) {
			return null;
		}

		Query query = getEntityManager().createQuery(queryString);
		return (List<UploadInfoEntity>) query.getResultList();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UploadInfoEntity> getNews(Date date, int numberOfUpdates)
			throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"FROM UploadInfoEntity e WHERE e.dateCreated>=:dateCreated OR e.dateModified>=:dateModified order by dateModified desc , dateCreated desc");
		query.setParameter("dateCreated", date);
		query.setParameter("dateModified", date);
		
		query.setMaxResults(numberOfUpdates);

		return (List<UploadInfoEntity>) query.getResultList();

	}

	@Override
	public Integer findPageForFile(Integer uploadId, Integer fileId, Integer perPage) throws PersistenceException {
		try {
			Query query = getEntityManager().createNativeQuery(
			"SELECT CAST(t.rank AS UNSIGNED) FROM (SELECT f.id, @rownum/*'*/:=/*'*/@rownum + 1 AS rank FROM mia.tblUploadedFiles f, (SELECT @rownum/*'*/:=/*'*/0) r WHERE f.logicalDelete != 1 AND f.idTblUpload = :uploadId order by id asc, imageOrder asc) t WHERE id = :fileId"
			);
			query.setParameter("uploadId", uploadId);
			query.setParameter("fileId", fileId);
			BigInteger result = (BigInteger) query.getSingleResult();
			if(result == null) return null;
			return result.intValue() % perPage == 0 ? result.intValue() / perPage : result.intValue() / perPage + 1;
		} catch (PersistenceException ex){
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> findOwnerById(Integer idTblUpload) {
		Query query = getEntityManager()
				.createQuery(
						"SELECT e.owner FROM UploadInfoEntity e WHERE e.uploadInfoId=:idTblUpload");
		query.setParameter("idTblUpload", idTblUpload);
		
		return (List<String>) query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<UploadInfoEntity> getNotDeletedUploadsByOwner(String owner)
			throws PersistenceException {

		String queryString = 
				  "SELECT DISTINCT u FROM UploadInfoEntity u "
				+ "WHERE u.owner = :owner "
				+ "AND u.logicalDelete = 0 "
				+ "ORDER BY u.dateCreated DESC";
		
		Query query = getEntityManager().createQuery(queryString);
		query.setParameter("owner", owner);

		return (List<UploadInfoEntity>) query.getResultList();
	}
	
	@Transactional(readOnly = false)
	@Override
	public UploadInfoEntity createNoImageUpload(
			Integer repositoryId, 
			Integer collectionId, 
			Integer volumeId,
			Integer insertId) {
		
		UploadInfoEntity uploadInfoEntity = new UploadInfoEntity();
		
		UserDetails userDetails = (UserDetails) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();
		
		uploadInfoEntity.setRepository(repositoryId);
		uploadInfoEntity.setCollection(collectionId);
		uploadInfoEntity.setVolume(volumeId);
		uploadInfoEntity.setInsertId(insertId);
		uploadInfoEntity.setAeName("");
		uploadInfoEntity.setAeDescription("");
		uploadInfoEntity.setDateCreated(new Date());
		uploadInfoEntity.setDateModified(new Date());
		uploadInfoEntity.setOwner(userDetails.getUsername());
		uploadInfoEntity.setModifiedBy(userDetails.getUsername());
		uploadInfoEntity.setOptionalImage(UploadFileEntity.NO_IMAGE);
		uploadInfoEntity.setLogicalDelete(0);
		
		Integer id = insertUplodInfo(uploadInfoEntity);
		uploadInfoEntity.setUploadInfoId(id);
	
		incrementsAeCount(volumeId, insertId);
		
		return uploadInfoEntity;
	}
	
	private void incrementsAeCount(Integer volumeId, Integer insertId) {
		// If no insert is specified increment volume count
		if (insertId != null) {
			InsertEntity insertEntity = getInsertDAO().find(insertId);
			insertEntity.setAeCount(insertEntity.getAeCount() + 1);
			getInsertDAO().persist(insertEntity);
		} else { // Otherwise increment insert count 
			Volume volume = getVolumeDAO().find(volumeId);
			volume.setAeCount(volume.getAeCount() + 1);
			getVolumeDAO().persist(volume);
		}
	}
	
	public VolumeDAO getVolumeDAO() {
		return volumeDAO;
	}

	public void setVolumeDAO(VolumeDAO volumeDAO) {
		this.volumeDAO = volumeDAO;
	}

	public InsertDAO getInsertDAO() {
		return insertDAO;
	}

	public void setInsertDAO(InsertDAO insertDAO) {
		this.insertDAO = insertDAO;
	}

}
