package org.medici.mia.dao.repository;

import java.util.List;

import javax.persistence.PersistenceException;

import org.medici.mia.dao.Dao;
import org.medici.mia.domain.RepositoryEntity;


public interface RepositoryDAO extends Dao<Integer, RepositoryEntity> {

	
	public List<RepositoryEntity> findByMatchingName(String name) throws PersistenceException;
	public List<RepositoryEntity> checkByMatchingNameLocation(String name, String location) throws PersistenceException;
	public List<RepositoryEntity> checkByMatchingLocationAbbreviation(String location, String abbreviation) throws PersistenceException;
	public List<RepositoryEntity> findByAbbreviation(String abbreviation) throws PersistenceException;
	public List<RepositoryEntity> findByName(String name) throws PersistenceException;
	public RepositoryEntity findRepoById(Integer id) throws PersistenceException;
	public Integer insertRepository(RepositoryEntity repEntity) throws PersistenceException;
	public List<RepositoryEntity> findAllRepositories() throws PersistenceException;
}
