package org.medici.mia.dao.repository;

import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.RepositoryEntity;
import org.springframework.stereotype.Repository;

@Repository
public class RepositoryDAOJpaImpl extends JpaDao<Integer, RepositoryEntity>
		implements RepositoryDAO {

	private static final long serialVersionUID = -5796533789041210555L;

	/**
	 * @param description
	 * @return List<Country> Countries matching search criteria on field
	 *         description
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<RepositoryEntity> findByMatchingName(String name)
			throws PersistenceException {

		CriteriaBuilder criteriaBuilder = getEntityManager()
				.getCriteriaBuilder();
		CriteriaQuery<RepositoryEntity> criteriaQuery = criteriaBuilder
				.createQuery(RepositoryEntity.class);
		Root RepositoryEntity = criteriaQuery.from(RepositoryEntity.class);
		Path nameAttr = RepositoryEntity.get("repositoryName");
		criteriaQuery.where(criteriaBuilder.like(nameAttr, "%" + name + "%"));
		TypedQuery typedQuery = getEntityManager().createQuery(criteriaQuery);
		return typedQuery.getResultList();
	}

	public List<RepositoryEntity> checkByMatchingNameLocation(String name,
			String location) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT r FROM RepositoryEntity r WHERE r.repositoryName =:name AND r.location =:location");
		query.setParameter("name", name);
		query.setParameter("location", location);

		return (List<RepositoryEntity>) query.getResultList();
	}

	public List<RepositoryEntity> checkByMatchingLocationAbbreviation(
			String location, String abbreviation) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT r FROM RepositoryEntity r WHERE r.location =:location AND r.repositoryAbbreviation =:abbreviation");
		query.setParameter("location", location);
		query.setParameter("abbreviation", abbreviation);

		return (List<RepositoryEntity>) query.getResultList();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<RepositoryEntity> findByAbbreviation(String abbreviation)
			throws PersistenceException {

		CriteriaBuilder criteriaBuilder = getEntityManager()
				.getCriteriaBuilder();
		CriteriaQuery<RepositoryEntity> criteriaQuery = criteriaBuilder
				.createQuery(RepositoryEntity.class);
		Root RepositoryEntity = criteriaQuery.from(RepositoryEntity.class);
		Path nameAttr = RepositoryEntity.get("repositoryAbbreviation");
		criteriaQuery.where(criteriaBuilder.like(nameAttr, abbreviation));
		TypedQuery typedQuery = getEntityManager().createQuery(criteriaQuery);
		return typedQuery.getResultList();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<RepositoryEntity> findByName(String name)
			throws PersistenceException {

		CriteriaBuilder criteriaBuilder = getEntityManager()
				.getCriteriaBuilder();
		CriteriaQuery<RepositoryEntity> criteriaQuery = criteriaBuilder
				.createQuery(RepositoryEntity.class);
		Root RepositoryEntity = criteriaQuery.from(RepositoryEntity.class);
		Path nameAttr = RepositoryEntity.get("repositoryName");
		criteriaQuery.where(criteriaBuilder.like(nameAttr, name));
		TypedQuery typedQuery = getEntityManager().createQuery(criteriaQuery);
		return typedQuery.getResultList();
	}

	@Override
	public RepositoryEntity findRepoById(Integer id)
			throws PersistenceException {
		RepositoryEntity repEntity = getEntityManager().find(
				RepositoryEntity.class, id);
		return repEntity;
	}

	@Override
	public Integer insertRepository(RepositoryEntity repEntity)
			throws PersistenceException {
		getEntityManager().persist(repEntity);

		return repEntity.getRepositoryId();
	}

	@Override
	public List<RepositoryEntity> findAllRepositories()
			throws PersistenceException {

		Query query = getEntityManager().createQuery(
				"SELECT r FROM RepositoryEntity r");

		return (List<RepositoryEntity>) query.getResultList();
	}
}
