package org.medici.mia.dao.discovery;

import java.util.List;

import org.medici.mia.dao.Dao;
import org.medici.mia.domain.Discovery;
import org.medici.mia.domain.DiscoveryStatus;

public interface DiscoveryDao extends Dao<Integer, Discovery> {
	
	public List<Discovery> findAll();

	public Discovery addNewDiscovery(Discovery discovery);
	
	public List<Discovery> findDiscoveries(String searchText, Integer limit, Integer offset);
	
	public List<Discovery> findUserDiscoveries(String account);
	
	public List<Discovery> findDiscoveries(DiscoveryStatus status, int limit, int offset);
	
	public int count(DiscoveryStatus status);
}
