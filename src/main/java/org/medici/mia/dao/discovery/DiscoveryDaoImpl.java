package org.medici.mia.dao.discovery;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.Discovery;
import org.medici.mia.domain.DiscoveryStatus;
import org.springframework.stereotype.Repository;

@Repository
public class DiscoveryDaoImpl extends JpaDao<Integer, Discovery> implements DiscoveryDao {

	private static final long serialVersionUID = 7124335030902428320L;

	@Override
	public Discovery addNewDiscovery(Discovery discovery) {
		getEntityManager().persist(discovery);
		return discovery;
	}
	
	@Override
	public List<Discovery> findDiscoveries( String searchText, Integer limit, Integer offset ) {
		StringBuilder query = new StringBuilder()
								.append("SELECT d FROM Discovery d ")
								.append("WHERE  d.logicalDelete = 0 ")
								.append("AND status != :status");
		
		if( StringUtils.isBlank(searchText) ) {
			return getEntityManager()
					.createQuery(query.toString(), Discovery.class)
					.setParameter("status", DiscoveryStatus.PUBLISHED)
					.setFirstResult(offset)
					.setMaxResults(limit)
					.getResultList();
		}
		
		query.append("AND d.title like :searchText ");
		
		return getEntityManager()
				.createQuery(query.toString(), Discovery.class)
				.setParameter("searchText", "%" + searchText + "%")
				.setParameter("status", DiscoveryStatus.PUBLISHED)
				.setFirstResult(offset)
				.setMaxResults(limit)
				.getResultList();
	}
	
	@Override
	public List<Discovery> findAll() {
		return getEntityManager()
				.createQuery("SELECT d FROM Discovery d WHERE d.logicalDelete = 0", Discovery.class)
				.getResultList();
	}
	
	@Override
	public List<Discovery> findUserDiscoveries(String account) {
		return getEntityManager()
				.createQuery("SELECT d FROM Discovery d WHERE d.logicalDelete = 0 AND d.document.createdBy = :account", Discovery.class)
				.setParameter("account", account)
				.getResultList();
	}
	
	@Override
	public int count(DiscoveryStatus status) {
		if( status == null ) {
			return ((Long)getEntityManager()
					.createQuery("SELECT COUNT(d) FROM Discovery d WHERE d.logicalDelete = 0 AND d.status != :status")
					.setParameter("status", DiscoveryStatus.PUBLISHED)
					.getSingleResult()).intValue();
		}
		
		return ((Long)getEntityManager()
				.createQuery("SELECT COUNT(d) FROM Discovery d WHERE d.logicalDelete = 0 AND d.status = :status")
				.setParameter("status", status)
				.getSingleResult()).intValue();
	}
	
	@Override
	public List<Discovery> findDiscoveries(DiscoveryStatus status, int limit, int offset ) {
		return getEntityManager()
				.createQuery("SELECT d FROM Discovery d WHERE d.logicalDelete = 0 AND status = :status", Discovery.class)
				.setParameter("status", status)
				.setFirstResult(offset)
				.setMaxResults(limit)
				.getResultList();
	}

}
