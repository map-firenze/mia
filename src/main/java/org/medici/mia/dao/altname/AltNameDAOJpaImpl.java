/*
 * AltNameDAOJpaImpl.java
 * 
 * Developed by Medici Archive Project (2010-2012).
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.dao.altname;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.medici.mia.common.json.ModifyPersonJson;
import org.medici.mia.common.json.search.PeopleWordSearchJson;
import org.medici.mia.dao.JpaDao;
import org.medici.mia.domain.AltName;
import org.medici.mia.domain.AltName.NameType;
import org.medici.mia.service.genericsearch.SearchUtils;
import org.springframework.stereotype.Repository;

@Repository
public class AltNameDAOJpaImpl extends JpaDao<Integer, AltName> implements
		AltNameDAO {

	/**
	 * 
	 * If a serializable class does not explicitly declare a serialVersionUID,
	 * then the serialization runtime will calculate a default serialVersionUID
	 * value for that class based on various aspects of the class, as described
	 * in the Java(TM) Object Serialization Specification. However, it is
	 * strongly recommended that all serializable classes explicitly declare
	 * serialVersionUID values, since the default serialVersionUID computation
	 * is highly sensitive to class details that may vary depending on compiler
	 * implementations, and can thus result in unexpected InvalidClassExceptions
	 * during deserialization. Therefore, to guarantee a consistent
	 * serialVersionUID value across different java compiler implementations, a
	 * serializable class must declare an explicit serialVersionUID value. It is
	 * also strongly advised that explicit serialVersionUID declarations use the
	 * private modifier where possible, since such declarations apply only to
	 * the immediately declaring class--serialVersionUID fields are not useful
	 * as inherited members.
	 */
	private static final long serialVersionUID = 617902723399766439L;

	@SuppressWarnings("unchecked")
	@Override
	public AltName findAltNamePerson(Integer personId, Integer nameId)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"from AltName where person.personId=:personId and nameId=:nameId");
		query.setParameter("personId", personId);
		query.setParameter("nameId", nameId);

		List<AltName> result = query.getResultList();

		if (result.size() == 0) {
			return null;
		} else {
			return result.get(0);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public String modifyPersonName(ModifyPersonJson modifyPersonJson)
			throws PersistenceException {

		// AltName altName = getEntityManager().find(AltName.class,
		// modifyPersonJson.getOldPersonName().getAltNameId());

		Query query = getEntityManager()
				.createQuery(
						"SELECT a from AltName a where a.person.personId=:personId and a.nameId=:nameId and a.altName=:altName");
		query.setParameter("personId", modifyPersonJson.getPersonId());
		query.setParameter("nameId", modifyPersonJson.getOldPersonName()
				.getAltNameId());
		query.setParameter("altName", modifyPersonJson.getOldPersonName()
				.getAltName());

		List<AltName> result = query.getResultList();

		if (result == null || result.size() == 0) {
			return "The personId or the oldPerson Name received from FE is not the same as found in DB. Person name is not modified";
		} else {

			AltName altName = (AltName) result.get(0);
			altName.setAltName(modifyPersonJson.getNewPersonName().getAltName());
			altName.setNameType(modifyPersonJson.getNewPersonName()
					.getNameType());
			getEntityManager().merge(altName);
			return "Person name is Modified.";
		}

	}

	@Override
	public Integer insertAltName(AltName altName) throws PersistenceException {

		Query query = getEntityManager()
				.createQuery(
						"SELECT a from AltName a where a.person.personId=:personId and a.altName=:altName");
		query.setParameter("personId", altName.getPerson().getPersonId());
		query.setParameter("altName", altName.getAltName());

		List<AltName> result = query.getResultList();

		if (result == null || result.size() == 0) {
			getEntityManager().persist(altName);
			return 1;
		} else {
			return 0;
		}

	}

	@Override
	public Integer deleteAltName(Integer nameId, Integer personId)
			throws PersistenceException {
		Query query = getEntityManager()
				.createQuery(
						"SELECT a from AltName a where a.person.personId=:personId and a.nameId=:nameId");
		query.setParameter("personId", personId);
		query.setParameter("nameId", nameId);

		List<AltName> result = query.getResultList();

		if (result == null || result.size() == 0) {
			return 0;
		} else {
			getEntityManager().remove((AltName) result.get(0));
			return 1;
		}

	}

	@Override
	public List<AltName> findAltNamesByWordSearch(PeopleWordSearchJson searchReq) {
		List<AltName> toret = new ArrayList<AltName>();	
		
		
			CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<AltName> query = cb.createQuery(AltName.class);

			Root<AltName> altNameRoot = query.from(AltName.class);

			Predicate whereClause = SearchUtils.getWhereClause(searchReq, cb,
					altNameRoot);

			query.where(whereClause);

			query.groupBy(altNameRoot.get("person"));

			toret = getEntityManager().createQuery(query).getResultList();
		
		
		return toret;
	}
	
	public AltName findAltNameByNametype(Integer personId)
			throws PersistenceException{
		
		Query query = getEntityManager()
				.createQuery(
						"SELECT a from AltName a where a.person.personId=:personId and a.nameType = :nameType");
		query.setParameter("personId", personId);
		query.setParameter("nameType", NameType.SearchName.toString());

		List<AltName> result = query.getResultList();

		if (result == null || result.size() == 0) {
			return null;
		} else {
			
			return result.get(0);
		}

		
	}
}
