/*
 * SitemapGeneratorJob.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.scheduler;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.joda.time.DateTime;
import org.medici.mia.common.util.DateUtils;
import org.medici.mia.domain.Sitemap;
import org.medici.mia.domain.Sitemap.ChangeFrequency;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.docbase.DocBaseService;
import org.medici.mia.service.geobase.GeoBaseService;
import org.medici.mia.service.peoplebase.PeopleBaseService;
import org.medici.mia.service.sitemap.SitemapService;
import org.medici.mia.service.volbase.VolBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * This class implements the scheduler to generates siteMaps urls.<br>
 * 
 * @author Lorenzo Pasquinelli (<a href=mailto:l.pasquinelli@gmail.com>l.pasquinelli@gmail.com</a>)
 *
 */
@Transactional(readOnly=true)
public class SitemapGeneratorJob {
	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private SitemapService sitemapService;
	
	@Autowired
	private DocBaseService docBaseService;
	
	@Autowired
	private PeopleBaseService peopleBaseService;

	@Autowired
	private VolBaseService volBaseService;
	
	@Autowired
	private GeoBaseService geoBaseService;
	
	/**
	 * Scheduled 24H after last execution @Scheduled(fixedDelay = 86400000)
	 */
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	@Scheduled(fixedDelay = 86400000)
	public void execute() {
		long start = System.currentTimeMillis();
		logger.info("SitemapGeneratorJob starts at " + 
				DateUtils.getMYSQLDateTime(new DateTime(start)));
		
		MDC.put("account", "threadsitemap");
		try {
			getSitemapService().deleteSitemapIndex();
			getSitemapService().deleteSitemaps();
			
			List<Sitemap> sitemaps = generateSitemaps();
			
 			getSitemapService().storeSitemaps(sitemaps);

 			getSitemapService().generateSitemapIndex();
 			
		} catch (ApplicationThrowable ath) {
			logger.error("Error during creating site map informations.");
		}
		
		long end = System.currentTimeMillis();
		logger.info("SitemapGeneratorJob ends at " + 
				DateUtils.getMYSQLDateTime(new DateTime(end)));
		logger.info("SitemapGeneratorJob work time: " + 
				(new Float(end - start) / 1000));
	}
	
	private List<Sitemap> generateSitemaps() {
		List<Sitemap> sitemaps = new ArrayList<Sitemap>();
		
		sitemaps.addAll(docBaseService.generateDocumentsSitemaps(
				ChangeFrequency.DAILY, 0.5));
		
		sitemaps.addAll(peopleBaseService.generatePeopleSitemaps(
				ChangeFrequency.DAILY, 0.5));
		
		sitemaps.addAll(volBaseService.generateVolumesSitemaps(
				ChangeFrequency.DAILY, 0.5));
		
		sitemaps.addAll(geoBaseService.generatePlacesSitemaps(
				ChangeFrequency.DAILY, 0.5));
		
		return sitemaps;
	}

	/**
	 * 
	 * @param sitemapService
	 */
	public void setSitemapService(SitemapService sitemapService) {
		this.sitemapService = sitemapService;
	}

	/**
	 * 
	 * @return
	 */
	public SitemapService getSitemapService() {
		return sitemapService;
	}
}
