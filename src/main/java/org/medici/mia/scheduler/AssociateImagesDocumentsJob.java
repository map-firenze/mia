/*
 * AssociateImagesDocumentsJob.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.scheduler;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.medici.mia.common.property.ApplicationPropertyManager;
import org.medici.mia.common.util.DateUtils;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.miadoc.MiaDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly=false)
public class AssociateImagesDocumentsJob {

	private static final Logger log = Logger.getLogger(AssociateImagesDocumentsJob.class);
	
	@Autowired
	private MiaDocumentService miaDocumentService;
	
	@Transactional(readOnly=false)
	//@Scheduled(cron="* 0 * * *")
	@Scheduled(fixedRate=300000)
	public void execute() {
		long start = System.currentTimeMillis();
		String installationName = ApplicationPropertyManager
				.getApplicationProperty("project.name");
		log.info("AssociateImagesDocumentsJob starts at " + DateUtils.getMYSQLDateTime(new DateTime(start)));
		String miaProject="Mia";
		try {
			if (installationName.equals(miaProject))
				{
				log.info("Associate images to documents");
				getMiaDocumentService().associateImagesDocuments();
				log.info("Update privacy documents");
				getMiaDocumentService().updateDocumentsPrivacy();
				log.info("Remove private images from documents");
				getMiaDocumentService().removePrivateImagesFromDocuments();
				}
			else {
				log.info("WARNING: This is not MIA but a different project. Association of images is not allowed!");
			}
		} catch (ApplicationThrowable ath) {
			log.error("Errore Elabrate AssociateImagesDocumentsJob ", ath);
		} finally {
			long end = System.currentTimeMillis();
			log.info("Project Name: " + installationName);
			log.info("AssociateImagesDocumentsJob ends at " + DateUtils.getMYSQLDateTime(new DateTime(end)));
			log.info("AssociateImagesDocumentsJob work time: " + (new Float(end - start) / 1000));
		}
	}
	
	public MiaDocumentService getMiaDocumentService() {
		return miaDocumentService;
	}
}
