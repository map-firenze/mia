/*
 * UpdatePeopleDocumentsCountJob.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.scheduler;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.medici.mia.common.util.DateUtils;
import org.medici.mia.dao.people.PeopleDAO;
import org.medici.mia.exception.ApplicationThrowable;
import org.medici.mia.service.peoplebase.PeopleBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Fabio Solfato
 *
 */
@Transactional(readOnly = false)
public class UpdatePeopleDocumentsCountJob {
	private final int OFFSET = 1; // does one record at a time
	private final int DELAY = 300; // after the updates it waits (milleseconds) before doing the next update

	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private PeopleBaseService peopleBaseService;
	
	@Autowired
	private PeopleDAO peopleDAO;

	@Transactional(readOnly = false)
	@Scheduled(cron = "0 0 0 1/2 * ?") // every two days
	public void execute() {
		try {
			long start = System.currentTimeMillis();
			logger.info("UpdatePeopleDocumentsCountJob starts at " + DateUtils.getMYSQLDateTime(new DateTime(start)));
			
			int peopleCount = getPeopleDAO().getAllCountPeople();
//			int peopleCount = 10;
			int index = 0;
			
			while (index <= peopleCount){

				List<Integer> peopleIds = getPeopleDAO().getIdsFromTo(index, OFFSET);

				getPeopleBaseService().updatePeopleDocumentsCount(peopleIds);
				
				index += OFFSET;
								
				Thread.sleep(DELAY);
			}
			
			long end = System.currentTimeMillis();
			logger.info("UpdatePeopleDocumentsCountJob ends at " + DateUtils.getMYSQLDateTime(new DateTime(end)));
			logger.info("UpdatePeopleDocumentsCountJob work time: " + (new Float(end - start) / 1000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ApplicationThrowable ath) {
			logger.error("Error during update people documents count");
		}
	}
	
	public PeopleBaseService getPeopleBaseService() {
		return peopleBaseService;
	}

	public void setPeopleBaseService(PeopleBaseService peopleBaseService) {
		this.peopleBaseService = peopleBaseService;
	}
	
	public PeopleDAO getPeopleDAO() {
		return peopleDAO;
	}

	public void setPeopleDAO(PeopleDAO peopleDAO) {
		this.peopleDAO = peopleDAO;
	}
}
