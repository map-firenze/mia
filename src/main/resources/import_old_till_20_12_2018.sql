

-- ADD REPOSITORY ASF and MEDICEO DEL PRINCIPATO
INSERT INTO `tblGoogleLocations` (`gplace_id`, `city`, `country`) VALUES
	('ChIJrdbSgKZWKhMRAyrH7xd51ZM', 'Florence', 'Italy');
INSERT INTO `tblRepositories` (`id`, `location`, `repositoryName`, `repositoryAbbreviation`, `repositoryDescription`) VALUES
	(1, 'ChIJrdbSgKZWKhMRAyrH7xd51ZM', 'Archivio di Stato di Firenze', 'ASFI', '');
INSERT INTO `tblCollections` (`id`, `repository`, `collectionName`, `collectionAbbreviation`, `collectionDescription`) VALUES
	(1, 1, 'Mediceo del Principato', 'MDP', '');

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='01/05/2016' WHERE  `id`='developement.database.update.date';

-- Delete rows in tblDocumentFields not necessary for FE

DELETE FROM `mia`.`tblDocumentFields` WHERE  `documentFieldId`=5;
DELETE FROM `mia`.`tblDocumentFields` WHERE  `documentFieldId`=10;
DELETE FROM `mia`.`tblDocumentFields` WHERE  `documentFieldId`=15;
DELETE FROM `mia`.`tblDocumentFields` WHERE  `documentFieldId`=19;
DELETE FROM `mia`.`tblDocumentFields` WHERE  `documentFieldId`=25;
DELETE FROM `mia`.`tblDocumentFields` WHERE  `documentFieldId`=29;
DELETE FROM `mia`.`tblDocumentFields` WHERE  `documentFieldId`=83;
DELETE FROM `mia`.`tblDocumentFields` WHERE  `documentFieldId`=89;
DELETE FROM `mia`.`tblDocumentFields` WHERE  `documentFieldId`=94;

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='02/05/2016' WHERE  `id`='developement.database.update.date';

ALTER TABLE `tblDocumentEnts`
	CHANGE COLUMN `placeOfOriginUnsure` `placeOfOriginUnsure` VARCHAR(2) NULL DEFAULT 'S' COMMENT 'S=sure, U=unsure' AFTER `placeOfOrigin`;

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='04/05/2016' WHERE  `id`='developement.database.update.date';

--
UPDATE `mia`.`tblApplicationProperty` SET `value`='Mia' WHERE  `id`='project.name';
UPDATE `mia`.`tblApplicationProperty` SET `value`='MIA User Account Locked' WHERE  `id`='mail.lockedUser.subject';
UPDATE `mia`.`tblApplicationProperty` SET `value`='Your login details for MIA' WHERE  `id`='mail.resetUserPassword.subject';

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='11/05/2016' WHERE  `id`='developement.database.update.date';

ALTER TABLE `tblUploads` ADD COLUMN `aeDescription` LONGTEXT NOT NULL AFTER `aeName`;

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='19/05/2016' WHERE  `id`='developement.database.update.date';

-- Dump of table mia.tblDocumentsEntsRefToPeople
ALTER TABLE `tblDocumentEnts` DROP COLUMN `referredToPeople`;

CREATE TABLE IF NOT EXISTS `tblDocumentEntsRefToPeople` (
	`documentId` INT(11) NOT NULL,
	`peopleId` INT(11) NOT NULL,
	PRIMARY KEY (`documentId`, `peopleId`),
	INDEX `docId_idx` (`documentId`),
	INDEX `peopId_idx` (`peopleId`),
	CONSTRAINT `FK1_documentId` FOREIGN KEY (`documentId`) REFERENCES `tblDocumentEnts` (`documentEntityId`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `FK1_peopleId` FOREIGN KEY (`peopleId`) REFERENCES `tblPeople` (`PERSONID`) ON UPDATE NO ACTION ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='24/05/2016' WHERE  `id`='developement.database.update.date';

-- Dump of table mia.tblDocumentsEntsRefToPeople
ALTER TABLE `tblDocumentEnts` DROP COLUMN `relatedDocuments`;

CREATE TABLE IF NOT EXISTS `tblDocumentEntsRelatedDocs` (
	`documentId` INT(11) NOT NULL,
	`documentRelatedId` INT(11) NOT NULL,
	PRIMARY KEY (`documentId`, `documentRelatedId`),
	INDEX `docId_idx` (`documentId`),
	INDEX `docRelatedId_idx` (`documentRelatedId`),
	CONSTRAINT `FK2_documentId` FOREIGN KEY (`documentId`) REFERENCES `tblDocumentEnts` (`documentEntityId`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `FK2_documentRelatedId` FOREIGN KEY (`documentRelatedId`) REFERENCES `tblDocumentEnts` (`documentEntityId`) ON UPDATE NO ACTION ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='27/05/2016' WHERE  `id`='developement.database.update.date';

-- Dump of table bibliographic references
ALTER TABLE `tblDocumentEnts`
	DROP COLUMN `bibliographicReferences`;

CREATE TABLE IF NOT EXISTS `tblBiblioRef` (
	`idRef` INT(11) NOT NULL AUTO_INCREMENT,
	`idDoc` INT(11) NOT NULL,
	`name` VARCHAR(50) NULL,
	`permalink` VARCHAR(50) NULL,
	PRIMARY KEY (`idRef`),
	CONSTRAINT `FK__tbldocumentents` FOREIGN KEY (`idDoc`) REFERENCES `tblDocumentEnts` (`documentEntityId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='30/05/2016' WHERE  `id`='developement.database.update.date';

-- Dump of table tblLanguages
CREATE TABLE IF NOT EXISTS `tblLanguages` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`language` VARCHAR(50) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mia`.`tblLanguages` (`language`) VALUES ('Italian');
INSERT INTO `mia`.`tblLanguages` (`language`) VALUES ('Spanish');
INSERT INTO `mia`.`tblLanguages` (`language`) VALUES ('French');
INSERT INTO `mia`.`tblLanguages` (`language`) VALUES ('German');
INSERT INTO `mia`.`tblLanguages` (`language`) VALUES ('English');
INSERT INTO `mia`.`tblLanguages` (`language`) VALUES ('Latin');
INSERT INTO `mia`.`tblLanguages` (`language`) VALUES ('Classical Greek');
INSERT INTO `mia`.`tblLanguages` (`language`) VALUES ('Portugese');
INSERT INTO `mia`.`tblLanguages` (`language`) VALUES ('Flamish');
INSERT INTO `mia`.`tblLanguages` (`language`) VALUES ('Turkish');
INSERT INTO `mia`.`tblLanguages` (`language`) VALUES ('Arabic');
INSERT INTO `mia`.`tblLanguages` (`language`) VALUES ('Persian');
INSERT INTO `mia`.`tblLanguages` (`language`) VALUES ('Hebrew');
INSERT INTO `mia`.`tblLanguages` (`language`) VALUES ('Russian');
INSERT INTO `mia`.`tblLanguages` (`language`) VALUES ('Polish');
INSERT INTO `mia`.`tblLanguages` (`language`) VALUES ('Danish');
INSERT INTO `mia`.`tblLanguages` (`language`) VALUES ('Other');

-- Dump of table tblDocumentEntsLanguages
CREATE TABLE IF NOT EXISTS `tblDocumentEntsLanguages` (
	`docId` INT(11) NOT NULL,
	`langId` INT(11) NOT NULL,
	PRIMARY KEY (`docId`, `langId`),
	CONSTRAINT `FK_docId` FOREIGN KEY (`docId`) REFERENCES `tblDocumentEnts` (`documentEntityId`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `FK_langId` FOREIGN KEY (`langId`) REFERENCES `tblLanguages` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `tblDocumentEnts` DROP COLUMN `languages`;

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='31/05/2016' WHERE  `id`='developement.database.update.date';

CREATE TABLE IF NOT EXISTS `tblDocTranscriptions` (
`docTranscriptionId` int(11) NOT NULL AUTO_INCREMENT,
`documentEntityId` int(11) DEFAULT NULL,
`transcription` varchar(2083) NOT NULL,
PRIMARY KEY (`docTranscriptionId`),
KEY `documentEntityId_idx` (`documentEntityId`),
CONSTRAINT `documentEntityId` FOREIGN KEY (`documentEntityId`) REFERENCES  `tblDocumentEnts` (`documentEntityId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='07/06/2016' WHERE  `id`='developement.database.update.date';

DROP TABLE `tblDocTranscriptions`;

CREATE TABLE `tblDocTranscriptions` (
	`docTranscriptionId` INT(11) NOT NULL AUTO_INCREMENT,
	`documentEntityId` INT(11) NOT NULL,
	`transcription` VARCHAR(2083) NOT NULL,
	PRIMARY KEY (`docTranscriptionId`),
	INDEX `FKEFD68D5BD387605C` (`documentEntityId`),
	CONSTRAINT `FKEFD68D5BD387605C` FOREIGN KEY (`documentEntityId`) REFERENCES `tblDocumentEnts` (`documentEntityId`)
)

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='13/06/2016' WHERE  `id`='developement.database.update.date';


UPDATE `mia`.`tblDocumentTypologies` SET `typologyName`='Libro di Entrate e di Uscite' WHERE  `typologyName`='Libro di Entrata e di Uscita';

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='01/07/2016' WHERE  `id`='developement.database.update.date';


ALTER TABLE `tblUploads` ADD FULLTEXT INDEX `aeName` (`aeName`);
ALTER TABLE `tblUploads` ADD FULLTEXT INDEX `aeDescription` (`aeDescription`);
ALTER TABLE `tblUploads` ADD FULLTEXT INDEX `aeName_aeDescription` (`aeName`, `aeDescription`);

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='05/07/2016' WHERE  `id`='developement.database.update.date';

-- createof table mia.tblDocumentTopicPlace
CREATE TABLE IF NOT EXISTS `tblDocumentTopicPlace` (
  `documentEntityId` int(11) NOT NULL,
  `placeId` int(11) NOT NULL,
  `topicListId` int(11) NOT NULL, 
  PRIMARY KEY (`documentEntityId`,`placeId`, `topicListId`),
  KEY `document_idx` (`documentEntityId`),
  KEY `place_idx` (`placeId`),
  KEY `topic_idx` (`topicListId`),
  CONSTRAINT `documentEntity` FOREIGN KEY (`documentEntityId`) REFERENCES `tblDocumentEnts` (`documentEntityId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `place` FOREIGN KEY (`placeId`) REFERENCES `tblPlaces` (`PLACEALLID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `topic` FOREIGN KEY (`topicListId`) REFERENCES `tblTopicsList` (`TOPICID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='29/07/2016' WHERE  `id`='developement.database.update.date';


INSERT INTO `mia`.`tblDocumentTypologies` (`type`, `categoryName`, `typologyName`, `typologyHelp`) VALUES ('Undefined', 'Undefined', 'Undefined', 'TODO');

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='29/07/2016' WHERE  `id`='developement.database.update.date';

CREATE TABLE IF NOT EXISTS `tblUploadfilesFolios` (
  `folioId` int(11) NOT NULL,
  `uploadedFileId` int(11) NOT NULL,
  `folioNumber` varchar(45) DEFAULT NULL,
  `rectoverso` varchar(45) DEFAULT NULL COMMENT 'three possible values R/V/N = recto, verso, notapplicable',
  PRIMARY KEY (`uploadedFileId`,`folioId`),
  KEY `uploadedFileId_idx` (`uploadedFileId`), 
  CONSTRAINT `FK_uploadedFileId` FOREIGN KEY (`uploadedFileId`) REFERENCES `tblUploadedFiles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='29/09/2016' WHERE  `id`='developement.database.update.date';

SET foreign_key_checks = 0;
DROP TABLE IF EXISTS tblDocumentFolios;
SET foreign_key_checks = 1;

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='30/09/2016' WHERE  `id`='developement.database.update.date';

RENAME TABLE tblUploadfilesFolios TO tblUploadFilesFolios;

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='03/19/2016' WHERE  `id`='developement.database.update.date';

ALTER TABLE `tblBiblioRef` CHANGE COLUMN `permalink` `permalink` VARCHAR(2083) NULL DEFAULT NULL AFTER `name`;

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='04/10/2016' WHERE  `id`='developement.database.update.date';

-- START 
ALTER TABLE `tblDocTranscriptions` ALTER `transcription` DROP DEFAULT;
ALTER TABLE `tblDocTranscriptions` CHANGE COLUMN `transcription` `transcription` LONGTEXT NULL AFTER `documentEntityId`;

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='10/12/2016' WHERE  `id`='developement.database.update.date';

-- START
ALTER TABLE `tblImages`	ADD INDEX `volNumIdx` (`volNum`);
ALTER TABLE `tblImages` ADD INDEX `volLetExtIdx` (`volLetExt`);
ALTER TABLE `tblImages` ADD INDEX `imageOrderIdx` (`imageOrder`);

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='16/12/2016' WHERE  `id`='developement.database.update.date';

-- New table for Annotation
CREATE TABLE IF NOT EXISTS `tblFileAnnotations` (
	`annotationId` INT(11) NOT NULL AUTO_INCREMENT,
	`dateCreated` DATETIME NULL DEFAULT NULL,
	`h` DOUBLE NOT NULL,
	`lastUpdate` DATETIME NULL DEFAULT NULL,
	`text` LONGTEXT NULL,
	`title` VARCHAR(255) NULL DEFAULT NULL,
	`type` VARCHAR(15) NOT NULL,
	`w` DOUBLE NOT NULL,
	`x` DOUBLE NOT NULL,
	`y` DOUBLE NOT NULL,
	`fileId` INT(11) NULL DEFAULT NULL,
	`account` VARCHAR(50) NULL DEFAULT NULL,
	`topicId` INT(11) NULL DEFAULT NULL,
	`logicalDelete` TINYINT(4) NOT NULL DEFAULT '0',
	`rgbColor` VARCHAR(255) NULL DEFAULT NULL,
	`transcribed` BIT(1) NULL DEFAULT NULL,
	`visible` TINYINT(4) NULL DEFAULT '1',
	`exportedTo` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`annotationId`),
	INDEX `FKFB0C086A4C915AD` (`account`),
	INDEX `FKFB0C086532DE72D` (`topicId`),
	INDEX `FKFB0C08679A2B452` (`exportedTo`),
	INDEX `tblUploadedFiles_ibfk_2` (`fileId`),
	CONSTRAINT `FKFB0C086532DE72E` FOREIGN KEY (`topicId`) REFERENCES `tblForumTopic` (`topicId`),
	CONSTRAINT `FKFB0C08679A2B453` FOREIGN KEY (`exportedTo`) REFERENCES `tblAnnotations` (`annotationId`),
	CONSTRAINT `FK_tblFileAnnotations_tblUser` FOREIGN KEY (`account`) REFERENCES `tblUser` (`account`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `tblUploadedFiles_ibfk_2` FOREIGN KEY (`fileId`) REFERENCES `tblUploadedFiles` (`id`)
)
ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='22/02/2017' WHERE  `id`='developement.database.update.date';



-- DB SYNCRONIZED TO DATE:
UPDATE `mia`.`tblApplicationProperty` SET `value`='22/02/2017' WHERE  `id`='developement.database.update.date';

INSERT INTO `tblApplicationProperty` (`id`, `help`, `value`) VALUES ('resume.person.path', 'Directory for user CV', '/data/resume');

ALTER TABLE `tblUser` ADD COLUMN `resumeName` varchar(255);

UPDATE `mia`.`tblApplicationProperty` SET `value`='17/03/2017' WHERE  `id`='developement.database.update.date';

ALTER TABLE `tblDocumentEnts` ADD FULLTEXT INDEX `transcription` (`transcription`);
ALTER TABLE `tblDocumentEnts` ADD FULLTEXT INDEX `generalNotesSynopsis` (`generalNotesSynopsis`);
ALTER TABLE `tblDocumentEnts` ADD FULLTEXT INDEX `transcription_generalNotesSynopsis` (`transcription`, `generalNotesSynopsis`);

UPDATE `mia`.`tblApplicationProperty` SET `value`='23/04/2017' WHERE  `id`='developement.database.update.date';

ALTER TABLE `tblDocTranscriptions` ADD FULLTEXT INDEX `transcription` (`transcription`);
UPDATE `mia`.`tblApplicationProperty` SET `value`='25/04/2017' WHERE  `id`='developement.database.update.date';

ALTER TABLE `tblUploadFilesFolios` ADD COLUMN `noNumb` tinyint(2) DEFAULT NULL;
UPDATE `mia`.`tblApplicationProperty` SET `value`='28/06/2017' WHERE  `id`='developement.database.update.date';

INSERT INTO `mia`.`tblApplicationProperty` (`id`, `help`, `value`) VALUES ('last.import.from.bia', 'date of last import from BIA', '2017-01-01 00:00:00');
UPDATE `mia`.`tblApplicationProperty` SET `value`='25/10/2017' WHERE  `id`='developement.database.update.date';

ALTER TABLE tblDocumentEntsRefToPeople ADD COLUMN unSure varchar(1);
UPDATE `mia`.`tblApplicationProperty` SET `value`='27/11/2017' WHERE  `id`='developement.database.update.date';

UPDATE `mia`.`tblUserAuthority` SET `priority`='1' WHERE  `authority`='ADMINISTRATORS';
UPDATE `mia`.`tblUserAuthority` SET `priority`='2' WHERE  `authority`='ONSITE_FELLOWS';
UPDATE `mia`.`tblUserAuthority` SET `priority`='3' WHERE  `authority`='FELLOWS';
UPDATE `mia`.`tblUserAuthority` SET `priority`='4' WHERE  `authority`='DIGITIZATION_COORDINATORS';
UPDATE `mia`.`tblUserAuthority` SET `priority`='5' WHERE  `authority`='COMMUNITY_COORDINATORS';
UPDATE `mia`.`tblUserAuthority` SET `priority`='6' WHERE  `authority`='DISTANCE_FELLOWS_COORDINATORS';
UPDATE `mia`.`tblUserAuthority` SET `priority`='7' WHERE  `authority`='DIGITIZATION_TECHNICIANS';
UPDATE `mia`.`tblUserAuthority` SET `priority`='8' WHERE  `authority`='SENIOR_DISTANCE_FELLOWS';
UPDATE `mia`.`tblUserAuthority` SET `priority`='9' WHERE  `authority`='FORMER_FELLOWS';
UPDATE `mia`.`tblUserAuthority` SET `priority`='10' WHERE  `authority`='TEACHERS';
UPDATE `mia`.`tblUserAuthority` SET `priority`='11' WHERE  `authority`='STUDENTS';
UPDATE `mia`.`tblUserAuthority` SET `priority`='12' WHERE  `authority`='COMMUNITY_USERS';
UPDATE `mia`.`tblUserAuthority` SET `priority`='12' WHERE  `authority`='GUESTS';
UPDATE `mia`.`tblApplicationProperty` SET `value`='08/01/2018' WHERE  `id`='developement.database.update.date';

CREATE TABLE `tblOrganizations` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`personId` INT(10) NOT NULL,
	`headquarters` INT(10) NOT NULL,
	`year` INT(4) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK_tblOrganizations_tblPeople` (`personId`),
	INDEX `FK_tblOrganizations_tblPlaces` (`headquarters`),
	CONSTRAINT `FK_tblOrganizations_tblPeople` FOREIGN KEY (`personId`) REFERENCES `tblPeople` (`PERSONID`),
	CONSTRAINT `FK_tblOrganizations_tblPlaces` FOREIGN KEY (`headquarters`) REFERENCES `tblPlaces` (`PLACEALLID`)
) 
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
UPDATE `mia`.`tblApplicationProperty` SET `value`='15/01/2018' WHERE  `id`='developement.database.update.date';

-- Start update 30/01/2018
DROP TABLE IF EXISTS `tblOrganizations`;
CREATE TABLE `tblOrganizationHeadquarters` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`personId` INT(10) NOT NULL,
	`headquarters` INT(10) NULL DEFAULT NULL,
	`year` INT(4) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK_tblOrganizationHeadquarters_tblPeople` (`personId`),
	INDEX `FK_tblOrganizationHeadquarters_tblPlaces` (`headquarters`),
	CONSTRAINT `FK_tblOrganizationHeadquarters_tblPeople` FOREIGN KEY (`personId`) REFERENCES `tblPeople` (`PERSONID`),
	CONSTRAINT `FK_tblOrganizationHeadquarters_tblPlaces` FOREIGN KEY (`headquarters`) REFERENCES `tblPlaces` (`PLACEALLID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
UPDATE `mia`.`tblApplicationProperty` SET `value`='30/01/2018' WHERE  `id`='developement.database.update.date';
-- End update 30/01/2018 

-- Start update 13/03/2018
INSERT INTO `mia`.`tblApplicationProperty` (`id`, `help`, `value`) VALUES ('installation.type', 'can be development or production (test servers on linux are production)', 'development');
INSERT INTO `mia`.`tblApplicationProperty` (`id`, `help`, `value`) VALUES ('vips.convert.executable', 'vips executable name (only linux)', 'vips');
INSERT INTO `mia`.`tblApplicationProperty` (`id`, `help`, `value`) VALUES ('vips.convert.path', 'path to vips (only linux) always end with slash', '/usr/local/bin/');
INSERT INTO `mia`.`tblApplicationProperty` (`id`, `help`, `value`) VALUES ('vips.thumbnail.executable', 'vips executable name (only linux)', 'vipsthumbnail');
UPDATE `mia`.`tblApplicationProperty` SET `value`='13/03/2018' WHERE  `id`='developement.database.update.date';
-- End update 13/03/2018

-- Start update 19/03/2018
ALTER TABLE `tblDocTranscriptions` ADD COLUMN `uploadedFileId` INT NULL AFTER `transcription`;
ALTER TABLE `tblDocTranscriptions` ADD CONSTRAINT `uploadedFileIdx` FOREIGN KEY (`uploadedFileId`) REFERENCES `tblUploadedFiles` (`id`);
UPDATE `mia`.`tblApplicationProperty` SET `value`='19/03/2018' WHERE  `id`='developement.database.update.date';
-- End update 19/03/2018

-- Start update 21/03/2018
ALTER TABLE `tblUploads` ADD COLUMN `modifiedBy` varchar(255) AFTER `owner`;
UPDATE `mia`.`tblApplicationProperty` SET `value`='21/03/2018' WHERE  `id`='developement.database.update.date';
-- End update 21/03/2018

-- Start update 22/03/2018
ALTER TABLE `tblForumPost` DROP FOREIGN KEY `FK7CB3574344BAD736`;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
DROP VIEW `asd`;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
UPDATE `mia`.`tblApplicationProperty` SET `value`='22/03/2018' WHERE  `id`='developement.database.update.date';
-- Start update 22/03/2018

-- Start update 10/06/2018
CREATE TABLE `tblLastAccessedRecords` (
	`idLastAccessedItems` INT(11) NOT NULL AUTO_INCREMENT,
	`account` VARCHAR(50) NOT NULL,
	`recordType` VARCHAR(50) NOT NULL COMMENT 'AE (Upload),DE (Document),BIO (Person),GEO (Place), VOL (Volume),INS(Insert)',
	`action` VARCHAR(50) NOT NULL COMMENT 'VIEW (View),EDIT (Edit),CREATE (Create)',
	`dateAndTime` DATETIME NOT NULL,
	`entryId` INT(11) NOT NULL,
	PRIMARY KEY (`idLastAccessedItems`)
)

UPDATE `mia`.`tblApplicationProperty` SET `value`='10/06/2018' WHERE  `id`='developement.database.update.date';
-- End update 10/06/2018

-- Start update 29/06/2018
ALTER TABLE `tblVolumes`
	CHANGE COLUMN `DATECREATED` `DATECREATED` DATETIME NULL DEFAULT NULL AFTER `createdBy`,
	CHANGE COLUMN `LASTUPDATE` `LASTUPDATE` DATETIME NULL DEFAULT NULL AFTER `lastUpdateBy`,
	ADD COLUMN `deletedBy` VARCHAR(50) NULL DEFAULT NULL AFTER `LASTUPDATE`,
	ADD COLUMN `dateDeleted` DATETIME NULL DEFAULT NULL AFTER `deletedBy`,
	ADD CONSTRAINT `deletedByAccount` FOREIGN KEY (`deletedBy`) REFERENCES `tblUser` (`account`);

ALTER TABLE `tblVolumes`
	ADD COLUMN `sezione` VARCHAR(50) NULL DEFAULT NULL AFTER `collection`,
	ADD COLUMN `title` VARCHAR(50) NULL DEFAULT NULL AFTER `sezione`,
	ADD COLUMN `otherNotes` LONGTEXT NULL AFTER `INVSOMDESC`,
	ADD COLUMN `SPINE` TINYINT ZEROFILL NULL AFTER `DATENOTES`,
	ADD COLUMN `spineImageName` VARCHAR(100) NULL AFTER `SPINE`;

ALTER TABLE `tblInserts`
	ADD COLUMN `startYear` INT NULL AFTER `volume`,
	ADD COLUMN `startMonth` INT NULL AFTER `startYear`,
	ADD COLUMN `startDay` INT NULL AFTER `startMonth`,
	ADD COLUMN `endYear` INT NULL AFTER `startDay`,
	ADD COLUMN `endMonth` INT NULL AFTER `endYear`,
	ADD COLUMN `endDay` INT NULL AFTER `endMonth`,
	ADD COLUMN `dateNotes` TEXT NULL AFTER `endDay`,
	ADD COLUMN `organizationalCriteria` TEXT NULL AFTER `dateNotes`,
	ADD COLUMN `paginationNotes` TEXT NULL AFTER `organizationalCriteria`,
	ADD COLUMN `pagination` TEXT NULL AFTER `paginationNotes`,
	ADD COLUMN `folioCount` VARCHAR(50) NULL AFTER `pagination`,
	ADD COLUMN `missingFolios` VARCHAR(50) NULL AFTER `folioCount`,
	ADD COLUMN `context` TEXT NULL AFTER `missingFolios`,
	ADD COLUMN `otherNotes` TEXT NULL AFTER `context`,
	ADD COLUMN `GUARDIA` TINYINT ZEROFILL NULL AFTER `context`,
	ADD COLUMN `guardiaImageName` VARCHAR(100) NULL AFTER `GUARDIA`,
	ADD COLUMN `createdBy` VARCHAR(50) NOT NULL DEFAULT 'pteam' AFTER `guardiaImageName` ,	
	ADD COLUMN `dateCreated` DATETIME NOT NULL DEFAULT '2018-01-01' AFTER `createdBy`,
	ADD COLUMN `lastUpdateBy` VARCHAR(50) NULL AFTER `dateCreated`,
	ADD CONSTRAINT `lastUpdateByKey` FOREIGN KEY (`lastUpdateBy`) REFERENCES `tblUser` (`account`);
ALTER TABLE `tblInserts`
	ADD COLUMN `dateLastUpdate` DATETIME NULL AFTER `lastUpdateBy`,
	ADD COLUMN `deletedBy` VARCHAR(50) NULL AFTER `dateLastUpdate`,
	ADD COLUMN `dateDeleted` DATETIME NULL AFTER `deletedBy`,
	ADD COLUMN `logicalDelete` INT NULL AFTER `dateDeleted`,
	ADD COLUMN `title` VARCHAR(50) NULL DEFAULT NULL AFTER `context`;


UPDATE `mia`.`tblApplicationProperty` SET `value`='29/06/2018' WHERE  `id`='developement.database.update.date';
-- End update 10/06/2018

-- Start update 13/08/2018
ALTER TABLE `tblDocumentEnts` ADD COLUMN `transSearch` LONGTEXT NOT NULL;
UPDATE `mia`.`tblApplicationProperty` SET `value`='13/08/2018' WHERE  `id`='developement.database.update.date';
-- End update 13/08/2018

-- Start update 06/09/2018 - DD/MM/YY
ALTER TABLE mia.tblVolumes CHANGE COLUMN STARTDAY STARTDAY INT UNSIGNED NULL DEFAULT NULL AFTER STARTMONTHNUM;
ALTER TABLE mia.tblVolumes CHANGE COLUMN STARTYEAR STARTYEAR INT NULL DEFAULT NULL AFTER STATBOX, CHANGE COLUMN ENDYEAR ENDYEAR INT NULL DEFAULT NULL AFTER STARTDATE, CHANGE COLUMN ENDDAY ENDDAY INT UNSIGNED NULL DEFAULT NULL AFTER ENDMONTHNUM;

ALTER TABLE mia.tblAltNames ADD FULLTEXT INDEX AltName (AltName);
UPDATE mia.tblApplicationProperty SET help='this mia database is updated to date (import.sql - format DD/MM/YYYY)' WHERE  id='developement.database.update.date';
UPDATE mia.tblApplicationProperty SET value='06/09/2018' WHERE  id='developement.database.update.date';
-- End update 06/09/2018 - DD/MM/YY

ALTER TABLE mia.tblPeople ADD FULLTEXT INDEX MAPNameLF (MAPNameLF);
UPDATE mia.tblApplicationProperty SET value='09/09/2018' WHERE  id='developement.database.update.date';
-- End update 06/09/2018 - DD/MM/YY

-- Start update 13/09/2018 - DD/MM/YY
ALTER TABLE `tblDocumentEnts` CHANGE COLUMN `transSearch` `transSearch` LONGTEXT NULL AFTER `placeOfOriginUnsure`;
UPDATE `mia`.`tblApplicationProperty` SET `value`='13/09/2018' WHERE  `id`='developement.database.update.date';
-- End update 13/09/2018 - DD/MM/YY

-- Start update 14/09/2018 - DD/MM/YY
ALTER TABLE `tblVolumes` ALTER `SPINE` DROP DEFAULT;
ALTER TABLE `tblVolumes` CHANGE COLUMN `SPINE` `SPINE` TINYINT(3) UNSIGNED ZEROFILL NULL AFTER `DATENOTES`;
UPDATE `mia`.`tblApplicationProperty` SET `value`='14/09/2018' WHERE  `id`='developement.database.update.date';
-- End update 14/09/2018 - DD/MM/YY

-- Start update 20/09/2018 - DD/MM/YY
ALTER TABLE `tblVolumes` DROP COLUMN `spineImageName`;
ALTER TABLE `tblInserts`DROP COLUMN `guardiaImageName`;
ALTER TABLE `tblUploads`ADD COLUMN `optionalImage` VARCHAR(50) NULL DEFAULT NULL AFTER `insertN`;
UPDATE `mia`.`tblApplicationProperty` SET `value`='20/09/2018' WHERE  `id`='developement.database.update.date';
-- End update 20/09/2018 - DD/MM/YY

-- Start update 03/10/2018 - DD/MM/YY
CREATE TABLE tblFileSharing (
	id int(11) NOT NULL AUTO_INCREMENT,
	idTblUpload int(11) NOT NULL,
	idTblUploadedFiles int(11) NOT NULL,
	account varchar(50) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (idTblUpload) REFERENCES tblUploads(id),
	FOREIGN KEY (idTblUploadedFiles) REFERENCES tblUploadedFiles(id),
	FOREIGN KEY (account) REFERENCES tblUser(account)
)
COLLATE='latin1_swedish_ci';
UPDATE `mia`.`tblApplicationProperty` SET `value`='03/10/2018' WHERE  `id`='developement.database.update.date';
-- End update 03/10/2018 - DD/MM/YY

ALTER TABLE `tblDocumentEnts` ADD FULLTEXT INDEX `transSearch` (`transSearch`);
UPDATE `mia`.`tblApplicationProperty` SET `value`='05/10/2018' WHERE  `id`='developement.database.update.date';

ALTER TABLE `tblPlaces` ADD FULLTEXT INDEX `placeName` (`placeName`);
UPDATE `mia`.`tblApplicationProperty` SET `value`='07/10/2018' WHERE  `id`='developement.database.update.date';

ALTER TABLE `tblDocumentEnts` ADD FULLTEXT INDEX `transSearch_generalNotesSynopsis` (`transSearch`, `generalNotesSynopsis`);
UPDATE `mia`.`tblApplicationProperty` SET `value`='08/10/2018' WHERE  `id`='developement.database.update.date';

-- Start update 15/10/2018 - DD/MM/YY
ALTER TABLE tblSchedone ADD COLUMN SUMMARYID INT(10) NOT NULL DEFAULT 0 AFTER ID;
UPDATE tblSchedone as s SET s.SUMMARYID = (select v.SUMMARYID from tblVolumes as v where v.volume = CONCAT(COALESCE(s.VOLNUM,),COALESCE(s.VOLLETEXT,)) and v.collection=1 and LOGICALDELETE=0)
UPDATE `mia`.`tblApplicationProperty` SET `value`='15/10/2018' WHERE  `id`='developement.database.update.date';
-- End update 15/10/2018 - DD/MM/YY

-- Start update 16/10/2018 - DD/MM/YY
ALTER TABLE `tblUser` ADD COLUMN `academiaEduLink` VARCHAR(255) NULL AFTER `resumeName`;
UPDATE `mia`.`tblApplicationProperty` SET `value`='16/10/2018' WHERE  `id`='developement.database.update.date';
-- End update 16/10/2018 - DD/MM/YY

-- Start update 19/10/2018 - DD/MM/YY
UPDATE `tblInserts` SET `logicalDelete` = 0 WHERE `logicalDelete` is null
-- End update 19/10/2018 - DD/MM/YY

-- Start update 23/10/2018 - DD/MM/YY
update tblVolumes SET logicalDelete = '0' where logicalDelete is null;

ALTER TABLE `tblVolumes`
	ALTER `LASTUPDATE` DROP DEFAULT;
ALTER TABLE `tblVolumes`
	CHANGE COLUMN `LOGICALDELETE` `LOGICALDELETE` TINYINT(4) NOT NULL DEFAULT '0' AFTER `DIGITIZED`,
	CHANGE COLUMN `LASTUPDATE` `LASTUPDATE` DATETIME NULL AFTER `lastUpdateBy`;

ALTER TABLE `tblInserts`
	ALTER `logicalDelete` DROP DEFAULT;
ALTER TABLE `tblInserts`
	CHANGE COLUMN `logicalDelete` `logicalDelete` INT(11) NOT NULL AFTER `dateDeleted`;

update tblUploadFilesFolios SET folioNumber = null where folioNumber = '';
update tblUploadFilesFolios SET rectoverso = null where rectoverso = '';
update tblUploadFilesFolios SET noNumb = '0' where noNumb is null;
update tblUploadFilesFolios SET noNumb = 1 where folioNumber is null and rectoverso is null;

UPDATE `mia`.`tblApplicationProperty` SET `value`='23/10/2018' WHERE  `id`='developement.database.update.date';
-- End update 23/10/2018 - DD/MM/YY

-- Start update 23/11/2018 - DD/MM/YY
ALTER TABLE `tblInserts`
	CHANGE COLUMN `logicalDelete` `logicalDelete` INT(11) NOT NULL DEFAULT '0' AFTER `dateDeleted`;

UPDATE `mia`.`tblApplicationProperty` SET `value`='23/11/2018' WHERE  `id`='developement.database.update.date';
-- End update 23/11/2018 - DD/MM/YY

-- Start update 20.12.2018 - DD/MM/YY
ALTER TABLE `mia`.`tblLastAccessedRecords`
ADD COLUMN `serializedData` MEDIUMTEXT NULL DEFAULT NULL AFTER `entryId`,
ADD COLUMN `accessedFrom` TINYTEXT NULL DEFAULT NULL AFTER `serializedData`,
CHANGE COLUMN `action` `action` VARCHAR(50) NOT NULL COMMENT 'VIEW (View), EDIT (Edit), CREATE (Create), DELETE(Delete), UNDELETE(Undelete)';

UPDATE tblPasswordChangeRequest SET MAILSENDED=1;
UPDATE tblApprovationUser SET mailSended=1;
UPDATE tblActivationUser SET mailSended=1;
UPDATE tblLockedUser SET mailSended=1;

UPDATE mia.tblApplicationProperty SET value='Action Required to Activate Membership for MIA' WHERE  id='mail.activationUser.subject';

UPDATE mia.tblApplicationProperty SET value='Dear {0},\r \r Thank you for registering in the MIA Portal. Before we can activate your 
account one last step must be taken to complete your registration.\r Please visit this url once to activate your account:\r \r {3}:
//{4}{5}user/ActivateUser.do?uuid={2}\r \r Please be sure not to add extra spaces.\r\r If you are still having problems signing up 
please contact a member of our support staff.\r \rYour Account is: {1}\r Your Activation uuid is:{2}\r \rMIA Support Service' WHERE  id='mail.activationUser.text';

UPDATE mia.tblApplicationProperty SET value='Dear {0},\r \r You have requested to reset your password on MIA.\r\r To reset your password, please go to this page:\r \r {3}://{4}{5}user/ResetUserPassword.do?uuid={2}\r \r Please be sure not to add extra spaces.\r \r If you are still having problems signing up please contact a member of our support staff.\r \rYour Username is: {1}\r \r \r All the best,\r BIA Support Service\r ' WHERE  id='mail.resetUserPassword.text';


UPDATE mia.tblApplicationProperty SET value='Dear {0},\r \r Your account has been approved.\r \r Please be sure not to add extra spaces.\r If you are still having problems signing up please contact a member of our support staff.\r \\nYour Account is: {4}\r \rPlease go to http://mia.medici.org:8080/Mia/\r\rand log in with your username and password\r\r\r \\nBest Wishes,\rMIA Support Service' WHERE  id='mail.approvedUser.text';

UPDATE mia.tblApplicationProperty SET value='150' WHERE  id='user.expiration.user.months';
UPDATE mia.tblApplicationProperty SET value='150' WHERE  id='user.expiration.password.months';
UPDATE mia.tblApplicationProperty SET value='mia.medici.org' WHERE  id='recaptcha.domainName';

UPDATE `mia`.`tblApplicationProperty` SET `value`='20/12/2018' WHERE  `id`='developement.database.update.date';

-- End update 20.12.2018 - DD/MM/YY