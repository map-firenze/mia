
-- Start update 18/12/2018 - DD/MM/YY

CREATE TABLE `tblCollations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `childDocId` int(11) unsigned DEFAULT NULL,
  `parentDocId` int(11) unsigned DEFAULT NULL,
  `type` varchar(45) NOT NULL,
  `created` datetime DEFAULT NULL,
  `createdBy` varchar(50) DEFAULT NULL,
  `destinationFile` int(10) unsigned DEFAULT NULL,
  `title` text,
  `uploadInfoId` int(11) unsigned DEFAULT NULL,
  `unsure` tinyint(4) DEFAULT '0',
  `description` text,
  `logicalDelete` tinyint(4) NOT NULL DEFAULT '0',
  `childVolId` int(11) DEFAULT NULL,
  `parentVolId` int(11) DEFAULT NULL,
  `childInsId` int(11) DEFAULT NULL,
  `parentInsId` int(11) DEFAULT NULL,
  `folioNumber` text,
  `foliorv` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idtblCollations_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- End update 18/12/2018 - DD/MM/YY

-- Start update 19/01/2019 - DD/MM/YY

CREATE TABLE `tblComments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT NULL,
  `recordId` int(11) NOT NULL,
  `recordType` varchar(45) NOT NULL,
  `createdBy` varchar(45) NOT NULL,
  `modifiedBy` varchar(45) DEFAULT NULL,
  `deletedBy` varchar(45) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `modifiedAt` datetime DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `title` text,
  `text` text NOT NULL,
  `logicalDelete` tinyint(4) NOT NULL DEFAULT '0',
  `repliedToId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `mia`.`tblUploads`
ADD COLUMN `commentsCount` INT NULL DEFAULT 0 AFTER `filePrivacy`;

ALTER TABLE `mia`.`tblDocumentEnts`
ADD COLUMN `commentsCount` INT NULL DEFAULT 0 AFTER `transSearch`;

ALTER TABLE `mia`.`tblPlaces`
ADD COLUMN `commentsCount` INT NULL DEFAULT 0 AFTER `lastUpdateBy`;

ALTER TABLE `mia`.`tblPeople`
ADD COLUMN `commentsCount` INT NULL DEFAULT 0 AFTER `lastUpdateBy`;

UPDATE `mia`.`tblApplicationProperty` SET `value`='18/01/2019' WHERE  `id`='delopement.database.update.date';

-- End update 18/01/2018 - DD/MM/YY
-- End update 19/01/2019 - DD/MM/YY


-- Start update 08/02/2019 - DD/MM/YY

ALTER TABLE tblDocuments ADD COLUMN IMPORTLOCK TINYINT(4) NOT NULL DEFAULT '0' ;

INSERT INTO mia.tblApplicationTemplate (name, parentName) VALUES ('mview/ArchivalEntityPageTurnerDialog', 'template.partialDOM');
INSERT INTO mia.tblApplicationTemplateAttributes (templateName, name, value, cascadeAttribute) VALUES ('mview/ArchivalEntityPageTurnerDialog', 'main', '/WEB-INF/jsp/mview/ArchivalEntityPageTurnerDialog.jsp', '0');


INSERT INTO mia.tblApplicationTemplate (name, parentName) VALUES ('mview/ShowArchivalEntityInManuscriptViewerHtml', 'template.manuscriptViewerHtmltemplate.manuscriptViewerHtmlDOM');
INSERT INTO mia.tblApplicationTemplateAttributes (templateName, name, value, cascadeAttribute) VALUES ('mview/ShowArchivalEntityInManuscriptViewerHtml', 'manuscriptviewer', '/WEB-INF/jsp/mview/ShowArchivalEntityInManuscriptViewerHtml.jsp', '0');

UPDATE `mia`.`tblApplicationProperty` SET `value`='08/02/2019' WHERE  `id`='developement.database.update.date';

-- End update 08/02/2019 - DD/MM/YY


-- Start update 21/02/2019 - DD/MM/YY

ALTER TABLE `tblDocuments` ADD COLUMN `IMPORTDATE` DATETIME NULL DEFAULT NULL AFTER `IMPORTLOCK`;
UPDATE `mia`.`tblApplicationProperty` SET `value`='21/02/2019' WHERE  `id`='developement.database.update.date';

-- End update 21/02/2019 - DD/MM/YY

-- Start update 25/02/2019 - DD/MM/YY
ALTER TABLE `mia`.`tblVolumes`
ADD COLUMN `ae_count` INT NOT NULL DEFAULT 0 AFTER `dateDeleted`;
update tblVolumes
set ae_count =
(select count(u.id) from tblUploads as u
where u.volume = tblVolumes.SUMMARYID AND u.insertN is null AND (u.logicalDelete = 0 OR u.logicalDelete is null));

ALTER TABLE `mia`.`tblInserts`
ADD COLUMN `ae_count` INT NOT NULL DEFAULT 0 AFTER `otherNotes`;
update tblInserts
set ae_count =
(select count(u.id) from tblUploads as u
where u.insertN = tblInserts.id AND (u.logicalDelete = 0 OR u.logicalDelete is null));

ALTER TABLE `mia`.`tblCollections`
ADD COLUMN `isStudyCollection` TINYINT NOT NULL DEFAULT 0 AFTER `collectionDescription`;
UPDATE `mia`.`tblCollections` SET `isStudyCollection`='1' WHERE `id`='1';

UPDATE `mia`.`tblApplicationProperty` SET `value`='25/02/2019' WHERE  `id`='developement.database.update.date';
-- End update 25/02/2019 - DD/MM/YY

-- Start update 04/03/2019 - DD/MM/YY
ALTER TABLE mia.tblPlaces
ADD COLUMN de_count INT NOT NULL DEFAULT 0 AFTER commentsCount;

-- Count query for de_count in tblPlaces
update tblPlaces
set de_count =
(select count(d.documentEntityId) from tblDocumentEnts as d
where (d.placeOfOrigin = tblPlaces.PLACEALLID) AND (d.flgLogicalDelete = 0 OR d.flgLogicalDelete is null));

CREATE TABLE tblCaseStudy (
  id int(11) NOT NULL AUTO_INCREMENT,
  title text NOT NULL,
  description text NOT NULL,
  notes text,
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  created_by varchar(45) NOT NULL,
  deleted_at timestamp NULL DEFAULT NULL,
  deleted_by int(11) DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE tblCaseStudyFolder (
  id int(11) NOT NULL AUTO_INCREMENT,
  case_study_id int(11) NOT NULL,
  title text NOT NULL,
  order_by int(11) NOT NULL DEFAULT '0',
  created_by varchar(45) NOT NULL,
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
);

CREATE TABLE tblCaseStudyItem (
  id int(11) NOT NULL AUTO_INCREMENT,
  entity_id int(11) DEFAULT NULL,
  entity_type varchar(45) NOT NULL,
  title text,
  folder_id int(11) NOT NULL,
  order_by int(11) NOT NULL DEFAULT '0',
  path text,
  link text,
  created_by varchar(45) NOT NULL,
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
);

CREATE TABLE tblCaseStudyUsers (
  id int(11) NOT NULL AUTO_INCREMENT,
  case_study_id int(11) NOT NULL,
  user_account varchar(45) NOT NULL,
  PRIMARY KEY (id)
);

-- Dir Used for uploading files in the casestudy
INSERT INTO tblApplicationProperty (id, help, value) VALUES
    ('casestudy.upload.path', '', '/data/casestudy/items');

UPDATE `mia`.`tblApplicationProperty` SET `value`='04/03/2019' WHERE  `id`='developement.database.update.date';
-- End update 04/03/2019 - DD/MM/YY

-- Start update 04/03/2019 - DD/MM/YY
ALTER TABLE `mia`.`tblPlaces`
ADD COLUMN `de_count` INT NOT NULL DEFAULT 0 AFTER `commentsCount`;

update tblPlaces
set de_count =
(select count(d.documentEntityId) from tblDocumentEnts as d
where (d.placeOfOrigin = tblPlaces.PLACEALLID) AND (d.flgLogicalDelete = 0 OR d.flgLogicalDelete is null));
-- End update 04/03/2019 - DD/MM/YY

-- Start update 16/03/2019 - DD/MM/YY

ALTER TABLE `mia`.`tblComments`
ADD COLUMN `mentions` TEXT NULL DEFAULT NULL AFTER `repliedToId`;

UPDATE `mia`.`tblApplicationProperty` SET `value`='16/03/2019' WHERE  `id`='developement.database.update.date';
-- End update 16/03/2019 - DD/MM/YY

-- Start update 25/03/2019 - DD/MM/YY
ALTER TABLE `mia`.`tblComments`
ADD COLUMN `reportedBy` TEXT NULL DEFAULT NULL AFTER `mentions`;

UPDATE `mia`.`tblApplicationProperty` SET `value`='25/03/2019' WHERE  `id`='developement.database.update.date';
-- End update 25/03/2019 - DD/MM/YY

---- Discovieries module section ---

-- Start update 26/03/2019 - DD/MM/YY
CREATE TABLE `tblDiscoveries` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(100) NOT NULL,
	`whyHistImp` TEXT NOT NULL,
	`shortNotice` TEXT NOT NULL,
	`bibliography` TEXT NULL,
	`status` VARCHAR(30) NOT NULL,
	`logicalDelete` TINYINT(4) NOT NULL DEFAULT '0',
	`idTblDocumentEnts` INT(11) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `idTblDocumentEnts` (`idTblDocumentEnts`),
	CONSTRAINT `idTblDocumentEnts` FOREIGN KEY (`idTblDocumentEnts`) REFERENCES `tblDocumentEnts` (`documentEntityId`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
-- End update 26/03/2019 - DD/MM/YY

-- Start update 27/03/2019 - DD/MM/YY

INSERT INTO `mia`.`tblUserAuthority` (`authority`, `priority`, `description`) VALUES ('SPOTLIGHT_COORDINATORS', '14', 'Spotlight Coordinators');

UPDATE `mia`.`tblApplicationProperty` SET `value`='27/03/2019' WHERE  `id`='developement.database.update.date';

-- End update 27/03/2019 - DD/MM/YY


-- Start update 29/03/2019 - DD/MM/YY
ALTER TABLE `tblDiscoveries` ADD COLUMN `submissionDate` DATETIME DEFAULT NULL;

UPDATE `mia`.`tblApplicationProperty` SET `value`='27/03/2019' WHERE  `id`='developement.database.update.date';
-- End update 29/03/2019 - DD/MM/YY

-- Start update 03/04/2019 - DD/MM/YY
CREATE TABLE IF NOT EXISTS `tblDiscoveriesRelLink` (
	`idDiscoveries` INT(11) NOT NULL,
	`idTblDocumentEnts` INT(11) NOT NULL,
	PRIMARY KEY (`idDiscoveries`, `idTblDocumentEnts`),
	CONSTRAINT `FK1_tblDiscoveriesRelLink` FOREIGN KEY (`idDiscoveries`) REFERENCES `tblDiscoveries` (`id`),
	CONSTRAINT `FK2_tblDiscoveriesRelLink` FOREIGN KEY (`idTblDocumentEnts`) REFERENCES `tblDocumentEnts` (`documentEntityId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO tblApplicationProperty (id, help, value) VALUES ('discoveries.upload.path', '', '/data/discoveries/items/');

UPDATE `mia`.`tblApplicationProperty` SET `value`='03/04/2019' WHERE  `id`='developement.database.update.date';
-- End update 03/04/2019 - DD/MM/YY

-- Start update 05/04/2019 - DD/MM/YY
CREATE TABLE tblDiscoveriesUpload (
  id int(11) NOT NULL AUTO_INCREMENT,
  fileName varchar(100) NOT NULL,
  internalFileName varchar(100) NOT NULL,
  descriptiveTitle varchar(100) NOT NULL,
  `idDiscoveries` INT(11),
  PRIMARY KEY (id),
  FOREIGN KEY (`idDiscoveries`) REFERENCES `tblDiscoveries` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

UPDATE `mia`.`tblApplicationProperty` SET `value`='05/04/2019' WHERE  `id`='developement.database.update.date';
-- End update 05/04/2019 - DD/MM/YY


-- Add column documentsCount to tblPeople

-- Start update 02/05/2019 - DD/MM/YY
ALTER TABLE mia.tblPeople ADD documentsCount INT DEFAULT 0 NULL;

UPDATE `mia`.`tblApplicationProperty` SET `value`='02/05/2019' WHERE  `id`='developement.database.update.date';
-- End update 02/05/2019 - DD/MM/YY


-- Start update 26/06/2019 - DD/MM/YY
ALTER TABLE `tblUserMessage` CHANGE COLUMN `messageBody` `messageBody` TEXT NULL DEFAULT NULL AFTER `messageId`;
UPDATE `mia`.`tblApplicationProperty` SET `value`='26/06/2019' WHERE  `id`='developement.database.update.date';
-- End update 26/06/2019 - DD/MM/YY


-- Start update 26/03/2020 - DD/MM/YY
UPDATE tblDocumentEnts SET docYear = 0 WHERE docYear is NULL;
UPDATE tblDocumentEnts SET docModernYear = 0 WHERE docModernYear is NULL;
UPDATE tblDocumentEnts SET docMonth = 0 WHERE docMonth is NULL;
UPDATE tblDocumentEnts SET docDay = 0 WHERE docDay is NULL;

ALTER TABLE tblDocumentEnts
ADD COLUMN docSortingYear INT(11) AFTER docModernYear;
UPDATE tblDocumentEnts
SET docSortingYear = CASE
WHEN docModernYear != 0 THEN docModernYear
ELSE docYear
END

INSERT INTO mia.tblApplicationProperty
(id, help, value)
VALUES('folio.transcription.allowMultiple','Allow multiple transcriptions for same folio','false');

UPDATE mia.tblApplicationProperty SET value='26/03/2020' WHERE id='developement.database.update.date';
-- End update 26/03/2020 - DD/MM/YY

-- Start update 08/04/2020 - DD/MM/YY
--- Personal And Public Profile
ALTER TABLE tblUser
ADD COLUMN hideActivities BIT(1) DEFAULT 0 NOT NULL,
ADD COLUMN hidePublications BIT(1) DEFAULT 0 NOT NULL,
ADD COLUMN hideStatistics BIT(1) DEFAULT 0 NOT NULL;

--- Messages and Mail to owner when modifying a record which is not yours.
INSERT INTO mia.tblApplicationProperty
(id, help, value)
VALUES('mail.recordEditedNotification.subject', 'Record edited notification mail subject', 'mia.medici.org - Your record has been modified');
INSERT INTO mia.tblApplicationProperty
(id, help, value)
VALUES('mail.recordEditedNotification.text', 'Record edited notification mail text', 'The Medici Archive Project - MIA

Your record has been modified by {0}. Please click on the "SHOW HISTORY" button on <a href="{1}">this record</a> to check the modification.');
INSERT INTO mia.tblApplicationProperty
(id, help, value)
VALUES('message.recordEditedNotification.subject', 'Record edited notification message subject', 'Your record has been modified');
INSERT INTO mia.tblApplicationProperty
(id, help, value)
VALUES('message.recordEditedNotification.text', 'Record edited notification message text', 'Your record has been modified by {0}. Please click on the "SHOW HISTORY" button on <a href="{1}">this record</a> t to check the modification.');
UPDATE mia.tblApplicationProperty SET value='08/04/2020' WHERE id='developement.database.update.date';
-- End update 08/04/2020 - DD/MM/YY

-- Start update 20/04/2020 - DD/MM/YY
-- chinese synopsis #89, #90
ALTER TABLE mia.tblDocumentEnts ADD COLUMN chineseSynopsis longtext AFTER generalNotesSynopsis;

ALTER TABLE mia.tblUser ADD COLUMN newsfeedLoginDate datetime DEFAULT NULL;

CREATE TABLE `tblUserPreferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `title` varchar(50) NOT NULL,
  `serializedData` longtext NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `account` (`account`),
  CONSTRAINT `account` FOREIGN KEY (`account`) REFERENCES `tblUser` (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO mia.tblApplicationProperty
(id, help, value)
VALUES('newsfeed.max.number', 'Maximum number of search filters that can be defined for the newsfeed section', '6');

UPDATE mia.tblApplicationProperty SET value='20/04/2020' WHERE id='developement.database.update.date';
-- End update 20/04/2020 - DD/MM/YY

-- Start update 06/05/2020 - DD/MM/YY
-- Annotation comments
CREATE TABLE `tblFileAnnotationsQuestions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `annotationId` int(11) NOT NULL,
  `account` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `text` longtext NOT NULL,
  `dateCreated` datetime NOT NULL,
  `lastUpdate` datetime NOT NULL,
  `logicalDelete` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY `tblFileAnnotationsQuestions_id` (`id`),
  INDEX `tblFileAnnotationsQuestions_annotationId` (`annotationId`),
  CONSTRAINT `tblFileAnnotationsQuestions_annotation` FOREIGN KEY (`annotationId`) REFERENCES `tblFileAnnotations` (`annotationId`),
  CONSTRAINT `tblFileAnnotationsQuestions_account` FOREIGN KEY (`account`) REFERENCES `tblUser` (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO mia.tblApplicationProperty
(id, help, value)
VALUES('annotation.question.max.number', 'Maximum number of comments that can be added to an annotation', '5');

--- Messages and Mail to owner when modifying a record which is not yours.
INSERT INTO mia.tblApplicationProperty
(id, help, value)
VALUES('mail.annotationQuestionNotification.subject', 'Annotation comment added notification mail subject', 'mia.medici.org - An annotation comment has been added');
INSERT INTO mia.tblApplicationProperty
(id, help, value)
VALUES('mail.annotationQuestionNotification.text', 'Annotation comment added notification mail text', 'The Medici Archive Project - MIA

An annotation comment has been added by {0}. Please <a href="{1}">click here</a> to read the comment.');
INSERT INTO mia.tblApplicationProperty
(id, help, value)
VALUES('message.annotationQuestionNotification.subject', 'Annotation comment added notification message subject', 'An annotation comment has been added');
INSERT INTO mia.tblApplicationProperty
(id, help, value)
VALUES('message.annotationQuestionNotification.text', 'Annotation comment added notification message text', 'An annotation comment has been added by {0}. Please <a href="{1}">click here</a> to read the comment.');
UPDATE mia.tblApplicationProperty SET value='06/05/2020' WHERE id='developement.database.update.date';
-- End update 06/05/2020 - DD/MM/YY

-- Start update 10/12/2020 - DD/MM/YY
INSERT INTO mia.tblApplicationProperty
(id, help, value)
VALUES('repository.multipleSupport', 'Enable multiple repositories support', 'false');
UPDATE mia.tblApplicationProperty SET value='10/12/2020' WHERE id='developement.database.update.date';
-- End update 10/12/2020 - DD/MM/YY

-- Start update 21/01/2021 - DD/MM/YY
INSERT INTO mia.tblApplicationProperty
(id, help, value)
VALUES('document.noImagesCreationSupport', 'Enable the support to create documents with no images', 'false');
UPDATE mia.tblApplicationProperty SET value='21/01/2021' WHERE id='developement.database.update.date';
-- End update 21/01/2021 - DD/MM/YY