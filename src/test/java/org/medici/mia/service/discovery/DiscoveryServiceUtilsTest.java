/*
 * DiscoveryUtilsTest.java
 *
 * Developed by The Medici Archive Project Inc. (2010-2012)
 * 
 * This file is part of DocSources.
 * 
 * DocSources is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * DocSources is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * As a special exception, if you link this library with other files to
 * produce an executable, this library does not by itself cause the
 * resulting executable to be covered by the GNU General Public License.
 * This exception does not however invalidate any other reasons why the
 * executable file might be covered by the GNU General Public License.
 */
package org.medici.mia.service.discovery;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.medici.mia.domain.DocumentTranscriptionEntity;
import org.medici.mia.domain.FolioEntity;
import org.medici.mia.domain.UploadFileEntity;

public class DiscoveryServiceUtilsTest {
	
	private String transcription;
	private List<DocumentTranscriptionEntity> transcriptions;
	
	@Before
	public void setUp() {
		String transcription1 = "[1v, 1r] document";
		String transcription2 = "[1v, 1r] documento";
		
		transcription = transcription1 + ", " + transcription2; 
	
		transcriptions = new ArrayList<DocumentTranscriptionEntity>();
		
		DocumentTranscriptionEntity trans1 = mock(DocumentTranscriptionEntity.class);
		DocumentTranscriptionEntity trans2 = mock(DocumentTranscriptionEntity.class);
		
		UploadFileEntity file1 = mock(UploadFileEntity.class);
		UploadFileEntity file2 = mock(UploadFileEntity.class);
		
		FolioEntity folio1 = mock(FolioEntity.class);
		FolioEntity folio2 = mock(FolioEntity.class);

		transcriptions.add(trans1);
		transcriptions.add(trans2);
		
		when(trans1.getUploadFileEntity()).thenReturn(file1);
		when(trans1.getTranscription()).thenReturn("document");
		
		when(trans2.getUploadFileEntity()).thenReturn(file2);
		when(trans2.getTranscription()).thenReturn("documento");
		
		when(file1.getFolioEntities()).thenReturn(Arrays.asList(folio1, folio2));
		when(file2.getFolioEntities()).thenReturn(Arrays.asList(folio1, folio2));
		
		when(folio1.getFolioNumber()).thenReturn("1");
		when(folio1.getRectoverso()).thenReturn("verso");
		when(folio2.getFolioNumber()).thenReturn("1");
		when(folio2.getRectoverso()).thenReturn("recto");
	}
	
	@Test
	public void testGetTranscriptions() {
		assertEquals(transcription, DiscoveryServiceUtils.getTranscription(transcriptions));
	}
}
