# README #

MIA is a community-sourcing research portal, developed by the Medici Archive Project (http: //www.medici.org), that uses self-digitization and self-archiving practices, along with
textual data-entry, to reconstruct and preserve the Medici Archive held in the Florence State Archive.  
This archive comprising over 15 million documents dating from the 1370s to 1743 - later dispersed into ten collections - is one of the most extensive and most disrupted archives in Western Europe. 
MIA allows a community of early-modern scholars to collaborate on research projects related to this archival corpus, and publish discoveries following a peer-review protocol.

MIA allows:  

- digitally reconstructing the archive through community-sourced uploads  
- allows annotations, transcriptions and organization of documents  
- enabling collaborative work and discussion
- publishing archival discoveries in the form of Short Notices following a peer-review protocol  



### Who worked on the project? ###

Lorenzo Allori - Project Manager (Product Owner)  
Shadab Bigdel - Software Architect  
Fabio Solfato - Fullstack Developer  
Maddalena Barlotti - Fullstack Developer  
Alexey Garmatin - Scrum Master  
Nikita Zikin - Backend Developer  
Alexey Stroevsky - Frontend Developer  
Gennadiy Borodin - Software Tester  
Maurizio Arfaioli - Software Tester  
Alessandro Binchi - Fullstack Developer   
Evgeniy Kravchenko - Frontend Developer  
Giovanni Avila - Frontend Developer  
Filipp Semirov - Frontend Developer  
Andrey Vygonnoy - Frontend Developer  
Rose Byfleet - Project Coordinator  
Vincent Chiang - Junior Backend Developer  
Jennifer Xie - Junior Frontend Developer  
Salvatore Gallo - Junior Developer  

