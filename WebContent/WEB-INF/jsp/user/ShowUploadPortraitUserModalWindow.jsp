<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<base href="/Mia"></base>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>MIA - Add/Edit Person Record</title>
		<link rel="shortcut icon" type="image/x-icon" href="/Mia/images/favicon_medici.png" />

		<link rel="stylesheet" type="text/css" media="screen" href="/Mia/styles/1024/AdministrationMenu.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="/Mia/styles/1024/MainContent.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="/Mia/styles/1024/Template.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="/Mia/styles/1024/js/modalbox.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="/Mia/styles/1024/modalbox-custom.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="/Mia/styles/1024/js/jquery.autocomplete2.css"/>
		<link rel="stylesheet" type="text/css" media="screen" href="/Mia/styles/1024/js/demo_table.css" />
<!-- 	<link rel="stylesheet" type="text/css" media="screen" href="/Mia/styles/1024/js/TableTools.css" />  -->
		<link rel="stylesheet" type="text/css" media="screen" href="/Mia/styles/1024/js/jquery-ui.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="/Mia/styles/1024/js/jquery.Jcrop.css"/>
		
		
		<!--[if lte IE 7]>
		<style type="text/css">
		html .jquerycssmenu{height: 1%;} /*Holly Hack for IE7 and below*/
		</style>
		<![endif]-->
		
		<script type="text/javascript" src="/Mia/scripts/jquery.min.js"></script>
		<script type="text/javascript" src="/Mia/scripts/jquery.advancedSearch.js"></script>
		<script type='text/javascript' src="/Mia/scripts/jquery.autocomplete.js"></script>
		<script type='text/javascript' src="/Mia/scripts/jquery.autocomplete.general.js"></script>
		<script type='text/javascript' src="/Mia/scripts/jquery.autocomplete.person.js"></script>
		<script type='text/javascript' src="/Mia/scripts/jquery.autocomplete.place.js"></script>
		<script type='text/javascript' src="/Mia/scripts/jquery.autocomplete.title.js"></script>
		<script type='text/javascript' src="/Mia/scripts/jquery.blockUI.js"></script>
		<script type="text/javascript" src="/Mia/scripts/mview/jquery.dialogextend.min.js"></script>
		<script type="text/javascript" src="/Mia/scripts/mview/jquery.pageTurner.js"></script>
		<script type="text/javascript" src="/Mia/scripts/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="/Mia/scripts/jquery.dataTables.pagination.js"></script>
		<script type="text/javascript" src="/Mia/scripts/jquery.form.js"></script>
		<script type="text/javascript" src="/Mia/scripts/jquery.Jcrop.js"></script>
		<script type="text/javascript" src="/Mia/scripts/jquery.color.js"></script>
		<script type="text/javascript" src="/Mia/scripts/jquery.open.js"></script>
		<script type="text/javascript" src="/Mia/scripts/jquery.shareButton.js"></script>
		<script type="text/javascript" src="/Mia/scripts/jquery.volumeExplorer.js"></script>
		<script type="text/javascript" src="/Mia/scripts/jquery-ui.min.js"></script>
		<script type="text/javascript" src="/Mia/scripts/pirobox_extended.js"></script>
		<script type="text/javascript" src="/Mia/scripts/jquery.tooltip.js"></script>
		<script type="text/javascript" src="/Mia/scripts/ModalBox/prototype.js"></script>
		<script type="text/javascript" src="/Mia/scripts/ModalBox/effects.js"></script>
		<script type="text/javascript" src="/Mia/scripts/ModalBox/modalbox.js"></script>
		<script type="text/javascript" src="/Mia/scripts/jquery.scrollTo.js"></script>
		<script type="text/javascript" src="/Mia/scripts/jquery.expander.js"></script>
		<script type="text/javascript" src="/Mia/scripts/scrolltopcontrol.js"></script>
		
<!-- 		<script type="text/javascript"> 
// 	           window.onbeforeunload = function() {
// 	               return "If you leave this page you will exit the Software Platform";
// 	           };
</script> -->


		<script type="text/javascript">
			history.go = function(){};

			var $j = jQuery.noConflict();
			$j(document).ready(function() {
				
				$j.ajaxSetup ({
					// Disable caching of AJAX responses
					cache: false,
					success: function(data) {
						var found = $j(data).find("#login");
						if (found.length > 0) {
							window.location = "/Mia/";
							return;
						}
					},
					error: function(xhr, status, err) {
						console.log(err);
				    }
				});		
				
			});
			
			if (navigator.appVersion.indexOf("Linux")!=-1)
				document.write('<link href="/Mia/styles/1024/MainContent_linux.css" rel="stylesheet" type="text/css">');
			 
			 if (navigator.appVersion.indexOf("X11")!=-1)
				 document.write('<link href="/Mia/styles/1024/MainContent_linux.css" rel="stylesheet" type="text/css">');
			var $timerId;
			
		</script>
	</head>
	
	<c:url var="ShowUploadPortraitUserURL" value="/user/ShowUploadPortraitUser.do">
	</c:url>
	
	<c:url var="DeletePortraitUserURL" value="/user/DeletePortraitUser.do">
		<c:param name="account" value="${user.account}" />
	</c:url>
	
	<c:url var="GetPortraitUserInformationURL" value="/user/GetPortraitUserInformation.json">
		<c:param name="account" value="${user.account}" />
	</c:url>
	
	<c:url var="ShowUserProfileURL" value="/user/ShowUserProfile.do"/>
<body>
	<form id="uploadPortraitUserForm" action="${ShowUploadPortraitUserURL}"	method="post" class="edit" enctype="multipart/form-data">
		<div class="listForm">
			<div class="row">
				<div class="col_l">
					<label for="link" id="linkLabel">Link</label>
				</div>
				<div class="col_l">
					<input id="link" name="link" class="input_40c" type="text" value="http://" />
				</div>
			</div>
			<div class="row">
				<div class="col_l">
					<label for="browse" id="browseLabel">Browse</label>
				</div>
				<div class="col_l">
					<input id="browse" name="browse" class="input_28c" type="file" value="" size="30" />
				</div>
			</div>
		</div>
		<br>
		<div>
			<input type="hidden" name="account" value="${user.account}" />
			<a id="resetPortrait" href="${DeletePortraitUserURL}" class="button_medium">Reset Portrait</a>
			<input id="save" type="submit" class="savePortrait"  value="Save" />
		</div>
	
	</form>

	<div id="output"></div>
	<script type="text/javascript">
		$j(document).ready(function() {
			$j("#resetPortrait").click(function(){
				$j.ajax({ type:"POST", url:$j(this).attr("href"), data:$j(this).serialize(), async:false, success:function(html) {
					Modalbox.show('${ShowUserProfileURL}', {title: "USER PREFERENCES", width: 760, height: 470});
					$j("#uploadPortraitUserWindow").dialog("close");
				}});
				return false;
			});
			
			var options = { 
				target:        '#output',   // target element(s) to be updated with server response 
				success:       showResponse  // post-submit callback 
		    }; 

		    $j('#uploadPortraitUserForm').ajaxForm(options);

		    // post-submit callback 
			function showResponse(responseText, statusText, xhr, $form)  {
			    // if the ajaxForm method was passed an Options Object with the dataType 
			    // property set to 'json' then the first argument to the success callback 
			    // is the json data object returned by the server 
			 	$j('#uploadPortraitUserWindow').html(responseText);
			 	$j.ajax({ url: '${GetPortraitUserInformationURL}', cache: false, success:function(json) { 
			 		if (json.portraitWidth > 100) {
						$j("#uploadPortraitUserWindow").dialog("option", "width", json.portraitWidth + 10);
			 		}
			 		if (json.portraitHeight> 100) {
						$j("#uploadPortraitUserWindow").dialog("option", "height", json.portraitHeight + 100);
			 		}
				}});
			} 
		});
		</script>
</body>
</html>