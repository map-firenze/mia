<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

	<c:url var="IIPImageServerURL" value="/mview/IIPImageServer.do"/>
	<c:url var="ImagePrefixURL" value="/images/mview/"/>
	<c:url var="GetImageAnnotationURL" value="/src/mview/GetImageAnnotation.json">
		<c:param name="imageId" value="${image.imageId}"></c:param>
		<c:param name="imageName" value="${image.imageName}"></c:param>
	</c:url>
	<c:url var="UpdateAnnotationsURL" value="/src/mview/UpdateAnnotations.json">
		<c:param name="imageId" value="${image.imageId}"></c:param>
		<c:param name="imageName" value="${image.imageName}"></c:param>
	</c:url>
	
		<script type="text/javascript">
			var imageName = "${image.imageName}";
			
			// RR: Added volume informations and insert informations (if needed)
			var volNum = "${image.volNum}"
			var volExt = "${image.volLetExt}";
			var insNum = "${image.insertNum}";
			var insExt = "${image.insertLet}";
			
			var credit = '<span style=\'font-size:16px\'><fmt:message key="mview.showDocumentInManuscriptViewerHtml.repository"/> ${documentExplorer.repository}';
			credit+=' &nbsp; - <fmt:message key="mview.showDocumentInManuscriptViewerHtml.collection"/> ${documentExplorer.collection}';
			credit+=' &nbsp; - <fmt:message key="mview.showDocumentInManuscriptViewerHtml.volume"/> ${documentExplorer.volume}';
			credit+=' &nbsp; - <fmt:message key="mview.showDocumentInManuscriptViewerHtml.folio"/> ${documentExplorer.folio}</span>';
alert('valorizzato con '+credit);
			
			var annotations = new Array();
			var annotationId = "${annotationId}";
			
			iip = new IIPMooViewer( "targetframe", {
				server: '${IIPImageServerURL}',
				prefix: '${ImagePrefixURL}',
				image: '${image}',
				annotationsType: 'remote',
				retrieveAnnotationsUrl: '${GetImageAnnotationURL}',
				updateAnnotationsUrl: '${UpdateAnnotationsURL}',
				maxAnnotationQuestionNumber: '${maxAnnotationQuestionNumber}',
				annotations: annotations,
				annotationId: annotationId,
				credit: credit,
				enableEdit: false,
				navigation: true,
				showNavWindow: true,
				showNavImage: true, // this property hide navigation image
				showNavButtons: true,
				winResize: true,
				zoom: 2,
				scale: 0
			});
			
			// RR The if-else condition has been removed because we always want IIPMooViewer instantiated with extended options
			/*
			if(annotationId != null && annotationId != ''){
				iip = new IIPMooViewer( "targetframe", {
					server: '${IIPImageServerURL}',
					prefix: '${ImagePrefixURL}',
					image: '${image}',
					annotationsType: 'remote',
					retrieveAnnotationsUrl: '${GetImageAnnotationURL}',
					updateAnnotationsUrl: '${UpdateAnnotationsURL}',
					maxAnnotationQuestionNumber: '${maxAnnotationQuestionNumber}',
					annotations: annotations,
					annotationId: annotationId,
					credit: credit,
					navigation: true,
					showNavWindow: true,
					showNavImage: true, // this property hide navigation image
					showNavButtons: true,
					winResize: true,
					zoom: 2,
					scale: 0
				});		
			}else{
				iip = new IIPMooViewer( "targetframe", {
					server: '${IIPImageServerURL}',
					prefix: '${ImagePrefixURL}',
					image: '${image}',
					credit: credit,
					navigation: true,
					showNavWindow: true,
					showNavImage: true, // this property hide navigation image
					showNavButtons: true,
					winResize: true,
					zoom: 2,
					scale: 0
				});		
			}
			*/

		</script>
