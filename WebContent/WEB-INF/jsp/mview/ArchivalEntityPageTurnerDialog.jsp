<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>

<c:url var="ContextPathURL" value="/" />

<c:url var="PageTurnerDialogURL" value="/src/mview/JumpToFolio.json"/>

<c:url var="SearchAjaxURL" value="${command.goToPage}" />

<c:url var="IIPImageServerURL" value="/mview/IIPImageServer.do" />

<c:url var="ImagePrefixURL" value="/images/mview/" />

<c:url var="GetImageAnnotationURL"
	value="/src/mview/GetImageAnnotation.json" />

<c:url var="UpdateAnnotationsURL"
	value="/src/mview/UpdateAnnotations.json" />

<c:url var="VolumeSummaryDialogURL" value="/src/mview/ShowSummaryVolumeDialog.do">
	<c:param name="volNum" value="${command.volNum}" />
	<c:param name="volLetExt" value="${command.volLetExt}" />
</c:url>

<div id="PageTurnerVerticalDiv">
	<div id="transcribeDiv">
		<c:if test="${command.documentCount == 0}">
			<a id="alreadyTranscribe" class="transcribeMessage" style="display: none;"><fmt:message key="mview.pageTurnerDialog.documentsAlreadyCreated"/></a>
			<a id="showAlreadyTranscribedDocs" target="_blank" href="${command.singleUploadPage}&modal=list" class="transcribe button_basic" title="<fmt:message key="mview.pageTurnerDialog.showThisDocuments.alt"/>" style="display: none; cursor: pointer;"><fmt:message key="mview.pageTurnerDialog.showThisDocuments"/></a>
			<a id="transcribeAnyway" target="_blank" href="${command.singleUploadPage}&modal=edit" class="transcribe button_basic" title="<fmt:message key="mview.pageTurnerDialog.transcribeAnyway.alt"/>" style="display: none; cursor: pointer; border-radius: 0px; border: 0px;"><fmt:message key="mview.pageTurnerDialog.transcribeAnyway"/></a>
			<a id="readyToTranscribe" target="_blank" href="${command.singleUploadPage}&modal=edit" class="transcribe button_basic" title="<fmt:message key="mview.pageTurnerDialog.readyToTranscribe.alt"/>" style="display: block; cursor: pointer; border-radius: 0px; border: 0px;"><fmt:message key="mview.pageTurnerDialog.readyToTranscribe"/> </a>
		</c:if>
		<c:if test="${command.documentCount > 0}">
			<a id="alreadyTranscribe" class="transcribeMessage" style="display: block;"><fmt:message key="mview.pageTurnerDialog.documentsAlreadyCreated"/></a>
			<a id="showAlreadyTranscribedDocs" target="_blank" href="${command.singleUploadPage}&modal=list" class="transcribe button_basic" title="<fmt:message key="mview.pageTurnerDialog.showThisDocuments.alt"/>" style="display: block; cursor: pointer;"><fmt:message key="mview.pageTurnerDialog.showThisDocuments"/></a>
			<a id="transcribeAnyway" target="_blank" href="${command.singleUploadPage}&modal=edit" class="transcribe button_basic" title="<fmt:message key="mview.pageTurnerDialog.transcribeAnyway.alt"/>" style="display: block; cursor: pointer;"><fmt:message key="mview.pageTurnerDialog.transcribeAnyway"/></a>
			<a id="readyToTranscribe" target="_blank" href="${command.singleUploadPage}&modal=edit" class="transcribe button_basic" title="<fmt:message key="mview.pageTurnerDialog.readyToTranscribe.alt"/>" style="display: none; cursor: pointer;"><fmt:message key="mview.pageTurnerDialog.readyToTranscribe"/> </a>
		</c:if>
    </div>
	<div id="line"></div>
	<div align="center">
		<b>Turn Pages</b>
	</div>
	<br />
	<div id="prevNextButtons">
		<div id="prevButton">
			<c:if test="${command.imagePosition == 0}">
				<a id="previous" href='javascript:void(0);'
					title="<fmt:message key="mview.pageTurnerDialog.previousFolio"/>"></a>
			</c:if>
			<c:if test="${command.imagePosition > 0}">
				<a id="previous" href="${command.previousPage}"
					title="<fmt:message key="mview.pageTurnerDialog.previousFolio"/>"></a>
			</c:if>
		</div>
		<div id="folio" title="Warning!" style="display: none">
			<p>
				<span class="ui-icon ui-icon-alert"
					style="float: left; margin: 0 7px 20px 0;"></span>
				<fmt:message
					key="mview.pageTurnerDialog.saveBeforeUsingPageTurner.alert" />
			</p>
		</div>
		<div id="nextButton">
			<c:if test="${command.imagePosition == command.total - 1}">
				<a id="next" href='javascript:void(0);'
					title="<fmt:message key="mview.pageTurnerDialog.previousFolio"/>"></a>
			</c:if>
			<c:if test="${command.imagePosition < command.total - 1}">
				<a id="next" href="${command.nextPage}"
					title="<fmt:message key="mview.pageTurnerDialog.nextFolio"/>"></a>
			</c:if>
		</div>
	</div>

	<div id="line"></div>

	<div id="folioMoveTo">
		<div id="insertErrorClient" class="errorClient" display="none"
			style="color: red;"></div>
		<div id="folioErrorClient" class="errorClient" display="none"
			style="color: red;"></div>
		<form:form id="moveToFolioForm" method="post" class="edit"
			action="${PageTurnerDialogURL}">
			<div class="goToPage">
				<span align="center"> <b>Go To page</b> <a class="helpIcon"
					title="Specify here the folio number you want to jump to without recto or verso">?</a>
				</span>

				<fmt:message key="mview.pageTurnerDialog.folio" />
				:
				<input id="imageProgTypeNum" name="imageProgTypeNum"
					class="input_4c" type="text" value="" /> <input
					id="missedNumbering" name="missedNumbering" class="input_4c"
					type="text" style="display: none" value="" />
				<!-- 				</div> -->
				<input id="go" class="button_mini" type="submit" value="Go"
					onclick="return validateForm();" />
			</div>

<%-- 			<form:hidden path="entryId" /> --%>
<%-- 			<form:hidden path="volNum" /> --%>
<%-- 			<form:hidden path="volLetExt" /> --%>
<%-- 			<form:hidden path="imageType" value="C" /> --%>
<%-- 			<form:hidden path="imageOrder" /> --%>
<%-- 			<form:hidden path="total" value="${command.total}" /> --%>
<%-- 			<form:hidden path="totalRubricario" --%>
<%-- 				value="${command.totalRubricario}" /> --%>
<%-- 			<form:hidden path="totalCarta" value="${command.totalCarta}" /> --%>
<%-- 			<form:hidden path="totalAppendix" value="${command.totalAppendix}" /> --%>
<%-- 			<form:hidden path="totalOther" value="${command.totalOther}" /> --%>
<%-- 			<form:hidden path="totalGuardia" value="${command.totalGuardia}" /> --%>
<%-- 			<form:hidden path="modeEdit" value="${command.modeEdit}" /> --%>
<%-- 			<form:hidden id="formSubmitting" path="formSubmitting" --%>
<%-- 				value="${command.formSubmitting}" /> --%>
		</form:form>
	</div>

	<div id="line"></div>

	<div align="center">
		<a id="volumeSummary" href="#"
			title="<fmt:message key="mview.pageTurnerDialog.volumeSummary.icon.title"/>"></a>
	</div>
	<div id="notFound" title="Alert" style="display:none">
		<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><fmt:message key="mview.pageTurnerDialog_horizontal.folioMissing.alert"/></p>
	</div>
</div>


<script type="text/javascript">
		$j(document).ready(function() {
			
			/**
			 * This function displays the given sections (messages or buttons).
			 * Possible sections are:
			 * 		#alreadyTranscribe 			-> message
			 *		#choiceThisFolioStart 		-> button 
			 *		#extractTranscribe			-> button
			 *		#lettersHere				-> message
			 *		#notExtract					-> message
			 *		#readyToTranscribe			-> button
			 *		#showAlreadyTranscribed		-> button
			 *		#showAlreadyTranscribedDocs	-> button
			 *		#showTranscription			-> button
			 *		#transcribeAnyway			-> button
			 *		#transcriptionsHere			-> message
			 *		#unvailableTranscribe		-> message
			 *
			 * @params sections the sections to show (listed in a js-array)
			 */
			function display(sections) {
				var notDisplayedSections = new Array(
					"#alreadyTranscribe",
					"#choiceThisFolioStart",
					"#extractTranscribe",
					"#lettersHere",
					"#notExtract",
					"#readyToTranscribe",
					"#showAlreadyTranscribed",
					"#showAlreadyTranscribedDocs",
					"#showTranscription",
					"#transcribeAnyway",
					"#transcriptionsHere",
					"#unvailableTranscribe"
				);
				
				if (typeof sections !== "undefined") {
					var sec;
					if (Object.prototype.toString.apply(sections) !== '[object Array]') {
						sec = new Array();
						sec[0] = sections;
					} else {
						sec = sections;
					}
					
					for(i = 0; i < sec.length; i++) {
						var selector = $j(sec[i]);
						if (typeof selector !== "undefined") {
							var idx = notDisplayedSections.indexOf(sec[i]);
							if (idx > -1) {
								notDisplayedSections.splice(idx, 1);
							}
							$j(selector).css('display', 'block');
						}
					}
				}
				
				for (i = 0; i < notDisplayedSections.length; i++) {
					var selector = $j(notDisplayedSections[i]);
					if (typeof selector !== "undefined") {
						$j(selector).css('display', 'none');
					}
				}
			};
			
			var annotations = new Array();
			
			var pageTurnerParams = {
				searchUrl: '${SearchAjaxURL}', 
// 		        getLinkedDocumentUrl:  '${LinkedDocumentUrl}',
				imagePrefix: '${ImagePrefixURL}', 
				IIPImageServer: '${IIPImageServerURL}', 
				annotationsType: 'remote',
				retrieveAnnotationsUrl: '${GetImageAnnotationURL}',
				updateAnnotationsUrl: '${UpdateAnnotationsURL}',
				maxAnnotationQuestionNumber: '${maxAnnotationQuestionNumber}',
				annotations: annotations,
				textRepository: '<span style=\'font-size:16px\'><fmt:message key="mview.showDocumentInManuscriptViewerHtml.repository"/>',
				textCollection: '<fmt:message key="mview.showDocumentInManuscriptViewerHtml.collection"/>',
				textVolume: '<fmt:message key="mview.showDocumentInManuscriptViewerHtml.volume"/>',
			    textExtension: '<fmt:message key="mview.credits.extension"/>',
			    textInsert: '<fmt:message key="mview.credits.insert"/>',
			    textIndexOfNames: '<fmt:message key="mview.credits.indexOfNames"/>',
			    textFolio: '<fmt:message key="mview.showDocumentInManuscriptViewerHtml.folio"/>',
			    textAttachment: '<fmt:message key="mview.credits.attachment"/>',
			    textGuardia : '<fmt:message key="mview.credits.guardia"/>',
			    textCoperta : '<fmt:message key="mview.credits.coperta"/>',
			    textSpine : '<fmt:message key="mview.credits.spine"/>',
			    textRecto : '<fmt:message key="mview.credits.recto"/>',
			    textVerso : '<fmt:message key="mview.credits.verso"/>',
				<security:authorize ifAnyGranted="ROLE_ADMINISTRATORS, ROLE_ONSITE_FELLOWS, ROLE_FELLOWS">
				canTranscribe: 'true'
				</security:authorize>
			};
			
			$j("#moveToFolioForm").pageTurnerForm(pageTurnerParams);
			$j("#indexNames").pageTurnerPage(pageTurnerParams);
			$j("#previous").pageTurnerPage(pageTurnerParams);
			$j("#next").pageTurnerPage(pageTurnerParams);
			
			
			$j("#notFound").dialog({
				resizable: false,
				height:140,
				modal: true,
				autoOpen : false,
				overlay: {
					backgroundColor: '#000',
					opacity: 0.5
				}
			});
			
			validateForm = function() {
				return true;
			}
			
			var $dialogVolumeSummary = $j('<div id="DialogVolumeSummaryDiv"></div>').dialog({
				resizable: false,
				width: 550,
				height: 600, 
				title: '<fmt:message key="mview.pageTurnerDialog.volumeSummaryDialog.title"/>',
				modal: true,
				autoOpen : false,
				zIndex: 3999,
				open: function(event, ui) { 
            		$(this).load('${VolumeSummaryDialogURL}');
           		},
				overlay: {
					backgroundColor: '#000',
					opacity: 0.5
				}
			});
			
			$j('#volumeSummary').click(function(){
				if ($dialogVolumeSummary.dialog("isOpen")) {
					$dialogVolumeSummary.dialog("close");;
					return false;
				} else {
					$dialogVolumeSummary.dialog("open");
					return false;
				}
			});
		});
	</script>
