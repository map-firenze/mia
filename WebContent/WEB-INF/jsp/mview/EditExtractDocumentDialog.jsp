<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

	<c:url var="ShowDocumentURL" value="/src/docbase/ShowDocument.do">
		<c:param name="entryId" value="${command.documentEntityId}"></c:param>
	</c:url>
	<c:url var="EditExtractOrSynopsisDocumentModalWindowURL" value="/de/docbase/EditExtractOrSynopsisDocument.do">
			<c:param name="entryId"   value="${command.documentEntityId}" />
			<c:param name="modalWindow"   value="true" />
		</c:url>
	
	<c:url var="editExtractDocumentDialogURL" value="/src/mview/EditExtractDocumentDialog.do"/>

	<div id="confirm" title="Alert" style="display:none">
		<p id="overwriteMessage"><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><fmt:message key="mview.pageTurnerDialog.confirmOverwrite.alert"/></p>
	</div>
	
	<div id="confirmAndExit" title="Alert" style="display:none">
		<p id="overwriteMessageAndExit"><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><fmt:message key="mview.pageTurnerDialog.confirmOverwrite.alert"/></p>
	</div>
			
	<div id="exit" title="Alert" style="display:none">
		<p id="closeMessage"><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><fmt:message key="mview.pageTurnerDialog.exitWithoutSaving.alert"/></p>
	</div>

	<div id="saveBeforeChange" title="Alert" style="display:none">
		<p id="closeMessageBeforeChange"><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><fmt:message key="mview.pageTurnerDialog.exitWithoutSaving.alert2"/></p>
	</div>

	<div id="EditExtractDocumentDiv">
	<%-- Loading div when saving the form --%>
	<div id="loadingDiv"></div>
	<form:form id="EditExtractDocumentForm" method="post" action="${editExtractDocumentDialogURL}" cssClass="edit">
		<form:textarea id="extract" path="docExtract" rows="22"/>
		
		<input id="save" class="button_small" style="cursor:pointer;" type="submit" value="Save"/>
		<input id="saveAndExit" class="button_medium" style="cursor:pointer;" type="submit" value="Save and Exit"/>
        <!-- <input id="editSynopsis" class="button_medium" type="submit" value="Edit Synopsis"/> -->
		
		<form:hidden path="documentEntityId"/>		
		<form:hidden path="docTranscriptionId" />
		<form:hidden path="uploadedFileId"/>
		<form:hidden path="confirmOverwrite"/>
	</form:form>
	</div>
	
	<div id="saveSuccess" title="Alert" style="display:none">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Your transcription has been saved!</p>
	</div>

	<script type="text/javascript">
		$j(document).ready(function() {
			$j("#EditExtractDocumentDiv").dialog("option" , "position" , ['center', 'middle']);
			$j("#EditExtractDocumentForm :input").change(function(){
				$j("#editExtractModify").val(1);
				return false;
			});
			
			$j("#save").click(function (){
				if (extractChanged) {
					$j("#editExtractModify").val(0);
		    		//Disabilito tasti
		    		$j("#save").prop( "disabled", true );
		    		$j("#saveAndExit").prop( "disabled", true );
		        	$j("#loadingDiv").css('visibility', 'visible');
					$j("#loadingDiv").css('height', $j("#loadingDiv").parent().height());
					$j("#loadingDiv").css('width', $j("#loadingDiv").parent().width());
					$j.ajax({ type:"POST", url:$j("#EditExtractDocumentForm").attr("action"), data:$j("#EditExtractDocumentForm").serialize(), async:false, success:function(html) { 
							$j("#EditExtractDocumentDiv").html(html);
							$j("#save").prop( "disabled", false );
				    		$j("#saveAndExit").prop( "disabled", false );
							$j("#loadingDiv").css('visibility', 'hidden');
							if ($j("#confirmOverwrite").val()){
								$j("#confirmOverwrite").val(false);
								$j('#confirm').dialog("open");
							} else {
								extractChanged=false;
								$j("#saveSuccess").dialog("open");
							}
						} 
					});
				}
				return false;
			});
			
			
			$j("#saveAndExit").click(function (){
				if (extractChanged) {
					$j("#editExtractModify").val(0);
					if($j("#editSynopsisModify").val() == 1){
						$j.ajax({ type:"POST", url:$j("#EditSynopsisDocumentForm").attr("action"), data:$j("#EditSynopsisDocumentForm").serialize(), async:false, success:function(html) { 
							$j("#EditSynopsisDocumentDiv").html(html);
							synopsisChanged=false;
						}
						});
					}
					$j("#loadingDiv").css('height', $j("#loadingDiv").parent().height());
					$j("#loadingDiv").css('width', $j("#loadingDiv").parent().width());
		        	$j("#loadingDiv").css('visibility', 'visible');
					$j.ajax({ type:"POST", url:$j("#EditExtractDocumentForm").attr("action"), data:$j("#EditExtractDocumentForm").serialize(), async:false, success:function(html) { 
							$j("#EditExtractDocumentDiv").html(html);
							$j("#loadingDiv").css('visibility', 'hidden');
							if ($j("#confirmOverwrite").val()){
								$j("#confirmOverwrite").val(false);
								$j('#confirmAndExit').dialog("open");
							} else {
								extractChanged=false;
								$j('#exit').dialog('open');
								if($j("#editExtractModify").val() == 1){
									$j("#closeMessage").text("<fmt:message key="mview.pageTurnerDialog.exitWithoutSaving.alert"/>");
								}else{
									$j("#closeMessage").text("<fmt:message key="mview.pageTurnerDialog.exit.alert"/>");
								}
								window.opener.$j("#body_left").load('${ShowDocumentURL}');
							}
						} 
					});
				} else {
					$j('#exit').dialog('open');
					if($j("#editExtractModify").val() == 1 || $j("editSynopsisModify").val() == 1){
						$j("#closeMessage").text("<fmt:message key="mview.pageTurnerDialog.exitWithoutSaving.alert"/>");
					}else{
						$j("#closeMessage").text("<fmt:message key="mview.pageTurnerDialog.exit.alert"/>");
					}
				}
				return false;
			});
			
			
			$j("#editSynopsis").click(function (){
				if (extractChanged) {
					$j("#editExtractModify").val(0);
						$j.ajax({ type:"POST", url:$j("#EditExtractDocumentForm").attr("action"), data:$j("#EditExtractDocumentForm").serialize(), async:false, success:function(html) { 
							$j("#synopsis").focus();
							$j("#EditExtractDocumentDiv").html(html);
							extractChanged=false;
							window.opener.$j("#body_left").load('${ShowDocumentURL}');
							if($j("#EditSynopsisDocumentDiv").dialog("isOpen"))
								$j("#EditSynopsisDocumentDiv").dialogExtend( "restore" );
							else
								$j("#EditSynopsisDocumentDiv").dialog( "open" );
							} 
						});
				}
				else {
					if($j("#EditSynopsisDocumentForm").length > 0)
							return false;
					else
						$j("#EditSynopsisDocumentDiv").dialog("open");
				}
				//$j("#EditExtractDocumentDiv").dialog("option" , "position" , ['left', 'middle']);
				//$j("#EditSynopsisDocumentDiv").dialog( "open" );

				return false;
			});

			$j("#exit").dialog({
				resizable: false,
				height:150,
				modal: true,
				autoOpen : false,
				overlay: {
					backgroundColor: '#000',
					opacity: 0.5
				},
				buttons: {
					YES : function() {
// 						window.onbeforeunload = function(e) {
// 						        return;
// 						}
						$j(this).dialog('close');
						window.close();
					},
					NO: function() {
						$j(this).dialog('close');
					}
					
				}
			});

			$j("#confirm").dialog({
				resizable: false,
				height:150,
				modal: true,
				autoOpen : false,
				overlay: {
					backgroundColor: '#000',
					opacity: 0.5
				},
				buttons: {
					YES : function() {
						$j(this).dialog('close');
						$j("#editExtractModify").val(0);
			    		//Disabilito tasti
			    		$j("#save").prop( "disabled", true );
			    		$j("#saveAndExit").prop( "disabled", true );
			        	$j("#loadingDiv").css('visibility', 'visible');
						$j("#loadingDiv").css('height', $j("#loadingDiv").parent().height());
						$j("#loadingDiv").css('width', $j("#loadingDiv").parent().width());
						$j.ajax({ type:"POST", url:$j("#EditExtractDocumentForm").attr("action"), data:$j("#EditExtractDocumentForm").serialize(), async:false, success:function(html) { 
								extractChanged=false;
								$j("#EditExtractDocumentDiv").html(html);
								$j("#save").prop( "disabled", false );
					    		$j("#saveAndExit").prop( "disabled", false );
								$j("#loadingDiv").css('visibility', 'hidden');
								$j("#saveSuccess").dialog("open");
							} 
						});
					},
					NO: function() {
						$j("#docTranscriptionId").val("");
						$j(this).dialog('close');
					}
				}
			});			

			$j("#confirmAndExit").dialog({
				resizable: false,
				height:150,
				modal: true,
				autoOpen : false,
				overlay: {
					backgroundColor: '#000',
					opacity: 0.5
				},
				buttons: {
					YES : function() {
						$j(this).dialog('close');
						$j("#editExtractModify").val(0);
						$j("#loadingDiv").css('height', $j("#loadingDiv").parent().height());
						$j("#loadingDiv").css('width', $j("#loadingDiv").parent().width());
			        	$j("#loadingDiv").css('visibility', 'visible');
						$j.ajax({ type:"POST", url:$j("#EditExtractDocumentForm").attr("action"), data:$j("#EditExtractDocumentForm").serialize(), async:false, success:function(html) { 
								$j("#EditExtractDocumentDiv").html(html);
								$j("#loadingDiv").css('visibility', 'hidden');
								extractChanged=false;
								$j('#exit').dialog('open');
								if($j("#editExtractModify").val() == 1){
									$j("#closeMessage").text("<fmt:message key="mview.pageTurnerDialog.exitWithoutSaving.alert"/>");
								}else{
									$j("#closeMessage").text("<fmt:message key="mview.pageTurnerDialog.exit.alert"/>");
								}
								window.opener.$j("#body_left").load('${ShowDocumentURL}');
							} 
						});
					},
					NO: function() {
						$j("#docTranscriptionId").val("");
						$j('#exit').dialog('open');
						if($j("#editExtractModify").val() == 1){
							$j("#closeMessage").text("<fmt:message key="mview.pageTurnerDialog.exitWithoutSaving.alert"/>");
						}else{
							$j("#closeMessage").text("<fmt:message key="mview.pageTurnerDialog.exit.alert"/>");
						}
						$j(this).dialog('close');
					}
				}
			});	
			
// 			$j("#saveAndEditSynopsis").click(function (){
// 				if (extractChanged) {
// 					$j("#editModify").val(0);
// 						$j.ajax({ type:"POST", url:$j("#EditExtractDocumentForm").attr("action"), data:$j("#EditExtractDocumentForm").serialize(), async:false, success:function(html) { 
// 							$j("#synopsis").focus();
// 							$j("#EditExtractDocumentDiv").html(html);
// 							extractChanged=false;
// 							window.opener.$j("#body_left").load('${ShowDocumentURL}');
// 							window.opener.Modalbox.show('${EditExtractOrSynopsisDocumentModalWindowURL}', {title: "Edit Synopsis", width: 850, height:550});
// 							window.close();
// 							} 
// 						});
// 				}
// 				else {
// 					window.opener.$j("#body_left").load('${ShowDocumentURL}');
// 					window.opener.Modalbox.show('${EditExtractOrSynopsisDocumentModalWindowURL}', {title: "Edit Synopsis", width: 850, height:550});
// 					window.close();
// 				}
			
// 				return false;
// 			});

			$j("#extract").change(function(){
				extractChanged=true;
			});
			
			$j("#saveSuccess").dialog({
				resizable: false,
				height:150,
				modal: true,
				autoOpen : false,
				title: '<fmt:message key="mview.editExtractDocumentDialog.transcriptionSaved"/>',
				overlay: {
					backgroundColor: '#000',
					opacity: 0.5
				},
				buttons: {
					OK : function() {
						$j(this).dialog('close');
					}
				}
			});
						
		});
	</script>

<span id="transcribeMode" class="transcribeMessage" style="display: none;"><fmt:message key="mview.editExtractDocumentDialog.youAreTranscribingFrom"/><br /><fmt:message key="mview.editExtractDocumentDialog.youAreTranscribingFrom.folio"/>: ${folioNum} <fmt:message key="mview.editExtractDocumentDialog.youAreTranscribingFrom.volume"/>: ${volNum}</span>
