<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

	<c:url var="ReverseProxyIIPImage" value="/mview/IIPImageServer.do"/>
	<c:url var="GetImageAnnotationURL" value="/src/mview/GetImageAnnotation.json">
		<c:param name="imageId" value="${archivalEntityExplorer.image.imageId}"></c:param>
		<c:param name="imageName" value="${archivalEntityExplorer.image.imageName}"></c:param>
	</c:url>
	<c:url var="UpdateAnnotationsURL" value="/src/mview/UpdateAnnotations.json">
		<c:param name="imageId" value="${archivalEntityExplorer.image.imageId}"></c:param>
		<c:param name="imageName" value="${archivalEntityExplorer.image.imageName}"></c:param>
	</c:url>
	<c:url var="ImagePrefixURL" value="/images/mview/"/>

 	<c:url var="PageTurnerDialogUrl" value="/src/mview/ArchivalEntityPageTurnerDialog.do" >
		<c:param name="archivalEntityId" value="${archivalEntityExplorer.archivalEntityId}" />
		<c:param name="imagePosition" value="${archivalEntityExplorer.imagePosition}" />
		<c:param name="nextPage" value="${archivalEntityExplorer.nextPage}" />
		<c:param name="previousPage" value="${archivalEntityExplorer.previousPage}" />
		<c:param name="goToPage" value="${archivalEntityExplorer.goToPage}" />
		<c:param name="total" value="${archivalEntityExplorer.total}" />
		<c:param name="volLetExt" value="${archivalEntityExplorer.volLetExt}" />
		<c:param name="volNum" value="${archivalEntityExplorer.volNum}" />
		<c:param name="documentCount" value="${archivalEntityExplorer.documentCount}" />
		<c:param name="singleUploadPage" value="${archivalEntityExplorer.singleUploadPage}" />
	</c:url>
		
		<script type="text/javascript">
			var $j = jQuery.noConflict();
			$j(document).ready(function() {
				$j.ajaxSetup ({
					// Disable caching of AJAX responses */
					cache: false
				});
				
				// RR: Added volume informations and insert informations (if needed)
				var volExt = "${archivalEntityExplorer.image.volLetExt}";
				var insNum = "${archivalEntityExplorer.image.insertNum}";
				var insExt = "${archivalEntityExplorer.image.insertLet}";
				var imageName = "${archivalEntityExplorer.image.imageName}";
				var annotations = new Array();
				
				// MIA section
				var repository = "${imagePreview.repositoryName}";
				var collection = "${imagePreview.collectionName}";
				var series = "${imagePreview.seriesTitle}";
				var volume = "${imagePreview.volumeName}";
				var insert = "${imagePreview.insertName}";
				var firstFolioNumber = "${imagePreview.firstFolio.folioNumber}";
				var firstFolioRectoverso = "${imagePreview.firstFolio.rectoverso}";
				var notNumberedFirstFolio = "${imagePreview.firstFolio.noNumb}";
				var secondFolioNumber = "${imagePreview.secondFolio.folioNumber}";
				var secondFolioRectoverso = "${imagePreview.secondFolio.rectoverso}";
				var notNumberedSecondFolio = "${imagePreview.secondFolio.noNumb}";
			
				var credit = '<span style=\'font-size:16px\'>' + repository + ' - '  + collection;
				if ('' != series) credit+=' - ' + series;
				credit+=' - ' + volume 
				if ('' != insert) credit+=' - Insert: ' + insert;
					
				if ('' != firstFolioNumber) {
					credit += ' - ' + firstFolioNumber + '&nbsp' + firstFolioRectoverso;
				} else {
					if (notNumberedFirstFolio == 'true') credit += ' - ' + 'Not Numbered';
				}

				if ('' != secondFolioNumber) {
					credit += ' - ' + secondFolioNumber + '&nbsp' + secondFolioRectoverso;
				} else {
					if (notNumberedSecondFolio == 'true') credit += ' - ' + 'Not Numbered';
				}
				
				iip = new IIPMooViewer( "targetframe", {
					server: '${ReverseProxyIIPImage}',
					prefix: '${ImagePrefixURL}',
					image: '${archivalEntityExplorer.pathToImage}',
					zoom: 3,
					credit: credit,
					navWinPos: 'left',
					annotationsType: 'remote',
					retrieveAnnotationsUrl: '${GetImageAnnotationURL}',
					updateAnnotationsUrl: '${UpdateAnnotationsURL}',
					maxAnnotationQuestionNumber: '${maxAnnotationQuestionNumber}',
					enableEdit: true,
					annotations: annotations
				});
				
				var $pageTurner = $j('<div id="PageTurnerVerticalDiv"></div>')
				.dialog({                                                                                                                                                                   
					autoOpen: true,
					resizable: false,
					width: 145,
					height: 380, 
					minWidth: 145,
					maxWidth: 145,
					maxHeight: 380,
					
					                                                                                                                                                         
					//title: 'Page Turner',
					title: '<fmt:message key="mview.showDocumentInManuscriptViewerHtml.pageTurnerWindow.title"/>',
					position: ['right','top'],                                                                                                                                                       
					closeOnEscape: false,
					open: function(event, ui) { 
						$j(".ui-dialog-titlebar-close").hide();
                		$(this).load('${PageTurnerDialogUrl}');
               			},
					dragStart: function(event, ui) {$j(".ui-widget-content").css('opacity', 0.30);},
					dragStop: function(event, ui) {$j(".ui-widget-content").css('opacity', 1);}
				}).dialogExtend({"minimize" : true});

				//To manage the form of the annotation
				$j("input[name=category]:radio").die();
				$j("input[name=category]:radio").live('change', function(){
					if ($j("input[name=category]:checked").val() == 'GENERAL') {
						$j(".annotation form").parent().css("background-color", "rgba(255, 255, 0, 0.2)");
						$j("#annotationTextarea").css("display", "inherit");
					} else if($j("input[name=category]:checked").val() == 'PALEOGRAPHY') {
						$j(".annotation form").parent().css("background-color", "rgba(255, 130, 0, 0.2)");
						$j("#annotationTextarea").css("display", "inherit");
					} else if ($j("input[name=category]:checked").val() == 'PERSONAL') {
						$j("#annotationTextarea").css("display", "inherit");
						$j(".annotation form").parent().css("background-color", "rgba(180, 0, 255, 0.2)");
					} else {
						$j("#annotationTextarea").css("display", "none");
					}
				});
				
				$j(".delAnnotation").die()
				$j(".delAnnotation").live('click',function(e){
					e.preventDefault();
									
					var div = $j(this);
					$j(this).parent().block({ message: $j('#question'),
						css: { 
							border: 'none', 
							padding: '5px',
							boxShadow: '1px 1px 10px #666',
							'-webkit-box-shadow': '1px 1px 10px #666'
							} ,
							overlayCSS: { backgroundColor: '#999' }	
					}); 
					
					$j("#noQuestion").die();
					$j('#noQuestion').live('click', function() {
						$j.unblockUI();
						$j(".blockUI").fadeOut("slow");
						$j("#question").hide();
						$j(div).parent().append($j("#question"));
						$j(".blockUI").remove();
						return false; 
					}); 
				});

			});
		</script> 

		  