<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

	<c:url var="editExtractDocumentDialogURL" value="/src/mview/EditExtractDocumentDialog.do"/>
	
	<c:url var="ShowSynopsisDialogURL" value="/src/mview/ShowSynopsisDocumentDialog.do" />

	<div id="ShowExtractDocumentDiv">
		<security:authorize ifAnyGranted="ROLE_ADMINISTRATORS, ROLE_ONSITE_FELLOWS, ROLE_FELLOWS, ROLE_COMMUNITY_USERS">
			<div id="content-scroller" style="overflow: auto; height: auto; max-height: 485px;">
				<div id="content">${docExtract.replaceAll("(\\r\\n|\\n)", "<br />")}</div>			
			</div>
		</security:authorize>
		<div id="showExtractDocumentCommands">
			<security:authorize ifAnyGranted="ROLE_ADMINISTRATORS, ROLE_ONSITE_FELLOWS, ROLE_FELLOWS, ROLE_COMMUNITY_USERS">
				<input id="editExtract" class="button_medium" type="submit" value="<fmt:message key="mview.showExtractDocumentDialog.editTranscription.alt"/>"/>
			</security:authorize>
			<input id="showSynopsis" class="button_medium" type="submit" value="<fmt:message key="mview.showExtractDocumentDialog.showSynopsis.alt"/>"/>
			<input id="exitExtract" class="button_small" type="submit" value="<fmt:message key="mview.showExtractDocumentDialog.close.button"/>"/>
		</div>	
    </div>
    
    <input type="hidden" id="extractEntryId" value="${entryId}" />
	
	<script type="text/javascript">
		$j(document).ready(function() {
			$j("#ShowExtractDocumentDiv").dialog("option" , "position" , ['center', 'middle']);
			
			
					$j("#ShowExtractDocumentDiv").dialog("close");
					if($j("#ShowSynopsisDocumentDiv").length != 0 && $j("#ShowSynopsisDocumentDiv").dialog("isOpen")){
						$j("#ShowSynopsisDocumentDiv").dialog("close");
					}
					$j("#EditExtractDocumentDiv").dialog("open");
					$j("#unvailableTranscribe").css('visibility', 'hidden');
					$j("#alreadyTranscribe").css('visibility', 'hidden');
					$j("#showAlreadyTranscribed").css('visibility', 'hidden');
					$j("#transcribeAnyway").css('visibility','hidden');
					$j("#notExtract").css('visibility', 'hidden');
					$j("#extractTranscribe").css('visibility', 'hidden');
					$j("#readyToTranscribe").css('visibility', 'hidden');
					$j("#choiceThisFolioStart").css('visibility', 'hidden');
					$j("#transcribeDiv").append($j("#transcribeModeFromShow"));
					$j("#transcribeModeFromShow").css('display', 'inline');
					return false;
			
		});
	</script>
	
<%-- 	<span id="transcribeModeFromShow" class="transcribeMessage" style="display: none;"><fmt:message key="mview.showExtractDocumentDialog.youAreTranscribingFrom"/><br /><fmt:message key="mview.showExtractDocumentDialog.youAreTranscribingFrom.folio"/>: ${folioNum} <fmt:message key="mview.showExtractDocumentDialog.youAreTranscribingFrom.volume"/>: ${volNum}</span> --%>
