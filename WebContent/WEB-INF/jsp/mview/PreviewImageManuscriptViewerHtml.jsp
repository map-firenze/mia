<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

	<c:url var="IIPImageServerURL" value="/mview/IIPImageServer.do"/>
	<c:url var="ImagePrefixURL" value="/images/mview/"/>
	<c:url var="GetImageAnnotationURL" value="/src/mview/GetImageAnnotation.json">
		<c:param name="imageId" value="${image.imageId}"></c:param>
		<c:param name="imageName" value="${image.imageName}"></c:param>
	</c:url>
	<c:url var="UpdateAnnotationsURL" value="/src/mview/UpdateAnnotations.json">
		<c:param name="imageId" value="${image.imageId}"></c:param>
		<c:param name="imageName" value="${image.imageName}"></c:param>
	</c:url>

	
		<script type="text/javascript">
 			// MIA variables
 			var volNum = "${image.volNum}"
			var imageName = "${image.imageName}";
			var repository = "${imagePreview.repositoryName}";
			var collection = "${imagePreview.collectionName}";
			var series = "${imagePreview.seriesTitle}";
			var volume = "${imagePreview.volumeName}";
			var insert = "${imagePreview.insertName}";
									
			var firstFolioNumber = "${imagePreview.firstFolio.folioNumber}";
			var firstFolioRectoverso = "${imagePreview.firstFolio.rectoverso}";
			var secondFolioNumber = "${imagePreview.secondFolio.folioNumber}";
			var notNumberedFirstFolio = "${imagePreview.firstFolio.noNumb}";
			var secondFolioRectoverso = "${imagePreview.secondFolio.rectoverso}";
			var notNumberedSecondFolio = "${imagePreview.secondFolio.noNumb}";
			
			// MIA variables end
			
			var credit = '<span style=\'font-size:16px\'>' + repository + ' - '  + collection;
			if ('' != series) credit+=' - ' + series;
			credit+=' - ' + volume 
			if ('' != insert) credit+=' - ' + insert;
				
			if ('' != firstFolioNumber) {
				credit += ' - ' + firstFolioNumber + '&nbsp' + firstFolioRectoverso;
			} else {
				if (notNumberedFirstFolio == 'true') credit += ' - ' + 'Not Numbered';
			}

			if ('' != secondFolioNumber) {
				credit += ' - ' + secondFolioNumber + '&nbsp' + secondFolioRectoverso;
			} else {
				if (notNumberedSecondFolio == 'true') credit += ' - ' + 'Not Numbered';
			}
										
		var annotations = new Array();
			var annotationId = "${annotationId}";
			if ("${image.imageType}" == 'R') {
				credit += '<span style=\'font-size:16px\'>' + '<fmt:message key="mview.showManuscriptViewerHtml.indexOfNames"/>&nbsp;</span>';
			} else if ("${image.imageType}" == 'C') {
				credit += '<span style=\'font-size:16px\'>' + '<fmt:message key="mview.showManuscriptViewerHtml.folio"/>&nbsp;</span>';
			} else if ("${image.imageType}" == 'A') {
				credit += '<span style=\'font-size:16px\'>' + '<fmt:message key="mview.showManuscriptViewerHtml.allegato"/>&nbsp;</span>';
			} else if ("${image.imageType}" == 'G') {
				credit += '<span style=\'font-size:16px\'>' + '<fmt:message key="mview.showManuscriptViewerHtml.guardia"/>&nbsp;</span>';
			} else if ("${image.imageType}" == 'O') {
				//MD: Is it correct the imageType 'O' for "costola" and "coperta"?
				if(imageName.indexOf("COPERTA") != -1){
					credit += '<span style=\'font-size:16px\'>' + '<fmt:message key="mview.showManuscriptViewerHtml.coperta"/>&nbsp;</span>';
				}
			} else {
           		credit += ' ';
           	}
		
			credit+= '<span style=\'font-size:22px\'>' + "${image.imageProgTypeNum}";
			if ("${image.missedNumbering}") {
				credit += ' ' + "${image.missedNumbering}";
			}
			if ("${image.imageRectoVerso}" == 'R') {
				credit += '</span>' + ' recto' + '</span>';
			} else if ("${image.imageRectoVerso}" == 'V'){
				credit += '</span>' + ' verso' + '</span>';
			}
			
			//MD:The last control is to verify if the image is a spine
			if(imageName.indexOf("SPI") != -1){
				credit = '<span style=\'font-size:16px\'>' + '<fmt:message key="mview.showManuscriptViewerHtml.spine"/>' + '</span>';
			}
			
			iip = new IIPMooViewer( "targetframe", {
				server: '${IIPImageServerURL}',
				prefix: '${ImagePrefixURL}',
				image: '${image.imageName}',
				annotationsType: 'remote',
				retrieveAnnotationsUrl: '${GetImageAnnotationURL}',
				updateAnnotationsUrl: '${UpdateAnnotationsURL}',
				maxAnnotationQuestionNumber: '${maxAnnotationQuestionNumber}',
				annotations: annotations,
				annotationId: annotationId,
				credit: credit,
				// put enableEdit to true if you want to enable the annotations
				/* enableEdit: false, */
				enableEdit: true,
				navigation: true,
				showNavWindow: true,
				showNavImage: true, // this property hide navigation image
				showNavButtons: true,
				navWinPos: 'left',
				winResize: true,
				zoom: 2,
				scale: 0
			});
			
			$j(".delAnnotation").die()
			$j(".delAnnotation").live('click',function(e){
				e.preventDefault();
								
				var div = $j(this);
				$j(this).parent().block({ message: $j('#question'),
					css: { 
						border: 'none', 
						padding: '5px',
						boxShadow: '1px 1px 10px #666',
						'-webkit-box-shadow': '1px 1px 10px #666'
						} ,
						overlayCSS: { backgroundColor: '#999' }	
				}); 
				
				$j("#noQuestion").die();
				$j('#noQuestion').live('click', function() {
					$j.unblockUI();
					$j(".blockUI").fadeOut("slow");
					$j("#question").hide();
					$j(div).parent().append($j("#question"));
					$j(".blockUI").remove();
					return false; 
				}); 
			});
			
		</script>
