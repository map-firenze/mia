<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

	<div id="MIA_logo">
		<img src="<c:url value="/img/logo.png" />" title="the Medici Archive Project - MIA" />
	</div>
			
	<div id="text_error">
		<b>Error HTTP 404: Page not found.</b>
		<p>The URL you requested was not found.</p><br />
	</div>