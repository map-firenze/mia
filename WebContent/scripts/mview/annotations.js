/*
   IIPMooViewer 2.0 - Annotation Extensions
   IIPImage Javascript Viewer <http://iipimage.sourceforge.net>

   Copyright (c) 2007-2012 Ruven Pillay <ruven@users.sourceforge.net>

   ---------------------------------------------------------------------------

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   ---------------------------------------------------------------------------

*/


/* Extend IIPMooViewer to handle annotations
 */
IIPMooViewer.implement({

	/**
	 * Initialize canvas events for our annotations
	 */
	initAnnotationTips: function() {

		this.annotationTip = null;
		this.annotationsVisible = true;

		// Use closure for mouseenter and mouseleave events
		var _this = this;

		/** MEDICI ARCHIVE PROJECT START **/
		// Display / hide our annotations if we have any
		/*if (this.annotations) {
			this.canvas.addEvent('mouseenter', function() {
				if (_this.annotationsVisible) {
					_this.canvas.getElements('div.annotation').removeClass('hidden');
				}
			});
			this.canvas.addEvent('mouseleave', function() {
				if (_this.annotationsVisible) {
					_this.canvas.getElements('div.annotation').addClass('hidden');
				}
			});
		}*/

		if (this.annotations && (this.annotationId == null || this.annotationId == '')) {
			this.canvas.addEvent('mouseenter', function() {
				if (_this.annotationsVisible ) {
					_this.canvas.getElements('div.annotation').removeClass('hidden');
				}
			});
			/*this.canvas.addEvent('mouseleave', function() {
				if (_this.annotationsVisible ) {
					_this.canvas.getElements('div.annotation').addClass('hidden');
				}
			});*/
			this.canvas.addEvent('click', function() {
				if (_this.annotationsVisible && _this.annotationTip) {
					_this.annotationTip.hide();
					_this.annotationTip.detach('div.annotation');
				}
			});
		} else if (this.annotations) {
			this.canvas.addEvent('mouseenter', function() {
				if (_this.annotationsVisible ) {
					_this.canvas.getElements('div.annotation').removeClass('hidden');
				}
			});
			this.canvas.addEvent('click', function() {
				if (_this.annotationsVisible && _this.annotationTip) {
					_this.annotationTip.hide();
					_this.annotationTip.detach('div.annotation');
				}
			});
		}
		/** MEDICI ARCHIVE PROJECT END **/
	},


	/**
	 * Create annotations if they are contained within our current view
	 */
	createAnnotations: function() {
		
		var _this = this;
		
		// If there are no annotations, simply return
		if (!this.annotations || this.annotations.length == 0) {
			return;
		}
	
	    // Convert our annotation object into an array - we'll need this for sorting later
		var annotation_array = new Array();
		
		/** MEDICI ARCHIVE PROJECT START **/
		// RR: We read annotations by index position
		/*for (var a in this.annotations) {
			this.annotations[a].id = a;
			annotation_array.push(this.annotations[a]);
		}*/
		for (var count = 0; count < this.annotations.length; count++) {
			this.annotations[count].id = count;
			if (typeof this.annotations[count].visibility !== 'undefined') {
				// RR: if annotation has visibility property it can be showed:
				// if true it can be showed in full color, if false it
				// can be showed in transparency (eg. for administrator users);
				// If annotation has not visibility property it cannot be showed
				// (eg. it has to be hidden for non admin users)
				annotation_array.push(this.annotations[count]);
			}
		}
		/** MEDICI ARCHIVE PROJECT END */
	
	    // Make sure we really have some content
		if (annotation_array.length == 0) {
			return;
		}
	
	    // Sort our annotations by size to make sure it's always possible to interact
	    // with annotations within annotations.
		annotation_array.sort(
			function(a,b) {
				return (b.w * b.h) - (a.w * a.h);
			}
		);
	
	    // Now go through our sorted list and display those within the view
		for (var i = 0; i < annotation_array.length; i++) {
	
			// Check whether this annotation is within our view
			if (this.wid * (annotation_array[i].x + annotation_array[i].w) > this.view.x
					&& this.wid * annotation_array[i].x < this.view.x + this.view.w
					&& this.hei * (annotation_array[i].y + annotation_array[i].h) > this.view.y
					&& this.hei * annotation_array[i].y < this.view.y + this.view.h
					// Also don't show annotations that entirely fill the screen
					/*	  (this.hei*annotation_array[i].x < this.view.x && this.hei*annotation_array[i].y < this.view.y &&
					this.wid*(annotation_array[i].x+annotation_array[i].w) > this.view.x+this.view.w */) {
	
				var _this = this;
				var cl = 'annotation';
				if (annotation_array[i].type) {
					cl += ' ' + annotation_array[i].type;
				}
				var annotId = annotation_array[i].annotationId ? annotation_array[i].annotationId : annotation_array[i].id;
				var annotation = new Element('div', {
					'id': 'annotation_' + annotId,
					'class': cl,
					'styles': {
						left: Math.round(this.wid * annotation_array[i].x),
						top: Math.round(this.hei * annotation_array[i].y),
						width: Math.round(this.wid * annotation_array[i].w),
						height: Math.round(this.hei * annotation_array[i].h),
						opacity: annotation_array[i].visibility ? 1.0 : 0.5  
					}
				}).inject(this.canvas);
				if (typeof annotation_array[i].color !== 'undefined') {
					annotation.setStyle('background-color', IIPMooViewer.convertRgbColorToRGBA(annotation_array[i].color, '0.2'));
				}
				
				/** MEDICI ARCHIVE PROJECT START **/
				// Show annotation title and text on single click in annotation area
				this.initAnnotationClickHandler(annotation);
				annotation.setStyle('cursor', 'pointer');

				/** MEDICI ARCHIVE PROJECT END */
				
				var annotationCommandOpenClose = null;
				if (this.editEnabled && (typeof this.annotationEditing === 'undefined' || !this.annotationEditing)) {
					annotationCommandOpenClose = this.initAnnotationCommands(annotation, annotId);
				}
			
				if (this.annotationsVisible == false) {
					annotation.addClass('hidden');
					if (annotationCommandOpenClose != null) {
						annotationCommandOpenClose.addClass('hidden');
					}
				}
	
				// Add edit events to annotations if we have included the functions
				if (typeof(this.editAnnotation) == "function") {
					if (annotation_array[i].edit == true) { 
						this.editAnnotation(annotation);
					}
				}
				var hasQuestions = annotation_array[i].questions != null && annotation_array[i].questions.length > 0;
				if (hasQuestions && typeof(this.editAnnotationQuestion) == "function") {
					for (j = 0; j < annotation_array[i].questions.length; j++){
						if (annotation_array[i].questions[j].edit == true) { 
							this.editAnnotationQuestion(annotation, annotation_array[i].questions[j]);
							break;
						}
					}
				}
			
				// Add our annotation text
				var text = annotation_array[i].text;

				/** MEDICI ARCHIVE PROJECT START **/
				var textElement = new Element('span', {
					'id': 'annotationText_' + annotId,
				    'html': text					
				});
				
				if (annotation_array[i].type == 'GENERAL') {
					new Element('div', {
						'id': 'annotationOwner_' + annotId,
						'styles': {'font-variant': 'small-caps', 'font-weight': 'bold', 'margin-top': '10px'},
					    'html': 'Owner:&nbsp;<a href="/Mia/index.html#/mia/public-profile/' + annotation_array[i].account + '" style="color: #ccc; cursor: pointer" target="_blank">' + annotation_array[i].account + '</a>'
					}).inject(textElement);
					if (hasQuestions) {
						for (j = 0; j < annotation_array[i].questions.length; j++){
							var question = annotation_array[i].questions[j];
							if (!question.newQuestion) {
								var annotationQuestionElement =  new Element('div', {
									'id': 'annotationQuestionHead_' + question.id,
									'styles': {'font-variant': 'small-caps', 'font-weight': 'bold', 'margin-top': '10px'},
									'html': question.dateCreated + '&nbsp;<a href="/Mia/index.html#/mia/public-profile/' + question.account + '" style="color: #ccc; cursor: pointer" target="_blank">' + question.account + '</a>'
								}).inject(textElement);
								if (this.editEnabled && (typeof this.annotationEditing === 'undefined' || !this.annotationEditing)) {									
									this.initAnnotationQuestionCommands(annotationQuestionElement, annotation, annotId, question);
								}
								new Element('div', {
									'id': 'annotationQuestionText_' + question.id,
									'html': question.text
								}).inject(textElement);
							}
						}	
					}
					if (this.editEnabled && (typeof this.annotationEditing === 'undefined' || !this.annotationEditing)) {
						if (hasQuestions && annotation_array[i].questions.length >= _this.maxAnnotationQuestionNumber) {
							var annotationQuestionElement =  new Element('div', {
								'id': 'createAnnotationQuestion_' + annotId,
								'styles': {'font-variant': 'small-caps', 'font-weight': 'bold', 'margin-top': '10px'},
								'html': '[Maximum number of replies has been reached - see comments for related discussion]'
							}).inject(textElement);
						} else {
							this.initAnnotationQuestionCreation(textElement, annotation, annotId);
						}					
					}
				}
		
				//if (text.length > 30) {
				//	text = text.substring(0, 29) + '&hellip;';
				//}
				var title = annotation_array[i].title || 'No title';
				//if (title.length > 25) {
				//	title = title.substring(0, 24) + '&hellip;';
				//}
				annotation.store('tip:title', '<h1>'+title+'</h1>');
				/*if (annotation_array[i].title) {
					text = '<h1>'+annotation_array[i].title+'</h1>' + text;
				}*/
			
				/** MEDICI ARCHIVE PROJECT END */
				annotation.store('tip:text', textElement);
	    	}
	
		}
	
	
		if (!this.annotationTip) {
			var _this = this;
			this.annotationTip = new Tips('div.annotation', {
				className: 'tip', // We need this to force the tip in front of nav window
				fixed: true,
				offset: {
					x: 0,
					y: -60
				},
				hideDelay: 300,
				link: 'chain',
				onShow: function(tip, el) {
	
					// Fade from our current opacity to 0.9
					tip.setStyles({
						opacity: tip.getStyle('opacity'),
						display: 'block'
					}).fade(0.9);

				},
				onHide: function(tip, el) {
					if (!tip.active) {
						tip.fade('out').get('tween').chain(
							function() {
								this.element.setStyle('display','none');
							}
						);
						tip.removeEvents(['mouseenter','mouseleave']);
					}
				}
			});
			this.annotationTip.position = function(event){
				if (!this.tip) document.id(this);

				var size = window.getSize(), scroll = window.getScroll(),
					tip = {x: this.tip.offsetWidth, y: this.tip.offsetHeight},
					props = {x: 'left', y: 'top'},
					bounds = {y: false, x2: false, y2: false, x: false},
					obj = {};

				for (var z in props){
					obj[props[z]] = event.page[z] + this.options.offset[z];
					if ((obj[props[z]] + tip[z] - scroll[z]) > size[z] - this.options.windowPadding[z]){
						obj[props[z]] = event.page[z] - this.options.offset[z] - tip[z];
					}
					if (obj[props[z]] < 0) obj[props[z]] = 0;
				}

				this.fireEvent('bound', bounds);
				this.tip.setStyles(obj);
			};
			this.annotationTip.elementEnter = function(e){
				e.stop();			
			};
			this.annotationTip.elementLeave = function(e){
				e.stop();
			};
		}
	},
	
	/** MEDICI ARCHIVE PROJECT START **/
	
	/**
	 * Initializes the click handler of the annotation area.
	 *  
	 * @param annotation the annotation 'div' element
	 */
	initAnnotationClickHandler: function(annotation) {
		var _this = this;
		annotation.addEvent('click', function(e) {
			var event = new DOMEvent(e);
			if (_this.annotationEditing != true) {
				event.stop();
				clearTimeout(_this.annotationTip.timer);
				_this.annotationTip.timer = (function(){
					_this.annotationTip.container.empty();

					['title', 'text'].each(function(value){
						var content = annotation.retrieve('tip:' + value);
						var div = _this.annotationTip['_' + value + 'Element'] = new Element('div', {
								'class': 'tip-' + value
							}).inject(_this.annotationTip.container);
						if (content) _this.annotationTip.fill(div, content);
					}, _this.annotationTip);
					_this.annotationTip.show(annotation);
					_this.annotationTip.position((_this.annotationTip.options.fixed) ? {page: annotation.getPosition()} : event);
				}).delay(_this.annotationTip.options.showDelay, _this.annotationTip);
			}
		});
	},
	
	/**
	 * Initializes the annotation action commands.
	 * 
	 * @param annotation the annotation 'div' element
	 * @param annotationId the annotation identifier
	 * @returns the master annotation command (open/close command)
	 */
	initAnnotationCommands: function(annotation, annotationId) {
		var _this = this;
		var idx = 0;
		while (idx < this.annotations.length) {
			if ((typeof this.annotations[idx].annotationId === "undefined" && this.annotations[idx].id == annotationId)
					|| this.annotations[idx].annotationId == annotationId) {
				break;
			}
			idx++;
		}
		
		var isDeletable = this.annotations[idx].deletable == true;
		var isUpdatable = this.annotations[idx].updatable == true;
		var canChangeVisibility = typeof this.adminPrivileges !== 'undefined' && this.adminPrivileges &&
									typeof this.annotations[idx].visibility !== 'undefined' && this.annotations[idx].visibility != null;
		
		if (isDeletable || isUpdatable || canChangeVisibility) {
			var annotationCommandOpenClose = new Element('div', {
				'id': ('commandBtn_' + annotationId),
				'class': 'commandBtn moreBtn',
				'styles': {
					left: 0,
					top: 0
				},
				'title': 'open/close annotation commands'
			}).inject(annotation);
			
			var delta = 0;
			
			if (isDeletable) {
				var annotationDelete = new Element('div', {
					'id': ('deleteBtn_' + annotationId),
					'class': 'commandBtn deleteBtn hidden',
					'styles': {
						left: 20,
						top: delta * 22
					},
					'title': 'delete this annotation'
				}).inject(annotation);
				
				annotationDelete.addEvent('click', function(e) {
					var event = new DOMEvent(e);
					event.stop();
					if (confirm('Do you want to delete this annotation?')) {
						_this.annotations.splice(idx, 1);
						_this.updateAnnotations();
						_this.fireEvent('annotationChange', _this.annotations);
					}
				});
				delta++;
			}
			
			if (isUpdatable) {
				var annotationUpdate = new Element('div', {
					'id': ('updateBtn_' + annotationId),
					'class': 'commandBtn updateBtn hidden',
					'styles': {
						left: 20,
						top: delta * 22
					},
					'title': 'modify this annotation'
				}).inject(annotation);
				
				annotationUpdate.addEvent('click', function(e) {
					e.stop();
					_this.canvas.getElements('div.commandBtn').addClass('hidden');
					_this.editAnnotation(annotation);
				});
				delta++;
			}
			
			if (canChangeVisibility) {
				if (this.annotations[idx].visibility === true) {
					var annotationHide = new Element('div', {
						'id': ('hideBtn_' + annotationId),
						'class': 'commandBtn hideBtn hidden',
						'styles': {
							left: 20,
							top: delta * 22
						},
						'title': 'hide this annotation'
					}).inject(annotation);
					
					annotationHide.addEvent('click', function(e) {
						e.stop();
						_this.annotations[idx].visibility = false;
						_this.updateAnnotations();
						_this.fireEvent('annotationChange', _this.annotations);
					});
					delta++;
				} else {
					var annotationShow = new Element('div', {
						'id': ('showBtn_' + annotationId),
						'class': 'commandBtn showBtn hidden',
						'styles': {
							left: 20,
							top: delta * 22
						},
						'title': 'show this annotation'
					}).inject(annotation);
					
					annotationShow.addEvent('click', function(e) {
						e.stop();
						_this.annotations[idx].visibility = true;
						_this.updateAnnotations();
						_this.fireEvent('annotationChange', _this.annotations);
					});
					delta++;
				}
			}
			
			annotationCommandOpenClose.addEvent('click', function(e) {
				var event = new DOMEvent(e);
				event.stop();
				var annotationId = this.id.substring(11, this.id.length);
				if (this.hasClass('moreBtn')) {
					this.removeClass('moreBtn');
					this.addClass('lessBtn');
					_this.canvas.getElements('#updateBtn_' + annotationId + ',#deleteBtn_' + annotationId + ',#hideBtn_' + annotationId + ',#showBtn_' + annotationId).removeClass('hidden');
				} else if (this.hasClass('lessBtn')) {
					this.removeClass('lessBtn');
					this.addClass('moreBtn');
					_this.canvas.getElements('#updateBtn_' + annotationId + ',#deleteBtn_' + annotationId + ',#hideBtn_' + annotationId + ',#showBtn_' + annotationId).addClass('hidden');
				}
			});
			
			return annotationCommandOpenClose;
		}
		
		return null;
	},
	
	initAnnotationQuestionCommands: function(element, annotation, annotationId, question) {
		var _this = this;
		var annotationIndex = 0;
		var questionIndex = 0;
		while (annotationIndex < this.annotations.length) {
			var found = false;
			if (this.annotations[annotationIndex].annotationId == annotationId) {
				while (questionIndex < this.annotations[annotationIndex].questions.length) {
					if (this.annotations[annotationIndex].questions[questionIndex].id == question.id){
						found = true;
						break;
					}
					questionIndex++;
				}
			}
			if (found) {
				break;
			}
			annotationIndex++;
		}
		if (question.updatable) {
			var updateAnnotationQuestion = new Element('span', {
				'id': 'updateAnnotationQuestion_' + question.id,
				'styles': {'cursor': 'pointer', 'font-weight': 'bold', 'font-variant': 'small-caps','margin-left': '5px'},
			    'html': '[Modify reply]',
			    'events': {
			        'click': function(e) {
						var event = new DOMEvent(e);
						event.stop();
						_this.editAnnotationQuestion(annotation, question);
			        }
			    }						
			}).inject(element);
		}
		if (question.deletable) {
			var deleteAnnotationQuestion = new Element('span', {
				'id': 'deleteAnnotationQuestion_' + question.id,
				'styles': {'cursor': 'pointer', 'font-weight': 'bold', 'font-variant': 'small-caps','margin-left': '5px'},
			    'html': '[Delete reply]',
			    'events': {
			        'click': function(e) {
						var event = new DOMEvent(e);
						event.stop();
						if (confirm('Do you want to delete this reply?')) {
							_this.annotations[annotationIndex].questions.splice(questionIndex, 1);
							_this.updateAnnotations();
							_this.fireEvent('annotationChange', _this.annotations);
						}
			        }
			    }						
			}).inject(element);
		}
	},
	
	initAnnotationQuestionCreation: function(element, annotation, annotationId) {
		var _this = this;
		var idx = 0;
		while (idx < this.annotations.length) {
			if ((typeof this.annotations[idx].annotationId === "undefined" && this.annotations[idx].id == annotationId)
					|| this.annotations[idx].annotationId == annotationId) {
				break;
			}
			idx++;
		}
		
		new Element('div', {
			'id': 'createAnnotationQuestion_' + annotationId,
			'styles': {'cursor': 'pointer', 'font-weight': 'bold', 'font-variant': 'small-caps', 'margin-top': '10px'},
		    'html': '[Add a reply]',
		    'events': {
		        'click': function(e) {
					var event = new DOMEvent(e);
					event.stop();
					if (_this.annotations[idx].questions == null){
						_this.annotations[idx].questions = new Array();
					}
					var question = {
						newQuestion: true
					};
					_this.annotations[idx].questions.push(question);
					_this.editAnnotationQuestion(annotation, question);
		        }
		    }	
		}).inject(element);
	},
	
	/**
	 * Renders or hides the commands associated to annotations
	 * 
	 * @param show true if you want to render commands
	 */
	renderCommands: function(show) {
		if (!show) {
			this.canvas.getElements('div.commandBtn').each(function(el) {
				el.addClass('hidden');
			}); 
		} else {
			this.canvas.getElements('div.lessBtn').each(function(el) {
				el.removeClass('lessBtn');
				el.addClass('moreBtn');
			});
			this.canvas.getElements('div.moreBtn,div.lessBtn').each(function(el) {
				el.removeClass('hidden');
			});
		}
	},
	
	/** MEDICI ARCHIVE PROJECT END **/
	
	/**
	 * Toggle visibility of any annotations
	 */
	toggleAnnotations: function() {
		var visible = this.annotationsVisible;
		if (visible) {
			this.canvas.getElements('div.annotation').each(function(el) {
				el.addClass('hidden');
			});
			this.renderCommands(true);
		} else if (!visible) {
			this.canvas.getElements('div.annotation').each(function(el) {
				el.removeClass('hidden');
			});
			this.renderCommands(false);
		}
		if (visible) {
			this.annotationsVisible = false;
			this.showPopUp(IIPMooViewer.lang.annotationsDisabled);
		} else {
			this.annotationsVisible = true;
		}
	},


	/** 
	 * Destroy our annotations
	 */
	destroyAnnotations: function() {
		if (this.annotationTip) {
			this.annotationTip.detach(this.canvas.getChildren('div.annotation'));
		}
		this.canvas.getChildren('div.annotation').each(function(el) {
			el.eliminate('tip:text');
			el.destroy();
		});
		this.canvas.getChildren('div.commandBtn').each(function(el) {
			el.destroy();
		});
	},


	/** MEDICI ARCHIVE PROJECT START **/
	retrieveAnnotations: function() {
		if (this.annotationsType == 'remote') {
			this.annotations = new Array();

			if (this.annotationId == null || this.annotationId == '') {
				
				new Request.JSON({
					method: 'get',
					async: false,
					url: this.retrieveAnnotationsUrl,
					noCache: true,
					onRequest: function() {
						// show some rotating loader gif...
					},

					onSuccess: function(responseJSON, responseText) {
						if (typeof responseJSON.operation === 'undefined' || responseJSON.operation === 'OK') {
							this.adminPrivileges = responseJSON.adminPrivileges;
							var data = new Array();  // temporary data array
							for (i = 0; i < responseJSON.annotations.length; i++) {
								data[i] = {
									annotationId: responseJSON.annotations[i].annotationId.toInt(),
									id: responseJSON.annotations[i].id,
									account: responseJSON.annotations[i].account,
									x: responseJSON.annotations[i].x.toFloat(), 
									y: responseJSON.annotations[i].y.toFloat(), 
									w: responseJSON.annotations[i].w.toFloat(), 
									h: responseJSON.annotations[i].h.toFloat(),
									type: responseJSON.annotations[i].type,
									title: responseJSON.annotations[i].title,
									text: responseJSON.annotations[i].text,
									deletable: responseJSON.annotations[i].deletable,
									updatable: responseJSON.annotations[i].updatable,
									forumTopicURL: responseJSON.annotations[i].forumTopicURL // Link To Forum
								};
								if (typeof responseJSON.annotations[i].color !== 'undefined') {
									data[i]["color"] = responseJSON.annotations[i].color;
								}
								if (typeof responseJSON.annotations[i].visibility !== 'undefined') {
									data[i]["visibility"] = responseJSON.annotations[i].visibility;
								}
								if (typeof responseJSON.annotations[i].questions !== 'undefined') {
									var questions = new Array();
									for (j = 0; j < responseJSON.annotations[i].questions.length; j++){
										questions[j] = {
											id: responseJSON.annotations[i].questions[j].id,
											account: responseJSON.annotations[i].questions[j].account,
											text: responseJSON.annotations[i].questions[j].text,
											dateCreated: responseJSON.annotations[i].questions[j].dateCreated,
											deletable: responseJSON.annotations[i].questions[j].deletable,
											updatable: responseJSON.annotations[i].questions[j].updatable
										}
									}
									data[i]["questions"] = questions;
								}
								this.annotations.push(data[i]);
							}
						} else {
							console.log('error');
							alert('Error during annotations retrieving...');
						}
					}.bind(this),

					onError: function(text, error) {
						console.log('error!!!' + text + ' - error : ' + error);
						alert('Error: annotation retrieving server call failed!!!');
					}
					
				}).get('');
				
			} else {
				
				new Request.JSON({
					method: 'get',
					async: false,
					url: this.retrieveAnnotationsUrl + '&annotationId=' + this.annotationId,
					noCache: true,
					onRequest: function() {
						// show some rotating loader gif...
					},
					
					onSuccess: function(responseJSON, responseText) {
						this.adminPrivileges = responseJSON.adminPrivileges;
						for (i=0; i<responseJSON.annotations.length; i++) {
							this.annotations.push({
								annotationId: responseJSON.annotations[i].annotationId.toInt(),
								id: responseJSON.annotations[i].id,
								account: responseJSON.annotations[i].account,
								x: responseJSON.annotations[i].x.toFloat(), 
								y: responseJSON.annotations[i].y.toFloat(), 
								w: responseJSON.annotations[i].w.toFloat(), 
								h: responseJSON.annotations[i].h.toFloat(),
								type: responseJSON.annotations[i].type,
								title: responseJSON.annotations[i].title,
								text: responseJSON.annotations[i].text,
								deletable: responseJSON.annotations[i].deletable,
								updatable: responseJSON.annotations[i].updatable,
								visibility: true // for annotation forum topic visibility cannot be hidden
								// forumTopicURL: responseJSON.annotations[i].forumTopicURL 
								// Link To Forum: not enabled because this feature has not to be enabled in
								// the annotation forum
							});
						}
		        	}.bind(this),
		        	
					onError: function(text, error) {
						console.log('error!!!' + text + ' - error : ' + error);
					}
		        	
			    }).get('');
				
			}
		} else if (this.annotationsType == 'local') {
			// in case of local annotation, we do nothing
		}
	}
	/** MEDICI ARCHIVE PROJECT END **/

});
